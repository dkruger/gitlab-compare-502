<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CPowerOffProcess</name>
        <message utf8="true">
            <source>Power off</source>
            <translation>Desligado</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set shuts down.</source>
            <translation>Por favor, espere enquanto o equipamento desliga</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CRebootProcess</name>
        <message utf8="true">
            <source>Reboot</source>
            <translation>Reinicializar</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set reboots.</source>
            <translation>Favor aguardar enquanto o conjunto do teste reinicia.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CShutdownPrompt</name>
        <message utf8="true">
            <source>Power options</source>
            <translation>Opções de energia</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
</TS>
