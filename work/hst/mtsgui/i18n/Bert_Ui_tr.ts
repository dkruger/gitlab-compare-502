<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Yazılım Revizyonu</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>Opsiyon Parola ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Opsiyonlar (Yazılım Seçenekleri):</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Başarısız</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Durduruluyor</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Başlıyor</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Rapor</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Başarısız!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Biçimlendir?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Hazır</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Uzaktan Erişim</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Üretici</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Taranıyor...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Başarısız</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Bağlan</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Unut</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Dosya Seç</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Gönder</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Bağlantıyı kes</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Saniye</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Dakika</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Saatleri</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Günler</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Dosya Adı:</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Sayfa hazırlanıyor. Lütfen bekleyiniz...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Uzun tarih:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Kısa tarih:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Uzun zaman:</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Kısa Zaman:</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Numaralar:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>İçe Aktar</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Tarih: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Zaman: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Tarih: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Zaman: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Aç</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Kopyala</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%2 de %1 boş</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Seçili dosya yok</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>Toplam %1 seçili dosyada</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Toplam %2 de %1 seçilen dosyalarda</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Dosya Adı:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Gönder</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Taranıyor</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Eşleşilen Cihazlar</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Biçimlendir</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Boş Alan</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Toplam Kapasite</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Üretici</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Etiket</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Güncelleme Sürümü</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Yüklü Sürümü</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Biçimlendir:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>Rapor</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>Rapor</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mesaj</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Sil...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tümü</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Yeniden Adlandır</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Geri</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Biçimlendir:</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Rapor</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Özelleştir...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Tek</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Biçimlendir:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Biçimlendir</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dosya Adı</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seç...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Yeniden Adlandır</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Geri</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Yeni opsiyonu yüklemek için anahtarı aşağıya girin.</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Opsiyon anahtarı</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Yükle</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>İçe Aktar</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Yazılım opsiyonlarını satınalmak için Viavi ile irtibata geçiniz.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>Opsiyon Parola ID: </translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Kilit kabul edildi! Yeni opsiyonu aktifleştirmek için yeniden başlat.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Reddedilen Kilit- Süresi dolan opsiyon slotları dolu.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Reddedilen Kilit- Süresi dolan opsiyon zaten yüklü.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Geçersiz Kilit- Lütfen tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%2  de  %1  kilitler Kabul edildi! Yeni opsiyonu aktifleştirmek için yeniden başlat.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>%1' i USB flash sürücüden açılamıyor.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Özelleştir</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dosya Adı</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seç...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Sonraki</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mesaj</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Geri</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Sonraki</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Baştan Başlat</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfigürasyon</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>Rapor</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
