<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI 체크</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>이전 설정 편집</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>프로파일로부터 설정 로딩</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>새 구성 시작 ( 이것은 모든 설정을 기본값로 리셋할 것입니다 )</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>수동</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>테스트를 수동으로 설정하세요</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>프로파일 저장</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>종료 : 수동으로 설정</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>저장됨</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>프로파일 로딩</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>종료 : 프로파일 로딩</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>편집 설정</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>실행</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>CPRI 확인 실행</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>SFP 인증</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>종료 : 테스트</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>테스트 반복</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>상세한 결과 보기</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation> *** CPRI 확인 나가기 ***</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>다시보기</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>인터페이스</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>레이어 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>종료 : 결과 재검토</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>보고서 정보</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>종료 : 보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>상세 결과 다시보기</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>미완료</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NONE</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI 체크:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** CPRI 확인 시작 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** 테스트가 끝났습니다 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** 테스트가 중단되었습니다 ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>프로파일 저장 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>이 테스트를 실행하기 위해 사용된 설정을 저장할 수 있습니다 .&#xA;&#xA; 미래의 테스트를 설정하기 위해 이것을 ( 통체로 또는 개별 &#xA; 프로파일을 선택하여 ) 사용할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>프로파일 로딩 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>로컬 SFP 인증</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>커넥터</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>SFP를 삽입하십시오.</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>SFP 파장 (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>SFP 벤더</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>SFP 벤더 Rev</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>SFP 벤더 P/N</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>권장 비율</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>추가 SFP 데이터 보기</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP가 좋습니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>이 비율로는 SFP를 검증할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP가 허용되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>테스트 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>원단 장치</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>에릭슨</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>기타</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>하드 루프</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>광학 Rx 레벨 최대 한계 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>광학 Rx 레벨 최소 한계 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>왕복 지연 최대 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>CPRI 확인 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP 체크</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>테스트 상태 키</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>스케쥴됨</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>테스트 &#xA; 실행</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>테스트 &#xA; 정지</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>로컬 SFP 결과</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>감지된 SFP가 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>파장 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>최대 Tx 레벨 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>최대 Rx 레벨 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>트랜시버</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>인터페이스 결과</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI 체크 테스트 판정</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>인터페이스 테스트</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>레이어 2 테스트</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD 테스트</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT 테스트</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>신호 있음</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Sync 획득</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Rx Freq 최대 편차 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>코드 위반</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>광학 Rx 레벨 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>레이어 2 결과</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>시작 상태</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>프레임 Sync</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>RTD 결과</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>왕복 지연 Avg (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>BERT 결과</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>패턴 Sync</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>패턴 손실</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Bit/TSE 에러</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>구성</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>장치 종류</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>L1 동기화</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>프로토콜 설정</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M 면 설정</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>작동</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>수동 링크</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>보고서 생성 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>테스트 보고서 정보</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>고객 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>기술자 ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>테스트 위치 :</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>작업 순서 :</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>설명 / 주의 :</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>라디오:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>밴드:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>전체 상태</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>향상된 FC 테스트</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC 테스트</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>연결</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>대칭</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>로컬 설정</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>원격 연결</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>네트워크</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>파이버 채널 설정</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>FC 테스트</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>설정 템플릿</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>이용률</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>처리량 테스트</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>프레임 손실 테스트</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>백투백 테스트</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>버퍼 크레딧 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>테스트 Ctls</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>지속 시간 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>한계 테스트</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>설정 변경</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>고급 파이버 채널 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>고급 이용률 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>고급 처리량 레이턴시 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>고급 백투백 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>고급 프레임 손실 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>서비스 활성화 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>설정 변경 및 테스트 재실행</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>처리량</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>백투백</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>버퍼 크레딧 처리량</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>FC 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>다른 보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>커버 페이지</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>단방향 지연 시간 소스 동기화가 로컬 장치에서의 테스트 도중 손실되었습니다 .  OWD 시간 소스 하드웨어를 위한 연결을 확인하세요 .  완료되지 않았다면 테스트가 계속될 것이지만 , 프레임 지연 결과는 사용할 수 없을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>단방향 지연 시간 소스 동기화가 원격 장치에서의 테스트 도중 손실되었습니다 .  OWD 시간 소스 하드웨어를 위한 연결을 확인하세요 .  완료되지 않았다면 테스트가 계속될 것이지만 , 프레임 지연 결과는 사용할 수 없을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>액티브 루프 발견</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>이웃 주소 결정이 성공적이지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>서비스 #1: 목적지 MAC 에 대한 ARP 요청을 전송하는 중 .</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>로컬 사이드의 서비스 #1: 목적지 MAC 에 대한 ARP 요청을 전송하는 중 .</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>원격 사이드의 서비스 #1: 목적지 MAC 에 대한 ARP 요청을 전송하는 중 .</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>목적지 MAC 에 대한 ARP 요청을 전송하는 중</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>로컬 사이드에서 목적지 MAC 에 대한 ARP 요청을 전송하는 중</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>원격 사이드에서 목적지 MAC 에 대한 ARP 요청을 전송하는 중</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>네트워크 요소 포트는 반이중 작업을 위해 제공됩니다 . 만약 계속 진행하고 싶으면 " 반이중에서 계속" 버튼을 누르세요 . 그렇지 않으면 " 테스트 취소" 를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>루프 업 시도 중 . 액티브 루프 확인 중 .</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>하드웨어 루프 확인 중 .</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>LBM/LBR 루프 확인 중 .</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>영구적인 루프 확인 중 .</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>DHCP 파라미터 요청시간을 초과했습니다 . DHCP 파라미터를 얻을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>루프백 모드를 선택하여 원격 장치로부터 연결이 차단되었습니다 . </translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>최종 전송 프레임 카운트를 결정할 수 없기 때문에 SAMComplete 가 타임아웃 되었습니다 .  측정된 전송 프레임 카운트는 예상외로 계속 증가되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>하드 루프 발견</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>원격 장치로의 연결이 실행되지 않았다면 J-Quickcheck 은 트래픽 연결성 테스트를 실행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>LBM/LBR 루프 발견</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>로컬 링크가 손실되고 원격 장치로의 연결이 차단되었습니다 . 일단 링크가 복구되면 원격 연결을 다시 시도할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>링크가 현재 활성화되지 않았음 .</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>소스와  목적지 IP 가 같습니다 . 소스나 목적지 IP 주소를 다시 구성하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>로컬 및 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 트래픽 애플리케이션이며 , IP 주소 #1 에 있는 원격 애플리케이션은 스트림 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>어떠한 루프도 설정할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>원격 장치로의 연결이 설정되지 않았습니다 . 연결 페이지로 가서 목적지 IP 주소를 확인한 후 원격 연결 버튼을 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>"네트워크" 페이지로 가서 구성을 확인하고 다시 시도하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>광모듈이 준비되길 기다리는 중이거나 광모듈이 존재하지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>영구적인 루프 발견</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>PPPoE 연결 타임아웃 . PPPoE 설정을 확인하고 다시 시도하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>복구할 수 없는 PPPoE 에러가 발생했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>서버에 로그온하려고 시도하는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>원격 연결에 문제가 발견되었습니다 . 원격 장치에 더 이상 접속할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>IP 주소 #1 에서 원격 장치로의 연결을 설정할 수 없었습니다 . 원격 소스 IP 주소를 확인하고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>IP 주소 #1에서 원격 애플리케이션은 루프백 애플리케이션입니다. 이것은 강화된 RFC 2544와 호환되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>로컬 애플리케이션과 원격 애플리케이션이 호환되지 않습니다 . IP 주소 #1 에서 원격 애플리케이션은 TCP WireSpeed 애플리케이션이 아닙니다 .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>원격 장치의 소프트웨어 버전이 더 새로운 버전으로 나타났습니다 . 테스트를 계속하면 몇 가지 기능이 제한될 수 있습니다 . 최적의 성능을 위해 이 장치의 소프트웨어를 업그레이드할 것을 권장합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>원격 장치의 소프트웨어 버전이 더 오래된 버전으로 나타났습니다 . 테스트를 계속하면 몇 가지 기능이 제한될 수 있습니다 . 최적의 성능을 위해 원격 장치의 소프트웨어를 업그레이드할 것을 권장합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>원격 장치의 소프트웨어 버전을 확인할 수 없었습니다 . 테스트를 계속하면 몇 가지 기능이 제한될 수 있습니다 . 장치 간의 소프트웨어 버전을 일치시킨 후 테스트하기를 권장합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>루프 업 시도가 성공적이지 않았습니다 . 다시 시도하고 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>선택된 템플릿을 위한 설정이 성공적으로 적용되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>원격 장치의 소프트웨어 버전을 확인할 수 없거나 이 장치의 버전과 맞지 않습니다. 테스트를 계속하면 몇 가지 기능이 제한될 수 있습니다. 장치 간의 소프트웨어 버전을 일치시킨 후 테스트하기를 권장합니다.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>명시적 로그인을 완료하지 못했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>로컬 애플리케이션과 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 레이어 #1 애플리케이션이며 , IP 주소 #2 에서 원격 애플리케이션은 레이어 #3 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>로컬 장치 라인 속도 (#1 Mbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 연결" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>로컬 장치 라인 속도 (#1 kbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 연결" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>로컬 장치 라인 속도 (#1 Mbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 연결" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>로컬 장치 라인 속도 (#1 kbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 연결" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>로컬 장치 라인 속도 (#1 Mbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 대칭" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>로컬 장치 라인 속도 (#1 kbps) 가 원격 장치 (#2 Mpbs) 의 속도와 일치하지 않습니다 . " 대칭" 페이지에서 비대칭으로 재설정하고 테스트를 재시작하십시오 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 적어도 하나의 테스트를 선택해야 합니다 . 적어도 하나의 테스트를 선택하고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>테스트 할 프레임 크기를 선택하지 않았습니다 . 시작하기 전에 적어도 하나의 프레임 크기를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>설정된 프레임 손실 한계를 초과한 프레임 손실이 마지막 초에서 발견되었습니다 . 수동 트래픽 테스트로 링크의 성능을 확인하세요 . 수동 테스트를 끝낸 후에 RFC 2544 를 재실행할 수 있습니다 .&#xA; 권장 수동 테스트 설정 :&#xA; 프레임 크기 : #1 바이트 &#xA; 트래픽 : #2 Mbps 로 일정함 &#xA; 테스트 지속 시간 : 설정된 테스트 지속 시간의 최소 3 배 . 처리량 테스트 지속 시간은 #3 초였습니다 .&#xA; 모니터링 결과 :&#xA; 에러 이벤트를 검색하기 위해 요약 상태 화면을 사용하세요 . 만약 산발적인 방법으로 에러 카운터가 증가하고 있다면 , 더 낮은 다른 트래픽 속도에서 수동 테스트를 실행하세요 . 만약 더 낮은 속도에서 여전히 에러가 발생한다면 , 최대 로드와는 관련이 없는 일반적인 문제가 링크에 있을 수 있습니다 . 산발적이거나 지속적인 프레임 손실 이벤트가 있는지 확인하기 위해 그래픽 결과 프레임 손실률 Cur 그래프도 사용할 수 있습니다 . 만약 산발적 에러와 관련된 문제를 해결할 수 없다면 , 작은 프레임 손실률을 허용하도록 프레임 손실 오차 한계를 설정할 수 있습니다 . 주의 : 일단 프레임 손실 오차를 사용하면 , 테스트는 RFC 2544 권장사항을 따르지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>주의 : 송신장치와 수신장치 사이에 VLAN 스택의 단계가 다르기 때문에, 각 포트 사이의 트래픽 차이를 보상하기 위해 설정된 트래픽이 조정될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>#1 바이트 프레임</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>#1 바이트 패킷</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>트래픽이 정지되었지만 L2 필터 Rx 액터나 프레임 카운트가 이전 10 초 이상 계속 증가했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>최대 처리량 속도에 제로 - 인</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>#1 L1 Mbps 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>#1 L2 Mbps 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>#1 L1 kbps 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>#1 L2 kbps 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>시도 중 #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>지금 #1 L1 Mbps 확인 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>지금 #1 L2 Mbps 확인 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>지금 #1 L1 kbps 확인 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>지금 #1 L2 kbps 확인 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>지금 #1 % 확인 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>측정된 최대 처리량 : #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>측정된 최대 처리량 : #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>측정된 최대 처리량 : #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>측정된 최대 처리량 : #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>측정된 최대 처리량 : #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>최대 처리량 측정은 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>프레임 손실 테스트 (RFC 2544 표준 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>프레임 손실 테스트 ( 하향식 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>프레임 손실 테스트 ( 상향식 )</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>#1 L1 Mbps 로드에서 테스트 실행 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>#1 L2 Mbps 로드에서 테스트 실행 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>#1 L1 kbps 로드에서 테스트 실행 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>#1 L2 kbps 로드에서 테스트 실행 중 . 이것은 #2 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>#1 로드에서 테스트 실행 중 .  이것은 #2 초가 결릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>백투백 프레임 테스트</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>시도 #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>정지 프레임 발견됨</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 패킷 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 프레임 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 패킷 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 프레임 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>버스트 헌트 테스트</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>#1 kB 버스트 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>#1 kB 이상</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>#1 kB 이하</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>버퍼 크기는 #1 kB 입니다</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>버퍼 크기는 #1 kB 미만입니다</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>버퍼 크기는 #1 kB 이상입니다</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>전송된 #1 프레임</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>수신된 프레임 #1</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>손실된 프레임 #1</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>버스트 (CBS) 테스트</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>예상 CBS: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>예상 CBS: 사용 불가</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>CBS 테스트를 건너뛸 것입니다 . 테스트 버스트 크기가 너무 커서 이 구성을 정확하게 테스트 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>CBS 테스트를 건너뛸 것입니다 . 테스트 지속 시간이 충분히 길지 않아서 이 구성을 정확하게 테스트 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>패킷 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>프레임 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>패킷 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>프레임 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>버스트 폴리싱 시도 #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>시스템 복구 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>이 패킷 크기에 대해서는 테스트가 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>테스트는 이 프레임 크기에 대해서는 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>시도 중에 #1 / #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>처리량 테스트가 관찰된 프레임 손실 없이 최대 대역폭에서 통과되었기 때문에 프레임 손실을 유도할 수 없을 것입니다</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>손실 이벤트를 유도할 수 없습니다 . 이 패킷 크기에 대해서는 테스트가 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>손실 이벤트를 유도할 수 없습니다 . 이 프레임 크기에 대해서는 테스트가 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>평균 복구 시간 : #1 초 이상</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>#1 초 이상</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>평균 복구 시간 : 사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>평균 복구 시간 : #1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 us</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>최적 크레딧 버퍼에 제로 - 인 . #1 크레딧 테스트 중</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>#1 크레딧 테스트 중</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>지금 #1 크레딧으로 확인 중입니다 .  이것은 #2 초가 결릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>버퍼 크레딧 처리량 테스트</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>주의 : 버퍼 크레딧이 2 미만인 하드 루프를 가정합니다 . 이 테스트는 유효하지 않습니다</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>주의 : 하드 루프 가정을 근거로 , 파이버의 두 배 길이를 보상하기 위해 각 단계에서 버퍼 크레딧의 수의 두배로 처리량 측정이 이루어집니</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>#1 버퍼 크레딧에서 처리량 측정 중</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>처리량 및 레이턴시 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>처리량과 패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>처리량 , 레이턴시 및 패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>레이턴시와 패킷 지터 시도 #1. 이것은 #2 초가 걸릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>레이턴시 지터 테스트 시도 #1. 이것은 #2 초가 걸릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>레이턴시 테스트 시도 #1. 이것은 #2 초가 걸릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>확인된 처리량 로드의 #2% 에서 레이턴시 테스트 시도 #1. 이것은 #3 초가 걸릴 것입니다</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 RFC 2544 테스트 #2 시작</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 FC 테스트 #2 시작</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>테스트 완료 .</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>결과를 저장하고 있습니다. 기다려주세요.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>확장 로드 테스트</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>FC 테스트:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>프레임 종류</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>테스트 모드</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>관리 도메인 레벨</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>전송자 TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>캡슐화</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN 스택 깊이</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>SVLAN DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>사용자 SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>CVLAN 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>SVLAN 7 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>사용자 SVLAN 7 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>SVLAN 7 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>SVLAN 6 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>사용자 SVLAN 6 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>SVLAN 6 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>SVLAN 5 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>사용자 SVLAN 5 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>SVLAN 5 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>SVLAN 4 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>사용자 SVLAN 4 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>SVLAN 4 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>SVLAN 3 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>사용자 SVLAN 3 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>SVLAN 3 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>SVLAN 2 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>사용자 SVLAN 2 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>SVLAN 2 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>SVLAN 1 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>사용자 SVLAN 1 TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>SVLAN 1 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>루프 종류</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>자동 증가 SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>시퀀스안에 MAC 수</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>IP EtherType 비활성</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Oos 결과 비활성</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>DA 종류</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>목적지 MAC</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>데이터 모드</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>인증 사용</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>사용자 이름</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>비밀번호</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>서비스 제공자</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>서비스 이름</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>소스 IP 종류</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>소스 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>기본값 게이트웨이</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>목적지 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>생존 시간 ( 홉 )</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>IP ID 증가하는 중</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>링크 - 로컬 주소 소싱</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>글로벌 주소 소싱</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>서브넷 프리픽스 길이</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>목적지 주소</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>트래픽 클래스</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>플로우 라벨</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>홉 리미트</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>트래픽 모드</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>포트 서비스 종류 소싱</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>소스 포트</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>목적지 포트 서비스 종류</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>목적지 포트</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>ATP 리슨 IP 종류</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>ATP 리슨 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>소스 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>목적지 ID</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>시퀀스 ID</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>발신자 ID</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>응답자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna 페이로드 버전</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>레이턴시 측정 정밀도</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>대역폭 단위</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>최대 테스트 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>업스트림 최대 테스트 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>최대 테스트 대역폭 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>업스트림 최대 테스트 대역폭 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>다운스트림 최대 테스트 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>다운스트림 최대 테스트 대역폭 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>최대 테스트 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>실제 100% 트래픽 허용</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>처리량 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>업스트림 처리량 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>다운스트림 처리량 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>처리량 제로 - 인 프로세스</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>처리량 프레임 손실 오차 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>업스트림 처리량 프레임 손실 오차 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>다운스트림 처리량 프레임 손실 오차 (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>모든 테스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>모든 테스트 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>처리량 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>처리량 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>처리량 통과 한계 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>업스트림 처리량 통과 한계 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>처리량 통과 한계 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>업스트림 처리량 통과 한계 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>다운스트림 처리량 통과 한계 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>다운스트림 처리량 통과 한계 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>처리량 통과 한계 (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>레이턴시 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>레이턴시 시도 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>지연시간 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>레이턴시 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>레이턴시 통과 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>업스트림 레이턴시 통과 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>다운스트림 레이턴시 통과 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>패킷 지터 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>패킷 지터 시도 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>패킷 지터 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>패킷 지터 패스 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>업스트림 패킷 지터 통과 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>다운스트림 패킷 지터 통과 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>프레임 손실 테스트 절차</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>프레임 손실 시도 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>프레임 손실 대역폭 입도 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>업스트림 프레임 손실 대역폭 입도 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>프레임 손실 대역폭 입도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>업스트림 프레임 손실 대역폭 입도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>다운스트림 프레임 손실 대역폭 입도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>다운스트림 프레임 손실 대역폭 입도 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>프레임 손실 대역폭 입도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>프레임 손실 범위 최소 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>프레임 손실 범위 최소 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>프레임 손실 범위 최소 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>프레임 손실 범위 최대 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>프레임 손실 범위 최대 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>프레임 손실 범위 최대 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>업스트림 프레임 손실 범위 최소 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>업스트림 프레임 손실 범위 최소 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>업스트림 프레임 손실 범위 최대 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>업스트림 프레임 손실 범위 최대 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>다운스트림 프레임 손실 범위 최소 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>다운스트림 프레임 손실 범위 최소 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>다운스트림 프레임 손실 범위 최대 (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>다운스트림 프레임 손실 범위 최대 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>단계의 프레임 손실 수</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>백투백 시도 수</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>백투백 입도 ( 프레임 )</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>백투백 최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>업스트림 백투백 최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>다운스트림 백투백 최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>일시정지 프레임 무시</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>버스트 테스트 종류</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>버스트 CBS 크기 (KB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>업스트림 버스트 CBS 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>다운스트림 버스트 CBS 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>버스트 헌트 최소 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>버스트 헌트 최대 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>업스트림 버스트 헌트 최소 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>업스트림 버스트 헌트 최대 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>다운스트림 버스트 헌트 최소 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>다운스트림 버스트 헌트 최대 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>버스트 테스트 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>버스트 CBS 통과 / 실패 보기</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>버스트 헌트 통과 / 실패 보기</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>버스트 헌트 크기 한계 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>업스트림 버스트 헌트 크기 한계 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>다운스트림 버스트 헌트 크기 한계 (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>시스템 복구 시도 수</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>시스템 복구 오버로드 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>확장 로드 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>확장 로드 처리량 스케일링 (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>확장 로드 패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>확장 로드 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>버퍼 크레딧 로그인 종류</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>버퍼 크레딧 최대 버퍼 크기</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>버퍼 크레딧 처리량 단계</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>버퍼 크레딧 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>원격 루프 다운 성공 : LLB 모드 외 장치 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>원격 루프 다운 성공 : 투명 LLB 모드 외 장치 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>원격 장치 **#1** 는 이미 루프 다운 되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>설정 변경으로 인한 원격 루프 다운</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>원격 루프 업 성공 : LLB 모드 내 장치 **#1** </translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>원격 루프 업 성공 : 투명 LLB 모드 내 장치 **#1** </translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>원격 장치 **#1** 는 이미 LLB 모드에 있었습니다</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>원격 장치 **#1** 는 이미 투명 LLB 모드에 있었습니다</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>셀프 루프 또는 다른 포트 루프는 지원하지 않습니다</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>스트림 #1: 원격 루프 다운 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>스트림 #1: 원격 루프 업 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>원격 루프 다운 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>원격 투명 루프 다운 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>원격 루프 업 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>원격 투명 루프 업 실패 : 확인 응답 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>글로벌 주소 중복 주소 탐지가 시작되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>글로벌 주소 설정이 실패하였습니다 ( 설정 확인 ).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>글로벌 주소 상에서 중복 주소 탐지를 기다리는 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>중복 주소 탐지가 성공하였습니다 . 글로벌 주소가 유효합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>중복 주소 탐지가 실패하였습니다 . 글로벌 주소가 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>링크 - 로컬 주소 중복 주소 탐지가 시작되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>링크 - 로컬 주소 설정이 실패하였습니다 ( 설정 확인 ).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>링크 - 로컬 주소 상에서 중복 주소 탐지를 기다리는 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>중복 주소 탐지가 성공하였습니다 . 링크 - 로컬 주소가 유효합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>중복 주소 탐지가 실패하였습니다 . 링크 - 로컬 주소가 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>스테이트리스 IP 검색이 시작되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>스테이트리스 IP 를 획득하는데 실패하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>스테이트리스 IP 를 성공적으로 획득하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>스테이트풀 IP 검색이 시작되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>스테이트풀 IP 를 제공하기 위한 라우터가 발견되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>스테이트풀 IP 검색 재시도 중 .</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>스테이트풀 IP 를 성공적으로 획득하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>연결할 수 없는 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>연결할 수 없는 호스트</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>연결할 수 없는 프로토콜</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>연결할 수 없는 포트</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>너무 긴 메시지</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>알려지지 않은 목적지 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>알려지지 않은 목적지 호스트</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>금지된 목적지 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>금지된 목적지 호스트</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>TOS 를 위해 연결할 수 없는 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>TOS 를 위해 연결할 수 없는 호스트</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>전송 중에 시간 초과</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>재조합 중에 시간 초과</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>ICMP 알려지지 않은 에러</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>연결할 수 없는 목적지</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>연결할 수 없는 주소</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>목적지로의 루트 없음</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>목적지가 이웃이 아님</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>목적지와의 통신이 관리 상으로 금지됨</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>너무 큰 패킷</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>전송에서 홉 제한 초과</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>프래그먼트 재조합 시간 초과</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>오류가 발생한 헤더 필드 발생</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>인식되지 않은 다음 헤더 종류 발생</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>인식되지 않은 IPv6 옵션 발생</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>비활성화</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE 액티브</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 액티브</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>네트워크 업</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>사용자 요청 비활성</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>데이터 레이어 정지</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 인증 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 실패 알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 실패</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>유효하지 않은 설정</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>내부 에러</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>최대</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP 성공 . 목적지 MAC 획득</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>ARP 서비스를 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>No ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>중단</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>반이중에서 계속</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>J-QuickCheck 정지</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 테스트</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>업스트림</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>다운스트림</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>J-QuickCheck 테스트</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>원격 루프 상태</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>로컬 루프 상태</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>PPPoE 상태</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>IPv6 상태</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>ARP 상태</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>DHCP 상태</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>ICMP 상태</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>이웃 디스커버리 상태</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Autoconfig 상태</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>주소 상태</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>장치 복구</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>장치 검색</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>검색</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>선택된 장치 사용</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>종료 중</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>원격으로 &#xA; 연결 중</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>원격 &#xA; 연결</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>연결 해제</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>원격으로 &#xA; 연결됨</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>연결 중</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>대칭</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>비대칭</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>단방향</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>루프백</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>다운스트림 및 업스트림</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>양방향</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>처리량 :</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>다운스트림과 업스트림 처리량이 동일합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>다운스트림과 업스트림 처리량이 다릅니다 .</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>한 방향으로만 네트워크를 테스트하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>측정 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>트래픽이 로컬로 생성되고 측정치는 로컬로 얻게 됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>트래픽이 로컬 및 원격으로 생성되고 측정치는 각 방향으로 얻게 됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>측정 방향 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>트래픽이 로컬로 생성되고 원격 테스트 장치에 의해 측정됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>트래픽이 원격으로 생성되고 로컬 테스트 장치에 의해 측정됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>왕복 시간 지연</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>편도 지연</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>왕복 지연 측정 전용 .&#xA; 원격 장치가 단방향 지연을 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>단일 방향 지연 측정 전용 .&#xA; 원격 장치가 양방향 RTD 를 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>지연 측정을 실행할 수 없습니다 .&#xA; 원격 장치가 양방향 RTD 또는 단방향 지연을 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>지연 측정을 실행할 수 없습니다 .&#xA; 원격 장치가 단방향 지연을 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>지연 측정을 실행할 수 없습니다 .&#xA; 원격 장치가 양방향 RTD 를 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>단일 방향 지연 측정 전용 .&#xA; 단일 방향으로 테스트할 때 RTD 는 지원되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>지연 측정을 실행할 수 없습니다 .&#xA; 단일 방향으로 테스트할 때 RTD 는 지원되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>지연 측정을 실행할 수 없습니다 .&#xA; 원격 장치가 단방향 지연을 할 수 없습니다 .&#xA; 단일 방향으로 테스트할 때 RTD 가 지원되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>지연 측정을 할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>레이턴시 측정 종류</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>로컬</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>원격</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>원격 Viavi 테스트 장치가 필요합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>버전 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>버전 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>광학 선택</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>원단까지 통신 채널용 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>루프백 테스트를 위해 필요한 로컬 IP 주소 설정이 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>정적</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>정적 - 서비스 당</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>정적 – 싱글</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>수동</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>스테이트풀</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>스테이트리스</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>소스 링크 - 로컬 Addr</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>소스 글로벌 Addr</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>자동 획득</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>사용자 정의</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>ARP 모드</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>소스 주소 종류</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>PPPoE 모드</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>고급</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>활성화됨</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>비활성화됨</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>고객 소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>공장 초기 설정</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>기본값 소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>고객 기본값 MAC</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>사용자 소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>고객 사용자 MAC</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>연결 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>스택 VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>로컬 네트워크 포트에서 몇 개의 VLAN 이 사용됩니까 ?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>VLANs 없음 ,</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs ( 스택 VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>VLAN ID 설정 입력 :</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>스택 깊이</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>사용자</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>디스커버리 서버 설정</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>주의: ‘원격으로 연결’하는데 링크-로컬 목적지 IP를 사용할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>글로벌 목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>핑잉</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>핑</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>목적지 IP 를 찾는 것을 도와주세요</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>로컬 포트</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>상태 알 수 없음</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>링크 손실 </translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>로컬 포트 :</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>자동 협상</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>분석 중</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Auto Negotiation:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>연결 대기중</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>연결 중...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>재시도 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTED</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>연결 실패 ...</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>연결 취소</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>통신 채널:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>디스커버리 서버 고급 설정</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>디스커버리 서버에서 목적지 IP 얻기</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>서버 IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>서버 포트</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>서버 암호</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>요청 임대 시간 ( 분 )</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>허용 임대 시간 ( 분 )</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>L2 네트워크 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>로컬 장치 설정</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>트래픽</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>LBM 트래픽</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 ( 가장 낮은 값 )</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 ( 가장 높은 값 )</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>사용자 SVLAN TPID (Hex)</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>루프 종류, EtherType 및 MAC 주소 설정</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>MAC 주소, EtherType 및 LBM 설정</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>MAC 주소 설정</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>MAC 주소 및 ARP 모드 설정</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>목적지 종류</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>유니캐스트</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>멀티캐스트</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>브로드캐스트</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>VLAN 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>원격 장치가 연결되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>L2 네트워크 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>원격 장치 설정</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>L3 네트워크 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>트래픽 클래스, 플로우 라벨 및 홉 제한 설정</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>귀하의 네트워크는 어떤 종류의 IP 우선순위를 사용합니까 ?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>생존 시간, IP ID 증가 및 PPPoE 모드 설정</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>생존 시간 및 PPPoE 모드 설정</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>생존 시간 및 IP ID 증가 설정</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>생존 시간 설정</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>귀하의 네트워크는 어떤 IP 우선순위를 사용합니까 ?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>L3 네트워크 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>L4 네트워크 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>소스 서비스 종류</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>DHCP- 클라이언트</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>DHCP- 서버</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>핑거</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>목적지 서비스 종류</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>ATP 리슨 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>L4 네트워크 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>루프 종류 및 시퀀스 , 응답자 및 발신자 ID 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>고급 L2 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>소스 종류</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>기본값 MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>사용자 MAC</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>LBM 설정</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>LBM 종류</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>전송자 TLV 활성화</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>고급 L2 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>고급 L3 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>생존 시간 ( 홉 )</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>고급 L3 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>고급 L4 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>고급 L4 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>고급 네트워크 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>템플릿</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>설정 템플릿을 사용하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Viavi 강화 패킷 접속 속도가 전송 속도보다 더 큽니다</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - 최상</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - 유럽</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - 전 세계</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - 메트로</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - 이동식 백홀 H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - 지역</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - VoIP 데이터 에뮬레이션</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC2544 비트 투명</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 패킷 접속 속도가 전송 속도보다 큽니다</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 패킷 접속 속도가 전송 속도와 같습니다</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>템플릿 적용</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>템플릿 설정 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>처리량 프레임 손실 (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>레이턴시 한계 (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>패킷 지터 한계 (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>프레임 크기 : 디폴트</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>최대 대역폭 ( 업스트림 ) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>최대 대역폭 ( 다운스트림 ) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>최대 대역폭 : #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>테스트 : 처리량 및 레이턴시 테스트</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>테스트 : 처리량 , 레이턴시 , 프레임 손실 및 백투백</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>지속 시간 및 시도 : 1 회 시도 #1 초</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>처리량 알고리즘 : Viavi 강화</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>처리량 알고리즘 : RFC 2544 표준</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>처리량 한계 ( 업스트림 ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>처리량 한계 ( 다운스트림 ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>처리량 한계 : #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>프레임 손실 알고리즘 : RFC 2544 표준</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>백투백 버스트 시간 : 1 초</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>백투백 입도 : 1 프레임</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>고급 이용률 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>최대 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>다운스트림 최대 대역폭 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>다운스트림 최대 대역폭 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>다운스트림 최대 대역폭 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>다운스트림 최대 대역폭 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>다운스트림 최대 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>업스트림 최대 대역폭 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>업스트림 최대 대역폭 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>업스트림 최대 대역폭 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>업스트림 최대 대역폭 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>업스트림 최대 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>최대 대역폭 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>최대 대역폭 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>최대 대역폭 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>최대 대역폭 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>최대 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>선택된 프레임</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>길이 종류</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>패킷</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>리셋</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>모두 삭제</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>업스트림 &#xA; 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>원격 장치가 연결되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>다운스트림 &#xA; 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>원격 &#xA; 장치가 &#xA; 연결되지 &#xA; 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>선택된 테스트</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>RFC 2544 테스트</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>레이턴시 ( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>버퍼 크레딧 ( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>버퍼 크레딧 처리량 ( 버퍼 크레딧 필요 )</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>시스템 복구 ( 루프백 전용 및 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>추가 테스트</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>패킷 지터 ( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>버스트 테스트</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>확장 로드 ( 루프백 전용 )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>처리량 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>제로 - 인 프로세스</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation> RFC 2544 표준</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi 강화</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>고급 처리량 측정 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>다운스트림 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>다운스트림 측정 정확도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>다운스트림 측정 정확도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>다운스트림 측정 정확도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>다운스트림 측정 정확도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>다운스트림 측정 정확도 (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>1.0% 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>0.1% 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>0.01% 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>0.001% 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>400 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>40 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>4 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>.4 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>1000 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>100 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>10 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>1 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>.1 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>.01 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>.001 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>.0001 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>92.942 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>9.2942 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>.9294 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>.0929 Mbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>10000 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>1000 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>100 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>10 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>1 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>.1 kbps 이내로</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>업스트림 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>업스트림 측정 정확도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>업스트림 측정 정확도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>업스트림 측정 정확도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>업스트림 측정 정확도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>업스트림 측정 정확도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>측정 정확도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>측정 정확도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>측정 정확도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>측정 정확도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>측정 정확도 (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>더 많은 정보</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>문제 해결</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>시운전</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>고급 처리량 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>레이턴시 / 패킷 지터 테스트 지속 시간 별도 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>레이턴시 테스트 지속 시간 별도 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>패킷 지터 테스트 지속 시간 별도 설정</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>프레임 손실 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>테스트 절차</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>하향식</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>상향식</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>단계 수</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 단계당 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 단계당 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % 단계당 라인 속도</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>다운스트림 테스트 범위 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>다운스트림 테스트 범위 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>다운스트림 테스트 범위 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>다운스트림 테스트 범위 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>다운스트림 테스트 범위 (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>최대</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>다운스트림 대역폭 입도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>다운스트림 대역폭 입도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>다운스트림 대역폭 입도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>다운스트림 대역폭 입도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>다운스트림 대역폭 입도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>업스트림 테스트 범위 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>업스트림 테스트 범위 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>업스트림 테스트 범위 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>업스트림 테스트 범위 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>업스트림 테스트 범위 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>업스트림 대역폭 입도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>업스트림 대역폭 입도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>업스트림 대역폭 입도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>업스트림 대역폭 입도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>업스트림 대역폭 입도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>테스트 범위 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>테스트 범위 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>테스트 범위 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>테스트 범위 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>테스트 범위 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>대역폭 입도 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>대역폭 입도 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>대역폭 입도 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>대역폭 입도 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>대역폭 입도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>고급 프레임 손실 측정 테스트 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>고급 프레임 손실 설정</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>선택적 테스트 측정</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>레이턴시 측정</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>패킷 지터 측정</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>백투백 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>다운스트림 최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>업스트림 최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>최대 버스트 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>버스트 입도 ( 프레임 )</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>고급 백투백 테스트 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>일시정지 프레임 정책</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>버퍼 크레딧 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>플로우 컨트롤 로그인 타입</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>함축적 ( 투명 링크 )</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>명시적 (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>최대 버퍼 크기</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>처리량 단계</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>테스트 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>테스트 지속 시간을 별도로 설정하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>패킷 지터</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>레이턴시 / 패킷 지터</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>버스트 (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>시스템 복구</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>모든 테스트</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>통과 / 실패 &#xA; 보기</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>업스트림 &#xA; 한계</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>다운스트림 &#xA; 한계</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>한계</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>처리량 역치 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>처리량 역치 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>처리량 역치 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>처리량 역치 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>처리량 한계 (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>원격 장치가 &#xA; 연결되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>레이턴시 RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>레이턴시 OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>패킷 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>버스트 헌트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>버스트 (CBS 폴리싱 )</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>높은 정밀도 – 낮은 지연</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>낮은 정밀도 – 높은 지연</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>FC 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>FC 테스트 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>버퍼 크레딧 &#xA; 처리량</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>업스트림 처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>업스트림 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>다운스트림 처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>다운스트림 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>처리량 예외</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>업스트림 처리량 예외</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>다운스트림 처리량 예외</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>처리량 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>처리량 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>프레임 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>프레임 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>프레임 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>프레임 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>프레임 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>프레임 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>프레임 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>프레임 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>프레임 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>프레임 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>통과 / 실패</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>프레임 길이 &#xA;( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>패킷 길이 &#xA;( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>측정된 &#xA;속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>측정된 &#xA; 속도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>측정된 &#xA; 속도 (frms/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>R_RDY&#xA; 발견</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Cfg 속도 &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>측정된 L1&#xA; 속도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>측정된 L1&#xA; 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>측정된 L1&#xA;(% 라인 속도 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>측정된 L2&#xA; 속도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>측정된 L2&#xA; 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>측정된 L2&#xA;(% 라인 속도 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>측정된 L3&#xA; 속도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>측정된 L3&#xA; 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>측정된 L3&#xA;(% 라인 속도 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>측정된 L4&#xA; 속도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>측정된 L4&#xA; 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>측정된 L4&#xA;(% 라인 속도 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>측정된 속도 &#xA; (frms/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>측정된 속도 &#xA; (pkts/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>일시 정지 &#xA; 발견</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Cfg 속도 &#xA;(L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Cfg 속도 &#xA;(L2, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Cfg 속도 &#xA;(L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Cfg 속도 &#xA;(L2, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>예외</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>OoS 프레임 &#xA; 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Acterna 페이로드 &#xA; 에러가 발견되었습니다</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>FCS&#xA; 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>IP 검사 합계 &#xA; 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>TCP/UDP 검사 합계 &#xA; 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>레이턴시 테스트</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>업스트림 레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>업스트림 레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>다운스트림 레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>다운스트림 레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>레이턴시 &#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>레이턴시 &#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>측정된 &#xA;% 라인 속도</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>일시 정지 &#xA; 발견</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>업스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>다운스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>프레임 손실 결과</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>프레임 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>구성된 속도 (L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>구성된 속도 (L2, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>구성된 속도 (L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>구성된 속도 (L2, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>설정된 속도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>프레임 손실 (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>처리량 속도 &#xA;(L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>처리량 속도 &#xA;(L2, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>처리량 속도 &#xA;(L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>처리량 속도 &#xA;(L2, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>처리량 속도 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>프레임 손실률 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>최대 평균 패킷 &#xA; 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>에러 &#xA; 발견</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>OoS&#xA; 발견</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Cfg 속도 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>업스트림 백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>다운스트림 백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>평균 &#xA; 버스트 프레임</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>평균 &#xA; 버스트 초</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>버퍼 크레딧 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>주의</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>최소 버퍼 크기 &#xA;( 크레딧 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>버퍼 크레딧 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>버퍼 크레딧 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>크레드 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>크레드 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>크레드 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>크레드 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>크레드 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>크레드 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>크레드 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>크레드 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>크레드 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>크레드 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>버퍼 크기 &#xA;( 크레딧 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>측정된 속도 &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>측정된 속도 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>측정된 속도 &#xA; ( 프레임 / 초 )</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>J-Proof - 이더넷 L2 투명성 테스트</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>J-Proof 프레임</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>J-Proof 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>요약</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>종료 : 상세한 결과</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>J-Proof 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>적용 중</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>레이어 2 서비스의 투명성 테스트에 대해 프레임 종류 설정</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   프로토콜   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>프로토콜</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> 프레임 종류 </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> 캡슐화 </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>스택</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>프레임 &#xA; 크기</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>프레임 크기</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>갯수</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>속도 &#xA;(fr/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>속도</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>타임아웃 &#xA;(msec)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>시간초과</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>컨트롤</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>DEI&#xA;비트</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>사용자 SVLAN TPID</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>자동 -Inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>자동 Inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>자동 -Inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>자동 Inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>캡슐화</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>캡슐화</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>프레임 &#xA; 종류</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN Id</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit Inc</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>빠른 &#xA; 설정</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>프레임 &#xA; 추가</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>프레임 &#xA; 제거</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>소스 주소는 모든 프레임에 공통적입니다 . 이것은 로컬 설정 페이지에서 구성됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Pbit 증가</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>VLAN 스택</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>SPbit 증가</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>길이</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>데이터</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>J-Proof 설정</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>집중도</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>빠른 (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>전체 (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>가족</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>전체</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>스패닝 트리</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>레이저 &#xA;On</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>레이저 &#xA;Off</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>프레임 시퀀스 &#xA; 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>프레임 시퀀스 &#xA; 정지</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof 테스트</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>정지됨</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>진행중</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>페이로드 에러</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>헤더 에러</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>카운트 불일치</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>중지됨</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>결과 요약</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>휴지</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>진행중</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>페이로드 불일치</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>헤더 불일치</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>이더넷</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>J-Proof 결과</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>광학 Rx&#xA; 리셋</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>QuickCheck 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>QuickCheck 설정</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>QuickCheck 확장 로드 결과</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>세부사항</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>싱글</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>스트림 당</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>처리량 테스트:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>QuickCheck 결과</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>확장 로드 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Tx 프레임 카운트</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Rx 프레임 카운트</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>에러 프레임 카운트</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>OoS 프레임 카운트</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>손실 프레임 카운트</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>프레임 손실 비율</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>하드웨어</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>영구적</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>액티브</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>처리량: %1 Mbps (L1), 지속 시간: %2 초, 프레임 크기: %3 바이트</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>처리량 : %1 kbps (L1), 지속 시간 : %2 초 , 프레임 크기 : %3 바이트</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>원했던 것이 아닙니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>목적지 검색</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>ARP 상태:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>원격 루프</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>액티브 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>하드 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>영구적인 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>LBM/LBR 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>액티브 루프</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>영구적인 루프</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR 루프</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>루프 실패</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>원격 루프 :</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>측정된 처리량 (L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>결정되지 않음</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>측정된 처리량 (L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>에러 보기</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>로드 (L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>로드 (L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>처리량 테스트 지속 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>비트 전송률</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>성능 테스트 지속 시간</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 초</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 초</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 분</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 분</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 분</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 분</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 시간</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 시간</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 시간</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 시간</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>사용자 정의</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>테스트 지속 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>성능 테스트 지속 시간 ( 분 )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>점보</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>사용자 길이</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>점보 길이</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  전기 커넥터:  10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>레이저 파장</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>전기 커넥터</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>광학 커넥터</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>세부사항 ...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>링크 액티브</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>트래픽 결과</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>에러 카운트</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>에러 프레임</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 프레임</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>손실된 프레임</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>SFP/XFP 세부사항 </translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>파장 (nm): 튜닝 가능한 SFP, 채널 / 파장 튜닝 셋업을 참조하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>케이블 길이 (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>튜닝 지원됨</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>파장 ; 채널</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>파장</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>채널</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;P>추천 비율&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>공칭 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>최소 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>최대 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>제조사</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>제조사 PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>제조사 리비전</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>전원 레벨 종류</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>진단 모니터링</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>진단 바이트</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>JMEP 설정</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>파장 (nm): 튜닝 가능한 장치, 채널/파장 튜닝 셋업을 참조하세요.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>CFP/QSFP 세부사항 </translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>CFP2/QSFP 세부사항 </translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>CFP4/QSFP 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>제조사 SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>날짜 코드</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>로트 코드</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW 버전 #</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW Spec. Rev#</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA Mgmt. I/F Rev#</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>파워 클래스</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx 전원 레벨 종류</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Rx 최대 람다 전원 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Tx 최대 람다 전원 (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>활성화된 광케이블의 #</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>파이버당 파장</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>진단 바이트</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>파이버 범위 당 WL (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>최대 네트워크 레인 비트율 (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>모듈 ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>지원되는 트래픽율</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>공칭 파장 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>공칭 비트 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** 100GigE RS-FEC 애플리케이션을 위한 권장 사용법 ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>CFP 전문가</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>CFP2 전문가</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>CFP4 전문가</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Viavi 루프백 활성화</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>CFP 비아비 루프백 활성화</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>CFP 전문가 모드 활성화</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>CFP2 전문가 모드 활성화</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>CFP4 전문가 모드 활성화</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>프리엠퍼시스</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>CFP Tx 프리엠퍼시스</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>로우</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>공칭</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>하이</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>클럭 디바이더</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>CFP Tx 클록 디바이더</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>스큐 오프셋 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>CFP Tx 스큐 오프셋</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>인버트 극성</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP Tx 역 극성</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Tx FIFO 리셋</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>균등화</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>CFP Rx 균등화</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP Rx 인버트 극성</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>LOS 무시</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP Rx 무시 LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Rx FIFO 리셋</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>QSFP 전문가</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>QSFP 전문가 모드 활성화</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx 무시 LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>CDR 바이패스</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>수신</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR 수신 바이패스</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>전송</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR 전송 바이패스</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>QSFP 모듈이 지원한다면 CRD 전송 및 수신 바이패스 컨트롤을 사용할 수 있을 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>엔지니어링</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR 코어 루프백</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>CFP XCVR 코어 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR 라인 루프백</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>CFP XCVR 라인 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>모듈 호스트 루프백</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>CFP 모듈 호스트 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>모듈 네트워크 루프백</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>CFP 모듈 네트워크 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>CFP 기어박스 시스템 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>CFP 기어박스 라인 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>기어박스 칩 ID</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>기어박스 칩 Rev</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>기어박스 유코드 버전</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>기어박스 유코드 CRC</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>기어박스 시스템 레인</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>기어박스 라인 레인</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>CFPn 호스트 레인</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>CFPn 네트워크 레인</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR 코어 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR 라인 루프백</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO 피크 DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO 피크 PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO 피크 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO 포크 DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO 포크 PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO 포크 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO 포크 값</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>레인 레이저 당 A013 컨트롤 등록 작동/정지.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C 피크 PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C 피크 DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C 피크 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C 포크 PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C 포크 DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C 포크 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C 포크 값</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>레인 레이저 당 0x56 컨트롤 등록 작동/정지.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+ 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>신호</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Tx 신호 클락</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>클락 소스</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>내부</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>복구됨</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>외부</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>외부 1.5M</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>외부 2M</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>외부 10M</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>원격 복구</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>타이밍 모듈</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>VC-12 소스</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>내부 – 주파수 오프셋 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>원격 클락</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>튜닝 가능한 장치</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>튜닝 모드</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>주파수</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>주파수 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>첫 번째 튜닝 가능한 주파수 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>마지막 튜닝 가능한 주파수 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>그리드 간격 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>스큐 경보</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>스큐 경보 한계 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>필요한 리싱크</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>리싱크 완료</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>통과한 RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>실패한 RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC는 일반적으로 SR4, PSM, CWDM4와 함께 사용합니다.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>RS-FEC 교정을 실행하려면 다음과 같이 하십시오. (CFP4에도 적용됩니다):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>QSFP28 어댑터를 CSAM에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>QSFP28 트랜시버를 어댑터에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>파이버 루프백 장치를 트랜시버에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>교정 클릭</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>교정</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>교정 중...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>리싱크</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>지금 트랜시버를 테스트 중인 장치(DUT)와 리싱크해야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>파이버 루프백 장치를 트랜시버에서 제거하기</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>테스트 중인 장치(DUT)로 연결하기 </translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>DUT 레이저를 ON합니다. </translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>CSAM의 신호 확인 LED가 초록색인지 확인합니다.</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>레인 리싱크 클릭</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>레인&#xA;리싱크</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>J-QuickCheck 결과</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>J-QuickCheck 테스트를 시작하기 위해 시작 버튼을 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>반이중 모드로 계속 작업함 .</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>업스트림 방향에서 트래픽 연결성을 확인하는 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>다운스트림 방향에서 트래픽 연결성을 확인하는 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>모든 서비스에 대한 트래픽 연결성이 성공적으로 확인되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>업스트림 방향에서 서비스에 대한 트래픽 연결성을 성공적으로 확인할 수 없었습니다 : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>다운스트림 방향에서 서비스에 대한 트래픽 연결성을 성공적으로 확인할 수 없었습니다 : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>업스트림 방향에서 서비스에 대한 트래픽 연결성을 성공적으로 확인할 수 없었습니다 : #1 과 서비스에 대한 다운스트림 방향 : #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>업스트림 방향에서 트래픽 연결성을 성공적으로 확인할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>다운스트림 방향에서 트래픽 연결성을 성공적으로 확인할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>업스트림이나 다운스트림 방향에 대해서 트래픽 연결성을 성공적으로 확인할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>연결성 확인</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>트래픽 연결성 :</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>QC 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>QC 정지</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>광학 셀프테스트</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>종료: 프로파일 저장</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>보고서</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>광학</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>광학 셀프테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>광학 신호가 사라졌습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>전원 레벨이 낮게 발견되었습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>전원 레벨이 높게 탐지되었습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>에러가 탐지되었습니다. 테스트가 중단되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>과도한 스큐가 탐지되었기 때문에 테스트가 실패하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>과도한 스큐가 탐지되었기 때문에 테스트가 실패하였습니다 . 테스트가 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>비트 에러가 탐지되었습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>BER이 설정된 한계를 초과하였기 때문에 테스트가 실패하였습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>테스트가 패턴 싱크 손실로 실패하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>삽입된 QSFP+ 를 읽을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>삽입된 CFP 를 읽을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>삽입된 CFP2 를 읽을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>삽입된 CFP4를 읽을 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>RS-FEC 수정할 수 있는 비트 에러가 감지되었습니다.  테스트가 중단되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>RS-FEC 수정할 수 없는 비트 에러가 감지되었습니다.  테스트가 중단되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>실패한 RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>입력 주파수 편차 에러 탐지</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>출력 주파수 편차 에러 발견</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>코드 위반이 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>정렬 마커 잠금 손실</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>무효 정렬 마커 탐지</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>BIP 8 AM 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>BIP 블록 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>스큐 발견</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>블록 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>소형 프레임이 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>원격 장애 발견</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>패턴 Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>패턴 Sync 손실로 BER 한계를 정확하게 측정할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>측정된 BER 은 선택한 BER 한계를 초과하였습니다</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS 가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>FAS 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>프레임 초과 발견</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>프레임 손실이 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>프레임 Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>논리 레인 마커 초과 발견</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>논리 레인 마커 에러 탐지</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>레인 정렬 손실이 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>레인 정렬 손실</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>MFAS 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>레인 정렬 초과 발견</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS 가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>리커버리 초과 발견</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>과잉 스큐가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>성공한 RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>에러</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>광학 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>테스트 종류</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>테스트가 100GE RSFEC를 사용합니다</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>광학 타입</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>전체 테스트 평가</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>신호 존재 테스트</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>광학 신호 레벨 테스트</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>과잉 스큐 테스트</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>비트 에러 테스트</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>일반 에러 테스트</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>BER 한계 테스트</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>광출력</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Rx 레벨 파장 #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Rx 레벨 파장 #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Rx 레벨 파장 #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Rx 레벨 파장 #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Rx 레벨 파장 #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Rx 레벨 파장 #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Rx 레벨 파장 #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Rx 레벨 파장 #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Rx 레벨 파장 #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Rx 레벨 파장 #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Rx 레벨 합계 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Tx 레벨 파장 #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Tx 레벨 파장 #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Tx 레벨 파장 #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Tx 레벨 파장 #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Tx 레벨 파장 #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Tx 레벨 파장 #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Tx 레벨 파장 #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Tx 레벨 파장 #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Tx 레벨 파장 #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Tx 레벨 파장 #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Tx 레벨 합계 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>테스트 하기를 원하는 커넥터의 Tx 및 Rx 터미널 사이에 짧고 깨끗한 패치 케이블을 연결하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>CFP&#xA; 광학 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>테스트 CFP2&#xA;광학/슬롯</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>테스트 취소</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>QSFP+&#xA; 광학 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>테스트 QSFP28&#xA;광학</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>설정:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>권장</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 분</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 분</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 시간</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 시간</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>사용자 지속 시간 ( 분 )</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>권장 지속 시간 (분)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>이 설정을 위한 권장 시간은 &lt;b>%1 &lt;/b> 분입니다 .</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>BER 한계 종류</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>포스트-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>프리-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>BER 한계</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>PPM 라인 오프셋 활성화</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>PPM 최대 오프셋 (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>에러 시 정지</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>결과 개요</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>광학/슬롯 타입</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>현재 PPM 오프셋</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>현재 BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>광출력 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Rx 레벨 파장 #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx 레벨 파장 #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx 레벨 파장 #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx 레벨 파장 #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx 레벨 파장 #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx 레벨 파장 #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx 레벨 파장 #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx 레벨 파장 #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx 레벨 파장 #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx 레벨 파장 #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Rx 레벨 합계</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx 레벨 파장 #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx 레벨 파장 #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx 레벨 파장 #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx 레벨 파장 #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx 레벨 파장 #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx 레벨 파장 #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx 레벨 파장 #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx 레벨 파장 #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx 레벨 파장 #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx 레벨 파장 #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Tx 레벨 합계</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>CFP 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>CFP2 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>CFP4 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>QSFP 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+ 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>QSFP28 인터페이스 세부사항</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>QSFP 없음 ,</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>QSFP 를 읽을 수 없습니다 – QSFP 를 다시 삽입하세요</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP 검사 합계 에러</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>필요한 QSFP 등록을 질의할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>QSFP 신원을 확인할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN 체크</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>기간 및 페이로드 BERT 테스트</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>GCC 투명성</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>테스트 선택 및 실행</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>고급 설정</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>OTN 체크 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>페이로드 BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>OTN 체크 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>측정 주파수</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>자동 협상 상태</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>RTD 설정</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>모든 레인</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN 체크:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** OTN 체크 테스트 시작 ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>페이로드 BERT 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>100,000 이상의 페이로드 BERT 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>페이로드 BERT BER 한계 초과됨</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 페이로드 BERT 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>페이로드 BERT 비트 에러 비율: #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>RTD 임계값 초과</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: 최소: #3, 최대: #4, 평균: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD 사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>페이로드 BERT 테스트 실행 중</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>RTD 테스트 실행 중</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>GCC 투명도 테스트 실행 중</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>GCC 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>GCC BER 임계 초과</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 GCC 비트 에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>100,000 GCC 비트 에러 이상 발견됨</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>GCC 비트 에러율: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** 루프백 확인 시작 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** 루프백 확인 건너뛰기 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** 루프백 확인 완료 ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>루프백이 감지됨</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>감지된 루프백 없음</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>채널 #1 사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>채널 #1 지금 사용 가능</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>패턴 Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>GCC 패턴 Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>테스트가 중단되었습니다: 비트 에러가 탐지되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>테스트 실패: BER이 구성된 임계를 초과했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>테스트 실패: RTD이 구성된 임계를 초과했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>테스트 중단: 감지된 루프백 없음.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>테스트가 중단되었습니다: 광학 신호 없음.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>테스트가 중단되었습니다: 프레임 손실.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>테스트가 중단되었습니다: 프레임 Sync 손실.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>테스트가 중단되었습니다: 패턴 Sync 손실.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>테스트가 중단되었습니다: GCC 패턴 Sync 손실.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>테스트가 중단되었습니다: 레인 정렬 손실.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>테스트가 중단되었습니다: 마커 lock 손실.</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>적어도 하나의 채널을 선택해야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>루프백이 확인되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>루프백이 감지되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>테스트 예정된 기간</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>테스트 실행 시간</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>OTN 체크는 트래픽 루프백을 실행해야 합니다. 이 루프백은 OTN 회선의 원격종단에서 필요합니다.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>OTN 체크 테스트</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>광학 오프셋, 신호 매핑</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>신호 매핑</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>신호 매핑 및 광학 선택</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>고급 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>신호 구조</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP 광학 RTD 오프셋 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP 광학 RTD 오프셋 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 광학 RTD 오프셋 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>CFP4 광학 RTD 오프셋 (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>기간 및 페이로드 BERT 테스트 구성</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>기간 및 페이로드 Bert 구성</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>페이로드 BERT 설정</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>신뢰수준 (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>회선속도, BER 임계값 및 신뢰수준을 근거로 추천 테스트 시간은 &lt;b>%1&lt;/b>입니다.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>패턴</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>BERT 패턴</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>에러 임계</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>성공/실패 보기</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>왕복 지연 설정</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>RTD 구성</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>포함</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>임계 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>측정 주파수 (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>GCC 구성</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>GCC 투명성 설정</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>GCC 채널</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>GCC BER 임계</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>BERT 패턴 2^23-1이 GCC에 사용됩니다.</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>루프백 체크</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>루프백 확인 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>루프백 확인을 시작하려면 “시작” 버튼을 누르십시오.</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>OTN 체크 테스트 넘김</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>루프백 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>루프백이 감지됨</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>루프백 확인 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>루프백 감지</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>루프백 감지</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>페이로드 BERT 결과</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>측정된 BER</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>비트 에러 카운트</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>판정</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>페이로드 BERT 구성</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>왕복 지연 결과</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>최소 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>최대 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>평균 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>주의: 평균 RTD가 임계를 초과할 때 실패 조건이 발생합니다.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>왕복 지연 구성</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>GCC 투명성 결과</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>GCC BERT 비트 에러율</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>GCC BERT 비트 에러</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>GCC 투명성 구성</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>프로토콜 분석</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>캡쳐</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>CDP 캡쳐 및 분석</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>캡쳐 및 분석</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>분석할 프로토콜 선택</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>분석 &#xA; 시작</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>분석 &#xA; 취소</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>분석</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>전문가 PTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>테스트를 수동으로 설정하세요 </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>임계</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>빠른 확인 실행</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>전문가 PTP 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>링크를 더는 사용할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>단방향 지연 시간 소스 동기화가 소실되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>PTP 슬레이브 세션이 갑자기 정지하였습니다.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>시간 소스 동기화가 존재하지 않습니다. 귀하의 시간 소스가 올바로 구성되어 있고 연결되어 있는지 검증하십시오.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>PTP 슬레이브 세션을 구성할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>GPS 리시버가 탐지되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>TOS 종류</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Rx 타임아웃 고지</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>고지</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>초당 128</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>초당 64</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>초당 32</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>초당 16</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>초당 8</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>초당 4</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>초당 2</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>초당 1</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Sync</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>지연 요청</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>임대 기간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>활성</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>시간 에러 최대 한계 활성</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>시간 에러 최대 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>시간 에러 최대 한계치 (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>테스트 지속 시간 ( 분 )</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>빠른 확인</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>마스터 IP</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>PTP 도메인</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>시작&#xA;확인</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>세션&#xA;시작 중</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>설정된&#xA;세션</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>시간 에러 최대 한계치 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>시간 에러 최대 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>시간 에러, Cur (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>시간 에러, Cur (us) vs. 시간</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>지속 시간 (분)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP 체크</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>PTP 체크 테스트</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>종료: PTP 확인</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>다른 테스트 시작</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>PTP 확인 나가기</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>향상된 RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>유효하지 않은 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>고급 IP 설정 - 로컬</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>고급 RTD 레이턴시 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>고급 버스트 (CBS) 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck 실행</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>J-QuickCheck 설정</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>지터</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>확장 로드</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>RFC 2544 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>로컬 네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>원격 네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>로컬 자동 협상 상태</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>이중</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>플로우 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX 사용 가능</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX 사용 가능</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>정지 사용 가능 </translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>원격 자동 협상 상태</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>절반</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>전체</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx 와 Tx 모두</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx 만</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx 만</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>RTD 프레임 속도</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>초당 1 프레임</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>초당 10 프레임</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>버스트 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>인정 버스트 크기</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>CBS 폴리싱 (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>버스트 헌트</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>다운스트림 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>다운스트림 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>최소</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>최대</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>업스트림 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>업스트림 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>고급 CBS 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>고급 CBS 폴리싱 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>고급 버스트 헌트 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>오차</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>확장 로드 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>처리량 스케일링 (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 는 다음의 유효하지 않은 구성 설정을 가지고 있습니다 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>트래픽 연결성이 성공적으로 확인되었습니다 . 로드 테스트를 실행하고 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>라인 속도에서 실행하려면 " 시작" 을 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>구성된 RFC 2544 대역폭을 사용하여 실행하려면 " 시작" 을 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>측정된 처리량은 RFC 2544 테스트용으로 사용되지 않을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>측정된 처리량은 RFC 2544 테스트용으로 사용될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>로드 테스트 프레임 크기 : %1 바이트 .</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>로드 테스트 패킷 크기 : %1 바이트 .</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>업스트림 로드 테스트 프레임 크기 : %1 바이트 .</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>다운스트림 로드 테스트 프레임 크기 : %1 바이트 .</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>측정된 처리량 :</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>설정된 RFC 2544 최대 대역폭을 사용해서 테스트</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>RFC 2544 최대 대역폭으로 측정된 처리량 측정치 사용</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>로드 테스트 프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>로드 테스트 패킷 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>업스트림 로드 테스트 프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>다운스트림 로드 테스트 프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>RFC 2544 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>RFC 2544 테스트 통과</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>하드웨어 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>업스트림 지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>업스트림 지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>다운스트림 지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>다운스트림 지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>최대 평균 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>최대 평균 지터</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>최대 평균 &#xA; 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>업스트림 CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>다운스트림 CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>업스트림 버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>다운스트림 버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>업스트림 CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>다운스트림 CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>버스트 (CBS 폴리싱 ) 테스트</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Cfg 버스트 &#xA; 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Tx 버스트 &#xA; 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>평균 Rx&#xA; 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>전송된 &#xA; 프레임</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>수신된 &#xA; 프레임</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>손실된 &#xA; 레임</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>버스트 크기 &#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>레이턴시 &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>지터 &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>구성된 버스트 &#xA; 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Tx 버스트 &#xA; 폴리싱 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>예상 &#xA;CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>업스트림 시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>업스트림 시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>다운스트림 시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>다운스트림 시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>회복 시간 (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>오버로드 속도 &#xA;(L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>오버로드 속도 &#xA;(L2, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>오버로드 속도 &#xA;(L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>오버로드 속도 &#xA;(L2, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>오버로드율 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>리커버리 속도 &#xA;(L1, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>리커버리 속도 &#xA;(L2, Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>리커버리 속도 &#xA;(L1, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>리커버리 속도 &#xA;(L2, kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>복구율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>평균 복구 &#xA; 시간 (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed 테스트</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>모드</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>컨트롤</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>TrueSpeed 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>셰이핑</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>단계 설정</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>단계 선택</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>경로 MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>워크 더 윈도우</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 처리량</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>고급 TCP</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>연결 설정</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed Ctl ( 고급 )</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Bc 장치 선택</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>워크 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>TrueSpeed 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>TCP 호스트 가 연결을 설정하는데 실패하였습니다 . 현재 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>트러블슈트 모드를 선택하여 원격 장치로부터 연결이 차단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>유효하지 않는 테스트 선택 : TrueSpeed 를 실행하려면 최소 하나의 테스트를 선택해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>측정된 MTU 는 계속하기에 너무 작습니다 . 테스트가 취소되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>당신의 파일이 너무 빨리 전송되었습니다 ! TCP 처리량에 대한 파일 크기를 선택하여 테스트가 적어도 5 초 동안 실행되도록 하시기 바랍니다 . 테스트는 적어도 10 초 동안 실행하는 것이 좋습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>최대 재전송 시도에 도달했습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>TCP 호스트 가 에러를 만났습니다 . 현재 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>TrueSpeed 테스트 시작.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>MTU 크기에 제로 - 인 중.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>#2 바이트 MSS 로 #1 바이트 MTU 테스트 중 .</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>패스 MTU 는 #1 바이트로 결정되었습니다 . 이것은 #2 바이트의 MSS 와 동일합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>RTT 테스트 실행 중 . 이것은 #1 초가 걸릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>왕복시간 (RTT) 가 #1 msec 로 측정되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>업스트림 워크 더 윈도우 테스트 실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>다운스트림 워크 더 윈도우 테스트 실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>TCP 트래픽의 #1 바이트 전송 중.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>로컬 출현 트래픽: 셰이핑되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>원격 출현 트래픽: 셰이핑되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>로컬 출현 트래픽: 셰이핑됨</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>원격 출현 트래픽: 셰이핑됨</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>지속 시간 (초): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>연결: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>윈도우 크기 (kB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>업스트림 TCP 처리량 테스트 실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>다운스트림 TCP 처리량 테스트 실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>고급 TCP 테스트 실행 중. 이것은 #1초가 걸릴 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>로컬 IP 종류</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>로컬 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>로컬 디폴트 게이트웨이</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>로컬 서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>로컬 캡슐화</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>원격 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>모드 선택</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>어떤 종류의 테스트를 실행하고 있습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>나는 새로운 회로를  설치하거나   올리고  있습니다 .*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>나는 기존의 회로를  트러블슈팅하고  있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>* 원격 MTS/T-BERD 테스트 도구가 필요합니다</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>처리량을 어떻게 구성하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>다운스트림과 업스트림 처리량이  같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>다운스트림과 업스트림 처리량이  다릅니다 .</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Truespeed&#xA; 구성을 로딩할 때 SAMComplete CIR 을 일치시키기 위해 보틀넥 대역폭 설정 .</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Truespeed&#xA; 구성을 로딩할 때 RFC 2544 최대 대역폭을 일치시키기 위해 보틀넥 대역폭 설정 .</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Iperf 서버</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>T-BERM/MTS 테스트 도구</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>IP 종류</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>원격 설정</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>TCP 호스트 서버 설정</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>이 단계는 모든 그 다음 TrueSpeed 단계를 위해 글로벌 설정을 구성할 것입니다 . 이것은 CIR( 인정 정보 속도 ) 과 처리량 테스트를 통과시키기 위해 필요한 CIR 의 퍼센트인 TCP 통과 % 를 포함합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>워크 더 윈도우 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>총 테스트 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>자동적으로 MTU 크기를 찾습니다 .</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>MTU 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>로컬 VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>우선순위</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>로컬 우선순위</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>로컬 TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>로컬 DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>다운스트림 CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>업스트림 CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>원격 &#xA;TCP 호스트</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>원격 VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>원격 우선순위</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>원격 TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>원격 DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>고급 TrueSpeed 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Bc 장치 선택</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>트래픽 쉐이핑</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Bc 장치</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>프로파일 셰이핑</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>로컬 및 원격 모두 출현 트래픽 셰이핑됨</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>로컬 출현 트래픽만 셰이핑됨</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>원격 출현 트래픽만 쉐이핑 됨</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>로컬 또는 원격 둘 다 출현 트래픽 셰이핑되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>트래픽 셰이핑 (워크 더 윈도우와 처리량 테스트 전용)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>처음은 셰이핑하지 않고, 그 다음 셰이핑하고 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) 로컬</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) 로컬</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) 로컬</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) 로컬</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) 로컬</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) 원격</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (원격)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) 원격</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) 원격</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) 원격</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) 원격</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>TCP 트래픽을 셰이핑하시겠습니까?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>셰이핑되지 않은 트래픽</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>셰이핑된 트래픽</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>셰이핑되지 않은 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>THEN</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>셰이핑된 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>로컬에서 셰이핑된 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>로컬과 원격에서 셰이핑과 함께 실행</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>셰이핑이 전혀 없는 경우 실행</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>로컬에서는 셰이핑과 함께, 원격에서는 셰이핑 없이 실행</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>로컬에서 셰이핑과 함께 실행</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>로컬에서는 셰이핑이 없이, 원격에서는 셰이핑과 함께 실행</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>로컬 트래픽 셰이핑</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>원격 장치가&#xA;연결되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>원격 트래픽 셰이핑</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>추가 셰이핑 옵션 보기</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>TrueSpeed 컨트롤 ( 고급 )</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>포트로 연결</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>TCP 통과 %</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>MTU 상위 제한 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>다중 연결 사용</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>포화 윈도우 활성</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>부스트 윈도우 (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>부스트 연결 (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>단계 설정</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>TrueSpeed 단계</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>테스트를 실행하려면 최소 하나의 단계가 있어야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>이 단계는 end-end 네트워크 경로의 최대 전송 단위를 자동적으로 결정하기 위해 RFC4821 에서 정의된 절차를 사용합니다 . TCP 클라이언트 테스트 세트는 다양한 패킷 크기로 TCP 세그먼트를 전송하려고 시도하고 ( 전통적인 경로 MTU 발견에서 필요로 하는 ) ICMP 없이 MTU 를 결정할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>이 단계는 저강도 TCP 전송을 실행하고  이후 테스트 단계 결과를 위한 기준으로 사용될 기준선 왕복 시간 (RTT) 을 조사하여 보고할 것입니다 .  기준선 RTT 는 네트워크 폭주로 인해 발생하는 부가적인 지연을 제외한 네트워크의  고유한 지연입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>지속 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>�� 단계는 TCP " 윈도우 스캔</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>윈도우 크기</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>윈도우 크기 1 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Conn.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>윈도우 1 연결</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>윈도우 크기 2 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>윈도우 2 연결</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>윈도우 크기 3 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>윈도우 3 연결</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>윈도우 크기 4 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>윈도우 4 연결</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>최대 Seg 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>경로 MTU 단계에서 확인된 &#xA;MSS 를 사용합니다</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>최대 세그먼트 크기 ( 바이트 )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>&lt;b>%1&lt;/b> Mbps 의 링크 대역폭과 &lt;b>%2&lt;/b> ms 의 RTT 에 따라 , 이상적인 TCP 윈도우는 &lt;b>%3&lt;/b> 바이트가 될 것입니다 . &lt;b>%4&lt;/b> 연결과 각 연결의 TCP 윈도우 크기가 &lt;b>%5&lt;/b> 바이트인 경우 , 이상적인 전송 처리량은 &lt;b>%6&lt;/b>kbps 일 것이며 , 각 연결을 통해 전송된 &lt;b>%7&lt;/b> MB 파일은 &lt;b>%8&lt;/b> 를 취해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>&lt;b>%1&lt;/b> Mbps 의 링크 대역폭과 &lt;b>%2&lt;/b> ms 의 RTT 에 따라 , 이상적인 TCP 윈도우는 &lt;b>%3&lt;/b> 바이트가 될 것입니다 . &lt;font color="red">&lt;b>%4&lt;/b> 연결과 각각의 연결에 대한 &lt;b>%5&lt;/b> 바이트 TCP 윈도우 크기로는 , 링크의 용량이 초과됩니다 . 실제 결과는 패킷 손실과 추가 지연으로 인해 예상 결과보다 좋지 않을 수 있습니다 . 링크 용량 내에서 테스트를 실행하기 위해 연결 개수나 TCP 윈도우 크기를 줄이시기 바랍니다 .&lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>윈도우 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>연결 당 파일 크기 (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>자동적으로 30 초 전 시간 동안 파일 크기를 찾습니다 .</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>연결의 수</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>RTT 단계에서 발견한 &#xA; RTT 를 사용합니다 (%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>경로 MTU 단계에서 &#xA; 발견한 MSS 를 사용합니다 . (%1 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>이 단계는 링크가 공평하게 대역폭을 분배하는지 ( 트래픽 쉐이핑 )  또는 공평하지 않게 대역폭을 분배하는지 ( 트래픽 폴리싱 ) 테스트하기 위해 다중 TCP 연결 전송을 실행합니다 .</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>이 단계는 링크가 공평하게 대역폭을 분배하는지 ( 트래픽 쉐이핑 )  또는 공평하지 않게 대역폭을 분배하는지 ( 트래픽 폴리싱 )  테스트하기 위해 다중 TCP 연결 전송을 실행합니다 . 윈도우 크기와 연결의 수가 자동적으로  계산되도록 하기 위해 , RTT 단곌르 실행하세요 . </translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>윈도우 크기 (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>RTT 단계가 실행되면 &#xA; 자동적으로 계산됩니다</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 바이트 </translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>TrueSpeed 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>TrueSpeed 테스트 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>최대 전송 단위 (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>최대 세그먼트 크기 (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>이 단계는 이 링크 (end-end) 를 위한 최대 전송 단위 (MTU) 를 %1 바이트로 결정했습니다 . 레이어 3/4 오버헤드를 뺀 이 값은 다음 단계를 위한 TCP 최대 세그먼트 크기 (MSS) 로 사용될 것입니다 . 이 경우 , MSS 는 %2 바이트가 될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>RTT 요약 결과</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>평균 RTD (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>최소 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>최대 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>왕복시간 (RTT) 가 %1 msec 로 측정되었습니다 . 최소 RTT 는 네트워크의 고유 레이턴시를 가장 가깝게 나타내기 때문에 이 최소 RTT 가 사용됩니다 . 다음 단계는 이것을 예상 TCP 전송 성능을 위한 기준으로 사용합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>왕복시간(RTT)이 %1 밀리 초로 측정되었습니다. 평균 (베이스) RTT는 네트워크의 고유 레이턴시를 가장 가깝게 나타내는 것으로 사용됩니다. 다음 단계는 이것을 예상 TCP 전송 성능을 위한 기준으로 사용합니다.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>업스트림 워크 더 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>다운스트림 워크 더 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>업스트림 윈도우 1 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>다운스트림 윈도우 1 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>업스트림 윈도우 2 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>다운스트림 윈도우 2 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>업스트림 윈도우 3 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>다운스트림 윈도우 3 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>업스트림 윈도우 4 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>다운스트림 윈도우 4 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>업스트림 윈도우 5 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>다운스트림 윈도우 5 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>업스트림 윈도우 1 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>다운스트림 윈도우 1 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>업스트림 윈도우 2 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>다운스트림 윈도우 2 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>업스트림 윈도우 3 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>다운스트림 윈도우 3 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>업스트림 윈도우 4 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>다운스트림 윈도우 4 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>업스트림 윈도우 5 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>다운스트림 윈도우 5 연결 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>업스트림 윈도우 1 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>업스트림 윈도우 1 실제 셰이핑되지 않음 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>업스트림 윈도우 1 실제 셰이핑됨 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>업스트림 윈도우 1 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>업스트림 윈도우 2 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>업스트림 윈도우 2 실제 셰이핑되지 않음 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>업스트림 윈도우 2 실제 셰이핑됨 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>업스트림 윈도우 2 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>업스트림 윈도우 3 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>업스트림 윈도우 3 실제 셰이핑되지 않음 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>업스트림 윈도우 3 실제 셰이핑됨 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>업스트림 윈도우 3 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>업스트림 윈도우 4 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>업스트림 윈도우 4 실제 셰이핑되지 않음 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>업스트림 윈도우 4 실제 셰이핑됨 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>업스트림 윈도우 4 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>업스트림 윈도우 5 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>업스트림 윈도우 5 실제 셰이핑되지 않음 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>업스트림 윈도우 5 실제 셰이핑됨 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>업스트림 윈도우 5 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>다운스트림 윈도우 1 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>셰이핑된 다운스트림 윈도우 1 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>다운스트림 윈도우 1 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>다운스트림 윈도우 2 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>셰이핑된 다운스트림 윈도우 2 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>다운스트림 윈도우 2 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>다운스트림 윈도우 3 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>셰이핑된 다운스트림 윈도우 3 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>다운스트림 윈도우 3 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>다운스트림 윈도우 4 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>셰이핑된 다운스트림 윈도우 4 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>다운스트림 윈도우 4 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>다운스트림 윈도우 5 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>셰이핑된 다운스트림 윈도우 5 실제 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>다운스트림 윈도우 5 예상 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbps, 평균.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>윈도우 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>실제</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>셰이핑되지 않은 실제</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>셰이핑된 실제</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>이상적</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>윈도우 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>윈도우 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>윈도우 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>윈도우 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>TCP 워크 더 윈도우 단계의 결과는 테스트한 각각의 윈도우 크기 / 연결에 대한 실제 대 이상적 처리량을 보여줍니다 . 실제가 이상보다 작은 경우는 손실이나 폭주로 인해 발생할 수 있습니다 . 실제가 이상보다 큰 경우 , 기준선으로 사용된 RTT 가 너무 높습니다 . TCP 처리량 단계로 TCP 이동을 더 깊이 분석할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>업스트림 TCP 처리량 실제 대 이상</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>업스트림 TCP 처리량 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>업스트림 TCP 처리량 재전송 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>업스트림 TCP 처리량 RTT 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>다운스트림 TCP 처리량 실제 대 이상</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>다운스트림 TCP 처리량 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>다운스트림 TCP 처리량 재전송 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>다운스트림 TCP 처리량 RTT 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>업스트림 이상적 전송 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>업스트림 실제 전송 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>업스트림 실제 L4 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>업스트림 실제 L4 셰이핑되지 않은 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>업스트림 실제 L4 셰이핑된 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>업스트림 이상적 L4 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>업스트림 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>업스트림 셰이핑되지 않은 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>업스트림 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>업스트림 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>업스트림 셰이핑되지 않은 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>업스트림 셰이핑된 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>다운스트림 실제 L4 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>다운스트림 실제 L4 셰이핑되지 않은 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>다운스트림 셰이핑된 실제 L4 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>다운스트림 이상적 L4 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>다운스트림 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>다운스트림 셰이핑되지 않은 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>다운스트림 셰이핑된 TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>다운스트림 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>다운스트림 셰이핑되지 않은 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>다운스트림 셰이핑된 버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>TCP 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>실제 대 이상</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>업스트림 테스트 결과는 다음을 포함할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>부가적인 제안이 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>이 테스트는 충분히 오랜 시간 동안 실행되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>네트워크 버퍼 / 셰이퍼는 튜닝이 필요합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>폴리서가 TCP 버스트 때문에 패킷을 드롭하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>처리량은 좋았지만 재전송이 탐지되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>네트워크가 혼잡하거나 트래픽이 셰이핑 되고 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>귀하의 CIR 가 잘못 구성된 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>귀하의 파일이 너무 빨리 전송되었습니다 !&#xA; 그 파일에 대한 예상 전송 시간을 검토하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>각각 %2 바이트 의 윈도우 크기인 %1 연결이 각 연결 ( 총 %4 MB) 를 통해 %3 MB 파일을 전송하기 위해 사용되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> 연결 이상으로 전송된 TCP 윈도우 &lt;b>%2&lt;/b> 바이트</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>실제 측정된 L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>이상적인 L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>전송 매트릭스</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>TCP 효율 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>버퍼 지연 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>다운스트림 테스트 결과는 다음을 포함할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>업스트림 실제 대 이상</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>TCP 효율 (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>버퍼 지연 (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>셰이핑되지 않은 테스트는 다음을 포함할 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>다운스트림 실제 대 이상</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>처리량 그래프</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Retrans Frm</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>기준선 RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>재전송이나 폭주 네트워크 영향 (RTT 기준선 초과 ) 으로 인해 가능한 TCP 성능 문제들 상호 관련시키기 위해 이 그래프들을 사용하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>재전송 그래프</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>RTT 그래프</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>연결 당 이상적 처리량</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>트래픽 셰이핑 된 링크의 경우 각 연결은 대역폭 중 비교적 고른 부분을 받아야 합니다 . 트래픽 폴리싱 된 (Traffic Policed) 링크의 경우 , 각 연결은 재전송이 폴리싱 때문에 발생하면서 바운스할 것입니다 . %1 연결 각각은 약 %2 Mbps 의 대역폭을 소비해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>랜덤</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF 테스트</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>고급 서버 연결</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>고급 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>보고서를 로컬로 생성</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>테스트 확인</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>종료: 상세한 결과 보기</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>종료: 보고서를 로컬로 생성</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>다른 보고서를 로컬로 생성</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS 테스트</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT 테스트</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>업스트림 테스트</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>다운스트림 테스트</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>다음의 구성이 필요합니다.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>다음의 구성은 범위를 초과하였습니다.  #1과 #2 사이의 값을 입력하세요.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>다음의 구성은 잘못된 값을 가지고 있습니다.  새로 선택하세요.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>클라이언트-투-서버 테스트 허가 없음.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>서버-투-서버 테스트 허가 없음.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>활성화된 테스트가 너무 많습니다. (최대 #1)</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>로컬 서버가 보류되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>원격 서버가 보류되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>테스트 인스턴스가 이미 존재합니다.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>테스트 데이터베이스 읽기 에러.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>테스트 데이터베이스에서 테스트가 발견되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>테스트가 만기되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>테스트 종류가 지원되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>테스트 서버를 선택할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>테스트 서버가 보류되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>테스트 서버 잘못된 요청 모드.</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>원격 서버에서는 테스트가 허용되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>원격 서버에서 HTTP 요청이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>원격 서버는 사용할 수 있는 충분한 리소스를 가지고 있지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>테스트 클라이언트를 선택할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>테스트 포트가 지원되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>시간 당 너무 많은 테스트를 시도하는 중.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>테스트 인스턴스 생성에 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>테스트 워크플로우 생성에 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>워크플로우 HTTP 요청이 좋지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>워크플로우 HTTP 요청이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>원격 테스트가 허용되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>리소스 관리자에서 에러가 발생했습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>테스트 인스턴스가 발견되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>테스트 상태에 충돌이 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>테스트 상태가 유효하지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>테스트 생성이 실패하였습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>테스트 업데이트가 실패하였습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>테스트가 성공적으로 생성되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>테스트가 성공적으로 업데이트되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>HTTP 요청이 실패했습니다: #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>VNF 서버 버전(#2)은 장치 버전(#1)과 호환하지 않을 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>#1에서 서버를 위한 사용자명과 인증키를 입력하십시오.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>테스트가 초기화하는 데 실패했습니다: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>테스트가 중단되었습니다: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>서버 연결</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>서버에 연결하기 위해 필요한 정보가 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>네트워크 통신을 실행하기 위한 링크가 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>네트워크 통신을 위한 유효한 소스 IP 주소가 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>지정한 IP 주소에서 핑이 실행되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>지정한 IP 주소에서 장치를 탐지할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>서버가 아직 지정한 IP 주소를 확인하지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>지정한 IP 주소에서 서버를 확인할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>서버가 확인되었지만 인증되지는 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>서버 인증이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>인증 실패, 확인 시도 중.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>인증되거나 확인되지 않음, 핑 시도 중.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>서버가 인증되었으며 테스트를 위해 사용할 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>서버가 연결되었고 테스트가 실행 중입니다.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>확인 중</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>확인</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>TCP 프록시 버전</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>테스트 컨트롤러 버전</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>링크 액티브:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>서버 ID:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>서버 상태:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>고급 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>고급 연결 설정</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>인증 키</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>사용자명과 키 기억</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>로컬 단위</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>서버</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>윈도우 워크 지속 시간 (초)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>자동</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>자동 지속 시간</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>테스트 설정 (고급)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>고급 테스트 파라미터</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>TCP 포트</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>자동 TCP 포트</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>윈도우 워크 수</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>연결 카운트</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>자동 연결 카운트</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>포화 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>포화 윈도우 실행</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>부스트 연결 (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>서버 보고서 정보</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>테스트 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>기술자 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>고객 이름*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>회사</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>이메일*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>전화</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>설명</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>테스트 ID 보기</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>TrueSpeed 테스트 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>서버가 연결되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>평균 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>테스팅 리소스가 준비되기를 기다리고 있습니다.  귀하는 대기 리스트의 #%1입니다.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>윈도우</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>포화&#xA;윈도우</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>윈도우 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>연결</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>업스트림 분석:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>보고 내용 없음</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>처리량 너무 느림</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>일관성이 없는 RTT</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>TCP 효율이 낮습니다.</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>버퍼 지연이 높습니다</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>처리량 CIR의 80% 미만</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>1400 미만의 MTU</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>다운스트림 분석:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>인증 코드</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>인증 생성일</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>만기일</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>시간 수정</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>테스트 정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>마지막으로 수정됨</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>이메일</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>업스트림 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>실제 대 목표</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>업스트림 실제 대 목표</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>업스트림 요약</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>피크 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (바이트)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>왕복 시간 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>업스트림 요약 결과 (최대 처리량 윈도우)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>연결 당 윈도우 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>전체 윈도우 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>목표 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>평균 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>목표</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>윈도우 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>윈도우 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>윈도우 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>윈도우 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>윈도우 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>윈도우 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>최대 처리량 윈도우:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 kB 윈도우: %2 conn. x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>업스트림 처리량 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>업스트림 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>업스트림 윈도우 1 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>연결 당 윈도우 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>실제 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>목표 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>총 재전송</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>업스트림 윈도우 2 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>업스트림 윈도우 3 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>업스트림 윈도우 4 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>업스트림 윈도우 5 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>업스트림 윈도우 6 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>업스트림 윈도우 7 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>업스트림 윈도우 8 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>업스트림 윈도우 9 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>업스트림 윈도우 10 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>업스트림 포화 윈도우 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>평균 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>다운스트림 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>다운스트림 실제 대 목표</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>다운스트림 요약</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>다운스트림 요약 결과 (최대 처리량 윈도우)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>다운스트림 처리량 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>다운스트림 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>다운스트림 윈도우 1 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>다운스트림 윈도우 2 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>다운스트림 윈도우 3 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>다운스트림 윈도우 4 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>다운스트림 윈도우 5 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>다운스트림 윈도우 6 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>다운스트림 윈도우 7 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>다운스트림 윈도우 8 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>다운스트림 윈도우 9 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>다운스트림 윈도우 10 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>다운스트림 포화 윈도우 처리량 결과</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation>#윈도우 워크</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>과포화 윈도우 (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>과포화 연결 (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>VLAN 스캔</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 VLAN 스캔 테스트 #2 시작</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>#2 초를 위한 VLAN ID #1 테스팅</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: 통과</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: 실패</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>액티브 루프가 성공적이지 않습니다. VLAN ID #1에 대한 테스트가 실패하였습니다.</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>테스트된 총 VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>통과한 ID의 수</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>실패한 ID의 수</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>ID 당 지속 시간 (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>범위의 수</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>선택한 범위</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>VLAN ID 최소</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID 최대</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>측정 시작</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>총 (%)</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>통과한 ID</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>실패한 ID</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>고급 VLAN 스캔 설정</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>대역폭 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>통과 기준</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>손실된 프레임 없음</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>일부 프레임 전송됨</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>TrueSAM 설정 ...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>TrueSAM 실행 ...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>예상 실행 시간</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>실패 시 정지</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>TrueSAM 보고서 보기 ...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>실패 !</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>사용자에 의해 정지됨</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>신호 손실 (Signal Loss)</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>링크 손실 .</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>원격 테스트 세트와의 통신이 끊어졌습니다 . 통신 채널을 재설정하고 다시 시도하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>로컬 테스트 세트를 구성하는 동안 문제가 발견되었으며 , 원격 테스트 세트를 사용한 통신이 손실되었습니다 . 통신 채널을 재설정하고 다시 시도하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>원격 테스트 세트를 구성하는 동안 문제가 발견되었으며, 이 테스트 세트를 사용한 통신이 손실되었습니다. 통신 채널을 재설정하고 다시 시도하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>로컬 테스트 세트를 구성하는 동안 문제가 발견되었으므로 TrueSAM 이 지금 종료할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>로컬 테스트 세트를 구성하려고 시도하는 동안 문제가 발견되었습니다 . 테스팅이 강제적으로 종료되었으며 원격 테스트 세트를 사용한 통신 채널이 손실되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>원격 테스트 세트를 구성하려고 시도하는 동안 문제가 발견되었습니다 . 테스팅이 강제적으로 종료되었으며 원격 테스트 세트를 사용한 통신 채널이 손실되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>테스트하는 동안 간섭을 방지하기 위해 스크린 세이버가 비활성화되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>로컬 테스트 세트에서 문제가 발견되었습니다 . TrueSAM 이 지금 종료할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>DHCP 파라미터 요청 중 .</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>DHCP 파라미터를 획득하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>이더넷 인터페이스를 초기화하는 중 . 기다려 주세요 .</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>북미</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>북미와 한국</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>북미 PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>인도</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>외부 시간 소스로부터 시간 신호가 상실되었으므로 1PPS sync 가 꺼짐으로 나타날 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>외부 시간 소스의 시간 신호와의 동기화를 시작합니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>외부 시간 소스의 날짜 시간 신호와의 동기화에 성공하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>루프 &#xA; 다운</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>커버 페이지</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>실행할 테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>End-to-end 트래픽 연결성 테스트</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>향상된 RFC 2544 / SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>이더넷 벤치마킹 테스트 수위트</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM Complete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Y.1564 이더넷 서비스 설정 및 성능 테스팅</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>컨트롤 플레인 프레임(CDP, STP 등)에 대한 레이어 2 투명도 테스트.</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>RFC 6349 TCP 처리량 및 성능 테스트</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>L3- 소스 종류</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>통신 채널용 설정 ( 서비스 1 사용 )</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>통신 채널 설정</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>원격 Viavi 테스트 장치에서 스트림 1 과 일치하도록 서비스 1 을 구성해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>로컬 상태</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>ToD Sync</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>1PPS Sync</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>채널에&#xA;연결</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>물리 레이어</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>휴지 길이 ( 양자 )</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>휴지 길이 ( 시간 – ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>테스트 &#xA; 실행</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>테스트 &#xA; 시작 중</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>테스트 &#xA; 정지 중</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>테스트 실행 예상 시간</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>총</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>실패시 정지</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>선택된 테스트에 대한 통과 / 실패 기준</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>업스트림 및 다운스트림</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>이것은 통과 / 실패의 개념이 없으므로 이 설정은 적용되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>다음 처리량에 달한 경우 통과 :</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>어떤 처리량 세트도 통과 / 실패를 보고하지 않을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>로컬:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>원격:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>로컬 및 원격 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>처리량 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>처리량 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>프레임 손실 오차 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>레이턴시 (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>다음 SLA 파라미터가 만족된 경우 통과 :</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>업스트림:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>다운스트림:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>CIR 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>CIR 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>EIR 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>EIR 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>EIR 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M- 허용치 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M- 허용치 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M- 허용치 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>프레임 지연 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>지연 변이 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>통과 / 실패는 내부적으로 결정될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>측정된 TCP 처리량이 다음 처리량에 달한 경우 통과 :</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>예상 처리량 퍼센트</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>활성화되지 않은 TCP 처리량 세트는 통과 / 실패를 보고하지 않을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>테스트 정지</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>이것은 현재 실행 중인 테스트와 이후 테스트 진행을 중지할 것입니다 . 확실히 중지하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 총 최대 로드가 0 이거나 #2 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 업스트림 방향 총 최대 로드가 0 이거나 #2 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 다운스트림 방향 총 최대 로드가 0 이거나 #2 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 총 최대 로드가 0 이거나 각 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 업스트림 방향 총 최대 로드가 0 이거나 각 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 다운스트림 방향 총 최대 로드가 0 이거나 각 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 업스트림 방향에서 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 다운스트림 방향에서 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 업스트림 방향에서 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최대 로드가 다운스트림 방향에서 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA;CIR, EIR 및 폴리싱은 모두 0 이 될 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA;CIR, EIR 및 폴리싱은 업스트림 방향에서 모두 0 이 될 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA;CIR, EIR 및 폴리싱은 다운스트림 방향에서 모두 0 이 될 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #3 에 대해 #2 Mbps CIR 설정을 사용하여 가장 작은 단계 로드 (CIR 의 #1%) 를 얻을 수 없습니다 . 서비스 #3 을 위해 현재 CIR 을 사용하는 가장 작은 단계 값은 #4% 입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 총 CIR (L1 Mbps) 가 0 이거나 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 업스트림 방향에 대한 총 CIR(L1 Mbps) 가 0 이거나 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 다운스트림 방향에 대한 총 CIR(L1 Mbps) 가 0 이거나 #1 L1 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 총 CIR (L2 Mbps) 가 0 이거나 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 업스트림 방향에 대한 총 CIR(L2 Mbps) 가 0 이거나 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 다운스트림 방향에 대한 총 CIR(L2 Mbps) 가 0 이거나 #1 L2 Mbps 라인 속도를 초과합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 설정 테스트나 서비스 성능 테스트가 선택되어야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 최소 하나의 서비스를 선택해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>유효하지 않은 구성:&#xA;&#xA;서비스 성능 테스트를 실행하길 원하는 경우 선택된 서비스 CIR (또는 CIR이 0인 서비스의 경우 EIR)의 총 합계가 라인 율을 초과할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 처리량 측정 (RFC 2544) 최대값은 선택된 서비스를 위한 CIR 값의 합보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 업스트림 처리량 측정 (RFC 2544) 최대값이 선택된 서비스를 위한 CIR 값의 합보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 다운스트림 처리량 측정 (RFC 2544) 최대값이 선택된 서비스를 위한 CIR 값의 합보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 처리량 측정 (RFC 2544) 최대값은 CIR 값보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 업스트림 처리량 측정 (RFC 2544) 최대값이 CIR 값보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 성능 테스트를 실행할 때는 지정된 다운스트림 처리량 측정 (RFC 2544) 최대값이 CIR 값보다 작을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>실패시 정지가 선택되었고 하나 이상의 KPI 가 서비스 #1 에 대한 SLA 를 만족시키기 않기 때문에 SAM Complete 이 정지되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>트리픽이 시작된 후 10 초 내에 서비스 #1 로 돌아온 데이터가 없었기 때문에 SAMComplete 이 정지되었습니다 . 원단이 트래픽을 더 이상 루프백 하지 않는 것일 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>트리픽이 시작된 후 10 초 내에 서비스 #1 로 돌아온 데이터가 없었기 때문에 SAMComplete 이 정지되었습니다 . 원단이 트래픽을 더 이상 전송하지 않는 것일 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>트래픽이 시작된 후 10초 내에 돌아온 데이터가 없었기 때문에 SAMComplete이 정지되었습니다. 원단이 트래픽을 더 이상 루프백 하지 않는 것일 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>트리픽이 시작된 후 10 초 내에 돌아온 데이터가 없었기 때문에 SAMComplete 이 정지되었습니다 . 원단이 트래픽을 더 이상 전송하지 않는 것일 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>로컬 및 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 스트림 애플리케이션이며 , IP 주소 #1 에 있는 원격 애플리케이션은 트래픽 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>로컬 애플리케이션과 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 스트림 애플리케이션이며 , IP 주소 #1 에서 원격 애플리케이션은 TCP WireSpeed 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>로컬 애플리케이션과 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 레이어 3 애플리케이션이며 , IP 주소 #1 에서 원격 애플리케이션은 레이어 2 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>로컬 및 원격 애플리케이션이 호환되지 않습니다 . 로컬 애플리케이션은 레이어 2 애플리케이션이며 , IP 주소 #1 에 있는 원격 애플리케이션은 레이어 3 애플리케이션입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>IP 주소 #1에서 원격 애플리케이션은 WAN IP를 위해 설정됩니다. 이것은 SAMComplete와 호환되지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP 주소 #1 에서 원격 애플리케이션은 스택 VLAN TCP 캡슐화를 위해 설정됩니다 . 이것은 SAMComplete 와 호환되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP 주소 #1 에서의 원격 애플리케이션은 VPLS 캡슐화를 위해 설정되었습니다 . 이것은 SAMComplete 와 호환되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP 주소 #1 에서의 원격 애플리케이션은 MPLS 캡슐화를 위해 설정되었습니다 . 이것은 SAMComplete 와 호환되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>원격 애플리케이션이 #1 서비스만 지원합니다.  </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>단방향 지연 테스트는 로컬 및 원격 테스트 장치 모두가 OWD 시간 소스 동기화를 가지고 있어야 합니다 .  로컬 장치에서 단방향 지연 동기화가 실패하였습니다 .  OWD 시간 소스 하드웨어로의 모든 연결을 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>단방향 지연 테스트는 로컬 및 원격 테스트 장치 모두가 OWD 시간 소스 동기화를 가지고 있어야 합니다 .  원격 장치에서 단방향 지연 동기화가 실패하였습니다 .  OWD 시간 소스 하드웨어로의 모든 연결을 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA;SAMComplete 테스트를 실행하기 전에 왕복 시간 (RTT) 테스트를 실행해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>TrueSpeed 세션을 설정할 수 없습니다 . " 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>업스트림 방향에 대한 TrueSpeed 세션을 설정할 수 없습니다. "네트워크" 페이지로 가서 구성을 확인하고 다시 시도하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>다운스트림 방향에 대한 TrueSpeed 세션을 설정할 수 없습니다. "네트워크" 페이지로 가서 구성을 확인하고 다시 시도하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>��재 선택한 서비스를 테스트하도록 변경하여 왕복 시간 (RTT) 테스트는 유효하지 않게 되었습니다 .  "TrueSpeed 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 다운스트림 방향의 총 CIR(Mbps) 가 0 일 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA; 서비스 #1 에 대한 업스트림 방향의 총 CIR(Mbps) 가 0 일 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>유효하지 않은 구성:&#xA;&#xA;서비스 #1에 대한 CIR(Mbps)가 0일 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>��떤 트래픽도 수신되지 않았습니다 . " 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>주 결과 보기</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>테스트 &#xA; 정지</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>   보고서가 생성되었습니다 . 보려면 Next 를 클릭하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>보고서 보기 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAM Complete- 이더넷 서비스 활성화 테스트</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>TrueSpeed 사용</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>로컬 네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>로컬 장치는 구성이 필요하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>원격 네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>원격 장치는 구성이 필요하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>로컬 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>원격 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>서비스</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>태깅</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>태깅이 사용되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>SLA 처리량</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>SLA 정책</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>어떤 폴리싱 테스트도 선택되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>SLA 버스트</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>필요한 기능이 없어 버스트 테스트가 비활성화 되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>SLA 성능</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>rueSpeed 는 현재 사용할 수 없습니다 .&#xA;&#xA;TrueSpeed 는 루프백 측정 모드가 아닐 때 " 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>로컬 고급 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>고급 트래픽 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>고급 LBM 설정</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>고급 SLA 설정</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>랜덤/EMIX 크기</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>고급 IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 고급</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>고급 태깅</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>서비스 Cfg 테스트</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>서비스 Perf 테스트</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Y.1564 테스트 나가기</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>디스커버리 서버 상태</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>디스커버리 서버 메시지</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>MAC 주소 및 ARP 모드</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>SFP 선택</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>WAN 소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>정적 - WAN IP</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>WAN 게이트웨이</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>WAN 서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>트래픽 소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>트래픽 서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>VLAN 태깅이 로컬 네트워크 포트에서 사용됩니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q ( 적층 VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>대기 중</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>서버 접속 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>서버에 접속할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP 획득</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>허용 임대 : #1 분</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>감소 임대 : #1 분</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>임대 해제</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>디스커버리 서버 :</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>서비스 1: VoIP - 트래픽의 25% - #1 및 #2 바이트 프레임</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>서비스 2: 비디오 전화통신 - 트래픽의 10% - #1 및 #2 바이트 프레임</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>서비스 3: 우선순위 데이터 - 트래픽의 15% - #1 및 #2 바이트 프레임</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>서비스 4: BE 데이터 - 트래픽의 50% - #1 및 #2 바이트 프레임</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>모든 서비스</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>서비스 1 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>음성</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>10GE 에서 대역폭 입도 레벨은 0.1Mbps 입니다 . 그 결과 CIR 값은 이 입도 레벨의 배수입니다 .</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>40GE 에서 대역폭 입도 레벨은 0.4Mbps 입니다 . 그 결과 CIR 값은 이 입도 레벨의 배수입니다 .</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>100GE 에서 대역폭 입도 레벨은 1Mbps 입니다 . 그 결과 CIR 값은 이 입도 레벨의 배수입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>크기 구성</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>정의된 길이</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>EMIX 주기</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>패킷 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>계산된 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>계산된 프레임 크기는 패킷 길이와 캡슐화를 사용하여 결정됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>서비스 2 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>서비스 3 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>서비스 4 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>서비스 5 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>서비스 6 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>서비스 7 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>서비스 8 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>서비스 9 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>서비스 10 트리플 플레이 속성</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>소형</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>서비스 수</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>레이어</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>LBM 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>트리플 플레이 설정 ...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>서비스 특성</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC 및 프레임 크기 설정</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC, 프레임 크기 설정 및 EtherType</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC, 패킷 길이 및 TTL 설정</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>DA MAC 및 패킷 길이 설정</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>네트워크 설정 ( 고급 )</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>길이가 &lt;b>%1&lt;/b> 바이트 패킷 이상인 경우 로컬 / 원격 장치로서 부적합합니다.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>서비스</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>설정…</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>  사용자 크기</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL ( 홉 )</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>목적지 MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>모두 보기</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>원격 전용</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>로컬 전용</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>LBM 설정 ( 고급 )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>네트워크 설정 랜덤/EMIX 크기</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 1 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>프레임 크기 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>패킷 길이 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>사용자 크기 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>점보 크기 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>프레임 크기 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>패킷 길이 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>사용자 크기 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>점보 크기 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>프레임 크기 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>패킷 길이 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>사용자 크기 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>점보 크기 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>프레임 크기 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>패킷 길이 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>사용자 크기 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>점보 크기 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>프레임 크기 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>패킷 길이 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>사용자 크기 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>점보 크기 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>프레임 크기 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>패킷 길이 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>사용자 크기 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>점보 크기 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>프레임 크기 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>패킷 길이 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>사용자 크기 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>점보 크기 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>프레임 크기 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>패킷 길이 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>사용자 크기 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>점보 크기 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>서비스 1 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>서비스 1 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 2 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>서비스 2 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>서비스 2 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 3 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>서비스 3 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>서비스 3 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 4 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>서비스 4 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>서비스 4 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 5 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>서비스 5 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>서비스 5 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 6 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>서비스 6 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>서비스 6 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 7 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>서비스 7 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>서비스 7 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 8 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>서비스 8 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>서비스 8 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 9 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>서비스 9 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>서비스 9 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>서비스 10 랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>서비스 10 랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>서비스 10 랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>서비스가 다른 VLAN ID's 나 사용자 특성을 가지고 있습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>아닙니다 . 모든 서비스가 동일한 VLAN 설정을 사용합니다</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>고급 ...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>사용자 Pri.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>SVLAN Pri.</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>VLAN 태깅 ( 캡슐화 ) 설정</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>SVLAN 우선순위</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>모든 서비스는 같은 트래픽 목적지 IP를 사용할 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>트래픽 목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>트래픽 목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>IP ID 증가 설정</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>IP 설정 (로컬 전용)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>IP 설정</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>IP 설정 (원격 전용)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>IP 설정 ( 고급 )</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>고급 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>서비스가 다른 TOS 나 DSCP 설정을 가지고 있습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>아닙니다 . TOS/DSCP 는 모든 서비스에서 동일합니다 .</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>전체 SLA</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>전체 CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>업스트림 전체 CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>다운스트림 전체 CIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>전체 EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>업스트림 전체 EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>다운스트림 전체 EIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>전체 모드</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>처리량 테스트 최대 로컬 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>처리량 테스트 최대 원격 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>전체 모드 활성화</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>경고: 선택한 무게 값은 현재 100%가 아닌 총 &lt;b>%1&lt;/b>%입니다.</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>SLA 처리량 , Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA 처리량 , L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>SLA 처리량 , L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>SLA 처리량 , L1 Mbps ( 단방향 )</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA 처리량 , L2 Mbps ( 단방향 )</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>무게 (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>폴리싱</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>최대 로드</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>다운스트림 전용</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>업스트림 전용</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>고급 트래픽 설정하기</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>SLA 폴리싱 , Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>최대 폴리싱</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>버스트 테스팅 실행</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>오차 (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>오차 (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>버스트 테스트를 실행하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>버스트 테스트 종류 :</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>고급 버스트 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>프레임 지연 정밀도</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>프레임 지연을 SLA 요구로 포함</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>프레임 지연 변이를 SLA 요구로 포함</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>SLA 성능 ( 단방향 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>프레임 지연 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>프레임 지연 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>고급 SLA 설정하기</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>서비스 설정</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>CIR 이하 단계 수</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>단계 지속 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>단계 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>단계 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>단계 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>단계 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>단계 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>단계 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>단계 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>단계 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>단계 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>단계 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>단계 퍼센트</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>서비스 성능</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>각 방향을 별도로 테스트하기 때문에 , 전체 테스트 지속 시간은 입력한 값의 두 배가 될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>실패 시 테스트 정지</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>고급 버스트 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>J-QuickCheck 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Y. 1564 테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>테스트 할 서비스 선택</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>모두 설정</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  총 :</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>서비스 설정 테스트</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>서비스 성능 테스트</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>옵션 측정</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>처리량 (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>최대 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>최대 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>최대 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>TrueSpeed 서비스가 활성화되었다면 옵션 측정을 수행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Y.1564 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Y.1564 테스트 건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>지연 변이</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>테스트 실패 요약</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>판정</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Y.1564 판정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>테스트 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>테스트 Svc 1 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>테스트 Svc 2 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>테스트 Svc 3 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>테스트 Svc 4 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>테스트 Svc 5 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>테스트 Svc 6 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>테스트 Svc 7 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>테스트 Svc 8 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>테스트 Svc 9 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>테스트 Svc 10 판정 설정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Perf 테스트 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Pert 테스트 Svc 1 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Pert 테스트 Svc 2 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Pert 테스트 Svc 3 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Pert 테스트 Svc 4 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Pert 테스트 Svc 5 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Pert 테스트 Svc 6 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Pert 테스트 Svc 7 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Pert 테스트 Svc 8 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Pert 테스트 Svc 9 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Pert 테스트 Svc 10 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Perf 테스트 IR 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Perf 테스트 프레임 손실 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Perf 테스트 지연 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Perf 테스트 지연 변이 판정</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Perf 테스트 TrueSpeed 판정</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>서비스 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>서비스 1 설정 결과</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>최대 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>다운스트림 최대 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>업스트림 최대 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>최대 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>다운스트림 최대 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>업스트림 최대 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>CIR 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>다운스트림 CIR 판정</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>프레임 지연 변이 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>OoS 개수</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>에러 프레임 발견</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>일시 정지 발견</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>업스트림 CIR 판정</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>CBS 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>다운스트림 CBS 판정</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>구성된 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tx 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>평균 Rx 버스트 크기 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>예상 CBS (KB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>업스트림 CBS 판정</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>EIR 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>다운스트림 EIR 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>업스트림 EIR 판정</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>폴리싱 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>다운스트림 폴리싱 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>업스트림 폴리싱 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>단계 1 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>다운스트림 단계 1 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>업스트림 단계 1 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>단계 2 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>다운스트림 단계 2 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>업스트림 단계 2 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>단계 3 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>다운스트림 단계 3 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>업스트림 단계 3 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>단계 4 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>다운스트림 단계 4 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>업스트림 단계 4 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>단계 5 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>다운스트림 단계 5 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>업스트림 단계 5 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>단계 6 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>다운스트림 단계 6 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>업스트림 단계 6 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>단계 7 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>다운스트림 단계 7 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>업스트림 단계 7 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>단계 8 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>다운스트림 단계 8 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>업스트림 단계 8 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>단계 9 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>다운스트림 단계 9 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>업스트림 단계 9 판정</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>단계 10 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>다운스트림 단계 10 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>업스트림 단계 10 판정</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>최대 처리량 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>최대 처리량 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>최대 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>단계</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>버스트</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>키</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>각 단계에 대한 결과를 검토하기 위해 바를 클릭하세요 .</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (처리량 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (처리량 L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (처리량 L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>에러 발견</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>단계 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>단계 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>단계 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>단계 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>단계 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>단계 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>단계 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>단계 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>단계 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>단계 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>SLA 한계</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>서비스 2 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>서비스 3 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>서비스 4 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>서비스 5 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>서비스 6 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>서비스 7 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>서비스 8 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>서비스 9 설정 결과</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>서비스 10 설정 결과</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>서비스 성능 결과</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Svc. 판정</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR 현재</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR 최대</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR 최소</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR 평균</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>프레임 손실 초</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>프레임 손실 카운트</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>지연 현재</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>지연 최대</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>지연 최소</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>지연 평균</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>지연 Var. 현재</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>지연 Var. 최대</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>지연 Var. 최소</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>지연 Var. 평균</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>가능</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>사용 가능</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>유효 초</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>유효하지 않은 초</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>심각한 에러 초 (SES)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>서비스 Perf 처리량</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>지연 변이</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>TrueSpeed 서비스 . TrueSpeed 결과 페이지에서 결과를 볼 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR 평균&#xA;(처리량&#xA;L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR 평균&#xA;(처리량&#xA;L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>프레임 손실 &#xA; 율</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>편도 지연 &#xA; 평균 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>지연 변이 &#xA; 최대 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>에러가 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>지연 &#xA; 평균 . (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Cur.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Cur.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, 최대 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, 최대 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, 최소 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, 최소 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>프레임 손실 &#xA; 개수</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>프레임 손실 &#xA; 율 &#xA; 한계</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>지연 &#xA; 현재 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>지연 &#xA; 최대 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>지연 &#xA; 최소 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>지연 &#xA; 평균 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>지연 &#xA; 임계 &#xA; (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>지연 &#xA; 현재 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>지연 &#xA; 최대 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>지연 &#xA; 최소 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>지연 &#xA; 한계 &#xA; (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>지연 Var.&#xA; 현재 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>지연 변이 &#xA; 평균 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>지연변이 &#xA; 한계 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>유효 &#xA; 초 &#xA; 비율</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>유효하지 않은 &#xA; 초</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>심각한 &#xA; 에러 &#xA; 초 (SES)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>서비스 설정 &#xA; 처리량 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>서비스 설정 &#xA; 처리량 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>서비스 Perf&#xA; 처리량 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>서비스 Perf&#xA; 처리량 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>네트워크 설정</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>사용자 프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>점보 프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>사용자 패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>점보 패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>계산된 프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>네트워크 설정 (랜덤/EMIX 크기)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>랜덤/EMIX 길이 (원격)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>랜덤/EMIX 길이 (로컬)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>랜덤/EMIX 길이</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>고급 IP 설정 - 원격</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>다운스트림 CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>다운스트림 EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>다운스트림 정책</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>다운스트림 M</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>업스트림 CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>업스트림 EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>업스트림 정책</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>업스트림 M</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA 고급 버스트</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>다운스트림 프레임 손실 비율</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>다운스트림 프레임 지연 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>다운스트림 프레임 지연 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>다운스트림 지연 변이 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>업스트림 프레임 손실 비율</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>업스트림 프레임 지연 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>업스트림 프레임 지연 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>업스트림 지연 변이 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>SLA 요구로 포함</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>프레임 지연 포함</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>프레임 지연 변이 포함</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete 는 다음의 유효하지 않은 구성 설정을 가지고 있습니다 :</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>서비스 설정 결과</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>TrueSpeed 실행</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>주의 : TrueSpeed 테스트는 서비스 성능 테스트가 활성화되었을 때만 실행됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>패킷 길이 TTl 설정</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>TCP/UDP 포트 설정</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>소스 종류</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>소스 포트</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>목적지 종류</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>목적지 포트</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>TCP 처리량 한계 (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>권장 총 윈도우 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>부스트된 총 윈도우 크기 (바이트)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>권장 # 연결</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>부스트된 # 연결 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>업스트림 권장 총 윈도우 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>업스트림 부스트된 총 윈도우 크기 (바이트)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>업스트림 권장 # 연결</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>업스트림 부스트된 # 연결</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>다운스트림 권장 총 윈도우 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>다운스트림 부스트된 총 윈도우 크기 (바이트)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>다운스트림 권장 # 연결</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>다운스트림 부스트된 # 연결 (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>권장 # 연결</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>TCP 처리량 (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Acterna 페이로드</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>왕복 시간 (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>연결 윈도우 크기와 갯수 권장 사항을 만들기 위해 다음 단계에서 RTT가 사용될 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>TrueSpeed 결과</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>TCP 전송 메트릭스</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>목표 L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>TrueSpeed 서비스 판정</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>업스트림 TrueSpeed 서비스 판정</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>실제 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>업스트림 실제 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>업스트림 목표 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>목표 TCP 처리량 (Mbps))</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>다운스트림 TrueSpeed 서비스 판정</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>다운스트림 실제 TCP 처리량 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>다운스트림 목표 TCP 처리량 (Mbps)</translation>
        </message>
    </context>
</TS>
