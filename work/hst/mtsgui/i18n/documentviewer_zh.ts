<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>加载： </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>加载PDF失败</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>渲染页面失败： </translation>
        </message>
    </context>
</TS>
