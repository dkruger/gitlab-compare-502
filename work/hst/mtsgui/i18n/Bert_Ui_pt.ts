<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation>Tem certeza que deseja cancelar?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Houve um erro ao ler o arquivo.</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>O arquivo tem quadros demais (mais de 50.000). Tente salvar um buffer parcial,&#xA;ou exporte-o para uma unidade USB e carregue-o em um dispositivo diferente. </translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>Deseja apagar todos os dados armazenados?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>Deseja remover o item selecionado?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Nome já existe.&#xA;Deseja substituir os dados antigos?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>Os gráficos estão encontrando erros ao serem gravados no disco. Por favor, salve os gráficos para preservar seu trabalho. Os gráficos irão parar e apagar automaticamente depois de alcançarem um nível crítico</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>Os gráficos encontraram muitos erros de disco para continuar, e foram parados. Os dados dos gráficos foram apagados. O usuário poderá reiniciar os gráficos se desejar</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>Memória USB</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Dispositivo removível</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>O registro Log está cheio</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Configuração Inválida:&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Inválido</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Data 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Data 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisão de especificação I/F de Gestão de CFP MSA suportada por SW</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisão de especificação I/F de Gestão de QSFP MSA suportada por SW</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>Info de DMC</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Código de barras</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Data de Fabricação</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Data de calibração</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Revisão do SW</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>Opção solicita ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Número de série da montagem</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Código da barra de montagem</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Software Instalado</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Opções :</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Aguardando pacotes...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Analisando pacotes...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Análise</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>Aguardando ligação...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>Pressionar "Iniciar Análise" para iniciar... </translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvando resultados, aguarde.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Falhou ao salvar os resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Os resultados estão salvos.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>Esperando para iniciar</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>Prévio teste foi parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>Prévio teste foi parado com erros</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>Prévio teste falhou e parar ao falhar foi ativado</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Sem Execução</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Tempo restante: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Executando</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>Teste não pode completar e foi abortado com erros</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Teste completado</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Teste completado com resultados de falha</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Aprovado</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Teste completado com todos os resultados passando</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>Teste foi parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Parando</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Nenhum teste em execução</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Iniciando</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Conexão verificada</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Conexão perdida</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Verificando conexão</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuração Inválida</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvando resultados, aguarde.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Falhou ao salvar os resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Os resultados estão salvos.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Gráfico do Teste de Taxa de transferência</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados do teste de vazão</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Gráfico do teste de Tráfego Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Resultados do teste de Tráfego Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Gráfico do teste de Tráfego Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Resultados do teste de Tráfego Downstream</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalias</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Anomalias Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Anomalias Downstream</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Resultados do Teste de Latência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Resultados do Teste de Latência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Resultados do Teste de Latência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade Downstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 resultados do teste de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de montante de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 resultados do teste do montante de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de perda de processo de corrente de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 resultados do teste de perda de processos de corrente de byte</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Resultados de Teste CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Resultados do Teste de CBS Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Resultados do Teste de CBS Downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Resultados do Controle de CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Resultados do Teste de Controle de CBS Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Resultados do Teste de Controle de CBS Downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura Downstream</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Resultados do teste 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Resultados do Teste de verificação de Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Resultados do Teste de verificação de Downstream</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Resultados do teste de recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Resultados do Teste de Recuperação de Sistema Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Resultados do Teste de Recuperação de Sistema Downstream</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Resultados do Teste de Carga Estendida</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Passo do Caminho MTU (Path MTU)</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>Etapa RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Passo de Siga a Janela (Walk the Window) upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Passo Siga pela Janela (Walk the Window) Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Passo de Rendimento de TCP Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Passo de Rendimento TCP Downstream</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvando resultados, aguarde.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Falhou ao salvar os resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Os resultados estão salvos.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Não foi possível obter sincronia. Certifique-se de que todos os cabos estão conectados, e que a porta esteja configurada adequadamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Não foi possível encontrar um link ativo. Certifique-se de que todos os cabos estejam conectados, e que a porta esteja configurada adequadamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Não foi possível determinar a velocidade ou duplex da porta conectada. Certifique-se de que todos os cabos estejam conectados, e que a porta esteja configurada adequadamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Não foi possível obter um endereço IP para o  teste local. Favor verificar as definições de comunicação e tentar outra vez. </translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>Incapaz de estabelecer um canal de comunicações para o teste remoto. Favor verificar suas configurações de comunicação e tentar outra vez.</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>O conjunto de testes remotos parece não ter uma versão compatível do software BERT. A versão compatível mínima é %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>A aplicação Layer 4 TCP Wirespeed deve estar disponível tanto na unidade local quanto na remota para executar o TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>A seleção do teste TrueSpeed (Velocidade Verdadeira) não é válida para esta configuração de encapsulamento e será removida.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>A seleção do teste TrueSpeed não é válida para esta configuração de tipo de quadro e será removida.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>A seleção do teste TrueSpeed (Velocidade Verdadeira) não é válida para esta configuração de encapsulamento remoto e será removida.</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>A seleção do teste completo SAM não é válida para esta configuração de encapsulamento e será removida.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuração Inválida</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>Política CBS</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Policiamento</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Etapa #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Etapa #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Etapa #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Etapa #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Etapa #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Etapa #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Etapa #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Etapa #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Etapa #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Etapa #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvando resultados, aguarde.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Falhou ao salvar os resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Os resultados estão salvos.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>ARP local falhou.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>ARP Remoto falhou.</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Tabela, cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Screenshot (cópia da tela)</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - O Histograma está espalhado por várias páginas</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Escala do tempo</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>Não disponível</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Informações do usuário</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Grupos de configuração</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Grupos de resultado</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Registradores de eventos</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Histogramas</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Cópias da tela</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Vazão TCP estimada</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> Relatório de múltiplos testes</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Relatório do teste</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Gerado pelo Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Gerado pelo Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Gerado pelo Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Gerado pelo instrumento de teste da Viavi </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> Passa</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Frequência de varredura (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Passa / Falha</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Configurações: Filtros / </translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> (Padrão e Máscara)</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Padrão</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Máscara</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configuração:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Nenhuma configuração aplicável...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Rádio</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Banda</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Resultado do teste geral do verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>Em progresso</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Hora da parada</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>Ligado</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Não.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Nome da Ocorrência</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Duração/Valor</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Registro de Ocorrência do Teste de Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Status - Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>Trabalhando. Aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Falha!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>Comando concluído!</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>Abortado!</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop Up</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop Down</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Armar</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Desarmar</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Desligar</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Enviar comando Loop</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Switch</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Reinicialização da chave</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Emissão de consulta</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Consulta Loopback</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Armar extremidade próxima</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Desarmar extremidade próxima</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Consulta de energia</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Consulta de alcance</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>Desativar tempo limite</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>Restaurar tempo limite</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Loop sequencial</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Rastreamento Stratum 1</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Rastreamento do relógio do nó de trânsito</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Rastreamento Stratum 2</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Rastreamento Stratum 3</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Rastreamento do relógio Sonet Min</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Rastreamento Stratum 3E</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provisão de Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Não usar para sincronização</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Equipado não-específico</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>Estrutura TUG</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>TU bloqueada</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>34M/45M assíncronas</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>140M assíncronas</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>Mapeamento de ATM</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>Mapeamento MAN (DQDB)</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>Mapeamento de FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>Mapeamento de HDLC/PPP</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 desembaralhado</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>Sinal de teste O.181</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Assíncrono</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Síncrono por Bit</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Síncrono por byte</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>STS-1 SPE estruturado em VT</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Modo VT bloqueado</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation> Mapeamento Assínc. DS3</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation> Mapeamento Assínc. DS4NA</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation> Mapeamento Assínc. FDDI</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 defeito na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 Defeitos na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Formatar?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Fora da faixa</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>DESLIGADO</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>LIGADO</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>HISTÓRICO</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + HISTÓRICO</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Espaço</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Mark</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>VERDE</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>AMARELO</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>VERMELHO</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NENHUMA</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>TODAS</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>REJEITAR</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>INCERTO</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ACEITAR</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>DESCONHECIDO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALHA</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASSA</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Executando</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Identificado</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>Não foi possível identificar</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Identidade desconhecida</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>Restam %1 hora e %2 minutos</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>%1 Falta minuto</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>MUITO BAIXA</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>MUITO ALTA</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>(MUITO BAIXA) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>(MUITO ALTA) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Porta trib</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>Não def.</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Não equipado</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Mapeamento em desenvolvimento</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC sobre SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Mapeamento simples de link de dados (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>Mapeamento de HCLC/LAP-S</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Mapeamento simples de link de dados (set-reset)</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>Mapeamento de quadros Ethernet de 10 Gbit/s</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>Mapeamento de GFP</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>Mapeamento de canal de fibra óptica de 10 Gbit/s</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Reservado - Proprietário</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Reservado - Nacional</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Mapeamento de sinal de Teste O.181</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Reservado (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Equipado não-específico</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Mapeamento DS3 assíncrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Mapeamento DS4NA assíncrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Mapeamento FDDI assíncrono</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC sobre SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 defeito na carga útil VT</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>Não alocado TEI</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Aguarda TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Est. Aguarda TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>Alocado TEI</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Aguarda Est.</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Aguarda Rel.</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Mult. Frm. Est.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Recuperação do timer</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Link desconhecido</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>AGUARDANDO ESTABELECIMENTO</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MULTIQUADRO ESTABELECIDO</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>DIALTONE</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>DISCAGEM BLOCO</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>TOQUE</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>CONECTADOS</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>LIBERAÇÃO DE CHAMADAS</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Discurso</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dados</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletex</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotex</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>Fala BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Data BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Data 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Procurando</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponível</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Solicitação de ingresso</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Repetir solicitação</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Deixar</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Ack Burst completado</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Resposta de ingresso</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Burst completado</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Resposta do status</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Conhecer furo no fluxo</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erro</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Erro: Serviço ainda não em buffer</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Erro: Solicitação de repetição de pacote não é válida</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Err: Serviço inexistente</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Erro: Seção inexistente </translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Erro: Erro da Sessão</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Erro: Versão de comando e controle sem suporte</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Erro: Servidor cheio</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Erro: Acesso duplicado</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Erro: IDs de sessão duplicados</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Erro: Taxa de Bits inválida</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Erro: Sessão destruída pelo servidor</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>curto</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>aberto</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>normal</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>invertido</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Nível muito baixo</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Selecionado</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Não selecionado</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>PARTIDA</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>PARADA</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Quadros com erro</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Quadros OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Quadros perdidos</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violações de código</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>Log de eventos está cheio</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>Nenhuma chave USB encontrada. Por favor, insira outra e tente novamente.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>Ajuda não disponível para este item.</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>ID de Unidade</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>Sem sinal</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Sinal</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Pronto</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Usado</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>ID Satélite</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>ID GNSS</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Máscara</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>Máscara MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>Máscara TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>Resultados MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>Máscara MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>Resultados TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>Máscara TDEV</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Dados orig. TIE</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Dados Offset rem.</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>Estilo de curva MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Linha + Pontos</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Apenas pontos</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Apenas MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Apenas TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Tipo de máscara</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>Passou MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>Falhou MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>Passou TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>Falhou TDEV</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Intervalo de observação (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Calculando </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Cálculo cancelado</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Cálculo encerrado</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>Atualizando dados TIE </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>Dados TIE carregados</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>Nenhum dado TIE carregado</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation> Memória insuficiente para executar Wander Analysis localmente.&#xA;São necessários 256 MB de RAM. Use um software externo de análise.&#xA;Veja mais detalhes no manual.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Desloc. freq. (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Taxa de deriva (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Amostras</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Amostra de taxa (por segundo)</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Blocos</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Bloco atual</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Remover Offset</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Seleção de curva</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Ambas as curvas</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Apenas rem. Offset</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Capturar tela</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Sem etiqueta</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Porta 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Porta 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Barra de ferramentas</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro de mensagens</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Informações gerais:</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Gráficos desabilitados</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Imprimir Erro!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mín Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Máx Mbps</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Jitter PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Máx Jitter PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err CC Tot</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Máx</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err PMT Tot</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Máx</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err PID Tot</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Máx</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Fluxos</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Erros de Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Erros de Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID do fluxo de transporte</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perda atual Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pico perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Jitter Pct (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Max jitter Pcts (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Tot. OoS Pcts</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS atual Pcts</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS máx Pcts</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot err dist</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Erro dist atual</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Erro dist máx</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Erro tot período</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err atual período</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Erro máx período</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Perda máx período</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Dist perda mín</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Total perdas de sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Total erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Erros Sync Byte atual</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Máx erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>DF MDI Atual</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>DF MDI Máx</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MLR MDI Atual</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>Máx MDI-MLR</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Total erros transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Erros transp atual</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Máx erros transp</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err PAT Tot</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Erros PAT atual</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Máx</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># fluxos analisados</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Total Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Erros de Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Erros de Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID do fluxo de transporte</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Prog n.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Mbps atual prog</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Mbps mín prog</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Mbps máx prog</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perda atual Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pico perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Jitter Pct (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Max jitter Pcts (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS Pkts Tot</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS atual Pcts</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS máx Pcts</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot err dist</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Erro dist atual</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Erro dist máx</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Erro tot período</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err atual período</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Erro máx período</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Perda máx período</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Dist perda mín </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Total perdas de sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Total erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Erros Sync Byte atual</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Máx erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>DF MDI Atual</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>DF MDI Máx</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MLR MDI Atual</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>Máx MDI-MLR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>JitterPCR atual</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Máx Jitter PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err CC Tot</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>Erros CC atual</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Máx</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Total erros transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Erros transp atual</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Máx erros transp</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err PAT Tot</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Erros PAT atual</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Máx</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err PMT Tot</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>Erros PMT atual</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Máx</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err PID Tot</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>Erros PID atual</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Máx</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>End IP</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perda pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Perda máx pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Jit pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Máx Jit pct</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Fluxos</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Mbps Rx, L1 atual</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Mbps mín prog</translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>ID de transporte</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Perdas de sinc</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Total erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Erro Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Máx erros Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Total erros PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>Err PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Máx</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Jitter PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>Máx Jitter PCR</translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Total erros CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Máx</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Total erros PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Máx</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Total erros PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Máx</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Total erros transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Erro transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Máx erros transp</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># fluxos analisados</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Total Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Erros de Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Erros de Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>Histórico MPEG</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perda atual Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pico perda Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Jitter atual pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Máx Jitter Pct</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico-a-Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pós-Pico</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Pico</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Pico Pico</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Pós Pico</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Neg-Pico</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informações do relatório de teste</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Código</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Caminho</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Ponte</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>não utilizado</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Resultado Geral do Teste Ótico</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Autoteste Ótico</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configurações:</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalhes</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Empilhados</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Quadro</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLAN empilhada</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profundidade da pilha VLAN</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>Bit DEI da SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>Prioridade de usuário da SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>TPID da SVLAN %1  (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN %1  (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>Nenhum quadro foi definido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Porta trib</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Instalação:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>Teste de verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Resultado do Teste Geral de Verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>Captura de byte POH</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Quadro</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binário</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configurações:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Nenhuma configuração aplicável...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Bytes de overhead</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>Receptor CDMA</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Imprimir Erro!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>Taste PTP</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Perfil carregado</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>Resultado de teste geral PTP</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Verificação PTP</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumo</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>TODOS OS RESULTADOS DO RESUMO OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Teste FC aprimorado</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Teste RFC 2544 Aprimorado</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Resultado geral do teste</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Resultados gerais do teste</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latência</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdas de quadros</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Vazão de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Não Executado - Teste anterior foi parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Não Executado - Teste anterior parou com erros</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Não Executado - Teste anterior falhou e parar quando falhar foi ativado</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Sem Execução</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Loopback simétrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>Upstream e Downstream simétricos</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Montante e jusante assimétricos</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latência</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdas de quadros</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Ruptura (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Vazão de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Testes a executar</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Duração de processos selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Duração de Pacote selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Duração de processos Upstream selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Pacote de Duração Upstream selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Duração de processos Downstream selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Pacote de Duração Downstream selecionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Duração de processos Downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de montante de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 gráfico do teste de perda de processo de corrente de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 Teste Gráfico de Taxa de transferência de crédito de Byte Buffer</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Duração (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Hora da parada</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Mais longa</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Mais curta</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Última</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Disrupções</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configuração:</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Conector</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Comprimento de onda nominal (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Taxa nominal de Bits (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Comprimento de onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Taxa mínima de Bits (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Taxa máxima de Bits (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo de nível da alimentação</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Cód. fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Máx nivel Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Máx nivel Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN do fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Código da data</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Código do lote</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitoramento de diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte de diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceptor</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>N.° da versão HW / SW</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>N.° da revisão da especificação MSA HW</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>N.° da revisão I/F do gerenciamento MSA</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Classe de energia</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo de nível da alimentação Rx</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Potência máxima de lambda (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>N.° de fibras ativas</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Comprimentos de onda por fibra</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL por intervalo da fibra (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Taxa de bits máxima da linha da rede (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Taxas suportadas</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duração</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Inválido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Atraso de quadro (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Atraso de quadro (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Atraso em um só sentido</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Linha</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Instalação:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Teste TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Virar para cima</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Solução de Problemas</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Assimétrico</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Simetria de Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>MTU do caminho</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Vazão TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avançado</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Etapas para executar</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Não Executado - Teste anterior foi parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Não Executado - Teste anterior parou com erros</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Não Executado - Teste anterior falhou e parar quando falhar foi ativado</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Sem Execução</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Teste de TrueSpeed VNF </translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>O teste foi cancelado pelo usuário.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>O teste não foi iniciado.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Senha de Upstream</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>A taxa de transferência é maior do que 90% do alvo.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Falha de Upstream</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>A taxa de transferência é menor do que 90% do alvo.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Senha de downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Falha de downstream</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informações do relatório de teste</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome do técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Empresa</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>E-mail</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefone:</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificação do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nome do teste</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Código de autenticação</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Data de criação da autenticação</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Tempo de parar o teste</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentários</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Hora da parada</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Acompanhar</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Sequência</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configuração:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Registro:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>StrmIP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nome do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Hora da parada</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nome Prog</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>Em progresso</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>Resultado do teste geral Varredura de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Teste de varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configuração:</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultados:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>TrueSam Resultado de teste global</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>Teste nao pode completar e foi abortado com erros. Resultados no relatório podem ser incompletos.</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>Teste foi parado por usuário. Resultados no relatório podem ser incompletos.</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Resultados do subteste</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>Resultado do Teste J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>Resultado do Teste RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>Resultado do Teste de SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>Resultado do Teste J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>Resultado do Teste TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Não Executado - Teste anterior foi parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Não Executado - Teste anterior parou com erros</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Não Executado - Teste anterior falhou e parar quando falhar foi ativado</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Sem Execução</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro de mensagens</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Mensagem Log (continuado)</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordem de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comentários/Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versão de SW</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>Resultado do Teste Global SAMCompleto</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAM Completo</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Resultados gerais do teste de configuração</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Resultados gerais do teste de desempenho</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdas de quadros</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variação de Delay</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Grupos de relatório</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>O relatório não pôde ser criado: </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>espaço em disco insuficiente.</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>O relatório não pôde ser criado</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Configurações da porta</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Ímpar (Odd)</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Par (Even)</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Frequência Baud (Baud rate)</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Paridade (Parity)</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Desliga</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Bits de dados</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Bits de parada</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Configurações do terminal</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Enter/Return</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Eco local</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Ativar teclas reservadas</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Teclas desativadas</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ligado</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Desabilitar</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Ativar</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>Soft Keys (teclas de função) EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP do painel</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Soft Keys (teclas de função) do painel</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Soft Keys (teclas de função) do painel</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>Teclas do Painel ARQUIVO, CONFIGURAÇÃO, RESULTADOS, EXPORTAR, INICIAR/PARAR</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Terminal&#xA;Janela</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Restaurar&#xA;padrões</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>Configuração&#xA;VT100</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Limpar&#xA;tela</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Exibir&#xA;teclado</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Mover&#xA;teclado</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>Autobaud</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturar&#xA;tela</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Ocultar&#xA;teclado</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Progresso do Loop:</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Resultado:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Erro inesperado</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Executar&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA; Isso irá reescanear o link e reiniciará o teste.&#xA;&#xA; Você não verá mais somente os streams que foram transferidos da aplicação Explorer.&#xA;&#xA;Continuar?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Progresso Automático:</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Detalhes:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Auto 'In Progress'. Aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Falha automática.</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Completado automaticamente.</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Abortado automaticamente.</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Status - Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Detecção </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Varredura...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Erro inesperado</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Detalhes do dispositivo emparelhado</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Enviar arquivo</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Esquecer</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Selecionar um arquivo</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Enviar</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Conectando...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Desconectando...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Nome do grupo: </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Digite o nome do gupo:</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Certificados e chaves (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Nome de Grupo Inválido</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Um bundle com nome de "%1" já existe.&#xA; Favo selecionar outro nome.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Adicionar arquivos</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Criar Grupo</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Renomear Grupo</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Modificar Grupo</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Abrir Pasta</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Adicionar novo pacote ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Adicionar certificados a %1 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Deletar %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>Deletar %1 de %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>ID de Pista Virtual</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>Distorção injetada (Bits)</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Distorção injetada (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>Faixa Física #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Faixa:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>Não foi possível configurar a data</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Slot Afluente</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Aplicar</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Padrão</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Largura de banda</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Variações ainda não estão aplicadas.</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>Muito trib. slots estão selecionados.</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>Muito poucos trib. slots estão selecionados.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Outro...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Hoje</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Digite a data: dd/mm/aaaa</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Digite a data: mm/dd/aaaa</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Até </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> bytes</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Segundos</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Minutos</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Horas</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Dias</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>dd/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Duração: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Configure o endereço de destino na guia 'All Services'.</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Configure o endereço de destino na guia 'All Streams'.</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo do destino</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo de Loop</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>IP origem deste Hop</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>IP dest do próximo Hop</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Máscara de sub-rede do próximo Hop</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Configure o endereço da origem, na guia Ethernet, para todos os quadros.</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo da origem</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC padrão</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC do usuário</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Configure o endereço da origem na guia 'All Services'.</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Configure o endereço da origem na guia 'All Streams'.</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC da origem</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridade de usuário da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>TPID da SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incremento do PBit</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID de usuário da SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>Não é configurável no modo loopback.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>Especifique o ID da VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridade do usuário</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Comprimento</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Comprimento dos dados (byte)</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Controle</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Tipo/&#xA;Comprimento</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Túnel&#xA;Rótulo</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>Filtro da ID da VLAN em B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>Prioridade B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>Bit DEI da B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Rótulo do túnel</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Prioridade do túnel</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>EtherType da B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>TTL do túnel</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>Rótulo&#xA;VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>Prioridade I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>Bit DEI da I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>Bit UCA da I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>Filtro da ID de serviço de I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>ID de serviço de I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>Rótulo VC</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>Prioridade VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>EtherType da I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>Rótulo&#xA;MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>Rótulo MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>Prioridade MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>Rótulo&#xA;MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>Rótulo MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>Prioridade MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dados</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Quadro do cliente que está sendo carregado:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo de quadro</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Tamanho do quadro do cliente</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Tamanho do quadro do usuário</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Tamanho subdimensionado</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Tamanho Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>A seção de dados contém um pacote IP. Configure esse pacote na guia IP.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Configure a filtragem de dados na guia 'IP Filter'.</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Carga útil Tx</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>Configuração RTD</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Análise da carga útil</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Carga útil Rx</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>Temporizador LPAC</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Padrão Rx BERT</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Padrão Tx BERT</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Padrão do usuário</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Configurar os quadros de entrada:</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Configurar os quadros de saída:</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>O comprimento/tipo do campo é 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>O comprimento dos dados é aleatório</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Índice de início de prioridades do usuário</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Tipo de arquivo:</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nome do arquivo:</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Você tem certeza que deseja excluir&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Tem certeza de que quer excluir todos os arquivos desta pasta?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Até </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervalo:  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> é inválido - os IPs de origem das Portas 1 e&#xA;2 não devem coincidir. IP anterior restaurado.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Endereço IP inválido&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervalo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>O endereço IP fornecido não é adequado para esta configuração.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 caracteres por byte, até </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 caracteres por byte, </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Nome do código de loop</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Padrão de Bit</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Entrega</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>Na banda</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Fora da banda</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop Up</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop Down</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Outro</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Nome do código de loop</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Del.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Adicionar/Remover</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Acompanhamento Tx</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Intervalo:  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Selecionar o canal</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>Digite o valor KLM</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Intervalo:  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Selecione o membro VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>Byte PSI</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Valor do Byte</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>Tipo ODU</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>Porta tributária #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervalo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Selecione o membro VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervalo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Editor de Byte Overhead</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Selecione um byte válido, para editar.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Óptica não reconhecida</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Preparando página.  Aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>Página vazia</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Adicionar item</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Modificar item</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Excluir item</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Adicionar linha</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Modificar linha</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Excluir linha</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Desliga</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carga</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Rastreabilidade desconhecida</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Não equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Não equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Não equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Não equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Comprimento:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Comprim:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Agora</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Digite a hora: hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Des-selecionar tudo</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Byte %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation> Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome do serviço</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Ambas as Direções</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Tipo do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>O tipo do Serviço foi reinicializado em 'Data' (dados) por causa das alterações ao comprimento do quadro e/ou CIR.</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Taxa de amostragem (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Chamadas</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamanho do quadro</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Comprimento do pacote</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>Num. Canais</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compressão</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>Criar VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>Definir os membros VCG com  numeração padrão de canal:</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Definir Tx VCG</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Definir Rx VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Largura de banda da carga útil (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Número de Membros</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Distribuir&#xA;os Membros</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>Distribuir membros VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Definir a distribuição personalizada de membros VCG</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Instância</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Etapa</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>Editar membros VCG</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Formato do endereço</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Novo membro</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Padrão</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>Fluxos agregados foram importados do Explorer. &#xA;Pressione o botão [Join Streams...] para juntar.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>IP de Src</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP do Destino</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Livro de endereços</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Não seleciona nenhum</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Localizar próxima</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Remover</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Não seleciona nenhum</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>IP de Src</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP do Destino</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Juntar fluxos ...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Adicionar</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP do destino</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>Os IPs introduzidos são adicionados ao catálogo de endereços.</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Para juntar</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Já combinado</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>IP de Src</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP do Destino</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>Juntar</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Deixar o fluxo</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Endereço IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>Endereço IP do dest.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Deixar fluxos...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Des-selecionar tudo</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Config rápido</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Obs.:  Isso irá substituir a configuração de quadro atual.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rápido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Completo (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensidade</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Família</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incremento do PBit</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridade do usuário</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>TPID (hex) de usuário</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Aplicando</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Índice de início de prioridades do usuário</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridade de usuário da SVLAN</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Carregar...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carga</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Data longa:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Data curta:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Tempo longo:</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Tempo curto:</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Números:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Desalocado</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Alocado</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Reservado10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reservado11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reservado01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reservado00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Salvar...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Restaurar os padrões do teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Configurar&#xA; fluxos...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Distribuição de carga</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Selecionar&#xA;Tudo</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Limpar&#xA;tudo</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Auto&#xA;Distribuir</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Fluxo</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamanho do quadro</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carga</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Taxa de quadros</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% da taxa de linha</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>Rampa inicia em</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Constante</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Limiar máx util</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Total (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Total (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Total (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Total (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Nota: </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Ao menos um fluxo deve estar ativado.</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>O limite máximo de utilização é </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>A carga máxima possível é </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>A carga total especificada não deve exceder esta.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Preencha o percentual:  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Inserir taxa de quadros:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Preencha o bit rate:  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Nota. &#xA; Taxa de bit não detectada. Por favor, pressione cancelar &#xA; e tente novamente quando a taxa de bit for detectada.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Definir serviços Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Taxa de&#xA;amostragem (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Chamadas</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Taxa por&#xA;chamada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Taxa total&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Total quadro básico&#xA;Tamanho (byte)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Supressão do silêncio</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Buffer da Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>Num. Canais</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compressão</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Taxa (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Tamanho total do quadro básico (byte)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Taxa de início (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Tipo de carga</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Incremento de tempo (s)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Incremento de carga (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Total Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Simulado</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Data 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Data 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Data 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Data 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Randômico</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Vídeo   </translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dados</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Nota:</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Ao menos um serviço deve estar ativado.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>A carga máxima possível é </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>A carga total especificada não deve exceder esta.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Distribuição: </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Largura de banda: </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Estrutura: </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>default (padrão)</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Etapa</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Livro de endereços</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Nova entrada</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>IP da origem (0.0.0.0 = "Qualquer")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>Necessário</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Nome (tamanho máx. 16 caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Adicionar entrada</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Importar/Exportar</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importar</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>Importar entradas do disco USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Exportar entradas para um disco USB</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Excluir</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Excluir tudo</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Salvar e fechar</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Opcional</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Importar entradas via USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Importar entradas</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Separado por vírgulas (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Adicionando entrada</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Cada entrada deve ter quatro campos: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Nome> separados por vírgulas.  Linha pulada.</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abortar</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Continuar</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>não é um endereço IP válido de origem.  Linha pulada.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>não é um endereço IP válido de destino.  Linha pulada.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>não é um PID válido.  Linha pulada.</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Uma entrada deve ter um nome (com até 16 caracteres).  Linha pulada</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Um nome de entrada não deve ser maior do que 16 caracteres.  Linha pulada</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Uma entrada com os mesmos Src. Ip, Dest. IP e PID&#xA;já existe e tem um nome</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>SOBRESCREVER o nome de uma entrada existente&#xA;ou&#xA;PULA a importação deste item?</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Pular</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Sobrescrever</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Uma das entradas importadas possui um valor PID.&#xA;Entradas com valores PID somente são usadas em aplicativos MPTS.</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Exportar entradas via USB</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Exportar entradas</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>IPTV_Address_Book</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>já está em uso. Escolha um nome diferente ou edite a entrada existente.</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>Já existe uma entrada com esses parâmetros.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>Você tem certeza de que deseja excluir todas as entradas?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP do destino</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Adicionando entrada</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>Já existe uma entrada com este nome, com</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>O que deseja fazer?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Data: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Hora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados do evento selecione 'View->Result Windows->Single', no menu.</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ligado</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Código</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Ponte</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Caminho</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados de byte K1/K2, selecione 'View->Result Windows->Single', no menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Quadro</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binário</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados de atraso em uma única direção do evento selecione ' View->Result Windows->Single', no menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Perda da origem</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>Err B1</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>Err REI-L</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>Err MS-REI</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>Err B2</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>Err REI-P</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>Erro B2</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>Err HP-REI</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>Err B3</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>Err REI-V</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>Err BIP-V</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>Erro B3</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>Err LP-REI</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>Err LP-BIP</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAZ</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAZ</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>Err FAZ STL</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAZ</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAZ</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Err Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Perda do sinal</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Perda de sinc de quadro</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Erro de pal do quadro</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>Erro FAZ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Mais longa</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Mais curta</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Última</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Disrupções</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados da interrupção do serviço, selecione 'View->Result Windows->Single', no menu.</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>No. SD</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados da interrupção do serviço, selecione 'View->Result Windows->Single', no menu.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>PARTIDA</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>PARADA</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>No. SD</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Estouro</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais resultados de chamadas, selecione 'View->Result Windows->Single', no menu.</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>tom de linha</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  VERDADEIRO</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duração</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Inválido</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Data: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Hora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nome Prog</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>Strm IP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nome do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Exportar categoria salva de resultado personalizado</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoria salva de resultado personalizado</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Não foi possível localizar a unidade flash USB.&#xA;Insira uma unidade flash ou remova e insira novamente.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>Exportar dados PTP para USB</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>Arquivos de dados PTP (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Exportar relatório para porta USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Exportar Screenshots para porta USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Imagens de tela salvas (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Compactar (ZIP) arquivos selecionados como:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Digite um nome para o arquivo: Máximo de 60 caracteres</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Zip&#xA;&amp;&amp; Exportar</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Digite um nome para o arquivo Zip</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Não foi possível localizar a unidade flash USB.&#xA;Insira uma unidade flash ou remova e insira novamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Não é possível compactar o(s) arquivo(s)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>Exportar dados TIE para USB</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Dados de arquivos Wander TIE (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Exportar dados de cronograma para USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Todos os arquivos de dados de cronograma (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Tipo de arquivo:</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Aberto</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Importar resultados salvos de um dispositivo USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoria salva de resultado personalizado</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Importação&#xA;Categoria personalizada</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Importar relatório de um USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Arquivos de imagem (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Importar&#xA;Logo</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Importar Quick Card do USB</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Arquivos PDF (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Importar&#xA;Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Importa&#xA;Teste</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Descompactar&#xA; &amp;&amp; Importar</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Erro - Incapaz de desanexar um dos arquivos.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>Espaço livre insuficiente no dispositivo de destino.&#xA;Operação de cópia cancelada.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Copiando arquivos ...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Concluído. Arquivos copiados.</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Erro - Falha na cópia dos itens a seguir: &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>Salvar Dados PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>Arquivos PTP (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copiar</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Espaço em disco insuficiente. Excluir outros arquivos salvos ou exportar para USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%1 de %2 livres</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Nenhum arquivo selecionado</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>Total %1 no arquivo selecionado</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Total %1 nos %2 arquivos selecionados</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nome do arquivo:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Digite um nome para o arquivo: Máximo de 60 caracteres</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>Excluir todos os arquivos desta pasta?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation> (Arquivos somente leitura não serão excluídos).</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Excluindo arquivos...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Você tem certeza que deseja excluir&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>Você tem certeza de que deseja excluir este item?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>Ótico não recomendado</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Canal de fibra óptica</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137,6 M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Exibe os resultados da descoberta da estrutura do sinal e da varredura.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Classificar por:</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Classificar</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Classificar novamente</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Apresenta uma seleção de testes disponíveis, que podem ser utilizados para analisar o canal selecionado</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Inicia o teste</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Aguarde ... configurando o canal selecionado ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Exibir detalhes</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Ocultar detalhes</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Range: %1 à %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Encontrar</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Original</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>Ajustar largura</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>Ajustar altura</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Seleção da visualização de teste duplo</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Mais ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(Continuação)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Aviso</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Buffer do log está cheio</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Captura interrompida</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Editar linha</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Selecionar byte:</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>Impossível capturar a cópia de tela.</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>espaço em disco insuficiente</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Cópia de tela capturada: </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>Sobre o fluxo</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>Discar DP</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>Discar MF</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>Discar DTMF</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Unidir</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Ocioso</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Tráfego extra</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>Nenhum disponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>Impossível navegar para links externos</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>Não encontrado</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Chegou ao fundo da página, continua no topo</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Ferramenta de seleção</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>DESLIGANDO relatórios automáticos antes de iniciar o Script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>LIGANDO relatórios automáticos anteriormente desativados</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Criar</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Padrão</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Excluir</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Excluir tudo</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carga</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Enviar</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Tentar novamente</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visão</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS (sinc)</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS (origem)</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Contêiner</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Rótulo do sinal</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Acompanhar</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Formato do traço</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconhecido</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Representa apenas o nível atual e não leva em conta os canais de ordem inferior ou superior. Apenas o canal atualmente selecionado receberá atualizações ao vivo.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>O status do canal, representado por um ícone.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Não há alarmes monitorados</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Alarmes presentes</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Monitoramento c/ nenhum alarme monitorado presente</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Monitoramento c/ alarmes presentes</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>O nome do contêiner do canal. Um sufixo 'c' indica um canal concatenado.</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>O número N KLM do canal, conforme especificado pela RFC 4606</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>O rótulo de sinal do canal</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>O último status conhecido do canal.</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>O canal é inválido.</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI presente</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS presente</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP presente</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Monitoramento</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>Status atualizado em: </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>nunca</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>As informações de rastreamento do canal</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>Informações de formato de rastreamento do canal</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>Não suportado</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Varredura</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Alarme</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Inválido</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Não formatado</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Byte único</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>Terminado com CR/LF</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Dispositivos pareados</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Dispositivos encontrados</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>Cert CA</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Cert de Cliente</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Chave de Cliente</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formatar</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Espaço livre</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Capacidade total</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Rótulo</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Versão atualizada</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Versão instalada</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Categorias</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Lista os nomes dados às categorias personalizadas. Clicar em um nome ativa/desativa a categoria personalizada.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Ao clicar, permite configurar uma categoria personalizada.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Permite excluir uma categoria personalizada alterando as categorias desejadas para exclusão.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>O nome dado à categoria personalizada. Clicar no nome ativa/desativa a categoria personalizada.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Clicar no ícone de configurar abre uma caixa de diálogo de configuração para a categoria personalizada.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Clicar no ícone de exclusão marca ou desmarca a categoria personalizada para exclusão.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Criar relatório</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Criar</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Exibir relatório após a criação</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erro - nome de arquivo não pode ser vazio.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>A modificação das configurações irá atualizar os resultados atuais.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Não está dentro da  sub-rede</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>Esperando por Link Active ... </translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>Esperando por DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Reconfigurando o IP de origem ...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP da origem</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Nome do sistema</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>Não está na sub-rede</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Não está dentro da  sub-rede</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Serviços</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Não está dentro da  sub-rede</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Serviços</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Descoberta (Discovery)</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>Prioridade VLAN</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>IP da rede</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>Redes IP</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Domínios</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Servidores</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Hosts</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Switches</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Roteadores</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Ajustes</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Infraestrutura</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Centro</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Distribuição</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Acesso</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Descoberta (Discovery)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Redes IP descobertas</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Domínios NetBIOS descobertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>VLANs descobertas</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Roteadores descobertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Switches descobertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Hosts descobertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Servidores descobertos</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Relatório de descobertas na rede</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>O relatório não pôde ser criado</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>Espaço em disco insuficiente</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Gerado pelo Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Gerado pelo Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Gerado pelo Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Gerado pelo instrumento de teste da Viavi </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>Geral</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>O disco está cheio, todos os gráficos foram interrompidos.&#xA;Libere espaço e reinicie o teste.&#xA;&#xA;Alternativamente, os gráficos podem ser desativados a partir de 'Tools->Customize'&#xA;, na barra de menus.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Toque para centralizar a escala do tempo</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Propriedades do gráfico</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx.</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Janela</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Janela de saturação</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Salvar os dados da plotagem</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>Espaço livre insuficiente no dispositivo de destino.</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>Pode exportar diretamente do USB se um dispositivo flash for inserido.</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Salvando dados de gráfico. Pode demorar um pouco, favor esperar...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Salvando dados de gráfico</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>Espaço livre insuficiente no dispositivo de destino.Dados do gráfico exportado incompletos.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Ocorreu um erro ao exportar os dados do gráfico. Os dados podem estar incompletos.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 Dia</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 horas</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 hora</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minutos</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 segundos</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Plot_Data</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Toque e arraste para fazer zoom</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Saturação</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Janela</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Conectar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>IP de Src</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP do Destino</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensagem</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Porta Src</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Porta Dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Excluir...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Confirmar ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>As categorias selecionadas serão removidas de todos os testes atualmente em execução.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>Tem certeza de que quer excluir os itens selecionados?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Novo...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Abre uma janela para configurar uma nova categoria de resultados personalizados.</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>Quando pressionado, permite marcar categorias personalizadas a serem excluídas da unidade. Pressione o botão novamente, quando tiver terminado a seleção, para excluir os arquivos.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Pressione "%1"&#xA;para começar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Configurar categoria de resultados personalizados</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>Os resultados selecionados, marcado por um '*' não se aplicam à configuração de teste atual e não aparecerão na janela de resultados.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Nome da categoria:</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Digite o nome da categoria personalizada: Máximo de %1 caracteres</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nome do arquivo:</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Salvar como</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Salvar novo</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>O arquivo %1, que contém a&#xA;categoria "%2"</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Resultados selecionados: </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   (Máx. de seleções </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configurar ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumo</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumo</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Máx (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Valor (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Botão Home da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Botão reverter da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Botão seguir da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Botão terminar da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>Sem histórico de chamadas</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Botão Home da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Botão reverter da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Botão seguir da Ajuda...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Botão terminar da Ajuda...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico-a-Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pós-Pico</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Pico</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico-a-Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pós-Pico</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Pico</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Latência (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Contar</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTADOS&#xA;INDISPONÍVEIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Status: PASSA</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Status: FALHA</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Status: N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Expandir para ver opções de filtro</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>N.° de MEPs descobertos</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Definir como pares</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Filtrar a exibição</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Filtro ativado</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Insira o valor do filtro: %1 caracteres no máximo</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>Tipo CCM</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>Taxa de CCM</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>ID MEP do par</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>Nível do domínio de manutenção </translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Especificar ID do domínio</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ID do domínio de manutenção</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>ID da associação de manutenção</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Conjunto de teste configurado. A linha realçada foi definida como MEP par para este conjunto de teste.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Configurar teste como o MEP par realçado falhou.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>Não é possível configurar %1 como %2.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTADOS&#xA;INDISPONÍVEIS</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>GRÁFICOS&#xA;DESATIVADO</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Mude esta janela de resultados para ocupar toda a tela.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>SEM RESULTADOS&#xA;DISPONÍVEIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Personalizado</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 resultados do teste de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 resultados do teste do montante de perda de processos de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 resultados do teste de perda de processos de corrente de byte</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 Teste de resultados de Taxa de transferência de crédito de Byte Buffer</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Teste de montante de Taxa de transferência de crédito de Byte Buffer</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Teste de resultados de Taxa de transferência de crédito de corrente de Byte Buffer</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Exportar Arquivo de Texto...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Exportou log para</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Detalhes&#xA;do fluxo</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Recolher todas as árvores de resultados nesta janela.</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Expandir todas as árvores de resultados nesta janela.</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>RESULTADOS&#xA;CATEGORIA&#xA;VAZIO</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTADOS&#xA;INDISPONÍVEIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTADOS&#xA;INDISPONÍVEIS</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>TODOS OS&#xA;RESULTADOS&#xA;DO RESUMO&#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Colunas</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Exibir colunas</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Des-selecionar tudo</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Exibir somente com Erro</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colunas...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Exibir&#xA;somente&#xA;colunas com erros</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Exibir somente colunas com Erros</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Taxas de tráfego (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Taxas de tráfego (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation># Fluxos analisados</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Tráfego agrupado por</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>Link total</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Fluxos exibidos 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Fluxos adicionais > 128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colunas...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Hop</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Atraso (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para visualizar mais dados de Traceroute, selecione ' View->Result Windows->Single', no menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Tempo Ideal de Transferência</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Tempo Real de Transferência</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Grupo:</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Fluxo</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Programas</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Perda de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Erro de distância</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Erro de período</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Erro Sync Byte</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Exibir somente programas com erro</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colunas...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Mbps total do programa</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Exibir somente&#xA;programas com erro</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Exibir somente programas com erro</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Exibir&#xA;somente&#xA;fluxos com Erro</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Exibir somente fluxos com Erros</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analisar</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation># fluxos&#xA;analisados</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>Mbps&#xA;total de L1</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>Erros de&#xA;Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>Erros de&#xA;Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Lançar&#xA;o analisador</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colunas...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Aguarde..lançando o aplicativo Analisador...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Seg</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Dia</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Escolha um Script.. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Script:</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Estado:</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Estado atual</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Temporizador:</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Tempo decorrido do Script:</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Quantidade do temporizador</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Saída:</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Script terminado em:  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Escolher&#xA;Script</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Executar Script</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Limpar&#xA;Saída</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Parar Script</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>EM EXECUÇÃO...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Tempo decorrido do Script:</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Escolha um Script diferente.. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Erro.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>EM EXECUÇÃO.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>EM EXECUÇÃO..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Personalizar lista de teste</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Mostrar resultados na inicialização</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Para cima</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Para baixo</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Deletar</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Excluir tudo</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renomear</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Separador</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Adicionar atalho</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Adicionar Teste Salvo</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>Excluir todos os favoritos?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>A lista de favoritos é padrão.</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Todos os favoritos personalizados serão excluídos e a lista será restaurada aos padrões para esta unidade.  Deseja continuar?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Configuração de Teste (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Configurações de teste duplo (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Selecionar Teste Salvo</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Aguarde ...&#xA;Lançamento Visuaização de Teste Vazio</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Teste de pin</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Renomear teste preso</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>Pin para lista de testes</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Salvar configuração de teste</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nome do teste:</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Insira o nome para exibir</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Este teste é o mesmo que %1</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>Este é um atalho para iniciar um aplicativo de teste.</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Descrição: %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Esta é uma Configuração de Teste Salva</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Nome do arquivo: %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Substituir</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>Não é possível o lançamento do teste ...&#xA;     As opções expiraram.&#xA;Encerre o programa e inicie novamente o BERT, a partir da página do sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>Este teste não pode ser executado agora.  Ele pode não ser suportado pela configuração de hardware atual, ou pode exigir mais recursos.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Tente remover os testes sendo executados em outras abas.</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Este teste está sendo executado em outro portal. Você quer ir para aquele teste? (no guia %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>Outro teste (no guia %1) pode ser reconfigurado para o teste selecionado.  Quer reconfigurar esse teste?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>A lista está vazia.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Não é possível iniciar teste...&#xA;Função de instabilidade Óptica DESLIGADA.&#xA;Iniciar função de instabilidade óptica a partir da página Home/System.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>Não é possível o lançamento do teste ...&#xA;Recursos de energia não disponíveis.&#xA;Remover/reconfigurar um teste existente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>Não é possível o lançamento do teste ...&#xA;Recursos de energia não disponíveis.&#xA;Des-selecionar outro módulo ou remover/reconfigurar&#xA;um teste existente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Aguarde...&#xA;Adicionando teste </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Aguarde...&#xA;Reconfigurando o teste para </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>Não foi possível iniciar o teste.&#xA;Recursos exigidos podem estar em uso.&#xA;&#xA;Por favor, contate a assitência técnica</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>Construindo os objectos da IU</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>Sincronizando IU com o módulo do aplicativo</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>Inicializando exibições da IU</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Volta</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Escolha&#xA;o Teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright Soluções Viavi</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Informação do equipamento</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Opções </translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>Arquivo gravado</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Modo de acesso por interface de usuário</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>O controle remoto está em uso.&#xA;&#xA;Modalidade de acesso somente leitura impede que o usuário altere as configurações&#xA;o que poderia afetar as operações com o controle remoto.</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Modo de Acesso</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>O MSAM foi reinicializado devido à alteração de configuração do PIM .</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Um PIM foi inserido ou removido. Se estiver fazendo troca de PIMs, continue a fazê-lo agora.&#xA;O MSAM agora será reiniciado. Isso pode demorar até 2 minutos. Aguarde...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>O módulo BERT está funcionando muito aquecido e será &#xA; automaticamente desligado se a temperatura interna continuar &#xA; a subir. Por favor, salve seus dados, desligue o módulo &#xA; BERT e entre em contato com o suporte técnico.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>O módulo BERT foi forçado a encerrar em razão de super aquecimento.</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>PIM XFP no slot errado. Mova o PIM XFP para a Porta #1.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>O PIM XFP do módulo BERT  está no slot errado.&#xA;Mova o PIM XFP para a Porta #1.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>Você selecionou um teste elétrico, mas o SFP selecionado parece ser um SFP óptico.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Assegure-e de estar usando um SFP elétrico.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>O Módulo BERT detectou um possível erro no aplicativo %1.&#xA;&#xA;Você selecionou um teste elétrico, mas o SFP parece um SFP óptico.  Substitua ou selecione outro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>Você selecionou um teste óptico, mas o SFP selecionado parece ser um SFP elétrico.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Assegure-e de estar usando um SFP óptico.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>O equipamento detectou um possível erro na aplicação %1.&#xA;&#xA;Você selecionou um teste óptico, mas o SFP aparenta ser&#xA; elétrico. Por favor, substitua ou selecione outro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>Você selecionou um teste de 10 G, mas o transceptor inserido não parece ser um SFP+. </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Assegure-e de estar usando um SFP+.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>O equipamento detectou um possível erro. Esse teste necessita de um SFP+, mas o transceiver não aparenta ser um desse tipo. Por favor, substitua por um SFP+.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Configurações do relatório automático</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Substituir o arquivo, sobregravando</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoReport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Aviso:    A unidade de disco selecionada está cheia. Você pode liberar espaço manualmente, ou permita que os 5 relatórios mais antigos sejam&#xA;automaticamente excluídos.</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Ativar relatórios automáticos</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Período de emissão do relatório</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Mín.:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Máx.:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Reinicie o teste após criação de relatório</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Criar um arquivo separado</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Nome do relatório</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>A data e a hora de criação serão automaticamente acrescentadas ao nome</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Texto</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Os relatórios automáticos não suportados no modo de leitura</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>Os relatórios automáticos serão salvos no disco rígido.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>Os relatórios automáticos necessitam de um disco rígido.  Não parece que esta unidade possua um instalado.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Relatório automáticos não podem ser habilitados quando um script automático está sendo executado.</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Criação de um relatório automático</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>Preparação ...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Criação </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Excluindo relatório anterior ...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Terminado.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** POTÊNCIA INSUFICIENTE.  DES-SELECIONE UM OUTRO MÓDULO ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Conexão serial bem-sucedida</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Aplicativo verificando existência de atualizações</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Aplicativo pronto para comunicações</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ERRO NA ATUALIZAÇÃO DO KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Certifique-se de que a segurança Ethernet = Padrão e/ou reinstale o software BERT *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERRO NA ATUALIZAÇÃO DO APLICATIVO.  Reinstale o software do módulo ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** ERRO NA ATUALIZAÇÃO. POTÊNCIA INSUFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Erro de inicialização: Desative o módulo BERT e, em seguida, reative-o ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Visão</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Relatórios</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Ferramentas</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Criar relatório...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Relatório automático...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Iniciar Teste</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Parar Teste</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Personalizar...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Modo de Acesso...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Restaurar os padrões do teste</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Limpar histórico</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Executar Scripts...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>Emulação VT100</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Configurações do modem...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Restaurar o layout padrão</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Janelas de resultados</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Único</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Dividida esquerda/direita</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Dividida em cima/em baixo</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>Grade 2 x 2</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>Ligar na parte inferior</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>Ligar na parte da esquerda</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Exibir somente resultados</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>Status do teste</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Painel de configuração</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Painel de ações</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Escolha o Script</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>Arquivos de script (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Selecionar&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Conexões de sinal</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Criar relatório</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Escolher&#xA;conteúdos</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Criar</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Exibir relatório após a criação</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erro - nome de arquivo não pode ser vazio.</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Escolher o conteúdo por</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Formatar</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome do arquivo</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Selecione...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Exibir relatório após a criação</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Incluir mensagem log</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Criar&#xA;relatório</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Visualizar&#xA;relatório</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erro - nome de arquivo não pode ser vazio.</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>O relatório foi salvo</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Atenue o sinal.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>O registro de eventos e o histograma estão cheios.</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>O registros K1/K2 estão cheios.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>O registro está cheio.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>O teste vai continuar sem registrar&#xA;itens adicionais desse tipo.  Reiniciar&#xA;o teste irá limpar todos os registros e histogramas.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Reinicialização&#xA;óptica</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Encerra o teste</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Sobrecarga no receptor</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>O registro Log está cheio</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Atenção</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Nenhum teste em execução</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Personalizar aparência e sensação da interface de usuário</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Salvar teste</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Salvar teste duplo</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Carregar teste</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carga</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Todos os arquivos (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Testes salvos (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Testes duplos salvos (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Carregar configuração</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Carregar teste duplo</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Importar teste salvo a partir do USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Todos os arquivos (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Configurações clássicas de teste RFC salvas (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Configurações do teste RFC salvas  (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>Configurações de teste FC salvos (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Configurações de TrueSpeed salvas (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Configurações de TrueSpeed VNF (*.vts) salvas</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Configurações salvas de SAMComplete (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>Configurações de SAMCompleto salvas (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Configurações de verificação OTN guardadas (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Configurações de auto-teste óticas salvas (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Configurações de verificação CRPI salvas (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Configurações de verificação CRPI salvas (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>Arquivos Zip salvos (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Exportar testes salvos para USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Todos os arquivos (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Testes múltiplos salvos pelo usuário</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Teste salvo pelo usuário</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>O controle remoto está sendo usado.&#xA;Esta operação não é permitida no modo de acesso somente leitura.&#xA; Use 'Use Tools->Access Mode', para permitir acesso total.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>As opções do módulo BERT expiraram.&#xA;Encerre o programa e inicie novamente o BERT, a partir da página do sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Ocultar</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Reinicilizar ambos os testes</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Reinicializar</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Ferramentas:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Ações</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Configurar</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Janela maximizada de resultados para o teste: </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Visão cheia</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Adicionar teste</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual do usuário</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Óptica recomendada</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Grade de frequências</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>O que é isto?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Salvar configuração de teste duplo como ...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Carregar configuração de teste duplo...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visão</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Alterar seleção do teste ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Ir para a visão total do teste</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Relatórios</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Criar relatório de teste duplo ...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Visualizar relatório ...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Ajuda</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Aviso</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Salvando arquivo</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Novo nome:</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Digitar um novo nome para o arquivo</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renomear</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>Impossível renomear, pois já existe um arquivo com esse nome.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Reinicializar</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Editar informações do usuário</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nenhum selecionado ...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Máximo de caracteres: </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Selecione o logotipo ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Visualização não disponível.</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagramas da ajuda </translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual do usuário</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Volta</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Início</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>À frente</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>QuickLaunch</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERRO NA ATUALIZAÇÃO JITTER.  Reinstale o software do módulo ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ERRO NA FUNÇÃO JITTER ÓPTICO. POTÊNCIA INSUFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Função jitter óptico em execução</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Perfis (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Selecione os perfis</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Deselecionar&#xA;Tudo</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Observação: carregar o perfil "Conectar" conectará o canal de comunicação com a unidade remota se necessário.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Excluir tudo</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Deletar</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Perfil Incompatível</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Perfis&#xA;de carga</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Tem certeza que quer excluir o %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Você tem certeza que deseja excluir todos os perfis %1?&#xA;&#xA;Esta operação não pode ser desfeita.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation> (Arquivos somente leitura não serão excluídos).</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>Falha ao carregar perfis de:</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Perfis carregados a partir de:</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Algumas configurações não foram carregadas adequadamente, pois não foram encontradas nesta aplicação.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Perfis carregados com sucesso de:</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Ativar teste duplo</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Aplicativos de indexação</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Opções de validação</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>Não existem testes disponíveis para o hardware ou as opções instalados.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>Não foi possível executar nenhum teste.  Verifique as opções instaladas e a configuração atual do hardware.</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Restaurando aplicativos em execução ao desligar</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Aplicativo em execução</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>Não foi possível montar o flash USB interno.Certifique-se o dispositivo está inserido ao lado da bateria. Uma vez inserido, reinicie o módulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>O sistema de arquivos do flash USB interno parece estar corrompido. Entre em contato com o suporte técnico para obter assistência.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>A capacidade interna do flash USB é menor do que o tamanho recomendado de 1G.&#xA;&#xA;Certifique-se o dispositivo está inserido ao lado da bateria. Uma vez inserido, reinicie o aparelho.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Ocorreu um erro ao restaurar as configurações.&#xA;Tente novamente.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Todos os testes em execução serão encerrados antes de carregar testes salvos.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Aguarde...&#xA;Carregando testes salvos</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>O nome do documento do teste ou o caminho do arquivo estão inválidos.&#xA;Use "Load Saved Test" (carregar teste salvo) para localizar o documento.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>Ocorreu um erro ao restaurar um teste.&#xA;Tente novamente.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Carregador de bateria ativado.</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Observe que o carregador de bateria não estará ativado neste modo. O carregador será automaticamente ativado quando forem selecionados testes adequados.</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>O controle remoto está sendo usado neste módulo e&#xA;o monitor foi desativado.</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Mensagens registradas. Clique para ver...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mensagem log para  %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>Sem mensagens</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 mensagem</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 mensagens</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mensagem log para  %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro de mensagens</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Configurações do modem</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Selecione um IP para o endereço deste servidor e um IP a ser alocado para o cliente de discagem de entrada</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>IP do servidor</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>IP do cliente</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Localização atual (Código do país)</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>Modem não detectado. Assegure-se de que o modem está corretamente conectado e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>O dispositivo está ocupado. Desconecte todas as sessões de discagem de entrada e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>Impssível atualizar o modem. Desconecte todas as sessões de discagem de entrada e tente novamente.</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Reiniciando teste(s) devido a mudança de tempo.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Informe abaixo a nova chave de opção para efetuar a instalação.</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Tecla de opção</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Instalar</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importar</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Entre em contato com a Viavi para adquirir opcionais do software.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>Opção solicita ID:</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Chave aceita! Reinicie para ativar a nova opção.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Chave rejeitada - Os slots de opção de expiração estão cheios.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Chave rejeitada - A opção de expiração já está instalada.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Chave inválida - Tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%1 de %2 Chave(s) Aceita(s)! Reiniciar para ativar nova(s) opção(ões).</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>Incapaz de abrir '%1' em movimentação instantânea USB (flash drive).</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>Ocorreu um problema ao obter as informações de opções...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte selecionado</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Padrões</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Legenda:</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI (Não usado)</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>Sobre o módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Módulo de transporte</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importar Um Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importar Um Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> Ocultar Menu</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Todos os testes</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizar</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Um, ou mais, dos screenshots selecionados foram capturados antes do início&#xA;do teste atual. Certifique-se de ter escolhido o arquivo correto.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Selecionar&#xA;Tudo</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Deselecionar&#xA;Tudo</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Selecionar&#xA;Cópias de tela</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Deselecionar&#xA;Cópias de tela</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibração RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrar...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>Calibração não está completa  A calibração é necessária para usar RS-FEC.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(Calibração pode ser executada a partir da guia RS-FEC nas páginas de configuração.)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Tentar novamente Calibração</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Deixar Calibração</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Calibração incompleta</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC 100GigE é tipicamente usada com SR4, PSM4, CWDM4</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Para executar a calibragem RS-FEC, faça o seguinte (também se aplica a CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserir um adaptador QSFP28 no CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserir um transceptor QSFP28 no adaptador</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserir um dispositivo de fibra de enlace de retorno para o transceptor</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Clique em Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Ressinc.</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Agora deve voltar a sincronizar o transceptor para o dispositivo em teste (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Remover o dispositivo de enlace de retorno da fibra do transceptor</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Estabelecer uma conexão com o dispositivo em teste (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Ligue a laser de DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verifique se o sinal presente LED em seu CSAM está verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Clique em Ressinc. Rota</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Ressinc. completa.  O diálogo pode agora ser fechado.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Salvar como somente leitura</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>Pin para lista de teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome do arquivo</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Selecione...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Salvar como somente leitura</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Salvar&#xA;perfis</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Perfis (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Perfis </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation> é um arquivo apenas para leitura. Não pode ser substituído.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Perfis salvos</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Selecionar logotipo</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Arquivos de imagem (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>Arquivo grande demais. Selecione outro arquivo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte selecionado</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Padrões</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Inicia&#xA;o teste</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Parar&#xA;Teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Executando</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Acesso total</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Somente leitura</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Escuro</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Claro</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Início rápido</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Exibição de resultados</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Vazão TCP total estimada (Mbps) com base em:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Largura máxima de banda disponível (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Atraso médio de retorno do sinal (Round Trip Delay) (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Número de sessões TCP paralelas necessárias para alcançar máxima vazão:</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Bit Rate</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Tamanho da janela</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Sessões</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Vazão TCP total estimada (kbps) com base em:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Largura máxima de banda disponível (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Vazão TCP</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>Ocorreu um erro ao salvar as configurações de teste.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>Ocorreu um erro ao carregar as configurações de teste.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>O disco selecionado está cheio.&#xA;Remova alguns arquivos e tente salvar novamente.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>Os seguintes arquivos de resultados personalizados de categorias diferem dos atualmente carregados:</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 outros</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>Sobrescrevê-los poderá afetar outros testes.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>Continuar sobrescrevendo?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Espere...&#xA;Restaurando configuração...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Recursos insuficientes para carregar o teste %1, neste momento.&#xA;Consulte o menu Teste para obter uma lista de testes disponíveis para a atual configuração.</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Impossível restaurar todas as configurações do teste.&#xA;O conteúdo do arquivo pode ser antigo ou estar corrompido.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>Modo de acesso é somente leitura. Para alterar, use 'Tools->Access Mode'</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Nenhum teste em execução</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>Isso restaurará todos os setups para o padrão.&#xA;&#xA;Continua?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Isso irá fechar e reiniciar o teste.&#xA;As configurações do teste serão restauradas para o padrão.&#xA;&#xA;Continua?</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Este fluxo de trabalho está sendo executado. Você quer terminar e começar o novo?&#xA;&#xA;Clique em Cancelar para continuar rodando o fluxo de trabalho anterior.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>Deseja terminar esse fluxo de trabalho?&#xA;&#xA;Clique em Cancelar para continuar rodando.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>Incapaz de lançar o VT100. Dispositivo serial já está sendo usado.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Módulo </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>Observe que, pressionar "Restart" irá limpar os resultados em *ambas* as portas.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Escolha&#xA;o Teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Carregar teste...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Salvar teste como...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Carregar apenas configurações...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Adicionar teste</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Remover teste</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Ajuda</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagramas da ajuda </translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Visualizar relatório...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Exportar Relatório...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Editar informações do usuário...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Importar Relatório Logo...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importar da porta USB...</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Teste salvo...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Categoria personalizada salva...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Exportar via USB</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Screenshot (cópia da tela)...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Dados cronograma...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Revisar/instalar opções...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Fazer cópia da tela...</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual do usuário</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Óptica recomendada</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Grade de frequências</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Conexões de sinal</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>O que é isto?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Quick Cards (cartões rápidos)</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Aguarde...&#xA;Removendo o teste</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Porta 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Porta 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nenhum selecionado ...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Selecionar arquivo...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Importar captura de pacotes de uma porta USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Selecionar&#xA;Arquivo</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Captura de pacote salva (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizar relatório</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visão</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Ver este&#xA;relatório</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Visualizar Outro&#xA;Relatórios</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>Renomear&#xA;relatório</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> já existe.&#xA;Você quer substituí-lo?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Relatório renomeado</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erro - nome de arquivo não pode ser vazio.</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>Um relatório foi salvo como</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>Nenhum relatório foi salvo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vá</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>Salvar dados TIE...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Salvar como tipo: </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>Arquivo HRD</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>Arquivo CHRD</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Gravar / salvar </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>Isso pode demorar vários minutos...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Erro - Incapaz de abrir arquivo HRD. Por favor, tente salvar novamente.</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Cancelando...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>Dados TIE salvos.</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Erro - Arquivo não pode ser salvo. Por favor, teste novamente.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Aviso</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Ao fechar a análise Wander, todos os resultados das análises serão perdidos.&#xA; Para continuar a análise, clique em Continuar a análise</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Fechar a análise</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Continuar a análise</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Análise Wander</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Atualizar&#xA;os dados TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>Parar&#xA;atualização TIE</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Calcular&#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Parar&#xA;cálculo</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Capturar &#xA; tela</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Carregar&#xA; Dados TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>Parar carregamento&#xA; TIE</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Fechar&#xA;análise</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Carregar dados TIE</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carregar</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Todos os aquivos Wander (*.chrd *.hrd);;Hrd files (*.hrd);; arquivos Chrd (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Toque duas vezes para definir o retângulo</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Excluir tudo</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Deletar</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Carregar perfil</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Tem certeza que quer excluir o %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Você tem certeza que deseja excluir todos os perfis %1?&#xA;&#xA;Esta operação não pode ser desfeita.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation> (Arquivos somente leitura não serão excluídos).</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 Perfis (*%2 e %3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>Não foi possível carregar o perfil.</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Carga falhou</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Seguinte</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensagem</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erro</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vá</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Aviso</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>O que você deseja fazer em seguida?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>Você tem certeza que deseja sair?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Restaurar as configurações ao sair</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Ir para Results</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Volta</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Aso a passo:</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Seguinte</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Guie-me</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Outra porta</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Começar de novo</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Vá até...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Outra porta</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>O teste stá começando, aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Encerrando o teste, aguarde...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro de mensagens</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Principal</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Exibir etapas</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Resposta: </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Executando</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nenhum selecionado ...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo do relatório</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Selecione o logotipo ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Visualização não disponível.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>O teste stá começando, aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Encerrando o teste, aguarde...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>Teste em andamento ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Tempo restante:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Nenhum teste em execução</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Teste incompleto</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>DESLIGANDO relatórios automáticos antes de iniciar o Script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>LIGANDO relatórios automáticos anteriormente desativados</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Principal</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Cópia de tela salva</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Cópia de tela salva:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Seleção de perfil</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Camada operacional</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Carregar um perfil salvo</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>Como você gostaria de configurar o TrueSAM?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Carregar configurações de um perfil salvo</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Vá</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Iniciar um novo perfil</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>Em que camada o seu serviço opera?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Camada 2: Teste usando endereços MAC, por exemplo, 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Camada 3: Teste usando endereços de IP, por exemplo, 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Favor aguardar... indo para o passo destacado.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuração</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Selecione os testes</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Estabelecer comunicação</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Configurar RFC 2544 melhorado</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Configurar SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Configurar J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Configurar TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Salvar Configuração</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Adicionar Informações do Relatório</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>Executar Testes Selecionados</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizar relatório</translation>
        </message>
    </context>
</TS>
