<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Foi recusada a designação de endereço IP inválido.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>Atribuição de Gateway inválida: 172.29.0.7. Reinicie o sistema MTS.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>O endereço IP mudou.  Reiniciar o módulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>Reiniciando o módulo BERT.  A solicitação de desligamento foi rejeitada.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>O módulo BERT está iniciando com a função de jitter óptico. A solicitação de desligamento foi rejeitada.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>Desligando o módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>O módulo BERT está DESLIGADO</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>O módulo BERT está desligado. Pressione HOME/SYSTEM, depois o ícone BERT para ativá-lo.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>O módulo BERT está LIGADO</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** ATUALIZANDO SOFTWARE APLICATIVO *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** ATUALIZAÇÃO COMPLETADA *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** ATUALIZANDO SOFTWARE DE JITTER *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** ATUALIZANDO SOFTWARE DO KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** INICIANDO A ATUALIZAÇÃO *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>Falha na IU do módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Iniciando a IU do módulo BERT com a função de jitter óptico</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>Iniciando a IU do Módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>Falha na IU do módulo BERT. Módulo DESLIGADO</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Módulo 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Módulo 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Módulo 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Módulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Módulo </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Módulo 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Módulo 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Módulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Módulo 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Módulo 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Módulo 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Módulo 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Módulo 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Módulo 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Módulo 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Módulo 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Módulo 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Módulo 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Módulo 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Módulo 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Módulo 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Módulo 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Módulo 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Atualização necessária para o software de base. A revisão atual não é compatível com o módulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>Reinstalação do software BERT necessária. O software BERT apropriado não está instalado no Módulo BERT em anexo.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>Atualização necessária para o hardware BERT. O hardware atual não é compatível.</translation>
        </message>
    </context>
</TS>
