<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> 确认需要取消吗？</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>读文件错误</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>文件中桢数量太大（多于 50000 ）。请试试存储部分缓冲， &#xA; 或者输出到 U 盘，再用其他的设备加载</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>您要删除所有存储的数据吗？</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>您要删除选定的项目吗？</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>名称已经存在。&#xA;您要覆盖旧数据吗？</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>图表遇到写入硬盘错误。请马上保存图表保留工作。达到临界水平时图表会停止并自动清除。</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>图表遇到过多错误无法继续，已经停止。&#xA;已清除图表数据。 如果需要可以重启图表。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>可卸载设备</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 毫秒</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 毫秒</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 毫秒</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>记录已经满了</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>无效配置:&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>无效</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>语音</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>数据 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>数据 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>软件支持的 CFP MSA 管理 I/F 规范修订</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>软件支持的 QSFP MSA 管理 I/F 规范修订</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>DMC 信息</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>条码</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>生产日期</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>校准日期</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>软件修订版</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>选件 Challenge ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>序列号</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>组装条码</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>已安装的软件</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>选件:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>等待数据包 ...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>正在分析数据包 ...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>等待链路 ...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>按“开始分析”，开始 ...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>正在保存结果，请稍候。</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>无法保存结果。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>结果已保存。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>等待启动</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>上一个测试由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>上一个测试发生错误而中止</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>上一个测试失败且已启用“故障时停止”</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>未运行</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>剩余时间: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>测试发生错误而中止，未能完成。</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>故障</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>测试完成，部分结果失败</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>测试完成，所有结果都通过</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>测试已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>停止中</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>测试没有运行</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>启动中</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>已验证连接</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>连接中断</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>正在验证连接</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>无效配置</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>正在保存结果，请稍候。</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>无法保存结果。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>结果已保存。</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>上游吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>上游吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>下游吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>下游吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>异常</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>上游异常</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>下游异常</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>上游延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>上游延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>下游延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>下游延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>上游抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>上游抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>下游抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>下游抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 字节丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 字节丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 字节上游丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 字节上游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 字节下游丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 字节下游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>上游 CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>下游 CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>上游 CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>下游 CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>上游突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>下游突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>背靠背测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>上游紧接测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>下游紧接测试结果</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>上游系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>上游系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>下游系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>下游系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>扩展负载测试结果</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>路径 MTU 步骤</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>RTT 步骤</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>上游 Walk the Window 步骤</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>下游 Walk the Window 步骤</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>上游 TCP 吞吐量步骤</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>下游 TCP 吞吐量步骤</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>正在保存结果，请稍候。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>无法保存结果。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>结果已保存。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>无法获取同步。 请确保所有电缆均已连接并且端口的配置正确。</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>找不到活动链路。 请确保所有电缆均已连接并且端口的配置正确。</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>无法确定连接端口的速度或双工模式。 请确保所有电缆均已连接并且端口的配置正确。</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>本地测试设备无法取得IP地址。 请检查通信设置并重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>无法建立到远程测试设备的通信信道。 请检查通信设置并重试。</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>远程测试设备的BERT软件版本似乎不兼容。 兼容的最低版本为%1。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>要运行TrueSpeed，本地设备和远程设备上必须能够使用第4层TCP Wirespeed应用</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>TrueSpeed测试选项对于此封装设置无效，将被删除。</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>TrueSpeed 测试选项对于此帧类型设置无效，将被删除。</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>TrueSpeed测试选项对于远程封装无效，将被删除。</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>SAM-Complete 测试选项对于此封装设置无效，将被删除。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>无效配置</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>CBS 管制</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>策略</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>步长 #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>步长 #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>步长 #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>步长 #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>步长 #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>步长 #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>步长 #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>步长 #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>步长 #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>步长 #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>正在保存结果，请稍候。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>无法保存结果。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>结果已保存。</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>本地ARP失败。</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>远程ARP失败。</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation>表格，内容</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>截屏</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - 柱状图分布在几页</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>时间单位</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>用户信息</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>配置组</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>结果组</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>事件记录器</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>柱状图</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>截屏</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>估计的 TCP 吞吐量</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> 多次测试报告</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> 测试报告</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>由 Viavi 测试仪器生成 </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> 通过</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>扫描频率 (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>通过 / 失败</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>设置： 过滤器 /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> （模式和掩码）</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>模板</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>没有可用设置 ...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI 检查</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>无线电</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>波段</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>CPRI 测试整体测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>测试未完成</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>进行中</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>停止时间</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>否.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>事件名称</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>持续时间 / 值</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>扩展负载测试事件日志</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>未知 - 状态</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>进行中，请等待 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>故障 !</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>命令已完成</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>建立环路</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>拆除环路</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Arm</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>消除</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>关机</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>发送环回指令</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>开关</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>交换机重置</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>问题查询</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>环回查询</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>近端 Arm</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>近端 Disarm</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>电量查询</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>链路查询</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>超时禁用</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>超时复位</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>序列环回</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Stratum 1 Trace</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 保留的</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 保留的</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 发送节点时钟踪迹</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 保留的</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 保留的</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Stratum 2 Trace</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 保留的</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 保留的</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Stratum 3 Trace</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 保留的</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet 最小时钟踪迹</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Stratum 3E Trace</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 由运营商提供</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 不要用来同步</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>未指明的装备。</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>TUG 结构</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>锁定 TU</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>异步 34M/45M</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>异步 140M</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>ATM 映射</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>MAN (DQDB) 映射</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>FDDI 映射</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>HDLC/PPP 映射</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 未译出</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>O.181 测试信号</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>异步的</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>比特同步</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>字节同步</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>保留的</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT- 结构 STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>锁定 VT 模式</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>异步 DS3 映射</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>异步 DS4NA 映射</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>异步 FDDI 映射</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>格式化 ?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>超出范围</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>历史</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation>历史</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>空间</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>标记</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>绿色</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>黄色</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>红色</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>全部</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>拒绝</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>不确认</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>接受</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>测试未完成</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>状态未知</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>已识别</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>无法识别</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>未知身份</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>剩下%1小时%2分</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>剩下%1分钟</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>太低</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>太高</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>( 太低 ) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>( 太高 ) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>数值</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>辅助端口</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>未定义</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>毫秒</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未准备</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>映射正在发展中</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC over SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>简单数据链路映射 (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>HCLC/LAP-S 映射</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>简单数据链路映射 ( 复位 - 再复位 )</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>10 Gbit/s 以太网帧映射</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>GFP 映射</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>10 Gbit/s 光纤通道映射</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>保留的 - 专有的</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>保留的 - 国家的</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>测试信号 O.181 映射</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>保留的 (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>未指明的装备</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>异步 DS3 映射</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>异步 DS4NA 映射</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>异步 FDDI 映射</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC Over SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 VT 净荷缺陷</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI 未分配</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>等待 . 电话</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>估计等待 TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI 已分配</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>等待 . 估计。</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>等待 . 关系 .</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>多帧估计</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>计时器恢复</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>连接未知</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>等待建立</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>已建立多帧</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>挂机</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>拨号音</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>整体拨号</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>响铃</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>已连接</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>呼叫释放</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>语音</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>数据</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>传真 G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>智能用户电报</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>可视图文</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>语音 BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>数据 BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>数据 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>传真 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>正在搜索</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>可获得的</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>加入请求</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>重试请求</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>许可</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>确认突发完成</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>加入响应</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>突发完成</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>状态响应</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>了解数据流中的孔</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>差错</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>错误： 服务尚未缓冲</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>错误：重试数据包请求无效</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>错误：无此服务</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>错误：无此部分</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>错误：会话错误</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>错误：不支持的命令和控制版本</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>错误：服务器已满</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>错误：重复加入</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>错误：重复会话 ID</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>错误：错误位速率</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>错误：会话被服务器销毁</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>短</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>正常</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>相反的</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>级别太低</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 字节</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 字节</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>已选择</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>未选择…</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>差错帧</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 帧</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>丢帧</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>编码违例</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>事件日志已满。</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>不可获得</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>未找到 USB 密匙。 请插入密匙并重试。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>没有帮助信息</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>设备 Id</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>无信号</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>信号</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>就绪</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>使用的</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>卫星 ID</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>GNSS ID</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>成帧</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>模板</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>MTIE 屏蔽</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>TDEV 屏蔽</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>MTIE 结果</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>MTIE 模板</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>TDEV 结果</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>TDEV 模板</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Orig. TIE 数据</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>偏移消除数据</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>MTIE/TDEV 曲线类型</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>线 + 点</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>只打点</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>模板类型</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE 通过</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>MTIE 失败</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV 通过</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>TDEV 失败</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>观察区间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>正在计算</translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>计算已取消</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>计算已完成</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>正在更新 TIE 数据</translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>TIE 数据载入</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>没有 TIE 数据加载</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>运行漂移分析内存不足。 &#xA; 需要 256 MB 的 RAM 。使用外部分析软件。 &#xA; 请参加用户手册。</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>频偏 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>漂移速率 (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>抽样</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>样本速率（每秒）</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>数据块</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>当前值块</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>消除偏移</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>曲线选择</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>所有曲线</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>偏移消除</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>抓屏</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>本地</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>远端</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>未添加标签</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>端口 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>端口 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>工具条</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>信息记录</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>一般信息：</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>禁用图形</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>打印错误</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>节目流量 #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps, 最小值</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps, 最大值</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR 抖动</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR 抖动最大值</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC 错误</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT 错误</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID 错误</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 错误最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># 数据流</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>传输流 ID</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>当前 RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>丢包全部</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>当前丢包值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>丢包峰顶值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>包抖动 ( 毫秒 )</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>包抖动最大值 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Oos 包总计</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Oos 包当前值</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Oos 包最大值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Distance 错误全部值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Distance 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>最大 Distance 错误</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Period 错误总计</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Period 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Period 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>最大丢失时期</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>最小丢失距离</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>同步损失总数</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>同步字节错误总数</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>当前同步字节错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同步字节错误最大值</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 当前值</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 最大值</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 当前值</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 最大值</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>运输错误总数</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>当前的传输错误</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>传输错误 最大值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 错误最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># 数据流分析</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>总 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>传输流 ID</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>节目号</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>当前节目流量 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>最小节目流量 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>最大节目流量 Mbps</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>当前 RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>丢包全部</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>当前丢包值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>丢包峰顶值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>包抖动 ( 毫秒 )</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>包抖动最大值 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS 数据包传输时间</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Oos 包当前值</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Oos 包最大值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Distance 错误全部值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Distance 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>最大 Distance 错误</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Period 错误总计</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Period 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Period 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>最大丢失时期</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>最小丢失距离 </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>同步损失总数</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>同步字节错误总数</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>当前同步字节错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同步字节错误最大值</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 当前值</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 最大值</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 当前值</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 最大值</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>PCR 抖动当前值</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR 抖动最大值</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>CC 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>运输错误总数</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>当前的传输错误</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>传输错误 最大值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>PMT 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID 错误所有值</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>PID 错误当前值</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 错误最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>丢包</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>最大丢包值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>包抖动</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>包抖动最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># 数据流</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Mbps, 当前 L1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>节目流量 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>最小节目流量 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>传输 ID</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>节目流量 #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>同步丢失</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>全部同步字节错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>同步字节错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同步字节错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>全部 PAT 错误</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>PAT 错误</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR 抖动</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>PCR 抖动最大值</translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>全部 CC 错误</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC 错误</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>全部 PMT 错误</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT 错误</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>全部 PID 错误</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID 错误</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 错误最大值</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>全部传输错误</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>传输错误</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>传输错误 最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># 数据流分析</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>总 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP 校验和错误</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>MPEG 历史</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>当前 RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>当前丢包值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>丢包全部</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>丢包峰顶值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>包抖动当前值</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>包抖动最大值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>峰峰值</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>POS 峰值</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>阴性峰值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>峰峰值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>POS 峰值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>负峰值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>测试报告信息</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>码</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>桥接</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>未使用</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>光学整体测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>光学自测</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>详细</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>堆叠</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>堆叠 VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN 堆栈深度</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>SVLAN %1 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>SVLAN %1 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>用户 SVLAN %1 TPID （ hex ） </translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>帧未定义</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>数值</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>辅助端口</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>OTN 检查测试</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>OTN 检查整体测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN 检查</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>测试未完成</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>POH 字节捕获</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>16进制</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>二进制</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>没有可用设置 ...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>净荷字节</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>CDMA Rx 机</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>打印错误</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP 测试</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>已加载配置文件</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>PTP全部测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP 检查</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>测试未完成</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>所有结果 OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>增强的 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>增强型 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>全部测试结果</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>整体测试结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>环路时延</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>突发</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>系统恢复</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>缓冲器信用量</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>缓冲器信用量吞吐</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>扩展负载</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>未运行 - 上一个测试由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>未运行 - 上一个测试发生错误而中止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>未运行 - 上一个测试失败且已启用“故障时停止”</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>未运行</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>对称回路</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>对称上游和下游</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>非对称上游和下游</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>上游</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>下游</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>环路时延</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>包抖动</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>突发 (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>背靠背</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>系统恢复</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>扩展负载</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>缓冲器信用量</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>缓冲器信用量吞吐</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>运行的测试</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>所选帧长 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>所选数据包长度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>所选上游帧长 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>所选上游数据包长度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>所选下游帧长 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>所选下游数据包长度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>下游帧长 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 字节丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 字节上游丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 字节下游丢帧测试图表</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 字节缓存点吞吐量测试图表</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>停止时间</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>最长的</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>最短的</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>最后的</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>终端</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>总计</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>连接器</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>标称波长 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>标称比特率 (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>波长 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>最小值比特率 (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>最大值比特速率 (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>功率电平类型</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>厂商</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>厂商 PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>厂商版本</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>最高 Rx 电平（ dBm ）</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>最高 Tx 电平（ dBm ）</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>厂商 SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>日期代码</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>批次码</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>诊断监测</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>诊断字节</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>收发信机</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>硬件/软件版本#</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA硬件规范版本号</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA管理I/F版本号</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>功率级别</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx 功率电平类型</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>最大 Lambda 功率 (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>活动光纤数</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>每一光纤波长</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>每一光纤波长范围 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>最大网络线路位速率 (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>支持的额定值</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>持续时间 / 期间</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>无效</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>帧延迟 (RTD，ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>帧延迟（ OWD ， ms ）</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>单向延迟</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>上游</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>下游</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>行</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed测试</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>开通</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>诊断</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>对称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非对称</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>吞吐量对称</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>通道 MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>移动窗口</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>高级 TCP</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>运行步骤</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>未运行 - 上一个测试由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>未运行 - 上一个测试发生错误而中止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>未运行 - 上一个测试失败且已启用“故障时停止”</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>未运行</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF 测试</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>测试未完成</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>用户中止了测试。</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>未开始测试。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>上游通过</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>吞吐量高于目标的 90%。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>上游失败</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>吞吐量低于目标的 90%。</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>下游通过</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>下游失败</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>测试报告信息</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>工程师姓名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>公司</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>电子邮件</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>电话</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>测试标识</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>测试名称</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>验证码</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>验证创建日期</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>测试停止时间</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>注释</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>停止时间</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>跟踪</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>序列</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>记录：</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>数据流 IP ：端口</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>数据流名称</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>停止时间</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>节目名称</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>进行中</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>VLAN 扫描整体测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>设置 :</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>结果</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>TrueSAM全部测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>测试发生错误而中止，未能完成。 报告中的结果可能不完整。</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>测试已由用户停止。 报告中的结果可能不完整。</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>子测试结果</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>J-QuickCheck测试结果</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>RFC 2544 测试结果</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>SAMComplete测试结果</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>J-Proof测试结果</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>TrueSpeed测试结果</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>未运行 - 上一个测试由用户停止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>未运行 - 上一个测试发生错误而中止</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>未运行 - 上一个测试失败且已启用“故障时停止”</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>未运行</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>信息记录</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>报文日志 ( 持续 )</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>工作顺序</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>备注 / 注意</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>仪器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>SAMComplete全部测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>整体配置测试结果</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>整体性能测试结果</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>延迟变化</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>报告组</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>无法创建报告： </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>磁盘空间不足。</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>不能创建报告</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>端口设置</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>奇</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>偶</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>波特率</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>奇偶</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>流控</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>硬件</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>数据位</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>停止位</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>终端设置</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>回车键</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>本地回显</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>启用保留键</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>无效键</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12 ， Ctrl+U ， Ctrl+Y ， Ctrl+P ， Ctrl+M ， Ctrl+R ， Ctrl+F ， Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>禁止</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>启用</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>导出、文件、设置、结果、脚本、开始 / 停止、面板软键</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>PRINT ， FILE ， SETUP ， RESULTS ， SCRIPT ， START/STOP ，面板功能鍵</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FILE ， SETUP ， RESULTS ， SCRIPT ， START/STOP ，面板功能鍵</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>文件、设置、结果、导出、开始 / 停止、面板软件</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>终端 &#xA; 窗口</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>恢复默认值</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100&#xA; 设置</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>清除 &#xA; 屏幕</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>显示 &#xA; 键盘</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>移动 &#xA; 键盘</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>自动波特</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>截取 &#xA; 屏幕</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>隐藏 &#xA; 键盘</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>循环过程</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>发生意外错误</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>运行脚本</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;这将重新扫描链路数据流重启测试。&#xA;&#xA; 从资源浏览器应用程序传输的数据流将会消失。&#xA;~继续？&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>自动过程</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>细节 :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>自动操作正在执行中 . 请等待 ...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>自动已失败。</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>自动已完成。</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>自动已中止。</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>未知 - 状态</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>正在检测</translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>正在扫描 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>故障</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>发生意外错误</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>已配对设备详细信息</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>发送文件</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>连接</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>忘记</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>选择文件</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>发送</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>正在连接...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>正在断开…</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>断开</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>数据束名称： </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>输入数据束名称：</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>证书和密匙 (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>无效数据束名称</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>名为 “ %1 ” 的数据束已经存在。 &#xA; 请选择其他名称。</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>添加文件</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>创建数据束</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>重命名数据束</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>修改数据束</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>打开文件夹</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>添加新数据束 ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>添加证书到 %1 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>删除 %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>从 %2 删除 %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation>比特</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>虚拟线路 ID</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>注入的偏移（位）</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>注入的偏移 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>物理线路 #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Range:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>无法设置日期</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> 数位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>支路时隙</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>应用</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>缺省 / 默认</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>带宽 </translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>更改尚未应用。</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>选择的支路时隙太多。</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>选择的支路时隙太少。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>其他的...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>字符</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 数位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>今天</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>输入日期：日 / 月 / 年</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>输入日期：日 / 月 / 年</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>多达 </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> 字节</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> 数位</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 数位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>分</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>小时</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>天</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>dd/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>时 : 分</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>持续时间/期间: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>在所有业务标签页中配置目的地址</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>在所有数据流标签页中配置目的地址</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>目的 MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>目的类型</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>环路类型</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>这一跳源 IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>下一个跳目的 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>下一个跳子网掩码</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation> 为所有帧配置源地址</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>源类型</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>缺省 MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>用户 MAC</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>在所有业务标签页中配置源地址</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>在所有数据流标签页中配置源地址</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>源 MAC</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Pbit 增量</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>用户 SVLAN TPID （ hex ） </translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>在环回模式下不可配置</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>指定 VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>用户优先级</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>长度</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>数据长度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>控制</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>以太网类型</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>类型 /&#xA; 长度</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>通道 &#xA; 标签</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>B-Tag VLAN ID 过滤</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>B-Tag 优先级</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>B-Tag DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>通道标签</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>通道优先级</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag 以太网类型</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>通道 TTL</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA; 标签</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>I-Tag 优先级</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>I-Tag DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>I-Tag UCA 比特</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>I-Tag Service ID 过滤</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag Service ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>VC 标签</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>VC 优先级</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag 以太网类型</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA; 标签</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>MPLS1 标签</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>MPLS1 优先级</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA; 标签</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>MPLS2 标签</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>MPLS2 优先级</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>数据</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>承载的用户帧</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>封装</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>帧类型</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>用户帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>用户帧长</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>尺寸过小</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>巨帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>数据部分包括 IP 包，在 IP 标签页上配置</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>在 IP 过滤器标签页中配置数据过滤</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Tx 净荷</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>RTD 设置</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>净荷分析</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Rx 净荷</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>LPAC 定时器</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Rx  BERT 码型</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Tx  BERT 码型</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>用户码型</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>配置输入帧 :</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>配置输出帧 :</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>长度 / 类型域是 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>随机数据长度</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>用户优先级开始指数</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>文件类型：</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>文件名 :</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>你确定你要删除 &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>你确认要删除该文件夹中的所有文件吗？</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> 数位</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>多达 </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>范围 :  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation>(hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 数位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> 是无效的 - 端口 1 和 &#xA;2 的源 IP 不匹配。恢复源 IP 。</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>无效 IP 地址 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>范围 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>所给的 IP 地址不适用于此设置。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>每个字节 2 个字符，最多 </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>每个字节 2 个字符， </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>循环码名称</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>比特码型</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>输出</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>带内</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>带外</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>建立环路</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>拆除环路</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>其他的</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>循环码名称</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 比特</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Del.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>添加 / 删除</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Tx 踪迹</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>范围 :  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>选择频道</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>输入 KLM 值</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>范围 :  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>选择 VCG 成员</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>PSI 字节</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>字节值</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>ODU 类型</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>辅助端口 #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>范围 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>选择 VCG 成员</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>范围 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>净荷字节编辑</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>选择一个有效的字节进行编辑。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>无法识别的光纤</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation> 正在创建页面。  请稍候 ...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>页面为空白。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>加入项目</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>更改项目</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>删除条目</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>加一行</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>更改行</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>删除行</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 未知踪迹</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未准备</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未准备</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未准备</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未准备</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>长度 : </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>字符</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>长度:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>字符</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>现在</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>输入时间：小时：分钟：秒</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>消选择所有</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>字节 %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation>比特</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>业务名</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>上游</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>下游</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>双向</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>业务类型</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>因为更改位帧长和 / 或 CIR ，业务类型已被重置为数据。</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>编码解码器</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>抽样速率 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># 呼叫数</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>包长度</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># 通道</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>压缩</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>创建 VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>使用缺省信道号定义 VCG 成员</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>定义 Tx VCG</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>定义 Rx VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>净荷带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>成员数量</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>分配 &#xA; 成员</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>分配 VCG 成员</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>定义 VCG 成员的客户分配</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>实例</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>步长</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>编辑 VCG 成员</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>地址格式</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>新成员</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>缺省 / 默认</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>加入的数据流已经被输入到浏览器。 &#xA; 按 [ 加入数据流 ...] 按键</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>地址薄</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>全不选</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>查找下一个</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>移除</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>全不选</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>加入数据流 ...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>加</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>源 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>输入的 IP 被加入到地址薄</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>加入</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>己经加入</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>加入</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>离开数据流</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>源 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>目的 IP 地址</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>离开数据流 ...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>消选择所有</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>快速配置</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>注意 : 这将覆盖当前帧配置</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>快速 (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>全 (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>强度</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>系列</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>封装</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Pbit 增量</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>用户优先级</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID （十六进制）  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>用户 TPID （十六进制）</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>应用</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>用户优先级开始指数</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 用户优先级</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>加载 ...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>长日期：</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>短日期：</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>长时间格式 :</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>短时间格式 :</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>编号：</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>未分配</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>已分配</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Reserved10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reserved11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reserved00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>保存 ...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>复位测试为缺省设置</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>配置 &#xA; 数据流 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>负载分配</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>全部清除</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>自动分发</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>数据流</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>帧速率</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>线速率 %</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>负载开始递增</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>突发</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>恒定的</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>最大利用率门限</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>总计 (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>总计 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>总计 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>总 (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>注意： </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>至少打开一个数据流</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>最大值利用率门限是</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>最大值可能负载是</translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>规定负载总计不能超过该值</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>输入百分比：</translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>输入帧率：  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>输入比特速率：</translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>注意： &#xA; 未检测到位速率。 检测到位速率时请按 &#xA; “取消”并重试。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>明确三重播放业务</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>编码解码器</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>抽样速率 &#xA;(毫秒)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># 呼叫数</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>每呼叫速率&#xA;（ Kbps ）</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>总计速率 &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>总基本帧尺寸&#xA;（字节）</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>静音抑制</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>抖动缓冲 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># 通道</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>压缩</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>总基本帧尺寸 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>开始速率（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>加载类型</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>时间步骤 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>负载步长 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>总 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>仿真</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>数据 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>数据 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>数据 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>数据 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>随机</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>语音</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>视频</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>数据</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>注意：</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>至少打开一个业务</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>最大值可能负载是</translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>规定负载总计不能超过该值</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>分配： </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>带宽 : </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>结构 : </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>缺省</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>步长</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>地址薄</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>新条目</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>源 IP 地址（ (0.0.0.0 = "任何地址"）)</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>必需的</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>名称（最长 16 个字符）</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>加入条目</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>输入 / 输出</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>输入</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>从 U 盘 驱动器输入条目</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>输出</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>输出到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>删除全部</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>存储并关闭</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>可选</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>从 U 盘 输入条目</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>输入条目</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation> 逗号分割 (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>添加条目</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>每一个条目名必须包括四部分 : &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> ，由逗号分割 .  跳行 .</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>连续</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>不是有效的源 IP 地址。跳行</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>不是有效的目的 IP 地址。跳行</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>不是有效的 PID 。跳行</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>条目必须有名字（最长 16 个字符）。跳行</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>条目名不能超过 16 个字符。跳行</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>一个有同样源 IP ，目的 IP 和 PID&#xA; 已经存在并且有名字</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>覆盖现有条目名 &#xA; 或 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>略过</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>改写</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>一个输入的条目有 PID 值 .&#xA; 有 PID 值的条目不仅仅应用于 MSTP.</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>输出条目到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>输出条目</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>IPTV 地址本</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>已经用了。选择一个不同的名字或编辑已存在的条目</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>带这些参数的条目已经存在</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>你确认你要删除所有条目吗？</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>源 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>添加条目</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>有同样名字的条目已经存在了</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>你要做什么</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>日期 :</translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>时间 :</translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多事件数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>打开</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>码</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>桥接</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多 K1/K2 字节数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>16进制</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>二进制</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>如要查看更多单向传输延迟事件的数据，请使用视图 -> 结果窗口 -> 单个菜单选择。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>源丢失</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>B1 错误</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>REI-L 错误</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>MS-REI 错误</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>B2 错误</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>REI-P 错误</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>B2 错误</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>HP-REI 错误</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>B3 错误</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>REI-V 错误</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>BIP-V 错误</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>B3 错误</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>LP-REI 错误</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>LP-BIP 错误</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>STL FAS 错误</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Bit/TSE 错误</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>信号丢失</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>帧同步丢失</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>误帧字</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>FAS 错误</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>最长的</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>最短的</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>最后的</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>终端</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多服务中断数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>总计</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD 号</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多服务中断数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD 号</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>持续时间 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>溢出</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多呼叫结果数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>拨号音</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  TRUE</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>持续时间 / 期间</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>无效</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>日期 :</translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>时间 :</translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>持续时间 (毫秒)/ 值</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>节目名称</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>持续时间 (毫秒)/ 值</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>数据流 IP ：端口</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>数据流名称</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>持续时间 (毫秒)/ 值</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>输出已保存的客户结果种类</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>已保存的客户结果种类</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>输出</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>无法找到 U 盘 .&#xA; 请插入 , 或拔出重新插入 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>导出 PTP 数据到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>PTP 数据文件 (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>导出报表到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>文本 (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>导出屏幕截图到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>保存屏幕截图 (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>将选中的文件解压为：</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>输入文件名：最大 60 个字符</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>压缩&#xA;&amp;&amp;导出</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>输出</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>请输入 Zip 文件的名称</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>无法找到 U 盘 .&#xA; 请插入 , 或拔出重新插入 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>无法压缩文件</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>导出 TIE 数据到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Wander TIE 数据文件 (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>将定时数据导出至 USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>所有定时数据文件 (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>文件类型：:</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>打开</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>从 U 盘 导入保存的自定义结果类别</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>已保存的客户结果种类</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>输入 &#xA; 客户种类</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>从 U 盘 导入报表标志</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>图像文件 (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>输入标志</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>导入 USB 的快速卡</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Pdf 文件 (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>导入 &#xA; 快速卡</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>输入 &#xA; 测试</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>解压缩&#xA; &amp;&amp;导入</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>错误 - 无法解压其中一个文件。</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>目标设备上未用空间不足。 &#xA; 复制操作已取消。</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>复制文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>已完成。 文件已复制。</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>错误： 复制以下项目失败： &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>保存 PTP 数据</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>PTP 文件 (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>复制</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>磁盘空间不足。删除其他已保存的文件或导出至 USB。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%2的%1的可用空间</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>未选中文件</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>所选文件中总计 %1</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>在 %2 所选文件中总计 %1 </translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>文件名 :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>输入文件名：最大 60 个字符</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>删除文件夹中的所有文件 ?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（只读文件不会被删除。）</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>正在删除文件……</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>你确定你要删除 &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>你确定你要删除它吗？</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>不是推荐的光学元件</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>以太网</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>光纤通道</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>显示信号结构的发现和扫描结果。</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>排列：</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>排序</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>重新排列</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>显示可用于分析所选通道的一些测试</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>开始测试</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>请等待。正在配置所选通道……</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>显示详细信息</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>隐藏详细信息</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>范围：%1到%2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>不可获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>查找</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>原</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>按宽度</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>按高度</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>双重测试视图选择</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>更多...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>（续）</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>警告</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>日志缓冲区已满</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>捕捉停止</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>编辑行</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>选择字节：</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>无法截屏</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>磁盘空间不足</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>所抓截屏 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>关于数据流</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>DP 拨号</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>MF 拨号</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>DTMF 拨号</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>保留的</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>单向</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>双向的</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>空闲</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>特大流量</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>总计</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>没有可用的</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>不能转移到外部连接</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>没有发现</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>已到页尾，从头查找</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>选择工具</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>开始脚本前关闭自动报告</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>打开之前关闭的自动报告</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>创建</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>缺省 / 默认</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>删除全部</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>发送</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>重试</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>浏览</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS ( 流入 )</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS ( 流出 )</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>容器</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>信号标签</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>跟踪</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>踪迹格式</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>未知</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>仅表示目前状态，不考虑任何低阶或高阶通道。只有目前选中的通道得到实时更新。</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>图标所示通道的状态。</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>无监测告警</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>有告警</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>监测 w ／无监测告警</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>监测 w ／有告警</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>信道容器名称。 “c”前缀表示级联信道。</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>RFC 4606 所规定的 N KLM 通道数</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>信道信号标记</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>最后所知道的通道状态。</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>通道无效。</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>当前 RDI</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>当前 AIS</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>当前 LOP</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>监测</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>状态更新于：</translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>从不</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>信道跟踪信息</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>信道跟踪格式信息</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>不支持</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>正在扫描</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>告警</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>无效</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>无格式的</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>单字节</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>CR/LF 终端的</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>配对设备</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>已发现的设备</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>CA 证书</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>客户端证书</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>客户端密匙</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>格式化</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>剩余空间</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>总容量</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>厂商</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>标识</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>升级版本</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>安装版本</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>种类</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>列出客户种类名。点击种类名将启用／禁用该客户种类。</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>点击可以配置客户种类</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>可以切换种类删除客户种类。</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>客户种类名。点击种类名将启用／禁用该客户种类。</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>点击配置图标启动客户种类配置对话框。</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>点击删除图标进行标记或者不标记要删除的客户种类。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>创建报告</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>文本 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>创建</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>格式化 :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>创建以后阅读报告</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>错误 - 文件名不能为空白。</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>修改设置将刷新当前结果。</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名称</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>未在子网内</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>等待链路工作 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>等待 DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>重置源 IP...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>源 IP</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名称</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>系统名称</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>未在子网上</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>未在子网内</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名称</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>服务</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>未在子网内</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>服务</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>发现</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>VLAN 优先权</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>设备</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>网络 IP</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>设备</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名称</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>设备</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>IP 网络</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>域</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>服务器</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>主机</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>开关</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>路由器</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>设置 </translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>基础设施</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>核心</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>分配： </translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>访问</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>发现</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>已发现的 IP 网络</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>已发现的 NetBIOS 域</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>已发现的 VLANS</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>已发现的路由器</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>已发现的交换机</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>已发现的主机</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>已发现的服务器</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>网络发现报告</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>不能创建报告</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>磁盘空间不足</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>由 Viavi 测试仪器生成 </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>磁盘已满，图形已经停止。 &#xA; 请腾出空间，然后重新开始测试。 &#xA;&#xA; 或者，从 &#xA; 菜单栏的工具 -> 自定义中停用图形。</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>拍敲把时间单位置于中部</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>图形道具</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>最大</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>窗口</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>饱和窗口</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>存储绘图数据</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>目标设备剩余空间不足。</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>如果已插入 USB 存储器，您可直接导出到 USB 中。</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>正在保存图形数据。 这可能需要一段时间，请稍候 ...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>正在保存图形数据</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>设备剩余空间不足。 导出的图形数据不完整。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>导出图形数据时出错。该数据可能不完整。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>刻度</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 天</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 小时</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 小时</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 分</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 分钟</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 秒</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>绘图数据</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>点击并托拽以缩放</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>饱和</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>窗口</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>连接</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>信息</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Src 端口</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>目标端口</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>删除 ...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>确认……</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>选定的种类将从所有正在运行的测试中移除。</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>你确定要删除选定的项目吗？</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>新建……</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>打开对话框配置新的客户结果种类。</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>按下可以标记将从设备中删除的客户种类。选择了以后再按一次按钮删除文件。</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>按 "%1"&#xA; 开始</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>配置客户结果种类</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>标有“*”的所选结果无法应用到当前测试配置，不会显示在结果窗口中。</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>种类名：</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>输入客户种类名：最多 %1 字符</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>文件名 :</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>另存为</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>保存新文件</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>文件 %1 包含 &#xA; 种类 "%2"</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>所选结果 : </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>( 最大选择</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>配置 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>概要</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>最多（ %1 ）：</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>数值 (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- 自动</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>帮助工具按钮首页 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>帮助工具按钮后退 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>帮助工具按钮前进 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>帮助工具按钮结束 ...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>无呼叫历史记录</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>帮助工具按钮首页 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>帮助工具按钮后退 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>帮助工具按钮前进 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>帮助工具按钮结束 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>峰峰值</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>POS 峰值</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>阴性峰值</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- 自动</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>峰峰值</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>POS 峰值</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>阴性峰值</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- 自动</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>延迟 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>计数</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>结果 &#xA; 无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>状态：正常</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>状态：失败</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>状态 : 不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>扩展以查看筛选选项</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation># MEP 发现</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>设置为对等</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>筛选显示</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>筛选条件</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>输入筛选器值： 最多 %1 个字符</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>CCM 类型</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>CCM 速率</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>对等 MEP ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>维护域等级 </translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>指定域 ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>维护域 ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>维护关联 ID</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>测试设备已配置。 突出显示行已设置为此测试设备的对等 MEP 。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>突出显示的对等 MEP 已失败，正在设置测试设备。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>无法将 %1 设置为 %2 。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>结果 &#xA; 无法获得</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>图形 &#xA; 禁用</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>把结果窗口切换到全屏。</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>没有结果 &#xA; 可获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>用户</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>所有的</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 字节丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 字节上游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 字节下游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 字节缓存点吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 字节上游缓存点吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 字节下游缓存点吞吐量测试结果</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>导出文本文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>已导出日志到</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>数据流 &#xA; 细节</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>在此窗口中折叠所有结果树。</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>在此窗口中展开所有结果树。</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>结果类别 &#xA; 是空的</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>结果 &#xA; 无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>结果 &#xA; 无法获得</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>所有结果 &#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>列</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>显示列</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>消选择所有</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>只显示错误</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>列 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>仅显示 &#xA; 错误 &#xA; 行</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>仅显示错误行</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>流量速率 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>流量速率 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation># 分析的流</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>流量分组方式</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>连接总数</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>显示的流 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>其它流 >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>列 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止的</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>跳</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>时延 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>要查看更多层跟踪路由数据，请使用“查看” > 结果窗口 -> “单个” 菜单选项。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>理想传输时间</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>实际传输时间</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>组 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>数据流</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>程序</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>丢包</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>包抖动</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Distance 错误</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Period 错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>同步字节错误</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>只显示错误节目</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>列 ...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>总 Prog. Mbps</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>只显示 &#xA; 错误节目</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>只显示错误节目</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>只显示 &#xA; 错误数据流</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>只显示错误数据流</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation># 数据流 &#xA; 被分析</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>总 &#xA;L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>IP 校验和 &#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>UDP 校验和 &#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>启动 &#xA; 分析仪</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>列 ...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>请等待 .. 正在加载分析仪应用 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- 自动</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>时</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- 自动</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>请选择一个脚本 ..</translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>脚本：</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>状态 :</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>当前值状态</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止的</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>定时器：</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>脚本执行时间 :</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>计时器数</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>输出：</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>脚本完成于：  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>选择脚本</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>运行脚本</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>清除输出</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>停止脚本</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>正在运行 ...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>脚本执行时间 :</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>请选择另外一个脚本 .. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>差错.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>正在运行 .</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>正在运行 ..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>自定义测试列表</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>在启动时显示结果</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>上移</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>下移</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>删除全部</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>重命名</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>分隔符</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>添加快捷方式</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>添加保存的测试</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>删除所有收藏项目？</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>收藏项目列表为默认值。</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>将删除所有自定义收藏项目，并还原列表为此单元的默认值。是否继续 ?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>测试配置（ *.tst ）</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>双重测试配置 (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>选择保存的测试</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>请稍候...&#xA;正在启动空白测试视图</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>固定测试</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>重命名已固定测试</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>固定到测试列表</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>保存测试配置</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>测试名:</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>输入显示名称</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>此测试与 %1 相同</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>这是一个启动测试应用程序的快捷方式。</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>说明： %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>这是一个保存的测试配置。</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>文件名 : %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>替换</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>无法启动测试 ...&#xA; 选项已过期。 &#xA; 请退出后从系统页重新启动 BERT 。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>现在无法启动此测试。当前的硬件配置可能不支持或需要更多资源。</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>尝试删除运行于其他选项卡的测试。</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>此测试正运行于另一端口。是否转至该测试 ? （位于选项卡 %1 ）</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>（选项卡 %1 上的）另一测试可被重新配置到所选测试。是否要重新配置该测试 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>列表为空。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>无法启动测试 ...&#xA; 光抖动功能关闭。 &#xA; 从首页 / 系统页面启动光抖动功能。</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>无法启动测试 ...&#xA; 无可用电源 .&#xA; 退出或重新配置一个测试 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>无法加载测试 ...&#xA; 无法获得电源 .&#xA; 释放另一个模块或清除 / 重新配置 &#xA; 一个存在的测试 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>请等待 ...&#xA; 正在增加测试</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>请等待 ...&#xA; 正在重新配置测试</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>无法启动测试...&#xA;所需资源可能正在使用。&#xA;&#xA;请联系技术支持。&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>正在构造用户接口对象</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>主机正在与传输模块同步</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>初始化用户接口</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>返回</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>选择测试</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>版权所有 Viavi Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>仪器信息</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>选件</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>已保存文件</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>用户界面访问模式</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>正在使用远程控制。 &#xA;&#xA; 只读访问模式防止用户更换设置 &#xA; 这些设置可能影响远程控制操作。</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>访问模式</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>由于 PIM 配置的变化， MSAM 已重新安排。</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>一个接口模块已经被插入或移除。如果更换 PIM ，请继续。 &#xA;MTS-6000 多业务应用模块正在重启。这将花费 2 分钟，请等待 ...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>BERT 模块运行过热，如果 &#xA; 内部温度持续上升，将自动关闭。 &#xA; 请保存数据，关闭 BERT 模块， &#xA; 呼叫技术支持。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>因过热 BERT 模块已被强行关闭。</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>XFP PIM 位于错误的插槽。请将 XFP PIM 移到 #1 端口。</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>BERT 模块 XFP PIM 位于错误的插槽中。 &#xA; 请将 XFP PIM 移到 #1 端口。</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>您选择了一个电子测试但所选的 SFP 看起来象光学 SFP 。</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>确定您使用的是电子 SFP 。</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>BERT 模块已检测到应用程序 %1 中可能的错误。 &#xA;&#xA; 您已选择电子测试，但 SFP 看起来像光学 SFP 。  请更换或选择其他 SFP 。</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>您选择了光学测试但所选的 SFP 看起来象电子 SFP 。</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>确定您使用的是光学 SFP 。</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>测试设备检测到应用程序%1中可能的错误。&#xA;&#xA;您已选择光学测试，但SFP看起来像&#xA;电子SFP。请更换或选择其他SFP。</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>您已选择 10G 测试，但插入的收发器看起来不像 SFP+ 。 </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>确保您正使用 SFP+ 。</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>测试装置已检测到一个可能的错误。本测试需要一台SFP+，但收发器好像没有。请用SFP+替换。</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>自动报告设置</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>覆盖同名文件</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>自动报告</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>警告 :    所选的驱动器已经满了 . 你可以手动释放一些空间 , 或者让最老的 5 个报告 &#xA; 被自动删除 .</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>启用自动报告</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>报告期</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>最小:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>最大:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>创建报告后重新启动测试</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>创建一个独立的文件</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>报告名</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>日期和时间自动创建附加到名字上</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>格式化 :</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>文本</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>只读访问模式下不支持自动报告。</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>自动报告将被存到硬盘</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>自动报告需要硬盘。这台及其没有安装硬盘。</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>当一个自动脚本正在运行时，自动报告不能被启用</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>创建自动报告</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>正在准备 ...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>正在创建</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>正在删除以前的报告 ...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>完成 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** 电源功率不足 .  去掉一个模块 ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>串口连接成功</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>升级应用检查</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>通信应用准备</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** 升级中出现错误 *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** 确保以太网安全 = 标准或重装 BERT 软件 *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** 应用升级中有错误 .  重装模块软件 ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** 升级失败 . 电源功率不足 . ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** 启动错误 : 请撤销 BERT 模块重新激活 ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>浏览</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>工具</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>创建报告 ...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>自动报告…</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>开始测试</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>停止测试</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>用户定制 ...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>访问模式 ...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>复位测试为缺省设置</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>清除历史</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>运行脚本 ...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>VT100 模拟</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Modem 设置 ...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>恢复缺省规划</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>结果窗口</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>单个包</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>左右分开 </translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>上下分开</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>2 x 2 网格</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>底部合并</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>左边合并</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>只显示结果</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>测试状态</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>配置面板</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>操作面板</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>选择脚本</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>脚本文件 (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>选择脚本</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>信号连接</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>创建报告</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>文本 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>选择内容</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>创建</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>格式化 :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>创建以后阅读报告</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>错误 - 文件名不能为空白。</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>选择内容</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>格式化</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>文件名</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>选择...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>创建以后阅读报告</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>包括报文日志</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>创建报告</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>查看 &#xA; 报告</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>文本 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>错误 - 文件名不能为空白。</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>报告已保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>请衰减信号</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>事件记录和柱状图已经满了 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>K1/K2 记录满了 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>记录满了 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>测试将继续</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>光重启</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>终端测试</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Rx 机过载</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>记录已经满了</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>注意</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>没有正在运行的测试</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>定制用户接口</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>保存测试</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>保存双重测试</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>加载测试</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>所有文件（ *.tst *.dual_tst ）</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>已保存的测试（ *.tst ）</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>已保存的双重测试（ *.dual_tst ）</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>加载设置</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>加载双重测试</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>从 USB 导入保存的测试</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>所有文件 (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>保存的典型 RFC 测试配置 (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>已保存的 RFC 测试配置 (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>保存的 FC 测试配置 (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>已保存的 TrueSpeed 配置（ *.truespeed ）</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>已保存的 TrueSpeed VNF 配置 (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>已保存的 SAMComplete 配置（ *.sam ）</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>已保存的 SAMComplete 配置（ *.ams ）</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>已保存 OTN 检查配置 (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>保存光纤自测配置 (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>已保存的 CPRI 检查配置 (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>已保存的 PTP 检查配置 (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>已保存的 Zip 文件 (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>导出保存的测试到 USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>所有文件 (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>用户保存的多个测试</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>用户保留测试</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>正在使用远程控制。 &#xA;&#xA; 只读访问模式不允许该操作。 &#xA; 使用工具 -> 访问模式，启用完全访问。</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>BERT 模块上的选项已过期。 &#xA; 请退出后从系统页重新启动 BERT 。</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>隐藏</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>重新启动两项试验</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>重启</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>工具:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>操作</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>最大化的测试结果窗口： </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>全视图</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>增加测试</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>用户手册</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>光学推荐</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>频率网格</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>这是什么？</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>保存双重测试配置为 ...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>装载双重测试配置 ...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>浏览</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>更改测试选项 ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>进入全测试视图</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>创建双重测试报告 ...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>查看报告 ...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>帮助</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>警告</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>保存文件</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>新名称：</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>输入新的文件名</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>重命名</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>不能重命名，因为该名称的文件已存在。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>重启</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>编辑用户信息</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>未选择 ...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>最大字符数 :</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>选择标识 ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>预览不可用。</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>帮助图表</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>用户手册</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>返回</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>主页</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>向前</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>快速启动</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** 抖动升级中有错误 . 重装模块软件 ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** 光抖动功能有错误 . 电源功率不足 . ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>光抖动功能正在运行</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 配置文件（ *.%2 ）</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>选择配置文件</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>全部未选择</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>注意： 如果加载“连接”配置文件，会在必要时将通信信道连接到远程设备。</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>删除全部</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>不兼容配置文件</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>加载 &#xA; 配置文件</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>是否确认删除 %1 ？</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>是否确认删除所有 %1 配置文件？该操作无法撤消。</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（只读文件不会被删除。）</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>无法从以下来源加载配置文件：</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>已加载配置文件自：</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>在此应用程序中找不到部分配置，因此这些配置无法正常加载。</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>已成功加载配置文件自：</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>启用双重测试</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>变址应用</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>确认选项</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>没有可用测试可用于已安装硬件或选件。</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>无法启动任何测试。请检查安装的选项和当前硬件配置。</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>正在恢复之前关机时的应用</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>应用正在运行</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>无法设置内置 U 盘。 &#xA;&#xA; 请确认设备已可靠插入到电池旁边。插入后请重启 BERT 模块。</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>内置 U 盘文件系统看来已损坏。请与技术支持联系获得帮助。</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>内置 U 盘容量小于 1G 的推荐容量。 &#xA;&#xA; 请确认设备已可靠地插入到电池旁边。插入后请重启设备。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>还原设置时发生错误。 &#xA; 请重试。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>在加载保存测试前将终止所有运行测试。</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>请等待…加载保存的测试</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>测试文档名字或文件路径是无效的 .&#xA; 使用加载存储测试 来定位文档 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>恢复测试时发生错误。 &#xA; 请重试。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>电池充电器开启</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>请注意在这种模式下电池充电器无法使用 当选择合适的测试时，充电器将自动开启</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>此模块在用远程控制 &#xA; 显示已关闭。</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>消息已记录。点击查看...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 的报文日志</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>无报文</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 报文</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 报文</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 的报文日志</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>信息记录</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Modem 设置</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>为服务器地址选择 IP,IP 将被安排到拨号客户端</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>服务器 IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>客户端 IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>当前值位置 ( 当前码 )</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>无法检测到 modem. 确认插入了 modem 并重试 .</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>设备忙 , 断开所有连接重试</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>不能更新 modem. 断开所有拨号部分重试 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>由于时间更改，正在重启测试。</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>在下方输入新的选件键，进行安装。</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>选件键</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>安装</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>输入</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>联系 Viavi 购买软件选件。</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>选件 Challenge ID ： </translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>密码被接受 ! 重启以激活新选件</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>被拒绝的密钥 - 过时选项槽已满。</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>被拒绝的密钥 - 已经安装过时选项。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>无效的密码－请重试</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>接收到 %2 个键中的 %1 个 重启激活新选项。</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>无法打开 USB 存储器上的 '%1' 。</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>有一个包含选件信息的问题 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>所选字节</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>缺省</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>图例</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>图例 :</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI（未用）</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>保留的</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>关于 BERT 模块</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>传输模块</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>导入快速卡</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>导入快速卡</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> 隐藏菜单</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> 全部测试</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>自定义</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>在当前测试开始&#xA;之前捕捉到一张或多张所选截图。确认选择正确的文件。</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>未选择全部</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>选择截屏</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>未选择屏幕截图</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 校准</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>校准</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>校准…</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>校准未完成。校准需要使用 RS-FEC。</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>（可以从设置页面的 RS-FEC 选项卡中运行校准。）</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>重试校准</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>离开校准</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>校准未完成</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC 通常与 SR4、PSM4、CWDM4 一同使用。</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>要执行 RS-FEC 校准，请执行以下操作（也适用于 CFP4）：</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>在 CSAM 中插入 QSFP28 适配器</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>在适配器中插入 QSFP28 收发器</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>在收发器中插入光纤环回设备</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>单击校准</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>重新同步</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>必须立即将收发器重新同步到被测设备 (DUT)。</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>从收发器中移除光纤环回设备</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>建立与被测设备 (DUT) 的连接</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>打开 DUT 激光</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>验证 CSAM 上的信号存在 LED 为绿色</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>单击通路重新同步</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>重新同步完成。现在可以关闭对话框。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>保存为只读文件</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>固定到测试列表</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>文件名</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>选择...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>保存为只读文件</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>保存 &#xA; 配置文件</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 配置文件（ *.%2 ）</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>配置文件 </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>是只读文件。 不能被替换。</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>配置文件已保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>选择标识</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>图像文件 (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>文件太大。请选择另一个文件。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>所选字节</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>缺省</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>图例</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>开始&#xA;测试</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>停止&#xA;测试</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止的</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>全部存取</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>只读</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>暗</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>亮</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>快速启动</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>结果视图</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>估计的全部 TCP 吞吐量 (Mbps) 基于</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>最大可用带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>平均值环路时延（毫秒）</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>并行 TCP 会话数量需要最大值吞吐量 :</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>比特率</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>窗口尺寸</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>对话</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>估计的全部 TCP 吞吐量 (kbps) 基于</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>最大可用带宽 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 吞吐量</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>存储设置时发生错误.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>加载测试设置时发生错误。</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>所选盘已满， &#xA; 请删除一些文件，再存储</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>下列保存的客户结果种类文件与目前加载的不同：</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 其它</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>覆盖它们可能会影响其它测试。</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>继续覆盖吗？</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>请稍候 ...&#xA; 正在还原设置 ...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>当前没有足够资源加载 %1 测试。 &#xA; 请查看“测试”菜单了解当前配置下可用的测试列表。</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>无法恢复所有的测试设置 .&#xA; 文件内容太老或已经损坏 .&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>访问模式为“只读”。 若要更改，可使用“工具” -> “访问模式”</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>没有正在运行的测试</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>这将复位所有的缺省设置 .&#xA;&#xA; 继续吗 ?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>这将关闭并重启测试 .&#xA; 测试设置将恢复为缺省 .&#xA;&#xA; 继续 ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>这个工作流现在正在运行。 您希望结束它并启动一个新的？ &#xA;&#xA; 点击取消，继续运行前一个工作流。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>希望结束此工作流 ?&#xA;&#xA; 点击取消继续运行。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>无法启动 VT100 。 已在使用串行设备。</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>模块 </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>请注意按重启将清除所有端口上的所有结果 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>选择测试</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>加载测试...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>另存测试为 ...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>仅加载设置 ...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>增加测试</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>删除测试</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>帮助</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>帮助图表</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>查看报告...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>输出报告 ...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>编辑用户信息...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>输入报告标志 ...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>从 U 盘 导入</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>已保存的测试 ...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>保存自定义类别 ...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>输出到 U 盘</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>截屏...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>定时数据...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>检查安装选件 ...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>取得屏幕截图</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>用户手册</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>光学推荐</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>频率网格</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>信号连接</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>这是什么？</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>快速卡</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>请等待 ...&#xA; 正在删除测试</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>端口 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>端口 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>未选择 ...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>选择文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>从 U 盘 导入数据包捕获</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>选择 &#xA; 文件</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Saved Packet Capture (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>查看报告</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有的文件 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>文本 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>浏览</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>查看此&#xA;报告</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>查看其他&#xA;报告</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>重命名 &#xA; 报告</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 已经存在。 &#xA; 你是否想替换它？</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>已重命名报告</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>错误 - 文件名不能为空白。</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>已将报告保存为</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>未保存报告。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>去</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>保存 TIE 数据 ...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>保存为类型： </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>HRD 文件</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>CHRD 文件</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>存储</translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>这将要花费几秒 ...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>错误： 无法打开 HRD 文件。 请尝试重新保存。</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>正在取消 ...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>TIE 数据已保存。</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>错误： 文件无法保存。 请重试。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>警告</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>在关闭Wander Analysis时所有分析结果都将消失。&#xA;如要继续分析，点击“继续分析”。</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>关闭分析</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>连续分析</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>漂移分析</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>更新 &#xA;TIE 数据</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>停止 TIE&#xA; 更新</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>计算 &#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>停止 &#xA; 计算</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>采取 &#xA; 截图</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>加载 &#xA;TIE 数据</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>停止 TIE&#xA; 负载</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>关闭 &#xA; 分析</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>加载 TIE 数据</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>所有 Wander 文件 (*.chrd *.hrd);;Hrd 文件 (*.hrd);;Chrd 文件 (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>按两次以定义长方形</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>删除全部</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>加载配置文件</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>是否确认删除 %1 ？</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>是否确认删除所有 %1 配置文件？该操作无法撤消。</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（只读文件不会被删除。）</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 配置文件（*%2.%3）</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>无法加载配置文件。</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>加载失败</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>下一步</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>信息</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>差错</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>去</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>警告</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>您想执行下一步吗？</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>您确认需要退出吗？</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>退出时恢复设置</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>退出到结果</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>返回</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>逐步：</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>下一步</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>指导我</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>其他端口</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>启动</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>去 ...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>其他端口</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>测试正在初始化，请稍候...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>测试正在关闭，请稍候...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>信息记录</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>主要的</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>显示步骤</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>响应： </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>未选择 ...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>报告标识</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>选择标识 ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>预览不可用。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>测试正在初始化，请稍候...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>测试正在关闭，请稍候...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>正在进行测试 ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>剩余时间:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>测试没有运行</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>测试未完成</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>开始脚本前关闭自动报告</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>打开之前关闭的自动报告</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>主要的</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>已保存截屏图</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>已保存截屏图 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>选择配置文件</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>操作层</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>加载保存的配置文件</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>如何配置 TrueSAM ？</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>从保存的配置文件加载配置</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>去</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>开始新配置文件</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>服务运行于哪一层 ?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>第2层： 使用MAC地址测试，例如00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>第3层： 使用IP地址测试，例如192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>请稍候...正在移动到突出显示的步骤。</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>选择测试</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>建立通信</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>配置增强型RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>配置SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>配置J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>配置TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>保存配置</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>添加报告信息</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>运行所选测试</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>查看报告</translation>
        </message>
    </context>
</TS>
