<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscópio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salvar</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vista FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Desistir</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Zoom +</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Overlay</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Analisando...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Perfil</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importar da porta USB...</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>Dica</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Autocentrar</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Botão de teste:</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testes</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Congela</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>Outras configurações...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Importar perfil microscópio</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Importar&#xA;Perfil</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Perfis microscópicos (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>Ao vivo</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>Gravar PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>Gravar PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Salvar imagem</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Gravar Relatório</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>Análise falida</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>Por favor verifique se o vídeo ao vivo mostra a extremidade da fibra e que está focada antes de testar novamente.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Selecionar imagem</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Arquivos de imagem (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Inspeção  Fibra e Teste Relatório</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Informação fibra</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>Informação extra fibra não definida</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASSA</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALHA</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Perfil:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Dica:</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Resumo de Inspeção</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>DEFEITOS</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>RASCUNHOS</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>ou </translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>Critérios</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Limite</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Contar</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>Resultado</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>qualquer</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Falha na geração do resumo de inspeção</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>BAIXA MAGNIFICAÇÃO</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>ALTA MAGNIFICAÇÃO</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Rascunho de teste não habilitado</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Job:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cabo:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Conector:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Empresa:</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Técnico:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Cliente:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Localização:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Job:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cabo:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Conector:</translation>
        </message>
    </context>
</TS>
