<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Микроскоп</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Испытания</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>Измеритель мощности</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>Система</translation>
        </message>
    </context>
</TS>
