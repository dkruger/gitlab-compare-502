<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>ﾓﾊﾞｲﾙｱﾌﾟﾘ機能</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth と WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>WiFi のみ</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>変更を反映するには、ﾃｽﾄ ｾｯﾄを、再度、開始します。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>貼り付け</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>ｺﾋﾟｰ</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>切り取り</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>単一</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>複数</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>すべて</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>開く</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>名前の変更</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>新しいﾌｫﾙﾀﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>ﾃﾞｨｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>貼り付けると、既存のﾌｧｲﾙが上書きされます。</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>ﾌｧｲﾙの上書き</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>不明なｴﾗーのため、ﾌｧｲﾙが開きません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>ﾌｧｲﾙが開きません。</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>選択されたﾌｧｲﾙは、完全に削除されます。</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>%1 個の選択されたﾌｧｲﾙは、完全に削除されます。</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>ﾌｧｲﾙの削除</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>ﾌｫﾙﾀﾞー名を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>ﾌｫﾙﾀﾞｰ作成</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>ﾌｫﾙﾀﾞｰ "%1" を作成できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>ﾌｫﾙﾀﾞｰ "%1" はすでに存在します。</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>ﾌｫﾙﾀﾞーの作成</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>貼り付け中...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>削除中...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Completed.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 が失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>ﾌｧｲﾙの貼り付けに失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>%1 個のﾌｧｲﾙの貼り付けに失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>空き領域が不足しています。</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>ﾌｧｲﾙの削除に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>%1 個のﾌｧｲﾙの削除に失敗しました。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>戻る</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>ｵﾌﾟｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>ﾌｧｲﾙ名</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>ｱｸｼｮﾝ</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>ﾓｼﾞｭｰﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>その他のﾜｲﾔﾚｽ ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>以下にﾜｲﾔﾚｽ ﾈｯﾄﾜｰｸ情報を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>名前 (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>暗号化の種類:</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞが利用可能</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync によりｱｯﾌﾟｸﾞﾚｰﾄﾞが可能であると判定されました。 &#xA; 今すぐｱｯﾌﾟｸﾞﾚｰﾄﾞしますか &#xA;&#xA; ｱｯﾌﾟｸﾞﾚｰﾄﾞする場合、実行中のすべてのﾃｽﾄは終了されます。 ﾌﾟﾛｾｽの最後に、ﾃｽﾄ ｾｯﾄは自動的に再起動して、ｱｯﾌﾟｸﾞﾚｰﾄﾞを完了します。</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>不明なｴﾗーが発生しました。再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞに失敗しました</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>機器情報:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>基本ｵﾌﾟｼｮﾝ:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>機器を既定に&#xA;ﾘｾｯﾄします</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>ﾛｸﾞを &#xA;USB にｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>ｼｽﾃﾑ情報 &amp;#xA;ﾌｧｲﾙを作成</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>ｼｽﾃﾑ情報はこのﾌｧｲﾙに&#xA;ｺﾋﾟーされました:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>再起動が必要です。ｼｽﾃﾑ設定とﾃｽﾄ設定は、既定にﾘｾｯﾄされます。</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>再起動してﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>ﾛｸﾞが正常にｴｸｽﾎﾟｰﾄされました。</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>ﾛｸﾞをｴｸｽﾎﾟｰﾄできません。 USB が正しく挿入されていることを確認してから再試行してください。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 ﾊﾞｰｼﾞｮﾝ %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>やりなおす</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>ﾃﾞﾌｫﾙﾄに戻す</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ方法を選択します:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>USB ﾒﾓﾘーに保存されたﾌｧｲﾙからｱｯﾌﾟｸﾞﾚｰﾄﾞします。</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>ﾈｯﾄﾜｰｸ経由で Web ｻｰﾊﾞーからｱｯﾌﾟｸﾞﾚｰﾄﾞをﾀﾞｳﾝﾛｰﾄﾞします。</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ ｻｰﾊﾞーのｱﾄﾞﾚｽを入力してください:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>ｻｰﾊﾞーのｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ ｻｰﾊﾞーへのｱｸｾｽする場合、必要に応じてﾌﾟﾛｷｼ ｻｰﾊﾞーのｱﾄﾞﾚｽを入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>ﾌﾟﾛｷｼ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>接続</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>入手可能なｱｯﾌﾟｸﾞﾚｰﾄﾞがあるかｻｰﾊﾞーに確認します。</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>前へ</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>次へ</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞの開始</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>ﾀﾞｳﾝｸﾞﾚｰﾄﾞの開始</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞがありません</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞに失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞにより実行中のﾃｽﾄは終了されます。過程の最後に、ﾃｽﾄ ｾｯﾄは自動的に再起動して、ｱｯﾌﾟｸﾞﾚｰﾄﾞを完了します。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞの確認</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>ﾃｽﾄ ｾｯﾄのﾀﾞｳﾝｸﾞﾚｰﾄﾞは可能ですが、推奨されません ﾀﾞｳﾝｸﾞﾚｰﾄﾞする場合は、Viavi TAC に相談することをお勧めします。</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>ﾀﾞｳﾝｸﾞﾚｰﾄﾞにより実行中のﾃｽﾄは終了されます。 ﾌﾟﾛｾｽが終了すると、ﾃｽﾄ ｾｯﾄは自動的に再起動して、ﾀﾞｳﾝｸﾞﾚｰﾄﾞを完了します。</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>ﾀﾞｳﾝｸﾞﾚｰﾄﾞは推奨されません</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>不明なｴﾗーが発生しました。再度、試行してください。</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>ｼｽﾃﾑ</translation>
        </message>
    </context>
</TS>
