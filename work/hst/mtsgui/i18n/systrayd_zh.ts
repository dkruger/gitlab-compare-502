<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>systrayd::CAudioSystemTrayDataItem</name>
        <message utf8="true">
            <source>Audio</source>
            <translation>音频</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBatterySystemTrayDataItem</name>
        <message utf8="true">
            <source>Battery/Charger</source>
            <translation>电池/充电器</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please plug in AC power to continue using the test instrument.</source>
            <translation>电池温度过高。要继续使用测试仪器，必须接通交流电源。</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please keep the AC power plugged in to continue using the test instrument.</source>
            <translation>电池温度过高。要继续使用测试仪器，必须始终接通交流电源。</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and cannot be used until it cools down. The AC power must remain plugged in to continue using the test instrument.</source>
            <translation>电池温度过高，冷却后方可使用。要继续使用测试仪器，必须始终接通交流电源。</translation>
        </message>
        <message utf8="true">
            <source>Battery charging is not possible at this time.</source>
            <translation>目前无法对电池充电。</translation>
        </message>
        <message utf8="true">
            <source>A power failure has occurred, the battery is currently not charging.</source>
            <translation>发生电源故障，电池现在无法充电。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBeepingWarning</name>
        <message utf8="true">
            <source>System Warning</source>
            <translation>系统警告</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBluetoothSystemTrayDataItem</name>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>蓝牙</translation>
        </message>
        <message utf8="true">
            <source>File received and placed in the bluetooth inbox.&#xA;Filename: %1</source>
            <translation>蓝牙收件箱内接收和放置的文件。&#xA;文件名：%1</translation>
        </message>
        <message utf8="true">
            <source>Pair requested. You may go to the Bluetooth system page to complete the pair by clicking on the icon above.</source>
            <translation>需要配对。可以转到蓝牙系统页面点击上面图表完成配对。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CNtpSystemTrayDataItem</name>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CP5000iSystemTrayDataItem</name>
        <message utf8="true">
            <source>P5000i</source>
            <translation>P5000i</translation>
        </message>
        <message utf8="true">
            <source>The P5000i Microscope will not function in this USB port.  Please use the other USB port.</source>
            <translation>P5000i 显微镜将无法在此 USB 端口工作。请使用其他 USB 端口。</translation>
        </message>
        <message utf8="true">
            <source>Microscope Error</source>
            <translation>微错误</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CStrataSyncSystemTrayDataItem</name>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CUsbFlashSystemTrayDataItem</name>
        <message utf8="true">
            <source>USB Flash</source>
            <translation>U 盘闪存</translation>
        </message>
        <message utf8="true">
            <source>Format complete. Please remember to eject the device from the USB system page before removing it.</source>
            <translation>格式化完成：请记住先从U 盘系统页面弹出设备再拔下。</translation>
        </message>
        <message utf8="true">
            <source>Formatting USB device. Do not remove device until process is completed.</source>
            <translation>格式化U 盘设备。请勿在完成之前拔下设备。</translation>
        </message>
        <message utf8="true">
            <source>The USB device was removed without being ejected. Data may be corrupted.</source>
            <translation>U 盘设备未弹出而被拔下。数据可能受到破坏。</translation>
        </message>
        <message utf8="true">
            <source>The USB device has been ejected and is safe to remove.</source>
            <translation>U 盘设备已弹出，可安全拔下。</translation>
        </message>
        <message utf8="true">
            <source>Please remember to eject the device from the USB system page before removing it.</source>
            <translation>请记住先从U 盘系统页面弹出设备再拔下。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CVncSystemTrayDataItem</name>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CWifiSystemTrayDataItem</name>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
    </context>
</TS>
