<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>ﾀﾞｳﾝﾛｰﾄﾞの表示</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>ﾂﾞｰﾑ ｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>ﾂﾞｰﾑ ｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>ﾂﾞｰﾑのﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>ﾀﾞｳﾝﾛｰﾄﾞしています</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>開く</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>残り %1 分</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>残り %1 秒</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>ﾀﾞｳﾝﾛｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>名前を付けて保存</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>ﾃﾞｨﾚｸﾄﾘ :</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>ﾌｧｲﾙ名::</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
</TS>
