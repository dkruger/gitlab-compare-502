<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>다운로드 보기</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>줌 인</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>줌 아웃</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>줌 리셋 </translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>다운로드 중</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>열기</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 분 남았음</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 초 남았음</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>다운로드</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>다른 이름으로 저장</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>디렉터리 :</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>파일 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
    </context>
</TS>
