<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>불법적인 접속을 방지하기 위해 사용자가 이 테스트 세트를 잠갔습니다.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>비밀번호가 필요하지 않기 때문에 OK를 눌러 잠금 해제하세요. </translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>테스트 세트를 잠금 해제하시겠습니까?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>잠금 해제하기 위해 비밀번호를 입력하세요:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>비밀번호 입력</translation>
        </message>
    </context>
</TS>
