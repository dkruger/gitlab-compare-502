<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Rechazada asignación de dirección IP no válida.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>Asignación de gateway no válido: 172.29.0.7. Reinicie el MTS.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>La dirección IP ha cambiado. Reiniciar Módulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>Inicializando módulo BERT.  Solicitud de APAGAR rechazada.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>Módulo BERT inicializando con función de jitter óptico. Solicitud de APAGAR rechazada.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>Apagando el módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>Mód BERT OFF</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>El módulo BERT está apagado. Pulse HOME/SYSTEM y posteriormente el icono BERT para activarlo.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>Módulo BERT ON</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** ACTUALIZANDO APLICACIÓN SOFTWARE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** ACTUALIZACION COMPLETADA *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** ACTUALIZANDO SOFTWARE JITTER *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** ACTUALIZANDO SOFTWARE DEL KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** INICIÁNDOSE ACTUALIZACIÓN *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>Fallo IU Mód BERT</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Arrancando Módulo BERT con función de Jitter</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>Abriendo IU Módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>Fallo Mód BERT. Módulo OFF</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Módulo 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Módulo 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Módulo 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Módulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Módulo </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Módulo 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Módulo 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Módulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Módulo 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Módulo 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Módulo 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Módulo 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Módulo 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Módulo 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Módulo 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Módulo 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Módulo 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Módulo 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Módulo 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Módulo 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Módulo 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Módulo 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Módulo 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Se requiere actualización del software. La versión actual no es compatible con Mód BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>Es necesaria la reinstalación del software BERT. No está instalado el software BERT adecuado para el módulo BERT adjunto.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>Se requiere actualización del hardware BERT El hardware actual no es compatible.</translation>
        </message>
    </context>
</TS>
