<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>无效 IP 地址被拒绝</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>无效网关分配： 172.29.0.7 。重启 MTS 系统。</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>IP 地址改变。重启 BERT 模块。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>BERT 模块初始化。“ OFF ”请求被拒绝。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>具有光抖动功能的 BERT 模块初始化。“ OFF ”请求被拒绝。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>BERT 模块关闭</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>BERT 模块关闭</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>BERT 模块已故障。 按 “首页 / 系统”，然后使用 BERT 图标激活它。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>BERT 模块打开</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** 升级应用软件 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** 升级完成 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** 升级抖动软件 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** 升级核心软件 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** 开始升级 *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>BERT 模块用户接口故障</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>正在启动带有光抖动功能的 BERT 模块 UI</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>加载 BERT 模块用户接口</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>BERT 模块故障。模块关闭</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>模块 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>模块 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>模块 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>模块 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>模块 </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>模块 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>模块 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>模块 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>模块 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>模块 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>模块 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>模块 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>模块 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>模块 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>模块 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>模块 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>模块 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>模块 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>模块 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>模块 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>模块 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>模块 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>模块 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>需要升级基本软件。当前版本不支持 BERT 模块</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>必须重新安装 BERT 软件。 正式 BERT 软件未安装在所连 BERT 模块中。</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>需要升级 BERT 硬件。现有的硬件不相容。</translation>
        </message>
    </context>
</TS>
