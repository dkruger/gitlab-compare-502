<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi Lecteur Vidéo</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Nom du fichier de la vidéo</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Temps:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>État de la vidéo</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Échelle et recadrage</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Echelle</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Ouvrir et Fichier ...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Format</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Echelle</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Mode d'échelle</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>Monter en vue</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Échelle et recadrage</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Ouvrir un fichier ...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Multimédia ( * . Avi * . Mp4 * . Mpeg * . Mpg * . Mpe * . Mp2 * . *. Mp3 aac * . M4a * . M4p * . Aif * . Wav )</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Ouvrir</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>Aucune Open Media</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Desactive</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Chargement en cours ...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Mise en mémoire tampon...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Jouer ...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Erreur...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Idle - Médias arrêt</translation>
        </message>
    </context>
</TS>
