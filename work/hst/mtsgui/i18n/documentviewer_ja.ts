<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>読み込み中: </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>PDF の読み込みに失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>ﾍﾟｰｼﾞの表示に失敗しました: </translation>
        </message>
    </context>
</TS>
