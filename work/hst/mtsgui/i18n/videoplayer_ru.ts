<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Видео - плеер Viavi</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Выйти</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Имя видеофайла</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Время:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>Состояние видео</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Масштабировать и обрезать</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Шкала</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Открыть &amp; Файл ...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Формат кадра</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Авто</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Шкала</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Режим масштабирования</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>Вид по размеру окна</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Масштабировать и обрезать</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Открыть файл ...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Мультимедиа (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Открыть</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>Нет открытых сред</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Остановлен</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Идет загрузка ...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Буферизация ,...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Воспроизведение ...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>Пауза</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Ошибка...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Не занято - Тормозящие среды</translation>
        </message>
    </context>
</TS>
