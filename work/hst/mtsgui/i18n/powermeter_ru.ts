<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>graphFrame</name>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
    </context>
    <context>
        <name>powermeter</name>
        <message utf8="true">
            <source>Viavi Powermeter</source>
            <translation>Измеритель мощности Viavi</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Сводка</translation>
        </message>
        <message utf8="true">
            <source>Graph</source>
            <translation>График</translation>
        </message>
        <message utf8="true">
            <source>Statistics</source>
            <translation>Статистика</translation>
        </message>
        <message utf8="true">
            <source>Display Type</source>
            <translation>Тип отображения</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Длина волны</translation>
        </message>
        <message utf8="true">
            <source>780 nm</source>
            <translation>780 nm</translation>
        </message>
        <message utf8="true">
            <source>820 nm</source>
            <translation>820 nm</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>980 nm</source>
            <translation>980 nm</translation>
        </message>
        <message utf8="true">
            <source>1300 nm</source>
            <translation>1300 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1480 nm</source>
            <translation>1480 nm</translation>
        </message>
        <message utf8="true">
            <source>1490 nm</source>
            <translation>1490 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>1625 nm</source>
            <translation>1625 nm</translation>
        </message>
        <message utf8="true">
            <source>Display Units</source>
            <translation>Единицы отображения</translation>
        </message>
        <message utf8="true">
            <source>dBm</source>
            <translation>dBm</translation>
        </message>
        <message utf8="true">
            <source>Milliwatts</source>
            <translation>Милливатты</translation>
        </message>
        <message utf8="true">
            <source>dB</source>
            <translation>dB</translation>
        </message>
        <message utf8="true">
            <source>Save Results</source>
            <translation>Сохранить результаты</translation>
        </message>
        <message utf8="true">
            <source>Clear Results</source>
            <translation>Очистить результаты</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Выйти</translation>
        </message>
    </context>
    <context>
        <name>statisticsFrame</name>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Длина волны</translation>
        </message>
        <message utf8="true">
            <source>Current</source>
            <translation>Текущ.</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Средн.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Макс.</translation>
        </message>
        <message utf8="true">
            <source>780 nm</source>
            <translation>780 nm</translation>
        </message>
        <message utf8="true">
            <source>820 nm</source>
            <translation>820 nm</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>980 nm</source>
            <translation>980 nm</translation>
        </message>
        <message utf8="true">
            <source>1300 nm</source>
            <translation>1300 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1480 nm</source>
            <translation>1480 nm</translation>
        </message>
        <message utf8="true">
            <source>1490 nm</source>
            <translation>1490 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>1625 nm</source>
            <translation>1625 nm</translation>
        </message>
        <message utf8="true">
            <source>USB Power Meter Statistics</source>
            <translation>Статистика измерителя мощности USB</translation>
        </message>
    </context>
    <context>
        <name>summaryFrame</name>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>dBm</source>
            <translation>dBm</translation>
        </message>
        <message utf8="true">
            <source>0.00</source>
            <translation>0.00</translation>
        </message>
        <message utf8="true">
            <source>REF</source>
            <translation>REF</translation>
        </message>
        <message utf8="true">
            <source>Wavelength:</source>
            <translation>Длина волны:</translation>
        </message>
        <message utf8="true">
            <source>Frequency:</source>
            <translation>Частота:</translation>
        </message>
        <message utf8="true">
            <source>1625</source>
            <translation>1625</translation>
        </message>
        <message utf8="true">
            <source>1550</source>
            <translation>1550</translation>
        </message>
        <message utf8="true">
            <source>1490</source>
            <translation>1490</translation>
        </message>
        <message utf8="true">
            <source>1480</source>
            <translation>1480</translation>
        </message>
        <message utf8="true">
            <source>1310</source>
            <translation>1310</translation>
        </message>
        <message utf8="true">
            <source>1300</source>
            <translation>1300</translation>
        </message>
        <message utf8="true">
            <source>980</source>
            <translation>980</translation>
        </message>
        <message utf8="true">
            <source>850</source>
            <translation>850</translation>
        </message>
        <message utf8="true">
            <source>820</source>
            <translation>820</translation>
        </message>
        <message utf8="true">
            <source>780</source>
            <translation>780</translation>
        </message>
        <message utf8="true">
            <source>780 nm</source>
            <translation>780 nm</translation>
        </message>
        <message utf8="true">
            <source>7000 Hz</source>
            <translation>7000 Hz</translation>
        </message>
        <message utf8="true">
            <source>ABS->REF</source>
            <translation>ABS->REF</translation>
        </message>
        <message utf8="true">
            <source>Reset  REF</source>
            <translation>Сбросить REF</translation>
        </message>
        <message utf8="true">
            <source> AUTO Wavelength Detect</source>
            <translation> Автообнаружение длины волны</translation>
        </message>
        <message utf8="true">
            <source>USB Power Meter Summary</source>
            <translation>Сводка показаний индикатора питания USB</translation>
        </message>
    </context>
    <context>
        <name>scxgui::powermeterGui</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Сохранить изображение</translation>
        </message>
        <message utf8="true">
            <source>List of Values</source>
            <translation>Список значений</translation>
        </message>
    </context>
</TS>
