<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Fichiers</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Stockage amovible</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Pas d'équipement détecté.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Format</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>En formatant cet équipement USB, toutes les données existantes seront effacées. Ceci comprend tous les fichiers et les partitions.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Éjecter</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Consulter</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>AUCUN</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NON</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Paire Bluetooth requise</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>Entrez PIN pour association</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Paire</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Requête d'association</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Requête d'association de:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Pour associer avec le périphérique, assurez-vous que le code indiqué ci-dessous correspond avec le code sur ce périphérique</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Association initialisée</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Requête d'association envoyée à:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Démarrer scan</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Arrêter scan</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Activer Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Permettre à tous les autres périphériques de s'associer à ce périphérique</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Nom de périphérique</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Application mobile activée</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Réseau</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>Mode IP</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statique</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>Adresse IP</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Masque de sous-réseau</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Passerelle</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>Serveur DNS</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>Mode IPv6</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Désactivé</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuel</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>l'adresse IPv6</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Longueur du préfixe de Sous-Réseau</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>Serveur DNS </translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Link-Local Address</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Addresse statut incomplet</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Adresse statut complet</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Modules non chargés</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Présent</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Pas présent</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Commençant</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Activation</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Initialisation</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Prêt</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Balayage</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Désactivation</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Mode d'arrêt</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Associant</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Associé</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Activation de l'adaptateur sans fil</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTÉ</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Déconnecté</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Connectez-vous à WPA Enterprise Network</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Nom du réseau</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>Méthode d'authentification externe</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Méthode d'authentification interne</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Nom de l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation> Identité anonyme</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Mot de passe</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>certificats</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Mot de passe privé</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Connecter au réseau personnel WPA</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Mot de passe</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>Pas trouvé d'équipement USB sans fil.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Statut</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Arrêt de connecter</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Oublier réseau</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>La connexion au réseau n'était pas possible.</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>service 3G</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>modem en activité</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>modem activé</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>modem désactivé</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>Aucun modem détecté</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Erreur - Le modem ne répond pas.</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Erreur - contrôleur de périphérique n'a pas commencé .</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Erreur - Impossible de configurer le modem .</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Attention - Le modem ne répond pas. Le modem est en cours de réinitialisation ...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Erreur - SIM n'est pas insérée .</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Erreur - Réseau inscription refusée.</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connecte</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Déconnecte</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connexion en cours</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Déconnexion</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Non connecté</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Echec de la connexion</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Configuration du modem</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Activer le modem</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>Aucun modem trouvé ou activé .</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>APN de réseau</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>État de la connexion</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>Vérifiez APN et essayez à nouveau .</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Serveur DNS Pri</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec serveur DNS</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Proxy&amp; Sécurité</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Type de proxy</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Serveur proxy</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Sécurité du réseau</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>Désactiver FTP, Telnet, et auto StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Gestion d'alimentation</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>Alimentation AC est branchée</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Fonctionne sur l'alimentation de la batterie</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Pas de batterie détectée</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Le chargement a été désactivé à cause de la consommation d'énergie</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Chargement de la batterie</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>La batterie est entièrement chargée</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>Pas de batterie en charge</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Impossible de charger la batterie à cause d'une coupure secteur</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>L'état de la batterie est inconnu</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>La batterie est trop chaude et peut ne pas charger</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>La batterie est trop chaude&#xA;Veuillez brancher l'alimentation AC</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>La batterie est trop chaude&#xA;Ne pas débrancher l'alimentation AC</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>La batterie est trop chaude&#xA;Le débranchement de l'alimentation AC forcera l'arrêt</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>La batterie est trop froide et ne peut pas charger</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>La batterie est en danger de surchauffe</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>La température de la batterie est normale</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Autoriser arrêt auto en batterie</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Durée inactive (minutes)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Date et heure</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Fuseau horaire</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Région</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>Afrique</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>Amériques</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antarctique</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Asie</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Océan Atlantique</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Australie</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Europe</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Océan Indien</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Océan Pacifique</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>Pays</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucun</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afghanistan</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>îles Åland</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Albanie</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Algérie</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Samoa américaines</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorre</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angola</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguilla</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua-et-Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Argentine</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Arménie</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Autriche</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Azerbaïdjan</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamas</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahreïn</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladesh</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbade</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Biélorussie</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Belgique</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belize</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Bénin</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermudes</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Bhoutan</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolivie</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bosnie-Herzégovine</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botswana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Ile Bouvet</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brésil</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Territoire britannique de l'océan Indien</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Darussalam</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgarie</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Cambodge</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Cameroun</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Canada</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Cap-Vert</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Îles Caïmans</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>Afrique centrale</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Chad</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Chili</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>Chine</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Île Christmas</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Îles Cocos</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Colombie</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Comores</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Congo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>République démocratique du Congo</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Îles Cook</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Costa Rica</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Côte d'Ivoire</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Croatie</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Cuba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Chypre</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>République Tchèque</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Danemark</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Djibouti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominique </translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>République Dominicaine</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Équateur</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Égypte</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Guinée équatoriale</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Érythrée</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estonie</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Éthiopie</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Îles Malouines</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Îles Féroé</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fidji</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finlande</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>France</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Guinée française</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Polynésie française</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Territoires Français du sud</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabon</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambie</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Géorgie</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Allemagne</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Ghana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Gibraltar</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Grèce</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Groenland</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Grenade </translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Guadeloupe</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Guernesey</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Guinée</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Guinée-Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guyana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haïti</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Îles Heard-et-MacDonald</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hong Kong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Hongrie</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Islande</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Inde</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Indonésie</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Iran</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Irak</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Irlande</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Île de Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Israël</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Italie</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Jamaïque</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Japon</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Jordanie</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Kazakhstan</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Kenya</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Corée du Nord</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Corée du Sud</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Koweït</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Kirghizistan</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Laos</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Lettonie</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Liban</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesotho</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Liberia</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Libye</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Liechtenstein</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Lituanie</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Luxembourg</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Macédoine</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagascar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malaisie</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Maldives</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Mali</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Malte</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Îles Marshall</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinique</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Mauritanie</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>Île Maurice</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>Mexique</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Micronésie </translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Moldavie</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Monaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Mongolie</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Monténégro</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Montserrat </translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Maroc</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Mozambique</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Birmanie</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namibie</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Népal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Pays-Bas</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Antilles Néerlandaises</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Nouvelle-Calédonie</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Nouvelle-Zélande</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nicaragua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Niger</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nigéria</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Île Norfolk</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Îles Mariannes du Nord</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Norvège</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Oman</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Pakistan</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palaos</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Palestine (État revendiqué)</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panamá</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papouasie-Nouvelle-Guinée</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguay</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Pérou</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Philippines</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Île Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Pologne</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portugal</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Porto Rico</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Qatar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>Réunion</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Roumanie</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Fédération Russe</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Rwanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>Saint Barthélemy</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Sainte-Hélène, Ascension et Tristan da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Saint-Christophe-et-Niévès</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Sainte-Lucie</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>Saint-Martin</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>Saint-Pierre-et-Miquelon</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>Saint-Vincent-et-les Grenadines</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>Saint-Marin</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Sao Tomé-et-Principe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Arabie Saoudite</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Sénégal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Serbie</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seychelles</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Sierra Leone</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapour</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Slovaquie</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Slovénie</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Salomon </translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somalie</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Afrique du Sud</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Géorgie du Sud-et-les Îles Sandwich du Sud</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>Espagne</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Soudan</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Surinam</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Île Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Swaziland</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Suède</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Suisse</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>Syrie</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Taïwan</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tadjikistan</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Tanzanie</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Thaïlande</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Timor Oriental</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinité-et-Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunisie</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Turquie</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Turkménistan</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Îles Turques-et-Caïques</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Ouganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ukraine</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Émirats Arabes Unis</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Royaume-Uni</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>États-Unis</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Îles mineures éloignées des États-Unis</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Ouzbékistan</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Vatican</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Vietnam</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Îles Vierges britanniques</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Îles Vierges des États-Unis</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis-et-Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Sahara Occidental</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Yémen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Zambie</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Zimbabwe</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Région</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Base Antarctique Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Base Antarctique Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Base Antarctique Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Base Antarctique Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>Base Antarctique McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Base Antarctique Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Base Antarctique Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Pôle Sud</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Vostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Territoire de la capitale australienne</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Nord</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Nouvelle-Galles du Sud</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Sud</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tasmanie</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Victoria</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Ouest</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasilia</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasilia - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasilia + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alaska</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlantique</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Central</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Oriental</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Hawaii</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Montagne</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>Terre-Neuve</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pacifique</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Île de Pâques</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Îles Galápagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Îles Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Îles Marquises</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Tahiti</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Occidental</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Est</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Îles Phnix</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Îles de la Ligne</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Îles Gilbert</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Nord Ouest</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Îles Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Açores</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madère</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkoutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrad</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnoïarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Moscou</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Vladivostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Iakoutsk</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Iekaterinbourg</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Îles Canaries</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Île Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Île de Wake</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Ajuste automatiquement pour l'heure normale (d'été)</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Date &amp; heure actuelles</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 heures</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 heures</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Réglez l'horloge avec NTP</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>Utilisez le temps en 24 heures</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN serveur NTP</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi- Fi serveur NTP</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>Serveur NTP 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>Serveur NTP 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (Anglais)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (Allemand)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (Espagnol)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (Chinois simplifié)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Japonais)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (Coréen)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (Russe)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Portuguais)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (Italien)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Turc)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Langue :</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Changer le standard de formatage :</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Échantillon pour le formatage sélectionné :</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personnalise</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Affiche</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Luminosité</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Économiseur d'écran</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Activer l'économiseur d'écran automatique</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Message</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Délai</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Mot de passe de l'économiseur d'écran</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Calibrer l'écran tactile ...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Distant</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>Activer l'accès VNC</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>Basculer à l'accès VNC ou à la protection par mot de passe va déconnecter les connexions existantes.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Mot de passe pour accès à distance</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Ce mot de passe est utilisé pour tout accès distant, tel que vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>Il y avait une erreur au lancement de VNC.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Mot de passe nécessaire pour accès VNS</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Smart Access Anywhere ( Accès intelligent partout)</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Code d'accès</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>Adresse LAN IP </translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Adresse IP Wi-Fi IP</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Nombre de connexions VNC</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Mise à niveau</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>Impossible de mettre à niveau, l'alimentation AC n'est pas branchée. Veuillez brancher l'alimentation AC et réessayer.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>Impossible de détecter un équipement flash USB. Veuillez confirmer que l'équipement flash USB est bien inséré et essayez à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Impossible de mettre à niveau. Veuillez spécifier les serveur de mise à niveau et essayez à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>La connexion réseau n'est pas disponible. Veuillez vérifier la connexion réseau et essayez à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Impossible de contacter le serveur de mise à niveau. Veuillez vérifier l'adresse du serveur et essayez à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Une erreur a été reçue pendant le recherche des mises à niveau disponibles.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>Impossible de faire la mise à niveau, la mise à niveau sélectionnée a des fichiers manquants ou corrompus.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>Impossible de faire la mise à niveau, la mise à niveau sélectionnée a des fichiers corrompus.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>Échec en générant les données de mise à niveau de l'USB.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Pas de mises à jour trouvées, veuillez vérifier vos réglages et réessayer.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>La tentative de mise à niveau a échoué, veuillez essayer à nouveau. Si le problème persiste contactez votre représentant commercial.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Verrouillage de l'équipement</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Le verrouillage de l'équipement de test empêche un accès non autorisé.&#xA;L'économiseur d'écran est affiché pour protéger l'écran.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Verrouillez l'équipement de test</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Un mot de passe qui est exigé n'a pas été configuré. Les utilisateur pourront déverrouiller l'équipement de test sans entrer un mot de passe.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Réglages de mot de passe</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Ces réglages sont partagés avec l'économiseur d'écran.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Mot de passe exigé</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Audio</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Volume de l'écouteur</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Sourdine</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Volume du micro</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Recherche de satellites</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>latitude</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>longitude</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>altitude</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Horodatage</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Nombre de satellites en difficulté</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>moyenne SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Informations satellite individuelle</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Information système</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Établir une connexion au serveur ...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Connexion établie...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Téléchargement de fichiers en cours...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Le téléchargement de fichiers ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Téléchargement d'informations de mise à niveau en cours...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Fichier journal de Téléchargement ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Réussite de synchronisation avec le serveur.</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>En tenant bin. En attendant d' être ajouté à l'inventaire ...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Impossible de synchroniser avec le serveur .</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Perte de connexion durant la synchronisation. Vérifiez la configuration du réseau.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Impossible de synchroniser avec le serveur . Le serveur est occupé .</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Impossible de synchroniser avec le serveur . Erreur système détectée.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Impossible d'établir une connexion. Vérifiez la configuration du réseau ou ID de compte .</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Synchronisation interrompue.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Configuration des données terminée.</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Rapports complets.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Entrez l'ID de compte .</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>démarrer la synchronisation</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>arrêter la synchronisation</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Adresse internet du serveur</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>ID de compte</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Option</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Réinitialiser aux valeurs par défaut</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Lecteur Vidéo</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Navigateur Internet</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Déboguer</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Équipement série</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Autoriser Getty à contrôler l'équipement série</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Le contenu de /root/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Tests</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Réglage du test de calibrage</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Gestionnaire de tâche</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Information de tâche</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Numéro de tâche</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>L'information de tâche est automatiquement ajoutée aux rapports de test.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Historique</translation>
        </message>
    </context>
</TS>
