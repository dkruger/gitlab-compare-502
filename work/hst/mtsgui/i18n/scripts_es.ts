<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>Tramas de {1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>Tramas de {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>Paquetes de {1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>Paquetes de {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} ráfaga tramas: falla</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} ráfaga tramas: pasa</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} de {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} ráfaga paquetes: falla</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} ráfaga paquetes: pasa</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} Retrieving {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   ¿Desea reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Pulse OK para intentarlo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} Probando VLAN ID {2} para {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Esperando...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       Puede cambiar el nombre para crear una nueva configuración.</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 mayores conferenciantes (de {1} conversaciones IP)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 mayores conversaciones de retransmisión TCP (de un total de {1} conversaciones)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abort</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Abortar Test</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Activo</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Activar bucle</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>Bucle activo no exitoso. Prueba anulada por identificación de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Actual Test</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Añade rango</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Una tasa de pérdida de tramas que excedió el umbral configurado de pérdida de tramas</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>Después de hacer sus pruebas manuales o en cualquier momento en que lo necesite, usted puede</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Se encontró un bucle de hardware</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>No se encontró un bucle de hardware</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Todas las pruebas</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Una aplicación de bucle de retorno no es una aplicación compatible</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>No está disponible una medición de velocidad máxima de transmisión (throughput)</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>No se encontró un bucle activo</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analizar</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analizando</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>Y</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>y prueba RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>Se encontró un bucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>Se encontró un bucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Se edetectó un bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Se anexa el log al final del informe</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Nombre de la aplicación</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Tiempo total approximado:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Un rango teórico de valores de throughput FTP será calculado en base a los valores reales medidos en el enlace. Introducir el ancho de banda del enlace, retardo y encapsulación.</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Hubo una respuesta por agotamiento de tiempo de espera.&#xA;No hubo respuesta al último comando&#xA;en {1} segundos. </translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> Se supone un bucle HARD activo.          </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>Transmisiones asimétricas desde el extremo cercano hasta el lejano en modo ascendente y desde el extremo lejano al cercano en modo descendente. El modo combinado correrá la prueba dos veces, transmitiendo secuencialmente en la dirección ascendente, utilizando la configuración local y luego en la dirección descendente utilizando la configuración remota. Utilice el botón para sobreescribir la configuración remota con la configuración local actual.</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Intentando</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Intentando un bucle superior</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Intentando iniciar sesión en el servidor...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>Los intentos de consulta fallaron. Detención de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>El intento de hacer el bucle superior no fue exitoso</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Autonegociación</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Autonegociación hecho</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Configuración Autonegociación</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Estado Autonegociación</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Average Burst</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Tasa promedio del paquete</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Tamaño promedio del paquete</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Prom.</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Resultados de Jitter de paquete medio y máximo:</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Latencia prom. (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Latencia prom. (RTD): N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Fluctuación de fase prom de paquete:</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Jitter de paquete medio: n/d</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Jitter Paq. Prom. (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>Velocidad media</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Granularidad Tramas Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Granularidad Tramas Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test Tramas Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Resultados Test Tramas Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Tiempo máx para Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Tiempo máx para Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Resultados Test Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>De regreso al resumen</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularidad del ancho de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Granularidad del Ancho de Banda (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Exact. Medida Ancho Banda:</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Exact. Medida Ancho Banda</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>Prueba básica de carga</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Beginning of range:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Ambos</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Ambos, Tx y Rx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>Las direcciones IP de origen local y remota no están disponibles</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom Up</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Buffer Credit&#xA;(requiere Rendimiento)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Duración prueba Buffer Credits:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Duración prueba Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Buffer Credit Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Resultados medida Buffer Credit:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Buffer Credit Throughput {1} Bytes:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Rendimiento de Buffer Credit&#xA;(requiere Buffer Credit)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Buffer Credit Throughput Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Resultados medida Throughput Buffer Credit:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Buffer Size</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Ráfagas</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Granularidad Ráfaga (tramas)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>Mirando las retransmisiones TCP versus tiempo adicional de utilización de la red, es posible correlacionar el rendimiento deficiente de la red con condiciones de red con pérdidas, tales como la congestión.</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>Mirando la tabla de conversaciones IP, se pueden identificar los "mayores conferenciantes" por los bytes o por las tramas.  Las nomenclaturas "S &lt;- D" y "S ->D" se refieren a la dirección de tráfico origen a destino y destino a origen de los bytes y tramas.</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation> bytes</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Bytes&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Bytes&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Calculated&#xA;Frame Length</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Calculated&#xA;Packet Length</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Calculando...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>¡No puede proceder!</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Resumen del análisis de captura</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Duración de la captura</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturar&#xA;Pantalla</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>¡CUIDADO!&#xA;&#xA;¿Está seguro de querer borrar &#xA;esta configuración?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Tasa Conf (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Tasa Conf</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Tasa Conf (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>Chasis Identificación</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>El(los) item(s) Rx revisados será(n) usado(s) para realizar las configuraciones del filtro de origen.</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>El(los) item(s) Tx revisados será(n) usado(s) para realizar las configuraciones del Tx destino.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Revisando bucle activo</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Revisando en búsqueda de un bucle de hardware</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Revisando en búsqueda de un bucle activo</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Revisión de un bucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Revisando si hay un bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Revisando en búsqueda de detección de puertos semi dúplex</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Revisando en búsqueda de tramas ICMP</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Revisando en búsqueda de posibles transmisiones o utilización de gran ancho de banda</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Revisando bucle físico</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Revisión de un bucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Revisando un bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Revisando las estadísticas de la jerarquía del protocolo</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Revisando disponibilidad de dirección de origen ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>Revisando si esta casilla causará el restablecimiento de configuraciones de prueba a sus ajustes originales cuando termine la prueba. Debido a la prueba asimétrica serán restablecidos tanto el lado local como el remoto. Restablecer configuraciones ocasionará que el enlace se reinicie.</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Verifique las configuraciones de puerto entre el IP de origen y el dispositivo al que está conectado; verifique que no existe la condición semi dúplex.  Se puede lograr seccionalización adicional trasladando el analizador más cerca del IP de destino; determine si se eliminan retransmisiones para aislar el(los) enlace(s) en falla.</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Seleccione un archivo de captura para analizar</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>Seleccione&#xA;Archivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Seleccione la exactitud del ancho de banda que desee&#xA;( 1% se recomienda 1% para tiempos cortos de medida ).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Seleccione el tipo de login de control de flujo</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Seleccione preferencia del tamaño de la trama o paquete</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Seleccione la granularidad con la cual desearía ejecutar la prueba Back to Back.</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Seleccione la granularidad que le gustaría para el medida Pérdidas de Tramas.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Seleccione el máximo tiempo de intentos para la prueba Back to Back.</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Seleccione el número de pruebas que le gustaría realizar en la medida Back to Back para cada tamaño de trama.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Seleccione el número de pruebas que le gustaría realizar en la medida de Retardo (RTD) para cada tamaño de trama.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Seleccione el número de pruebas a realizar para la medida de Jitter de paquete para cada tamaño de trama.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Seleccione el porcentaje permitido para la tolerancia a Pérdida de Tramas en Throughput.&#xA;NOTA: Una config > 0.00 NO CUMPLE la RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Seleccione el tiempo de duración de cada prueba de Retardo(RTD).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Seleccione el tiempo que durará cada intento de fluctuación de paquete.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Seleccione el tiempo durante el cual se debe enviar una tasa, sin error, para aprobar la prueba de rendimiento.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Seleccione el tiempo que quisiera que durara cada intento de pérdida de trama.</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Seleccione el tiempo para la prueba de Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Protocolo de descubrimiento de Cisco</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>Se detectaron mensajes del protocolo CDP de Cisco (Cisco Discovery Protocol) en esta red y la tabla enlista aquellas direcciones y puertos MAC que anunciaron configuraciones semi dúplex.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Borrar todo</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Haga clic en el botón "Resultados" para conmutar a la interfaz estándar de usuario.</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violaciones Código</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Combinado</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation> Comentarios</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentarios</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Comunicación establecida con el equipo remoto</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>Comunicación no puede ser establecida con el equipo remoto</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>Comunicación con el extremo lejano cuando se ha perdido</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>completo</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completo</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>Finalizado&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Configuraciones</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuración</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation> Nombre de Config.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Nombre de Configuración:</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Nombre de Config.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>Necesario Nombre de Configuración</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Configuración de sólo lectura</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Resumen de configuración</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Configurar item(s) revisados</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Configure durante cuánto tiempo {1} enviará tráfico.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Confirmación para Reemplazar Configuración</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Confirmación para Borrar</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Conectado</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Conectar a Aplicación de Medida</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continúe en semi dúplex</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Continuando en el modo semi dúplex</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Copiar configuración local&#xA;a configuración remota</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Copy&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>Podría no hacer el bucle superior en el extremo remoto</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crear Informe</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>credits</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(credits)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA;Script Actual: {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Current Selection</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation> Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Tasa de bits de datos</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Tasa de bytes de datos</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Parado capa datos</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo Datos</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Modo datos configurado a PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Tamaño de datos</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation> Fecha</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>FECHA &amp; HORA</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>días</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Retardo, act (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Retardo, Act (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Dirección Destino</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Configuración destino</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>Destino ID</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Destino IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>Dir IP&#xA;Destino</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>No se encontró la MAC de destino para dirección IP {1}</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>MAC Destino encontrada.</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>Direcc MAC dest</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>Etiqueta de detalle</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalles</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>detectado</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>Detected</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Detectado ancho de banda del enlace</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Se han detectado más tramas de las transmitidas en {1} del Ancho de Banda - Test inválido.</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Determinando la velocidad de transmisión simétrica</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Detalles de dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ID del dispositivo</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>No están disponibles los parámetros DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>Parámetros DHCP detectados.</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Dispositivos descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Descubriendo</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Descubriendo el tipo de bucle extremo lejano...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Descubrimiento&#xA;no está&#xA;disponible&#xA;actualmente</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Visualizado por:</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Dirección descendente</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> Do you wish to proceed anyway? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Dúplex</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duración</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Activado</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Activar prueba de tráfico capa 2 extendida</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Encapsulación:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulación</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>End of range:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Introducir la dirección IP o nombre del servidor contra el que realizar la prueba FTP.</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Enter the Login Name for the server to which you want to connect</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Introducir la password de la cuenta que quiere usar</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Introducir nombre para la nueva configuración&#xA;(Utilice sólo letras, números, espacio, guiones y guión bajo):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Error: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ERROR: Hubo una respuesta por agotamiento de tiempo de espera.&#xA;No hubo respuesta en</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Error: no se ha podido establecer la conexión</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Total errores</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Tramas con Error</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Error al cargar el archivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Error: Primary DNS failed name resolution.</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Error: unable to locate site</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Tiempo estimado para terminar</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Tiempo estimado restante</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>Informe de Medida Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>El registro del evento está lleno.</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Se encontraron retransmisiones excesivas</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>Salir de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Velocidad de transmisión esperada</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>La velocidad de transmisión esperada es</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>No está disponible la velocidad de transmisión esperada</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>Botón "Prueba RFC 2544 experto".</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explícito (Puerto- E)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Explícita (Fabric/N-Port)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Explicit login was unable to complete. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FAIL</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>FALLO</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>El extremo alejado es una batería de pruebas de JDSU Smart Clase Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>Equipo remoto es un Viavi SmartClass Ethernet</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>La prueba FC se ejecuta con Acterna Test Payload</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>Informe_de_Medida_FC</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Aceptar FDX</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Informe de Medida FC</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>Fichero de configuración</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nombre del Fichero</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Archivos</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>Tamaño archivo</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>File Size:</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>File Size</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>File Size: {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>Tamaños de fichero:</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>Tamaños de fichero</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>El archivo fue transferido muy rápidamente.  Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Encontrando la velocidad de rendimiento esperada</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Encontrando los "mayores conferenciantes"</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>Los primeros 50 puertos semi dúplex (de un total de {1})</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>Los primeros 50 mensajes ICMP (de un total de {1})</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Control de flujo</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flow Control Login Type</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Carpetas</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> for each frame is reduced to half to compensate double length of fibre.</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>encontrado</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Se encontró bucle activo.</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Se encontró un bucle en hardware.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Trama&#xA;Longitud</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Longitud trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Longitud Trama (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Longitudes Trama:</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Longitudes Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Longitudes Trama a medir</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Pérdidas de Tramas (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Pérdida Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Pérdidas de Tramas de {1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Pérdidas de Tramas de {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Gran. Ancho Banda Pérd. de Tramas:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Gran. Ancho Banda Pérd. de Tramas</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Frame Loss Maximum Bandwidth</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Frame Loss Minimum Bandwidth</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Frame Loss Rate</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Ratio Pérdidas de Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Medida Pérdidas de Tramas</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Frame Loss Test Procedure:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Frame Loss Test Procedure</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Resultados Medida Pérdidas de Tramas:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolerancia Pérdida Tramas (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Dur. de la prueba Pérd. de Tramas:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Dur. de la prueba Pérd. de Tramas</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Trama o Paquete</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>tramas</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Tramas</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>tamaño de trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamaño Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Tamaño Trama:  {1} Bytes</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Tramas&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Tramas&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Tramas</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(frms)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(frms/sec)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>Throughput FTP</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>FTP Throughput Test&#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>¡Se ha completado la medida de Throughput FTP!</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>Informe de la prueba de caudal de FTP</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>Obtener inf PCAP</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Puertos semi dúplex</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Bucle de hardware</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>(Hardware&#xA;o Activo)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>(Hardware,&#xA;Permanente&#xA;o Activo)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Aceptar HDX</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Alta utilización</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>horas</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>Prueba de caudal de HTTP</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>Informe de la prueba de caudal de HTTP</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>Código&#xA;ICMP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>Mensajes ICMP</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Si los contadores de error están incrementando de manera esporádica active el modo manual</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Si persiste el problem favor de actualizar la predeterminada configuración del Menú de herramientas.</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Si no puede resolver el problema con los errores esporádicos usted puede configurar</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implícito (enlace transparente)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>Información</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Inicializando comunicaciónes con</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Para determinar el ancho de banda en el cual </translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>Taza de entrada para el equipo remoto y local no coinciden</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Se están observando problemas intermitentes en la línea.</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Error Interno</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Error interno - reiniciar PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config Inválida</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Configuración IP no válida</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>Direcciones IP</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>Conversaciones IP</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>está saliendo</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>está comenzando</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Prueba de Jitter</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>La J-QuickCheck está completa</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck perdió el enlace o no fue capaz de establecerlo</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>C1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>C2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>La prueba de tráfico L2 se puede relanzar ejecutando de nuevo J-QuickCheck.</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>Pruebas de latencia (RTD) y jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Promedio latencia (RTD): N/A</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Umbral Pasa para Retardo (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Umbral Pasa para Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Retardo (RTD)&#xA;(requiere la medida de Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Resultados de latencia (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Test Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Resultados de la Prueba de Retardo (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Resultados de medida de Latencia (RTD): ABORTADO</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Latency (RTD) Test Results: FAIL</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Latency (RTD) Test Results: PASS</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Resultados de prueba de latencia (RTD) omitidos</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Prueba de latencia (RTD) omitida</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> Latency (RTD) Threshold: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Latency (RTD) Threshold (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Duración de la Prueba de Retardo (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Duración de la Prueba de Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Retardo (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Capa 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Capa 1 / 2&#xA;Condición de Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Capa 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Enlace capa 2 Activo</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Prueba rápida Capa 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Capa 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>Capa 3&#xA;Condición IP</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Capa 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>Capa 4&#xA;Condición TCP</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR Loop</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Long.</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Se ha encontrado enlace</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Protocolo de descubrimiento Capa de Enlace</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Enlace perdido</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Se detectó velocidad de enlace en archivo de captura</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Puerto de escucha</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Load Format</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>CARGANDO ... Espere, por favor</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>La dirección IP local de destino está configurada para</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>La dirección MAC local de destino está configurada para</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>El puerto local de destino está configurada para</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>El tipo de bucle local está configurado para Unicast</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Puerto local</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>La dirección IP remota local está configurado para</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> Número de serie local</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Configuración local</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> Revisión de software local</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>El filtro IP local de origen está configurado con</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>El filtro MAC local de origen está configurado con</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>El filtro local de puerto de origen está configurado con</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Resumen local</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> Nombre del instrumento local</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Ubique el dispositivo con la(s) dirección(es) MAC y el(los) puerto(s) listado(s) en la table y asegúrese que las configuraciones dúplex están definidas como "completo" y no "automático".  No es extraño para un anfitrión ser configurado como "auto" y el dispositivo de red configurado como "auto" y el enlace negocia de manera incorrecta a semi dúplex.</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation> Ubicación</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Ubicación</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Login:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Login</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Nombre login:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Nombre login</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Bucle fallido</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Desactivando bucle en el extremo remoto...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Activando bucle en el extremo remoto...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Estado del bucle desconocido</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Falló el bucle superior</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Bucle superior tuvo éxito</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Bucle activado con éxito</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>¡Se detectó pérdida de enlace en capa 2!</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Perdido</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Tramas Perdidas</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Dirección de la Gerencia</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>Tipo MAU</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx.</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MÁX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( máx {1} caracteres )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Max Avg</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Máx fluctuación de fase prom paq:</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Jitter de paq med máx: N/D</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Jitter Paq. Prom. máx (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Máx. ancho de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Max Bandwidth (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Max Buffer Size</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>Máximo del valor medio del retardo, que se permite "Pasar" en la prueba de Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Jitter de paquete máx y med permitidos para ""Pasa"" para la prueba de Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Máximos Buffer Credits en Rx</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Medida Ancho de Banda Máximo:</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Medida Ancho de Banda Máximo</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Máxima velocidad de transmisión medida:</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Tiempo límite máximo de {1} por ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Tiempo límite máximo de 7 días por ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Tiempo Máximo de la prueba (segundos)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Máximos Buffer Credits en Tx</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>tasa máx</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Se logró un máx. de intentos de retransmisión. Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Measured</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Measured Rate(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Measured Rate</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Measured Rate(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Exactitud de la Medida</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Midiendo Throughput en {1} Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensaje</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimo</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Porcentaje Mínimo del Ancho de Banda</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>Porcentaje ancho de banda mínimo requerido para "Pasar" la medida Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Tiempo límite mínimo de 5 segundos por ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Min Rate</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>mins</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>minuto(s)</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>minutos</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modelo</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Modificar</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>Encapsulación MPLS/VPLS actualmente no soportada ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/A (bucle físico)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Red Activa</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Utilización de red</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>No se detectó que la utilización de red fuera excesiva, pero este cuadro suministra una gráfica de utilización de red</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>No se detectó que la utilización de red fuera excesiva, pero esta tabla suministra una lista de los mayores conversadores en IP</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Nuevo</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Nombre Nueva Configuración</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Nuevo URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Siguiente</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>No existe una aplicación compatible</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt;CONFIGURACIÓN NO DISPONIBLE></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>No se han  seleccionado los ficheros para la prueba</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>No se halló un bucle de hardware </translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>No se han detectado dispositivos JDSU</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>¡No se detectó enlace en capa 2!</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>No se pudo establecer el bucle</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>No se podrían establecer o hallar bucles</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>No se encontró un bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>No existe una aplicación vigente</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>NO CUMPLE la RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>No está conectado</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>No determinado</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>NOTA:  Una config > 0.00</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Nota: se asume bucle hard con Buffer credits menos que 2.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;Nota: Asume un bucle duro si los creditos de acumulación son menos que 2.&#xA; Esta prueba no es válida.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Note: Based on the hard loop assumption, minimum buffer credits calculated</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Note: Based on the hard loop assumption, throughput measurements are made at twice</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Nota: Una vez que use una Tolerancia de pérdidas de la trama, la prueba no cumple</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Notas</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>No Seleccionado</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>No&#xA;se descubrieron&#xA;dispositivos&#xA;Viavi</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>Saliendo...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>Verificando ahora</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Verificando ahora con {1} credits.  Esto llevará  {2} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Número de Pruebas Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Número de Pruebas Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Number of Failures</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Number of IDs tested</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Número de Pruebas de Retardo (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Número de Pruebas de Retardo (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Número de pruebas de Jitter de paquete:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Número de pruebas de Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Número de paquetes</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Number of Successes</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Número de Pruebas:</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Número de Pruebas</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>de</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Off</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>de tramas fueron perdidas dentro de un segundo.</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>del rendimiento esperado de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(% de tasa lín.)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% de tasa lín.</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>de tasa lín.</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * Only {1} Trial(s) yielded usable data *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(ENCENDIDO o APAGADO)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Tramas OoS</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID  Autor</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Fuera de Rango</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>    Resultados Totales Medida: {1}  </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>Resultados globales: Abortado</translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Por encima del Rango</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>en los últimos 10 segundos, aún cuando el tráfico debería estar detenido</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Jitter Paquete, med</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Jitter de paquete pasa el umbral:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Jitter de paquete pasa el umbral</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Jitter de paquete&#xA;(requiere la medida de Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Resultados de fluctuación de fase del paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Packet Jitter Test</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Resultados de medida de Jitter de paquete: ABORTADO </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Packet Jitter Test Results: FAIL</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Packet Jitter Test Results: PASS</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> Packet Jitter Threshold: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Packet Jitter Threshold (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Duración de prueba de Jitter de paquete:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Duración de prueba de Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Longitud&#xA;de paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Longitud Paquetes (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Longitudes Paquete:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Longitudes Paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Longitudes Paquete a Medir</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>tamaño del paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Tamaño Paquete</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASA</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Pasa / Fallo</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>PASA/FALLO</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Tasa de paso (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Tasa de paso (trama/seg)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Tasa de paso (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Tasa de paso (trama/seg)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Contraseña:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Contraseña</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Anunciar Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Aceptar Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Det. pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Se han detectado Tramas Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Se han detectado Tramas Pause - Prueba inválida</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Error de análisis sintáctico en archivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>Archivos PCAP</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>Pendiente</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Performing cleanup</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>(Permanente&#xA;o Activo)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pkt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Jitter Paq (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Pkt Length</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pérd. Paq.</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(pkts)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(pkts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Plataforma</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Por favor, revise si tiene sincronismo y enlace,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Por favor, compruebe que está conectado adecuadamente,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Por favor, compruebe que está conectado adecuadamente,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Please choose another configuration name.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Por favor, introduzca un nombre de archivo para guardar el informe (máx. caracteres) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Please enter any Comments you have ( max {1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Please enter any Comments you have max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca cualquier comentario que tenga (máx {1} caracteres)&#xA;(Use letras, números, espacios, guiones y subraya solamente)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Please enter your Customer's Name max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Por favor, introduzca el nombre de su Cliente (máx. %{1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca su nombre de cliente&#xA;(máx. {1} caracteres) &#xA;(Use letras, números, espacios, guiones y subraya solamente)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca su nombre de cliente (máx. {1} caracteres)&#xA;(Use letras, números, espacios, guiones y subraya solamente) </translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca su nombre de cliente (máx. {1} caracteres)&#xA;(Use letras, números, espacios, guiones y subraya solamente) </translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Please enter your Technician Name ( max {1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Please enter your Technician Name max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca su nombre de técnico (máx. {1} caracteres)&#xA;(Use letras, números, espacios, guiones y subraya solamente) </translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Please enter your Test Location ( max {1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Please enter your Test Location max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Por favor, introduzca su ubicación de prueba (máx. {1} caracteres)&#xA;(Use letras, números, espacios, guiones y subraya solamente) </translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Por favor, presione el botón "Conectar a remota"</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Por favor, verifique el rendimiento del enlace con una prueba manual de tráfico.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Por favor, verifique sus direcciones IP de origen local y remoto e inténtelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Por favor, verifique su dirección IP de origen local e inténtelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>verifique su dirección IP remota y pruebe de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Por favor, verifique su dirección IP de origen remoto e inténtelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Por favor espere ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Por favor Espere ....</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Please wait while the PDF file is written.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Por favor espere. Se está escribiendo el fichero del PDF.&#xA;Esto requiere hasta 90 segundos ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Puerto:</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Puerto {1}: Desea guardar un reporte de la prueba?&#xA;&#xA;Oprima "Si" or "No".</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>ID de puerto</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Puerto:				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Puerto:				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Activo</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP Auten. Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP Fallo Desconocido</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Activo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE Inactivo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE Iniciado</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>Estado PPPoE: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE Espera Máxima</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE Up</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>Falló PPPPoE </translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP Espera Máxima</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP Fallo desconocido</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP Fallo Up</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP Fallo</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>Pulse "Cerrar" para volver a la pantalla principal.</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>Presione "Salir" para regresar a la pantalla principal o "Ejecutar" para ejecutarlo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Presione&#xA;botón&#xA;de Refrescamiento&#xA;para&#xA;descubrir</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Presione el botón "Salir de J-QuickCheck" para salir de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Presione el botón Refrescar para descubrir dispositivos Viavi actualmente en la subred. Seleccione un dispositivo para ver detalles en la tabla a la derecha. Si Refrescamiento no está disponible, revise para asegurar que Descubrimiento está habilitado y que tiene sincronismo y enlace.</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Presione el botón "Salir de J-QuickCheck" para verificar la configuración de prueba local y remota y el ancho de banda disponible</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Prev</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>Progreso</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Propiedad</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Próximos pasos propuestos</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Proveedor</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Abandonar</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aleatorio</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> Rango</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Margen</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Tasa</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Tasa (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>califica el enlace puede tener problemas generales no relacionados con la carga máxima.</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>Received {1} bytes from {2}</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Tramas Recibidas</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Recomendación</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Configuración recomendada para la prueba manual:</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Refrescar</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Dirección IP remota</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Bucle remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation> Número de serie remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>Número de serie remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Configuración remota</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Las configuraciones remotas pudieran no ser restablecidas</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> Revisión de software remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Versión del software remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Resumen remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation> Nombre del instrumento remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Nombre del instrumento remoto de prueba</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Oprima el rango</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID Responder</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>Respondiendo&#xA;IP de Router</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>Reinicie J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Restablezca las configuraciones previas a la prueba antes de salir</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Restaurando configuraciones del equipo de prueba remoto ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>Restringir RFC a </translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>El resultado de la prueba básica de carga no está disponible, por favor haga clic en "Próximos pasos propuestos" para posibles soluciones</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Resultados a monitorizar:</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Retransmisiones</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Se encontraron retransmisiones&#xA;Analizando tiempo adicional de ocurrencias de retransmisiones </translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>se encontraron retransmisiones. Por favor, exporte el archivo a USB para análisis posterior.</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>Retrieval of {1} was aborted by the user</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>regrese a la interfaz de usuario RFC 2544 haciendo clic en el </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>Informe de prueba Ethernet RFC 2544</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Norma RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>RFC 2544 Test</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>Prueba RFC 2544 se ejecuta utilizando Acterna Test Payload</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>Informe_de_Medida_RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>Una medida RFC/FC no se pueden activar al estar activos los Resultados Gráficos MultiFlujo</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY Det</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Ejecutar</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>Correr Prueba FC</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Correr J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>Ejecutar $l2quick::testLongName</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Lanzando prueba en {1}{2} carga.</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Realizando prueba a carga {1}{2}.  Se tardará {3} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>Correr prueba RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Iniciar Script</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Mbps en Rx, Act C1</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx Sólo</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>Salvar el informe de medida de Throughput FTP</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>Salvar el informe de medida de Throughput HTTP</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>Archive el Reporte de exploración VLAN</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> ancho de banda escalado</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Impresión Pantalla capturada</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Script abortado.</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>segs</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(Segs)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>Segs</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Select a name for the copied configuration</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Select a name for the new configuration</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Select a range of VLAN IDs</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Seleccionada la dirección Tx:</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Seleccionada la dirección Tx&#xA;Descendente</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Seleccionada la dirección Tx&#xA;Ascendente</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Aviso Selección</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Seleccione "OK" para modificar la configuración&#xA;Puede cambiar el nombre para crear una nueva configuración.&#xA;(Utilice sólo letras, números, espacio, guiones y guión bajo)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Seleccionar Configuración Medida:</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Seleccione la propiedad según la cual desea ver los dispositivos listados descubiertos.</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Seleccione las medidas que le gustaría realizar:</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>Seleccione URL</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Seleccione cuál formato utilizar para configuraciones relacionadas con la carga.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Enviando peticon ARP para MAC destino.</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Enviando tráfico para </translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>envía tráfico, el rendimiento esperado descubierto por J-QuickCheck se escalará por este valor.</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID Secuencia</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation> Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>IP servidor:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>IP servidor</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nombre del Servicio</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Mostrar estado de Pasa/Falla</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;Mostrar estado de Pasa/Falla para:</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Size</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Skip</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Omitiendo la prueba de latencia (RTD) y continuando</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Rev de software</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> Revisión software</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Dirección origen</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>No está disponible la dirección de origen</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Se estableció la disponibilidad del origen ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID Fuente</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>Dirección&#xA;IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>Dirección IP de origin es la misma que la dirección destino</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Fuente</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Dirección&#xA;MAC Fuente</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Especifique el ancho de banda del enlace</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Directorio</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocidad (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>INICIO</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Iniciando prueba básica de carga</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Iniciando prueba</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Estado desconocido</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>FIN</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Estudie el gráfico para determinar si las retransmisiones TCP alinean con la utilización de red degradada.  Mire la pestaña de retransmisiones TCP para determinar la IP de origen que está causando retransmisiones TCP significativas. Revise las configuraciones de puerto entre la IP de origen y el dispositivo al que está conectado; verifique que no existe la condición semi duplex.  Se puede lograr seccionalización adicional trasladando el analizador más cerca de la IP de destino; determine si se eliminan retransmisiones para aislar el(los) enlace(s) en falla.</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>¡Con éxito!</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>¡Con éxito!</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumen</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Summary of Measured Values:</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Summary of Page {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridad Usuario SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>Modo simétrio transmite y recive usando el loopback local. Moso asimétrico transmite del local al remoto en modo Upstream y del remoto al local en modo Downstream</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simetría</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Haga clic en un nombre de configuración para seleccionarla</translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	Get {1} MB file....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	Poner archivo de {1} MB....</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Tasa: {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Tasa: {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx Tramas {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              ¡PRECAUCIÓN!&#xA;&#xA;	    ¿Está seguro de que desea eliminar&#xA;	           permanentemente esta configuración?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Por favor, ¡use letras, números, espacios,&#xA;		   guiones y subrayados solamente!</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Esta configuración es de sólo lectura&#xA;		   y no se puede eliminar.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Debe introducir un nombre para la nueva&#xA;		           configuración utilizando el teclado.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   La configuración especificada ya existe.</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Hora: {1} segundos&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tramas de Tx {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>El Host de TCP no pudo establecer una conexión. Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>El Host de TCP encontró un error. Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>Retransmisiones TCP</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation> Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nombre Técnico</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Terminación</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>                              Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Prueba anulada por el usuario.</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Prueba del ajuste de ancho de banda máximo configurado desde la pestaña Preparación - Todas las pruebas</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>prueba en diferentes tasas de tráfico más bajas. Si todavía consigue errores aún en la más baja</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Configuración de prueba:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuración de prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Duración test</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Duración de la prueba: Al menos 3 veces la duración de la prueba configurada.</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Ancho de banda probado</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Prueba completa ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Midiendo {1} credits </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Midiendo {1} credits </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Probando en  </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Analizando conexión... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> Nombre del instrumento de prueba</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Test is starting up</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Test Log:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nombre Test:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Nombre de la prueba:			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Nombre de la prueba:			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Test Procedure</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Test Progress Log</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Rango de la prueba (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Test Range (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Resultados de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Configuración Test</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Medidas a realizar:</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Medidas a realizar</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Test was aborted</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>El bucle remoto activo falló y no se encontró un bucle hardware</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>El bucle remoto  activo falló y no se encontró un bucle Hard o permanente</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>The average rate for {1} was {2} kbps</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>The average rate for {1} was {2} Mbps</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>El archivo excede el limite de 50000 paquetes para JMentor</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>el umbral de tolerancia de pérdida de tramas para tolerar tasas pequeñas de tramas perdidas</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>El Protocolo de mensajes de control de internet (ICMP, Internet Control Message Protocol), es ampliamente conocido en el contexto del "ping" de IMCP. El mensaje "Destino ICMP inaccesible" indica que el enrutador o dispositivo de red no ha logrado llegar a un destino.</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>El conteo de trama Acterna Rx con filtro C2 ha continuado incrementando</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>El bucle LBM/LBR falló.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>Las direcciónes IP locales y remotas son idénticas</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation>Los ajustes en las configuraciones locales se copiaron exitosamente en la configuración remota</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>La dirección IP de origen local está Indisponible</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>La MTU medida es demasiado pequeña para continuar. Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>The name you chose is already in use.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>El puerto de elemento de red al cual estamos conectados está preparado para modo semi dúplex. Si esto es correcto, presione el botón "Continúe en semi dúplex". De lo contrario, presione "Salir de J-QuickCheck" y prepare de nuevo el puerto.</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>El diagrama de utilización de la red muestra el ancho de banda consumido por todos los paquetes en el archivo de captura durante el tiempo de captura.  Si fueran detectadas las retransmisiones TCP, se aconseja estudiar los resultados de la Capa TCP,  regresando a la pantalla principal de análisis.</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>the number of buffer credits at each step to compensate for the double length of fibre.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Cálculo teórico</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Theoretical &amp; Measured Values:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>El puerto compañero (elemento de red) tiene AutoNeg OFF y el caudal esperado no está disponible, por lo tanto, el puerto compañero está principalmente en modo semi dúplex. &#xA;Si el modo semi dúplex en el puerto compañero no es correcto, cambie por favor las configuraciones en el puerto compañero a dúplex total o bidireccional y ejecute el J-QuickCheck. Después de ello, &#xA;si el caudal esperado medido es más razonable, puede ejecutar la prueba RFC 2544. Si el caudal esperado no está todavía disponible, revise las configuraciones del puerto en el &#xA;lado remoto. Tal vez haya un desacople en el modo de puerto HD a FD.&#xA;&#xA;Si es correcto el semi dúplex en el puerto compañero vaya, por favor, a Resultados -> Configuración - > Interfaz -> Capa &#xA;Física y cambie la configuración de dúplex de bidireccional a semi dúplex. Luego regrese al guión de RFC2544 (Resultados -> Prueba experta RFC2544), salga de J-QuickCheck, vaya a la pestaña de caudal y seleccione &#xA;Proceso de introducción de ceros "Norma RFC 2544 (semi dúplex)" y ejecute la Prueba RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>Existe un problema de comunicación con el extremo lejano.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>el dispositivo remoto en bucle no responde al comando de bucle de retorno Viavi, pero retorna las tramas transmitidas al dispositivo local con los campos de dirección de origen y destino intercambiados</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>el dispositivo remoto en bucle responde al comando de bucle de retorno Viavi y retorna las tramas transmitidas al dispositivo local con los campos de dirección de origen y destino intercambiados</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>el dispositivo remoto en bucle regresa las tramas transmitidas sin cambio al dispositivo local</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>el dispositivo de bucle remoto soporta OAM LBM y responde a una trama LBM recibida, transmitiendo de regreso una trama LBR correspondiente, a un dispositivo local.</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>El lado remoto está en el momento está configurado para encapsulción MPLS</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>El lado remoto parece ser una aplicación de bucle de retorno</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>La dirección IP de origen remoto no está disponible</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA;The report has been saved as "{1}{2}" in PDF format</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>The report has been saved as "{1}{2}" in PDF format</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>El informe ha sido guardado como "{1}{2}" en formatos PDF, TXT y LOG</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>El informe ha sido guardado como "{1}{2}" con formatos TXT y LOG</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>El IP del enrutador que responde no puede dirigir el paquete a la dirección IP de destino, de manera que se debería realizar el análisis de problemas entre el IP del enrutador que responde y el destino.</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>La prueba RFC 2544 no soporta la encapsulación MPLS.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>Se ha activado el láser de Transmisión&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>¡Se ha desactivado el láser de Transmisión!&#xA;¿Activar láser?&#xA;Pulse "Sí" para acitvarlo o "No" para abortarlo.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>¡Se ha desactivado el láser de Transmisión! ¿Activar láser?&#xA;&#xA;Pulse "Sí" para acitvarlo o "No" para abortarlo.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>La pruba de exploración de VLAN no puede ser completada con OAM CCM vigente.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>La pruba de exploración de VLAN no puede ser configurada.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Esta configuración es de sólo lectura y no puede ser modificada.</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Este debe ser la dirección IP remota cuando en modo asimétrico.</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>Esta tabla identifica las direcciones IP de origen que están experimentando las retransmisiones TCP. Cuando se detectan las retransmisiones TCP, ésto podría ser debido a la pérdida de paquetes descendentes (hacia el lado destino).  También podría indicar que hay una orden del puerto semi dúplex.</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>La prueba ejecuta usando Acterna Test Payload</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Esta prueba es invalida.</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Esta prueba requiere que el tráfico tenga una encapsulación VLAN. Asegúrese de que la red conectada entregará una dirección IP para esta configuración.</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Se tardará {1} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Throughput ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Pruebas de velocidad de transmisión y latencia (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Pruebas de velocidad de transmisión y fluctuación de fase de paquete</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Tolerancia Pérd. Tramas Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Tolerancia Pérd. Tramas Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Pruebas de velocidad de transmisión, latencia (RTD) y fluctuación de fase de paquete</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Umbral Pasa Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Umbral Pasa Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Escala de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Factor de escala del rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>La duración de la prueba de velocidad de transmisión fue de {1} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Resultados Medida Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados Medida Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Resultados medida Throughput: ABORTADO   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Throughput Test Results: FAIL</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Throughput Test Results: PASS</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Umbral de caudal (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> Umbral Througput: {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Umbral Througput (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Duración Prueba Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Duración Prueba Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Proceso de puesta a cero en velocidad de transmisión:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Proceso de puesta a cero en velocidad de transmisión</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation> Fin</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Tiempo por ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Time: (seconds)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>Tiempo&#xA;(segs)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation> Hora de inicio</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Hora de inicio</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>Veces visitados</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Para continuar, por favor, revise su conexión de cable y luego reinicie J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Para determinar el rendimiento máximo, seleccione el método de la norma RFC 2544, que acopla el conteo de tramas de Tx y Rx o el método Viavi Mejorado, que utiliza el Prom. %Util. medido.</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Para ahorrar tiempo la latencia (RTD) en modo asimétrico debería ejecutarse en una sola dirección</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>para  ver si hay esporádicas o constantes pérdida de tramas.</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Total de bytes</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Tramas&#xA;totales</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Numbero total</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Total Util {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>Total Util (kbps):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>Total Util (Mbps):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>To view report, select "View Report" on the Report menu after exiting {1}.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>Dentro de</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Tráfico: Constante con {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Resultados de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>El tráfico estaba siendo todavía generado desde el extremo remoto</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>¡ Láser de Transmisión está desactivado !</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Tramas Transmitidas</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Transmitiendo aguas abajo</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Transmitiendo aguas arriba</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Prueba</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Prueba {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Trial {1} complete&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Prueba {1} de {2}:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Duración de la prueba (segundos)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>pruebas</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Intentando una segunda vez</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>error tshark</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>Credits Buffer to Buffer en Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Dirección de Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Láser de Tx desactivado</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Mbps en Tx, Act C1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx Sólo</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> No es posible activar el bucle remoto de forma&#xA; automática. </translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>¡No es posible conectar con la aplicación de Medida!</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>¡No es posible conectar con la aplicación de Medida!&#xA;Pulse "Sí" para reintentar o "No" para abortarlo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>No se pudo obtener una dirección DHCP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>No es posible realizar es test RFC2544 con Bucle Local activado.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>No se pudo ejecutar la prueba</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Unable to run VLAN Scan test with Local Loopback enabled.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>INDISP.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Identificador Equipo</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>ASC</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>(Arriba o abajo)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Dirección ascendente</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>El usuario ha abortado la prueba</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>Test Cancelado por Usuario</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nombre de usuario</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridad Usuario</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>El Usuario Requiere Inact</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Usuario seleccionado&#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Usuario seleccionado ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Utilice la pantalla de resumen de estado para mirar los eventos de error</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Usando</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Usando el tamaño de trama de </translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Utilización y retransmisiones TCP</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>Values highlighted in blue are from actual tests.</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Verificando que el enlace está activo...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>verifique su dirección IP remota e inténtelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>verifique su dirección IP remota e inténtelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Mejorado</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Rangos de VLAN a probar</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN Scan Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAN Scan Test Results</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>¡Trama VTP/DTP/PAgP/UDLD detectada!</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>Esperando hasta que AutoNegociación finalice ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>Esperando MAC de destino para&#xA;  dirección IP</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>Esperando Parámetros DHCP ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>Esperando enlace capa 2 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>Esperando por el enlace</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>Esperando por la activación OWD, sincronización ToD y sincronización 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Waiting for successful ARP ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>fue detectado en el último segundo.</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Tamaño Website</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Tenemos un bucle activo</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>We have an error!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>Cuando pruebe los enlaces semi dúplex seleccione la norma RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Window Size/Capacity</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>con la recomendación RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>¿Le gustaría guardar un informe de prueba?&#xA;&#xA;Pulse "Sí" o "No".</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>Usted también puede usar la gráfica de resultados Curva de tasa de pérdida de trama</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>No se puede iniciar el script desde {1}.</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Usted puede necesitar esperar hasta que se detenga para reconectar</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Buscando la tasa máxima de throughput</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Zeroing in on optimal credit buffer</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Proceso puesta a cero</translation>
        </message>
    </context>
</TS>
