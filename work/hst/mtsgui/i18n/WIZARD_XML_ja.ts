<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>前の構成を編集</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの構成の読み込み</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>新しい構成の開始 ( ﾃﾞﾌｫﾙﾄに戻す )</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>手動</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>ﾃｽﾄ設定の手動構成</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>テスト設定値</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの保存</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>終了 : 手動構成</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>ｽﾄｱｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの読み込み</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>終了 : ﾌﾟﾛﾌｧｲﾙの読み込み</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>構成の編集</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>実行</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>CPRI チェックを実行</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>SFP の検証</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>ﾃｽﾄ終了</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>ﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>ﾃｽﾄの反復</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>結果詳細の表示</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>CPRI チェックの終了</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>確認</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>ｲﾝﾀﾌｪｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>ﾚｲﾔ 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>終了 : ﾚﾋﾞｭー結果</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>ﾚﾎﾟｰﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>終了 : ﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>結果詳細の確認</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>未完了</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>完了</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI ﾁｪｯｸ:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** CPRI チェックを開始します ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** ﾃｽﾄが終了しました ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** ﾃｽﾄは中止されました ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>ﾌﾟﾛﾌｧｲﾙ保存のｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>このﾃｽﾄに使用した構成を保存できます。 &#xA;&#xA; この構成は ( 全体として、あるいは &#xA; ﾌﾟﾛﾌｧｲﾙを個別に選択して ) 今後のﾃｽﾄに使用できます。</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>ﾌﾟﾛﾌｧｲﾙ読み込みのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>ローカル SFP の検証</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>ｺﾈｸﾀ</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>SFP を挿入してください。</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>SFP 波長 (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>SFP ﾍﾞﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>SFP ﾍﾞﾝﾀﾞ Rev</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>SFP ベンダー P/N</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>推奨レート</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>SFP の追加データを表示</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP は正常です。</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>このレートでは SFP を検証できません。</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP は不適格です。</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>設定 </translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>ﾃｽﾄ持続時間</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>遠端デバイス</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>その他</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>ﾊｰﾄﾞ ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>光受信レベルの上限 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>光受信レベルの下限 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>ラウンドトリップ遅延上限 (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>CPRI チェックをスキップ</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>ﾃｽﾄ状態ｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>定期</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>ﾃｽﾄ &#xA; の実行</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>ﾃｽﾄ停止</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>ローカル SFP の結果</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>SFP は検出されませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>波長 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>最大 Tx ﾚﾍﾞﾙ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>最大 Rx ﾚﾍﾞﾙ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>ﾄﾗﾝｼｰﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>インターフェイスの結果</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI チェック テストの判定</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>インターフェイス テスト</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>レイヤ 2 のテスト</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>信号検知</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>同期 取得</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Rx 周波数 最大 偏差 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>ｺｰﾄﾞ 違反</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>光 Rx ﾚﾍﾞﾙ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>レイヤ 2 の結果</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>ｽﾀｰﾄｱｯﾌﾟ状態</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>ﾌﾚｰﾑ 同期</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>RTD 結果</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>往復遅延平均 (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>BERT 結果</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>ﾊﾟﾀｰﾝ同期</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>ﾊﾟﾀｰﾝ 損失</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Bit/TSE ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>機器のﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>L1 同期</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>ﾌﾟﾛﾄｺﾙ ｾｯﾄｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M ﾌﾟﾚｰﾝ ｾｯﾄｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>操作</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>ﾊﾟｯｼﾌﾞ ﾘﾝｸ</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>ﾚﾎﾟｰﾄ作成のｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>ﾃｽﾄ ﾚﾎﾟｰﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>顧客名 :</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>技術者 ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>ﾃｽﾄの場所:</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>作業指示書 :</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>ｺﾒﾝﾄ / 注 :</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>無線:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>帯域:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>全体状態</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>拡張 FC ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>接続</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>対称</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>ﾛｰｶﾙ設定</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>ﾘﾓｰﾄに接続</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>ﾌｧｲﾊﾞ ﾁｬﾈﾙ設定</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>FC ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>構成ﾃﾝﾌﾟﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>ﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>利用率</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>ﾌﾚｰﾑ ﾛｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>ｺﾝﾄﾛｰﾙ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>ﾃｽﾄ持続時間</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>しきい値のﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>構成の変更</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>ﾌｧｲﾊﾞ ﾁｬﾈﾙの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>使用率の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾚｲﾃﾝｼーの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>ｻｰﾋﾞｽ ｱｸﾃｨﾍﾞｰｼｮﾝ ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>構成の変更とﾃｽﾄの再実行</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>ﾚｲﾃﾝｼ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>ﾌﾚｰﾑﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Exit FC ﾃｽﾄの終了</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>別のﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>ｶﾊﾞｰ ﾍﾟｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾃｽﾄ中､一方向の遅延時間ｿｰｽの同期が外れました｡ OWD 時間ｿｰｽ ﾊｰﾄﾞｳｪｱの接続を確認してください｡ﾃｽﾄが完了していなければ続行しますが､ﾌﾚｰﾑ遅延結果は使用できません｡</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄのﾃｽﾄ中､一方向の遅延時間ｿｰｽの同期が外れました｡ OWD 時間ｿｰｽ ﾊｰﾄﾞｳｪｱの接続を確認してください｡ﾃｽﾄが完了していなければ続行しますが､ﾌﾚｰﾑ遅延結果は使用できません｡</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>ｱｸﾃｨﾌﾞ ﾙｰﾌﾟが見つかりました</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>ﾈｲﾊﾞｰ ｱﾄﾞﾚｽの解決に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>ｻｰﾋﾞｽ #1: 宛先 MAC への ARP 要求送信</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>ﾛｰｶﾙ側のｻｰﾋﾞｽ #1: 宛先 MAC への ARP 要求送信</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>ﾘﾓｰﾄ側のｻｰﾋﾞｽ #1: 宛先 MAC への ARP 要求送信</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>宛先 MAC へ ARP ﾘｸｴｽﾄを送信中</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>ﾛｰｶﾙ側から宛先 MAC への ARP 要求送信</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>ﾘﾓｰﾄ側から宛先 MAC への ARP 要求送信</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>ﾈｯﾄﾜｰｸ要素ﾎﾟｰﾄは、半二重用に準備されています。 続行する場合は、[Continue in Half Duplex] (半二重で続行) ﾎﾞﾀﾝを押します。 中止する場合は [Abort Test] (ﾃｽﾄの中止) を押します。</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟを試行しています。 ｱｸﾃｨﾌﾞなﾙｰﾌﾟを確認しています。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>ﾊｰﾄﾞｳｪｱ･ﾙｰﾌﾟの確認中.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>LBM/LBR ﾙｰﾌﾟを確認しています。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>無限ﾙｰﾌﾟを確認しています.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀ要求がﾀｲﾑｱｳﾄしました。 DHCP ﾊﾟﾗﾒｰﾀを取得できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ ﾓｰﾄﾞを選択したことで､ﾘﾓｰﾄ ﾕﾆｯﾄから切断されました｡</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>最終的なﾌﾚｰﾑ受信数が決定できないため、 SAMComplete がﾀｲﾑｱｳﾄしました。 受信ﾌﾚｰﾑの計測数が予想外に増え続けています。</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>ﾊｰﾄﾞ ﾙｰﾌﾟが見つかりました</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄとの接続が確立されていない限り､ J-QuickCheck はﾄﾗﾌｨｯｸ接続ﾃｽﾄを実行できません｡</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>LBM/LBR ﾙｰﾌﾟが見つかりました</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>ﾛｰｶﾙ ﾘﾝｸが壊れ､ﾘﾓｰﾄ ﾕﾆｯﾄとの接続が切断されました｡ ﾘﾝｸが元通り確立されれば､ﾘﾓｰﾄ ｴﾝﾄﾞとの接続を再試行できます｡</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>現在ﾘﾝｸがｱｸﾃｨﾌﾞではありません。</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>送信元と送信先の IP が同じです｡ 送信元と送信先の IP ｱﾄﾞﾚｽを再構成してください｡</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝとﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはﾄﾗﾌｨｯｸ ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはｽﾄﾘｰﾑ ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>ﾙｰﾌﾟを確立できませんでした.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄとの接続が確立していません。 [ 接続 ] ﾍﾟｰｼﾞに移動し、送信先の IP ｱﾄﾞﾚｽを確認して [ ﾘﾓｰﾄに接続 ] ﾎﾞﾀﾝを押してください。</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>[ﾈｯﾄﾜｰｸ] ﾍﾟｰｼﾞに移動して、構成を検証し、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>光ﾓｼﾞｭｰﾙが準備中か存在しません｡</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>無限ﾙｰﾌﾟが見つかりました</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>PPPoE 接続のﾀｲﾑｱｳﾄ。 PPPoE 設定を確認してから再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>回復不能の PPPoE ｴﾗーが発生しました</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>ｻｰﾊﾞーへのﾛｸﾞｵﾝを試行中 ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>ﾘﾓｰﾄ接続の問題が検出されました｡ ﾘﾓｰﾄ ﾕﾆｯﾄにｱｸｾｽできません</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ﾕﾆｯﾄとの接続が確立できませんでした｡ﾘﾓｰﾄ発信元 IP ｱﾄﾞﾚｽを確認し､再度､試行してください｡</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはﾙｰﾌﾟﾊﾞｯｸ ｱﾌﾟﾘｹｰｼｮﾝです。 これは拡張 RFC 2544 との互換性がありません。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは TCP WireSpeed ｱﾌﾟﾘｹｰｼｮﾝではありません｡</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄのｿﾌﾄｳｪｱのﾊﾞｰｼﾞｮﾝが更新されています。 ﾃｽﾄを続行すると、一部の機能が制限される場合があります。 最適なﾊﾟﾌｫｰﾏﾝｽを得るため、このﾕﾆｯﾄのｿﾌﾄｳｪｱをｱｯﾌﾟｸﾞﾚｰﾄﾞすることが推奨されます。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄのｿﾌﾄｳｪｱが旧ﾊﾞｰｼﾞｮﾝです。 ﾃｽﾄを続行すると、一部の機能が制限される場合があります。 最適なﾊﾟﾌｫｰﾏﾝｽを得るため、ﾘﾓｰﾄ ﾕﾆｯﾄのｿﾌﾄｳｪｱをｱｯﾌﾟｸﾞﾚｰﾄﾞすることが推奨されます。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄのｿﾌﾄｳｪｱのﾊﾞｰｼﾞｮﾝが確認できませんでした。 ﾃｽﾄを続行すると、一部の機能が制限される場合があります。 同じﾊﾞｰｼﾞｮﾝのｿﾌﾄｳｪｱを使用するﾕﾆｯﾄ間でﾃｽﾄすることが推奨されます。</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟが失敗しました。 再試行しています。</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>選択されたﾃﾝﾌﾟﾚｰﾄの設定が正常に適用されました。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄのｿﾌﾄｳｪｱのﾊﾞｰｼﾞｮﾝが確認できないか、このﾕﾆｯﾄのﾊﾞｰｼﾞｮﾝに対応していません。 ﾃｽﾄを続行すると、一部の機能が制限される場合があります。 同じﾊﾞｰｼﾞｮﾝのｿﾌﾄｳｪｱを使用するﾕﾆｯﾄ間でﾃｽﾄすることが推奨されます。</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>明示的なﾛｸﾞｲﾝを完了できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ #1 ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #2 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ #3 ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 Mbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 Mbps) と一致しません。 [接続] ﾍﾟｰｼﾞで非対称に再設定してください。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 kbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 kbps) と一致しません。 [接続] ﾍﾟｰｼﾞで非対称に再設定してください。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 Mbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 Mbps) と一致しません。 [ 接続 ] ﾍﾟｰｼﾞで非対称に再設定してから、ﾃｽﾄを再開してください。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 kbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 kbps) と一致しません。 [ 接続 ] ﾍﾟｰｼﾞで非対称に再設定してから、ﾃｽﾄを再開してください。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 Mbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 Mbps) と一致しません。 RFC2544 [対称] ﾍﾟｰｼﾞで非対称に再設定してください。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#1 kbps) がﾘﾓｰﾄ ﾕﾆｯﾄのﾗｲﾝ ﾚｰﾄ (#2 kbps) と一致しません。 RFC2544 [対称] ﾍﾟｰｼﾞで非対称に再設定してください。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>無効な構成 :&#xA;&#xA; 少なくとも 1 つのﾃｽﾄを選択してください。 少なくとも 1 つのﾃｽﾄを選択して、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>ﾃｽﾄするﾌﾚｰﾑ ｻｲﾂﾞを選択していません。 少なくとも 1 つのﾌﾚｰﾑ ｻｲﾂﾞを選択してから開始してください。</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>設定されたしきい値を超えるﾌﾚｰﾑ損失率が最後に検出されました。手動ﾄﾗﾌｨｯｸ ﾃｽﾄでﾘﾝｸのﾊﾟﾌｫｰﾏﾝｽを検証してください。手動ﾃｽﾄを行った後、 RFC 2544 を再実行できます。 &#xA; 推奨される手動ﾃｽﾄ構成 :&#xA; ﾌﾚｰﾑ ｻｲﾂﾞ :#1 ﾊﾞｲﾄ &#xA; ﾄﾗﾌｨｯｸ : 常時 #2 Mbps&#xA; ﾃｽﾄ持続時間 : 設定されたﾃｽﾄ持続時間の 3 倍以上。ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ持続時間は #3 秒でした。 &#xA; 監視する結果 :&#xA;[ 要約ｽﾃｰﾀｽ ] 画面でｴﾗｰ ｲﾍﾞﾝﾄを探してください。ｴﾗｰ ｶｳﾝﾀが散発的に増加している場合、ﾄﾗﾌｨｯｸ ﾚｰﾄを下げて手動ﾃｽﾄを実行してください。ﾄﾗﾌｨｯｸ ﾚｰﾄを下げてもｴﾗーが発生する場合、ﾘﾝｸに最大負荷とは別の一般的な問題があると考えられます。ｸﾞﾗﾌ結果の現在のﾌﾚｰﾑ損失率ｸﾞﾗﾌから、散発的または持続的なﾌﾚｰﾑ損失ｲﾍﾞﾝﾄがあるかどうかを確認することもできます。散発的ｴﾗーの問題を解決できない場合、低いﾌﾚｰﾑ損失率を許容できるようにﾌﾚｰﾑ損失許容差しきい値を設定できます。注 : ﾌﾚｰﾑ損失許容差を使用すると、ﾃｽﾄは RFC 2544 勧告に適合しなくなります。</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>注 :   ﾄﾗﾝｽﾐｯﾀとﾚｼｰﾊﾞの VLAN ｽﾀｯｸ深さが違うことから、構成されたﾚｰﾄはﾎﾟｰﾄ間のﾚｰﾄ差を補正するように調整されます。</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>#1 ﾊﾞｲﾄﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>#1 ﾊﾞｲﾄﾊﾟｹｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>最後の 10 秒間は、ﾄﾗﾌｨｯｸが停止しても L2 ﾌｨﾙﾀ Rx Acterna ﾌﾚｰﾑ ｶｳﾝﾄが増え続けました。</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>最大 ｽﾙｰﾌﾟｯﾄ ﾚｰﾄで ｾﾞﾛにしています</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>#1 L1 Mbps を試行しています</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>#1 L2 Mbps を試行しています</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>#1 L1 kbps を試行しています</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>#1 L2 kbps を試行しています</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>試行中 #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>#1 L1 Mbps を検証しています。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>#1 L2 Mbps を検証しています。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>#1 L1 kbps を検証しています。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>#1 L2 kbps を検証しています。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>現在 検証中 #1 % 。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ実測値 : #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ実測値 : #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ実測値 : #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ実測値 : #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>測定された最大ｽﾙｰﾌﾟｯﾄ : #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄの測定はできません</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄ (RFC 2544 標準 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄ ( ﾄｯﾌﾟﾀﾞｳﾝ )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄ ( ﾎﾞﾄﾑｱｯﾌﾟ )</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>#1 L1 Mbps の負荷でﾃｽﾄを実行します。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>#1 L2 Mbps の負荷でﾃｽﾄを実行します。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>#1 L1 kbps の負荷でﾃｽﾄを実行します。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>#1 L2 kbps の負荷でﾃｽﾄを実行します。 これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>#1% 負荷でﾃｽﾄ実行中です。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾌﾚｰﾑ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>試行 #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>ﾎﾟｰｽﾞ ﾌﾚｰﾑの検出</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>#1 kB のﾊﾞｰｽﾄを試行しています</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>#1 kB より大きい</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>#1 kB 未満</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>ﾊﾞｯﾌｧ ｻｲﾂﾞは #1 kB です</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>ﾊﾞｯﾌｧ ｻｲﾂﾞは #1 kB 未満です</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>ﾊﾞｯﾌｧ ｻｲﾂﾞは #1 kB 以上です</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>送信された #1 ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>受信ﾌﾚｰﾑ #1</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>ﾛｽﾄﾌﾚｰﾑ #1</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>ﾊﾞｰｽﾄ (CBS) ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>予測 CBS: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>予測 CBS: 取得不可</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>CBS ﾃｽﾄはｽｷｯﾌﾟされます。 この構成を正確にﾃｽﾄするためには、ﾃｽﾄのﾊﾞｰｽﾄ ｻｲﾂﾞが大きすぎます。</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>CBS ﾃｽﾄはｽｷｯﾌﾟされます。 この構成を正確にﾃｽﾄするためのﾃｽﾄ持続時間が不十分です。</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>ﾊﾞｰｽﾄ ﾎﾟﾘｼﾝｸﾞの試行 #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>ｼｽﾃﾑ回復ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>このﾊﾟｹｯﾄ ｻｲﾂﾞではﾃｽﾄは無効です</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>このﾌﾚｰﾑ ｻｲﾂﾞではﾃｽﾄは無効です</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>試行 #1 / #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄは最大帯域幅で合格し、ﾌﾚｰﾑ損失は観測されなかったため、ﾌﾚｰﾑ損失が誘導される可能性はありません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>損失ｲﾍﾞﾝﾄを発生させることができません。 このﾊﾟｹｯﾄ ｻｲﾂﾞではﾃｽﾄは無効です。</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>損失ｲﾍﾞﾝﾄを発生させることができません。 このﾌﾚｰﾑ ｻｲﾂﾞではﾃｽﾄは無効です。</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>平均復旧時間 : #1 秒を超える</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>#1 秒より長い</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>平均復旧時間 : 取得不可</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>平均復旧時間 :#1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 us</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>最適なｸﾚｼﾞｯﾄ ﾊﾞｯﾌｧに対してｽﾞﾛｲﾝ中。 #1 ｸﾚｼﾞｯﾄをﾃｽﾄしています</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>#1 ｸﾚｼﾞｯﾄ のﾃｽﾄ中</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>現在 #1 ｸﾚｼﾞｯﾄで検証中。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>注 : ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄが 2 未満のﾊｰﾄﾞ ﾙｰﾌﾟを想定しています。 このﾃｽﾄは無効です。</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>&#xA; 注 : ﾊｰﾄﾞ ﾙｰﾌﾟの仮定に基づき、ｽﾙｰﾌﾟｯﾄ計測は、ﾌｧｲﾊﾞーの 2 倍の長さを補償する目的で、各ｽﾃｯﾌﾟ でのﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄの数の 2 倍にされます。</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>#1 ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄでｽﾙｰﾌﾟｯﾄの測定中</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄとﾚｲﾃﾝｼーのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄおよびﾊﾟｹｯﾄ･ｼﾞｯﾀのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄ、ﾚｲﾃﾝｼｰ、ﾊﾟｹｯﾄ ｼﾞｯﾀーのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>ﾚｲﾃﾝｼーとﾊﾟｹｯﾄ ｼﾞｯﾀーの試行 #1 。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀｰ ﾃｽﾄの試行 #1 。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄの試行 #1 。これには #2 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>検証されたｽﾙｰﾌﾟｯﾄ負荷 #2% によるﾚｲﾃﾝｼｰ ﾃｽﾄの試行 #1 これには #3 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 RFC 2544 ﾃｽﾄ #2 の開始</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 FC ﾃｽﾄ #2 の開始</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>ﾃｽﾄが完了しました。</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>結果を保存しています。しばらくお待ちください。</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>拡張負荷ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>FC ﾃｽﾄ:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>ﾈｯﾄﾜｰｸ構成</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>ﾃｽﾄ ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>ﾒﾝﾃﾅﾝｽのﾄﾞﾒｲﾝ ﾚﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>送信元 TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>ｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN ｽﾀｯｸの深さ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN User Priority</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>SVLAN DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>CVLAN ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>ﾕｰｻﾞ優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>SVLAN 7 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>SVLAN 7 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>SVLAN 6 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>SVLAN 6 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>SVLAN 5 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>SVLAN 5 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>SVLAN 4 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>SVLAN 4 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>SVLAN 3 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>SVLAN 3 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>SVLAN 2 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>SVLAN 2 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>SVLAN 1 ﾕｰｻﾞ 優先度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>SVLAN 1 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>ﾙｰﾌﾟ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>送信元 MAC</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>SA MAC の自動ｲﾝｸﾘﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>連続 MAC 番号</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>IP EtherType を無効にする</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>OoS の結果を無効にする</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>DA ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>送信先 MAC</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>ﾃﾞｰﾀﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>認証の使用</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>ﾕｰｻﾞ名</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>ｻｰﾋﾞｽﾌﾟﾛﾊﾞｲﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>ｻｰﾋﾞｽ名</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>送信元 IP ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>送信元 IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>ﾃﾞﾌｫﾙﾄｹﾞｰﾄｳｪｲ</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>ｻﾌﾞﾈｯﾄﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>宛先 IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Time To Live ( ﾎｯﾌﾟ )</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>IP ID ｲﾝｸﾘﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>発信元ﾘﾝｸ - ﾛｰｶﾙ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>発信元ｸﾞﾛｰﾊﾞﾙ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>ｻﾌﾞﾈｯﾄ ﾌﾚﾌｨｸｽ長</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Destination Address</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>ﾄﾗﾌｨｯｸ ｸﾗｽ</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>ﾌﾛｰ ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>ﾎｯﾌﾟ ﾘﾐｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>ﾄﾗﾌｨｯｸ ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>発信元ﾎﾟｰﾄ ｻｰﾋﾞｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>送信元 ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>送信先ﾎﾟｰﾄのｻｰﾋﾞｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>宛先 ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>ATP ﾘｯｽﾝ IP ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>ATP ﾘｯｽﾝ IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>送信元 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>送信先 ID</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Sequence ID</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Originator ID</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Responder ID</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>ﾃｽﾄ 構成</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna ﾍﾟｲﾛｰﾄﾞ ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>ﾚｲﾃﾝｼー測定精度</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>帯域幅の単位</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>最大ﾃｽﾄ帯域幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ﾃｽﾄ帯域幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>最大ﾃｽﾄ帯域幅 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ﾃｽﾄ帯域幅 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ﾃｽﾄ帯域幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ﾃｽﾄ帯域幅 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>最大ﾃｽﾄ帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>完全に 100% のﾄﾗﾌｨｯｸを許可</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>ｽﾙｰﾌﾟｯﾄ測定精度</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定精度</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定精度</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>ｽﾙｰﾌﾟｯﾄのｽﾞﾛ機能ﾌﾟﾛｾｽ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ 損失 しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ損失許容差 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ損失許容差 (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>すべてのﾃｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>すべてのﾃｽﾄの試行回数</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 合格 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ合格しきい値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ合格しきい値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ合格しきい値 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ合格しきい値 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ合格しきい値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ合格しきい値 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 合格 しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>ﾚｲﾃﾝｼーの試行回数</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>ﾚｲﾃﾝｼーの試行持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>ﾚｲﾃﾝｼー帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>ﾚｲﾃﾝｼー合格しきい値</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>ﾚｲﾃﾝｼー合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ 合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ 合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ試行数</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀーの試行持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 合格 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀー合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾟｹｯﾄ ｼﾞｯﾀー合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾟｹｯﾄ ｼﾞｯﾀー合格しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>ﾌﾚｰﾑ 損失 ﾃｽﾄ 手順</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>ﾌﾚｰﾑ損失の試行持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>ﾌﾚｰﾑ損失の帯域幅細分性 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失の帯域幅細分性 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>ﾌﾚｰﾑ損失の帯域幅細分性 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失の帯域幅細分性 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失の帯域幅細分性 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失の帯域幅細分性 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>ﾌﾚｰﾑ 損失 帯域幅 粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>最小ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>最小ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>最小ﾌﾚｰﾑ損失範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>最大ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>最大ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>最大ﾌﾚｰﾑ損失範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最小ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最小ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最小ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最小ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ﾌﾚｰﾑ損失範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ﾌﾚｰﾑ損失範囲 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>ﾌﾚｰﾑ損失のｽﾃｯﾌﾟ数</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの試行回数</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの細分性 ( ﾌﾚｰﾑ )</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸの最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸの最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>ﾎﾟｰﾂﾞ ﾌﾚｰﾑを無視</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>ﾊﾞｰｽﾄ CBS ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ CBS ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ CBS ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄの最小ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄの最大ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄの最小ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄの最大ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄの最小ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄの最大ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS 持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄの試行回数</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>ﾊﾞｰｽﾄ CBS が合否を表示</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄが合否を表示</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ ｻｲﾂﾞしきい値 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ｻｲﾂﾞしきい値 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ｻｲﾂﾞしきい値 (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>ｼｽﾃﾑ復元の試行数</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>ｼｽﾃﾑ復元のｵｰﾊﾞｰﾛｰﾄﾞ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>拡張負荷持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>拡張負荷ｽﾙｰﾌﾟｯﾄ ｽｹｰﾘﾝｸﾞ (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>拡張負荷ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>拡張負荷ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄのﾛｸﾞｲﾝ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄの最大ﾊﾞｯﾌｧ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄのｽﾙｰﾌﾟｯﾄ ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄの持続時間</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾀﾞｳﾝ 成功 : LLB ﾓｰﾄﾞ外の装置 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾀﾞｳﾝ 成功 : ﾄﾗﾝｽﾍﾟｱﾚﾝﾄ LLB ﾓｰﾄﾞ外の装置 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>ﾘﾓｰﾄ 装置 **#1** は既にﾙｰﾌﾟ ﾀﾞｳﾝされていました</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>構成の変更によるﾘﾓｰﾄ ﾙｰﾌﾟ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ｱｯﾌﾟ 成功 : LLB ﾓｰﾄﾞ内の装置 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ｱｯﾌﾟ 成功 : ﾄﾗﾝｽﾍﾟｱﾚﾝﾄ LLB ﾓｰﾄﾞ内の装置 **#1**</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>ﾘﾓｰﾄ 装置 **#1** は既に LLB ﾓｰﾄﾞ内にありました</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>ﾘﾓｰﾄ 装置 **#1** は既に透過 LLB ﾓｰﾄﾞ内にありました</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>ｾﾙﾌﾙｰﾌﾟ、その他のﾎﾟｰﾄにﾙｰﾌﾟする、はｻﾎﾟｰﾄしていません</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>ｽﾄﾘｰﾑ #1: ﾘﾓｰﾄ ﾙｰﾌﾟ ﾀﾞｳﾝ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>ｽﾄﾘｰﾑ #1: ﾘﾓｰﾄ ﾙｰﾌﾟ ｱｯﾌﾟ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾀﾞｳﾝ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>ﾘﾓｰﾄ 透過 ﾙｰﾌﾟ ﾀﾞｳﾝ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ｱｯﾌﾟ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>ﾘﾓｰﾄ 透過 ﾙｰﾌﾟ ｱｯﾌﾟ 失敗 : ACK ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽのための 2 重ｱﾄﾞﾚｽ検出を開始しました。</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽの設定に失敗しました（設定をﾁｪｯｸしてください）。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽのための 2 重ｱﾄﾞﾚｽを待機しています。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>2 重ｱﾄﾞﾚｽの検出に成功しました。ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽは有効です。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>2 重ｱﾄﾞﾚｽの検出に失敗しました。ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽは無効です。</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>ﾘﾝｸ‐ﾛｰｶﾙｱﾄﾞﾚｽのための 2 重ｱﾄﾞﾚｽ検出を開始しました。</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>ﾘﾝｸ‐ﾛｰｶﾙｱﾄﾞﾚｽの設定に失敗しました（設定をﾁｪｯｸしてください）。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>ﾘﾝｸ‐ﾛｰｶﾙで 2 重ｱﾄﾞﾚｽ検出を待機中です。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>2 重ｱﾄﾞﾚｽ検出に成功しました。ﾘﾝｸ‐ﾛｰｶﾙｱﾄﾞﾚｽは有効です。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>2 重ｱﾄﾞﾚｽ検出に失敗しました。ﾘﾝｸ‐ﾛｰｶﾙｱﾄﾞﾚｽは無効です。</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>ｽﾃｰﾄﾚｽ IP の復元を開始しました。</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>ｽﾃｰﾄﾚｽ IP の取得に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>ｽﾃｰﾄﾚｽ IP の取得に成功しました。</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>ｽﾃｰﾄﾌﾙな IP の復元を開始しました。</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>ｽﾃｰﾄﾌﾙな IP を提供するﾙｰﾀが見つかりませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>ｽﾃｰﾄﾌﾙ IP の復元を再試行中です。</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>ｽﾃｰﾄﾌﾙ IP の取得に成功しました。</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>ﾈｯﾄﾜｰｸ未達</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>ﾎｽﾄ未達</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>ﾌﾟﾛﾄｺﾙ未達</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>ﾎﾟｰﾄ未達</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>ﾒｯｾｰｼﾞ が長すぎます</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>宛先 ﾈｯﾄﾜｰｸ 不明</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>宛先 ﾎｽﾄ 不明</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>宛先 ﾈｯﾄﾜｰｸ 禁止</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>宛先 ﾎｽﾄ 禁止</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>ﾈｯﾄﾜｰｸ TOS 未到達</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>ﾎｽﾄ TOS に到達不可</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>ﾄﾗﾝｼﾞｯﾄ中に時間超過</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>再構成中に時間超過</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>ICMP 不明 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>送信先未達</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>ｱﾄﾞﾚｽ未達</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>送信先へのﾙｰﾄがありません</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>宛先は近隣にはありません</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>送信先との接続は管理上禁止されています</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>ﾊﾟｹｯﾄ 大きすぎ</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Hop Limit Exceeded in Transit ( 移行中 Hop ﾘﾐｯﾄを超過 )</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>断片の再構築時間を超過しました</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>ｴﾗｰ ﾍｯﾀﾞｰ ﾌｨｰﾙﾄﾞ遭遇</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>認識できない次ﾍｯﾀﾞｰ ﾀｲﾌﾟに遭遇</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>認識できない IPv6 ｵﾌﾟｼｮﾝに遭遇</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>無効</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE 有効</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 有効</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>ﾈｯﾄﾜｰｸ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>ﾕｰｻﾞ要求による無効化</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>ﾃﾞｰﾀ ﾚｲﾔｰ 停止</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 認証失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 失敗 原因不明</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 失敗</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>不適切な設定</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>内部ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP 成功。 宛先 MAC 取得</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>ARP ｻｰﾋﾞｽの待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>No ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>半二重で続行</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>J-QuickCheck の終了</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>J-QuickCheck ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>ﾘﾓｰﾄﾙｰﾌﾟ状態</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>ﾛｰｶﾙ ﾙｰﾌﾟ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>PPPoE ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>IPv6 状態</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>ARP 状態</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>DHCP 状態</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>ICMP 状態</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>ﾈｲﾊﾞー検出ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>自動設定のｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>ｱﾄﾞﾚｽのｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>ﾕﾆｯﾄ検出</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>ﾕﾆｯﾄの探索</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>検索中</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>選択された装置の使用</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>終了中</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>ﾘﾓｰﾄに &#xA; 接続</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>ﾘﾓｰﾄに &#xA; 接続</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>切断</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>ﾘﾓｰﾄに &#xA; 接続しました</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>接続中</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>対称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非対称</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>単一方向</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑとｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>両方向</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑとｱｯﾌﾟｽﾄﾘｰﾑのｽﾙｰﾌﾟｯﾄが同じです。</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑとｱｯﾌﾟｽﾄﾘｰﾑのｽﾙｰﾌﾟｯﾄが異なります。</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>一方向のﾈｯﾄﾜｰｸのみをﾃｽﾄします。</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>測定 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>ﾄﾗﾌｨｯｸをﾛｰｶﾙで生成し、ﾛｰｶﾙで測定します。</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>ﾄﾗﾌｨｯｸをﾛｰｶﾙとﾘﾓｰﾄで生成し、それぞれの方向で測定します。</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>測定方向 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>ﾄﾗﾌｨｯｸをﾛｰｶﾙで生成し、ﾘﾓｰﾄ ﾃｽﾄ機器で測定します。</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>ﾄﾗﾌｨｯｸをﾘﾓｰﾄで生成し、ﾛｰｶﾙ ﾃｽﾄ機器で測定します。</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>往復遅延</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>一方向の遅延</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>ﾗｳﾝﾄﾞ ﾄﾘｯﾌﾟ遅延測定のみ。 &#xA; ﾘﾓｰﾄ機器が一方向の遅延に対応していません。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>一方向の遅延の測定のみ。 &#xA; ﾘﾓｰﾄ ﾕﾆｯﾄが双方向 RTD に対応していません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>遅延測定ができません。 &#xA; ﾘﾓｰﾄ ﾕﾆｯﾄが双方向 RTD または一方向の遅延に対応していません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>遅延測定ができません。 &#xA; ﾘﾓｰﾄ ﾕﾆｯﾄが一方向の遅延に対応していません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>遅延測定ができません。 &#xA; ﾘﾓｰﾄ ﾕﾆｯﾄが双方向 RTD に対応していません。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>一方向の遅延の測定のみ。 &#xA; 単一方向のﾃｽﾄでは RTD はｻﾎﾟｰﾄされていません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>遅延測定ができません。 &#xA; 単一方向のﾃｽﾄでは RTD はｻﾎﾟｰﾄされません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>遅延測定ができません。 &#xA; ﾘﾓｰﾄ ﾕﾆｯﾄが一方向の遅延に対応していません。 &#xA; 単一方向のﾃｽﾄでは RTD はｻﾎﾟｰﾄされません。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>遅延測定ができません。</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>ﾚｲﾃﾝｼー測定ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>ﾘﾓｰﾄ Viavi ﾃｽﾄ機器が必要です。</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>ﾊﾞｰｼﾞｮﾝ 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>ﾊﾞｰｼﾞｮﾝ 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 校正</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>光学装置の選択</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>遠端との通信ﾁｬﾈﾙの IP 設定</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ ﾃｽﾄに必要なﾛｰｶﾙ IP ｱﾄﾞﾚｽ設定がありません。</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>固定</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>静的 - ｻｰﾋﾞｽあたり</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>ｽﾀﾃｨｯｸ‐単一</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>ﾏﾆｭｱﾙ (? 手動 )</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>ｽﾃｰﾄﾌﾙ</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Stateless: 権限のない</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>送信元ﾘﾝｸ‐ﾛｰｶﾙｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>送信元ｸﾞﾛｰﾊﾞﾙｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>ｵｰﾄ 取得</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>ﾕｰｻﾞ定義</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>ARP ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>ｿｰｽｱﾄﾞﾚｽのﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>PPPoE ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>詳細</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>無効</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>顧客 送信元 MAC</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>ﾃﾞﾌｫﾙﾄ</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>ﾃﾞﾌｫﾙﾄ 送信元 MAC</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>顧客 既定 MAC</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>ﾕｰｻﾞ ｿｰｽ MAC</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>Cust. ﾕｰｻﾞｰ MAC</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>接続のｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>ｽﾀｯｸﾄﾞ VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>ﾛｰｶﾙ ﾈｯﾄﾜｰｸ ﾎﾟｰﾄでいくつの VLAN が使用されていますか ?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>VLANs がありません</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLAN (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLAN ( ｽﾀｯｸされた VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>VLAN ID 設定を入力してください :</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>ｽﾀｯｸの深さ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (16 進数 )</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>ﾕｰｻﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>検出ｻｰﾊﾞーの設定</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>注:送信先のﾘﾝｸﾛｰｶﾙ IP が「ﾘﾓｰﾄ接続」に使用できません</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>ｸﾞﾛｰﾊﾞﾙ送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Ping 実行中</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>&#xA; 送信先 IP を見つけるには</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>ﾛｰｶﾙﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>不明ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>ﾘﾝｸの損失</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>ﾛｰｶﾙﾎﾟｰﾄ:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>オートネゴシエーション</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>解析中 </translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>オートネゴシエーション:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>接続待機中</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>接続中...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>再試行中 ...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>接続完了</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>接続に失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>接続が中止されました</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>通信ﾁｬﾈﾙ:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>検出ｻｰﾊﾞーの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>検出ｻｰﾊﾞーから送信先 IP を取得</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Server IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>ｻｰﾊﾞｰ ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>ｻｰﾊﾞｰ ﾊﾟｽﾌﾚｰﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>要求されるﾘｰｽ時間 ( 分 )</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>許可されたﾘｰｽ時間 ( 分 )</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>L2 ﾈｯﾄﾜｰｸ設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>ﾄﾗﾌｨｯｸ</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>LBM ﾄﾗﾌｨｯｸ</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 ( 最小 )</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 ( 最大 )</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>ﾕｰｻﾞの SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>ﾙｰﾌﾟ ﾀｲﾌﾟ、EtherType、MAC ｱﾄﾞﾚｽの設定</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>MAC ｱﾄﾞﾚｽ、EtherType、LBM の設定</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>MAC ｱﾄﾞﾚｽの設定</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>MAC ｱﾄﾞﾚｽと ARP ﾓｰﾄﾞの設定</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>宛先 ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>ﾕﾆｷｬｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>ﾏﾙﾁｷｬｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>ﾌﾞﾛｰﾄﾞｷｬｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>VLAN User Priority</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄが接続されていません｡</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>L2 ﾈｯﾄﾜｰｸ設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>L3 ﾈｯﾄﾜｰｸ設定- ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>ﾄﾗﾌｨｯｸ ｸﾗｽ、ﾌﾛｰ ﾗﾍﾞﾙ、およびﾎｯﾌﾟ ﾘﾐｯﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>ご使用のﾈｯﾄﾜｰｸではどのﾀｲﾌﾟの IP 優先順位が使用されていますか ?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>有効期間、IP ID ｲﾝｸﾘﾒﾝﾄ、PPPoE ﾓｰﾄﾞの設定</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>有効期間と PPPoE ﾓｰﾄﾞの設定</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>有効期間と IP ID ｲﾝｸﾘﾒﾝﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>有効期間の設定</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>ご使用のﾈｯﾄﾜｰｸではどの IP 優先順位が使用されていますか ?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>L3 ﾈｯﾄﾜｰｸ設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>L4 ﾈｯﾄﾜｰｸ設定- ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>送信元 ｻｰﾋﾞｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>DHCP ｸﾗｲｱﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>DHCP ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>ﾌｨﾝｶﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>宛先のｻｰﾋﾞｽﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>ATP ﾘｯｽﾝ IP の設定</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>L4 ﾈｯﾄﾜｰｸ設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>ﾙｰﾌﾟ ﾀｲﾌﾟとｼｰｹﾝｽ、ﾚｽﾎﾟﾝﾀﾞ、発信元 ID の設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>L2 の詳細設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>送信元ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>ﾃﾞﾌｫﾙﾄ MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>ﾕｰｻﾞ MAC</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>LBM 構成</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>LBM ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>送信元 TLV を有効にする</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>L2 の詳細設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>L3 の詳細設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Time To Live ( ﾎｯﾌﾟ )</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>L3 の詳細設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>L4 の詳細設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>L4 の詳細設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>ﾈｯﾄﾜｰｸの詳細設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>ﾃﾝﾌﾟﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>構成ﾃﾝﾌﾟﾚｰﾄを使用しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Viavi 拡張ﾊﾟｹｯﾄ ｱｸｾｽ ﾚｰﾄ > ﾄﾗﾝｽﾎﾟｰﾄ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - ﾍﾞｽﾄ ｴﾌｫｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - ｺﾝﾁﾈﾝﾀﾙ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - ｸﾞﾛｰﾊﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - ﾒﾄﾛ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - ﾓﾊﾞｲﾙ ﾊﾞｯｸﾎｰﾙ H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - ﾘｰｼﾞｮﾅﾙ</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - VoIP ﾃﾞｰﾀ ｴﾐｭﾚｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC 2544 ﾋﾞｯﾄ透過</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 ﾊﾟｹｯﾄ ｱｸｾｽ ﾚｰﾄ > ﾄﾗﾝｽﾎﾟｰﾄ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 ﾊﾟｹｯﾄ ｱｸｾｽ ﾚｰﾄ = ﾄﾗﾝｽﾎﾟｰﾄ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>ﾃﾝﾌﾟﾚｰﾄを適用</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>ﾃﾝﾌﾟﾚｰﾄ構成 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ損失 (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>ﾚｲﾃﾝｼーしきい値 (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ しきい値 (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ : ﾃﾞﾌｫﾙﾄ</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>最大帯域幅 ( ｱｯﾌﾟｽﾄﾘｰﾑ ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>最大帯域幅 ( ﾀﾞｳﾝｽﾄﾘｰﾑ ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>最大帯域幅 : #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>ﾃｽﾄ : ｽﾙｰﾌﾟｯﾄとﾚｲﾃﾝｼーのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>ﾃｽﾄ : ｽﾙｰﾌﾟｯﾄ、ﾚｲﾃﾝｼｰ、ﾌﾚｰﾑ損失、ﾊﾞｯｸﾂｰﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>持続時間と試行 : 1 試行で #1 秒間</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｱﾙｺﾞﾘﾂﾞﾑ : 拡張 Viavi</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｱﾙｺﾞﾘﾂﾞﾑ : RFC 2544 標準</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>ｽﾙｰﾌﾟｯﾄしきい値 ( ｱｯﾌﾟｽﾄﾘｰﾑ ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>ｽﾙｰﾌﾟｯﾄしきい値 ( ﾀﾞｳﾝｽﾄﾘｰﾑ ): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>ｽﾙｰﾌﾟｯﾄ しきい値 : #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>ﾌﾚｰﾑ損失ｱﾙｺﾞﾘﾂﾞﾑ : RFC 2544 標準</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸのﾊﾞｰｽﾄ時間 : 1 秒</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの細分性 : 1 ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>利用率の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>最大 帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大帯域幅 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大帯域幅 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大帯域幅 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大帯域幅 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大帯域幅 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大帯域幅 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大帯域幅 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大帯域幅 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>最大帯域幅 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>最大帯域幅 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>最大帯域幅 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>最大帯域幅 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>最大 帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>選択されたﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>長さ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>ﾊﾟｹｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>ﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>全てｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ &#xA; ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄが接続されていません｡</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ &#xA; ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄが &#xA; 接続されていません｡</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>選択されたﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>RFC 2544 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>ﾚｲﾃﾝｼｰ ( ｽﾙｰﾌﾟｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ( ｽﾙｰﾌﾟｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ( ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>ｼｽﾃﾑ復元 ( ﾙｰﾌﾟﾊﾞｯｸのみ、ｽﾙｰﾌﾟｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>追加ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀｰ ( ｽﾙｰﾌﾟｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>拡張負荷 ( ﾙｰﾌﾟﾊﾞｯｸのみ )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>ｽﾙｰﾌﾟｯﾄ構成</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>ｾﾞﾛｲﾝｸﾞ ｲﾝ ﾌﾟﾛｾｽ</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 の標準</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi 拡張</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>ｽﾙｰﾌﾟｯﾄ測定の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの測定精度 (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>1.0% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>0.1% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>0.01% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>次の範囲が上限 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>400 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>40 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>4 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>.4 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>1000 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>100 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>10 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>1 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>.1 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>.01 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>.001 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>.0001 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>92.942 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>9.2942 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>.9294 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>.0929 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>10000 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>1000 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>100 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>10 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>1 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>.1 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの測定精度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>測定精度</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>測定確度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>測定精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>測定精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>測定精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>測定確度 (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>詳細情報</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>ﾄﾗﾌﾞﾙｼｭｰﾃｨﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>ｺﾐｯｼｮﾆﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>ｽﾙｰﾌﾟｯﾄの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>ﾚｲﾃﾝｼｰ / ﾊﾟｹｯﾄ ｼﾞｯﾀｰ ﾃｽﾄの持続時間を別に設定する</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>ﾚｲﾃﾝｼｰ  ﾃｽﾄの持続時間を別に設定する</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀｰ ﾃｽﾄの持続時間を別に設定する</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>ﾌﾚｰﾑ損失構成</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>ﾃｽﾄ手順</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>ﾄｯﾌﾟ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>ﾎﾞﾄﾑ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>ｽﾃｯﾌﾟ数</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>ｽﾃｯﾌﾟあたり #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>ｽﾃｯﾌﾟあたり #1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>ｽﾃｯﾌﾟあたり #1 % ﾗｲﾝ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>最大</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの帯域幅細分性 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの帯域幅細分性 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの帯域幅細分性 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの帯域幅細分性 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの帯域幅細分性 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ範囲 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの帯域幅細分性 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの帯域幅細分性 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの帯域幅細分性 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの帯域幅細分性 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの帯域幅細分性 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>ﾃｽﾄ範囲 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>ﾃｽﾄ範囲 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>ﾃｽﾄ範囲 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>ﾃｽﾄ範囲 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>ﾃｽﾄ 範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>帯域幅細分性 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>帯域幅細分性 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>帯域幅細分性 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>帯域幅細分性 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>帯域幅 粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>ﾌﾚｰﾑ損失測定の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>ﾌﾚｰﾑ損失の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>ｵﾌﾟｼｮﾝのﾃｽﾄ測定</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>ﾚｲﾃﾝｼーの測定</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀーの測定</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの構成</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>最大ﾊﾞｰｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>ﾊﾞｰｽﾄ 粒度 ( ﾌﾚｰﾑ数 )</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸの詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>ﾎﾟｰｽﾞ ﾌﾚｰﾑ ﾎﾟﾘｯｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄの構成</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>ﾌﾛｰ 制御 ﾛｸﾞｲﾝ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>暗黙 ( 透過的ﾘﾝｸ )</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explicit (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>最大 ﾊﾞｯﾌｧ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>ﾃｽﾄ制御</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>ﾃｽﾄ持続時間を別に設定しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>ﾄﾗｲｱﾙ回数</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>ﾚｲﾃﾝｼｰ / ﾊﾟｹｯﾄ ｼﾞｯﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>ﾊﾞｰｽﾄ (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>ｼｽﾃﾑ復元</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>全ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>&#xA; 合否の表示</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ &#xA; しきい値</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ &#xA; しきい値</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>しきい値</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>ｽﾙ-ﾌﾟｯﾄ しきい値(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄしきい値(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄのしきい値 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄのしきい値 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄが &#xA; 接続されていません｡</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>ﾚｲﾃﾝｼ RTD (往復遅延) (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>ﾚｲﾃﾝｼｰ OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>ﾊﾞｰｽﾄ (CBS ﾎﾟﾘｼﾝｸﾞ )</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>高精度 - 低 遅延</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>低精度 - 高 遅延</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>FC ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>FC ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ &#xA; ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>ｽﾙｰﾌﾟｯﾄ異常</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ異常</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ異常</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>ｽﾙｰﾌﾟｯﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Frame 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Frame 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Frame 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Frame 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Frame 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Frame 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Frame 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Frame 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Frame 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Frame 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>合格 / 失格</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>ﾌﾚｰﾑ長 &#xA;( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>ﾊﾟｹｯﾄ 長 &#xA;( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>測定 ﾚｰﾄ &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>測定 ﾚｰﾄ &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>実測 &#xA; ﾚｰﾄ ( ﾌﾚｰﾑ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>R_RDY&#xA; 検出</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>構成ﾚｰﾄ &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>実測 L1&#xA; ﾚｰﾄ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>実測 L1&#xA; ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>実測 L1&#xA;(% ﾗｲﾝ ﾚｰﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>実測 L2&#xA; ﾚｰﾄ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>実測 L2&#xA; ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>実測 L2&#xA;(% ﾗｲﾝ ﾚｰﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>実測 L3&#xA; ﾚｰﾄ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>実測 L3&#xA; ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>実測 L3&#xA;(% ﾗｲﾝ ﾚｰﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>実測 L4&#xA; ﾚｰﾄ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>実測 L4&#xA; ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>実測 L4&#xA;(% ﾗｲﾝ ﾚｰﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>実測ﾚｰﾄ &#xA; ( ﾌﾚｰﾑ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>実測ﾚｰﾄ &#xA; ( ﾊﾟｹｯﾄ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>ﾎﾟｰﾂﾞの &#xA; 検出</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>構成ﾚｰﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>構成ﾚｰﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>構成ﾚｰﾄ &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>構成ﾚｰﾄ &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>例外</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>OoS ﾌﾚｰﾑ &#xA; が検出されました</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Acterna ﾍﾟｲﾛｰﾄﾞ &#xA; ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>FCS&#xA; ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>IP ﾁｪｯｸｻﾑ &#xA; ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>TCP/UDP ﾁｪｯｸｻﾑ &#xA; ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>ﾚｲﾃﾝｼｰ &#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>ﾚｲﾃﾝｼｰ &#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>実測 &#xA;% ﾗｲﾝ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>ﾎﾟｰﾂﾞの &#xA; 検出</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>ﾌﾚｰﾑ 損失 ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>ﾌﾚｰﾑ損失の結果</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Frame 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>設定ﾚｰﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>設定ﾚｰﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>設定ﾚｰﾄ (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>設定ﾚｰﾄ (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>設定ﾚｰﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>ﾌﾚｰﾑ 損失 (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾚｰﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾚｰﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾚｰﾄ &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾚｰﾄ &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ･ﾚｰﾄ &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>ﾌﾚｰﾑ 損失 ﾚｰﾄ &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>損失したﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>最大平均ﾊﾟｹｯﾄ &#xA; ｼﾞｯﾀｰ (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>ｴﾗｰ &#xA; 検出</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>OoS&#xA; 検出</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>構成ﾚｰﾄ &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>平均 &#xA; ﾊﾞｰｽﾄ ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>平均 &#xA; ﾊﾞｰｽﾄ秒数</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>注意</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>MinimumBufferSize&#xA;( ｸﾚｼﾞｯﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>ｸﾚｼﾞｯﾄ 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>ｸﾚｼﾞｯﾄ 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>ｸﾚｼﾞｯﾄ 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>ｸﾚｼﾞｯﾄ 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>ｸﾚｼﾞｯﾄ 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>ｸﾚｼﾞｯﾄ 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>ｸﾚｼﾞｯﾄ 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>ｸﾚｼﾞｯﾄ 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>ｸﾚｼﾞｯﾄ 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>ｸﾚｼﾞｯﾄ 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>ﾊﾞｯﾌｧ･ｻｲﾂﾞ &#xA;( ｸﾚｼﾞｯﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>測定 ﾚｰﾄ &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>測定 ﾚｰﾄ &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>実測ﾚｰﾄ &#xA;( ﾌﾚｰﾑ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>J-Proof - ｲｰｻﾈｯﾄ L2 透過性ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>J-Proof ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>J-Proof ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>ｻﾏﾘ</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>終了 : 結果詳細</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Exit J-Proof ﾃｽﾄの終了</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>適用中</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>ﾌﾚｰﾑ ﾀｲﾌﾟをﾚｲﾔ 2 透過性のﾃｽﾄ ｻｰﾋﾞｽに設定する</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   ﾌﾟﾛﾄｺﾙ   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>ﾌﾟﾛﾄｺﾙ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> ﾌﾚｰﾑ </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> ｶﾌﾟｾﾙ化 </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>ｽﾀｯｸﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>ﾚｰﾄ &#xA;( ﾌﾚｰﾑ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>ﾚｰﾄ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>ﾀｲﾑｱｳﾄ &#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>ﾀｲﾑｱｳﾄ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>制御</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>DEI&#xA; ﾋﾞｯﾄ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>ﾕｰｻﾞｰ SVLAN TPID</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>ｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>ｶﾌﾟｾﾙ化</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit 増分</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>ｸｲｯｸ &#xA; 構成</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>ﾌﾚｰﾑ &#xA; 追加</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>ﾌﾚｰﾑ &#xA; の削除</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>発信元ｱﾄﾞﾚｽはすべてのﾌﾚｰﾑで共通です。これは [ ﾛｰｶﾙ設定 ] ﾍﾟｰｼﾞで設定します。</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>PBit 増加</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>VLAN ｽﾀｯｸ</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>SPbit 増加</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>長さ</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>J-Proof 構成</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>強度</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>ｸｲｯｸ (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>ﾌﾙ (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>ﾌｧﾐﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>全て</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>ｽﾊﾟﾆﾝｸﾞ ﾂﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>ﾚｰｻﾞｰ &#xA;On</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>ﾚｰｻﾞｰ &#xA;Off</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>ﾌﾚｰﾑ ｼｰｹﾝｽ &#xA; 開始</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>ﾌﾚｰﾑｼｰｹﾝｽ &#xA; 中止</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>ｻｰﾋﾞｽ 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>ﾍﾟｲﾛｰﾄﾞｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>ﾍｯﾀﾞ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>ｶｳﾝﾄ 不一致</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>結果の要約</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>ｱｲﾄﾞﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ 不一致</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>ﾍｯﾀﾞ 不一致</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>ｲｰｻﾈｯﾄ</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>J-Proof 結果</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>光 Rx&#xA; ﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>QuickCheck ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>QuickCheck 設定</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>QuickCheck 拡張負荷の結果</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>詳細</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>単一</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>ｽﾄﾘｰﾑ につき</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>QuickCheck の結果</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>拡張負荷ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Tx ﾌﾚｰﾑ･ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Rx ﾌﾚｰﾑ･ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>ｴﾗｰ･ﾌﾚｰﾑ･ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>OoS ﾌﾚｰﾑ･ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>損失ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>ﾌﾚｰﾑﾛｽ率</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>ﾊｰﾄﾞｳｪｱ</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>無限</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>ｽﾙｰﾌﾟｯﾄ :%1 Mbps (L1) 、持続時間 :%2 秒、ﾌﾚｰﾑ ｻｲﾂﾞ :%3 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>ｽﾙｰﾌﾟｯﾄ :%1 kbps (L1) 、持続時間 :%2 秒、ﾌﾚｰﾑ ｻｲﾂﾞ :%3 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>うまくいきませんか ?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>成功</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>送信先を探索中</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>ARP 状態:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>ﾘﾓｰﾄ･ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>ﾊｰﾄﾞ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>無限ﾙｰﾌﾟの確認</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>LBM/LBR ﾙｰﾌﾟを確認しています</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>無限ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>ﾙｰﾌﾟ失敗</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>ﾘﾓｰﾄ･ﾙｰﾌﾟ:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>実測ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>未確定</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>実測ｽﾙｰﾌﾟｯﾄ (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>ｴﾗーを表示</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>負荷 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>負荷 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>ﾋﾞｯﾄﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ持続時間</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 秒</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 秒</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 分</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 分</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 分</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 分</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 時間表示</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 時間</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 時間</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 時間</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>ﾕｰｻﾞ定義</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>ﾃｽﾄ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ持続時間 ( 分 )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>ﾌﾚｰﾑ長 ( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>ｼﾞｬﾝﾎﾞ</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>ﾕｰｻﾞー名の長さ</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>特大長さ</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  電気ｺﾈｸﾀ:  10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>ﾚｰｻﾞｰ 波長</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>電気 ｺﾈｸﾀ</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>光ｺﾈｸﾀ</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>詳細 ...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Link ｱｸﾃｨﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>ﾄﾗﾌｨｯｸの結果</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>ｴﾗー数</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>ｴﾗｰﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>ﾛｽﾄﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>ｲﾝﾀｰﾌｪｲｽ詳細</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>SFP/XFP 詳細</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>波長 (ﾅﾉﾒｰﾄﾙ):ﾁｭｰﾆﾝｸﾞ可能な SFP、ﾁｬﾈﾙ/波長ﾁｭｰﾆﾝｸﾞ ｾｯﾄｱｯﾌﾟを参照してください。</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>ｹｰﾌﾞﾙ長 (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>ｻﾎﾟｰﾄされているﾁｭｰﾆﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>波長、ﾁｬﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>波長</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>推奨ﾚｰﾄ&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>公称 ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>最小 ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>最大 ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>ﾍﾞﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>ﾍﾞﾝﾀﾞ PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>ﾍﾞﾝﾀﾞ Rev</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>ﾊﾟﾜｰ ﾚﾍﾞﾙ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>診断 ﾓﾆﾀ</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>診断 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>JMEP の構成</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>波長 (ﾅﾉﾒｰﾄﾙ):ﾁｭｰﾆﾝｸﾞ可能なﾃﾞﾊﾞｲｽ、ﾁｬﾈﾙ/波長ﾁｭｰﾆﾝｸﾞ ｾｯﾄｱｯﾌﾟを参照してください。</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>CFP/QSFP 詳細</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>CFP2/QSFP の詳細</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>CFP4/QSFP の詳細</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>ﾍﾞﾝﾀﾞｰ SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>日付ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>ﾛｯﾄ ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW ﾊﾞｰｼﾞｮﾝ番号</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW 仕様書の改訂番号</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA 測定 I/F 改訂番号</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>ﾊﾟﾜｰｸﾗｽ</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx ﾊﾟﾜｰ ﾚﾍﾞﾙ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Rx 最大ﾗﾑﾀﾞ電力 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Tx 最大ﾗﾑﾀﾞ電力 (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>ｱｸﾃｨﾌﾞなﾌｧｲﾊﾞ数</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>各ﾌｧｲﾊﾞの波長</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>診断ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>ﾌｧｲﾊﾞ範囲別の WL (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>最大ﾈｯﾄﾜｰｸ ﾚｰﾝのﾋﾞｯﾄ ﾚｰﾄ (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>ﾓｼﾞｭｰﾙ ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>ｻﾎﾟｰﾄされているﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>公称波長 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>公称 ﾋﾞｯﾄ ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** 100GigE RS-FEC アプリケーションへの使用を推奨します ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>CFP ｴｷｽﾊﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>CFP2 ｴｷｽﾊﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>CFP4 Expert</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Viavi ﾙｰﾌﾟﾊﾞｯｸを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>CFP Viavi ループバックを有効にする</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>CFP ｴｷｽﾊﾟｰﾄ ﾓｰﾄﾞを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>CFP2 ｴｷｽﾊﾟｰﾄ ﾓｰﾄﾞを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>CFP4 Expert モードを有効にする</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>ﾌﾟﾘｴﾝﾌｧｼｽ</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>CFP Tx プリエンファシス</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>既定</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>低</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>公称</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>High</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>ｸﾛｯｸ分周器</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>CFP Tx クロック デバイダ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>ｽｷｭｰ ｵﾌｾｯﾄ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>CFP Tx スキュー オフセット</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>極性の反転</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP Tx 極性反転</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Tx FIFO のﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>等化</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>CFP Rx 等化</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP Rx 極性反転</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>LOS を無視</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP Rx LOS 無視</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Rx FIFO のﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>QSFP ｴｷｽﾊﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>QSFP ｴｷｽﾊﾟｰﾄ ﾓｰﾄﾞを有効にする</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx LOS 無視</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>CDR バイパス</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>受信</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR 受信バイパス</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>送信</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR 送信バイパス</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>CDR 送受信バイパス制御は QSFP モジュールでサポートされていれば使用可能です。</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>ｴﾝｼﾞﾆｱﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR ｺｱ ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>CFP XCVR コア ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR ﾗｲﾝ ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>CFP XCVR ライン ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>ﾓｼﾞｭｰﾙ ﾎｽﾄ ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>CFP モジュール ホスト ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>ﾓｼﾞｭｰﾙ ﾈｯﾄﾜｰｸ ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>CFP モジュール ネットワーク ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>CFP ギヤボックス システム ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>CFP ギヤボックス ライン ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>Gearbox ﾁｯﾌﾟ ID</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Gearbox ﾁｯﾌﾟ Rev</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Gearbox Ucode ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>Gearbox Ucode CRC</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Gearbox ｼｽﾃﾑ ﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Gearbox ﾗｲﾝ ﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>CFPn ﾎｽﾄ ﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>CFPn ﾈｯﾄﾜｰｸ ﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR コア ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR ライン ループバック</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO Peek DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO Peek PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO Poke DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO Poke PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO Poke Value</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>A013 ｺﾝﾄﾛｰﾙ ﾚｰﾝ単位ﾚｰｻﾞー有効/無効を登録します。</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C Peek PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C Peek DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C Poke PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C Poke DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C Poke Value</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>レジスタ 0x56 のレーン別レーザー コントロールを有効 / 無効にする。</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+ 詳細</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>ｼｸﾞﾅﾙ</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Tx 信号 ｸﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>ｸﾛｯｸ ｿｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>内部</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>回復</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>外部</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>外部 1.5M</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>外部 2M</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>外部 10M</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>ﾘﾓｰﾄ 回復</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>タイミング モジュール</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>VC-12 ｿｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>内部 - 周波数 ｵﾌｾｯﾄ (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>ﾘﾓｰﾄ ｸﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>ﾁｭｰﾆﾝｸﾞ可能ﾃﾞﾊﾞｲｽ</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>ﾁｭｰﾆﾝｸﾞ ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>周波数</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>周波数 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>調整可能な最初の周波数 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>調整可能な最後の周波数 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>ｸﾞﾘｯﾄﾞ間隔 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>ｽｷｭｰ ｱﾗｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>ｽｷｭｰ ｱﾗｰﾑしきい値 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>再同期が必要</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>再同期完了</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>RS-FEC 校正合格</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>RS-FEC 校正失敗</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC は通常 SR4、PSM4、CWDM4 と一緒に使用します。</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>RS-FEC 校正を実行するには、以下の手順を実行します (CFP4 にも適用されます)。</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>CSAM に QSFP28 アダプタを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>アダプタに QSFP28 トランシーバを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>トランシーバにファイバ ループバック デバイスを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>[校正] をクリックする</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>校正</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>校正中...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>再同期</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>トランシーバを試験中のデバイス (DUT) と今すぐ同期する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>トランシーバからファイバ ループバック デバイスを取り外す</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>試験中のデバイス (DUT) への接続を確立する</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>DUT レーザを ON にする</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>CSAM の信号確認 LED が緑であることを確認する</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>[レーンの再同期] をクリックする</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>レーンの&#xA;再同期</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>J-QuickCheck 結果</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>[ 開始 ] ﾎﾞﾀﾝを押すと、 J-QuickCheck ﾃｽﾄが開始されます。</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>半二重ﾓｰﾄﾞで続行中.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続ﾁｪｯｸ中｡</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続ﾁｪｯｸ中｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>すべてのｻｰﾋﾞｽのﾄﾗﾌｨｯｸ接続が正常に検証されました｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>ｻｰﾋﾞｽ #1 のｱｯﾌﾟｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>ｻｰﾋﾞｽ #1 のﾀﾞｳﾝｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>ｻｰﾋﾞｽ #1 のｱｯﾌﾟｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続とｻｰﾋﾞｽ #2 のﾀﾞｳﾝｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ方向またはﾀﾞｳﾝｽﾄﾘｰﾑ方向のﾄﾗﾌｨｯｸ接続が正常に検証されませんでした｡</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>接続ﾁｪｯｸ中</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>ﾄﾗﾌｨｯｸ接続 :</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>QC の開始</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>QC の中止</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>光ｾﾙﾌﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>終了:プロファイルの保存</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>光</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>光学機器のセルフテストの終了</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>光信号が失われました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>低出力ﾚﾍﾞﾙが検出されました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>高出力ﾚﾍﾞﾙが検出されました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>ｴﾗーが検出されました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>過度のｽｷｭーが検出されたため、ﾃｽﾄは失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>過度のｽｷｭーが検出されたため、ﾃｽﾄは失敗しました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>ﾋﾞｯﾄ ｴﾗーが検出されました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>BER が設定されたしきい値を超えたため、ﾃｽﾄは失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>ﾊﾟﾀｰﾝ同期の損失により、ﾃｽﾄは失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>挿入された QSFP+ を読み取ることができません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>挿入された CFP を読み取ることができません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>挿入された CFP2 を読み取ることができません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>挿入された CFP4 を読み取れません。</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>RS-FEC の訂正可能なビット エラーが検出されました。テストは中断されました。</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>RS-FEC の訂正不能なビット エラーが検出されました。テストは中断されました。</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>RS-FEC 校正失敗</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>入力周波数偏差ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>出力周波数偏差ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>同期が失われました</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>ｺｰﾄﾞ違反が検出されました</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>ｱﾗｲﾒﾝﾄ ﾏｰｶｰ ﾛｯｸが失われました</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>無効なｱﾗｲﾒﾝﾄ ﾏｰｶーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>BIP 8 AM ﾋﾞｯﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>BIP ﾌﾞﾛｯｸ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>ｽｷｭーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>ﾌﾞﾛｯｸ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>ｱﾝﾀﾞｰｻｲﾂﾞ ﾌﾚｰﾑが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>ﾘﾓｰﾄの障害が検出されました</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 ﾋﾞｯﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>ﾊﾟﾀｰﾝ同期が失われました</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>ﾊﾟﾀｰﾝ同期の損失により BER しきい値を正確に測定できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>BER 実測値が、選択された BER しきい値を超えました</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS が検出されました</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>FAS ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>ﾌﾚｰﾑ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>ﾌﾚｰﾑ損失が検出されました</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>ﾌﾚｰﾑ同期が失われました</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>論理ﾚｰﾝ ﾏｰｶｰ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>論理ﾚｰﾝ ﾏｰｶｰ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>ﾚｰﾝ ｱﾗｲﾒﾝﾄ損失が検出されました</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>ﾚｰﾝ ｱﾗｲﾒﾝﾄが失われました</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>MFAS ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>ﾚｰﾝ ｱﾗｲﾒﾝﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS が検出されました</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>復元ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>過度のｽｷｭーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>RS-FEC 校正成功</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>光ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>ﾃｽﾄ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>テストでは 100GE RS-FEC を使用します</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>ｵﾌﾟﾃｨｸｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>ﾃｽﾄ全体の判定</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>信号ﾌﾟﾚｽﾞﾝｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>光信号ﾚﾍﾞﾙ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>過度のｽｷｭｰ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>ﾋﾞｯﾄ ｴﾗｰ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>一般的なｴﾗｰ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>BER しきい値ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>光ﾊﾟﾜｰ</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Rx ﾚﾍﾞﾙ合計 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Tx ﾚﾍﾞﾙ合計 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>ﾃｽﾄするｺﾈｸﾀの Tx 端子および Rx 端子間をつなぐ、短くて汚れていないﾊﾟｯﾁ ｹｰﾌﾞﾙを接続してください。</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>CFP&#xA; 光学特性のﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>CFP2&#xA;光モジュール/スロットのテスト</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>ﾃｽﾄの中止</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>QSFP+&#xA; 光学特性のﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>QSFP28&#xA;光学機器のテスト</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>設定:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>推奨される</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 分</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 分</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 時間</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 時間</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>ﾕｰｻﾞー持続時間 ( 分 )</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>推奨継続時間 (分)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>この構成で推奨される時間は &lt;b>%1&lt;/b> 分です。</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>BER 閾値タイプ</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Post-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pre-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>BER しきい値</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>PPM ﾗｲﾝ ｵﾌｾｯﾄ を有効にする</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>PPM 最大ｵﾌｾｯﾄ (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>ｴﾗー発生時に停止</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>結果の概要</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>光学機器/スロットのタイプ</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>現在の PPM ｵﾌｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>現在 BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>光ﾊﾟﾜｰ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Rx ﾚﾍﾞﾙ合計</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx ﾚﾍﾞﾙ ﾗﾑﾀﾞ #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Tx ﾚﾍﾞﾙ合計</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>CFP ｲﾝﾀｰﾌｪｲｽ詳細</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>CFP2 ｲﾝﾀｰﾌｪｲｽ詳細</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>CFP4 インターフェイスの詳細</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>QSFP ｲﾝﾀｰﾌｪｲｽ詳細</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+ インターフェイスの詳細</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>QSFP28 インターフェイスの詳細</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>QSFP がありません</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>QSFP を読み取ることができません。 QSFP を再度、挿入してください。</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP ﾁｪｯｸｻﾑ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>必要な QSFP ﾚｼﾞｽﾀーに問い合わせできません。</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>QSFP の ID を確認できません。</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>持続時間とﾍﾟｲﾛｰﾄﾞ BERT ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>GCC 透過性</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>ﾃｽﾄの選択と実行</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>OTN ﾁｪｯｸ ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>OTN ﾁｪｯｸ ﾃｽﾄを終了します</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>測定周波数</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>ｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>RTD 構成</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>すべてのﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN ﾁｪｯｸ:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** OTN ﾁｪｯｸ ﾃｽﾄを開始します ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT ﾋﾞｯﾄ ｴﾗｰが検出されました</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>100,000 件を超えるﾍﾟｲﾛｰﾄﾞ BERT ﾋﾞｯﾄ ｴﾗｰが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT BER閾値を超えました</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 ﾍﾟｲﾛｰﾄﾞ BERT ﾋﾞｯﾄ ｴﾗｰが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT ﾋﾞｯﾄ ｴﾗｰﾚｰﾄ:#1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>RTD 閾値を超えました</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1:#2 - RTD:最小:#3, 最大:#4, 平均:#5:</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1:RTD が取得できません</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>ﾊﾟｲﾛｰﾄﾞ BERT ﾃｽﾄの実行中</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>RTD ﾃｽﾄの実行中</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>GCC 透過性ﾃｽﾄの実行中</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>GCC ﾋﾞｯﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>GCC BER 閾値を超えました</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 GCC ﾋﾞｯﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>100,000 件を超える GCC ﾋﾞｯﾄ ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>GCC ﾋﾞｯﾄ ｴﾗｰ:#1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** ループバック チェックを開始します ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** ループバック チェックをスキップします ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** ループバック チェックを完了しました ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>ループバックが検出されました</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>ループバックは検出されませんでした</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>ﾁｬﾈﾙ #1 は使用できません</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>ﾁｬﾈﾙ #1 は使用可能です</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>ﾊﾟﾀｰﾝ同期の損失。</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>GCC ﾊﾟﾀｰﾝ同期の損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>ﾃｽﾄは中止されました。ﾋﾞｯﾄ ｴﾗーが検出されました。</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>ﾃｽﾄは失敗しました。BER が設定閾値を超えています。</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>ﾃｽﾄは失敗しました。RTD が設定閾値を超えています。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>テストは中断されました。ループバックは検出されませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>ﾃｽﾄは中止されました。光信号が存在しません。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>ﾃｽﾄは中止されました。ﾌﾚｰﾑの損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>ﾃｽﾄは中止されました。ﾌﾚｰﾑ同期の損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>ﾃｽﾄは中止されました。ﾊﾟﾀｰﾝ同期の損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>ﾃｽﾄは中止されました。GCC ﾊﾟﾀｰﾝ同期の損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>ﾃｽﾄは中止されました。ﾚｰﾝ ｱﾗｲﾒﾝﾄの損失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>ﾃｽﾄは中止されました。ﾏｰｶー ﾛｯｸの損失。</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>1 つ以上のﾁｬﾈﾙを選択する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>ループバックが選択されていません</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>ループバックは検出されませんでした</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>ﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>計画されたﾃｽﾄ持続時間</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>ﾃｽﾄの実行時間</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>OTN ﾁｪｯｸではﾄﾗﾌｨｯｸ ﾙｰﾌﾟﾊﾞｯｸを実行する必要があります。このﾙｰﾌﾟﾊﾞｯｸは OTN 回路の終端に必要です。</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>OTN ﾁｪｯｸ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>光学装置のｵﾌｾｯﾄ、ｼｸﾞﾅﾙ ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>ｼｸﾞﾅﾙ ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>ｼｸﾞﾅﾙ ﾏｯﾋﾟﾝｸﾞと光ﾓｼﾞｭｰﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>信号 構造</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP 光ﾓｼﾞｭｰﾙ RTD ｵﾌｾｯﾄ (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP 光学 RTD ｵﾌｾｯﾄ (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 光ﾓｼﾞｭｰﾙ RTD ｵﾌｾｯﾄ (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>CFP4 光 RTD オフセット (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>持続時間とﾍﾟｲﾛｰﾄﾞ BERT ﾃｽﾄの構成</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>持続時間とﾍﾟｲﾛｰﾄﾞ BERT の構成</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT の設定</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>信頼度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>ﾗｲﾝ ﾚｰﾄに基づき､BER 閾値､信頼ﾚﾍﾞﾙ､推奨ﾃｽﾄ時間は &lt;b>%1&lt;/b> です｡</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>BERT ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>ｴﾗー閾値</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>合否の表示</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>ﾗｳﾝﾄﾞ ﾄﾘｯﾌﾟ遅延の設定</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>RTD の構成</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>含める</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>閾値 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>測定周波数 (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>GCC の構成</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>GCC 透過性の設定</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>GCC ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>GCC BER 閾値</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>BERT ﾊﾟﾀｰﾝ 2^23-1 は GCC で使用されています。</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>ループバック チェックをスキップ</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>[開始] ボタンを押すと、ループバック チェックが始まります。</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>OTN ﾁｪｯｸ ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>ループバックをチェックしています</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>ループバックが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>ループバック チェックをスキップ</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>ループバック検出</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>ループバック検出</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT の結果</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>BER 測定値</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>ﾋﾞｯﾄ ｴﾗー件数</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>判定</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ BERT の構成</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>ﾗｳﾝﾄﾞ ﾄﾘｯﾌﾟ遅延の結果</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>最小 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>最大 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>平均 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>注:RTD 平均が閾値を超えると障害が発生します。</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>ﾗｳﾝﾄﾞ ﾄﾘｯﾌﾟ 遅延 構成</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>GCC 透過性の結果</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>GCC BERT ﾋﾞｯﾄ ｴﾗー率</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>GCC BERT ﾋﾞｯﾄ ｴﾗー秒数</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>GCC 透過性の構成</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>ﾌﾟﾛﾄｺﾙ分析</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>ｷｬﾌﾟﾁｬ</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>CDP のｷｬﾌﾟﾁｬと分析</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>ｷｬﾌﾟﾁｬと分析</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>分析するﾌﾟﾛﾄｺﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>分析 &#xA; 開始</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>分析の &#xA; 中止</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>解析</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>ｴｷｽﾊﾟｰﾄPTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>ﾃｽﾄ設定の手動構成 </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>しきい値</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>接続ﾁｪｯｸを実行</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>PTPｴｷｽﾊﾟｰﾄを終了</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>ﾘﾝｸがｱｸﾃｨﾌﾞでありません</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>時刻同期が取れません</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>PTPｽﾚｰﾌﾞｾｯｼｮﾝが中断されました。</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>時刻同期ｿｰｽを検出できません。時刻同期ｿｰｽの接続･設定を確認して下さい。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>PTPｽﾚｰﾌﾞｾｯｼｮﾝが確立できません。</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>GPS ﾚｼｰﾊﾞは検出されませんでした。</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>TOS ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>ｱﾅｳﾝｽ Rx ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>ｱﾅｳﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 回 / 秒</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>同期</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>遅延要求</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>ﾘｰｽ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Max ﾀｲﾑｴﾗｰ 閾値 有効</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Max ﾀｲﾑｴﾗｰ (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Max ﾀｲﾑｴﾗｰ 閾値 (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>ﾃｽﾄ持続時間 ( 分 )</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>接続ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>ﾏｽﾀｰIP</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>PTPﾄﾞﾒｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>ﾁｪｯｸ&#xA;開始</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>ｾｯｼｮﾝ&#xA;開始中</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>ｾｯｼｮﾝ&#xA;OK</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Max ﾀｲﾑｴﾗｰ 閾値 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Max ﾀｲﾑｴﾗｰ (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>ﾀｲﾑｴﾗｰ 瞬間値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>ﾀｲﾑｴﾗｰ 瞬間値 (us) x 時間</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>時間 (分)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>PTP ﾁｪｯｸ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>PTPﾁｪｯｸ終了</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>新規ﾃｽﾄ開始</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>PTP チェックの終了</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>拡張 RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>無効な設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>IP の詳細設定 - ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>RTD ﾚｲﾃﾝｼーの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>ﾊﾞｰｽﾄ (CBS) ﾃｽﾄの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck の実行</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>J-QuickCheck 設定</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>拡張負荷</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>RFC 2544 ﾃｽﾄの終了</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>ﾛｰｶﾙ ﾈｯﾄﾜｰｸ構成</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>ﾘﾓｰﾄ ﾈｯﾄﾜｰｸ構成</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>ﾛｰｶﾙのｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>ｽﾋﾟｰﾄﾞ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>ﾃﾞｭﾌﾟﾚｸｽ</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>ﾌﾛｰ制御</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX 可能</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX 可能</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>ﾎﾟｰｽﾞ可能</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>ﾘﾓｰﾄのｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>ﾊｰﾌ</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>ﾌﾙ</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>両方なし</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Both Rx and Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx のみ</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx のみ</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>RTD ﾌﾚｰﾑ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 ﾌﾚｰﾑ / 秒</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 ﾌﾚｰﾑ / 秒</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>ﾊﾞｰｽﾄの構成</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>認定ﾊﾞｰｽﾄ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>CBS ﾎﾟﾘｼﾝｸﾞ (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>最大</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>CBS の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>CBS ﾎﾟﾘｼﾝｸﾞの詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄの詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>許容差</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>拡張負荷構成</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｽｹｰﾘﾝｸﾞ (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 の次の構成設定は無効です :</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>ﾄﾗﾌｨｯｸ接続が正常に検証されました。 負荷ﾃｽﾄを実行しています。</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>[ 開始 ] を押すと、ﾗｲﾝ ﾚｰﾄで実行します。</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>[ 開始 ] を押すと、設定された RFC 2544 帯域幅で実行されます。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>ｽﾙｰﾌﾟｯﾄ実測値は RFC 2544 ﾃｽﾄに使用されません。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>ｽﾙｰﾌﾟｯﾄ実測値は RFC 2544 ﾃｽﾄに使用されます。</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ : %1 ﾊﾞｲﾄ。</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>負荷ﾃｽﾄのﾊﾟｹｯﾄ ｻｲﾂﾞ : %1 ﾊﾞｲﾄ。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ : %1 ﾊﾞｲﾄ。</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ : %1 ﾊﾞｲﾄ。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>実測ｽﾙｰﾌﾟｯﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>設定された RFC 2544 最大帯域幅でのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>ｽﾙｰﾌﾟｯﾄ実測値を RFC 2544 最大帯域幅として使用</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>負荷ﾃｽﾄのﾊﾟｹｯﾄ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ負荷ﾃｽﾄのﾌﾚｰﾑ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>RFC 2544 ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>RFC 2544 ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>ﾊｰﾄﾞｳｪｱのﾙｰﾌﾟを確認しています</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>最大平均ｼﾞｯﾀｰ (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>最大平均ｼﾞｯﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>最大平均 &#xA; ｼﾞｯﾀｰ (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ CBS ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ CBS ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>ﾊﾞｰｽﾄ (CBS ﾎﾟﾘｼﾝｸﾞ ) ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>ﾊﾞｰｽﾄ &#xA; ｻｲﾂﾞの構成 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Tx ﾊﾞｰｽﾄ &#xA; ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>平均 Rx&#xA; ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>送信された &#xA; ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>ﾌﾚｰﾑが &#xA; 受信されました</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>ﾌﾚｰﾑが &#xA; 失われました</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>ﾊﾞｰｽﾄ ｻｲﾂﾞ &#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>ﾚｲﾃﾝｼｰ &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>ｼﾞｯﾀｰ &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>構成済みﾊﾞｰｽﾄ &#xA; ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Tx ﾊﾞｰｽﾄ &#xA; ﾎﾟﾘｼﾝｸﾞ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>予測 &#xA;CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>ｼｽﾃﾑ回復ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>復旧時間 (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>ｵｰﾊﾞｰﾛｰﾄﾞ ﾚｰﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>ｵｰﾊﾞｰﾛｰﾄﾞ ﾚｰﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>ｵｰﾊﾞｰﾛｰﾄﾞ ﾚｰﾄ &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>ｵｰﾊﾞｰﾛｰﾄﾞ ﾚｰﾄ &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>過負荷率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>復元ﾚｰﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>復元ﾚｰﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>復元ﾚｰﾄ &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>復元ﾚｰﾄ &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>回復率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>平均復旧 &#xA; 時間 (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>ｺﾝﾄﾛｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>TrueSpeed ｺﾝﾄﾛｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞあり</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>ｽﾃｯﾌﾟ構成</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>ｽﾃｯﾌﾟの選択</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>ﾊﾟｽ MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Throughput</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>ｱﾄﾞﾊﾞﾝｽﾄ TCP</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>接続設定</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed ｺﾝﾄﾛｰﾙ ( 詳細 )</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Bc 単位の選択</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>ｳｨﾝﾄﾞｳ ｳｫｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Exit TrueSpeed ﾃｽﾄの終了</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>TCP ﾎｽﾄ が接続の確立に失敗しました。 現在のﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>ﾄﾗﾌﾞﾙｼｭｰﾄ ﾓｰﾄﾞを選択したことで、ﾘﾓｰﾄ ﾕﾆｯﾄから切断されました。</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>無効なﾃｽﾄ選択 : TrueSpeed を実行するには、ﾃｽﾄを少なくとも 1 つ選択してください。</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>測定された MTU が小さすぎて続行できません。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>ﾌｧｲﾙの転送速度が速すぎます。 TCP ｽﾙｰﾌﾟｯﾄのﾌｧｲﾙ ｻｲﾂﾞを選択する場合は、ﾃｽﾄが少なくとも 5 秒間実行されるようなｻｲﾂﾞにしてください。 ﾃｽﾄの実行時間は少なくとも 10 秒にすることを推奨します。</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>再試行の回数が上限に到達しました。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>TCP ﾎｽﾄ にｴﾗーが発生しました。 現在のﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>TrueSpeed ﾃｽﾄの開始.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>MTU ｻｲﾂﾞに対してｽﾞﾛｲﾝ中.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>#2 ﾊﾞｲﾄ MSS で #1 ﾊﾞｲﾄ MTU をﾃｽﾄしています。</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>ﾊﾟｽ MTU は #1 ﾊﾞｲﾄと判定されました。 これは MSS の #2 ﾊﾞｲﾄに相当します。</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>RTT ﾃｽﾄを実行しています。 これには #1 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>往復時間 (RTT) は #1 ﾐﾘ秒と判定されました。</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸ ﾃｽﾄを実行しています。</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸ ﾃｽﾄを実行しています。</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>#1 ﾊﾞｲﾄの TCP ﾄﾗﾌｨｯｸを送信しています。</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>ﾛｰｶﾙ出口ﾄﾗﾌｨｯｸ: ｼｪｰﾋﾟﾝｸﾞなし</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>ﾘﾓｰﾄ出口ﾄﾗﾌｨｯｸ: ｼｪｰﾋﾟﾝｸﾞなし</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>ﾛｰｶﾙ出口ﾄﾗﾌｨｯｸ: ｼｪｰﾋﾟﾝｸﾞあり</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>ﾘﾓｰﾄ出口ﾄﾗﾌｨｯｸ: ｼｪｰﾋﾟﾝｸﾞあり</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>持続時間 (秒): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>接続: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ (KB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄを実行しています。</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄを実行しています。</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>詳細 TCP ﾃｽﾄを実行しています。 これには #1 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>ﾛｰｶﾙの IP ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>ﾛｰｶﾙ IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>ﾛｰｶﾙ ﾃﾞﾌｫﾙﾄ ｹﾞｰﾄｳｪｲ</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>ﾛｰｶﾙ ｻﾌﾞﾈｯﾄ ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>ﾛｰｶﾙのｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>ﾘﾓｰﾄ IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>ﾓｰﾄﾞの選択</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>どのようなﾃｽﾄを実行していますか ?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>新しい回線を  実装  または  ﾁｭｰﾝｱｯﾌﾟ  します。 *</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>既存の回線の  ﾄﾗﾌﾞﾙｼｭｰﾃｨﾝｸﾞ  を行います。</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>* ﾘﾓｰﾄ MTS/T-BERD ﾃｽﾄ機器が必要です</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>ｽﾙｰﾌﾟｯﾄをどのように構成しますか ?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑとｱｯﾌﾟｽﾄﾘｰﾑのｽﾙｰﾌﾟｯﾄは  同じ  です。</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑとｱｯﾌﾟｽﾄﾘｰﾑのｽﾙｰﾌﾟｯﾄは  異なり  ます。</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Truespeed 構成を読み込むときは、ﾎﾞﾄﾙﾈｯｸ帯域幅を SAMComplete CIR に合わせて &#xA; 設定します。</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Truespeed 構成を読み込むときは、ﾎﾞﾄﾙﾈｯｸ帯域幅を RFC 2544 最大帯域幅に合わせて &#xA; 設定します。</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Iperf ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>T-BERD/MTS ﾃｽﾄ機器</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>IP ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>ﾘﾓｰﾄ設定</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>TCP ﾎｽﾄ ｻｰﾊﾞー設定</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>このｽﾃｯﾌﾟでは、以後のすべての TrueSpeed ｽﾃｯﾌﾟに使用するｸﾞﾛｰﾊﾞﾙ設定を構成します。 これには CIR ( 認定情報速度 ) と TCP しきい値 ( ｽﾙｰﾌﾟｯﾄ ﾃｽﾄの合格に必要な CIR の割合 ) が含まれます。</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>ｳｨﾝﾄﾞｳ ｳｫｰｸ ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>合計ﾃｽﾄ時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>MTU ｻｲﾂﾞの自動検出</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>MTU ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>ﾛｰｶﾙ VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>優先度</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>ﾛｰｶﾙの優先度</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>ﾛｰｶﾙ TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>ﾛｰｶﾙ DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>ﾘﾓｰﾄ &#xA;TCP ﾎｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>ﾘﾓｰﾄ VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>ﾘﾓｰﾄの優先度</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>ﾘﾓｰﾄ TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>ﾘﾓｰﾄ DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>TrueSpeed の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Bc 単位の選択</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>ﾄﾗﾌｨｯｸ ｼｪｰﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Bc 単位</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞ ﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのどちらの出口ﾄﾗﾌｨｯｸもｼｪｰﾋﾟﾝｸﾞされました</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>ﾛｰｶﾙの出口ﾄﾗﾌｨｯｸのみｼｪｰﾋﾟﾝｸﾞされました</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>ﾘﾓｰﾄの出口ﾄﾗﾌｨｯｸのみｼｪｰﾋﾟﾝｸﾞされました</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのどちらの出口ﾄﾗﾌｨｯｸもｼｪｰﾋﾟﾝｸﾞされていません</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>ﾄﾗﾌｨｯｸ ｼｪｰﾋﾟﾝｸﾞ (ｳｨﾝﾄﾞｳ ｳｫｰｸとｽﾙｰﾌﾟｯﾄのﾃｽﾄのみ)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>ﾃｽﾄはまずｼｪｰﾋﾟﾝｸﾞした状態で、次にｼｪｰﾋﾟﾝｸﾞをせずに行います</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) ローカル</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) ローカル</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) リモート</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) リモート</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>TCP ﾄﾗﾌｨｯｸをｼｪｰﾋﾟﾝｸﾞしますか?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされていないﾄﾗﾌｨｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされたﾄﾗﾌｨｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞをせずにﾃｽﾄを実行する</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>THEN</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞをしてﾃｽﾄを実行する</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>ﾛｰｶﾙでｼｪｰﾋﾟﾝｸﾞをしてﾃｽﾄを実行する</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄでｼｪｰﾋﾟﾝｸﾞをして実行する</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞを全くせずに実行する</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>ﾛｰｶﾙではｼｪｰﾋﾟﾝｸﾞをし、ﾘﾓｰﾄではｼｪｰﾋﾟﾝｸﾞをせずに実行する</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>ﾛｰｶﾙでｼｪｰﾋﾟﾝｸﾞをして実行する</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>ﾛｰｶﾙではｼｪｰﾋﾟﾝｸﾞをし、ﾘﾓｰﾄではｼｪｰﾋﾟﾝｸﾞをせずに実行する</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>ﾛｰｶﾙ ﾄﾗﾌｨｯｸのｼｪｰﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄが&#xA;接続されていません</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>ﾘﾓｰﾄ ﾄﾗﾌｨｯｸのｼｪｰﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>他のｼｪｰﾋﾟﾝｸﾞ ｵﾌﾟｼｮﾝの表示</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>TrueSpeed ｺﾝﾄﾛｰﾙ ( 詳細 )</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>ﾎﾟｰﾄの接続</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>TCP ﾊﾟｽ %</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>MTU 上限 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>複数接続の使用</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>飽和ｳｨﾝﾄﾞｳの有効化</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>ﾌﾞｰｽﾄ ｳｨﾝﾄﾞｳ (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>ﾌﾞｰｽﾄ接続 (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>ｽﾃｯﾌﾟ構成</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>TrueSpeed ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>ﾃｽﾄを実行するには、少なくとも 1 つのｽﾃｯﾌﾟを選択してください。</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>このｽﾃｯﾌﾟでは、 RFC4821 で定義された手順を使用して、ｴﾝﾄﾞﾂｰｴﾝﾄﾞのﾈｯﾄﾜｰｸ ﾊﾟｽの最大転送単位を自動的に決定します。 TCP ｸﾗｲｱﾝﾄ ﾃｽﾄ ｾｯﾄは、さまざまなﾊﾟｹｯﾄ ｻｲﾂﾞで TCP ｾｸﾞﾒﾝﾄの送信を試行し、 ICMP なしに MTU を決定します ( 従来型ﾊﾟｽ MTU 検出では ICMP が必要でした ) 。</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>  このｽﾃｯﾌﾟでは､少量の TCP 転送を実行し､以降のﾃｽﾄ ｽﾃｯﾌﾟ結果の基準として使用される､ﾍﾞｰｽﾗｲﾝ往復時間 (RTT) のﾚﾎﾟｰﾄを返します｡ﾍﾞｰｽﾗｲﾝ RTT とは､ﾈｯﾄﾜｰｸ輻輳による追加の遅延を除外した､ ﾈｯﾄﾜｰｸ固有の待機時間です｡</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>��のｽﾃｯﾌﾟでは、 TCP " ｳｨﾝﾄﾞｳ ｽｷｬﾝ</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ 1 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># 接続</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>ｳｨﾝﾄﾞｳ 1 接続</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ 2 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>ｳｨﾝﾄﾞｳ 2 接続</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ 3 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>ｳｨﾝﾄﾞｳ 3 接続</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ 4 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>ｳｨﾝﾄﾞｳ 4 接続</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>最大 ｾｸﾞﾒﾝﾄ ｻｲｽﾞ ﾊﾞｲﾄ数</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>ﾊﾟｽ MTU ｽﾃｯﾌﾟの &#xA;MSS を使用します</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>最大ｾｸﾞﾒﾝﾄ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>ﾘﾝｸ帯域幅が &lt;b>%1&lt;/b> Mbps 、 RTT が &lt;b>%2&lt;/b> ms の場合、理想的な TCP ｳｨﾝﾄﾞｳは &lt;b>%3&lt;/b> ﾊﾞｲﾄとなります。 接続数が &lt;b>%4&lt;/b> 、各接続の TCP ｳｨﾝﾄﾞｳ ｻｲﾂﾞが &lt;b>%5&lt;/b> ﾊﾞｲﾄの場合、理想的な送信ｽﾙｰﾌﾟｯﾄは &lt;b>%6&lt;/b> kbps 、各接続間の &lt;b>%7&lt;/b> MB ﾌｧｲﾙの転送時間は &lt;b>%8&lt;/b> 秒となります。</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>ﾘﾝｸ帯域幅が &lt;b>%1&lt;/b> Mbps、RTT が &lt;b>%2&lt;/b> ms の場合、理想的な TCP ｳｨﾝﾄﾞｳは &lt;b>%3&lt;/b> ﾊﾞｲﾄとなります。 &lt;font color="red">接続数が &lt;b>%4&lt;/b>、各接続の TCP ｳｨﾝﾄﾞｳ ｻｲﾂﾞが &lt;b>%5&lt;/b> ﾊﾞｲﾄの場合、ﾘﾝｸ容量を超過します。 ﾊﾟｹｯﾄ損失とさらなる遅延により、実測値は予測値より悪化することもあります。 接続数を減らしたり TCP ｳｨﾝﾄﾞｳ ｻｲﾂﾞを縮小したりして、ﾘﾝｸ容量の範囲内でﾃｽﾄを実行してください。&lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>接続あたりのﾌｧｲﾙ ｻｲﾂﾞ (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>30 秒で送信するﾌｧｲﾙ ｻｲﾂﾞの自動検出</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>接続の数</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>RTT ｽﾃｯﾌﾟの RTT を使用します (%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>ﾊﾟｽ MTU ｽﾃｯﾌﾟの MSS を使用します (%1 ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>このｽﾃｯﾌﾟでは、ﾘﾝｸが平等に帯域幅を共有している (ﾄﾗﾌｨｯｸ ｼｪｰﾋﾟﾝｸﾞ) か、あるいは不平等に共有している (ﾄﾗﾌｨｯｸ ﾎﾟﾘｼﾝｸﾞ) かを、複数の TCP 接続転送を実行してﾃｽﾄします。</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>このｽﾃｯﾌﾟでは、ﾘﾝｸが平等に帯域幅を共有している (ﾄﾗﾌｨｯｸ ｼｪｰﾋﾟﾝｸﾞ) か、あるいは不平等に共有している (ﾄﾗﾌｨｯｸ ﾎﾟﾘｼﾝｸﾞ) かを、複数の TCP 接続転送を実行してﾃｽﾄします。ｳｨﾝﾄﾞｳ ｻｲﾂﾞと接続数を自動的に計算するには、 RTT ｽﾃｯﾌﾟを実行します。</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Window Size (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>RTT ｽﾃｯﾌﾟが実施されるとき、 &#xA; 自動的に計算されます</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>TrueSpeed ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>TrueSpeed ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>最大転送単位 (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>最大ｾｸﾞﾒﾝﾄ ｻｲﾂﾞ (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>このｽﾃｯﾌﾟでは、このﾘﾝｸ ( ｴﾝﾄﾞﾂｰｴﾝﾄﾞ ) の最大転送単位 (MTU) を %1 ﾊﾞｲﾄと判定します。 この値 ( ﾚｲﾔｰ 3 および 4 のｵｰﾊﾞｰﾍｯﾄﾞを引いた値 ) 、それ以降のｽﾃｯﾌﾟで TCP 最大ｾｸﾞﾒﾝﾄ ｻｲﾂﾞ (MSS) のｻｲﾂﾞとして使用されます。 この場合の MSS は %2 ﾊﾞｲﾄです。</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Time (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>RTT 要約結果</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>平均 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>最小 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>最大 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>往復時間 (RTT) は %1 ﾐﾘ秒と計測されました。 ﾈｯﾄﾜｰｸ固有の待機時間を最も正確に表すという理由から、最小 RTT が使用されます。 以後のｽﾃｯﾌﾟでは最小 RTT を TCP ﾊﾟﾌｫｰﾏﾝｽ予測の基礎として使用します。</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>往復時間 (RTT) は %1 ﾐﾘ秒となるように測定されました。ﾈｯﾄﾜｰｸ固有の待機時間を最も的確に表すという理由から、平均 (ﾍﾞｰｽ) RTT が使用されます。以降のｽﾃｯﾌﾟでは、予測される TCP ﾊﾟﾌｫｰﾏﾝｽの基礎として使用します。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 1 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 1 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 2 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 2 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 3 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 3 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 4 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 4 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 5 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 5 ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 1 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 1 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 2 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 2 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 3 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 3 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 4 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 4 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 5 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたｳｨﾝﾄﾞｳ 5 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないｳｨﾝﾄﾞｳ 1 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 1 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないｳｨﾝﾄﾞｳ 2 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 2 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないｳｨﾝﾄﾞｳ 3 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 3 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないｳｨﾝﾄﾞｳ 4 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 4 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないｳｨﾝﾄﾞｳ 5 実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ 5 予測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbps 、平均.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>ｳｨﾝﾄﾞｳ 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>実測値</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされていない実測値</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされた実測値</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>理想的</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>ｳｨﾝﾄﾞｳ 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>ｳｨﾝﾄﾞｳ 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>ｳｨﾝﾄﾞｳ 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>ｳｨﾝﾄﾞｳ 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>TCP ｳｨﾝﾄﾞｳ ｳｫｰｸのｽﾃｯﾌﾟの結果は、ﾃｽﾄされた各ｳｨﾝﾄﾞｳ ｻｲﾂﾞで、ｽﾙｰﾌﾟｯﾄの実測値と理想値を示します 実際のｽﾙｰﾌﾟｯﾄが理想より低い原因としては、損失や混雑が考えられます。 実際のｽﾙｰﾌﾟｯﾄが理想より大きい場合は、ﾍﾞｰｽﾗｲﾝとして使用した RTT が高すぎます。 TCP ｽﾙｰﾌﾟｯﾄ ｽﾃｯﾌﾟでは、 TCP 転送のより詳細な分析を行います。</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄの実測値と理想値</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ再転送ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ RTT ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄの実測値と理想値</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ再転送ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ RTT ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの理想的な転送時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの実測転送時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされていないｱｯﾌﾟｽﾄﾘｰﾑの L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされた L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの L4 ｽﾙｰﾌﾟｯﾄ理想値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていない TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされた TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされていないﾀﾞｳﾝｽﾄﾘｰﾑの L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされた L4 ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの理想的な L4 ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていない TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされた TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされていないﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｼｪｰﾋﾟﾝｸﾞされたﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>実際と理想</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ結果が示す可能性 :</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>ほかに勧告はありません。</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>ﾃｽﾄ持続時間が不足しています。</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>ﾈｯﾄﾜｰｸ ﾊﾞｯﾌｧ / ｼｪｰﾊﾟーのﾁｭｰﾆﾝｸﾞが必要です。</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>TCP ﾊﾞｰｽﾄのため、ﾎﾟﾘｻーがﾊﾟｹｯﾄを廃棄しました。</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>ｽﾙｰﾌﾟｯﾄは良好でしたが、再送信が検出されました。</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>ﾈｯﾄﾜｰｸが混雑しているか、ﾄﾗﾌｨｯｸがｼｪｰﾋﾟﾝｸﾞされています。</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>CIR の設定が間違っている可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>ﾌｧｲﾙの転送速度が速すぎます !&#xA; ﾌｧｲﾙの予測転送時間を確認してください。</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>それぞれのｳｨﾝﾄﾞｳ ｻｲﾂﾞが &lt;b>%2&lt;/b> ﾊﾞｲﾄの &lt;b>%1&lt;/b> つの接続を使用し、各接続間で &lt;b>%3&lt;/b> MB ﾌｧｲﾙ ( 合計 &lt;b>%4&lt;/b> MB) を転送しました。</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%2&lt;/b> つの接続を使用する &lt;b>%1&lt;/b> ﾊﾞｲﾄ TCP ｳｨﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>実際の L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>理想の L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>転送ﾒﾄﾘｯｸ</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>TCP 効率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>ﾊﾞｯﾌｧ遅延 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ結果が示す可能性 :</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの実測値と理想値</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>TCP 効率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>ﾊﾞｯﾌｧ遅延 (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>ｼｪｰﾋﾟﾝｸﾞされていないﾃｽﾄ結果から考えられること:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの実測値と理想値</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>再送信ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>ﾍﾞｰｽﾗｲﾝ RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>これらのｸﾞﾗﾌを使用して、再転送やﾈｯﾄﾜｰｸの混雑の影響 ( ﾍﾞｰｽﾗｲﾝを超える RTT) により TCP ﾊﾟﾌｫｰﾏﾝｽに問題が生じないか確認します。</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>再転送ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>RTT ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>接続ごとの理想的なｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>ﾄﾗﾌｨｯｸ ｼｪｰﾋﾟﾝｸﾞされたﾘﾝｸの場合、各接続の受信帯域幅は比較的安定している必要があります。 ﾄﾗﾌｨｯｸ ﾎﾟﾘｼﾝｸﾞされたﾘﾝｸでは、ﾎﾟﾘｼﾝｸﾞによる再送信に伴い各接続はﾊﾞｳﾝｽします。 %1 接続ごとに、各接続は約 %2 Mbps の帯域幅を使用する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>ﾗﾝﾀﾞﾑ</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>ﾃｽﾄ設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>詳細ｻｰﾊﾞー接続</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>詳細ﾃｽﾄ設定</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>ﾚﾎﾟｰﾄをﾛｰｶﾙに作成</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>ﾃｽﾄ識別</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>終了:結果詳細の表示</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>終了:ﾚﾎﾟｰﾄをﾛｰｶﾙに作成</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>別のﾚﾎﾟｰﾄをﾛｰｶﾙに作成</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>次の設定は必須です。</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>次の設定は範囲外です。#1 から #2 の間の値を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>次の設定は値が不正です。新しい選択を行ってください。</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>[ｸﾗｲｱﾝﾄからｻｰﾊﾞｰ] ﾃｽﾄ ﾗｲｾﾝｽがありません。</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>[ｻｰﾊﾞーからｸﾗｲｱﾝﾄ] ﾃｽﾄ ﾗｲｾﾝｽがありません。</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>ｱｸﾃｨﾌﾞなﾃｽﾄが多すぎます (最大数は #1)。</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>ﾛｰｶﾙ ｻｰﾊﾞーは予約されていません。</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>ﾘﾓｰﾄ ｻｰﾊﾞーは予約されていません。</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>ﾃｽﾄ ｲﾝｽﾀﾝｽがすでに存在します。</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>ﾃｽﾄ ﾃﾞｰﾀﾍﾞｰｽ読み取りｴﾗｰ。</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>ﾃｽﾄがﾃｽﾄ ﾃﾞｰﾀﾍﾞｰｽで見つかりません。</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>ﾃｽﾄが期限切れです。</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>ﾃｽﾄ ﾀｲﾌﾟがｻﾎﾟｰﾄされていません。</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>ﾃｽﾄ ｻｰﾊﾞーはｵﾌﾟｼｮﾝではありません。</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>ﾃｽﾄ ｻｰﾊﾞーは予約済みです。</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>ﾃｽﾄ ｻｰﾊﾞー不正要求ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>ﾘﾓｰﾄ ｻｰﾊﾞーでのﾃｽﾄは許可されていません。</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>HTTP 要求がﾘﾓｰﾄ ｻｰﾊﾞーで失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>ﾘﾓｰﾄ ｻｰﾊﾞーに使用できる十分なﾘｿｰｽがありません。</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>ﾃｽﾄ ｸﾗｲｱﾝﾄはｵﾌﾟｼｮﾝではありません。</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>ﾃｽﾄ ﾎﾟｰﾄはｻﾎﾟｰﾄされていません。</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>1 時間に多数のﾃｽﾄに失敗</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>ﾃｽﾄ ｲﾝｽﾀﾝｽのﾋﾞﾙﾄﾞに失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>ﾃｽﾄ ﾜｰｸﾌﾛーのﾋﾞﾙﾄﾞに失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>ﾜｰｸﾌﾛｰ HTTP 要求は不正です。</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>ﾜｰｸﾌﾛｰ HTTP 要求に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>ﾘﾓｰﾄﾃｽﾄは許可されていません。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>ﾘｿｰｽ ﾏﾈｰｼﾞｬで発生したｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>ﾃｽﾄ ｲﾝｽﾀﾝｽが見つかりません。</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>ﾃｽﾄ状態が競合しています。</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>ﾃｽﾄ状態が無効です。</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>ﾃｽﾄの作成に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>ﾃｽﾄの更新に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>ﾃｽﾄは正常に作成されました。</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>ﾃｽﾄは正常に更新されました。</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>HTTP 要求失敗: #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>VNF ｻｰﾊﾞｰ ﾊﾞｰｼﾞｮﾝ (#2) は機器のﾊﾞｰｼﾞｮﾝ (#1) と互換性がない可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>#1 にｻｰﾊﾞーのﾕｰｻﾞー名と認証ｷーを入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>ﾃｽﾄ初期化失敗: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>ﾃｽﾄ中止: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>ｻｰﾊﾞー接続</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>ｻｰﾊﾞーに接続するために必要な情報がありません。</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>ﾈｯﾄﾜｰｸ通信を行うためのﾘﾝｸがありません。</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>ﾈｯﾄﾜｰｸ接続のための有効な送信元 IP がありません。</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>指定した IP ｱﾄﾞﾚｽで Ping 未実行です。</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>指定された IP ｱﾄﾞﾚｽでﾕﾆｯﾄを検出できません。</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>指定された IP ｱﾄﾞﾚｽがｻｰﾊﾞーで識別されていません。</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>指定された IP ｱﾄﾞﾚｽでｻｰﾊﾞーを識別できません。</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>ｻｰﾊﾞーは識別済みですが未認証です。</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>ｻｰﾊﾞー認証に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>認証が失敗しました。識別を試行します。</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>認証または識別されていません。Ping を試行します。</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>ｻｰﾊﾞーは認証済みでﾃｽﾄに使用できます。</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>ｻｰﾊﾞーが接続されﾃｽﾄを実行中です。</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>識別中</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>識別</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>TCP ﾌﾟﾛｷｼ ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>ﾃｽﾄ ｺﾝﾄﾛｰﾗ ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>ｱｸﾃｨﾌﾞなﾘﾝｸ:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ｻｰﾊﾞ ID:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>ｻｰﾊﾞーの状態:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>‎接続の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>認証ｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>ﾕｰｻﾞー名とｷーを記憶</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>ｻｰﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>ｳｨﾝﾄﾞｳ ｳｫｰｸ持続時間 (秒)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>ｵｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>自動持続時間</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>ﾃｽﾄ設定 (詳細)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>詳細ﾃｽﾄ ﾊﾟﾗﾒｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>TCP ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>自動 TCP ﾎﾟｰﾄ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>ｳｨﾝﾄﾞｳ ｳｫｰｸ数</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>接続ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>自動接続数</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>飽和ｳｨﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>飽和ｳｨﾝﾄﾞｳの実行</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>ﾌﾞｰｽﾄ接続 (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>ｻｰﾊﾞｰ ﾚﾎﾟｰﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>ﾃｽﾄ名</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>ﾃｸﾆｼｬﾝ名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>顧客名*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>会社</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>電子ﾒｰﾙ*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>電話</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>ｺﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>ﾃｽﾄ ID の表示</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>TrueSpeed ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>ｻｰﾊﾞーが接続されていません。</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>平均(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>ﾃｽﾄ ﾘｿｰｽの準備完了待ち。待機ﾘｽﾄ#%1 番目です。</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>ｳｲﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>彩度&#xA;ウィンドウ</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>ｳｨﾝﾄﾞｳ ｻｲﾂﾞ (KB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ診断:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>ﾚﾎﾟｰﾄなし</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>低すぎるｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT 不一致</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>TCP 効率が低いです</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>ﾊﾞｯﾌｧ遅延が高いです。</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>CIR の 85% 未満のｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>1400 未満の MTU</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ診断:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Auth ｺｰﾄﾞ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Auth 作成日</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>有効期限</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>時間を修正</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>ﾃｽﾄ停止時間</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>最終変更</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>電子ﾒｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>実測値と目標値</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ実測値と目標値</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ要約</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>ﾋﾟｰｸ TCP ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>往復時間 (ﾐﾘ秒)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ要約結果 (最大ｽﾙｰﾌﾟｯﾄ ｳｨﾝﾄﾞｳ)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>接続ごとのｳｨﾝﾄﾞｳ ｻｲﾂﾞ (KB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>合計ｳｨﾝﾄﾞｳ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ目標 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>平均 TCP ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>目標</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>ｳｨﾝﾄﾞｳ 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>ｳｨﾝﾄﾞｳ 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>ｳｨﾝﾄﾞｳ 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>ｳｨﾝﾄﾞｳ 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>ｳｨﾝﾄﾞｳ 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>ｳｨﾝﾄﾞｳ 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ ｳｨﾝﾄﾞｳ:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 kB ｳｨﾝﾄﾞｳ:%2 接続 x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ詳細</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 1 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>接続ごとのｳｨﾝﾄﾞｳ ｻｲﾂﾞ (KB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>実際のｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>目標ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>再送信合計</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 2 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 3 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 4 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 5 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 6 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 7 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 8 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 9 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 10 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ飽和ｳｨﾝﾄﾞｳ ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>平均ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの実測値と目標値</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ要約</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ要約結果 (最大ｽﾙｰﾌﾟｯﾄ ｳｨﾝﾄﾞｳ)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ詳細</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 1 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 2 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 3 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 4 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 5 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 6 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 7 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 8 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 9 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｳｨﾝﾄﾞｳ 10 ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ飽和ｳｨﾝﾄﾞｳ ｽﾙｰﾌﾟｯﾄ結果</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># ｳｨﾝﾄﾞｳ ｳｫｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>過飽和ｳｨﾝﾄﾞｳ (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>過飽和接続 (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>VLAN ｽｷｬﾝ</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 VLAN ｽｷｬﾝ ﾃｽﾄ #2 の開始</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>VLAN ID #1 を #2 秒間ﾃｽﾄしています</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: 成功</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: 失敗</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>ｱｸﾃｨﾌﾞ ﾙｰﾌﾟは成功しませんでした。 VLAN ID #1 のためﾃｽﾄは失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>ﾃｽﾄ済み VLAN ID の合計</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>ID 認証の成功数</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>ID 認証の失敗数</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>ID 別の持続時間 (秒)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>範囲の数</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>選択された範囲</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>VLAN ID 最小</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID 最大</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>ﾃｽﾄ開始</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>ID の合計</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>ID 認証の成功数</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>ID 認証に失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>拡張 VLAN ｽｷｬﾝ設定</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>帯域幅 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>合格基準</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>ﾌﾚｰﾑ損失はありません</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>ﾌﾚｰﾑが受信されました</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>TrueSAM を構成 ...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>TrueSAM の実行 ...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>実行時間予測</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Stop on failure (失敗時点で停止)</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>ﾚﾎﾟｰﾄの表示</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>TrueSAM ﾚﾎﾟｰﾄの表示 ...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>ﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>信号 損失.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>ﾘﾝｸ 損失.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄとの通信が失われました。 もう一度通信ﾁｬﾈﾙを確立し、再度試行してください。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>ﾛｰｶﾙ ﾃｽﾄ ｾｯﾄの構成中に問題が発生し、ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄとの通信が失われました。 もう一度通信ﾁｬﾈﾙを確立し、再度試行してください。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄの構成中に問題が発生し、ﾃｽﾄ ｾｯﾄとの通信が失われました。 もう一度通信ﾁｬﾈﾙを確立し、再度試行してください。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>ﾛｰｶﾙ ﾃｽﾄ ｾｯﾄの構成中に問題が発生したため、 TrueSAM は終了します。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>ﾛｰｶﾙ ﾃｽﾄ ｾｯﾄの構成中に問題が発生しました。 ﾃｽﾄが強制停止され、ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄとの通信ﾁｬﾈﾙが失われました。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄの構成中に問題が発生しました。 ﾃｽﾄが強制停止され、ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄとの通信ﾁｬﾈﾙが失われました。</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>ｽｸﾘｰﾝ ｾｰﾊﾞーは、ﾃｽﾄ中の干渉防止のため無効にされています。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>ﾛｰｶﾙ ﾃｽﾄ ｾｯﾄで問題が発生しました。 TrueSAM は終了します。</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀをﾘｸｴｽﾄ中です。</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀ が取得されました。</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>ｲｰｻﾈｯﾄ ｲﾝﾀﾌｪｰｽの初期化中。お待ちください。</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>北米</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>北米と韓国</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>北米の PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>ｲﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>外部時間ｿｰｽの時刻信号喪失により、 1PPS 同期の表示がｵﾌになります。</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>外部時間ｿｰｽの時刻信号との同期開始 ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>外部時間ｿｰｽの時刻信号との同期に成功しました。</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>ﾙｰﾌﾟ &#xA; ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>ｶﾊﾞｰ ﾍﾟｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>実行するﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>ｴﾝﾄﾞﾂｰｴﾝﾄﾞ ﾄﾗﾌｨｯｸ接続ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>拡張 RFC 2544 / SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>ｲｰｻﾈｯﾄ ﾍﾞﾝﾁﾏｰｸ ﾃｽﾄ ｽｲｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Y.1564 ｲｰｻﾈｯﾄ ｻｰﾋﾞｽの構成とﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>ｺﾝﾄﾛｰﾙ ﾌﾟﾚｰﾝ ﾌﾚｰﾑのﾚｲﾔ 2 透過性ﾃｽﾄ (CDP、STP、その他)。</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>RFC 6349 TCP ｽﾙｰﾌﾟｯﾄとﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>L3- ｿｰｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>通信ﾁｬﾈﾙの設定 ( ｻｰﾋﾞｽ 1 を使用 )</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>通信ﾁｬﾈﾙ設定</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>ｻｰﾋﾞｽ 1 は、ﾘﾓｰﾄ Viavi ﾃｽﾄ機器のｽﾄﾘｰﾑ 1 と一致するように構成する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>ﾛｰｶﾙ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>ToD 同期</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>1PPS 同期</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>ﾁｬﾈﾙに&#xA;接続</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>物理 ﾚｲﾔ</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>ﾎﾟｰｽﾞ 長さ ( 量 )</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>ﾎﾟｰｽﾞ 長さ ( 時間 - ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>ﾃｽﾄ &#xA; の実行</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>ﾃｽﾄ &#xA; 開始</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>ﾃｽﾄ &#xA; を停止しています</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>ﾃｽﾄ実行時間予測</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>ﾄｰﾀﾙ</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>失敗時点で停止</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>選択されたﾃｽﾄの合否基準</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑとﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>これは合否判定事項ではないので、この設定は適用されません。</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>次のしきい値に適合すれば合格です :</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>しきい値ｾｯﾄがありません - 合否はﾚﾎﾟｰﾄされません。</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>ﾛｰｶﾙの:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>ﾘﾓｰﾄ:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>ﾌﾚｰﾑ 損失 しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>ﾚｲﾃﾝｼｰ (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>次の SLA ﾊﾟﾗﾒｰﾀに適合すれば合格です :</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>CIR ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>CIR ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>EIR ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>EIR ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>EIR ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - 許容差 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - 許容差 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - 許容差 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>ﾌﾚｰﾑ遅延 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>遅延変動 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>合否は内部で判定されます。</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ実測値が次のしきい値に適合すれば合格です :</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>予測ｽﾙｰﾌﾟｯﾄの割合</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄが有効になっていません - 合否はﾚﾎﾟｰﾄされません。</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>ﾃｽﾄの停止</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>これによって実行中のﾃｽﾄが停止し、以後のﾃｽﾄが実行されます。 停止してもよろしいですか ?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 の最大負荷合計が 0 、または #2 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のｱｯﾌﾟｽﾄﾘｰﾑ方向の最大負荷合計が 0 、または #2 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のﾀﾞｳﾝｽﾄﾘｰﾑ方向の最大負荷合計が 0 、または #2 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 の最大負荷合計が 0 、または個別の L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のｱｯﾌﾟｽﾄﾘｰﾑ方向の最大負荷合計が 0 、または個別の L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のﾀﾞｳﾝｽﾄﾘｰﾑ方向の最大負荷合計が 0 、または個別の L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; 最大負荷が #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ｱｯﾌﾟｽﾄﾘｰﾑ方向の最大負荷が #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ方向の最大負荷が #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; 最大負荷が #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ｱｯﾌﾟｽﾄﾘｰﾑ方向の最大負荷が #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ方向の最大負荷が #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>無効な構成 :&#xA;&#xA;CIR 、 EIR 、ﾎﾟﾘｼﾝｸﾞはすべて 0 にできません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ｱｯﾌﾟｽﾄﾘｰﾑ方向の CIR ､ EIR ､ﾎﾟﾘｼﾝｸﾞをすべて 0 にはできません｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>無効な構成 :&#xA;&#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ方向の CIR ､ EIR ､ﾎﾟﾘｼﾝｸﾞをすべて 0 にはできません｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #3 の #2 Mbps CIR 設定では最小ｽﾃｯﾌﾟ負荷 (CIR の #1%) が得られません。 ｻｰﾋﾞｽ #3 の現在の CIR を使用した最小ｽﾃｯﾌﾟ値は #4% です。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA;CIR (L1 Mbps) 合計が 0 、または #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ｱｯﾌﾟｽﾄﾘｰﾑ方向の CIR (L1 Mbps) 合計が 0 、または #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ方向の CIR (L1 Mbps) 合計が 0 、または #1 L1 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA;CIR (L2 Mbps) 合計が 0 、または #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ｱｯﾌﾟｽﾄﾘｰﾑ方向の CIR (L2 Mbps) 合計が 0 、または #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>無効な構成 :&#xA;&#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ方向の CIR (L2 Mbps) 合計が 0 、または #1 L2 Mbps ﾗｲﾝ ﾚｰﾄを超えています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ構成ﾃｽﾄとｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄのどちらかを必ず選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>無効な構成 :&#xA;&#xA; 少なくとも 1 つのｻｰﾋﾞｽを選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>無効な構成:&#xA;&#xA;ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄを実行する場合、選択したｻｰﾋﾞｽの CIR (または CIR が 0 のｻｰﾋﾞｽの EIR) の合計がﾗｲﾝ ﾚｰﾄを超えることはできません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は、選択したｻｰﾋﾞｽの CIR の合計より小さい値に指定できません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は､選択したｻｰﾋﾞｽの CIR の合計より小さい値に指定できません｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は､選択したｻｰﾋﾞｽの CIR の合計より小さい値に指定できません｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は CIR より小さい値に指定できません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は CIR より小さい値に指定できません｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ実行時のﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ測定 (RFC 2544) 最大値は CIR より小さい値に指定できません｡</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>失敗時点で停止が選択され、 1 つ以上の KPI がｻｰﾋﾞｽ #1 の SLA を満たしていないため、 SAMComplete は停止しました。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>ﾄﾗﾌｨｯｸの開始後 10 秒以内にｻｰﾋﾞｽ #1 のﾃﾞｰﾀが返されなかったため、 SAMComplete が停止しました。 遠端はﾄﾗﾌｨｯｸをﾙｰﾌﾟ ﾊﾞｯｸできません。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>ﾄﾗﾌｨｯｸの開始後 10 秒以内にｻｰﾋﾞｽ #1 のﾃﾞｰﾀが返されなかったため､ SAMComplete が停止しました｡遠端からﾄﾗﾌｨｯｸを送信できません｡</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>ﾄﾗﾌｨｯｸの開始後 10 秒以内にﾃﾞｰﾀが返されなかったため、 SAMComplete が停止しました。 遠端はﾄﾗﾌｨｯｸをﾙｰﾌﾟ ﾊﾞｯｸできません。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>ﾄﾗﾌｨｯｸの開始後 10 秒以内にﾃﾞｰﾀが返されなかったため､ SAMComplete が停止しました｡遠端からﾄﾗﾌｨｯｸを送信できません｡</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝとﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはｽﾄﾘｰﾑ ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはﾄﾗﾌｨｯｸ ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはｽﾄﾘｰﾑ ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは TCP WireSpeed ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄのｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ 3 ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ 2 ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝとﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝに互換性がありません｡ﾛｰｶﾙ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ 2 ｱﾌﾟﾘｹｰｼｮﾝ､ IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝはﾚｲﾔ 3 ｱﾌﾟﾘｹｰｼｮﾝです｡</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝが WAN IP 用に設定されています。これは SAMComplete との互換性がありません。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは、ｽﾀｯｸされた VLAN ｶﾌﾟｾﾙ化の設定がなされています。 これは SAMComplete との互換性がありません。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは VPLS ｶﾌﾟｾﾙ化の設定がなされています。これは SAMComplete との互換性がありません。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP ｱﾄﾞﾚｽ #1 のﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは MPLS ｶﾌﾟｾﾙ化の設定がなされています｡これは SAMComplete との互換性がありません｡</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>ﾘﾓｰﾄ ｱﾌﾟﾘｹｰｼｮﾝは #1 ｻｰﾋﾞｽのみをｻﾎﾟｰﾄします。 </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>一方向の遅延ﾃｽﾄでは､ﾛｰｶﾙ ﾃｽﾄ ﾕﾆｯﾄとﾘﾓｰﾄ ﾃｽﾄ ﾕﾆｯﾄの両方で OWD 時間ｿｰｽの同期をとる必要があります｡ﾛｰｶﾙ ﾕﾆｯﾄで一方向の遅延の同期に失敗しました｡ OWD 時間ｿｰｽ ﾊｰﾄﾞｳｪｱのすべての接続を確認してください｡</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>一方向の遅延ﾃｽﾄでは､ﾛｰｶﾙ ﾃｽﾄ ﾕﾆｯﾄとﾘﾓｰﾄ ﾃｽﾄ ﾕﾆｯﾄの両方で OWD 時間ｿｰｽの同期をとる必要があります｡ﾘﾓｰﾄ ﾕﾆｯﾄで一方向の遅延の同期に失敗しました｡ OWD 時間ｿｰｽ ﾊｰﾄﾞｳｪｱのすべての接続を確認してください｡</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>無効な構成 :&#xA;&#xA;SAMComplete ﾃｽﾄの前に往復時間 (RTT) ﾃｽﾄを実行する必要があります｡</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Unable to establish TrueSpeed ｾｯｼｮﾝを確立できません。 [ ﾈｯﾄﾜｰｸ ] ﾍﾟｰｼﾞに移動して、構成を検証し、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ方向の TrueSpeed ｾｯｼｮﾝを確立できません。 [ ﾈｯﾄﾜｰｸ ] ﾍﾟｰｼﾞに移動して、構成を検証し、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ方向の TrueSpeed ｾｯｼｮﾝを確立できません。 [ ﾈｯﾄﾜｰｸ ] ﾍﾟｰｼﾞに移動して、構成を検証し、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>ﾃｽﾄするｻｰﾋﾞｽの選択を変更したため、往復時間 (RTT) ﾃｽﾄは無効になりました。 [TrueSpeed ｺﾝﾄﾛｰﾙ ] ﾍﾟｰｼﾞに移動して、 RTT ﾃｽﾄを再度実行してください。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のﾀﾞｳﾝｽﾄﾘｰﾑ方向の CIR (Mbps) 合計は 0 にできません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>無効な構成 :&#xA;&#xA; ｻｰﾋﾞｽ #1 のｱｯﾌﾟｽﾄﾘｰﾑ方向の CIR (Mbps) 合計は 0 にできません。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>無効な構成:&#xA;&#xA;ｻｰﾋﾞｽ #1 の CIR (Mbps) は 0 にできません。</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>ﾄﾗﾌｨｯｸが受信されていません。 [ ﾈｯﾄﾜｰｸ ] ﾍﾟｰｼﾞに移動して、構成を検証し、再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>結果ﾒｲﾝ ﾋﾞｭｰ</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>ﾃｽﾄ &#xA; 停止</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>  ﾚﾎﾟｰﾄが作成されました。 [ 次へ ] をｸﾘｯｸすると表示されます</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>ﾚﾎﾟｰﾄ表示のｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete - ｲｰｻﾈｯﾄ ｻｰﾋﾞｽ ｱｸﾃｨﾍﾞｰｼｮﾝ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>TrueSpeed を含む</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>ﾛｰｶﾙ ﾈｯﾄﾜｰｸ設定</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>ﾛｰｶﾙ ﾕﾆｯﾄの構成は必要ありません｡</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>ﾘﾓｰﾄ ﾈｯﾄﾜｰｸ設定</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>ﾘﾓｰﾄ ﾕﾆｯﾄの構成は必要ありません｡</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>ﾛｰｶﾙ IP 設定</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>ﾘﾓｰﾄ IP 設定</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>ｻｰﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>ﾀｷﾞﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>ﾀｷﾞﾝｸﾞは使用されていません。</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>SLA Throughput</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>SLA ﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄが選択されていません。</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>SLA ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄは必要な機能がないため無効になっています。</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>SLA ﾊﾟﾌｫｰﾏﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>ueSpeed は現在無効です。 &#xA;&#xA;TrueSpeed は、ﾙｰﾌﾟﾊﾞｯｸ測定ﾓｰﾄﾞにないときに</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>ﾛｰｶﾙの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>ﾄﾗﾌｨｯｸの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>LBM の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>SLA の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>ﾗﾝﾀﾞﾑ/EMIX ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>ｱﾄﾞﾊﾞﾝｽﾄ IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 詳細</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>詳細ﾀｷﾞﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>ｻｰﾋﾞｽ構成ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Y.1564 ﾃｽﾄの終了</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>検出ｻｰﾊﾞーのｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>検出ｻｰﾊﾞーのﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>MAC ｱﾄﾞﾚｽと ARP ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>SFP 選択</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>WAN 送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>静的 - WAN IP</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>WAN ｹﾞｰﾄｳｪｲ</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>WAN ｻﾌﾞﾈｯﾄﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>ﾄﾗﾌｨｯｸ送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>ﾄﾗﾌｨｯｸ ｻﾌﾞﾈｯﾄ ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>ﾛｰｶﾙ ﾈｯﾄﾜｰｸ ﾎﾟｰﾄで VLAN ﾀｷﾞﾝｸﾞが使用されていますか ?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q ( ｽﾀｯｸされた VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>待機中</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>ｻｰﾊﾞーにｱｸｾｽ中 ...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>ｻｰﾊﾞーにｱｸｾｽできません</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP が取得されました</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>許可されたﾘｰｽ : #1 分</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>短縮されたﾘｰｽ : #1 分</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>解除されたﾘｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>検出ｻｰﾊﾞｰ :</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR、#2 ms FTD、#3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>ｻｰﾋﾞｽ 1: VoIP - ﾄﾗﾌｨｯｸの 25% - #1 および #2 ﾊﾞｲﾄ ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>ｻｰﾋﾞｽ 2: ﾋﾞﾃﾞｵ ﾃﾚﾌｫﾆｰ - ﾄﾗﾌｨｯｸの 10% - #1 および #2 ﾊﾞｲﾄ ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>ｻｰﾋﾞｽ 3: 優先度ﾃﾞｰﾀ - ﾄﾗﾌｨｯｸの 15% - #1 および #2 ﾊﾞｲﾄ ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>ｻｰﾋﾞｽ 4: BE ﾃﾞｰﾀ - ﾄﾗﾌｨｯｸの 50% - #1 および #2 ﾊﾞｲﾄ ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>全ての ｻｰﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 1 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>音声</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U-law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A-law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U-law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A-law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>10 ｷﾞｶﾞﾋﾞｯﾄ ｲｰｻ の帯域幅細分性は 0.1 Mbps です。したがって CIR 値はこの値の倍数となります</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>40 ｷﾞｶﾞ ﾋﾞｯﾄｲｰｻ の帯域幅細分性は 0.4 Mbps です。したがって CIR 値はこの値の倍数となります</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>100 ｷﾞｶﾞ ﾋﾞｯﾄｲｰｻ の帯域幅細分性は 1 Mbps です。したがって CIR 値はこの値の倍数となります</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>ｻｲﾂﾞの構成</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ ( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>定義された長さ</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>EMIX ｻｲｸﾙ長</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>ﾊﾟｹｯﾄ 長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>計算 ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>計算ﾌﾚｰﾑ ｻｲﾂﾞは、ﾊﾟｹｯﾄ長とｶﾌﾟｾﾙ化を使用して決定されます。</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 4 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 5 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 6 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 7 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 8 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 9 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>ｻｰﾋﾞｽ 10 ﾄﾘﾌﾟﾙﾌﾟﾚｲ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>ｱﾝﾀﾞｰｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>ｻｰﾋﾞｽの数</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>ﾚｲﾔ</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>LBM 設定</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>ﾄﾘﾌﾟﾙﾌﾟﾚｲの構成</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>ｻｰﾋﾞｽ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC とﾌﾚｰﾑ ｻｲﾂﾞの設定</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC、ﾌﾚｰﾑ ｻｲﾂﾞ設定、および EtherType</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC 、ﾊﾟｹｯﾄ長、 TTL の設定</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>DA MAC とﾊﾟｹｯﾄ長の設定</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>ﾈｯﾄﾜｰｸ設定 ( 詳細 )</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>ﾛｰｶﾙ / ﾘﾓｰﾄ ﾕﾆｯﾄに互換性がない場合は &lt;b>%1&lt;/b> ﾊﾞｲﾄ ﾊﾟｹｯﾄ長以上が必要です。</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>ｻｰﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>構成 ...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>  ﾕｰｻﾞｰ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL ( ﾎｯﾌﾟ数 )</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>宛先 MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>両方の表示</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>ﾘﾓｰﾄのみ</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>ﾛｰｶﾙのみ</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>LBM 設定 ( 詳細 )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>ﾈｯﾄﾜｰｸ設定ﾗﾝﾀﾞﾑ/EMIX ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 1 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>ﾊﾟｹｯﾄ長 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>特大ｻｲﾂﾞ 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>ﾊﾟｹｯﾄ長 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>特大ｻｲﾂﾞ 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>ﾊﾟｹｯﾄ長 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>特大ｻｲﾂﾞ 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>ﾊﾟｹｯﾄ長 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>特大ｻｲﾂﾞ 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>ﾊﾟｹｯﾄ長 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>特大ｻｲﾂﾞ 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>ﾊﾟｹｯﾄ長 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>特大ｻｲﾂﾞ 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>ﾊﾟｹｯﾄ長 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>特大ｻｲﾂﾞ 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>ﾌﾚｰﾑ ｻｲﾂﾞ 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>ﾊﾟｹｯﾄ長 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>ﾕｰｻﾞｰ ｻｲﾂﾞ 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>特大ｻｲﾂﾞ 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 1 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 1 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 2 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 2 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 2 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 3 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 3 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 3 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 4 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 4 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 4 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 5 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 5 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 5 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 6 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 6 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 6 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 7 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 7 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 7 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 8 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 8 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 8 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 9 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 9 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 9 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>ｻｰﾋﾞｽ 10 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>ｻｰﾋﾞｽ 10 ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>ｻｰﾋﾞｽ 10 ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>ｻｰﾋﾞｽの VLAN ID またはﾕｰｻﾞー優先度はほかにもありますか ?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>いいえ、すべてのｻｰﾋﾞｽで同じ VLAN 設定を使用します</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>詳細 ...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>ﾕｰｻﾞｰ PRI</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>SVLAN PRI</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>VLAN ﾀｷﾞﾝｸﾞ ( ｶﾌﾟｾﾙ化 ) 設定</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>SVLAN 優先度</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>すべてのｻｰﾋﾞｽが同じﾄﾗﾌｨｯｸ宛先 IP を使用します。</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>ﾄﾗﾌｨｯｸ宛先 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>ﾄﾗﾌｨｯｸ宛先IP</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>ID ｲﾝｸﾘﾒﾝﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>IP 設定値 (ﾛｰｶﾙのみ)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>IP 設定</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>IP 設定値 (ﾘﾓｰﾄのみ)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>IP 設定 ( 詳細 )</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>IP の詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>ｻｰﾋﾞｽの TOS または DSCP 設定はほかにもありますか ?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>いいえ、すべてのｻｰﾋﾞｽで TOS/DSCP は同じです</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>SLA の合計</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>CIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ CIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ CIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>EIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ EIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ EIR の合計</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>合計ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>ﾛｰｶﾙのｽﾙｰﾌﾟｯﾄ ﾃｽﾄ最大値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>ﾘﾓｰﾄのｽﾙｰﾌﾟｯﾄ ﾃｽﾄ最大値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>合計ﾓｰﾄﾞを有効にする</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>警告: 選択した重み値は現在、合計が &lt;b>%1&lt;/b>% であり、100% になっていません</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>SLA ｽﾙｰﾌﾟｯﾄ､ Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA ｽﾙｰﾌﾟｯﾄ、 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>SLA ｽﾙｰﾌﾟｯﾄ、 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>SLA ｽﾙｰﾌﾟｯﾄ、 L1 Mbps ( 一方向 )</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA ｽﾙｰﾌﾟｯﾄ、 L2 Mbps ( 一方向 )</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>重み (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>ﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>最大負荷</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ専用</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ専用</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>ﾄﾗﾌｨｯｸの詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>SLA ﾎﾟﾘｼﾝｸﾞ､ Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>最大ﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>許容差 (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>許容差 (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄを実行しますか。</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄ ﾀｲﾌﾟ :</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>ﾊﾞｰｽﾄの詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>ﾌﾚｰﾑ遅延精度</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>ﾌﾚｰﾑ遅延を SLA 要件として含める</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>ﾌﾚｰﾑ遅延変動を SLA 要件として含める</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>SLA ﾊﾟﾌｫｰﾏﾝｽ ( 一方向 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>ﾌﾚｰﾑ遅延 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>ﾌﾚｰﾑ遅延 (OWD ､ ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>SLA の詳細設定を設定</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>ｻｰﾋﾞｽ構成</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>CIR 未満のｽﾃｯﾌﾟ数</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>ｽﾃｯﾌﾟ持続時間 ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>ｽﾃｯﾌﾟ 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>ｽﾃｯﾌﾟの割合</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>方向別にﾃｽﾄするので、全体のﾃｽﾄ持続時間は入力値の 2 倍になります。</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Stop Test on failure (失敗時点でﾃｽﾄ停止)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>ﾊﾞｰｽﾄ ﾃｽﾄの詳細設定</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>J-QuickCheck のｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Y.1564 ﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>ｻｰﾋﾞｽを選択してﾃｽﾄする</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>全て設定</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>ﾄｰﾀﾙ:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>ｻｰﾋﾞｽ構成ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>ｵﾌﾟｼｮﾝ測定</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>最大 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>最大 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>最大 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>TrueSpeed ｻｰﾋﾞｽを有効にすると､ｵﾌﾟｼｮﾝ測定は実行できません｡</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Y.1564 ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Y.1564 ﾃｽﾄのｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>遅延変動</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>ﾃｽﾄ失敗の要約</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>判定</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Y.1564 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>構成ﾃｽﾄ判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 1 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 2 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 3 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 4 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 5 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 6 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 7 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 8 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 9 判定</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>構成ﾃｽﾄ ｻｰﾋﾞｽ 10 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 1 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 2 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 3 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 4 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 5 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 6 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 7 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 8 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 9 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ｻｰﾋﾞｽ 10 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ IR 判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ ﾌﾚｰﾑ損失判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ遅延判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ遅延変動判定</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄ TrueSpeed 判定</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>ｻｰﾋﾞｽ構成結果</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 1 構成結果</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの最大ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの最大ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>CIR 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの CIR 判定</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>ﾌﾚｰﾑ遅延変動 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>OoS ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>ｴﾗｰ ﾌﾚｰﾑ検出</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>ﾎﾟｰﾂﾞの検出</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの CIR 判定</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>CBS 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの CBS 判定</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>構成済みﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tx ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>平均 Rx ﾊﾞｰｽﾄ ｻｲﾂﾞ (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>予測 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの CBS 判定</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>EIR 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの EIR 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの EIR 判定</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>ﾎﾟﾘｼﾝｸﾞ判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾎﾟﾘｼﾝｸﾞ判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾎﾟﾘｼﾝｸﾞ判定</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 1 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 1 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 1 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 2 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 2 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 2 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 3 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 3 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 3 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 4 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 4 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 4 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 5 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 5 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 5 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 6 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 6 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 6 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 7 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 7 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 7 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 8 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 8 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 8 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 9 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 9 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 9 判定</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>ｽﾃｯﾌﾟ 10 判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｽﾃｯﾌﾟ 10 判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｽﾃｯﾌﾟ 10 判定</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>ｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>ﾊﾞーをｸﾘｯｸして各ｽﾃｯﾌﾟの結果を確認します。</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (ｽﾙｰﾌﾟｯﾄ Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (ｽﾙｰﾌﾟｯﾄ L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (ｽﾙｰﾌﾟｯﾄ L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>ｴﾗー検出</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>1 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>2 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>3 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>4 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>5 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>6 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>7 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>8 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>9 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>10 ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>SLA しきい値</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 2 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 3 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 4 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 5 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 6 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 7 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 8 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 9 構成結果</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>ｻｰﾋﾞｽ 10 構成結果</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ結果</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>ｻｰﾋﾞｽ判定</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR 現在</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR 最大</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR 最小</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR 平均</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>ﾌﾚｰﾑ損失秒数</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>ﾌﾚｰﾑ損失数</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>遅延、現在</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>遅延、最大</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>遅延、最小</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>遅延、平均</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>遅延変動 現在</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>遅延変動 最大</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>遅延変動 最小</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>遅延変動 平均</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>可用性</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>使用可能</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>使用可能秒数</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>使用不可能な秒数</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>重大ｴﾗー秒数</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>遅延変動</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>TrueSpeed ｻｰﾋﾞｽ｡ TrueSpeed 結果ﾍﾟｰｼﾞに結果を表示します｡</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR、平均&#xA;(ｽﾙｰﾌﾟｯﾄ&#xA;L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR、平均&#xA;(ｽﾙｰﾌﾟｯﾄ&#xA;L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>ﾌﾚｰﾑﾛｽ率</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>一方向の遅延 &#xA; 平均 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>遅延変動 &#xA; 最大 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>ｴﾗーが検出されました</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>遅延 &#xA; 平均 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR、現在 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR、現在 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR、最大 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR、最大 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR、最小 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR、最小 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>ﾌﾚｰﾑ損失数</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>ﾌﾚｰﾑ損失 &#xA; 率 &#xA; しきい値</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>遅延 &#xA; 現在 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>遅延 &#xA; 最大 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>遅延 &#xA; 最小 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>遅延 &#xA; 平均 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>遅延 &#xA; しきい値 &#xA;(OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>遅延 &#xA; 現在 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>遅延 &#xA; 最大 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>遅延 &#xA; 最小 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>遅延 &#xA; しきい値 &#xA;(RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>遅延変動 &#xA; 現在 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>遅延変動 &#xA; 平均 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>遅延変動 &#xA; しきい値 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>使用可能 &#xA; 秒数 &#xA; 比</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>使用不可能 &#xA; 秒数</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>重大 &#xA; ｴﾗｰ &#xA; 秒数</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>ｻｰﾋﾞｽ構成 &#xA; ｽﾙｰﾌﾟｯﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>ｻｰﾋﾞｽ構成 &#xA; ｽﾙｰﾌﾟｯﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ &#xA; ｽﾙｰﾌﾟｯﾄ &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ &#xA; ｽﾙｰﾌﾟｯﾄ &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>ﾈｯﾄﾜｰｸ設定</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>ﾕｰｻﾞ ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>ｼﾞｬﾝﾎﾞ ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>ﾕｰｻﾞ ﾊﾟｹｯﾄ 長</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>ｼﾞｬﾝﾎﾞ ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>計算 ﾌﾚｰﾑ ｻｲｽﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>ﾈｯﾄﾜｰｸ設定 (ﾗﾝﾀﾞﾑ/EMIX ｻｲﾂﾞ)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>ﾗﾝﾀﾞﾑ/EMIX 長 (ﾘﾓｰﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>ﾗﾝﾀﾞﾑ/EMIX 長 (ﾛｰｶﾙ)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>ﾗﾝﾀﾞﾑ/EMIX 長</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>IP の詳細設定 - ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの M</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの M</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA 詳細ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾌﾚｰﾑ損失率</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾌﾚｰﾑ遅延 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾌﾚｰﾑ遅延 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの遅延変動 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾌﾚｰﾑ損失率</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾌﾚｰﾑ遅延 (OWD 、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのﾌﾚｰﾑ遅延 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの遅延変動 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>SLA 要件として含める</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>ﾌﾚｰﾑ遅延を含める</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>ﾌﾚｰﾑ遅延変動を含める</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete の次の構成設定は無効です :</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>ｻｰﾋﾞｽ構成結果</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>TrueSpeed の実行</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>注 :TrueSpeed ﾃｽﾄが実行されるのは、ｻｰﾋﾞｽ ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄが可能な場合に限られます。</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>ﾊﾟｹｯﾄ長 TTL の設定</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>TCP/UDP ﾎﾟｰﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>送信元のﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>送信元ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>送信先のﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>送信先ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄしきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>推奨されるﾄｰﾀﾙ ｳｨﾝﾄﾞｳ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>ﾌﾞｰｽﾄ合計ｳｨﾝﾄﾞｳ ｻｲﾂﾞ (ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>推奨される接続数</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>ﾌﾞｰｽﾄ # 接続</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの推奨される合計ｳｨﾝﾄﾞｳ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾞｰｽﾄ合計ｳｨﾝﾄﾞｳｻｲﾂﾞ (ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの推奨される接続数</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾞｰｽﾄ # 接続</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの推奨される合計ｳｨﾝﾄﾞｳ ｻｲﾂﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾞｰｽﾄ合計ｳｨﾝﾄﾞｳ ｻｲﾂﾞ (ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの推奨される接続数</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾞｰｽﾄ # 接続</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>推奨される接続の数</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Acterna ﾍﾟｲﾛｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>ﾗｳﾝﾄﾞﾄﾘｯﾌﾟ ﾀｲﾑ (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>以降のｽﾃｯﾌﾟでは、ｳｨﾝﾄﾞｳ ｻｲﾂﾞと接続数の推奨値を出すために RTT が使用されます。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>TrueSpeed 結果</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>TCP 転送ﾒﾄﾘｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>目標 L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>TrueSpeed ｻｰﾋﾞｽ判定</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TrueSpeed ｻｰﾋﾞｽ判定</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ目標 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>TCP ｽﾙｰﾌﾟｯﾄ目標 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TrueSpeed ｻｰﾋﾞｽ判定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ実測値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ目標 (Mbps)</translation>
        </message>
    </context>
</TS>
