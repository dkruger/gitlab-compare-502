<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Dateien</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Wechselmedien</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Keine Geräte erkannt.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formatieren</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>Beim Formatieren dieses USB-Geräts werden alle darauf befindlichen Daten gelöscht. Dies gilt für alle Dateien und alle Partitionen.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Auswerfen</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Durchsuchen...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>KEINE</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NEIN</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Bluetooth-Verbindungsanfrage</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>PIN zum Koppeln eingeben</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Paar</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Kopplungsanfrage</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Kopplungsanfrage von:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Um mit diesem Gerät zu koppeln, stellen Sie sicher, dass der unten angezeigte Code mit dem Code auf dem Gerät übereinstimmt</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Kopplung initiiert</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Kopplungsanfrage gesendet an:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Scannen starten</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Scannen anhalten</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Bluetooth aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Anderen Geräten die Kopplung mit diesem Gerät erlauben</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Gerätename</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Mobilapp aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Netzwerk</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>IP-Modus</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statisch</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC-Adresse</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Subnetzmaske</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Gateway</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>DNS-Server</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>IPv6-Modus</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Deaktiviert</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuell</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Subnetz-Präfixlänge</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>DNS-Server</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Link - Lokale Adresse</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Zustandslose Adresse</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Zustandsorientierte Adresse</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Module wurden nicht geladen</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Nicht vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Starten</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Initialisieren</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Bereit</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Scannen</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Anhalten</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Zuordnen</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Zugeordnet</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>WLAN-Adapter aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Verbunden</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Getrennt</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Mit WPA Firmennetzwerk verbinden</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Netzwerkname</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>Äußere Echtheitsmethode</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Innere Echtheitsmethode</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Benutzername</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>Anonyme Identität</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Passwort</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>Zertifikate</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Privates Schlüsselpasswort</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Mit WPA Persönliches Netzwerk verbinden</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Passphrase</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>Kein USB-WiFi-Gerät gefunden.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Verbindung trennen</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Netzwerk vergessen</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>Keine Verbindung zum Netzwerk möglich.</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>3G Service</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>Modem aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>Modem aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>Modem deaktiviert</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>Kein Modem erkannt</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Fehler - Modem reagiert nicht.</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Fehler - Gerätesteuerung nicht gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Fehler - Moden kann nicht konfiguriert werden.</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Warnung - Modem reagiert nicht. Modem wird zurückgesetzt...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Fehler - SIM nicht eingesetzt.</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Fehler - Netzwerkregistrierung verweigert.</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Trennen</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Unterbrechung:</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Nicht verbunden</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Verbindung fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Modemeinstellung</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Modem aktivieren</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>Kein Modemgerät gefunden oder aktiviert.</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>Netzwerk APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>Verbindungsstatus</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>APN überprüfen und erneut versuchen.</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS Server</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec DNS Server</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Vertretung &amp; Sicherheit</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Proxytyp</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Proxy-Server</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Netzwerk Sicherheit</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>FTP, Telnet, und auto StrataSync deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Netzmanagement</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>AC-Netzkabel ist eingesteckt</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Akkubetrieb</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Kein Akku erkannt.</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Ladevorgang wurde aufgrund des Stromverbrauchs deaktiviert</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Akku aufladen</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>Akku ist vollständig aufgeladen</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>Batterie lädt nicht</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Akku konnte aufgrund eines Fehlers bei der Stromversorgung nicht geladen werden</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Unbekannter Akkustatus</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>Die Batterie ist zu heiß und lädt vielleicht nicht</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>Die Batterie ist zu heiß&#xA;Stecken Sie bitte das AC-Kabel ein</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>Die Batterie ist zu heiß&#xA;Stecken Sie das AC-Kabel nicht aus</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>Die Batterie ist zu heiß&#xA;Das Ausstecken des AC-Kabels erzwingt ein Herunterfahren</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>Die Batterie ist zu kalt und lädt vielleicht nicht</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>Es besteht die Gefahr, dass der Akku überhitzt wird</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>Akkutemperatur ist normal</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Prozent geladen</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Automatisches Ausschalten während Batteriebetrieb erlauben</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Inaktive Zeit (Minuten)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Datum und Uhrzeit</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Zeitzone</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Region</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>Afrika</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>Nord-, Mittel- und Südamerika</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antarktis</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Asien</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Atlantik</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Australien</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Europa</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Indischer Ozean</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Pazifik</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>Land</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keines</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afghanistan</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Åland Islands</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Albanien</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Algerien</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Amerikanisch-Samoa</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorra</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angola</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguilla</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua und Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Argentinien</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Armenien</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Österreich</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Aserbaidschan</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamas</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahrain</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladesch</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbados</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Weißrussland</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Belgien</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belize</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Benin</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermudas</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Bhutan</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolivien</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bosnien und Herzegowina</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botswana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Bouvetinsel</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brasilien</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Britisches Territorium im Indischen Ozean</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Darussalam</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgarien</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Kambodscha</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Kamerun</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Kanada</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Kap Verde</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Kaimaninseln</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>Zentralafrikanische Republik</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Tschad</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Chile</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>China</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Weihnachtsinsel</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Kokosinseln</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Kolumbien</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Komoren</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Kongo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>Kongo, Demokratische Republik</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Cookinseln</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Costa Rica</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Elfenbeinküste</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Kroatien</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Kuba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Zypern</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>Tschechische Republik</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Dänemark</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Dschibuti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominica</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>Dominikanische Republik</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Ecuador</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Ägypten</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>El Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Äquatorialguinea</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Eritrea</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estland</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Äthiopien</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Falkland-Inseln (Malvinas)</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Färöer</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fidschi</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finnland</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>Frankreich</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Französisch-Guyana</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Französisch-Polynesien</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Französische Südgebiete</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabun</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambia</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Georgia</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Deutschland</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Ghana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Gibraltar</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Griechenland</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Grönland</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Grenada</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Guadeloupe</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Guernsey</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Guinea</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Guinea-Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guyana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haiti</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Heard und McDonaldinseln</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hongkong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Ungarn</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Island</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Indien</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Indonesien</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Iran</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Irak</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Irland</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Isle of Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Israel</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Italien</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Jamaika</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Japan</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Jordanien</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Kasachstan</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Kenia</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Korea, Demokratische Volksrepublik</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Korea, Republik</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Kuwait</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Kirgisistan</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Laos, Demokratische Volksrepublik</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Lettland</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Libanon</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesotho</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Liberia</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Libyen</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Malaysia</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Litauen</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Luxemburg</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Mazedonien, Ehemalige jugoslawische Republik</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagaskar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malaysia</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Malediven</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Mali</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Malta</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Marshallinseln</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinique</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Mauretanien</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>Mauritius</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>Mexico</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Mikronesien, Föderierte Staaten von</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Moldawien, Republik von</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Monaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Mongolei</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Montenegro</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Montserrat</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Marokko</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Mosambik</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Myanmar</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namibia</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Nepal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Niederlande</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Niederländisch-Antillen</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Neukaledonien</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Neuseeland</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nicaragua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Niger</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nigeria</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Norfolkinsel</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Nördliche Marianen</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Norwegen</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Oman</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Pakistan</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palau</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Palästinensisches Gebiet</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panama</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papua-Neuguinea</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguay</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Peru</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Philippinen</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Polen</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portugal</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Puerto Rico</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Katar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>Réunion</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Rumänien</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Russische Föderation</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Ruanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>Saint Barthélemy</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Saint Helena, Ascension und Tristan da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>St. Kitts und Nevis</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>St. Lucia</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>Saint Martin</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>St. Pierre und Miquelon</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>St. Vincent und die Grenadinen</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>San Marino</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Sao Tome und Principe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Saudi-Arabien</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Senegal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Serbien</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seychellen</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Sierra Leone</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapur</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Slowakei</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Slowenien</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Salomonen</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somalia</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Südafrika</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Südgeorgien und Südliche Sandwichinseln</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>Südkorea</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Sudan</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Suriname</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Svalbard und Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Swasiland</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Schweden</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Schweiz</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>Arabische Republik Syrien</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Taiwan</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tadschikistan</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Tansania, Vereinigte Republik</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Thailand</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Osttimor</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinidad und Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunesien</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Türkei</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Turkmenistan</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Turks- und Caicosinseln</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Uganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ukraine</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Vereinigte Arabische Emirate</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>Vereinigte Staaten von Amerika</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Kleinere amerikanische Überseeinseln</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Usbekistan</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Vatikanstadt</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Vietnam</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Jungferninseln, Britisch</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Jungferninseln, U.S.</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis und Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Westsahara</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Jemen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Sambia</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Simbabwe</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Fläche</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Südpol</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Wostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Australian Capital Territory</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>North</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>New South Wales</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>South</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tasmanien</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Victoria</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>West</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasilien</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasilien - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasilien + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alaska</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlantic</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Central</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Eastern</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Hawaii</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Mountain</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>New Foundland</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pacific</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Osterinsel</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Galapagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Marquesas</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Tahiti</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Western</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>East </translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Phönixinseln</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Line-Inseln</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Gilbert-Inseln</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Northwest</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Azoren</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madeira</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrad</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnojarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Moskau</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Wladiwostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Jakutien</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Ekaterinburg</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Kanarische Inseln</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Wake</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Automatisch auf Sommer-/Winterzeit umstellen</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Aktuelles Datum/Uhrzeit</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 Stunden</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 Stunden</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Uhr mit NTP einstellen</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>wahr</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>falsch</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>24-Stundenanzeige</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP Server</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP Server</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>NTP Server 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>NTP Server 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (Englisch)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (Spanisch)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (Französisch)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (Chinesisch)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Japanisch)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (Koreanisch)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (Russisch)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Portugiesisch)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (Italienisch)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Türkisch)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Sprache:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Standardformat ändern:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Muster für ausgewähltes Format:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Anpassen</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Anzeige</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Helligkeit</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Bildschirmschoner</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Bildschirmschoner aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Meldung</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Bildschirmschonerkennwort</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Touchdisplay kalibrieren...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Gegenstelle</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>VNC-Zugriff aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>Wenn Sie den VNC-Zugriff oder den Kennwortschutz aktiveren bzw. deaktivieren, werden bestehenden Verbindungen getrennt.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Fernzugriff Passwort</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Dieses Passwort wird für sämtliche entfernten Zugänge verwendet, z.B. vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>Beim Starten von VNC ist ein Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Passwort verlangen zum VNC Zugriff</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Smarter Zugang überall</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Zugangscode</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>LAN-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Wi-Fi-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Anzahl der VPN-Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Upgrade</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>Upgrade kann nicht durchgeführt werden, das AC-Kabel ist nicht eingesteckt. Bitte stecken Sie das AC-KAbel ein und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>Es konnte kein USB-Flashmedium erkannt werden. Überprüfen Sie, ob das USB-Flashmedium ordnungsgemäß angeschlossen ist und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Upgrade konnte nicht installiert werden. Geben Sie den Upgradeserver an und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>Keine Netzwerkverbindung verfügbar. Überprüfen Sie die Netzwerkverbindung und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Keine Verbindung zum Upgradeserver möglich. Überprüfen Sie die Serveradresse und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Beim Abrufen der verfügbaren Upgrades ist ein Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>Upgrade kann nicht durchgeführt werden, das ausgewählte Upgrade enthält fehlende oder korrupte Dateien.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>Upgrade kann nicht durchgeführt werden, das ausgewählte Upgrade enthält korrupte Dateien.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>Fehler beim Erstellen der Upgradedaten für USB-Geräte.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Keine Upgrades verfügbar. Überprüfen Sie die Einstellungen und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Bei dem Versuch, das Upgrade zu installieren, ist ein Fehler aufgetreten. Wiederholen Sie diesen Vorgang. Wenn das Problem weiterhin auftritt, wenden Sie sich an Ihren Händler.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Prüfgerätsperre</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Durch Sperren des Prüfgeräts können Sie es vor unberechtigtem Zugriff schützen. Das Display wird mit einem Bildschirmschoner gesichert.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Prüfgerät sperren</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Es wurde noch kein Kennwort festgelegt. Benutzer können das Prüfgerät ohne Kennworteingabe entsperren.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Kennworteinstellungen</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Diese Einstellungen werden auch von dem Bildschirmschoner verwendet.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Kennwort erforderlich</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Audio</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Lautsprecherlautstärke</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Stummschaltung</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Mikrofonpegel</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Satelliten suchen</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>Ausdehnung</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>Länge</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>Quote</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Zeitstempel</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Anzahl fixer Satelliten</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>Durchschnittlicher SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Individuelle Satelliteninformation</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Systeminfo</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Verbindung zum Server wird hergestellt...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Verbindung erstellt...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Downloading Dateien...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Dateien heraufladen...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Downloading Aktualisierungsinformation...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Protokolldatei heraufladen...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Erfolgreich mit dem Server synchronisiert.</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>In dem Aufbewahrungsbehälter. Wartet auf Aufnahme in den Bestand...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Konnte nicht mit dem Server synchronisieren.</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Verbindung während der Synchronisation verloren. Prüfen Sie die Netzwerkkonfigurierung.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Konnte nicht mit dem Server synchronisieren. Server ist beschäftigt.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Konnte nicht mit dem Server synchronisieren. Systemfehler erfasst.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Verbindung konnte nicht erstellt werden. Prüfen Sie die Netzwerkkonfigurierung oder Konto-ID.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Synchronisation abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Konfigurierungsdaten Komplett.</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Auswertungen komplett.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Konto-ID eingeben.</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Sync starten</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Sync anhalten</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Serveradresse</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>Konto ID</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Option</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Voreinstellungen wiederherstellen</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Video Wiedergabeprogramm</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Web Browser</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Debug</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Serielles Gerät</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Steuerung des seriellen Gerät über getty zulassen</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Inhalt von /root/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Screening-Prüfung einrichten</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Job Manager</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Jobinformation</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Jobnummer</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>Jobinformation wird automatisch an den Testbericht angehängt.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Historie</translation>
        </message>
    </context>
</TS>
