<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>本测试设备被用户锁定，防止非法访问。</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>无需密码，按确认解锁。</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>解锁测试设备?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>请输入密码解锁：</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>输入密码</translation>
        </message>
    </context>
</TS>
