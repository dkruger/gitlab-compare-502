<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CEvdevCalibrator</name>
        <message utf8="true">
            <source>Please wait...</source>
            <translation>Bitte warten ...</translation>
        </message>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>Zeitüberschreitung bei Kalibrierung</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTsCalibrateWindow</name>
        <message utf8="true">
            <source>Touch crosshair to calibrate the touchscreen.</source>
            <translation>Tippen Sie auf das Fadenkreuz, um das Touchdisplay zu kalibrieren.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTslibCalibrator</name>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>Zeitüberschreitung bei Kalibrierung</translation>
        </message>
    </context>
</TS>
