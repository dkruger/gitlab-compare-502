<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Zaman:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Oto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aç</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
