<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>현미경</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>파워미터</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>시스템</translation>
        </message>
    </context>
</TS>
