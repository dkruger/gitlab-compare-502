<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Просмотреть загрузки</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Увеличение</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Уменьшить</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>Сброс зума</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>Идет загрузка</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Открыть</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 мин осталось</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 сек осталось</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Загрузки</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>Сохранить как</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Директория :</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Название файла :</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
</TS>
