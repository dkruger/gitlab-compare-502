<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>{1} – байтовые кадры :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>{1} – байтовые кадры</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>{1} – байтовые пакеты :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>{1} – байтовые пакеты</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Ошибка : При попытке восстановления произошло превышение лимита времени {2}, проверьте соединение и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} – кадровый пакет : ошибка</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} – кадровый пакет : успешн .</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} из {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} – пакетный комплект : ошибка</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} – пакетный комплект : успешн .</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} Восстановление {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Нажмите OK, чтобы повторить попытку</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} Испытание идентифик . номера VLAN {2} для {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} мкс</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} мкс</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Идет ожидание ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       Вы можете изменить имя , чтобы создать новую конфигурацию .</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 наиболее активных пользователей ( из общего количества , составляющего {1} IP- соединений )</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 первых соединений , сопровождавшихся повторными передачами TCP ( из общего количества , составляющего {1} соединений )  </translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Прервать</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Прервать испытание</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Активн .</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Активная петля</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>Активный шлейф не успешен . Проверка прервана для VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Фактич . испытание</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Добавить диапазон</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Частота потери кадров , превышающая установленное значение порога потери кадров .</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>После выполнения испытаний вручную или в любой момент времени вы можете</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Аппаратная петля обнаружена</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>Аппаратная петля не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Все испытания</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Приложение проверки по шлейфу не является совместимым приложением</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Измерение максимальной производительности не предусмотрено</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>Активная петля не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Анализировать</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Идет анализ</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>и</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>и тест RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>Петля LBM/LBR не найдена .</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>Петля LBM/LBR найдена .</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Постоянная петля обнаружена</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Добавить журнал хода работ в конец отчета</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Имя приложения</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Приблиз . общее время :</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Диапазон теоретических значений производительности FTP рассчитывается исходя из фактических измеренных значений в канале связи .  Введите измеренные значения полосы пропускания канала связи , задержки подтверждения приема и инкапсуляции .</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Превышен лимит времени отклика .&#xA; Отклик на последнюю команду не был предоставлен &#xA; в течение {1} секунд .</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> Предполагается наличие аппаратной петли .        </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Асимметричный</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>В асимметричном режиме передача происходит из ближнего конца в дальний конец при восходящем режиме и из дальнего конца в ближний конец при нисходящем режиме .  В совмещенном режиме испытание выполняется дважды , сопровождаясь последовательной передачей в восходящем направлении с использованием локальной настройки , а затем в нисходящем направлении с использованием дистанционной настройки .  Используйте эту кнопку , чтобы переписать в дистанционную настройку текущую локальную настройку .</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Идет попытка</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Идет попытка установления восходящ . петли</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Идет попытка входа в систему на сервере ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>Сбой попыток зацикливания . Остановка теста</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>Неудачная попытка установлен . восходящ . петли</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Автосогласование</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Автосогласование выполнено</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Настройки автосогласования</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Состояние автосогласования</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Доступн .</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Среднее</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Средн . пакет</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Средняя скорость передачи пакета</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Средний размер пакета</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Средн .</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Результаты испытания средн . и максим . средн . джиттера пакета :</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Средняя задержка (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Средняя задержка (RTD): нет данных</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Средний джиттер пакета :</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Средний джиттер пакета : нет данных</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Средн . джиттер пакета ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>Средн . скорость</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Взаимн .</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Гранулярность кадров при испытании взаимного процесса :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Гранулярность кадров при испытании взаимного процесса</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Испытание взаимных кадров</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Результаты испытания взаимных кадров :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Максимальное время попытки испытания взаимного процесса :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Максимальное время попытки испытания взаимного процесса :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Результаты испытания взаимного процесса :</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>Назад в Обзор</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Гранулярность полосы пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Гранулярность полосы пропускания (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Погрешность измерения полосы пропускания :</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Погрешность измерения полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>Испытание основной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Начало диапазона :</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation> Биты</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Оба</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx и Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>IP- адреса локального и удаленного источников недоступны</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Снизу вверх</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Буфер</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Кредит буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Кредит буфера &#xA;требуется Производительность</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Кредиты буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Длительность попытки испытания кредитов буфера :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Длительность попытки испытания кредитов буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Испытание кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Результаты испытания кредита буфера :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Производительность кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Байты производительности кредита буфера {1}:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Производительн . кредита буфера &#xA;требуется Кредит буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Испытание производительности кредитов буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Результаты испытания производительности кредита буфера :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Размер буфера</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Гранулярность пакетов ( кадры )</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>Наблюдение за повторными передачами TCP при использовании сети связи в течение некоторого времени дает возможность установить связь между некачественной производительностью сети и состояниями сети , сопровождающимися потерями данных , такими как перегруженность .</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>В таблице Активность IP- адресов наиболее активные пользователи сети определяются по байтам или кадрам .    Списки S &lt;- D и S -> D относятся к направлениям трафика байтов и кадров от источника к пункту назначения и от пункта назначения к источнику .</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>( Байты)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation> байт .</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>( Байты )</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Байты &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Байты S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Расчетн .&#xA; длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Расчетн .&#xA; длина пакета</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Идет вычисление…</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>Невозможно продолжить работу !</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Сводка по анализу захваченного файла</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Длительность захвата</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Выполнить снимок &#xA; экрана</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>ВНИМАНИЕ !&#xA;&#xA; Вы хотите навсегда &#xA; удалить данную конфигурацию ?&#xA; {1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Настр . скорость (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Настр . скорость</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Скорость конфигурац . ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>ID шасси</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>Проверенные элементы приема (Rx) будут использованы для настройки установочных значений источника фильтра .</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>Проверенные элементы передачи (Tx) будут использованы для настройки установочных значений назначения передачи Tx.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Проверка активной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Проверка наличия аппаратной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Проверка активной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Проверка на наличие петли LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Проверка наличия постоянной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Проверка обнаружения полудуплексных портов</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Проверка наличия кадров ICMP,</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Проверка наличия возможных повторных передач или повышенного использования полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Проверка аппаратной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Проверка петли LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Проверка наличия постоянной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Проверка статистики иерархии протоколов</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Идет проверка наличия адреса источника ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>Установка флажка в данном поле приведет к восстановлению первоначальных значений настроек испытания при выходе из испытания . Для асимметричного испытания данные значения будут восстановлены как на локальной , так и на удаленной сторонах . Восстановление значений настроек приведет к установке канала связи в исходное состояние .</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Проверьте настройки порта между IP- адресом источника и устройством , к которому он подключен , убедитесь в том , что полудуплексный режим отключен .  Дальнейшее разбиение на участки может быть достигнуто путем перемещения анализатора ближе к IP- адресу назначения . Определите , наблюдается ли в этом случае исчезновение повторных передач и изолируйте неисправные каналы связи .</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Выбрать захваченный файл для анализа</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>Выберите &#xA; файл PCAP</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Выберите требуемую погрешность при измерении полосы пропускания &#xA;( рекомендуется 1%, чтобы обеспечить более короткое время испытания ).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Выберите тип входа в систему управления потоком</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Выбрать предпочтительный размер кадра или пакета</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Выбрать уровень детализации для выполнения теста ретрансляции .</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Выберите гранулярность , с которой Вы хотите выполнять испытания Потери кадров .</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Выбрать максимальную полосу пропускания для конфигурации цепи .  Устройство будет использовать это значение в качестве максимальной полосы пропускания при организации передачи , что приведет к уменьшению длительности испытания :</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Выберите максимальный размер кредита буфера .&#xA; Испытание начнется с использованием данного значения , что позволит сократить длительность испытания :&#xA; ПРИМЕЧАНИЕ :  Дистанционное устройство , обеспечивающее зацикливание трафика , должно быть установлено со &#xA; следующими параметрами :&#xA;&#xA;1.  Управление потоком ВКЛ &#xA;2.  {1}&#xA;3.  {2} Кредиты буфера должны быть установлены с тем же значением , которое указано выше .</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Выбрать максимальное время выполнения теста ретрансляции .</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Выберите минимальное и максимальное значения нагрузки , подлежащие использованию при проведении процедур испытаний 'Сверху вниз' и 'Снизу вверх' </translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Выбрать количество попыток выполнения испытания Взаимного процесса для каждого размера кадра .</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Выберите количество попыток выполнения испытания задержки (RTD) для каждого размера кадра .</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Выбрать количество попыток выполнения испытания Джиттера пакета для каждого размера кадра .</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Выберите разрешенный процент для допустимой потери кадров при испытании производительности .&#xA; ПРИМЕЧАНИЕ :  Значение > 0.00 НЕ СООТВЕТСТВУЕТ требованиям RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Выберите время длительности каждой попытки испытания задержки (RTD).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Выбрать продолжительность каждой проверки искажений пакета (Packet Jitter).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Выбрать продолжительность отправки частоты без ошибок для прохождения теста пропускной способности .</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Выбрать продолжительность каждой проверки потери кадров .</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Выберите время попытки проведения испытания Кредита буфера .</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Выберите процедуру , подлежащую использованию при испытании потери кадров . &#xA; ПРИМЕЧАНИЕ : Процедура RFC2544 выполняется при максимальной полосе пропускания и уменьшается на значение гранулярности полосы пропускания при каждой попытке . Данная процедура завершается , если в течение двух последовательных попыток не будет обнаружено потери кадров .</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Протокол обнаружения Cisco</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>Сообщения Протокола обнаружений устройств Cisco (CDP) были обнаружены в этой сети , в таблице представлены MAC- адреса и порты , в которых указаны настройки полудуплексного режима .</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Очистить все</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Нажмите кнопку Результаты , чтобы перейти к стандартному интерфейсу пользователя .</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Нарушения кода</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Совмещен .</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>Комментарии</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Комментарии</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Связь с дальним концом успешно установлена</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>Невозможно установить связь с дальним концом</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>Потеряна связь с дальним концом</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>завершить</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Завершен .</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>завершен .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Конфиг .</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Конфигурация</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>Имя конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Имя конфигурации :</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Имя конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>Требуется имя конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Конфигурация только для чтения</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Обзор конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Настроить отмеченные элементы</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Настроить длительность {1} отправки трафика.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Подтвердить замену конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Подтвердить удаление</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>ПОДКЛЮЧЕН</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Идет подключен .</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Подключиться к приложению измерения показателей испытаний !</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Продолжить в полудуплексном режиме</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Продолжение в полудуплексном режиме</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Копировать настройки локальной стороны &#xA; в нстройки удаленной стороны</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Копировать &#xA; выбран .</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>Невозможно установить восх . петлю на дальнем конце</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Создать отчет</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>кредиты</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>( Кредиты )</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA; Текущий сценарий : {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Текущий выбор</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>Абонент</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Абонент</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Скорость передачи данных в битах ,</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Скорость передачи данных в байтах</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Уровень данных остановлен</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Режим данных</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Режим данных установлен в PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Размер данных</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>Дата &amp; Время</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>дни</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Задержка , текущ . ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Задержка , текущ . ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Конфигурация назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>IP- адрес &#xA; назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>MAC- адрес назначения для IP- адреса {1} не обнаружен</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>Обнаружен MAC- адрес назначения .</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>MAC- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>Метка со сведениями</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Сведения</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>обнаружен .</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>Обнаружен .</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Обнаружено значение полосы пропускания канала связи</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Обнаружено больше кадров , чем передано для {1} полосы пропускания – недействительное испытание .</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Определение симметричного испытания производительности</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Сведения об устройстве</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ID устройства</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>Отсутствуют параметры DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>Обнаружены параметры DHCP.</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Обнаруженные устройства</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Идет обнаружение</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Обнаружение типа закольцевания на дальнем конце ...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Функция &#xA; обнаружения &#xA; недоступна &#xA; в даный момент</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Отобразить по : </translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Выходной ключевой поток .</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Нисходящее направление</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> Хотите продолжить ? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Дуплексн .</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Длительность</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Длит./ Действит.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Включен.</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Включить расширенный тест трафика 2 уровня</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Инкапсуляция :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Инкапсуляция</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Конец</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>Конец диапазона :</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Введите IP- адрес или имя сервера , с которыми вы хотите выполнить испытание FTP.</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Введите имя для входа в систему для сервера , к которому Вы хотите выполнить подключение</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Введите пароль учетной записи , которую вы хотите использовать</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Введите новое имя конфигурации &#xA;( Используйте только буквы , числа , пробелы , тире и знаки подчеркивания ):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Ошибка : {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ОШИБКА : Превышение лимита времени отклика &#xA; Отклик не произошел в течение</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Ошибка : Невозможно установить соединение</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Подсчет ошибок</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Кадры с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Ошибка при загрузке файла PCAP</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Ошибка : Ошибка разрешения имени основной службы DNS.</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Ошибка : Невозможно определить местоположение узла</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Расчетн . показатель оставшегося времени</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Расчетн . показатель оставшегося времени</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     Отчет об испытании Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>Журнал событий заполнен .</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Обнаружены излишние передачи</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>Выйти из операции J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Ожидаем . производительность</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>Ожидаем . производительность составляет</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>Показатели ожидаемой производительности недоступны</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>кнопку Испытание Expert RFC 2544 .</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Явный (E- порт )</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Явный (Fabric/N- порт )</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Не удалось завершить явный вход в систему . </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ОШИБКА</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>НЕУДАЧН .</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>Дальний конец представляет собой тестовый набор JDSU Смарт класса Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>Дальний конец предназначен для комплекта испытаний Smart Class Ethernet Viavi</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Испытание FC</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>Испытание FC проводится с использованием полезной нагрузки испытания Acterna</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Возможность FDX</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Отчет об испытании волоконно - оптического канала</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>Конфигурация файла</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Название файла</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Файлы</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>Размер файла</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>Размер файла :</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>Размер файла</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>Размер файла : {1} МБ</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>Размеры файлов :</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>Размеры файлов</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>Передача файла происходила очень быстро . Проверка прервана .</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Идет расчет ожидаемой производительности</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Обнаружение наиболее активных пользователей сети</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>50 первых полудуплексных портов ( из общего количества , составляющего {1})</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>50 первых сообщений ICMP ( из общего количества , составляющего {1})</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Управление потоком</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Тип входа в систему управления потоком</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Папки</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> для каждого кадра , сокращается в два раза для компенсации двойной длины волоконно - оптического канала .</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>обнаружен</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Найдена активная петля .</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Найдена замкнутая система аппаратного обеспечения .</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Длина &#xA; кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Длина кадра ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Длина кадра :</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Длина кадра для испытания</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Потеря кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Потеря кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Байты потери кадров {1}</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Байты потери кадров {1}</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Гранулярность полосы пропускания при испытании потери кадров :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Гранулярность полосы пропускания при испытании потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Максим . полоса пропускания при испытании потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Миним . полоса пропускания при испытании потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Частота потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Коэффициент потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Испытание потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Процедура испытания потери кадров :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Процедура испытания потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Результаты испытаний потери кадров :,</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Допустимая потеря кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Длительность попытки испытания потери кадров :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Длительность попытки испытания потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Кадр или пакет</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>кадры</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Кадры</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>размер кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Размер кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Размер кадра :  {1} байтов</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Кадры &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Кадры S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Формирование кадров</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>( кадры )</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>( кадры / сек )</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>Производительность FTP</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>Испытание производительности FTP &#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>Испытание производительности FTP завершено !</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>Отчет об испытании производительности FTP</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Полн .</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>Получить информацию о PCAP</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Половин .</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Полудуплексные порты</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Оборудование</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Аппаратная птеля</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>( аппаратн .&#xA; или активн .)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>( Аппаратн .,&#xA; постоян .&#xA; или активн .)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Возможность HDX</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Повышенное использование</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Домой</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>часы</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>Испытание производительности HTTP</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>Отчет об испытании производительности HTTP</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>Код &#xA; ICMP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>Сообщения ICMP</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Если счетчики ошибок увеличиваются случайным образом выполните</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Если проблема не может быть устранена , выполните Установку испытания в значения по умолчанию , воспользовавшись меню Сервис .</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Если вы не можете решить проблему со случайными ошибками вы можете установить</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Неявный ( прозрачный канал связи )</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>Информация</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Идет инициализация связи с</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Для определения пропускной способности , при которой</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>Скорость входящего потока для локальной и удаленной сторон не совпадает</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Периодические проблемы в линии .</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Внутренняя ошибка</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Внутренняя ошибка – перезапустить PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Недействительн . конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Недействительн . конфигурац . IP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>IP адрес</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>IP- соединения</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>произв . выход</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>запущен</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect .</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Испытание джиттера</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>Операция J-QuickCheck проверка завершена</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>Во время операции J-QuickCheck проверка был потерян канал связи или нельзя было установить канал связи</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>кбайт</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>L2 Тест трафика можно запустить повторно , если выполнить J-QuickCheck еще раз .</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Задержка (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>Испытания задержки (RTD) и джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Средняя задержка (RTD): нет данных</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Порог успешного прохождения испытания задержки (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Порог успешного прохождения испытания задержки (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Задержка (RTD)&#xA;требуется Производительность</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Результаты испытаний задержки (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Испытание задержки (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Результаты испытаний задержки (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Результаты испытаний задержки (RTD): ИСПЫТАНИЕ ПРЕРВАНО   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Результаты испытаний задержки (RTD): НЕУДАЧН .</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Результаты испытаний задержки (RTD): УСПЕШН .</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Результаты испытаний задержки (RTD) опущены :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Испытание задержки (RTD) пропущено</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> Порог успешного прохождения испытания задержки (RTD): {1} мкс</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Порог успешного прохождения испытания задержки (RTD) ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Длительность попытки испытания задержки (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Длительность попытки испытания задержки (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Задержка (RTD) ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Уровень 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Состояние Ethernet&#xA; уровня 1 / 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Уровень 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Обнаружено наличие канала связи уровня 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Быстрый тест 2 уровня</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Уровень 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>Уровень 3&#xA; состояние IP</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Уровень 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>Состояние TCP&#xA; уровня 4</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Петля LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Длина</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Обнаружен канал связи</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Протокол обнаружения канального уровня</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Потеря канала связи</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Скорость канала связи обнаружена в захваченном файле</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Порт прослушивания</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Формат загрузки</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>ИДЕТ ЗАГРУЗКА ... Подождите</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Локальн .</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>IP- адрес локального назначения установлен в </translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>MAC- адрес локального назначения установлен в </translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>Порт локального назначения установлен в</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>Тип локальной петли установлен в значение Однонаправленная передача</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Локальн . порт</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>Локальный / удаленный IP- адрес установлен в</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> Серийный номер локальной стороны</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Настройка локальной стороны</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> Версия программного обеспечения локальной стороны</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>Фильтр IP- адреса локального источника установлен в</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>Фильтр MAC- адреса локального источника установлен в</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>Фильтр порта локального источника установлен в</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Обзор настройки локальной стороны</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> Название средства испытания локальной стороны</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Определите устройство , MAC- адреса и порты источника которого представлены в таблице и убедитесь в том , что параметры дуплексного режима для него имеют значение полнодуплексный , а не авто .  Зачастую хост - устройство и сетевое устройство могут иметь установленное значение авто , в связи с чем канал связи некорректно согласуется с полудуплексным режимом .</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>Местоположен .</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Местоположен .</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Вход в систему :</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Вход в систему</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Имя для входа в систему</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Имя для входа в систему</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Ошибка петли</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Установлен . нисх . петли с устройством на дальнем конце ...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Установлен . восх . петли с устройством на дальнем конце ...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Неизвестн . состояние петли</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Неудачн . установка восходящ . петли</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Восходящ . петля установлена успешно</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Успешная установка восх . петли</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>Обнаружена потеря канала связи уровня 2!</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Потерян .</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Потерянные кадры</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Адрес упарвления</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>Тип MAU</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Макс .</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( макс . {1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Макс . средн .</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Максимальный средний джиттер пакета :</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Максимальный средний джиттер пакета : нет данных</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Макс . средн . джиттер пакета ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Максим . полоса пропуск . (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Максим . полоса пропуск . (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Макс размер буфера</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>Максимальная задержка ( среднее значение ), при которой испытание задержки (RTD) будет считаться успешно выполненным</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Максимальный джиттер пакета ( среднее значение ), при котором испытание Джиттера пакета будет считаться успешно выполненным</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Максимальное количество кредитов буфера при приеме</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Максим . полоса пропускания для испытаний :</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Максим . полоса пропускания для испытаний</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Измерен . макс . производительность :</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Максимальный лимит времени для VLAN ID - {1} </translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Максимальный лимит времени для VLAN ID - 7 дней</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Максим . время попытки ( секунды )</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Максимальное количество кредитов буфера при передаче</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>Макс . скорость</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Достигнуто максимальное количество попыток переприема . Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Измерен .</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Измеренная скорость (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Измеренная скорость</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Измеренная скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Точность измерения</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Измерение производительности при {1} кредитах буфера</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Сообщение</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин .</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Миним .</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Минимальный процент полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>Минимальный процент полосы пропускания , необходимый для успешного прохождения испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Минимальный лимит времени для VLAN ID - 5 секунд</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Мин . скорость</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>минуты</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>минута ( минуты )</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>минуты</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Модель</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Изменить</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>Инкапсуляция MPLS/VPLS не поддерживается в текущий момент ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>Нет данных ( аппаратная петля )</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Ни одно</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Вкл . сети</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Использование сети</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>Использование сети оказалось не чрезмерным , при этом в данном графике представлена диаграмма использования сети</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>Использование сети оказалось не чрезмерным , при этом в данной таблице представлен перечень наиболее активных пользователей IP- адресов </translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Создать</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Новое имя конфигурации</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Новый URL- адрес</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Следующ .</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Нет.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>Совместимых приложений не обнаружено</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt; КОНФИГУРАЦИИ ОТСУТСТВУЮТ ></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>Не выбраны файлы для испытаний</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>Аппаратная петля не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>Устройства&#xA;JDSU&#xA;не&#xA;обнаружены</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>Канал связи уровня 2 не обнаружен !</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>Невозможно установить петлю</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>Невозможно установить или обнаружить петлю</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>Постоянная петля не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>Не обнаружено функционирующих приложений</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>НЕ СООТВЕТСТВУЕТ RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Отсутствует соединение</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Не определен .</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>ПРИМЕЧАНИЕ :  Установочное значение > 0,00</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Примечание : Предполагает наличие аппаратной петли с кредитами буфера менее 2.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA; Примечание : предполагает замкнутую цепь с буферными кредитами менее 2.&#xA; Этот тест является недействительным &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Примечание : Исходя из предположения о наличии аппаратной петли , количество минимальных кредитов буфера , рассчитанных</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Примечание : Исходя из предположения о наличии аппаратной петли , измерения производительности выполняются с использованием кредитов буфера , количество которых в два раза больше </translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Примечание : Если используется значение допустимой потери кадров , испытание не будет соответствовать .</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Примечания</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Не выбран .</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Устройства &#xA;Viavi&#xA; не &#xA; обнаружены</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>Идет выход ...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>Идет проверка</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Идет проверка с {1} кредитами .  Этой займет {2} секунд .</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Количество попыток испытания взаимного процесса :</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Количество попыток испытания взаимного процесса</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Количество неудачных испытаний</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Количество испытанных идентификационных номеров</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Количество попыток испытания задержки (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Количество попыток испытания задержки (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Количество попыток испытания джиттера пакета :</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Количество попыток испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Количество пакетов</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Количество успешных испытаний</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Количество попыток :</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Количество попыток</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>для</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Выкл .</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>ВЫКЛ</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>кадров было потеряно в течение одной секунды .</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>Ожидаемая пропускная способность J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(% скорости передачи в линии )</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% скорости передачи в линии</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>скорости передачи в линии</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Вкл</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ВКЛ</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * Только {1} попытка ( попытки ) выдала подходящие данные *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>( ВКЛ или ВЫКЛ )</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Кадры OoS</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Идентифкационный код исходящей стороны</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Вне диапазона</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        Общий результат испытания : {1}        </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    Общий результат испытания : ИСПЫТАНИЕ ПРЕРВАНО   </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Превышение диапазона :</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>в течение 10 последних секунд несмотря на то , что трафик должен быть остановлен</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Джиттер пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Джиттер пакета , средн .</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Порог успешного прохождения испытания джиттера пакета :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Порог успешного прохождения испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Джиттер пакета &#xA;требуется Производительность</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Результаты испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Испытание джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Результаты испытания джиттера пакета : ИСПЫТАНИЕ ПРЕРВАНО   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Результаты испытания джиттера пакета : НЕУДАЧН .</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Результаты испытания джиттера пакета : УСПЕШН .</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> Порог джиттера пакета : {1} мкс</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Порог джиттера пакета ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Длительность попытки испытания джиттера пакета :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Длительность попытки испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Длина &#xA; пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Длина пакета ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Длина пакета :</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Длина пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Длина пакета для испытания</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>размер пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Размер пакета</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ПРОЙДЕНО</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Прошел / Ошибка</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>УСПЕШН . / НЕУДАЧН .</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Скорость прохождения % )</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Скорость прохождения ( кадры / сек )</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Скорость прохождения ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Скорость прохождения ( пакеты / сек )</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Пароль :</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Пароль</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Пауза</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Объявлен . паузы</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Возможн . паузы</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Обнаруж . пауз .</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Обнаружены кадры с паузой</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Обнаружены кадры с паузой – недействительное испытание</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Ошибка анализа файла PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>Файлы PCAP</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>Отложенная операция</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Выполнение очистки</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Постоян .</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Постоян . петля</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>( Постоян .&#xA; или активн .)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Джиттер пакета ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Длина пакета</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Потеряно пакетов</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>( пакеты )</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>( пакеты / сек )</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Платформа</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Проверьте наличие синхронизации и канала связи ,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Проверьте , надлежащим ли образом организовано ваше соединение ,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Проверьте , надлежащим ли образом организовано ваше соединение ,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Выберите другое имя конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Введите имя файла для сохранения отчета ( макс . %{1} символов ) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Введите свои комментарии ( максимум {1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Введите свои комментарии ( максимум %{1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите свои замечания (макс {1} символов)&#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Введите имя абонента ( максимум %{1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Пожалуйста , введите свое имя заказчика ( макс . %{1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите имя клиента&#xA;(макс {1} символов) &#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите имя клиента (макс {1} символов)&#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите имя клиента(макс {1} символов)&#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Введите имя специалиста ( максимум {1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Введите имя специалиста ( максимум %{1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите имя специалиста (макс {1} символов)&#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Введите местоположение испытания ( максимум {1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Введите местоположение испытания ( максимум %{1} знаков )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Введите место проведения теста (макс {1} символов)&#xA;(можно использовать только буквы, цифры, пробелы, пунктир и знаки подчеркивания)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Нажмите кнопку Выполнить соединение с удаленной стороной</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Проверьте работу канала связи , воспользовавшись испытанием трафика вручную ..</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Проверьте IP- адреса локального и удаленного источников и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Проверьте IP- адрес локального источника и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>Проверьте свой удаленный IP- адрес и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Проверьте IP- адрес удаленного источника и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Подождите ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Подождите…</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Дождитесь завершения записи файла PDF.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Дождитесь окончания записи файла PDF.&#xA; Это может занять до 90 секунд ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Порт :</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Порт {1}: Хотите сохранить отчет об испытании ?&#xA;&#xA; Нажмите Да или Нет .</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>ID порта</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Порт :				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Порт :				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>Активн . PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Ошибка проверки подлинности PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Неизвестн . ошибка PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>Ошибка PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Ошибка PPP LCP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>Активн . PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Отказ PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>Неактивн . PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>Выполнен запуск PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>Состояние PPPoE: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Превышение лимита времени PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>Запуск PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>Ошибка PPPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Превышение лимита времени PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>Неизвестный отказ PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>Отказ запуска PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Ошибка PPP UP</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>Нажмите Закрыть , чтобы вернуться в главное окно .</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>Нажмите кнопку Выход , чтобы вернуться в главное окно или кнопку Выполнить испытание , чтобы повторить испытание .</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Нажмите &#xA; кнопку &#xA; Обновить &#xA;, чтобы &#xA; начать операцию обнаружения</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Нажмите кнопку Выйти из операции J-QuickCheck проверка для выхода из операции J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Нажмите кнопку Обновить ниже , чтобы обнаружить устройства Viavi, находящиеся в данный момент в подсети . Выберите устройство для ознакомления со сведениями о нем в правой части таблицы . Если кнопка Обновить недоступна , проверьте , включена ли функция обнаружения и имеются ли в наличии синхронизация и канал связи . </translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Нажмите кнопку Выполнить операцию J-QuickCheck проверка &#xA;, чтобы проверить установки испытания локальной и удаленной сторон и доступную полосу пропускания</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Предыдущ .</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>Ход выполнения</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Свойство</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Рекомендуемые следующие действия</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Поставщик</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Выйти</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Произвольн .</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> Диапазон</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Диапазон</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Скорость</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>скорости , это значит , что в канале связи могут быть общие проблемы , не относящиеся к максимальной нагрузке .</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>Принято {1} байтов из {2}</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Принятые кадры</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Рекомендация</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Рекомендуемая конфигурация испытания , выполняемого вручную :</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Обновить</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Дистанцион .</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Дистанционный IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Петля удален . сторон .</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation>Серийный номер удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>Серийный номер удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Настройка удаленной стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Невозможно восстановить дистанционные настройки</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> Версия программного обеспечения удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Версия программного обеспечения удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Обзор настройки удаленной стороны</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>Название средства испытания удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Название средства испытания удален . стороны</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Удалить диапазон</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Идентифкационный код отвечающей стороны</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>IP- адрес &#xA; отвечающего маршрутизатора</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>Перезапустить операцию J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Восстановить заданную до испытаний конфигурацию перед выходом</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Идет восстановление дистанционных настроек испытания ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>Ограничить RFC до</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>Результаты испытания основной нагрузки недоступны , нажмите кнопку Рекомендуемые следующие действия для ознакомления с возможными решениями данной проблемы </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Результаты , подлежащие контролю :</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Повторные передачи</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Обнаружены повторные передачи &#xA; Идет анализ случаев повторной передачи с течением времени</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>обнаружены повторные передачи . Экспортируйте файл в USB для дальнейшего анализа .</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>Восстановление страницы {1} было прервано пользователем</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>вернуться в интерфейс пользователя RFC 2544, нажав для этого </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>Отчет об испытании Ethernet RFC 2544</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>Режим RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>Режим RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Стандартн . RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>Испытание RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>Испытание RFC 2544 проводится с использованием полезной нагрузки испытания Acterna</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>Испытание RFC/FC не может быть выполнено , пока идет выполнение приложения Графические результаты мультипотоков</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Режим Rfc</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>Обнаружен . R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Выполнить</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>Выполнить испытание FC</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Выполнить операцию J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>Запустить $l2quick::testLongName</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Идет выполнение испытания при нагрузке {1}{2}.</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Идет выполнение испытания при нагрузке {1}{2}.  Этой займет {3} секунд .</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>Выполнить испытание RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Запустить сценарий</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Rx Мбит/с , текущ. L1</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Только Rx</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>Сохранить отчет об испытании производительности FTP</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>Сохранить отчет об испытании производительности HTTP</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>Сохранить отчет об испытании сканирования сети VLAN</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> масштабируемая пропускная способность </translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Снимок экрана выполнен</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Сценарий прерван .</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>секунды</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>( сек )</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>сек</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Выберите имя для копируемой конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Выберите имя новой конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Выбрать диапазон идентификационных номеров сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Выбранное направление Tx:</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Выбрано нисходящее &#xA; направление Tx</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Выбрано восходящее &#xA; направление Tx</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Предупреждение , касающееся выбора конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Нажмите OK, чтобы изменить конфигурацию &#xA; Измените имя , чтобы создать новую конфигурацию &#xA;( Используйте только буквы , числа , пробелы , тире и знаки подчеркивания )</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Выбрать конфигурацию испытания :</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Выберите свойство , которое будет использовано в качестве критерия при представлении обнаруженных устройств .</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Выберите испытания , которые вы хотите выполнить :</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>Выбрать URL- адрес</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Выбрать используемый формат для загрузки связанных установок .</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Передача запроса ARP для MAC- адр . назначения .</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Отправка трафика для</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>отправляет трафик , ожидаемая пропускная способность , обнаруженная J-QuickCheck, будет масштабируема этим значением .</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Идентификационный код последовательности</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID:- адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>ID- адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Имя службы</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Показать состояние Успешн ./ Неудачн .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA; Показать состояние Успешн ./ Неудачн . для :</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Размер</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Пропустить</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Пропуск испытания Задержки (RTD) и продолжение работы</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Номер редакции программного обеспечения</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> Номер редакции программного обеспечения</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>Отсутствует адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Установлено наличие источника ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>IP- адрес &#xA; источника</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>IP- адрес источника совпадает с адресом назначения</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC источника</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>MAC- адрес &#xA; источника</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Указать полосу пропускания канала связи</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Скорость</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>НАЧАЛО</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Запуск испытания основной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Идет запуск попытки испытания</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Неизвестн . состоян .</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>ОКОНЧАНИЕ</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Изучите графическую информацию , чтобы узнать , соотносятся ли повторные передачи TCP с нарушениями использования сети .   Ознакомьтесь с вкладкой Повторные передачи TCP, чтобы определить IP- адрес источника , который вызывает значительное количество повторных передач TCP. Проверьте настройки порта между IP- адресом источника и устройством , к которому он подключен , убедитесь в том , что полудуплексный режим отключен .  Дальнейшее разбиение на участки может быть достигнуто путем перемещения анализатора ближе к IP- адресу назначения . Определите , наблюдается ли в этом случае исчезновение повторных передач и изолируйте неисправные каналы связи .,</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>Испытание завершилось успешно !</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Успешн .</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Сводка измерен . значений :</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Сводка по странице {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Приоритет пользователя сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Симметрия</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>В симметричном режиме передача и прием происходят на ближнем конце с использованием проверки по шлейфу . В асимметричном режиме передача происходит из ближнего конца в дальний конец при восходящем режиме и из дальнего конца в ближний конец при нисходящем режиме . </translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Симметричн .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Нажмите на имени конфигурации , чтобы выбрать данную конфигурацию </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	 Получить файл {1} МБ ....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	 Размещен . файл . {1} МБ ....</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Скорость : {1} Кбит / с &#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Скорость : {1} Мбит / с &#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	 Кадры приема {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              ВНИМАНИЕ !&#xA;&#xA;	    Вы хотите навсегда &#xA;	           удалить данную конфгурацию ?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Используйте только буквы , числа , пробелы ,&#xA;		   тире и знаки подчеркивания !</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Данная конфигурация предназначена только для чтения &#xA;		   и не может быть удалена .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Имя новой конфигурации &#xA;		           следует вводить с помощью клавиатуры .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   Указанная конфигурация уже существует .</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Время : {1} секунд .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	 Кадры передачи {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>TCP хосту не удалось установить соединение . Проверка прервана .</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>TCP хост столкнулся с ошибкой . Проверка прервана .</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>Повторные передачи TCP</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>Специалист</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Специалист</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Имя специалиста</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Завершение</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>                       Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Проверка прервана пользователем .</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Тест при установленной максимальной ширине полосы в Setup - Закладка All Tests ( Все тесты )</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>испытание вручную с другой более низкой скоростью трафика . Если ошибки происходят даже при более низкой </translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Конфигурация испытания :</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Конфигурация испытания</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Длительность теста</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Длительность испытания : Как минимум в три раза больше установленной в настройках длительности испытаний .</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Проверенная пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Выполнение испытания завершено ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Испытание {1} кредитов </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Испытание {1} кредитов </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Тестирование при </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Идет испытание соединения ... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> Название средства испытания</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Идет запуск испытания</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Журнал испытания :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Имя испытания :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Имя испытания :			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Имя испытания :			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Процедура испытания</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Журнал хода выполнения испытаний</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Диапазон испытаний (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Диапазон испытаний (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Результаты испытаний</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Установка настроек испытания</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Испытания , подлежащие выполнению :</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Испытания , подлежащие выполнению</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>Ошибка активной восх . петли , аппаратная петля не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>Ошибка активной восх . петли , аппаратная или постоянная петля не обнаружены</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>Средняя скорость для {1} составляла {2} Кбит / с</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>Средняя скорость для {1} составляла {2} Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>Файл превышает ограничение в 50000 пакетов для JMentor</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>порог допустимой потери кадров , чтобы разрешить небольшую частоту потери кадров . </translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Протокол управляющих сообщений сети Интернет (ICMP) наиболее широко известен в связи с использованием команды Ping ( отправка Интернет - пакетов ) в рамках данного протокола . Сообщение Не достигнут пункт назначение ICMP означает , что пункт назначения не может быть достигнут маршрутизатором или сетевым устройством .</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>Количество кадров Acterna Rx фильтра L2 продолжало увеличиваться</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>Петля LBM/LBR дала сбой .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>IP- адреса локального и удаленного источников идентичны</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> Установочные параметры локальной настройки успешно скопированы для дистанционной настройки</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>IP- адрес локального источника недоступен</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Измеренный MTU слишком мал для продолжения . Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>Выбранное вами имя уже используется .</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>Порт элемента сети , к которому мы подключены , предоставляется в полудуплексном режиме . Если это правильно , нажмите кнопку Продолжить в полудуплексном режиме . Иначе , нажмите кнопку Выйти из операции J-QuickCheck проверка и выполните повторное предоставление порта .</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>В графике использования сети представлена полоса пропускания , потребляемая всеми пакетами в захваченном файле в период выполнения захвата .  Если также были обнаружены повторные передачи TCP, рекомендуется ознакомиться с результатами уровня TCP, вернувшись для этого в главное окно анализа .</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>на каждой стадии , что компенсирует двойную длину волоконно - оптического канала .</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Теоретич . расчет</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Теоретич . &amp; измерен . значения :</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>Функция автосогласования AutoNeg партнерского порта ( элемента сети ) отключена , показатели ожидаемой производительности недоступны , таким образом , партнерский порт вероятно функционирует в полудуплексном режиме . Если функционирование партнерского порта в полудуплексном режиме нежелательно , измените настройки партнерского порта для функционирования в полнодуплексном режиме и повторно выполните операцию J-QuickCheck проверка . После этого , если измеренные показатели ожидаемой производительности будут более приемлемыми , можно выполнить испытание RFC 2544.  Если показатели ожидаемой производительности по - прежнему будут недоступны , проверьте конфигурацию порта на удаленной стороне . Возможно , проблема заключается в несовпадении режимов портов от HD до FD.&#xA;&#xA; Если использование полудуплексного режима в партнерском порту допустимо , воспользуйтесь пунктами меню Результаты -> Настройка -> Интерфейс -> Физический уровень и измените параметр Дуплексный режим со значения Полнодуплексный в значение Полудуплексный .   Затем , вернитесь в сценарий RFC2544 ( Результаты -> Испытание Expert RFC2544), выйдите из операции J-QuickCheck проверка , перейдите к меню Испытание производительности , выберите пункт Стандартное обнуление процесса RFC 2544 ( полудуплексный режим ) и выполните испытание RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>Проблема связи с дальним концом .</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>удаленное устройство , с которым установлена петля , не отвечает на команду проверки по шлейфу Viavi, но возвращает переданные кадры обратно в локальное устройство , при этом поля адреса источника и адреса назначения в этих кадрах переставлены местами</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>удаленное устройство , с которым установлена петля , отвечает на команду проверки по шлейфу Viavi и возвращает переданные кадры обратно в локальное устройство , при этом поля адреса источника и адреса назначения в этих кадрах переставлены местами</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>удаленное устройство , с которым установлена петля , возвращает переданные кадры в локальное устройство без изменений</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>удаленное устройство цепи поддерживает OAM LBM и отвечает на полученный кадр LBM передачей соответствующего кадра LBR обратно в локальное устройство .</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>Удаленная сторона установлена для инкапсуляции MPLS</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>На удаленной стороне выполняется приложение проверки по шлейфу</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>IP- адрес удаленного источника недоступен</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; Отчет сохранен под именем {1}{2} в формате PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>Отчет сохранен под именем {1}{2} в формате PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>Отчет сохранен под именем {1}{2} в форматах PDF, TXT и LOG </translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>Отчет сохранен под именем {1}{2} в форматах TXT и LOG </translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>IP- адрес отвечающего маршрутизатора не может отправить пакет в направлении IP- адреса пункта назначения , в связи с чем операция обнаружения и устранения неисправностей должна быть проведена между IP- адресом отвечающего маршрутизатора и пунктом назначения . </translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>Испытание RFC 2544 не поддерживает инкапсуляцию MPLS.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>Лазер передачи включен &#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Лазер передачи выключен !&#xA; Хотите включить лазер ?&#xA; Нажмите кнопку Да , чтобы включить лазер или кнопку Нет , чтобы прервать операцию .</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Лазер передачи выключен !&#xA; Хотите включить лазер ?&#xA; Нажмите кнопку Да , чтобы включить лазер или кнопку Нет , чтобы прервать операцию .</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>Испытание сканирования сети VLAN не может быть завершено при включенной функции OAM CCM.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>Испытание сканирования сети VLAN не может быть настроено надлежащим образом .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Данная конфигурация предназначена только для чтения и не может быть изменена .</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Этот адрес должен представлять собой IP- адрес на дальнем конце при использовании асимметричного режима</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>В данной таблице представлены IP- адреса источников , которые подвергаются повторным передачам TCP. Обнаруженная повторная передача TCP может производиться в связи с потерей нисходящего пакета ( направленного к стороне пункта назначения ).  Она также может означать наличие проблемы , связанной с полудуплексным режимом порта .</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>Данное испытание проводится с использованием полезной нагрузки испытания Acterna</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Данное испытание недействительно .</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Для проведения данного испытания требуется , чтобы трафик имел инкапсуляцию VLAN. Убедитесь в том , что подключенная сеть предоставит IP- адрес для данной конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Этой займет {1} секунд .</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Производительность (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Производительность&#xA; ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Испытания производительности и задержки (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Испытания производительности и джиттера пакета .</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Допустимая потеря кадров при испытании производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Допустимая потеря кадров при испытании производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Испытания производительности , задержки (RTD) и джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Порог успешного прохождения испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Порог успешного прохождения испытания производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Масштабирование пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Коэффициент масштабирования пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Испытание производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>Длительность испытания производительности составляла {1} секунд .</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Результаты испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Результаты испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Результаты испытания производительности : ИСПЫТАНИЕ ПРЕРВАНО   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Результаты испытания производительости : НЕУДАЧН .,</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Результаты испытания производительости : УСПЕШН .,</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation> Порог производительности (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> Порог производительности : {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Порог производительности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Длительность попытки испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Длительность попытки испытания производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Процесс обнуления при испытании производительности :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Процесс обнуления при испытании производительности</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>Окончание отсчета времени</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Окончание отсчета времени</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Время испытания одного идентификационного номера :</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Время ( секунды )</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>Время &#xA;( сек .)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>Начало отсчета времени</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Начало отсчета времени</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>Количество посещений</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Чтобы продолжить работу , проверьте подключение кабеля , а затем повторно выполните операцию J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Чтобы определить максимальную производительность выберите стандартный метод RFC 2544, который соответствует количеству кадров передачи и приема или расширенный метод Viavi, который использует измеряемый параметр L2 Avg % Util. </translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Сверху вниз</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Для экономии времени испытание задержки (RTD) в асимметричном режиме следует выполнять только в одном направлении</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>чтобы узнать характер событий потери кадров – случайный или постоянный .</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Всего байтов</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Всего &#xA; кадров</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Общее количество</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Всего использ . {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>Всего использ . ( кбит / с ):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>Всего использ . Мбит/с ):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>Для ознакомления с отчетом выберите пункт Ознакомиться с отчетом в меню Отчет после выхода из {1}.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>до</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Трафик : Постоянное значение с {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Результаты трафика</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>Трафик по - прежнему генерировался из удаленного конца</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>Лазер передачи выключен !</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Переданные кадры</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Передача в нисходящ . направлении</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Передача в восходящ . направлении</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Попытка</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Попытка {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Попытка {1} завершена &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Плпытка {1} из {2}:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Длительность попытки ( секунды )</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>попытки</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Выполняется повторная попытка</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>ошибка tshark</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>Буфер TX – кредиты буфера</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Направление Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Лазер передачи выключен</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Tx Мбит/с текущ. L1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Только Tx</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> Невозможно автоматически установить восх . петлю на дальнем конце . </translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>Невозможно подключиться к приложению измерения показателей испытаний !</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>Невозможно подключиться к приложению измерения показателей испытаний !&#xA; Нажмите Да , чтобы повторить попытку , Нет , чтобы прервать выполнение операции .</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>Невозможно получить адрес DHCP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>Невозможно запустить испытание RFC2544 при включенной локальной проверке по шлейфу .</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>Невозможно выполнить испытание</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Невозможно запустить испытание сканирования сети VLAN при включенной локальной проверке по шлейфу .</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Недоступн .</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>НЕДОСТУПН .</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Идентифик . номер устройства</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>UP</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>( Вверх или вниз )</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Обратный поток</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Восходящее направление</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>( мкс )</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>Испытание прервано пользователем</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>Испытание отменено пользователем</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Имя пользователя</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Приоритет пользователя</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Неактивн . запрашиваемый пользователь</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Выбран . пользователем &#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Выбран . пользователем      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Используйте окно Состояние обзора для поиска событий с ошибками .</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Использ .</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Использование размера кадра</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Использование и повторные передачи TCP</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Значение</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>Значения , выделенные синим цветом , взяты из фактических испытаний .</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Проверка активности данного канала связи ...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>проверьте свой удаленный IP- адрес и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>проверьте свой удаленный IP- адрес и повторите попытку</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Расширен . Viavi</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Диапазоны идентификационных номеров сети VLAN, подлежащие испытанию</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Испытание сканирования сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>Отчет об испытании сканирования сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>Результаты испытания сканирования сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>Отчет об испытании VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>Обнаружен кадр VTP/DTP/PAgP/UDLD!</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>Ожидание выполнения автосогласования ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>Ожидание MAC- адреса назначения для &#xA;  IP- адреса</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>Ожидание параметров DHCP ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>Ожидание наличия канала связи уровня 2 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>Ожидание канала связи</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>Ожидание включения OWD, синхронизации ToD и синхронизации 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Ожидание успешного соединен . ARP ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>обнаружена в ходе последней секунды .</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Размер веб - сайта</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Активная петля доступна</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>Произошла ошибка !!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>При испытании полудуплексных каналов связи выберите стандарт RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Размер / емкость окна</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>рекомендации RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Хотите сохранить отчет об испытании ?&#xA;&#xA; Нажмите Да или Нет .</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>Вы также можете использовать график Графические результаты текущей частоты потери кадров .</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>Выполнение этого сценария из {1} невозможно .</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Вам может понадобиться подождать некоторое время до окончания данного процесса , чтобы повторить соединение</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Обнуление при максимальной производительности</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Обнуление в оптимальном буфере кредитов</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Процесс обнуления</translation>
        </message>
    </context>
</TS>
