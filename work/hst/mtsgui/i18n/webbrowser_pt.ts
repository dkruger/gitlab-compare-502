<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Vista Downloads</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Zoom +</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Afasta a imagem</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>Restabelecer zoom</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>Descarregando</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aberto</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 mins restante</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 segs restante</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Descargas</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Limpar</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>Gravar como</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Diretório:</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nome do arquivo:</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
</TS>
