<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscópio</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testes</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>Medidor de Potência</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>Sistema</translation>
        </message>
    </context>
</TS>
