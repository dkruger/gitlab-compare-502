<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>Rapor</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>Teknisyen ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Bağlan</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Ağ</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Subnet Prefix Uzunluğu</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Bağlantıyı kes</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Uzaktan Erişim</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statik</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>manuel</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Etkin</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Devre dışı</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Bağlandı</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testler</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>   Isim   </translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Isim</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tümü</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation type="unfinished"/>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation type="unfinished"/>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>  Isim   </translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Tek</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Üretici</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfigürasyon</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Oto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Başarısız</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Hindistan</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Uzaktan Erişim:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation type="unfinished"/>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation type="unfinished"/>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
