<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WhatsThis</name>
        <message utf8="true">
            <source>History column illuminates indicating an error, alarm or event has occurred.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows control of the transmit Laser.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows control of Ethernet traffic and loopback features.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows transmission of various alarm signals.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows transmission of various error conditions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows configuration of the most commonly-used settings.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar allows configuration of the Ethernet payload.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LED Panel displays current status (round LED) and history (square LED) of each monitored condition.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This toolbar controls discoverability of this test instrument with other Viavi test instruments which support J-Connect.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turns transmit laser on or off.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turns Ethernet traffic on or off.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Commands remote test instrument to begin looping back traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Commands remote test instrument to stop looping back traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Turns Local Loopback of received traffic on or off.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmits a pause frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of bit or Test Sequence Error (TSE) errors that occurred after initial pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ratio of bit or Test Sequence Error (TSE) errors to received pattern data bits since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times synchronization is lost after initial pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which pattern synchronization was lost after initial pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which one or more pattern slips occurred after initial pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The round trip delay for the last delay pattern sent and successfully received by the MSAM. Calculated in milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maintenance domain level configured for the CCM frame received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON indicates that a loss of continuity has occurred.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maintenance association ID configured for the CCM frame received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON indicates that CCM frames have been received with the same maintenance domain level specified for transmitted frames, but the received CCM frames carry a different maintenance association ID (MAID).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maintenance entity group end point ID for the instrument's peer as configured.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether or not remote defect indication is ON or OFF.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which an RDI was declared since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of CCM frames received since the last OAM setting was specified or changed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON indicates that CCM frames have been received with a maintenance entity group level lower than that specified as the maintenance domain level when you configured the OAM settings for the transmitting instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON indicates that a CCM was received from a different maintenance end point than that specified as the instrument's peer MEG End Point.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON indicates that a CCM was received with the correct maintenance domain level, maintenance association ID, and maintenance end point ID, but with a period value that was not the same as the instrument's CCM rate.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of CCM frames transmitted since the last OAM setting was specified or changed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum received frequency deviation, expressed in ppm.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the destination IP address as defined for the currently selected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value of the signal label (V5) byte, indicating the type of data in the VT.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the PID type (Audio, Video, PMT, or PAT).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which AIS was present for any portion of the test second.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of CRC errors detected since initial frame synchronization. CRC errors are counted only when ESF framing is present in the received data.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of CRC errors to the number of extended superframes received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more CRC errors occurred.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of frame errors detected since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of frame errors to received framing bits since initially acquiring frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more frame errors occurred since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of frame synchronization losses since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more frame synchronization losses occurred or during which frame synchronization could not be achieved, since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times LOF was present since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of LOFs since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds frame synchronization was not present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the received payload type after multiframe synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more RAI errors occurred since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds in which the Path trace identifier (J2) is different than the expected value since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds a Loss of Multiframe alignment occurred.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds that VT LOP was present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds in which payload mismatch VT errors occurred since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of negative and positive VT pointer adjustments on the selected receive channel since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times the pointer bytes indicated a decrement to the VT payload pointer since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times the pointer bytes indicated an increment to the VT payload pointer since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The binary setting of the size bits in the H1 byte. The normal setting for the pointer size bits is 00 to indicate a SONET payload, or 10 to indicate a SDH payload.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current VT pointer value from 0 to 103 or 139. UNAVAILABLE appears under a number of error conditions, such as Line AIS, etc. OUT OF RANGE appears if the pointer value is outside the range.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of REI errors present. Up to 2 REI errors may be counted per multi-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of VT REIs to the total number of received bits, less the SOH, LOH and the POH overhead.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of C-Bit parity errors detected since initial DS3 frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of C-bit parity errors to the number of bits over which C-bit parity was calculated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more C-bit parity error occurred since initial DS3 frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which sync loss occurred due to C-Bit parity errors since initial DS3 frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Display of the FEAC message carried in the C-bit FEAC channel.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which the received X-bits are zero within the 1 second interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of FEBEs detected since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of FEBEs to the number of bits over which C-bit parity was calculated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which at least one FEBE occurred since initial DS3 C-bit frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which an LOF was present since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which an out-of-frame condition or an AIS is detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of M-Frames that contain a mismatch between either parity bit (P-bits) and the parity calculated from the information bits in the previous M-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of parity errors to the number of bits over which parity was calculated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more parity errors occurred since initial DS3 frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current status of the received X-bits when in a framed mode. The result is available after receiving DS3 frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current setting of the transmitted X-bits when in a framed mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times sync loss occurred due to CRC errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which sync loss occurred due to CRC errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of REBEs (Remote End Block Errors) detected in the E bits since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of FAS bit errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of FAS bit errors to the number of bits over which FAS was calculated.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of FAS word errors received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during with MF-AIS was detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of MFAS word errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of MFAS word errors to received framing bits since initially acquiring frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of MFAS synchronization losses since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which one or more MFAS synchronization losses occurred or during which frame synchronization could not be achieved, since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which MF-RDI was detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Display of the Non-Frame Alignment word carried in the C-bit FEAC channel.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of line codes to received bits since detecting a signal.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which line codes occurred since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of line codes detected in the received signal (that are not line codes embedded in valid B8ZS sequences) since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of seconds during which FAS bit errors were detected since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of discrete losses of frame synchronization since initial frame synchronization or last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of FAS word errors to received framing bits since initially acquiring frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of seconds during which FAS word errors were detected since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of errored frame alignment signal (FAS) subsets (subset of bytes A1 and A2) received since gaining initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power level of the received signal, expressed in dBdsx.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power level of the received signal, expressed in dBm.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Power level of the received signal, expressed in Vpp.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the current number of credits the transmitter can use to send frames. Each time a frame is transmitted, the count decreases by one; each time a frame is acknowledged from the far end through an R_RDY, the count increases by one, up to the maximum value established during login.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of credits communicated by the far end during ELP login.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the status of the Fibre Channel login process by displaying one of the following: IN PROGRESS, COMPLETE, FAILED/LOOP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of credits communicated by the near-end during Implicit login.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel Rx_RDY primitives since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of accept messages received in response to login requests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of acknowledgments received in response to login requests or accept/reject messages.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of rejections received in response to login requests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of login requests received from another Viavi compliant Ethernet tester or a distance extension device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of transmitted Fibre Channel Rx_RDY primitives since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of accept messages transmitted in response to login requests from another Viavi compliant Ethernet tester or a distance extension device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of acknowledgments transmitted in response to login requests or accept/reject messages from another Viavi compliant Ethernet tester or a distance extension device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of rejections transmitted in response to login requests from Viavi compliant Ethernet tester or a distance extension device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of login requests transmitted to another Viavi compliant Ethernet tester or a distance extension device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A one second period in which one or more errors are recorded or Loss of Link is detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of Errored Seconds to total seconds in available time during a fixed measurement interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A one second period in which 30 percent or more of the frames are lost or FCS errored or Loss or Link is detected. If in a one second period ((FCS Errors + Lost Frames)/(Frames Received + Lost Frames)) >= 0.3, then this shall be reported.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of Severely Errored Seconds to total seconds in available time during a fixed measurement interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This starts when 10 consecutive severely errored seconds have been detected. These 10 seconds are part of the displayed value.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of FAS errors to the total number of frames received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of FAS errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of discrete losses of frame synchronization since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which one or more LOFs occurred, since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of MFAS errors to the total number of frames received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of MFAS errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which one or more MFAS synchronization losses occurred or during which MFAS synchronization could not be achieved, since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which an OOF was counted since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which an OOM was detected since initial frame synchronization. OOM is declared if the received MFAS is out of sequence for five consecutive frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Correctable Bit Error Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Correctable Bit Errors</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Correctable Word Error Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current ratio of uncorrectable word errors, to the total bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of uncorrectable word errors received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the port ID for the source N port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the destination node.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the port ID for the destination N port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the destination N port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the status of the fabric login process by displaying one of the following:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the fabric that the instrument logged into.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether a fabric is present (Yes or No).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the F Port that the instrument logged into.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the status of the N Port login process by displaying one of the following:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the source node.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name of the source N port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A summed count of FCS Errored Frames, Jabbers and Undersized Frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Shows the current status of the Summary Result page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Green:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All results okay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Yellow:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There is or was at least one warning.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Red:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There is or was at least one alarm and/or error event present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current value of the High Path or Low Path Alarm Indicator Signal, depending on which Section is being monitored.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Text string of the Tandem Connection Access Point Identifier.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the difference between the incoming BIP value and the IEC value in the TCM multiframe.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cumulative value of the Incoming Error Count since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the receive side has lost frame sync with the TCM multiframe contained in the TCM byte (N1 or N2), resulting in Loss of Tandem connection (LTC).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Outgoing Defect Indication (ODI) status carried in the TCM Multi-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Outgoing Error Indication (OEI) count carried in the TCM Multi-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Remote Defect Indication (RDI) status carried in the TCM Multi-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Remote Error Indication (REI) status carried in the TCM Multi-frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates there is no TCM information carried in the TCM byte; the byte value is zero.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of IGMP frames received since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Gateway address assigned by the DHCP server for the currently selected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the IP address assigned by the DHCP server to the currently selected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Subnet mask assigned by the DHCP server for the currently selected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received layer 4 (TCP/UDP) traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet broadcast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet broadcast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by filtered IP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet multicast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 1024 and 1500 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 128 and 255 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 20 and 45 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 256 and 511 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 46 and 63 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 512 and 1023 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with lengths between 64 and 127 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered IP packets received since the last test restart, including errored packets.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet IP packets with a length greater than 1500 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average rate of filtered packets, calculated over the time period elapsed since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current rate of filtered packets. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum rate of filtered packets over a one second period.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum rate of filtered packets over a one second period.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average size of filtered packets since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum size of filtered packets since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum size of filtered packets since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average bandwidth utilized by filtered IP traffic. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by filtered IP traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The peak bandwidth utilized by filtered IP traffic since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum bandwidth utilized by filtered IP traffic since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet unicast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received IP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received IP packets with a checksum error in the header.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet unicast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet multicast IP packets received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 1024 and 1500 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 128 and 255 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 20 and 45 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 256 and 511 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 46 and 63 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 512 and 1023 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with lengths between 64 and 127 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of IP packets received since the last test restart, including errored packets.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet IP packets with a length greater than 1500 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received IP packets that exceed the available Ethernet payload field.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average rate of received packets, calculated over the time period elapsed since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current rate of received packets. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum rate of received packets over a one second period.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum rate of received packets over a one second period.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average size of packets received since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum size of packets received since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum size of packets received since IP packet detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average bandwidth utilized by the received IP traffic. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received IP traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The peak bandwidth utilized by the received IP traffic since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum bandwidth utilized by the received IP traffic since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the hardware (MAC) address of either the gateway or the destination host as resolved by ARP for the currently selected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the transmitted IP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of IP packets transmitted since the last test restart. This result does not appear when testing in Monitor mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of errored blocks not occurring as part of an SES.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of Background Block Errors (BBE) to total blocks in available time during a fixed measurement interval. The count of total blocks excludes all blocks during SESs.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errored seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of errored seconds to the number of available seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of severely errored periods, defined as a sequence of between 3 to 9 consecutive severely errored seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of SEP events in available time, divided by the total available time in seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Severely errored seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of severely errored seconds to the number of available seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of test seconds which met the associated ITU-T recommendation's definition of unavailable time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ACCEPT indicates that the test results have met the ITU-T recommendation's performance objectives.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current packet jitter measured for received packets during the last test interval, calculated in milliseconds. When running Analyzer applications, if the stream is RTP encapsulated, this is derived using the RTP header.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The highest packet delay variation measured since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current transmitter clock frequency, expressed in Hz.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of each invalid 66-bit code word in the bit stream due to synchronization header errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of seconds the pattern is received without any errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The percentage of seconds that the received pattern is error free.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of pattern bit errors to received pattern bits since initially acquiring pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of seconds that the received pattern contained at least one error.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of received bits in a recognized pattern that do not match the expected value.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of code violations to bits received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of seconds during which code violations occurred.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of instances when block synchronization was lost since the last test start or restart. Only applicable when running 10 GigE applications.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of each invalid 66-bit code word in the bit stream due to synchronization header errors. Code Violations are a good indicator of bit errors on the link from remote port to the local port. Even if there is no Ethernet load the Code Violations will detect layer 1 link problems.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of pattern errors to received patterns since initially acquiring pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of received patterns that do not match the expected pattern.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which pattern synchronization was lost since initially acquiring pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the total number of bits received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which a synchronization was not present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Synchronization presence. If OFF, synchronization is not currently present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of pattern bit errors to received pattern bits since initially acquiring frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of error-free seconds during which error analysis has been performed since initial pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of error-free seconds divided by the number of seconds during which error analysis has been performed since initial pattern synchronization, expressed as a percentage.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which one or more pattern bit errors occurred since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times pattern synchronization was lost since initially acquiring pattern synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Synchronization with the received layer 2 patterns has been achieved.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History Yellow:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Synchronization has been lost once or multiple times since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A summed count of Fibre Channel frames containing Cyclic Redundancy Check (CRC) errors. When receiving Fibre Channel jumbo frames containing CRC errors, the CRC error count does not increment. Instead, these frames are counted as Fibre Jabbers.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of frames received since the last test restart, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames are currently detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames were detected, and then not present for > 1 second.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of transmitted frames since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the name specified when you configured the test frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays one of the following:&#xA;N/A. Indicates that a particular test frame is not configured to be transmitted.&#xA;IDLE. Indicates that a particular test frame is in the queue to be transmitted.&#xA;In Progress. Indicates that a particular test frame is currently being transmitted, and has not&#xA;yet encountered an error.&#xA;Timeout. Indicates that for a particular test frame a timeout was reached while waiting for a&#xA;transmitted frame to return; however, all frames were successfully looped back before the&#xA;end of the test frame's transmission.&#xA;Payload Errors. Indicates that for a particular test frame all transmitted frames were successfully&#xA;looped back, but a received frame contained a payload that was not the same as its&#xA;transmitted payload.&#xA;Header Errors. Indicates that for a particular test frame, all transmitted frames were successfully&#xA;looped back, but a received frame contained a header that was different from its transmitted&#xA;header.&#xA;Count Mismatch. Indicates that the number of received frames for a particular test frame did&#xA;not match the number of frames transmitted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of test frames for a particular test frame type received by the instrument since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of test frames for a particular test frame type transmitted by the instrument since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the transmitted TCP/UDP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel Class 1 frames since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel Class 2 frames since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel Class 3 frames since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel Class F frames since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A summed count of FCS Errored Frames, Jabbers, and Undersized Frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 1024 and 2140 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 128 and 252 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 256 and 5088 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 28 and 64 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 512 and 1020 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Fibre Channel frames with lengths between 68 and 124 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the total number of frame bytes received since the test was started. The count starts at the Destination Address and continues to the Frame Check Sequence.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of filtered error-free frames since the test was started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average rate is calculated over the time period elapsed since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current rate of filtered frames taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum rate is taken over a one second period since frame detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum rate is taken over a one second period.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the filtered traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current data rate of filtered frames calculated over the prior second of test time. Data rate is the frame bandwidth, excluding the preamble, start of frame delimiter, and minimum inter-frame gap.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average bandwidth utilized by the filtered traffic, expressed as a percentage of the line rate of available bandwidth calculated over the time period since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the filtered traffic expressed as a percentage of the line rate of available bandwidth. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The peak bandwidth utilized by the filtered traffic since the last test restart expressed as a percentage of the line rate of available bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum bandwidth utilized by the filtered traffic since the last test restart expressed as a percentage of the line rate of available bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 1024 and 2140 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 128 and 252 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 256 and 5088 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 28 and 64 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 512 and 1020 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Fibre Channel frames with lengths between 68 and 124 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current rate of received frames taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current data rate of received frames calculated over the prior second of test time. Data rate is the frame bandwidth, excluding the preamble, start of frame delimiter, and minimum inter-frame gap.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The service disruption time (maximum inter-frame gap) when service switches to a protect line calculated in microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average bandwidth utilized by the received traffic, expressed as a percentage of the line rate of available bandwidth calculated over the time period since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received traffic expressed as a percentage of the line rate of available bandwidth. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The peak bandwidth utilized by the received traffic since the last test restart expressed as a percentage of the line rate of available bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum bandwidth utilized by the received traffic since the last test restart expressed as a percentage of the line rate of available bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of frames under the minimum 64 byte with a good FCS.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the transmitted traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current data rate of transmitted frames calculated over the prior second of test time. Data rate is the frame bandwidth, excluding the preamble, start of frame delimiter, and minimum inter-frame gap.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the total number of frame bytes transmitted since the test was started. The count starts at the Destination Address and continues to the Frame Check Sequence. The count does not include the preamble.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errors detected because receive checksum value is not valid&amp;#60;br&amp;#62;&amp;#60;br&amp;#62;&amp;#60;b&amp;#62;Possible cause(s)&amp;#60;&amp;#47;b&amp;#62;&amp;#13;&amp;#60;ul&amp;#62;&amp;#60;li&amp;#62;Receiving UDP traffic and expecting TCP traffic&amp;#60;&amp;#47;li&amp;#62;&amp;#60;&amp;#47;ul&amp;#62;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Destination Port number for the last layer 4 packet received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the Source Port number for the last layer 4 packet received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by filtered layer 4 (TCP/UDP) traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the total number of LBM frame received since the last OAM setting was specified or changed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the total number of LBM frames transmitted since the last OAM setting was specified or changed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the total number of LBR frames transmitted since the last OAM setting was specified or changed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of APS messages since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The B2 Errors/Total number of received bits, less the SOH. The denominator of the message is the total number of non-Section received bits instead of the number of B2 so that the result is used to approximate overall received bit error rate. This approximation works on the assumption that only one bit error occurs per SOH frame per bit position.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of errors in the even parity Line B2 byte used as a parity check against the preceding frame less the regenerator or section overhead. Up to eight B2 errors may be counted per STS.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays a total count of instances where the distance errors fell below the Distance Error threshold during the last test interval. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value for the shortest loss period detected since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays a total count of instances where the distance errors fell below the Distance Error threshold since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the number of loss period errors detected during the last test interval. A loss period error is declared whenever the loss period exceeds the Loss Period threshold. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value for the longest loss period detected since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of loss period errors detected since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of PAT errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of sync byte errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of sync byte errors detected since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of transport errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the transport stream ID carried in the PAT for each discovered stream.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The smoothed average value of the packet delay variation since the last test restart (per RFC 1889), calculated in microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current Packet Jitter measured over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum Packet Jitter, Avg (us) measured since the last test restart, calculated in microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet broadcast frames received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet frames containing Frame Check Sequence (FCS) errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet broadcast frames since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet frames with lengths between 128 and 255 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet frames with lengths between 256 and 511 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet frames with lengths between 512 and 1023 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet frames with a length of 64 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Ethernet frames with lengths between 65 and 127 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet multicast frames received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered 802.1d spanning tree frames since frame detection after the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the DEI of the last filtered tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SVLAN priority of the last filtered tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SVLAN ID of the last filtered tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of filtered Ethernet unicast frames since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered VLAN frames as defined in IEEE 802.p/q since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the VLAN priority of the last filtered tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the VLAN ID of the last filtered tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered Q-in-Q frames since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames with lengths between 128 and 255 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames with lengths between 256 and 511 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames with lengths between 512 and 1023 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames with a length of 64 bytes.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames with lengths between 65 and 127 bytes, inclusive.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Ethernet frames that have a byte value greater than the maximum 1518 frame length (or 1522 for VLAN tagged and 1526 for QinQ tagged) and an errored FCS. Jabbers are not counted at the FCS Errored Frames count.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of seconds during which LPAC (loss of performance assessment capability) is declared since the last test restart. LPAC is declared if:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received VLAN frames as defined in IEEE 802.p/q since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received QinQ frames since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet frames containing Frame Check Sequence (FCS) errors. When receiving Ethernet jumbo frames containing FCS errors, the FCS error count does not increment. Instead, these frames are counted as Jabbers.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet multicast frames received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of pause frames received from a remote Ethernet device. Pause frames are utilized for flow control and alert the transmitting device that it must reduce the outgoing frame rate or risk a receiver overflow on the far end, resulting in dropped traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of Ethernet frames under the minimum 64 byte frame length containing Frame Check Sequence (FCS) errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received 802.1d spanning tree frames since frame detection after the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Ethernet unicast frames received since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the DEI of the last received tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SVLAN priority of the last received tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SVLAN ID of the last received tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN tagged frames are currently detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN tagged frames were detected and then not present for > 1 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the VLAN priority of the last received tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the VLAN ID of the last received tagged frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN tagged frames are currently detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN tagged frames were detected and then not present for > 1 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the total number of VPLS customer frame bytes transmitted since the test was started. The count starts at the Destination Address and continues to the Frame Check Sequence. The count does not include the preamble.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current bandwidth utilized by the program expressed in megabits per second. This measurement is an average taken during the current test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum bandwidth utilized by the program expressed in megabits per second since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the minimum bandwidth utilized by the program expressed in megabits per second since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current media delivery index delay factor (MDI-DF). The current count is an average of the measurements taken during each test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current media delivery index loss rate (MDI MLR). For RTP encapsulated video streams, the current MLR is calculated by counting the number of lost IP packets during the last test interval, and multiplying this number by seven. If a stream is not RTP encapsulated, this result is the same as the CC Lost Count.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered MPLS frames since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 2 priority of the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 1 priority of the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays label 2 of the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays label 1 of the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 2 TTL value for the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 1 TTL value for the last filtered MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received VPLS or MPLS-TP frames since the test was started, including errored frames. Appears in the L2 SP Link Counts category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 2 priority of the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 1 priority of the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays label 2 of the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays label 1 of the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 2 TTL value for the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the label 1 TTL value for the last received MPLS encapsulated frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of VPLS or MPLS-TP labels for all frames received since starting the test. Result appears in the L2 SP Link Stats category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the minimum number of VPLS or MPLS-TP labels for all frames received since starting the test. Result appears in the L2 SP Link Stats category.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received MPLS frames since the test was started, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of MPLS labels for all frames received since starting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the minimum number of MPLS labels for all frames received since starting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average amount of time, in ms, of a MSTV burst complete message to its AckBurstComplete message</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of time, in ms, of a MSTV burst complete message to its AckBurstComplete message</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum amount of time, in ms, of a MSTV burst complete message to its AckBurstComplete message</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average amount of time, in ms, of a MSTV command message to its appropriate response, including instant channel change (ICC) request and status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of time, in ms, of a MSTV command message to its appropriate response, including instant channel change (ICC) request and status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum of time, in ms, of a MSTV command message to its appropriate response, including instant channel change (ICC) request and status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Distribution of the number of ICC Latency (without Burst) measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Distribution of the number of ICC Latency (with Burst) measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first unicast media packet of the burst video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of ICC Latency (with burst) measurements done</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first unicast media packet of the burst video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first unicast media packet of the burst video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first multicast media packet of the video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of ICC Latency (without burst) measurements done</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first multicast media packet of the video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum amount of time, in ms, of a MSTV Instant Channel Change (ICC) request to the first multicast media packet of the video stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average amount of time, in ms, of a RUDP request message to the first unicast retry media packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of RUDP Latency measurements done</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Distribution of the number of RUDP Latency measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of time, in ms, of a RUDP request message to the first unicast retry media packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum amount of time, in ms, of a RUDP request message to the first unicast retry media packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of MSTV ICC requests sent</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average bitrate of all ICC media packets, plus uncategorized or late RUDP media packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bitrate of all ICC media packets, plus uncategorized or late RUDP media packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum bitrate of all ICC media packets, plus uncategorized or late RUDP media packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which ODU-AIS was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which PM-BDI was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of PM-BEI errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of PM-BEI errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of PM-BIP errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of PM-BIP errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the PM-DAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the DAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether there is no signal, the signal failed, or the signal is degraded for the backward/upstream signal. Appears after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the backward/upstream operator identifier after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which the backward/upstream signal was degraded after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which the backward/upstream signal failed after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether there is no signal, the signal failed, or the signal is degraded for the forward/downstream signal. Appears after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the received forward/downstream operator identifier after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which the forward/downstream signal failed after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the forward/downstream operator identifier after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which ODU-LCK was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which ODU-OCI was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the operator specific identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the PM-SAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which BDI was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of BEI errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of BEI errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which BIAE was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of BIP errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of BIP errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which IAE was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which TIM was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which PM-TIM was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value of the destination node bit field carried in the K1 byte. It is typically set when APS events occur in the network.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the status code carried in bits 6-8 of the K2 byte. It is typically set when APS events occur in the network.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value of the source node bit field carried in the K2 byte. It is typically set when APS events occur in the network.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value of the Path code bit field carried in the K2 byte. It is typically set when APS events occur in the network.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of the channel bridged bits 1-4 on the protection line. If 0, then no line is bridged to the APS line.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the S1 byte message, carried in bits 5 through 8, after frame synchronization and signal presence are detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate of Cyclic Redundancy Check errors detected while processing CDMA Sync messages.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The model and firmware version of the detected CDMA Receiver.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Automatic amplification of the cellular signal. A higher gain control implies higher signal noise. Typically between 150 to 220.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If a hardware failure is detected, the CDMA Receiver will need to be returned to the manufacturer for service.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The CDMA Receiver has been unable to acquire a signal for more than one hour. Check cellular coverage in your area. If the problem persists, the CDMA receiver may need to be returned to the manufacturer for service.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The noise offset of the currently locked base station, or 0 if signal state is not locked.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal to noise ratio of the currently detected signal, or 0 if no signal detected. Typically between 2.5 to 11.0.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current state of the CDMA Receiver's signal processor.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The measured accuracy of the received signal.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Figure Of Merit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Description</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Accuracy &amp;lt; +/- 100 microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Accuracy &amp;lt; +/- 1 millisecond.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Accuracy &amp;lt; +/- 10 milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Accuracy &amp;gt; +/- 10 milliseconds or no  signal locked.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Condition of the CDMA Receiver's internal oscillation frequency. If excessive drift is measured, the CDMA Receiver is nearing its end of life. If a failure occurs, the CDMA Receiver will need to be returned to the manufacturer for service.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of available seconds during which one or more relevant anomalies or defects were present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Seconds during which one or more defects were present or the anomaly rate exceeded the ITU-T recommended threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which the expected and received payload types do not match after multi-frame synchronization was gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which OTU-AIS was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which SM-BDI was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of SM-BEI errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of SM-BEI errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which SM-BIAE was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of SM-BIP errors to the total number of bits received since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of SM-BIP errors since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SM-DAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which SM-IAE was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the SM-SAPI identifier after multi-frame synchronization is gained.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of asynchronous test seconds in which SM-TIM was present for any portion of the test second since initial frame synchronization.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current number packets lost within the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum value recorded for the Pkt Loss Cur result since starting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of packets lost since starting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average one way delay calculated in microseconds. If unavailable, ensure that both near and far-end units have One Way Delay equipment installed and are configured to transmit Acterna payload, which is located on the Ethernet"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current one way delay calculated in microseconds. If unavailable, ensure that both near and far-end units have One Way Delay equipment installed and are configured to transmit Acterna payload, which is located on the Ethernet"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum one way delay calculated in microseconds. If unavailable, ensure that both near and far-end units have One Way Delay equipment installed and are configured to transmit Acterna payload, which is located on the Ethernet"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum one way delay calculated in microseconds. If unavailable, ensure that both near and far-end units have One Way Delay equipment installed and are configured to transmit Acterna payload, which is located on the Ethernet"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received Acterna frames, including errored frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The round trip delay for all pings sent and successfully received by the MSAM since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current round trip delay calculated in microseconds. To measure round trip delay, an Acterna payload must be originated at the near end and looped back at the far end.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of lost Acterna test frames in the received traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of each instance where the RX detects out of sequence Acterna test frames in the filtered traffic. For example, if the Rx detects sequence numbers: 1,2,3, ,5,6, , , , ,11,12) it counts 5 frame loss (4,7,8,9,10) and 2 OoS (3 to 5 and 6 to 11).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of frames lost to the number of frames expected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum round trip delay calculated in microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum round trip delay calculated in microseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of received IP packets containing Acterna Payload checksum errors.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This is a warning that Acterna Test Packets (ATP) are not being received. ATP must be present to obtain Frame Loss, Jitter and Round Trip Delay results. If you wish to see these results, please start test traffic and make sure the Tx Payload is configured for Acterna.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>These LEDs tell you whether Acterna Test Packets (ATP) are being received. ATP must be present to obtain Frame Loss, Jitter and Round Trip Delay results. If you wish to see these results, please start test traffic and make sure the Tx Payload is configured for Acterna.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames with Acterna test payload are currently detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A frame with Acterna test payload was detected, and then not present for > 1 second.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of transmitted Acterna frames since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current number of PAT errors detected during the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the drop eligible bit carried in the frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the VLAN priority carried in the frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the ID for the backbone VLAN used as the path to the destination carried in the frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value carried in the B-Tag field (VLAN ID + Priority + Drop Eligible) in a hexadecimal format.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the drop eligible bit carried in the last frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the priority carried in the last frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the service ID carried in the last frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value carried in the I-Tag field (Service ID + Priority + DEI + Use Customer Address) in a hexadecimal format.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current PCR jitter measured as an average taken during the last test interval, in milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the errored blocks received since the last test start or restart. Only applicable when running 10 GigE applications.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link activity. If OFF, link is not currently active.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AutoNeg was successful, and link is established.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History Red:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link has been lost once or multiple times since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which the link was down (lost).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Fault Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the number of test seconds during which a local fault occurred, indicating that the MSAM could not detect a received signal, could not obtain PCS block synchronization, or detects 16 or more errored PCS block sync headers in a 125 microsecond period. Only applicable when testing 10 Gigabit Ethernet interfaces.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Fault Seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the number of test seconds during which the instrument transmits a remote fault indication in response to the receipt of a remote fault indication from its link partner. Only applicable when testing 10 Gigabit Ethernet interfaces.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of times the unit has received a jam signal while it was not transmitting frames. Result only appears for half-duplex 10/100 Ethernet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of each incorrect 64B/66B block found, as defined by IEEE 802.3ae. If there are Symbol errors and no Code Violations it is probably a problem in the opposite connected port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Synchronization is established</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Synchronization has been lost once or multiple times since the lasts test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of frames received containing both a framing error and an FCS error. Only applicable when testing on 10/100 Mbps circuits.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the negotiated duplex setting for the link (half or full).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether Flow Control is turned On or Off on your unit.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is full duplex capable at 1000Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is full duplex capable at 100Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is full duplex capable at 10Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is full duplex capable (YES or NO).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is half duplex capable at 1000Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is half duplex capable at 100Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is half duplex capable at 10Base-TX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is half duplex capable (YES or NO).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates that the Port has sent its advertisement for its auto-negotiation capability to the link partner.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates that the link partner has acknowledged the receipt of a valid auto-negotiation capability advertisement from the Port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether the Ethernet link partner is operating as the master (providing the clock for timing), or slave (deriving the clock from the MSAM). Applicable when testing 1000 Base-Tx only.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the flow control capabilities of the Ethernet link partner. Those capabilities are:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If supported by the Ethernet link partner, indicates a reason for auto-negotiation failure. If auto-negotiation succeeded, the result will read "NO".</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the negotiated speed setting for the link (10, 100, 1000).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the negotiated speed setting for the link (10 or 100 Mbps).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of times the unit has transmitted a frame, and then received a jam signal in the time slot for the frame. Result only appears for half duplex 10/100 Ethernet tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of the number of times the unit has transmitted a frame, and then experiences a collision more than 64 byte times after the transmission begins. Result only appears for half-duplex 10/100 Ethernet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of continuity counter errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the number of continuity counter errors detected during the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of continuity counter errors since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current number of PMT errors detected during the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of PID errors detected since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of PID errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the PID number.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the bandwidth utilized by the PID in Mbps.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the DNS errors received during the course of trying to ping the host.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum round trip delay for the pings sent and successfully received by the MSAM. Calculated in milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The minimum round trip delay for the pings sent and successfully received by the MSAM. Calculated in milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The percentage of the total test seconds during which replies were not received within 3 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of Ping requests sent by the MSAM for which replies were not received within 3 seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the ping replies received in response to the ping requests sent by the MSAM.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the ping replies sent from the MSAM.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the ping requests received by the MSAM (in other words, requests sent to the MSAM's IP address) from another Layer 3 device on the network.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the ping requests sent from the MSAM.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of PMT errors detected during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Protocol being used to transmit data. Either IPoE or PPPoE.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum PCR jitter during a single test interval since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the total number of PIDs for a particular program.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the program ID for the PMT (Program Map Table)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the program number carried in the PAT for the stream.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average received Delay Request instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current received Delay Request instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum received Delay Request instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum received Delay Request instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Delay Request packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Delay Request packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Delay Request packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum Delay Request packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Characterizes the grandmaster clock for the purpose of the best grandmaster clock algorithm</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Denotes the traceability of the time or frequency distributed by the grandmaster clock</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unique identifier for the grandmaster clock</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Used in the execution of the best master clock algorithm.  Lower values take precedence</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates the source of the time used by the grandmaster clock</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average latency of one-way delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current latency of one-way delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum latency of one-way delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum latency value of one-way delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PTP state machine has following states: initializing, faulty, disabled, listening, pre-master, master, passive, uncalibrated, and slave</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of received Announce messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Announce frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Delay Request frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Delay frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Delay Response frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Delay Response frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of received domain mismatched messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Follow Up frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Follow Up frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Management frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Management frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Signaling frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Signaling frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of received Sync frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of received Sync frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average received Sync instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current received Sync instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum received Sync instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum received Sync instantaneous packet delay variation (IPDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average received Synch packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current received Synch packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum received Synch packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum received Synch packet delay variation (PDV)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Announce messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Announce frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Delay Request frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Request frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Delay Response frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Delay Response frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Follow Up frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Follow Up frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Management frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Management frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Signaling frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Signaling frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The count of transmitted Sync frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The rate of transmitted Sync frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays a count of out of sequence frames detected during the current test interval. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays a count of out of sequence frames detected since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>For each discovered stream, Yes indicates that an RTP header is present; No indicates that no RTP header is present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of B3 errors (indicating an error in the previous frame since initial SONET frame synchronization) since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of errors in the even parity BIP-8 (B1) byte used as a parity check against the preceding frame. Up to eight B1 errors may be counted per frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of Section BIP errors to the total number of received bits.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the 16 or 64-byte Section trace ASCII message that is carried in the Section overhead byte (J0).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of four contiguous errored frame alignment words (A1/A2 pair) detected since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the value of the signal label (C2) byte, indicating the type of data in the Path.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allow traffic to go above 99.99x%. In some instances, traffic rates over 99.99x% can over run the receiving device, in which case lost/dropped frames may be detected. Note - 99.99x% load is sufficient when installing and commissioning Ethernet and FC services.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA;The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  Implicit (Transparent Link)/Explicit (E-Port) Login&#xA;3.  TX/RX Buffer Credits set to the same value as entered above.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of steps to test for Buffer Credit Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire (larger values will result in a shorter test time when using the RFC 2544 Standard zeroing-in process).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the percentage of the bandwidth found during the Throughput test at which you would like to run the Latency test for each frame size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test decreases the transmitted bandwidth in these configured increments in the presence of frame loss.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose whether to run a 'Committed Burst Size' (CBS), CBS Policing (MEF 34), or Burst Hunt test.&#xA;&#xA;The Committed Burst Size test attempts a number of bursts that average out to either the configured Max Bandwidth or the bandwidth discovered from the Throughput test if performed.&#xA;&#xA;The CBS Policing test increases the CBS test's burst size to account for throughput refresh and then doubles the burst size. An average throughput is maintained in the same way as the CBS test.&#xA;&#xA;The Burst Hunt test attempts to determine the maximum supported burst size by varying the duration of a burst between a user-configurable minimum and maximum burst size in kBytes (kB).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selecting this option will allow the Burst (CBS) test to ignore Pause Frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selecting this option will allow the Back to Back test to ignore Pause Frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The CBS Policing test is considered to Pass if the estimated CBS value matches the configured CBS Size +/- the configured tolerance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure the CBS Size to match the CBS under test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum points for the Burst Hunt test.  The test will not exceed the maximum value while testing.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the duration, in seconds, for the CBS Test duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the Extended Load Test sends traffic, the Max Bandwidth will be scaled by this value.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds during which an invalid signal was received since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current received frequency deviation. Displayed in PPM.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frequency of the clock recovered from the received signal, expressed in Hz.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the receive level for optical signals in dBm.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times LOS was present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which an LOS was present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays ON if the received optical power level is greater than the receiver shutdown specification as stated in the specifications appendix of the Getting Started guide that shipped with your instrument, or as stated in the vendor specifications for the SFP or XFP you have inserted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of times signal was not present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of seconds during which a signal was not present since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal presence. If OFF, signal is not currently present.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>At the Rx is a signal with in valid power range.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Signal has been lost once or multiple times since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum transmitted frequency deviation, expressed in ppm.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current transmitted frequency deviation. Displayed in PPM.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the external timing for the received signal.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the timing source (INTERNAL, RECOVERED, or BITS).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate of Path BIP byte (B3) errors divided by the total number of received bits less the SOH and LOH.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count of the number of seconds in which the Path trace identifier (J1) is different than the expected value since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds in which payload mismatch Path errors occurred since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current number of sync byte errors detected during the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of SSM event messages received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of information SSM messages received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of malformed SSM messages received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PDU rate of received SSM messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>QL value of received SSM</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of SSM messages received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ratio of BPVs to received bits since detecting a signal.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of seconds during which BPVs occurred since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of bipolar violations (BPVs) detected in the received signal (that are not BPVs embedded in valid B8ZS sequences) since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received TCP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of filtered TCP packets received since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A count of TCP packets received since the last test start or restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the number of transport errors detected during the last test interval.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current day and month.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current time of day in hours, minutes, and seconds (hh:mm:ss).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Amount of time in hours, minutes, and seconds (hh:mm:ss) since the last test restart.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average, maximum, and minimum size of frames received since frame detection.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the destination UDP port number carried in each discovered stream.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum packet jitter measured for received packets since the last test restart, calculated in milliseconds. When running Analyzer applications, if the stream is RTP encapsulated, this is derived using the RTP header.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum number of distance errors measured in a test interval since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum value for the Period Err Cur result since starting or restarting the test. This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the layer 1 bandwidth utilized by each discovered stream (in Mbps).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum media delivery index delay factor (MDI-DF) detected since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the current media delivery index loss rate (MDI MLR) declared since starting or restarting the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>For MPTS streams, displays the number of programs carried in each discovered stream.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Displays the maximum value for the OoS Pkts Cur result since starting or restarting the test.  This result is only available when analyzing RTP encapsulated video streams.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current bandwidth utilized by the received UDP traffic expressed in megabits per second. This measurement is an average taken over the prior second of test time.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Burst R Factors (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Conversational Quality MOS scores (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Conversational Quality R Factors (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of G.107 R Factors (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Gap R Factors (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Listener Quality MOS scores (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average of Listener Quality R Factors (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time that data is held in the Jitter Buffer.  This is the Jitter Buffer Size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffering delay as a percentage of the total delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current R Factor for Burst conditions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The duration of the call in the following format: HH:MM:SS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The state of the current call or Unavailable if the network state is down or not registered.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The codec used for the audio being received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The codec used for the audio being transmitted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Conversational Quality MOS score.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Conversational Quality R Factor.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time to encode the audio sample by the Codec.  Based on the Codec Type.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encoding Delay as a percentage of Total Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of voice data being received per Audio packet. In units of milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of voice data being transmitted per Audio packet. In units of milliseconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current G.107 R Factor.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current R Factor for Gap conditions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of times Jitter Buffer has dropped audio samples.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Jitter Buffer Replays.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio bytes transmitted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Content rating for the current call. Either Pass or Fail.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Content rating history for the current call. Either Pass or Fail.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way delay, Current = Round Trip Delay determined from RTCP)/2.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way Delay, Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way Delay, Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Rating based on Delay thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History rating based on Delay thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Deviation in packet arrival times in msec.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Deviation in packet arrival times in msec.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Deviation in packet arrival times in msec.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Rating based on Jitter thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History rating based on Jitter thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of lost Audio packets.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio packets that were lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost Audio packets as a percentage of total Audio packets received.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current rating based on Loss thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History rating based on Loss thresholds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio packets that were out of sequence.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio packets received</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio Packets transmitted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Receive rate of Audio packets. In Kbps.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmit rate of Audio packets. In Kbps.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Listener Quality MOS score.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Listener Quality R Factor.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current R Factor for Listener Quality.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Burst R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Conversational Quality MOS score (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Conversational Quality R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max G.107 R Factor (for all the previous calls)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Gap R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Listener Quality MOS score (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Listener Quality R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Burst R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Conversational Quality MOS score (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Conversational Quality R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min G.107 R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Gap R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Listener Quality MOS score (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Listener Quality R Factor (for all the previous calls).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(RTD calculated from RTCP)/2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Delay as a percentage of Total Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The current network state. Network is up if sync and link are active and a source address has been assigned.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time it is taking to packetize the incoming data.  This is the frame interval (speech per frame) for the incoming audio data.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packetization Delay as a percentage of Total Delay.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Input Signal Strength.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Output Signal Strength.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Phone Number Alias of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio bytes transmitted from the far end.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way delay, Current.  This is the same result as the Audio Delay Current ? Local.  It is computed as RTD/2.  RTD is determined from RTCP.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way delay, Max.  This is the same result as the Audio Delay Max ? Local.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>One way delay, Min.  This is the same result as the Audio Delay Min ? Local.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Audio Jitter at the Remote End.  Reported by RTCP Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Audio Jitter at the Remote End.  Reported by RTCP Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum Audio Jitter at the Remote End.  Reported by RTCP Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The number of Audio Packets lost on their way to the Remote End.  Reported by RTCP Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The percentage of Audio packets lost on their way to the Remote End.  Calculated from RTCP Reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ID of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number of Audio packets transmitted from the far end.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether RTCP is being used for the Audio Path.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Indicates whether Silence suppression is being used.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The state of the VoIP registration sequence, or unavailable if the network state is down.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total delay including network, encoding, packetization, and buffering.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HI BER - High Bit Error Rate (based on Sync Bits)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BIP-8 - Bit Interleaved Parity-8 (found in Alignment Markers)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skew - Inter-lane relative delay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OOLLM - Out of logical lane marker which is calculated as sum of all lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LLM - Logical Lane Marker</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OOL - Out of lane alignment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OOR - Out of recovery which is calculated as sum of all lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOR -- Loss of recovery which is calculated as sum of all lanes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOL - Loss of lane alignment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Filters set on Filters page may have affected the MEPs discovered.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This setting controls how traffic will be displayed within the profile.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The profile will display traffic as individual streams, where a "stream" is a group of traffic uniquely identified by the parameters in your selection below. For example, choosing "VLAN ID" means that traffic with every unique combination of SVLAN and VLAN IDs will be grouped into a separate stream.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Up to 128 streams will be discovered.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Both VLAN ID and SVLAN ID tags will be considered. Traffic must contain at least one VLAN tag to be included in the profile. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID and Source MAC Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both VLAN ID tags as well as the source MAC address will be considered. Unlike above, traffic need not be VLAN tagged.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID, Source MAC and Destination MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Similar to above, but also considers the destination MAC address. Use this selection if you want to see MAC-to-MAC conversations.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID and Source IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both VLAN ID tags as well as the source IP address will be considered. Traffic need not be VLAN tagged, but must be IP-based to be included.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN ID and well-known (0-1023) TCP/UDP port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both VLAN tags and the TCP/UDP port number will be considered. Traffic need not be VLAN tagged, but must be TCP or UDP traffic to/from well-known ports to be included. Use this selection to see which services are running, since well-known ports usually identify services.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP, Destination IP, Source Port and Destination Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All four of these parameters will be considered. These parameters form the two ends of a TCP or UDP conversation, so use this selection if you want to see such conversations.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS Labels with VLAN </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both MPLS Labels tags as well as the VLAN ID tags will be considered. Traffic need not be VLAN tagged, but must be MPLS.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS Labels and PW Labels with VLAN </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both MPLS Labels tags and PW Labels tags as well as the VLAN ID tags will be considered. Traffic need not be VLAN tagged, but must be MPLS and PW.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Single: The unit provides results for all traffic originating from the device with the specified source address, and destined for the device with the specified destination address.&#xA;Either: The unit provides results for all traffic originating from the device with the specified source and destined for the device with the specified destination address, or originating from the device with the specified destination and destined for the device with the specified source address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Basic/Detailed Filter Set</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>16 Byte Pattern</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Filter Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Filter. Both filters have to pass with (AND) coupling.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trigger Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Extended Trigger. No filters set (they are all Don't Care). Trigger on unfiltered packets. The filter counts are same as link counts.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Triggering occurs on filtered packets. Only filtered packets will be captured.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please refer to Frequency Grid in the Help menu.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>M is a delta (tolerance) in Mbps above the CIR+EIR rate which is allowed before declaring a policing failure.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Allows the specification of Total CIR and EIR for all services and a Weight % per service. These values are used to automatically allocate the CIR and EIR for each service.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selects policing (overshoot) test to be run.  Traffic will be sent at a rate of Max Load to test policing, and the test will declare a failure if traffic is received at a rate greater than CIR+EIR+M.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The maximum rate of traffic to be sent.  When the policing test is selected, it is defined as CIR + 1.25 x EIR (or 1.25 x CIR + EIR, if EIR is less than 20% of CIR).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable or disable auto answering.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP address of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Phone number of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The amount of audio data to buffer before transmission.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The codec used for audio being transmitted. The call may not be successful if the codec type does not match on the receiving and transmitting end.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Alias of the Near End node. To register enter alias@domain</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dial by either phone number or Name/Uniform Resource Identifier/E-mail address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Name/Uniform Resource Identifier/E-mail address of the Far End.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The sender TLV value is configured by changing the Unit Identifier found on the Network Visibility tab of the Interface setup page.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The TCP Throughput percent represents the percentage of Layer 4 predicted throughput that the measured Layer 4 throughput must exceed for test "pass" verdict.  For example, if the TrueSpeed test predicts a TCP Throughput of 94.9 Mbps for a 100 Mbps CIR then the Layer 4 throughput must exceed 90.16 Mbps.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Presents the signal as a grid where each block is a channel in the signal. The color of a block in the grid indicates that channel's status.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Grey</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Blue</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Red</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Monitored alarm present</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Slashes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid channel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The currently selected channel is highlighted with a yellow frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>When enabled, while the test is running the PPM offset will be increased each second until the positive Max Offset is reached. The PPM Offset will stay at the maximum for ten seconds and then decrease each second until the negative Max Offset is reached. This process repeats for the duration of the test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Destination IP identifies the far end Viavi test instrument on the network for establishing the Communications Channel. This will be located in the IP settings in an L3 or L4 application, or the Network Visibility settings in an L2 application.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>When ARP is disabled, the test instrument will no longer send or respond to ARP requests. The source and destination ethernet MAC addresses will need to be configured on the near end and far end for communication to be successful.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>These settings are for establishing a communications channel to the remote unit. This channel will be used to control it from this end while testing.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If enabled, TrueSAM will stop as soon as any test capable of returning a verdict of Pass or Fail ends with a status of Fail.&#xA;&#xA;A test may not always be capable of checking for Pass/Fail. The checking might be turned off, or it might not support a Pass/Fail verdict at all. In these cases, this setting will have no effect for that particular test.&#xA;&#xA;Independent of this setting, all tests will also check for general errors while running. Such errors may cause the test to abort with a status of Incomplete. This will also cause the overall TrueSAM sequence to abort.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame/Packet Lengths to Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured. The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire. (10.00 Mbps / 1.000 % / 10000 kbps - is recommended for a shorter test time when using the RFC 2544 Standard zeroing-in process).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>For Troubleshooting the RFC 2544 Standard Zeroing-in Process should be used.&#xA;&#xA;For example: when checking the maximum throughput for a given buffer size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>For Commissioning both Zeroing-in Processes, Viavi Enhanced or RFC 2544 Standard, may be used.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency test for each frame/packet size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select the desired behavior in the event that a OWD test cannot be performed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency trial will last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the percentage of the bandwidth found during the Throughput test at which you would like to run the Latency test for each frame/packet size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame/packet size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.  NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame/packet size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the System Recovery test for each frame/packet size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time each System Recovery trial will last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>It is recommended to select all steps for the first trial, since the automated test will use results from prior test steps as the input for subsequent steps.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the IP address and TCP port number for the TCP Server test set.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the TOS Type and value for the TCP Server test set.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU) is the end-end maximum IP packet size which will be forwarded without fragmentation.  This value represents the starting point (upper value) with which the test set will begin the Path MTU search.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The duration of the Round Trip Time (RTT) step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the four (4) window sizes that will be used to conduct the TCP throughput scan.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the duration for each window size of the Walk the Window step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If the Path MTU step is conducted, the Walk the Window step will use the MSS determined by the MTU step. The Maximum Segment Size (MSS) is the size of the TCP payload within the Layer 2 and 3 encapsulations. For the normal Ethernet and IP overhead of 54 bytes, an MSS of 1460 bytes would equal a 1514 byte Ethernet frame.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The ideal Window size is the Bandwidth Delay Product (BDP) and is highlighted in the text above.  This window size will be used by each connection, so multiple connections will each use the window size specified. The step defaults to the Windows XP TCP window size of 64KB.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This step will upload a simulated file from the Client to Server test set, and this file will be transferred across the specified number of connections. It is recommended that the file be large enough to conduct at least a 1 minute test (the predicted time is indicated in the text at the top of this screen).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Throughput is declared a failure if the ratio of the actual (measured) L4 TCP Throughput to target (predicted) L4 TCP Throughput is less than the configured threshold. Smaller numbers imply a larger failure tolerance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This is the number of simultaneous TCP connections over which the file will be transferred (default is 1 connection).  Example: 100MB file over 4 connections is equivalent to 4 users each uploading a 100MB file.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If the RTT step was done, then this step will use the RTT found in the RTT step. The RTT will be used to calculate BDP and predicted transfer times.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If the MTU step was done, then this step will use the MSS found in the MTU step. The MSS will be used to calculate the effective Window size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the duration of the Traffic Shaping step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The test set uses the Windows XP default TCP window size of 64KB. It is advisable to use this value for this step.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This is the number of simultaneous TCP connections that will be used to upload data to the server.  By default, the step will over subscribe the link by a factor of ~2 and verify whether traffic shaping or policing are being applied to the TCP traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This represents the minimum bandwidth between the TCP Client and Server testers. Example, the test Client and Server are physically on a GigE LAN.  The WAN connection between sites is 45 Mbps which is the Bottleneck Bandwidth.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disable OoS Results will disable OoS Frame count, Lost Frame Count, and Frame Loss Ratio results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>Policing Failure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MaxIR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CIR (Committed Information Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput committed to conform to Key Performance Indicators (KPIs).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>EIR (Excess Information Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Best effort throughput above CIR, may or may not be dropped entirely.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MaxIR (Max Load)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum rate of traffic to be sent.  All traffic above CIR+EIR must be dropped by policer.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Example</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same. (works with all loop types)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurements</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.  This does not distinguish between errors in the Downstream vs. Upstream links.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely.  Downstream and Upstream traffic measurements are taken independently.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Direction</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Buffer Delay Percentage represents the increase in RTT (during a TCP throughput test) with respect to the baseline network RTT and equals:&#xA;&#xA;(Avg RTT - Baseline RTT) x 100 / Baseline RTT&#xA;&#xA;High values of Buffer Delay percent may indicate that network congestion is causing an increase in RTT (i.e., 0% = no increase in RTT over baseline). Review the Throughput Graphs for detailed analysis of TCP retransmissions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This metric is calculated as:&#xA;&#xA;(Total Bytes - Retransmitted Bytes) * 100 / Total Bytes&#xA;&#xA;Values below 99% indicate packet loss may be significantly affecting TCP performance; review the Throughput Graphs for detailed analysis of TCP retransmissions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The TCP listen port for the server. All connections will be established with this port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The percent (%) of the specified Committed Information Rate (CIR) that is required to pass the TCP Throughput test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The port address of the TrueSpeed VNF server used for defining and controlling tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The port address for all test traffic between the instrument and the TrueSpeed VNF server.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP retransmits were detected per RFC 6349 (TCP Efficiency %) and Buffer Delay % was high enough to suggest buffering was inadequate.&#xA;This often occurs when a 'down shift' from higher speed to lower speed interface occurs.&#xA;Consider increasing the size of lower speed interface buffer or the shaper queue.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There is a potential misconfiguration because your actual throughput was greater than your expected throughput.&#xA;Double check your CIR (Mbps) setting.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Efficiency and Buffer Delay were within acceptable limits, but average throughput was lower than expected.  This could be due to the TCP Slow-Start duration.  Looking at the throughput graph will reveal if TCP Slow-Start (sloped throughput increase) is significant.&#xA;Try increasing the test time to reduce the effect of TCP Slow-Start in the measurement</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP retransmits were detected per RFC 6349 (TCP Efficiency %) and Buffer Delay % was low enough to suggest policing was the culprit.&#xA;TCP traffic is bursty and a policer will 'chop' traffic which exceeds the Committed Burst Size (CBS).&#xA;Consider shaping the traffic before sending into a network policer or increase the CBS size of the policer.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The measured throughput was higher than the ideal threshold, so the test passed.  However, TCP Efficiency was below 99.9% which may indicate network issues that could become a problem, especially as RTT increases.  Examples include poor cabling or duplex mismatch.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Large buffers are increasing RTT, which can negatively impacting TCP performance (high Buffer Delay %).&#xA;Network buffering can help performance up to a certain degree, but excessive network buffering will degrade TCP performance.&#xA;If traffic shaping is intended, you should increase the TCP Window size to fully employ the shaper (this requires re-running TrueSpeed in Troubleshoot mode).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of target.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Warning: Inconsistent RTT values detected. This usually means a TCP proxy intercepted the connection and might have skewed the test results.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is low due to TCP Re-Transmits. This usually indicates a problem and conveys packet loss and/or out-of-order TCP packets which (in the absence of excess load) may be caused by any of following:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Delay is high. Increasing Buffer Delay indicates the problem cause is probably one of the following:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput is less than 85% of the CIR partly because:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Warning: MTU Size below 1400 detected.  The tool calculates the Target TCP Throughput by subtracting TCP, IP, and Ethernet overhead from the path's discovered MTU size (PMTUD).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of order TCP packet arrival can cause TCP retransmissions even without packet loss - check LAG or load sharing.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Policer drops due to small policer burst size settings or misconfigured traffic shapers.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duplex mis-match at any Ethernet circuit in the end-to-end path causing CRCs and/or collisions.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Any other WAN or LAN errors, CRCs, overruns, etc. at any circuit in the end-to-end path.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet drops due to a misconfigured queue-limit at the CE router, PE router, Firewall, or other network appliance.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic congestion due to oversubscription at network aggregation points.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Offered test load exceeds effective available capacity due to an under-provisioned shaper.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Offered test load exceeds true available capacity due to an unexpected slower hop in the end-to-end path.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth is wasted by excessive protocol overhead relative to payload due to a small TCP MSS or MTU size (this will be evident in the report output).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>These settings are also applied to the Ethernet Encapsulation settings in the Voice stream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Version 3 utilizes a higher timestamp resolution resulting in more accurate measurements, and is recommended for use with newer test sets.  Version 2 can be used in conjunction with older test sets.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose whether or not to include Frame Delay as an SLA criteria.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose whether or not to include Delay Variation as an SLA criteria.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The combined length of the Maintenance Domain ID field and the Maintenance Association ID field may not exceed a total of 44 characters.  Modifying either field may thus result in the other being truncated in order to meet this requirement.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The WAN Source IP will be source of the Ping operation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The window size and the number of connections are calculated so the test can attain CIR traffic loads based on the measured baseline RTT latency.  These values can be scaled up or down by the percentage specified to account for fluctuation in network latency.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Injecting skew needs to be performed out of service and will cause an instantaneous traffic hit at the moment when the actual skew injection occurs.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The verdict is determined by comparing the average RTD with the Threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Confidence Level is the statistical probability that the actual measured Bit Error Rate is below the user specified Bit Error Rate Threshold.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the total propagation delay of the antenna and cabling system.  Use a value of 28 ns for the Taoglas AA.171.301111 antenna, or 45 ns for the Taoglas AA.162.301111 antenna.  Additional delay must be added if extension cables are used.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the time difference between the reference 1PPS IN 1 and 1 PPS IN 2 input.  Enter a positive value if the delay from IN 1 is greater than from IN 2.</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
