<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Ver Descargas</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Zoom +</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Zoom -</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>Reset Zoom</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>Descargando</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Abierto</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 minutos restantes</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 segundos restantes</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Descargas</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>Guardar como</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Directorio:</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nombre del Fichero:</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
</TS>
