<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Un usuario bloqueó este instrumento de prueba para evitar acceso no autorizado.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>No se requiere contraseña, presione Aceptar para desbloquear.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Desbloquear el instrumento de prueba?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Por favor, introduzca la contraseña para desbloquear:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Introduzca la contraseña</translation>
        </message>
    </context>
</TS>
