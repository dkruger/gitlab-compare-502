<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> 取り消してもよろしいですか?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>ﾌｧｲﾙ読み込み時にｴﾗー発生</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>ﾌｧｲﾙは多すぎるﾌﾚｰﾑ数を含みます (50,000 件以上 ) 。部分ﾊﾞｯﾌｧへのｾｰﾌﾞをお試し下さい。或いは USB ﾄﾞﾗｲﾌﾞに書き込み、異なるﾃﾞﾊﾞｲｽにﾛｰﾄﾞしてください。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>保存されたデータをすべて消去しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>選択されたアイテムを削除しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>その名前はすでに存在します。&#xA;古いデータを上書きしますか ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>ﾃﾞｨｽｸに書き込み中にｸﾞﾗﾌにｴﾗーが発生しました。作業を保持するには、ｸﾞﾗﾌをすぐに&#xA;保存してください。危機的なﾚﾍﾞﾙに達すると、ｸﾞﾗﾌは自動的に停止して消去されます。</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>多くのﾃﾞｨｽｸ ｴﾗーが発生したことにより、ｸﾞﾗﾌを続行できず、停止しました。&#xA;ｸﾞﾗﾌ化ﾃﾞｰﾀは消去されました。必要に応じて、ｸﾞﾗﾌを、再度、開始することができます。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>ﾘﾑｰﾊﾞﾌﾞﾙ ﾃﾞﾊﾞｲｽ</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>ﾛｸﾞが一杯です。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>無効な構成 :&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>無効</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>音声</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>ﾃﾞｰﾀ 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>ﾃﾞｰﾀ 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>SW でｻﾎﾟｰﾄされる CFP MSA Mgmt I/F 仕様改訂</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>SW でｻﾎﾟｰﾄされる QSFP MSA Mgmt I/F 仕様改訂</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>DMC 情報</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>ﾊﾞｰ ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>製造日</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>校正日</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>SW ﾘﾋﾞｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>ｵﾌﾟｼｮﾝ ﾁｬﾚﾝｼﾞ ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>ｱｾﾝﾌﾞﾘ ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>ｱｾﾝﾌﾞﾘ ﾊﾞｰ ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>ｲﾝｽﾄｰﾙされたｿﾌﾄｳｪｱ</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>ｵﾌﾟｼｮﾝ:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>ﾊﾟｹｯﾄを待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>ﾊﾟｹｯﾄの分析中 ...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>解析</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>ﾘﾝｸを待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>[ 分析開始 ] ﾎﾞﾀﾝを押すと開始します ...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>結果を保存しています。しばらくお待ちください。</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>結果の保存に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>結果が保存されます。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>待機中</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>前のﾃｽﾄはﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>前のﾃｽﾄはｴﾗーで中止されました</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>前のﾃｽﾄは失敗し、 Stop on failure ( 失敗時点で停止 ) が有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>実行されていません</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>残り時間: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>ﾃｽﾄはｴﾗーで完了できずに中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>ﾃｽﾄ完了</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>ﾃｽﾄの結果は不合格</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>ﾃｽﾄの結果はすべて合格</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>ﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>ﾃｽﾄはﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>停止中</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>ﾃｽﾄ開始していません</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>開始中</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>接続が検証されました</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>接続が切断されました</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>接続を検証中</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>無効な構成</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>結果を保存しています。しばらくお待ちください。</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>結果の保存に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>結果が保存されます。</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>例外</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ異常</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ異常</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾚｲﾃﾝｼｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼﾞｯﾀｰ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ CBS ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ CBS ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ CBS ﾎﾟﾘｼﾝｸﾞ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｰｽﾄ ﾊﾝﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>ｼｽﾃﾑ回復ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ ｼｽﾃﾑ復元ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>拡張負荷ﾃｽﾄ結果</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>ﾊﾟｽ MTU のｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>RTT ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸ ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのｳｨﾝﾄﾞｳ ｳｫｰｸのｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの TCP ｽﾙｰﾌﾟｯﾄ ｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>結果を保存しています。しばらくお待ちください。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>結果の保存に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>結果が保存されます。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>同期が得られません。すべてのｹｰﾌﾞﾙが取り付けられ、ﾎﾟｰﾄが正しく設定されているか確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>ｱｸﾃｨﾌﾞなﾘﾝｸを検出できませんでした。すべてのｹｰﾌﾞﾙが取り付けられ、ﾎﾟｰﾄが正しく設定されているか確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>接続したﾎﾟｰﾄの速度またはﾃﾞｭﾌﾟﾚｯｸｽを測定できませんでした。すべてのｹｰﾌﾞﾙが取り付けられ、ﾎﾟｰﾄが正しく設定されているか確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>ﾛｰｶﾙ ﾃｽﾄ ｾｯﾄの IP ｱﾄﾞﾚｽを取得できませんでした。通信設定を確認してから再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄとの通信ﾁｬﾈﾙを確立できません。通信設定を確認してから再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>ﾘﾓｰﾄ ﾃｽﾄ ｾｯﾄには BERT ｿﾌﾄｳｪｱの互換ﾊﾞｰｼﾞｮﾝがないと思われます。最低限の互換ﾊﾞｰｼﾞｮﾝは %1 です。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>TrueSpeed を実行するには、ﾛｰｶﾙ ﾕﾆｯﾄとﾘﾓｰﾄ ﾕﾆｯﾄのどちらでもﾚｲﾔ 4 TCP Wirespeed ｱﾌﾟﾘｹｰｼｮﾝが使用できる必要があります。</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>TrueSpeed ﾃｽﾄ選択は、このｶﾌﾟｾﾙ化の設定値としては無効のため、削除されます。</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>TrueSpeed ﾃｽﾄ選択は、このﾌﾚｰﾑ ﾀｲﾌﾟの設定値としては無効のため、削除されます。</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>TrueSpeed ﾃｽﾄ選択は、ﾘﾓｰﾄ ｶﾌﾟｾﾙ化には無効のため、削除されます。</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>SAM- 完了ﾃｽﾄ選択は、このｶﾌﾟｾﾙ化の設定値としては無効のため、削除されます。</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>無効な構成</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>CBS ﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>ﾎﾟﾘｼﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>ｽﾃｯﾌﾟ #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>ｽﾃｯﾌﾟ #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>ｽﾃｯﾌﾟ #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>ｽﾃｯﾌﾟ #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>ｽﾃｯﾌﾟ #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>ｽﾃｯﾌﾟ #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>ｽﾃｯﾌﾟ #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>ｽﾃｯﾌﾟ #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>ｽﾃｯﾌﾟ #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>ｽﾃｯﾌﾟ #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>結果を保存しています。しばらくお待ちください。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>結果の保存に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>結果が保存されます。</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>ﾛｰｶﾙ ARP に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>ﾘﾓｰﾄ ARP に失敗しました。</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> 表 , cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation> - ﾋｽﾄｸﾞﾗﾑは複数のﾍﾟｰｼﾞに分けられます</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>時間 ｽｹｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>利用不可</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>ﾕｰｻﾞ情報</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>構成 ｸﾞﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>結果 ｸﾞﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>ｲﾍﾞﾝﾄ ﾛｶﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>ﾋｽﾄｸﾞﾗﾑ</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>推定 TCP ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> 複数ﾃｽﾄのﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation>ﾃｽﾄﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 により生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 により生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 作成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Viavi ﾃｽﾄ機器で生成 </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> 合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>ｽｷｬﾝ 周波数 (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>合格 / 失格</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>ｾｯﾄｱｯﾌﾟ : ﾌｨﾙﾀ /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> ( ﾊﾟﾀｰﾝとﾏｽｸ )</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>ﾏｽｸ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Setup:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>適切な設定がありません ...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>無線</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>帯域</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>CPRI テストの全体結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>ﾃｽﾄは完了していません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>開始時間</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>いいえ.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>ｲﾍﾞﾝﾄ名</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>持続時間 / 値</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>拡張負荷ﾃｽﾄのｲﾍﾞﾝﾄ ﾛｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>不明 - ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>処理中です。お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>失敗 !</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>ｺﾏﾝﾄﾞ が完了しました</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>中止 !</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>ﾙｰﾌﾟ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Arm</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>解除</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>ﾊﾟﾜｰ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>ﾙｰﾌﾟ ｺﾏﾝﾄﾞの送信</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>ｽｲｯﾁ</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>ｽｲｯﾁ ﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>ｸｴﾘの発行</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ ｸｴﾘ</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>近端 Arm</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>近端 Disarm</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>ﾊﾟﾜｰ ｸｴﾘ</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>ｽﾊﾟﾝ ｸｴﾘ</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>ﾀｲﾑｱｳﾄ 無効</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>ﾀｲﾑｱｳﾄ ﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>連続 ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Stratum 1 Trace</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 ﾄﾗﾝｼﾞｯﾄ ﾉｰﾄﾞ ｸﾛｯｸ ﾄﾚｰｽ</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Stratum 2 Trace</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Stratum 3 Trace</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet Min Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Stratum 3E Trace</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provision by Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Don't Use for Synchronization</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Equipped Non-specific</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>TUG 構造</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>ﾛｯｸ TU</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>非同期 34M/45M</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>非同期 140M</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>ATM ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>MAN (DQDB) ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>FDDI ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>HDLC/PPP ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 解読済み</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>O.181 ﾃｽﾄ 信号</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>非同期</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>ﾋﾞｯﾄ 同期</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>ﾊﾞｲﾄ 同期</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT- 構造 STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>ﾛｯｸ VT ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>非同期 DS3 ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>非同期 DS4NA ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>非同期 FDDI ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>ﾌｫｰﾏｯﾄ ?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Out Of Range</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>履歴</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + 履歴</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>ｽﾍﾟｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>ﾏｰｸ</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>GREEN</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>黄色</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>RED</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>全て</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>REJECT</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>UNCERTAIN</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>了解</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>ﾃｽﾄ未完了</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>不明ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>識別済み</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>識別できません</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>不明を識別</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>残り %1 時間 %2 分</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>残り %1 分</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>過少</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>過大</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>( 過少 ) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>( 過大 ) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>値</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>ﾄﾘﾋﾞｭﾀﾘ ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>未定義</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未装備</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>開発中のﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC over SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>ｼﾝﾌﾟﾙ ﾃﾞｰﾀ ﾘﾝｸ ﾏｯﾋﾟﾝｸﾞ (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>HCLC/LAP-S ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>ｼﾝﾌﾟﾙ ﾃﾞｰﾀ ﾘﾝｸ ﾏｯﾋﾟﾝｸﾞ ( ｾｯﾄ - ﾘｾｯﾄ )</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>10 Gbps ｲｰｻﾈｯﾄ ﾌﾚｰﾑ ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>GFP ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>10 Gbps ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>予約 - 所有権</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>予約 - National</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>ﾃｽﾄ 信号 O.181 ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>予約 (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Equipped Non-Specific</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>非同期 DS3 ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>非同期 DS4NA ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>非同期 FDDI ﾏｯﾋﾟﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC Over SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 VT ﾍﾟｲﾛｰﾄﾞ 不良</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI 未割り当て。</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>TEI 待機</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>確立待機 TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI 割り当て済み</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>確立待機</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>相対待機</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>複数ﾌﾚｰﾑ推定</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>ﾀｲﾏ回復</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>不明ﾘﾝｸ</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>確立の待機中</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>マルチフレームを確立しました</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>発信音</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>一括ダイヤル</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>呼び出し中</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>接続完了</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>呼び出し解除</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>発声</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>テレテックス</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>ビデオテックス</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>スピーチ BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>ﾃﾞｰﾀ BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>ﾃﾞｰﾀ 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>検索中</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>使用可能</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>結合の要求</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>要求の再試行</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>残す</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Ack Burst ( 受信確認ﾊﾞｰｽﾄ ) が完了しました</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>結合の応答</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>ﾊﾞｰｽﾄが完了しました</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>ｽﾃｰﾀｽの応答</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>ｽﾄﾘｰﾑ内のﾎｰﾙを認識</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>ｴﾗｰ : ｻｰﾋﾞｽﾊﾞｯﾌｧーにまだ格納されていません</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>ｴﾗｰ : 再試行ﾊﾟｹｯﾄ要求が無効です</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>ｴﾗｰ : そのようなｻｰﾋﾞｽはありません</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>ｴﾗｰ : そのようなｾｸｼｮﾝはありません</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>ｴﾗｰ : ｾｯｼｮﾝ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>ｴﾗｰ : ｻﾎﾟｰﾄされていないｺﾏﾝﾄﾞとｺﾝﾄﾛｰﾙのﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>ｴﾗｰ : ｻｰﾊﾞーが満杯です</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>ｴﾗｰ : 重複した結合です</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>ｴﾗｰ : 重複したｾｯｼｮﾝ ID です</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>ｴﾗｰ : 不適切なﾋﾞｯﾄ ﾚｰﾄです</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>ｴﾗｰ : ｻｰﾊﾞーによりｾｯｼｮﾝが破棄されました</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>ｼｮｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>開く</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>ﾉｰﾏﾙ</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>逆</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>低すぎるﾚﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 bytes</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>選択済</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>未選択</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>ｴﾗｰﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>ﾛｽﾄﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>ｺｰﾄﾞ 違反</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>ｲﾍﾞﾝﾄ ﾛｸﾞがいっぱいです。</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>使用不可能</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>USB ｷーが見つかりません。もう一度挿入して、再度、試行してください。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>ﾍﾙﾌﾟはこの項目には提供されていません。</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>ﾕﾆｯﾄ ID</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>信号がありません</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>ｼｸﾞﾅﾙ</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>準備完了</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>使用</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>衛星 ID</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>GNSS ID</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>MTIE ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>TDEV ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>MTIE 結果</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>MTIE ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>TDEV 結果</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>TDEV ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Orig. TIE ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Offset rem. ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>MTIE/TDEV 曲線ｽﾀｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>ﾗｲﾝ + ﾄﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>ﾄﾞｯﾄ のみ</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>ﾏｽｸ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE 合格</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>MTIE 失敗</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV 合格</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>TDEV 失敗</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>監視 間隔 (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>計算中</translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>計算はｷｬﾝｾﾙされました</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>計算が終了しました</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>TIE ﾃﾞｰﾀ のｱｯﾌﾟﾃﾞｰﾄ中</translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>TIE ﾃﾞｰﾀ ﾛｰﾄﾞ済み</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>TIE ﾃﾞｰﾀ が ﾛｰﾄﾞ されていません</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>ﾜﾝﾀﾞ解析を実行するにはﾒﾓﾘが不十分です。 &#xA;256 MB が必要です。代わりに外部解析ｿﾌﾄを使用してください。 &#xA; 詳しくはﾏﾆｭｱﾙを参照してください。</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>周波数 ｵﾌｾｯﾄ (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>ﾄﾞﾘﾌﾄ ﾚｰﾄ (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>ｻﾝﾌﾟﾙ</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>ｻﾝﾌﾟﾙ ﾚｰﾄ ( 毎秒 )</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>ﾌﾞﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>現在 ﾌﾞﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>ｵﾌｾｯﾄの削除</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>曲線 選択</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>両 曲線</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Offs.rem.only</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄのｷｬﾌﾟﾁｬ</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Time (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>ﾗﾍﾞﾙなし</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>ﾎﾟｰﾄ 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>ﾎﾟｰﾄ 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>ﾂｰﾙ･ﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>一般 情報 :</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>ｸﾞﾗﾌ 無効</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>印刷ｴﾗｰ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps 最小</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps 最大</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCRｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCRｼﾞｯﾀ 最大</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID ｴﾗｰ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation>ｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ﾄﾗﾝｽﾎﾟｰﾄ ｽﾄﾘｰﾑ ID</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP あり</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>ﾊﾟｹｯﾄ 損失 合計</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>ﾊﾟｹｯﾄ 損失 現在</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>ﾊﾟｹｯﾄ 損失 ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 最大 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>OoS ﾊﾟｹｯﾄ数 合計 .</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS ﾊﾟｹｯﾄ数 現在</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS ﾊﾟｹｯﾄ数 最大</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>距離 ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>距離 ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>距離 ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>期間 ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>期間 ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>期間 ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>最大 損失 期間</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>最小 損失 距離</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>同期 損失 合計</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 現在</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 最大</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 現在</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 最大</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT ｴﾗｰ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation>解析 ｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>合計 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ﾄﾗﾝｽﾎﾟｰﾄ ｽﾄﾘｰﾑ ID</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ No</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ Mbps 現在</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ Mbps 最小 </translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ Mbps 最大</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP あり</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>ﾊﾟｹｯﾄ 損失 合計</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>ﾊﾟｹｯﾄ 損失 現在</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>ﾊﾟｹｯﾄ 損失 ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 最大 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS ﾊﾟｹｯﾄの合計</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS ﾊﾟｹｯﾄ数 現在</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS ﾊﾟｹｯﾄ数 最大</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>距離 ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>距離 ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>距離 ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>期間 ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>期間 ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>期間 ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>最大 損失 期間</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>最小 損失 距離 </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>同期 損失 合計</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 現在</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 最大</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 現在</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 最大</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>PCR ｼﾞｯﾀ 現在</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR ｼﾞｯﾀ 最大</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>CC ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>PMT ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID ｴﾗｰ 合計</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>PID ｴﾗｰ 現在</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID ｴﾗｰ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>ﾊﾟｹｯﾄﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>ﾊﾟｹｯﾄ 損失 最大</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation>ｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Mbps, 現在 L1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ Mbps 最小 </translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>ﾄﾗﾝｽﾎﾟｰﾄ ID</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>同期 損失</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>合計 同期 ﾊﾞｲﾄ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>合計 PAT ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>PAT ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCRｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>PCR ｼﾞｯﾀ 最大 </translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>合計 CC ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>合計 PMT ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>合計 PID ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID ｴﾗｰ 最大</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>合計 ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>ﾄﾗﾝｽﾍﾟｱﾚﾝｼ ｴﾗｰ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation>解析 ｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>合計 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ﾁｪｯｸｻﾑ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>MPEG 履歴</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP あり</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>ﾊﾟｹｯﾄ 損失 現在</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>ﾊﾟｹｯﾄ 損失 合計</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>ﾊﾟｹｯﾄ 損失 ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 現在</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 最大</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>ﾋﾟｰｸ - ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>ﾎﾟｼﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>ﾈｶﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>ﾋﾟｰｸ ﾋﾟｰｸ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>ﾎﾟｼﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>ﾈｶﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>ﾃｽﾄ ﾚﾎﾟｰﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>ﾊﾟｽ</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Chan</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>未使用</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>光全体ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>光ｾﾙﾌﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>設定 :</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>詳細</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>ｽﾀｯｸﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>ｽﾀｯｸﾄﾞ VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN ｽﾀｯｸの深さ</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>SVLAN %1 DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>SVLAN %1 User Priority</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>ﾌﾚｰﾑが定義されていません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>値</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>ﾄﾘﾋﾞｭﾀﾘ ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>設定:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>OTN ﾁｪｯｸ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>OTN ﾁｪｯｸ全体のﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>ﾃｽﾄは完了していません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>POH ﾊﾞｲﾄ･ｷｬﾌﾟﾁｬ</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>16 進</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>ﾊﾞｲﾅﾘ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>設定 :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>適切な設定がありません ...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>ｵｰﾊﾞｰﾍｯﾄﾞ ﾊﾞｲﾄ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>CDMA ﾚｼｰﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>印刷ｴﾗｰ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>ﾃｽﾄﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>PTP 全体ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP ﾁｪｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>ﾃｽﾄは完了していません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>ｻﾏﾘ</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>全ての 概要 結果 OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>拡張 FC ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>拡張 RFC 2544 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>全体ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>ﾃｽﾄ全体の結果</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>ﾚｲﾃﾝｼ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>ﾌﾚｰﾑﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>ｼｽﾃﾑ復元</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>拡張負荷</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>ﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>実行されていません - 前のﾃｽﾄはﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>実行されていません - 前のﾃｽﾄはｴﾗーで中止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>実行されていません - 前のﾃｽﾄは失敗し、 Stop on failure ( 失敗時点で停止 ) が有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>実行されていません</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>対称ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>対称のｱｯﾌﾟｽﾄﾘｰﾑとﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>非対称のｱｯﾌﾟｽﾄﾘｰﾑとﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>ﾚｲﾃﾝｼ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>ﾌﾚｰﾑﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>ﾊﾞｰｽﾄ (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>ｼｽﾃﾑ復元</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>拡張負荷</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>実行するﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>選択されたﾌﾚｰﾑ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>選択されたﾊﾟｹｯﾄ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>選択されたｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>選択されたｱｯﾌﾟｽﾄﾘｰﾑのﾊﾟｹｯﾄ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>選択されたﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>選択されたﾀﾞｳﾝｽﾄﾘｰﾑのﾊﾟｹｯﾄ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑのﾌﾚｰﾑ長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 ﾊﾞｲﾄ ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ｸﾞﾗﾌ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>中止時間</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>最長</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>最短</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>最後</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>中断数</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>ﾄｰﾀﾙ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Setup:</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>ｺﾈｸﾀ</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>公称波長 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>公称 ﾋﾞｯﾄ ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>波長 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>最小 ﾋﾞｯﾄ ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>最大 ﾋﾞｯﾄ ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>ﾊﾟﾜｰ ﾚﾍﾞﾙ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>ﾍﾞﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>ﾍﾞﾝﾀﾞ PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>ﾍﾞﾝﾀﾞ Rev</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>最大 Rx ﾚﾍﾞﾙ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>最大 Tx ﾚﾍﾞﾙ (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>ﾍﾞﾝﾀﾞｰ SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>日付ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>ﾛｯﾄ ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>診断 ﾓﾆﾀ</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>診断 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>ﾄﾗﾝｼｰﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW ﾊﾞｰｼﾞｮﾝ番号</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW 仕様書の改訂番号</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA 測定 I/F 改訂番号</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>ﾊﾟﾜｰｸﾗｽ</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx ﾊﾟﾜｰ ﾚﾍﾞﾙ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>最大ﾗﾑﾀﾞﾊﾟﾜｰ (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>ｱｸﾃｨﾌﾞなﾌｧｲﾊﾞ数</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>各ﾌｧｲﾊﾞの波長</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>ﾌｧｲﾊﾞ範囲別の WL (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>最大ﾈｯﾄﾜｰｸ ﾚｰﾝのﾋﾞｯﾄ ﾚｰﾄ (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>ｻﾎﾟｰﾄされているﾚｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>期間</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>無効</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>ﾌﾚｰﾑ遅延 (RTD、 ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>ﾌﾚｰﾑ遅延 (OWD ､ ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>一方向の遅延</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>行</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>設定:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>ﾁｭｰﾝｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>ﾄﾗﾌﾞﾙｼｭｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>対称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非対称</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>ｽﾙｰﾌﾟｯﾄの対称性</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>ﾊﾟｽ MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Throughput</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>ｱﾄﾞﾊﾞﾝｽﾄ TCP</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>実行するｽﾃｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>ﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>実行されていません - 前のﾃｽﾄはﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>実行されていません - 前のﾃｽﾄはｴﾗーで中止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>実行されていません - 前のﾃｽﾄは失敗し、 Stop on failure ( 失敗時点で停止 ) が有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>実行されていません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>ﾃｽﾄ未完了</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>ﾕｰｻﾞーによってﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>ﾃｽﾄが開始されていません。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ合格</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>ｽﾙｰﾌﾟｯﾄは目標の 90% を超えています。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ失敗</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>ｽﾙｰﾌﾟｯﾄは目標の 90% 未満です。</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ合格</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ失敗</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>ﾃｽﾄ ﾚﾎﾟｰﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>ﾃｸﾆｼｬﾝ名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>会社</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>電子ﾒｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>電話</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>ﾃｽﾄ識別</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>ﾃｽﾄ名</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Auth ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Auth 作成日</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>ﾃｽﾄ停止時間</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>ｺﾒﾝﾄ</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>中止時間</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>ﾄﾚｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>ｼｰｹﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Setup:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>ｽﾄﾘｰﾑ IP: ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>ｽﾄﾘｰﾑ名</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>中止時間</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ名</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>実行中</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>VLAN ｽｷｬﾝ全体のﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN ｽｷｬﾝ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失敗</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Setup:</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>結果 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>TrueSAM 全体ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>ﾃｽﾄはｴﾗーで完了できずに中止されました。ﾚﾎﾟｰﾄの結果は完全でない可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>ﾃｽﾄはﾕｰｻﾞーによって停止されました。ﾚﾎﾟｰﾄの結果は完全でない可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>ｻﾌﾞﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>J-QuickCheck ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>RFC 2544 ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>SAMComplete ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>J-Proof ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>TrueSpeed ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completed</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>ﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>実行されていません - 前のﾃｽﾄはﾕｰｻﾞーによって停止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>実行されていません - 前のﾃｽﾄはｴﾗーで中止されました</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>実行されていません - 前のﾃｽﾄは失敗し、 Stop on failure ( 失敗時点で停止 ) が有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>実行されていません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞ ( 連続 )</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>作業指示書</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>ｺﾒﾝﾄ / 注</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>機器</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>SAMComplete 全体ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>全体構成ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>全体ﾊﾟﾌｫｰﾏﾝｽ ﾃｽﾄの結果</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>ﾌﾚｰﾑﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>遅延変動</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>ﾚﾎﾟｰﾄ ｸﾞﾙｰﾌﾟ</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>ﾚﾎﾟｰﾄは作成できませんでした｡ </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>ﾃﾞｨｽｸ ｽﾍﾟｰｽが不足しています｡</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>ﾚﾎﾟｰﾄは作成されていません</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>ﾎﾟｰﾄの設定</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>奇数</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>偶数</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>ﾎﾞｰﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>ﾊﾟﾘﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>ﾌﾛｰ制御</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>ﾊｰﾄﾞｳｪｱ</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>ﾃﾞｰﾀﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>ｽﾄｯﾌﾟﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>ﾀｰﾐﾅﾙの設定</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>ｴﾝﾀｰ / ﾘﾀｰﾝｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>ﾛｰｶﾙｴｺｰ</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>ﾘｻﾞｰﾌﾞされたｷーを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>機能停止中のｷｰ</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>無効</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ｴｸｽﾎﾟｰﾄ､ﾌｧｲﾙ､ｾｯﾄｱｯﾌﾟ､結果､ｽｸﾘﾌﾟﾄ､開始 / 終了､ﾊﾟﾈﾙのｿﾌﾄｷｰ</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, ﾊﾟﾈﾙ上のｿﾌﾄｷｰ</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, RESULTS, SCRIPT, START/STOP, ﾊﾟﾈﾙ上のｿﾌﾄｷｰ</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>ﾌｧｲﾙ、ｾｯﾄｱｯﾌﾟ、結果、ｴｸｽﾎﾟｰﾄ、開始 / 終了、ﾊﾟﾈﾙのｿﾌﾄｷｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>ﾀｰﾐﾅﾙ&#xA;ｳｨﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>初期設定回復</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100 の&#xA;設定</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>ｽｸﾘｰﾝを&#xA;ｸﾘｱします</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>ｷｰﾎﾞｰﾄﾞを&#xA;表示します</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>ｷｰﾎﾞｰﾄﾞを&#xA;移動します</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>通信速度自動選択</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>ｽｸﾘｰﾝを&#xA;ｷｬﾌﾟﾁｬします</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>ｷｰﾎﾞｰﾄﾞを&#xA;非表示にします</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>ﾙｰﾌﾟ 進捗 :</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>結果 :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>予期せぬｴﾗｰに遭遇しました</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Run&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;これにより、ｽﾄﾘｰﾑについて、再度、ﾘﾝｸをｽｷｬﾝして、ﾃｽﾄを、再度、開始します。&#xA;&#xA;ｴｸｽﾌﾟﾛｰﾗｰ ｱﾌﾟﾘｹｰｼｮﾝから転送された&#xA;ｽﾄﾘｰﾑ以外も表示されます。&#xA;&#xA;続行しますか?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>ｵｰﾄ 進捗 :</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>詳細 :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>ｵｰﾄ 実行中。お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>自動は失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>自動は完了しました。</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>自動は中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>不明 - ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>検出中</translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>ｽｷｬﾝ中 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>予期せぬｴﾗｰに遭遇しました</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>ﾍﾟｱﾘﾝｸﾞされたﾃﾞﾊﾞｲｽの詳細</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>ﾌｧｲﾙの送信</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>接続</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>破棄</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>ﾌｧｲﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>送信</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>接続中...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>切断しています...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>切断</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>ﾊﾞﾝﾄﾞﾙ名 : </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>ﾊﾞﾝﾄﾞﾙ名の入力 :</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>証明書とｷｰ (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>無効なﾊﾞﾝﾄﾞﾙ名</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>"%1" という名前のﾊﾞﾝﾄﾞﾙはすでに存在します。 &#xA; 別の名前を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>ﾌｧｲﾙを追加</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>ﾊﾞﾝﾄﾞﾙの作成</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>ﾊﾞﾝﾄﾞﾙ名の変更</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>ﾊﾞﾝﾄﾞﾙの変更</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>ﾌｫﾙﾀﾞーを開く</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>新しいﾊﾞﾝﾄﾞﾙを追加 ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>証明書を %1 に追加 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>削除 %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>%2 から %1 を削除</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> BITS</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>仮想ﾚｰﾝ ID</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>注入ｽｷｭｰ ( ﾋﾞｯﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>注入ｽｷｭｰ (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>物理ﾚｰﾝ番号</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>範囲:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>日付を設定できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation>単位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>ﾄﾘﾋﾞｭﾀﾘ ｽﾛｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>適用</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>既定</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>ﾊﾞﾝﾄﾞ幅 </translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>変更はまだ適用されていません。</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>選択されたﾄﾘﾋﾞｭﾀﾘ ｽﾛｯﾄ数が多すぎます。</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>選択されたﾄﾘﾋﾞｭﾀﾘ ｽﾛｯﾄ数が少なすぎます。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>その他...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>7 文字</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 単位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>今日</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>日付の入力 : dd/mm/yyyy</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>日付の入力 : mm/dd/yyyy</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>以下のものまで</translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> bytes</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation>単位</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 単位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>分</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>dd/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>期間: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>[ 全てのｻｰﾋﾞｽ ] ﾀﾌﾞの宛先ｱﾄﾞﾚｽの構成</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>[ 全てのｽﾄﾘｰﾑ ] ﾀﾌﾞの宛先ｱﾄﾞﾚｽの構成</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>送信先 MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>宛先 ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>ﾙｰﾌﾟ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>この ﾎｯﾌﾟ 送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>次 ﾎｯﾌﾟ 宛先 IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>次 ﾎｯﾌﾟ ｻﾌﾞﾈｯﾄ ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>すべてのﾌﾚｰﾑの送信元ｱﾄﾞﾚｽはｲｰｻﾈｯﾄ ﾀﾌﾞで設定してください</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>送信元ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>ﾃﾞﾌｫﾙﾄ MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>ﾕｰｻﾞ MAC</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>[ 全てのｻｰﾋﾞｽ ] ﾀﾌﾞの送信元ｱﾄﾞﾚｽを構成する</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>[ 全てのｽﾄﾘｰﾑ ] ﾀﾌﾞの送信元ｱﾄﾞﾚｽを構成する</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>送信元 MAC</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN User Priority</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit 増加</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>ﾕｰｻﾞの SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ ﾓｰﾄﾞでは設定不可です。</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>VLAN ID の指定</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>ﾕｰｻﾞ優先度</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>長さ</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>ﾃﾞｰﾀ長 ( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>制御</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>ﾀｲﾌﾟ /&#xA; 長</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>ﾄﾝﾈﾙ &#xA; ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>B-Tag VLAN ID ﾌｨﾙﾀ</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>B-Tag 優先度</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>B-Tag DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>ﾄﾝﾈﾙ ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>ﾄﾝﾈﾙ 優先度</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>ﾄﾝﾈﾙ TTL</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC &#xA; ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>I-Tag 優先度</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>I-Tag DEI ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>I-Tag UCA ﾋﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>I-Tag ｻｰﾋﾞｽ ID ﾌｨﾙﾀ</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag ｻｰﾋﾞｽ ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>VC ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>VC 優先度</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA; ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>MPLS1 ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>MPLS1 優先度</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA; ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>MPLS2 ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>MPLS2 優先度</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>送信された顧客 ﾌﾚｰﾑ :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>ｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>顧客 ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>ﾕｰｻﾞ ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>ｱﾝﾀﾞｰｻｲｽﾞの ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>ｼﾞｬﾝﾎﾞ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>ﾃﾞｰﾀｾｸｼｮﾝは IP ﾊﾟｹｯﾄに含まれます IP ﾀﾌﾞで設定してください</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>[IP ﾌｨﾙﾀ ] ﾀﾌﾞのﾃﾞｰﾀ ﾌｨﾙﾀの構成</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Tx ﾍﾟｲﾛｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>RTD ｾｯﾄｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>ﾍﾟｲﾛｰﾄﾞ 解析</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Rx ﾍﾟｲﾛｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>LPAC ﾀｲﾏ</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Rx BERT ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Tx BERT ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>ﾕｰｻﾞ ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>入力ﾌﾚｰﾑの構成 :</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>送出ﾌﾚｰﾑの構成 :</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>長さ / ﾀｲﾌﾟ ﾌｨｰﾙﾄﾞは 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>ﾃﾞｰﾀ長はﾗﾝﾀﾞﾑです</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>User Priority Start Index</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>ﾌｧｲﾙ ﾀｲﾌﾟ :</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>ﾌｧｲﾙ名 :</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>本当に削除しますか &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>このﾌｫﾙﾀﾞ内の全てのﾌｧｲﾙを削除しますか ?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation>単位</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>以下のものまで</translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>範囲 :  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation>(hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 単位</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> は無効です。 - ﾎﾟｰﾄ 1 と 2 の送信元 IP は、 &#xA; 一致すべきではありません。前回の IP が復元されました。</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>無効 IP ｱﾄﾞﾚｽ &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>範囲 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>与えられた IP ｱﾄﾞﾚｽはこの設定には適切ではありません。&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>ﾊﾞｲﾄあたり 2 文字、最大 </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>ﾊﾞｲﾄあたり 2 文字、 </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>ﾙｰﾌﾟ - ｺｰﾄﾞ 名称</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>ﾋﾞｯﾄ ﾊﾟﾀｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>ﾃﾞﾘﾊﾞﾘ</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>ｲﾝﾊﾞﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>帯域外</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>ﾙｰﾌﾟ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>その他</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>ﾙｰﾌﾟ - ｺｰﾄﾞ 名称</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 BITS</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>追加 / 削除</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Tx ﾄﾚｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>範囲 :  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>ﾁｬﾝﾈﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>KLM 値の入力</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>範囲:</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG ﾒﾝﾊﾞの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>PSI ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>ﾊﾞｲﾄ値</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>ODU ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>ﾄﾘﾋﾞｭﾀﾘ ﾎﾟｰﾄ番号</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>範囲 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG ﾒﾝﾊﾞの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>範囲 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>ｵｰﾊﾞｰﾍｯﾄﾞ ﾊﾞｲﾄ ｴﾃﾞｨﾀ</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>編集する有効なﾊﾞｲﾄを選択してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>非認識ﾚﾝﾂﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>ﾍﾟｰｼﾞをﾋﾞﾙﾄﾞしています。  しばらくお待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>ﾍﾟｰｼﾞは空です。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>項目の追加</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>項目の修正</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>項目の削除</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>列の追加</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>列の修正</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>列の削除</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>負荷</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 未知のﾄﾚｰｻﾋﾞﾘﾃｨ</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未装備</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未装備</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未装備</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>未装備</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>長さ :  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>7 文字</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>長さ:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation>7 文字</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>現在</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>時間を入力してください : hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>全て非選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>ﾊﾞｲﾄ %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation>BITS</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>ｻｰﾋﾞｽ名</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>両方向</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>ｻｰﾋﾞｽ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>ﾌﾚｰﾑ長や CIR の変更により、ｻｰﾋﾞｽ ﾀｲﾌﾟはﾃﾞｰﾀにﾘｾｯﾄされました。</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>コーデック</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>ｻﾝﾌﾟﾘﾝｸﾞ ﾚｰﾄ (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>ｺｰﾙ数</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>圧縮</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>VCG の作成</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>ﾃﾞﾌｫﾙﾄﾁｬﾝﾈﾙで VCG ﾒﾝﾊﾞの定義 :</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Tx VCG の定義</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Rx VCG の定義</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>ﾍﾟｲﾛｰﾄﾞﾊﾞﾝﾄﾞ幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>ﾒﾝﾊﾞ数</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>ﾒﾝﾊﾞーを&#xA;配分する</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>VCG ﾒﾝﾊﾞーを配分する</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>VCG ﾒﾝﾊﾞーのｶｽﾀﾑ配分を定義する</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>ｲﾝｽﾀﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>ｽﾃｯﾌﾟ</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>VCG ﾒﾝﾊﾞの編集</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>ｱﾄﾞﾚｽ ﾌｫｰﾏｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>新規ﾒﾝﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>既定</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>ｴｸｽﾌﾟﾛｰﾗからｲﾝﾎﾟｰﾄしたｽﾄﾘｰﾑに参加します。 &#xA; 参加するには [ ｽﾄﾘｰﾑの参加 ...] ﾎﾞﾀﾝを押してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>ｱﾄﾞﾚｽﾌﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>選択しない</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>次の検索</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>選択しない</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>ｽﾄﾘｰﾑの参加 ...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>追加</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>入力した IP は ｱﾄﾞﾚｽ ﾌﾞｯｸ に追加されます。</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Join する</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>既に Join しています</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>参加</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Leave ｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>送信元 IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>宛先 IP ｱﾄﾞﾚｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Leave ｽﾄﾘｰﾑ ...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>全て非選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>ｸｲｯｸ 構成</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>注意 : これは現在のﾌﾚｰﾑ構成を無効にします。</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>ｸｲｯｸ (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>ﾌﾙ (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>強度</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>ﾌｧﾐﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>ｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit 増加</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>ﾕｰｻﾞ優先度</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (16 進 )  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>ﾕｰｻﾞｰ TPID (16 進 )</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>適用中</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>User Priority Start Index</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN User Priority</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>負荷...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>負荷</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>長い日付形式:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>短い日付形式:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>長い時間形式 :</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>短い時間形式 :</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>番号:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>未割り当て</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>割り当て済</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>予約済み 10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reserved11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reserved00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Save...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>ﾃｽﾄを ﾃﾞﾌｫﾙﾄ にﾘｾｯﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>ｽﾄﾘｰﾑを &#xA; 設定 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>負荷分配</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>全て&#xA;選択</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>全て &#xA; ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>自動 &amp;#xA;均等分配</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>ｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>負荷</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>ﾌﾚｰﾑ ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>ﾗｲﾝﾚｰﾄの比率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>ﾗﾝﾌﾟ  開始</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>ｺﾝｽﾀﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>最大 使用率 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>合計 (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>ﾄｰﾀﾙ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>ﾄｰﾀﾙ (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>合計 (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>注 : </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>1 つ以上のｽﾄﾘｰﾑが有効でなければなりません。</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>最大 使用率 しきい値は </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>最大 可能 負荷は </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>合計 指定された負荷はこれを超過できない。</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>ﾊﾟｰｾﾝﾄを入力してください :  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>ﾌﾚｰﾑ ﾚｰﾄを入力してください。  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>ﾋﾞｯﾄﾚｰﾄを入力してください e:  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>注 : &#xA; ﾋﾞｯﾄﾚｰﾄが検出されませんでした。 [ ｷｬﾝｾﾙ ] を押して &#xA; ﾋﾞｯﾄﾚｰﾄが検出されてから、再度、試行してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>ﾄﾘﾌﾟﾙ ﾌﾟﾚｲ ｻｰﾋﾞｽの定義</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>コーデック</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>ｻﾝﾌﾟﾘﾝｸﾞ ﾚｰﾄ &#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>ｺｰﾙ数</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>ｺｰﾙ ﾚｰﾄ &#xA;(Kbps) につき</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>合計 ﾚｰﾄ &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>合計 基本 ﾌﾚｰﾑ ｻｲｽﾞ &#xA;( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>無音圧縮</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>ｼﾞｯﾀｰ ﾊﾞｯﾌｧｰ (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>圧縮</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>ﾚｰﾄ(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>合計 基本 ﾌﾚｰﾑ ｻｲｽﾞ ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>開始 ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>負荷ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>時間 ｽﾃｯﾌﾟ ( 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>負荷 ｽﾃｯﾌﾟ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>合計 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>ｼﾐｭﾚｰﾄされた</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>ﾃﾞｰﾀ 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>ﾃﾞｰﾀ 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>ﾃﾞｰﾀ 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>ﾃﾞｰﾀ 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>ﾗﾝﾀﾞﾑ</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>音声</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>ﾋﾞﾃﾞｵ</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>ﾃﾞｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>注 :</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>1 つ以上のｻｰﾋﾞｽが有効でなければなりません。</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>最大 可能 負荷は </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>合計 指定された負荷はこれを超過できない。</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>配布：</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>ﾊﾞﾝﾄﾞ幅 : </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>構造 : </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>既定値</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>ｽﾃｯﾌﾟ</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>ｱﾄﾞﾚｽﾌﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>新規 ｴﾝﾄﾘ</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>送信元 IP (0.0.0.0 = "Any")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>必須</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>名前 ( 半角 16 文字以下 )</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>ｴﾝﾄﾘ の追加</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>ｲﾝﾎﾟｰﾄ / ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>ｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>ｴﾝﾄﾘの USB ﾄﾞﾗｲﾌﾞからのｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>USB ﾄﾞﾗｲﾌﾞへのｴﾝﾄﾘのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>全て削除</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>保存して閉じます</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>ｵﾌﾟｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>ｴﾝﾄﾘの USB からのｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>ｴﾝﾄﾘのｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Comma Separated (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>ｴﾝﾄﾘの追加</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>すべてのｴﾝﾄﾘはｶﾝﾏで区切られた : &lt; 送信元 IP>,&lt; 送信先 IP>,&lt;PID>,&lt; 名前 > の 4 つのﾌｨｰﾙﾄﾞが必要です</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>次へ</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>送信元 IP ｱﾄﾞﾚｽが間違っています</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>送信先 IP ｱﾄﾞﾚｽが間違っています</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>PID が間違っています</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>ｴﾝﾄﾘには名前が必要です ( 半角 16 文字以下 )</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>ｴﾝﾄﾘ名は 16 文字 ( 半角 ) 以下にしてください</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>同じ送信元 IP 、送信先 IP 、 PID&#xA; が存在します</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>既にあるｴﾝﾄﾘ &#xA; を書き換えますか？もしくは &#xA; のｲﾝﾎﾟｰﾄをｽｷｯﾌﾟしますか？</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>ｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>上書き</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>ｲﾝﾎﾟｰﾄされたｴﾝﾄﾘの 1 つは PID 値を持ちます。 PID 値を持ったｴﾝﾄﾘ &#xA; は MPTS で使用されます</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>ｴﾝﾄﾘの USB へのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>ｴﾝﾄﾘのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>IPTV ｱﾄﾞﾚｽﾌﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>既に使用されています。異なった名前を選ぶか、または既存のｴﾝﾄﾘを編集してください。</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>ｴﾝﾄﾘとﾊﾟﾗﾒｰﾀは既に存在しています</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>すべてのｴﾝﾄﾘ名を削除しますか？</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>ｴﾝﾄﾘの追加</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>同じ名前が存在しています</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>何を実行しますか？</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>日付 </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>時間 :</translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細なｲﾍﾞﾝﾄ･ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>ｵﾝ</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Chan</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>ﾊﾟｽ</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細な K1/K2 ﾊﾞｲﾄ･ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>16 進</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>ﾊﾞｲﾅﾘ</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>他の [ 一方向の遅延ｲﾍﾞﾝﾄ ] ﾃﾞｰﾀを表示するには、 [ 表示 ]->[ 結果ｳｨﾝﾄﾞｳ ]->[ 単一ﾒﾆｭーの選択項目 ] を使用してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>送信元 損失</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>B1 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>REI-L ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>MS-REI ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>B2 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>REI-P ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>B2 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>HP-REI ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>B3 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>REI-V ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>BIP-V ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>B3 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>LP-REI ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>LP-BIP ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>STL FAS ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Bit/TSE ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>信号 損失</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>ﾌﾚｰﾑ 同期 損失</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>ﾌﾚｰﾑ ﾜｰﾄﾞ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>FAS ｴﾗｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>最長</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>最短</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>最後</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>中断数</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細なｻｰﾋﾞｽ中断ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>ﾄｰﾀﾙ</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細なｻｰﾋﾞｽ中断ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>不合格</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>期間 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>ｵｰﾊﾞｰﾌﾛｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細な呼び出し結果ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>ﾀﾞｲﾔﾙ･ﾄｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  TRUE</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>期間</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>無効</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>日付 </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>時間 :</translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>期間 (ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ名</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>期間 (ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>ｽﾄﾘｰﾑ IP: ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>ｽﾄﾘｰﾑ名</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>期間 (ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>保存されたｶｽﾀﾑの結果ｶﾃｺﾞﾘーをｴｸｽﾎﾟｰﾄする</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>保存されたｶｽﾀﾑ結果ｶﾃｺﾞﾘｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>USB ﾌﾗｯｼｭ ﾄﾞﾗｲﾌﾞが見つかりません。 &#xA; ﾌﾗｯｼｭ ﾄﾞﾗｲﾌﾞを挿入するか、抜き差ししてください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>USB への PTP ﾃﾞｰﾀのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>PTP データ ファイル (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>USB へのﾚﾎﾟｰﾄのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>ﾃｷｽﾄ (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>USB へのｽｸﾘｰﾝ ｼｮｯﾄのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>保存されたｽｸﾘｰﾝｼｮｯﾄ (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>選択されたﾌｧｲﾙを次の名前で Zip 処理 :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>ﾌｧｲﾙ名を入力してください : 最大 60 文字（半角の場合）</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Zip&#xA;&amp;&amp; ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Zip ﾌｧｲﾙの名前を入力してください</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>USB ﾌﾗｯｼｭ ﾄﾞﾗｲﾌﾞが見つかりません。 &#xA; ﾌﾗｯｼｭ ﾄﾞﾗｲﾌﾞを挿入するか、抜き差ししてください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>ﾌｧｲﾙを Zip 処理できません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>USB への TIE ﾃﾞｰﾀのｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Wander TIE ﾃﾞｰﾀ ﾌｧｲﾙ (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>タイミング データを USB にエクスポート</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>すべてのタイミング データ ファイル (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>ﾌｧｲﾙ ﾀｲﾌﾟ :</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>開く</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>保存したｶｽﾀﾑ結果ｶﾃｺﾞﾘを USB からｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>保存されたｶｽﾀﾑ結果ｶﾃｺﾞﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>ｲﾝﾎﾟｰﾄ &#xA; ｶｽﾀﾑｶﾃｺﾞﾘｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>USB からﾚﾎﾟｰﾄ ﾛｺﾞをｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>ｲﾒｰｼﾞ ﾌｧｲﾙ (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>ﾛｺﾞ の &#xA; ｲﾝﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>USB からｸｲｯｸ ｶｰﾄﾞをｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Pdf ﾌｧｲﾙ (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>ｸｲｯｸｶｰﾄﾞを &#xA; ｲﾝﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>ﾃｽﾄ の &#xA; ｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Zip 解除&#xA; &amp;&amp; ｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>ｴﾗｰ - いずれかのﾌｧｲﾙの TAR は解除できません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>宛先ﾃﾞﾊﾞｲｽ上で空きｽﾍﾟｰｽが不十分です。 &#xA; ｺﾋﾟｰ操作は中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>ﾌｧﾙのｺﾋﾟｰ中 ...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>終了しました。ﾌｧｲﾙがｺﾋﾟーされました。</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>ｴﾗｰ : 次の項目をｺﾋﾟーできませんでした。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>PTP データの保存</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>PTP ファイル (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>ｺﾋﾟｰ</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>ディスク容量が不十分です。保存した他のファイルを削除するか、USB にエクスポートしてください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>空き容量: %2中、%1</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>ﾌｧｲﾙが選択されていません</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>合計 %1 個、選択されたﾌｧｲﾙ内</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>合計 %1 個、 %2 個の選択されたﾌｧｲﾙ内</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>ﾌｧｲﾙ名 :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>ﾌｧｲﾙ名を入力してください : 最大 60 文字（半角の場合）</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>このﾌｫﾙﾀﾞ内の全てのﾌｧｲﾙを削除しますか？</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（読み取り専用のﾌｧｲﾙは削除されません。）</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>ﾌｧｲﾙを削除中です．．．</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>本当に削除しますか &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>この項目を本当に削除しますか ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>推奨される光ﾓｼﾞｭｰﾙではありません</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>ｲｰｻﾈｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>信号構造の所見とｽｷｬﾝの結果表示。</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>以下の項目で並べ替え：</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>並び替え</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>並べ替えなおし</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>選択されたﾁｬﾈﾙの解析に使用できるﾃｽﾄの一覧を表示します。</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>ﾃｽﾄ開始</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>少々お待ちください．．．選択されたﾁｬﾈﾙの設定中です．．．</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>詳細の表示</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>詳細の非表示</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>範囲:%1 から %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>使用不可能</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>検出</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>ｵﾘｼﾞﾅﾙ</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>幅に合わせる</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>高さに合わせる</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄ ﾋﾞｭーの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>詳細</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(続き)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>注意</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>ﾛｸﾞ･ﾊﾞｯﾌｧがいっぱい</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>ｷｬﾌﾟﾁｬを中止しました。</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>列の編集</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>ﾊﾞｲﾄの選択 : </translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄを撮ることが出来ません</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>ﾃﾞｨｽｸ･ｽﾍﾟｰｽ不足</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>ｷｬﾌﾟﾁｬ済みｽｸﾘｰﾝｼｮｯﾄ : </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>ｽﾄﾘｰﾑ について</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>DP ﾀﾞｲﾔﾙ</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>MF ﾀﾞｲﾔﾙ</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>DTMF ﾀﾞｲﾔﾙ</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>ﾘｻﾞｰﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>ﾕﾆﾀﾞｲﾚｸｼｮﾅﾙ</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>ｱｲﾄﾞﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>ｴｷｽﾄﾗ ﾄﾗﾌｨｯｸ</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>合計</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>利用できるものはありません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>外部 ﾘﾝｸ へ進めません</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>未検出</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>ﾍﾟｰｼﾞの最後に到達しました。ﾄｯﾌﾟから継続します。</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>ﾂｰﾙの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>ｽｸﾘﾌﾟﾄを開始する前に、自動ﾚﾎﾟｰﾄを OFF にします。</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>以前に表示された自動ﾚﾎﾟｰﾄを ON にします。</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>作成</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>既定</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>全て削除</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>読込み</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>送信</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>再試行</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>表示</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS ( ｼﾝｸ )</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS ( ｿｰｽ )</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>ｺﾝﾃﾅ</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>ﾁｬﾝﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>信号 ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>ﾄﾚｰｽ</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>ﾄﾚｰｽ ﾌｫｰﾏｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>不明</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>これは現在のﾚﾍﾞﾙのみを表し、より低次、高次のﾁｬﾈﾙは考慮に入っていません。現在選択されているﾁｬﾈﾙのみがﾗｲﾌﾞのｱｯﾌﾟﾃﾞｰﾄを受けます。</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>ｱｲｺﾝで表されているﾁｬﾈﾙのｽﾃｰﾀｽ。</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>ﾓﾆﾀーされたｱﾗｰﾑ無し</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>ｱﾗｰﾑが存在します</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>ﾓﾆﾀーされたｱﾗｰﾑ無しでのﾓﾆﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>ｱﾗｰﾑ付きでﾓﾆﾀーする</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>ﾁｬﾈﾙのｺﾝﾃﾅーの名前。'c' のｻﾌｨｯｸｽは、連結されたﾁｬﾈﾙを示します。</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>RFC 4606 で指定の N KLM 数のﾁｬﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>ﾁｬﾈﾙの信号ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>このﾁｬﾈﾙの最近の既知のｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>ﾁｬﾈﾙは無効です。</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI あり</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS あり</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP あり</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>ﾓﾆﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>以下でｽﾃｰﾀｽがｱｯﾌﾟﾃﾞｰﾄされました：</translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>無し</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>ﾁｬﾈﾙのﾄﾚｰｽ情報</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>ﾁｬﾈﾙのﾄﾚｰｽ ﾌｫｰﾏｯﾄ情報</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>ｻﾎﾟｰﾄされていません</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>ｽｷｬﾝ中</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>ｱﾗｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>無効</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>未ﾌｫｰﾏｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>ｼﾝｸﾞﾙ ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>CR/LF 終端</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>ペア済みﾃﾞﾊﾞｲｽ</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>検出されたﾃﾞﾊﾞｲｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>CA 証明書</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>ｸﾗｲｱﾝﾄ証明書</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>ｸﾗｲｱﾝﾄ ｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>ﾌｫｰﾏｯﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>空き領域</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>合計の容量</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>ﾍﾞﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>ﾗﾍﾞﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>これからｱｯﾌﾟｸﾞﾚｰﾄﾞするﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>ｲﾝｽﾄｰﾙされたﾊﾞｰｼﾞｮﾝ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>ｶﾃｺﾞﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>ｶｽﾀﾑｶﾃｺﾞﾘーにつけた名前の一覧を表示します。名前をｸﾘｯｸするとそのｶｽﾀﾑｶﾃｺﾞﾘーが有効 / 無効になります。</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>ｸﾘｯｸするとｶｽﾀﾑｶﾃｺﾞﾘーの設定が出来るようになります。</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>削除するｶﾃｺﾞﾘーの選択 / 非選択の切り替えでｶｽﾀﾑｶﾃｺﾞﾘーの削除が出来るようにします。</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>ｶｽﾀﾑｶﾃｺﾞﾘーにつけた名前。名前をｸﾘｯｸするとそのｶｽﾀﾑｶﾃｺﾞﾘーが有効 / 無効になります。</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>設定ｱｲｺﾝをｸﾘｯｸするとｶｽﾀﾑｶﾃｺﾞﾘーの設定用のﾀﾞｲｱﾛｸﾞが現れます。</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>削除ｱｲｺﾝをｸﾘｯｸすると削除するｶｽﾀﾑｶﾃｺﾞﾘーのﾁｪｯｸを付け外しできます。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>ﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>ﾃｷｽﾄ (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>作成</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>ﾌｫｰﾏｯﾄ :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>ﾚﾎﾟｰﾄを表示</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>ｴﾗｰ - ﾌｧｲﾙ名を空白にすることはできません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>設定を変更すると、現在の結果は更新されます。</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IPｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MACｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>ｻﾌﾞﾈｯﾄ内になし</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>ﾘﾝｸがｱｸﾃｨﾌﾞになるのを待機中...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>DHCP を待機中...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>発信元 IP の再設定中...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>送信元 IP</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IPｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MACｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>ｼｽﾃﾑ名</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>ｻﾌﾞﾈｯﾄ上になし</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IPｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MACｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>ｻﾌﾞﾈｯﾄ内になし</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IPｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MACｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>ｻｰﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>ｻﾌﾞﾈｯﾄ内になし</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MACｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>ｻｰﾋﾞｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 名</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>ﾃﾞｨｽｶﾊﾞﾘ</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>VLAN優先</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>ﾃﾞﾊﾞｲｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>ﾈｯﾄﾜｰｸ IP</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>ﾃﾞﾊﾞｲｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 名</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>ﾃﾞﾊﾞｲｽ</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>IP ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>ﾄﾞﾒｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>ﾎｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>ｽｲｯﾁ</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>ﾙｰﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>設定 </translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>ｲﾝﾌﾗｽﾄﾗｸﾁｬ</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>ｺｱ</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>配布：</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>ｱｸｾｽ</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>ﾃﾞｨｽｶﾊﾞﾘ</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>検出された IP ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>検出された NetBIOS ﾄﾞﾒｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>検出された VLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>検出されたﾙｰﾀｰ</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>検出されたｽｲｯﾁ</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>検出されたﾎｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>検出されたｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>ﾈｯﾄﾜｰｸ検出ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>ﾚﾎﾟｰﾄは作成されていません</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>ﾃﾞｨｽｸ･ｽﾍﾟｰｽ不足</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 により生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 により生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 により生成</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Viavi ﾃｽﾄ機器で生成 </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>一般</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>時間</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>ﾃﾞｨｽｸが一杯で、全てのｸﾞﾗﾌが中止しています。 &#xA; ｽﾍﾟｰｽを開放し、ﾃｽﾄを再開してください。 &#xA;&#xA; あるいは、ﾒﾆｭｰﾊﾞｰの [ ﾂｰﾙ ]>[ ｶｽﾀﾏｲｽﾞ ] から &#xA; ｸﾞﾗﾌを無効にできます。</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>時間ｽｹｰﾙを中央にするにはﾀｯﾌﾟしてください</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>ｸﾞﾗﾌ ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>最大</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>ｳｲﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>飽和ｳｨﾝﾄﾞｳ</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>ﾌﾟﾛｯﾄ ﾃﾞｰﾀの保存</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>送信先のﾃﾞﾊﾞｲｽに十分な空き領域がありません。</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>USB ﾌﾗｯｼｭ ﾃﾞﾊﾞｲｽが挿入されている場合、直接 USB にｴｸｽﾎﾟｰﾄできます。</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>ｸﾞﾗﾌ ﾃﾞｰﾀを保存します。 この処理には時間がかかる場合があります。しばらくお待ち下さい。</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>ｸﾞﾗﾌ ﾃﾞｰﾀの保存</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>ﾃﾞﾊﾞｲｽに十分な空き領域がありません。 ｴｸｽﾎﾟｰﾄされたｸﾞﾗﾌ ﾃﾞｰﾀが不完全です。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>ｸﾞﾗﾌ ﾃﾞｰﾀのｴｸｽﾎﾟｰﾄ中にｴﾗーが発生しました。 ﾃﾞｰﾀは完全でない可能性があります。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>ｽｹｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 日</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 時間</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 時間表示</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 分</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 分</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 秒</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>ﾃﾞｰﾀのﾌﾟﾛｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>拡大するには ﾀｯﾌﾟ ﾄﾞﾗｯｸﾞしてください</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>飽和</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>ｳｲﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>接続</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Time (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>ﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Src ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>宛先ﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>削除...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>確認．．．</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>選択されたｶﾃｺﾞﾘーは現在実行中の全てのﾃｽﾄから削除されます。</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>選択したｱｲﾃﾑを削除してよろしいですか？</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>新規．．．</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>新しいｶｽﾀﾑｶﾃｺﾞﾘーを設定するためのﾀﾞｲｱﾛｸﾞを開く。</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>これを押すと、ｶｽﾀﾑｶﾃｺﾞﾘーを装置からの削除のために印付けできます。ﾌｧｲﾙ削除の選択終了時に再度このﾎﾞﾀﾝを押してください。</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>「%1」を押して&#xA;を開始してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>ｶｽﾀﾑ 結果 ｶﾃｺﾞﾘの構成</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>選択された結果のうち '*' が付いている結果は、現在のﾃｽﾄ構成に適用されず、結果ｳｨﾝﾄﾞｳに表示されません。</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>ｶﾃｺﾞﾘー名</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>ｶﾃｺﾞﾘー名を入力してください：（最大%1文字）</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>ﾌｧｲﾙ名::</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>適用外</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>名前を付けて保存する</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>新たに保存する</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>ｶﾃｺﾞﾘｰ%2を含む &#xA; %1ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>選択 結果 : </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   ( 最大 選択 </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>構成 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>ｻﾏﾘ</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>ｻﾏﾘ</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>最大 (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>値 (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>ﾍﾙﾌﾟ toolButton ﾎｰﾑ ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>ﾍﾙﾌﾟ toolButton 後方 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>ﾍﾙﾌﾟ toolButton 前方 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>ﾍﾙﾌﾟ toolButton 末尾 ...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>呼び出し履歴なし</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>ﾍﾙﾌﾟ toolButton ﾎｰﾑ ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>ﾍﾙﾌﾟ toolButton 後方 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>ﾍﾙﾌﾟ toolButton 前方 ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>ﾍﾙﾌﾟ toolButton 末尾 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>ﾋﾟｰｸ - ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>ﾎﾟｼﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>ﾈｶﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>ﾋﾟｰｸ - ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>ﾎﾟｼﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>ﾈｶﾞﾃｨﾌﾞ ﾋﾟｰｸ</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>ﾚｲﾃﾝｼｰ (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>ｶｳﾝﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>結果 &#xA; 入手不可</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>ｽﾃｰﾀｽ : 合格</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>ｽﾃｰﾀｽ : 不合格</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>ｽﾃｰﾀｽ : N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>展開してﾌｨﾙﾀ ｵﾌﾟｼｮﾝを表示</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>MEP 検出数</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>ﾋﾟｱとして設定</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>表示のﾌｨﾙﾀﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>ﾌｨﾙﾀ ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>ﾌｨﾙﾀ値を入力してください :%1 文字以内</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>CCM ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>CCM ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>ﾋﾟｱ MEP Id</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>管理ﾄﾞﾒｲﾝ ﾚﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>ﾄﾞﾒｲﾝ ID の指定</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ﾒﾝﾃﾅﾝｽのﾄﾞﾒｲﾝ ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>ﾒﾝﾃﾅﾝｽのｱｿｼｴｰｼｮﾝ ID</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>構成されたﾃｽﾄ ｾｯﾄ。強調表示された行は、このﾃｽﾄ ｾｯﾄのﾋﾟｱ MEP に設定されています。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>ﾃｽﾄ ｾｯﾄを強調表示されたﾋﾟｱ MEP に設定できませんでした。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>%1 を %2 に設定できません。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>結果 &#xA; 入手不可</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>ｸﾞﾗﾌ作成 &#xA; 無効</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>この結果ｳｲﾝﾄﾞｳをﾌﾙｽｸﾘｰﾝに切替えます。</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>ﾚｰﾄ用できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>有効な &#xA; 結果なし</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>ｶｽﾀﾑ</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>全て</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ｱｯﾌﾟｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾀﾞｳﾝｽﾄﾘｰﾑ ﾌﾚｰﾑ損失ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ｱｯﾌﾟｽﾄﾘｰﾑ ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 ﾊﾞｲﾄ ﾀﾞｳﾝｽﾄﾘｰﾑ ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ結果</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>ﾃｷｽﾄ ﾌｧｲﾙのｴｸｽﾎﾟｰﾄ ...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>ｴｸｽﾎﾟｰﾄﾛｸﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>ｽﾄﾘｰﾑ &#xA; 詳細</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>この ｳｲﾝﾄﾞｳ内の全ての結果ﾂﾘｰを折りたたむ</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>このｳｲﾝﾄﾞｳの全ての結果ﾂﾘｰを展開する</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>結果 &#xA; ｶﾃｺﾞﾘ &#xA; は空</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>結果 &#xA; 入手不可</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>結果 &#xA; 入手不可</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>全ての 概要 &#xA; 結果 &#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>ｶﾗﾑ</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>列の表示</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>全て非選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>ｴﾗｰのみ表示</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>ｶﾗﾑ ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>ｴﾗーのある&#xA;行のみを&#xA;表示する</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>ｴﾗーのある行のみを表示する</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>ﾄﾗﾌｨｯｸ ﾚｰﾄ (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>ﾄﾗﾌｨｯｸ速度 (L1 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation>分析済みｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>ﾄﾗﾌｨｯｸ分類基準</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>合計ﾘﾝｸ</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>ｽﾄﾘｰﾑ 1 ～ 128 を表示</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>追加のｽﾄﾘｰﾑ > 128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>ｶﾗﾑ ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>ﾎｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>遅延 (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IPｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>詳細なﾄﾚｰｽﾙｰﾄ･ﾃﾞｰﾀを表示するには、 [ 表示 ] ﾒﾆｭーの [ 結果ｳｨﾝﾄﾞｳ ] をﾎﾟｲﾝﾄし、 [ ｼﾝｸﾞﾙ ] をｸﾘｯｸします。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>理想的な転送時間</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>実際の転送時間</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>ｸﾞﾙｰﾌﾟ：</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>ｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>ﾌﾟﾛｸﾞﾗﾑ</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>ﾊﾟｹｯﾄﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>距離 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>期間 ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>同期 ﾊﾞｲﾄ ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>ｴﾗｰ ﾌﾟﾛｸﾞﾗﾑのみ表示</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>ｶﾗﾑ ...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>合計 ﾌﾟﾛｸﾞﾗﾑ Mbps</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>ｴﾗｰ ﾌﾟﾛｸﾞﾗﾑ &#xA; のみ表示</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>ｴﾗｰ ﾌﾟﾛｸﾞﾗﾑのみ表示</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>ｴﾗｰ &#xA; ｽﾄﾘｰﾑ &#xA; のみ表示</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>ｴﾗｰ ｽﾄﾘｰﾑのみ表示</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>解析</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名前</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation>解析 &#xA; ｽﾄﾘｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>合計 &#xA;L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>IP ﾁｪｯｸｻﾑ &#xA; ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>UDP ﾁｪｯｸｻﾑ~ ｴﾗｰ数</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>ｱﾅﾗｲｻﾞ &#xA; 起動</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>ｶﾗﾑ ...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>お待ちください .. ｱﾅﾗｲｻﾞｱﾌﾟﾘｹｰｼｮﾝを設定しています</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>時間表示</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>ｽｸﾘﾌﾟﾄを選択してください</translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>ｽｸﾘﾌﾟﾄ :</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>状態 :</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>現在の状態</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>ﾀｲﾏｰ :</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>ｽｸﾘﾌﾟﾄ 経過時間 :</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>ﾀｲﾏの量</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>出力 :</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>ｽｸﾘﾌﾟﾄ完了 :   </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>ｽｸﾘﾌﾟﾄ &#xA; 選択</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>ｽｸﾘﾌﾟﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>出力 &#xA; ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>ｽｸﾘﾌﾟﾄ中止</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>実行中 ...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>ｽｸﾘﾌﾟﾄ 経過時間 :</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>別のｽｸﾘﾌﾟﾄを選択してください。 </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>ｴﾗｰ.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>実行中 .</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>実行中 ..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>テスト リストのカスタマイズ</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>起動時に結果を表示</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>上に移動</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>下に移動</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>全て削除</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>名前の変更</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>ｾﾊﾟﾚｰﾀ</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>ｼｮｰﾄｶｯﾄを追加</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>保存されたﾃｽﾄを追加</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>すべてのお気に入りを削除しますか。</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>お気に入りﾘｽﾄはﾃﾞﾌｫﾙﾄです。</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>個別のお気に入りはすべて削除され、この機器のﾃﾞﾌｫﾙﾄ ﾘｽﾄに復元されます。    続行しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>ﾃｽﾄ構成 (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄ構成 (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>保存されたﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>お待ちください…&#xA;空のテスト ビューを起動しています</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>ピン テスト</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>ピン テストの名前を変更</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>テストするピン リスト</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>テスト構成の保存</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>ﾃｽﾄ 名 :</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>表示名を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>このﾃｽﾄは %1 と同じです</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>これはﾃｽﾄ ｱﾌﾟﾘｹｰｼｮﾝを起動するためのｼｮｰﾄｶｯﾄです。</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>説明 : %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>これは保存されたﾃｽﾄ構成です。</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>ﾌｧｲﾙ名 :%1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>置換</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>ﾃｽﾄを開始できません ...&#xA; ｵﾌﾟｼｮﾝは期限切れです。 &#xA; 終了して、ｼｽﾃﾑ ﾍﾟｰｼﾞから BERT を再起動してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>このﾃｽﾄはすぐに開始できません。    現在のﾊｰﾄﾞｳｪｱ構成でｻﾎﾟｰﾄされていないか、ﾘｿｰｽが不足している可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>他のﾀﾌﾞで実行しているﾃｽﾄの削除を試行します。</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>このﾃｽﾄは他のﾎﾟｰﾄで実行中です。    ﾃｽﾄを実行しますか ? ( ﾀﾌﾞ %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>他のﾃｽﾄ ( ﾀﾌﾞ %1) を、選択されたﾃｽﾄに再構成することもできます。    ﾃｽﾄを再構成しますか ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>ﾘｽﾄは空です。</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>ﾃｽﾄを開始できません ...&#xA; 光ｼﾞｯﾀー機能が OFF になっています。 &#xA;[ ﾎｰﾑ / ｼｽﾃﾑ ] ﾍﾟｰｼﾞから光ｼﾞｯﾀー機能を起動してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>ﾃｽﾄを開始できません ...&#xA; ﾟﾜｰ ﾘｿｰｽが利用できません。 &#xA; 既存ﾃｽﾄを除去または再構成してください。 &#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>ﾃｽﾄを開始できません ...&#xA; ﾊﾟﾜｰ ﾘｿｰｽが利用できません。 &#xA; 別のﾓｼﾞｭｰﾙを解除するか、既存ﾃｽﾄを &#xA; 除去または再構成してください。 &#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>お待ちください ...&#xA; ﾃｽﾄの追加中</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>お待ちください ...&#xA; ﾃｽﾄの再設定を行っています</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>ﾃｽﾄを開始できません...&#xA;必要なﾘｿｰｽが使用中の可能性があります。&#xA;&#xA;ﾃｸﾆｶﾙ ｻﾎﾟｰﾄにお問い合わせください。&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>UI ｵﾌﾞｼﾞｪｸﾄの構築中</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>UI とｱﾌﾟﾘｹｰｼｮﾝ ﾓｼﾞｭｰﾙの同期中</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>UI ﾋﾞｭｰの初期化中。</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>戻る</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>ﾃｽﾄの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright Viavi Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>機器情報</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>ｵﾌﾟｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>保存されたﾌｧｲﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>ﾕｰｻﾞｰ ｲﾝﾀｰﾌｪｲｽ ｱｸｾｽ ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>ﾘﾓｰﾄ制御は使用中です。 &#xA;&#xA; 読み取り専用ｱｸｾｽ ﾓｰﾄﾞが原因で、ﾕｰｻﾞーは設定を変更できません。 &#xA; そのため、ﾘﾓｰﾄの制御操作に影響を与える可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>ｱｸｾｽ ﾓｰﾄﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>MSAM は PIM 構成変更のためにﾘｾｯﾄされました。</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>PIM が挿入あるいは取り外されました。PIM を交換している場合は、その作業を続行してください。&#xA;MSAM は、再度、開始されます。最大で 2 分かかることがあります。しばらくお待ちください...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>BERT ﾓｼﾞｭｰﾙの温度が高すぎます。 &#xA; 内部温度が &#xA; 上昇し続ける場合、自動的にｼｬｯﾄ ﾀﾞｳﾝされます。ﾃﾞｰﾀを保存してから BERT &#xA; ﾓｼﾞｭｰﾙをｼｬｯﾄ ﾀﾞｳﾝし、ﾃｸﾆｶﾙ ｻﾎﾟｰﾄにご連絡ください。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>BERT ﾓｼﾞｭｰﾙは過熱のため強制的にｼｬｯﾄ ﾀﾞｳﾝされました。</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>XFP PIM が誤ったｽﾛｯﾄにあります。 XFP PIM をﾎﾟｰﾄ番号 #1 に移動してください。</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>BERT ﾓｼﾞｭｰﾙ XFP PIM が誤ったｽﾛｯﾄにあります。 &#xA; XFP PIM をﾎﾟｰﾄ番号 #1 に移動してください。</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>電気ﾃｽﾄを選択していますが、選択した SFP は光 SFP のようです。</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>電気 SFP を確実に使用してください。</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>BERT ﾓｼﾞｭｰﾙによって、ｱﾌﾟﾘｹｰｼｮﾝ %1 で潜在的なｴﾗーが検出されました。 &#xA;&#xA; ﾕｰｻﾞーは電気的ﾃｽﾄを選択しましたが、 SFP は光 SFP のようです。 SFP を置き換えるか、他の SFP を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>光ﾃｽﾄを選択していますが、選択した SFP は電気 SFP のようです。</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>光 SFP を確実に使用してください。</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>ﾃｽﾄ ｾｯﾄによって、ｱﾌﾟﾘｹｰｼｮﾝ %1 で潜在的なｴﾗーが検出されました。&#xA;&#xA;ﾕｰｻﾞーは光学ﾃｽﾄを選択しましたが、SFP は&#xA;電気的 SFP のようです。SFP を置き換えるか、他の SFP を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>ﾕｰｻﾞーは 10G ﾃｽﾄを選択しましたが、挿入されたﾄﾗﾝｼｰﾊﾞは SFP+ ではないようです。 </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>SFP+ を使用していることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>ﾃｽﾄによって潜在的なｴﾗーが検出されました。このﾃｽﾄでは SFP+ が必要ですが、ﾄﾗﾝｼｰﾊﾞは SFP+ ではないようです。SFP+ に置き換えてください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>自動 ﾚﾎﾟｰﾄ 設定</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>同じﾌｧｲﾙを上書きする</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoReport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Warning:    選択されたﾃﾞｨｽｸは一杯です 空きを作るか最初の古い 5 個のﾌｧｲﾙが &#xA; 自動的に削除されます</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>自動ﾚﾎﾟｰﾄの有効</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>ﾚﾎﾟｰﾄ 期間</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>最小:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>最大:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>ﾚﾎﾟｰﾄ作成後にﾃｽﾄを再開する</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>分割ﾌｧｲﾙの作成</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>ﾚﾎﾟｰﾄ 名称</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>名前に作成日時を自動的に付加する</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>ﾌｫｰﾏｯﾄ :</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>ﾃｷｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>読み取り専用ｱｸｾｽ ﾓｰﾄﾞでは、自動ﾚﾎﾟｰﾄはｻﾎﾟｰﾄされません。</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>自動ﾚﾎﾟｰﾄはﾊｰﾄﾞﾃﾞｨｽｸに保存されます</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>自動ﾚﾎﾟｰﾄにはﾊｰﾄﾞﾃﾞｨｽｸが必要です。この装置には内蔵のﾊｰﾄﾞﾃﾞｨｽｸが見当たりません。</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>自動化ｽｸﾘﾌﾟﾄの実行中は自動ﾚﾎﾟｰﾄを使用できません。</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>自動ﾚﾎﾟｰﾄ作成中</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>準備中 ...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>作成中</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>前回のﾚﾎﾟｰﾄを削除中 ...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>完了</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** 電源が足りません 他のﾓｼﾞｭｰﾙを解除してください ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>ｼﾘｱﾙ 接続 成功</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>ｱﾌﾟﾘｹｰｼｮﾝはｱｯﾌﾟｸﾞﾚｰﾄﾞをﾁｪｯｸしています</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>ｱﾌﾟﾘｹｰｼｮﾝは通信の準備ができています</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ERROR ｶｰﾈﾙｱｯﾌﾟｸﾞﾚｰﾄﾞ *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** ｲｰｻﾈｯﾄｾｷｭﾘﾃｨが標準であるかを確認、もしくは BERT ﾓｼﾞｭｰﾙｿﾌﾄを再ｲﾝｽﾄｰﾙしてください *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERROR ｱﾌﾟﾘｹｰｼｮﾝｱｯﾌﾟｸﾞﾚｰﾄﾞ ﾓｼﾞｭｰﾙｿﾌﾄを再ｲﾝｽﾄｰﾙしてください ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** ERROR 電源を確認してください ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** 開始 ｴﾗｰ : BERT ﾓｼﾞｭｰﾙ を無効にし、再度有効にしてください ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>表示</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>ﾂｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>ﾚﾎﾟｰﾄの作成 ...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>自動ﾚﾎﾟｰﾄ ...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>ﾃｽﾄ開始</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>ﾃｽﾄ停止</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>ｶｽﾀﾏｲｽﾞ...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>ｱｸｾｽ ﾓｰﾄﾞ ...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>ﾃｽﾄを ﾃﾞﾌｫﾙﾄ にﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>ﾋｽﾄﾘを削除</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>ｽｸﾘﾌﾟﾄの実行 ...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>VT100 ｴﾐｭﾚｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>ﾓﾃﾞﾑの設定 ...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>ﾃﾞﾌｫﾙﾄ ﾚｲｱｳﾄの復元</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>結果 ｳｲﾝﾄﾞｳ</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>単一</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>分割 左 / 右</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>分割 上 / 下</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>2 x 2 ｸﾞﾘｯﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>底でつなぎ合わせる</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>左でつなぎ合わせる</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>結果のみ表示</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>ﾃｽﾄ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>構成 ﾊﾟﾈﾙ</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Actions ﾊﾟﾈﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>ｽｸﾘﾌﾟﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>ｽｸﾘﾌﾟﾄ ﾌｧｲﾙ (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>選択 &#xA; ｽｸﾘﾌﾟﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>信号 接続 </translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>ﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>ﾃｷｽﾄ (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>ｺﾝﾃﾝﾂ &#xA; 選択</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>作成</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>ﾌｫｰﾏｯﾄ :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>ﾚﾎﾟｰﾄを表示</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>ｴﾗｰ - ﾌｧｲﾙ名を空白にすることはできません。</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>ｺﾝﾃﾝﾂの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>ﾌｫｰﾏｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>ﾌｧｲﾙ名</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>ﾚﾎﾟｰﾄを表示</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞを含める</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>ﾚﾎﾟｰﾄ &#xA; 作成</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>ﾚﾎﾟｰﾄの表示 </translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>ﾃｷｽﾄ (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>ｴﾗｰ - ﾌｧｲﾙ名を空白にすることはできません。</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>保存されたﾚﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>信号の減衰を行ってください</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>ｲﾍﾞﾝﾄﾛｸﾞとﾋｽﾄｸﾞﾗﾑは一杯です &#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>K1/K2 ﾛｸﾞは一杯です。 &#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>ﾛｸﾞは一杯です。 &#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>この種類の追加項目のﾛｸﾞ記録なしで、 &#xA; ﾃｽﾄは継続します。ﾃｽﾄの再開は全ての &#xA; ﾛｸﾞとﾋｽﾄｸﾞﾗﾑを消去します。</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>光 &#xA; ﾘｾｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>ﾃｽﾄ終了</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>ﾚｼｰﾊﾞ 過負荷</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>ﾛｸﾞが一杯です。</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>注意</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>実行中のﾃｽﾄがありません</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>ﾕｰｻﾞｲﾝﾀﾌｪｰｽのｶｽﾀﾏｲｽﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>ﾃｽﾄ設定の保存</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄの保存</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>ﾃｽﾄ設定の読込み</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>読込み</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>すべてのﾌｧｲﾙ (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>保存されたﾃｽﾄ (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>保存されたﾃﾞｭｱﾙ ﾃｽﾄ (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>設定の読み込み</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄの読み込み</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>保存されたﾃｽﾄを USB からｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>すべてのファイル (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>保存されたｸﾗｼｯｸ RFC ﾃｽﾄ構成 (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>保存された RFC ﾃｽﾄ構成 (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>保存された FC ﾃｽﾄ構成 (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>保存された TrueSpeed 構成 (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>保存された TrueSpeed VNF 設定 (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>保存された SAMComplete 構成 (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>保存された SAMComplete 構成 (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>保存された OTN ﾁｪｯｸ構成 (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>保存された光学機器のセルフテスト構成 (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>保存された CPRI チェック構成 (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>保存されたPTPﾁｪｯｸﾌﾟﾛﾌｧｲﾙ (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>保存された Zip ﾌｧｲﾙ (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>保存されたﾃｽﾄを USB にｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>すべてのファイル (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>ﾕｰｻﾞーは複数のﾃｽﾄを保存しました</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>ﾕｰｻﾞ 保存 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>ﾘﾓｰﾄ制御は使用中です。 &#xA;&#xA; この操作は読み取り専用ｱｸｾｽ ﾓｰﾄﾞでは許可されません。 &#xA; ﾌﾙ ｱｸｾｽを有効にするには、 [ ﾂｰﾙ ]->[ ｱｸｾｽ ﾓｰﾄﾞ ] を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>BERT ﾓｼﾞｭｰﾙのｵﾌﾟｼｮﾝは期限切れです。 &#xA; 終了して、ｼｽﾃﾑ ﾍﾟｰｼﾞから BERT を再起動してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>非表示</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>両方のﾃｽﾄの再開</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>ﾘｽﾀｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>ﾂｰﾙ:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>ｱｸｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>ﾃｽﾄの目的で最大化した結果ｳｨﾝﾄﾞｳ : </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>ﾌﾙ ﾋﾞｭｰ</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>ﾃｽﾄ の追加</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>ﾕｰｻﾞ ﾏﾆｭｱﾙ</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>推奨 光学</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>周波数ｸﾞﾘｯﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>ﾍﾙﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄ構成に名前を付けて保存 ...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄ構成の読み込み ...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>表示</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>ﾃｽﾄ選択の変更 ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>ﾌﾙ ﾃｽﾄ ﾋﾞｭーに移行</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄ ﾚﾎﾟｰﾄの作成 ...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>ﾚﾎﾟｰﾄの表示 ...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>ﾍﾙﾌﾟ</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>注意</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>ﾌｧｲﾙを保存しています</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>新しい名前 :</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>新しいﾌｧｲﾙ名を入力します。</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>名前の変更</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>その名前のﾌｧｲﾙが存在するため、その名前に変更することはできません。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>ﾘｽﾀｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>ﾕｰｻﾞ 情報の編集</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>選択なし ...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max characters: </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>ﾛｺﾞの選択 ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>ﾌﾟﾚﾋﾞｭーはありません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>ﾍﾙﾌﾟ ﾀﾞｲｱｸﾞﾗﾑ</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>ﾕｰｻﾞ ﾏﾆｭｱﾙ</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>戻る</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>ﾎｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Forward</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>QuickLaunch</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERROR ｼﾞｯﾀ ｱｯﾌﾟｸﾞﾚｰﾄﾞ ﾓｼﾞｭｰﾙｿﾌﾄを再ｲﾝｽﾄｰﾙしてください ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ERROR 入力信号ﾚﾍﾞﾙを確認してください ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>光 ｼﾞｯﾀ 機能 実行中</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 ﾌﾟﾛﾌｧｲﾙ (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>すべてを選択</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>全て &#xA; 非選択</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>注:"接続" ﾌﾟﾛﾌｧｲﾙを読み込むとき、必要に応じて通信ﾁｬﾈﾙをﾘﾓｰﾄ ﾕﾆｯﾄに接続します。</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>全て削除</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>互換性のないﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>読み込み &#xA; ﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>%1 を削除してもよろしいですか</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>すべての %1 ﾌﾟﾛﾌｧｲﾙを削除してもよろしいですか ?&#xA;&#xA; この操作は元に戻せません｡</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（読み取り専用のﾌｧｲﾙは削除されません。）</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの読み込みに失敗しました :</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>ﾌﾟﾛﾌｧｲﾙを読み込みました :</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>一部の構成がこのｱﾌﾟﾘｹｰｼｮﾝになかったため、正しく読み込まれませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>ﾌﾟﾛﾌｧｲﾙを正常に読み込みました :</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>ﾃﾞｭｱﾙ ﾃｽﾄの有効化</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>ｱﾌﾟﾘｹｰｼｮﾝのｲﾝﾃﾞｯｸｽ中</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>ｵﾌﾟｼｮﾝを有効にします</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>ｲﾝｽﾄｰﾙ済みのﾊｰﾄﾞｳｪｱまたはｵﾌﾟｼｮﾝに対して使用可能なﾃｽﾄがありません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>ﾃｽﾄを開始できません。    ｲﾝｽﾄｰﾙしたｵﾌﾟｼｮﾝと現在のﾊｰﾄﾞｳｪｱ構成を確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>ﾊﾟﾜｰ ﾀﾞｳﾝ時に実行されていたｱﾌﾟﾘｹｰｼｮﾝの復元中</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>ｱﾌﾟﾘｹｰｼｮﾝが実行中です</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>内部 USB ﾌﾗｯｼｭをﾏｳﾝﾄできません。&#xA;&#xA;ﾃﾞﾊﾞｲｽが電池の隣にしっかりと挿入されているようにしてください。挿入後に BERT ﾓｼﾞｭｰﾙを再始動してください。</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>内部 USB ﾌﾗｯｼｭのﾌｧｲﾙｼｽﾃﾑが破損しているようです。ﾃｸﾆｶﾙｻﾎﾟｰﾄにご連絡ください。</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>内部 USB ﾌﾗｯｼｭの容量が推奨する１ G に足りません。&#xA;&#xA;ﾃﾞﾊﾞｲｽが電池の隣にしっかり挿入されているようにしてください。挿入後、装置を再始動してください。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>設定の復元中にｴﾗーが発生しました。 &#xA; 再度、試行してください。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>保存済みのﾃｽﾄを読み込む前に、すべての実行中ﾃｽﾄは終了となります。</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>お待ちください ...&#xA; 保存したﾃｽﾄを読み込んでいます</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>ﾃｽﾄ文書名もしくはﾌｧｲﾙﾊﾟｽが間違っています &#xA; 文書の場所を Load Saved Test を使用して指定してください to locate the document.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>ﾃｽﾄの復元中にｴﾗｰが発生しました｡ &#xA; 再度､試行してください｡ &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>ﾊﾞｯﾃﾘﾁｬｰｼﾞｬが有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>このﾓｰﾄﾞではﾊﾞｯﾃﾘﾁｬｰｼﾞｬは有効になりません 適切なﾃｽﾄが選択された場合 ﾊﾞｯﾃﾘﾁｬｰｼﾞｬは自動的に有効になります</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>このﾓｼﾞｭｰﾙに対してﾘﾓｰﾄ ｺﾝﾄﾛｰﾙが使用中です。 &#xA; ﾃﾞｨｽﾌﾟﾚｲが無効になっています。</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>ﾒｯｾｰｼﾞはﾛｸﾞ記録されました。ｸﾘｯｸすると表示されます...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 に関するﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>ﾒｯｾｰｼﾞはありません</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 件のﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 件のﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 に関するﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>ﾓﾃﾞﾑ設定</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>このｻｰﾊﾞのｱﾄﾞﾚｽに対する IP とﾀﾞｲﾔﾙｲﾝ ｸﾗｲｱﾝﾄに割当てられた IP を選択してください｡</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Server IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>ｸﾗｲｱﾝﾄ IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>現在地 ( 国ｺｰﾄﾞ )</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>ﾓﾃﾞﾑを検出できません｡ﾓﾃﾞﾑが正常に接続されていることを確認し､再度実行してください｡</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>ﾃﾞﾊﾞｲｽは使用中です｡全てのﾀﾞｲｱﾙｲﾝｾｯｼｮﾝを切断し､再度実行してください｡</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>ﾓﾃﾞﾑをｱｯﾌﾟﾃﾞｰﾄできません｡全てのﾀﾞｲｱﾙｲﾝｾｯｼｮﾝを切断し､再度実行してください｡</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>時間変更によるﾃｽﾄの再開</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>新しいｵﾌﾟｼｮﾝｷｰを入力してｲﾝｽﾄｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>ｵﾌﾟｼｮﾝ ｷｰ</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>ｲﾝｽﾄｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>ｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>新規ｵﾌﾟｼｮﾝをご購入の場合、Viaviまでご連絡下さい。</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>ｵﾌﾟｼｮﾝ ﾁｬﾚﾝｼﾞ ID: </translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>ｷｰ が承認されました。新しいｵﾌﾟｼｮﾝを有効にするために再起動してください。</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>拒否されたｷｰ - 期限切れｵﾌﾟｼｮﾝ ｽﾛｯﾄがいっぱいです。</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>拒否されたｷｰ - 期限切れｵﾌﾟｼｮﾝはすでにｲﾝｽﾄｰﾙされています。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>無効 ｷｰ - 再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%2 ｷーのうち %1 ｷーが受け入れられました ! 再起動すると新しいｵﾌﾟｼｮﾝが有効になります。</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>USB ﾒﾓﾘーの '%1' が開きません。</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>ｵﾌﾟｼｮﾝ情報をの取得中に問題が発生しました ...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>選択 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>ﾃﾞﾌｫﾙﾄ</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>凡例</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>凡例 :</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI ( 未使用 )</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>ﾘｻﾞｰﾌﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>BERT ﾓｼﾞｭｰﾙについて</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>ﾄﾗﾝｽﾎﾟｰﾄ ﾓｼﾞｭｰﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>A ｸｲｯｸ ｶｰﾄﾞをｲﾝﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>A ｸｲｯｸ ｶｰﾄﾞをｲﾝﾎﾟｰﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> メニューを隠す</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> 全ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>ｶｽﾀﾏｲﾂﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>1 つ以上の選択されたｽｸﾘｰﾝ ｼｮｯﾄが現在のﾃｽﾄの開始前に&#xA;ｷｬﾌﾟﾁｬされました。適切なﾌｧｲﾙを選択していることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>全て&#xA;選択</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>全て &#xA; 非選択</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>選択 &#xA; ｽｸﾘｰﾝｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄ &#xA; 非選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 校正</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>校正</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>校正中...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>校正が完了していません。RS-FEC を使用するには校正が必要です。</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(校正はセットアップページの [RS-FEC] タブから実行できます。)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>校正を再試行する</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>校正を終了する</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>校正未完了</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC は通常 SR4、PSM4、CWDM4 と一緒に使用します。</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>RS-FEC 校正を実行するには、以下の手順を実行します (CFP4 にも適用されます)。</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>CSAM に QSFP28 アダプタを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>アダプタに QSFP28 トランシーバを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>トランシーバにファイバ ループバック デバイスを挿入する</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>[校正] をクリックする</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>再同期</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>トランシーバを試験中のデバイス (DUT) と今すぐ同期する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>トランシーバからファイバ ループバック デバイスを取り外す</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>試験中のデバイス (DUT) への接続を確立する</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>DUT レーザを ON にする</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>CSAM の信号確認 LED が緑であることを確認する</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>[レーンの再同期] をクリックする</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>再同期が完了しました。ここでダイアログが閉じる場合があります。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>読み取り専用で保存する</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>テストするピン リスト</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>ﾌｧｲﾙ名</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>読み取り専用で保存する</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>保存 &#xA; ﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 ﾌﾟﾛﾌｧｲﾙ (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>ﾌﾟﾛﾌｧｲﾙ </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>は読み取り専用ﾌｧｲﾙです。置換することはできません。</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>保存されたﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>ﾛｺﾞの選択</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>ｲﾒｰｼﾞ ﾌｧｲﾙ (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>ﾌｧｲﾙが大きすぎます。別のﾌｧｲﾙを選択してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>選択 ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>ﾃﾞﾌｫﾙﾄ</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>凡例</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>ﾃｽﾄ開始</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>ﾃｽﾄ停止</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>遅延</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>ﾌﾙ ｱｸｾｽ</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>読み取り専用</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>ﾀﾞｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>ﾗｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>ｸｲｯｸ ｽﾀｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>結果ﾋﾞｭｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>推定 合計 TCP ｽﾙｰﾌﾟｯﾄ (Mbps) based on:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>最大 有効 帯域幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>平均 ﾗｳﾝﾄﾞ ﾄﾘｯﾌﾟ ﾃﾞｨﾚｲ (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>最大 ｽﾙｰﾌﾟｯﾄを達成するのに必要な ﾊﾟﾗﾚﾙ TCP ｾｯｼｮﾝ数 :</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>ﾋﾞｯﾄﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>ｳｲﾝﾄﾞｳ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>ｾｯｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>推定 合計 TCP ｽﾙｰﾌﾟｯﾄ (kbps) based on:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>最大 有効 帯域幅 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Throughput</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>ﾃｽﾄ設定を保存中にｴﾗｰが発生しました。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>ﾃｽﾄ設定をﾛｰﾄﾞ中にｴﾗｰが発生しました。</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>選択ﾃｨｽｸは一杯です。 &#xA; 　不要ﾌｧｲﾙを削除し、もう一度ｾｰﾌﾞしてください。</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>以下のｶｽﾀﾑ保存された結果のｶﾃｺﾞﾘーのﾌｧｲﾙは現在ﾛｰﾄﾞされているものと異なります。</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>．．．その他%1件</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>これらを上書きすると他のﾃｽﾄに影響を及ぼす可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>上書きを続けますか？</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>しばらくお待ちください ...&#xA; 設定を復元しています ...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>現時点では %1 ﾃｽﾄを読み込むにはﾘｿｰｽが不足しています。&#xA;現在の構成で使用可能なﾃｽﾄのﾘｽﾄについては、[ﾃｽﾄ] ﾒﾆｭーを参照してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>全てのﾃｽﾄ設定を回復できません。 &#xA; ﾌｧｲﾙ内容は古いか正しくありません。 &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>ｱｸｾｽ ﾓｰﾄﾞは読み取り専用です。変更するには、 [ ﾂｰﾙ ]->[ ｱｸｾｽ ﾓｰﾄﾞ ] を使用してください</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>実行中のﾃｽﾄがありません</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>これは全ての設定を既定にﾘｾｯﾄします。 &#xA;&#xA; 続けますか？</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>これはﾃｽﾄをｼｬｯﾄﾀﾞｳﾝし、再開します。 &#xA; ﾃｽﾄ設定は既定に回復されます。 &#xA;&#xA; 続けますか？ &#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>このﾜｰｸﾌﾛーは現在実行中です。 実行中のﾜｰｸﾌﾛーを終了して新しいﾜｰｸﾌﾛーを開始しますか ?&#xA;&#xA; 以前のﾜｰｸﾌﾛーを続行するには [ ｷｬﾝｾﾙ ] をｸﾘｯｸしてください。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>このﾜｰｸﾌﾛーを終了しますか ?&#xA;&#xA; ﾜｰｸﾌﾛーを続行するには [ ｷｬﾝｾﾙ ] をｸﾘｯｸしてください。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>VT100 を起動できません。ｼﾘｱﾙ ﾃﾞﾊﾞｲｽは既に使用されています。</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>ﾓｼﾞｭｰﾙ </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>再起動 を押すと両ﾎﾟｰﾄの結果が消去されることに､注意してください｡</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>ﾃｽﾄの選択</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>ﾃｽﾄ設定を読込む…</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>ﾃｽﾄ設定を保存…</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>設定のみ読み込み ...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>ﾃｽﾄ の追加</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>ﾃｽﾄの削除</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>ﾍﾙﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>ﾍﾙﾌﾟ ﾀﾞｲｱｸﾞﾗﾑ</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>ﾚﾎﾟｰﾄの表示...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>ﾚﾎﾟｰﾄのｴｸｽﾎﾟｰﾄ ...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>ﾕｰｻﾞ 情報の編集...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>ﾚﾎﾟｰﾄ ﾛｺﾞのｲﾝﾎﾟｰﾄ ...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>USB からｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>保存されたﾃｽﾄ ...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>保存されたｶｽﾀﾑ･ｶﾃｺﾞﾘ ...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>USB へ ｴｸｽﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄ...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>タイミング データ...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>ﾚﾋﾞｭｰ / ｲﾝｽﾄｰﾙ ｵﾌﾟｼｮﾝ ...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄの取得</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>ﾕｰｻﾞ ﾏﾆｭｱﾙ</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>推奨 光学</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>周波数ｸﾞﾘｯﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>信号 接続 </translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>ﾍﾙﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>ｸｲｯｸ ｶｰﾄﾞ</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>お待ちください ...&#xA; ﾃｽﾄの削除を行っています</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Port 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Port 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>選択なし ...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>ﾌｧｲﾙの選択 ...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>USB からﾊﾟｹｯﾄ ｷｬﾌﾟﾁｬをｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>選択 &#xA; ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Saved Packet Capture (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>ﾚﾎﾟｰﾄの表示</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>全ての ﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>ﾃｷｽﾄ (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>表示</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>このﾚﾎﾟｰﾄを&#xA;表示</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>他のﾚﾎﾟｰﾄ &#xA; の表示</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>ﾚﾎﾟｰﾄ &#xA; の名前変更</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 既に存在します &#xA; 置き換えますか？</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>名前を変更したﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>ｴﾗｰ - ﾌｧｲﾙ名を空白にすることはできません。</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>ﾚﾎﾟｰﾄが名前を付けて保存されました</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>ﾚﾎﾟｰﾄが保存されていません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>進む</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>TIE ﾃﾞｰﾀを保存します ...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>ﾀｲﾌﾟを指定して保存 : </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>HRD ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>CHRD ﾌｧｲﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>保存中</translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>これには数分かかります ...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>ｴﾗｰ :HRD ﾌｧｲﾙを開けません。再度、保存してください。</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>ｷｬﾝｾﾙ中です ...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>TIE ﾃﾞｰﾀは保存されました。</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>ｴﾗｰ : ﾌｧｲﾙを保存できません。再度、試行してください。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>注意</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Wander Analysis を閉じると、すべての分析結果は失われます。&#xA;分析を続行するには、[Continue Analysis] (分析の続行) をｸﾘｯｸします。</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>解析を閉じる</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>解析の続行</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>ﾜﾝﾀﾞ 解析</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>TIE ﾃﾞｰﾀ &#xA; ｱｯﾌﾟﾃﾞｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>TIE ｱｯﾌﾟﾃﾞｰﾄ &#xA; 中止</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>MTIE/TDEV&#xA; の計算</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>計算 &#xA; 中止</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>ｽｸﾘｰﾝｼｮｯﾄを &#xA; 取得します</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>&#xA;TIE ﾃﾞｰﾀの読み込み</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>TIE&#xA; 読み込みの停止</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>解析を &#xA; 閉じる</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>TIE ﾃﾞｰﾀの読み込み</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>読み込み</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>すべての Wander ﾌｧｲﾙ (*.chrd *.hrd);;Hrd ﾌｧｲﾙ (*.hrd);;Chrd ﾌｧｲﾙ (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>長方形を定義するには 2 回ﾀｯﾌﾟしてください</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>全て削除</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>ﾌﾟﾛﾌｧｲﾙの読み込み</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>%1 を削除してもよろしいですか</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>すべての %1 ﾌﾟﾛﾌｧｲﾙを削除してもよろしいですか ?&#xA;&#xA; この操作は元に戻せません｡</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>（読み取り専用のﾌｧｲﾙは削除されません。）</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 ﾌﾟﾛﾌｧｲﾙ (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>ﾌﾟﾛﾌｧｲﾙを読み込めません。</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>読み込み失敗</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>次へ</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>ﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>ｴﾗｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>進む</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>注意</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>次は何をしますか ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>終了してもよろしいですか ?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>終了時にｾｯﾄｱｯﾌﾟを保存</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>終了して結果を表示</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>戻る</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>ｽﾃｯﾌﾟﾊﾞｲｽﾃｯﾌﾟ :</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>次</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>ｶﾞｲﾄﾞ ﾐｰ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>他のポート</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>やりなおす</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>に移動...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>他のポート</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>ﾃｽﾄの起動中 お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>ﾃｽﾄを停止しています。しばらくお待ちください...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>ﾒｯｾｰｼﾞ ﾛｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>ﾒｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>ｽﾃｯﾌﾟの表示</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>応答 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>選択なし ...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>ﾚﾎﾟｰﾄ ﾛｺﾞ</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>ｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>ﾛｺﾞの選択 ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>ﾌﾟﾚﾋﾞｭーはありません。</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>ﾃｽﾄの起動中 お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>ﾃｽﾄを停止しています。しばらくお待ちください...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>ﾃｽﾄ中です ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>残り時間:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>ﾃｽﾄ開始していません</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>ﾃｽﾄ未完了</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>ｽｸﾘﾌﾟﾄを開始する前に、自動ﾚﾎﾟｰﾄを OFF にします。</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>以前に表示された自動ﾚﾎﾟｰﾄを ON にします。</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>ﾒｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>保存されたｽｸﾘｰﾝｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>保存されたｽｸﾘｰﾝｼｮｯﾄ :</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>ﾌﾟﾛﾌｧｲﾙ選択</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>使用ﾚｲﾔ</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>保存されたﾌﾟﾛﾌｧｲﾙの読み込み</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>TrueSAM をどのように構成しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>保存されたﾌﾟﾛﾌｧｲﾙの構成の読み込み</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>進む</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>新しいﾌﾟﾛﾌｧｲﾙの開始</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>どのﾚｲﾔでｻｰﾋﾞｽを実行しますか ?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>ﾚｲﾔ 2:MAC ｱﾄﾞﾚｽ (たとえば 00:80:16:8A:12:34) を使用したﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>ﾚｲﾔ 3:IP ｱﾄﾞﾚｽ (たとえば 192.168.1.9) を使用したﾃｽﾄ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>しばらくお待ちください。強調表示されたｽﾃｯﾌﾟに移動しています。</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>ﾃｽﾄの選択</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>通信の確立</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>拡張 RFC 2544 の構成</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>SAMComplete の構成</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>J-Proof の構成</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>TrueSpeed の構成</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>構成の保存</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>ﾚﾎﾟｰﾄ情報の追加</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>選択したﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>ﾚﾎﾟｰﾄの表示</translation>
        </message>
    </context>
</TS>
