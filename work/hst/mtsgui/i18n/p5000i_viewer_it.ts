<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscopio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Visualizza FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Blocca</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Zoom avanti</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Sovrapposizione</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Analisi in corso...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Profilo</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importa da USB</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>Suggerimento</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Centratura automatica</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Pulsante di test:</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Blocca</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>Altre impostazioni...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Importa profilo microscopio</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Importa&#xA;Profilo</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Profili Microscopio (* .pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Blocca</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>Live</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>Salva PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>Salva PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Salva immagine</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Salva report</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>Analisi non riuscita</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>Impossibile analizzare la fibra. Controllare che il video in diretta ne mostri l'estremità e che sia a fuoco prima di ripetere il test.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Seleziona immagine</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>File immagine (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Verifica fibra e report test</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Informazioni fibra</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>Nessuna informazione fibra definita</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALLITO</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Profilo:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Suggerimento:</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Riepilogo controllo</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>DIFETTI</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>SCRATCH</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>o</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>Criteri</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Soglia</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Conteggio</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>Risultato</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>qualsiasi</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Impossibile generare riepilogo controllo.</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>INGRANDIMENTO RIDOTTO</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>INGRANDIMENTO ELEVATO</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Scratch test non abilitato</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Processo:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cavo:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Connettore:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Società:</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Tecnico:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Cliente:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Posizione:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Processo:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cavo:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Connettore:</translation>
        </message>
    </context>
</TS>
