<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscopio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>EtiquetaTexto</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vista FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Abandonar</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Zoom +</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Capa</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Analizando...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Perfil</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importar desde USB</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>Tip</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Centrado automático</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Botón de Prueba:</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Pruebas</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Freezes</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>Otra configuración...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Importar perfil de microscopio</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Importar perfil</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Perfiles de microscopio (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>Real</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>Guardar PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>Guardar PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Guardar imagen</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Guardar informe</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>Análisis fallido</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>No se puede analizar la fibra. Por favor, compruebe que el video muestra el extremo de la fibra y  que está centrado antes de volver a hacer la prueba.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Seleccionar imagen</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Archivos de imagen (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Inspección de Fibra e Informe de Prueba</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Información de la fibra</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>No hay información definida de fibra extra</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASA</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FAIL</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Perfil:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Tip:</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Resumen de inspección</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>DEFECTOS</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>RAYADO</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>o</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>Criterios</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Umbral</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Número</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>Resultado</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>cualquiera</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Fallo al generar el resumen de la inspección.</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>BAJO AUMENTO</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>GRAN AUMENTO</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Prueba de rayado no habilitada</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Trabajo:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cable:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Conector:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Compañía:</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Técnico:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Cliente:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Ubicación:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Trabajo:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Cable:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Conector:</translation>
        </message>
    </context>
</TS>
