<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Afficher les téléchargements</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Zoom avant</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Zoom -</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>réinitialiser Zoom</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>téléchargement</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Ouvrir</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 minutes restantes</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 secondes restantes</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Téléchargements</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>enregistrer sous</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Répertoire:</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nom de fichier:</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
</TS>
