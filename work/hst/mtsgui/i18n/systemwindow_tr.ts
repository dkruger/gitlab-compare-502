<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Dosyalar</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Çıkarılabilir Bellek</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Hiçbir cihaz tespit edilemedi.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Biçimlendir</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>Bu usb cihazın biçimlendirilmesiyle, tüm mevcut verileri silinir. Bu, tüm dosyaları ve bölümleri içerir.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Çıkar</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Araştır</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>EVET</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Bluetooth Eşleşme Talep edildi</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Etkinleştir Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Ağ</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>IP Mod</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statik</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP Adres</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Subnet Mask</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Ağ Geçidi</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>DNS Sunucu</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>IPv6 mod</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Devre dışı</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>manuel</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Oto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6 Adresi</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Subnet Prefix Uzunluğu</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>DNS Sunucu</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Link-Yerel Adres</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Durumsuz Adres</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Durumlu Adres</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Modüller yüklenmemiş</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Mevcut</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Mevcut değil</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Başlıyor</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Etkinleştiriliyor</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Başlatılıyor</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Hazır</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Taranıyor</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Devre dışı bırakılıyor</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Durduruluyor</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>İlişkilendiriliyor</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>İlişkilendirildi</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Kablosuz Adaptörü Etkinleştir</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Bağlandı</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Bağlı değil</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Başarısız</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>USB Kablosuz aygıt bulunamadı</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Durum</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>Ağa bağlanamadı</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Bağlan</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Bağlantıyı kes</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Proxy &amp; Güvenlik</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Proxy sunucu</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Ağ Güvenliği</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>FTP, telnet ve Oto StrataSync Devredışı bırak</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Batarya gücü ile çalışıyor</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Batarya tespit edilemedi</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Şarj Etme güç tüketimi nedeniyle devre dışı bırakıldı</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Batarya Şarj Ediliyor</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>Batarya tam dolu şarj edildi</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Güç hatası nedeniyle batarya şarj edilemiyor</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Bilinmeyen batarya durumu</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>Batarya aşırı ısınma tehlikesiyle karşı karşıya</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>Batarya sıcaklığı normal</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Şarj</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Tarih ve Zaman</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Zaman Bölgesi</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Bölge</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>Afrika</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>Amerika</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antartika</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Asya</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Atlantik Okyanusu</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Avustralya</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Avrupa</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Hint Okyanusu</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Pasifik Okyanusu</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>Ülke</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afganistan</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Aland Adaları</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Arnavutluk</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Cezayir</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Amerikan Samoası</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorra</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angora</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguilla</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua ve Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Arjantin</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Ermenistan</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Avusturya</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Azerbeycan</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamalar</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahreyn</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladeş</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbados</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Beyaz Rusya</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Belçika</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belize</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Benin</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermuda</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Butan</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolivya</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bosna- Hersek</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botsvana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Bouvet Adası</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brezilya</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>İngiliz Hint Okyanusu Toprakları</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Sultanlığı</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgaristan</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Kamboçya</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Kamerun</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Kanada</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Cape Verde</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Cayman Adaları</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>Orta Afrika Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Çad</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Şili</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>Çin</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Christmas Adası</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Cocos ( Keeling) Islands</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Kolombiya</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Komorlar</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Kongo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>Kongo Demokratik Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Cook Adaları</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Kostarika</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Fildişi Sahili</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Hırvatistan</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Küba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Kıbrıs</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>Çek Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Danimarka</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Cibuti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominika</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>Dominik Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Ekvador</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Mısır</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>El Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Ekvator Ginesi</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Eritre</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estonya</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Etiyopya</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Falkland Adaları (Malvinas )</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Faroe Adaları</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fiji</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finlandiya</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>Fransa</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Fransız Guyanası</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Fransız Polinezyası</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Fransız Güney Toprakları</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabon</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambiya</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Georgia</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Almanya</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Gana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Cebelitarık</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Yunanistan</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Grönland</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Grenada</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>guadeloupe</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>yün denizci yeleği</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Gine</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Gine - Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guyana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haiti</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Heard Adası ve McDonald Adaları</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hong Kong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Macaristan</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>İzlanda</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Hindistan</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Endonezya</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>İran</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Irak</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>İrlanda</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Isle of Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>İsrail</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>İtalya</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Jamaika</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Japonya</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Ürdün</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Kazakistan</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Kenya</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Kore Demokratik Halk Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Kore , Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Kuveyt</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Kırgızistan</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Lao Demokratik Halk Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Letonya</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Lübnan</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesotho</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Liberya</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Libya</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Lihtenştayn</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Litvanya</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Lüksemburg</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Makedonya, Eski Yugoslav Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagaskar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malezya</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Maldivler</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Mali</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Malta</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Marshall Adaları</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinik</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Moritanya</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>Mauritius</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>Meksika</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Mikronezya , Federe Devletleri</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Moldova, Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>monaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Moğolistan</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Karadağ</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Montserrat</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Fas</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Mozambik</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Myanmar</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namibya</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Nepal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Hollanda</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Hollanda Antilleri</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>yeni Kaledonya</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>yeni Zelanda</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nikaragua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Nijer</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nijerya</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Norfolk Adası</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Kuzey Mariana Adaları</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Norveç</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Umman</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Pakistan</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palau</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Filistin</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panama</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papua Yeni Gine</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguay</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Peru</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Filipinler</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Polonya</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portekiz</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Portoriko</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Katar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>Réunion</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Romanya</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Rusya Federasyonu</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Ruanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>saint Barthelemy</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Saint Helena , Ascension ve Tristan da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Saint Kitts ve Nevis</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Saint Lucia</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>Saint Martin</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>Saint Pierre ve Miquelon</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>Saint Vincent ve Grenadinler</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>San Marino</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Sao Tome ve Principe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Suudi Arabistan</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Senegal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Sırbistan</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seyşeller</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Sierra Leone</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapur</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Slovakya</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Slovenya</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Solomon Adaları</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somali</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Güney Afrika</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Güney Georgia ve Güney Sandwich Adaları</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>İspanya</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Sudan</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Surinam</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Svalbard ve Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Svaziland</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>İsveç</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>İsviçre</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>Suriye Arap Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Tayvan</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tacikistan</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Tanzanya, Birleşik Cumhuriyeti</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Tayland</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Doğu Timor</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinidad ve Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunus</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Türkiye</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Türkmenistan</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Turks ve Caicos Adaları</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Uganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ukrayna</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Birleşik Arap Emirlikleri</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Büyük Britanya</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>Amerika Birleşik Devletleri</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>ABD Küçük Dış Adaları</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Özbekistan</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Vatikan</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Vietnam</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Virgin Adaları, İngiliz</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Virgin Adaları, ABD</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis ve Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Batı Sahra</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Yemen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Zambiya</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Zimbabve</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Alan</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Güney Kutbu</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Vostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Avustralya Başkent Bölgesi</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Kuzey</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Yeni Güney Galler</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Güney</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tazmanya</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Victoria</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Batı</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasilia</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasilia - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasilia + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alaska</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlantik</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>merkezi</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>doğu</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Hawaii</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Mountain</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>yeni Foundland</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pasifik</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Paskalya Adası</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Galapagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Markiz</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Tahiti</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>batı</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Doğu</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Phoenix Adaları</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Line Islands</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Gilbert Islands</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>kuzeybatı</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Azores</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madeira</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrad</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnoyarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Moskova</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Vladivostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Yakutsk</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Yekaterinburg</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Kanarya Adaları</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Wake</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Günışığından faydalanma zamanına otomatik ayarla</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Geçerli Tarih &amp; Saat</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 saat</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 saat</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>24- saat zaman kullan</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (İngilizce)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (Almanca)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (İspanyolca)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (Fransız)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (Basitleştirilmiş Çince)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本语 (Japonca)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (Korece)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (Rusça)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Portekizce)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (İtalyanca)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Dil:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Biçimlendirme Standardını değiştir:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Seçili biçimlendirme için örnekler:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Özelleştir</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Ekran</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Parlaklık</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Ekran koruyucu</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Otomatik erkan koruyucuyu etkinleştir</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mesaj</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Gecikme</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Ekran koruyucu şifresi</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Dokunmatik Ekranı Kalibre et…</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Uzaktan Erişim</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>VNC Erişimi etkinleştir</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>VNC erişimine geçiş  veya şifre koruması mevcut bağlantıları kesecektir.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Uzaktan Erişim şifresi</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>VNC başlatmada data oluştu.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>VNC erişimi için şifre gerekiyor</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>LAN IP adresi</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Wi-Fi IP adresi</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>VNC bağlantı sayısı</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Güncelleme</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>USB Flash tespit edilemiyor. Lütfen USB flashın doğru takıldığından emin olun ve tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Güncelleme yapılamıyor. Lütfen güncelleme sunucusunu belirterek ve tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>Ağ bağlantısı mevcut değil. Lütfen ağ bağlantısını kontrol edin ve tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Güncelleme sunucusuna bağlanamıyor. Lütfen sunucuyu kontrol edin ve tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Mevcut güncellemeler araştırılırken hata oluştu.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>USB güncelleme verisi oluşturma başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Güncelleme bulunamadı, lütfen ayarları kontrol edin ve  tekrar deneyin.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Güncelleme girişimi başarısız oldu, lütfen tekrar deneyin. Sorun devam ederse satış temsilciniz ile temasa geçin.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Test Cihazını Kitle</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Test Cihazının kilitlenmesi yetkisiz erişimi engeller. Ekran koruyucu ekranı korumak için görüntülenir.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Test Cihazını Kitle</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Gerekli bir parola ayarlanmamış. Kullanıcıların şifre girmeden cihazın kilidini açmaları mümkün.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Şifre Ayarları</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Bu ayarlar ekran koruyucu ile paylaşılır.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Şifre Gerekiyor</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Ses</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Hoparlör sesi</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Sessiz</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Mikrofon Sesi</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Sistem Bilgileri</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Sunucuya bağlanıyor...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Bağlantı kuruldu...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Dosyalar indiriliyor...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Dosyalar yükleniyor...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Güncelleme bilgisi indiriliyor...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Log dosyası yükleniyor...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Sunucuyla başarılı bir şekilde senkronize oldu.</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>Bekleme kutusunda. Envantere eklenmeyi bekliyor...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Sunucu ile senkronizasyon başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Senkronizasyon sırasında bağlantı koptu. Ağ ayarlarını kontrol ediniz.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Sunucu ile senkronizasyon başarısız oldu. Sunucu meşgul.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Sunucu ile senkronizasyon başarısız oldu. Sistem hatası tespit edildi.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Bağlantı kurulamadı. Ağ ayarlarını ve Müşteri ID'nizi kontrol ediniz.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Senkronizasyon durduruldu.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Konfigürasyon Verisi Tamam.</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Raporlar Tamam.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Müşteri ID Giriniz.</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Senkronizasyon Başlat</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Senkronizasyon Durdur.</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Sunucu adresi</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>Müşteri ID</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Teknisyen ID</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfigürasyon</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>Rapor</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opsiyon</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Varsayılana Resetle</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Video Player</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Web Tarayıcı</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Debug</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Seri Cihaz</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Seri cihazı kontrol etmesi için Getty'e izin ver</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>İçerik /root/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testler</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Tarama testi ayarla</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Geçmiş</translation>
        </message>
    </context>
</TS>
