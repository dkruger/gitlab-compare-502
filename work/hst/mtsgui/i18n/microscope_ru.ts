<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Микроскоп Viavi</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загрузить</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Вид 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Вид 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Вид 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Вид 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Вид FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Микроскоп</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Захватить</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Зафиксировать</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Изображение</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Яркость</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Контраст</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Режим экрана</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Полный экран</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Каскадный режим</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Выйти</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Сохранить изображение</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Выбрать изображение</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Файлы изображения (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
    </context>
</TS>
