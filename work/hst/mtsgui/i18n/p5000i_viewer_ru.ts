<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Микроскоп Viavi</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Вид FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Выйти</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Зафиксировать</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Увеличение</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Оверлей</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Идет анализ...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Профиль</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Импортировать из USB</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>Подсказка</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Авто - центр</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Тестовая кнопка :</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Испытания</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Замораживания</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>Другие настройки ...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Импортировать микроскоп. профиль</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Импортировать&#xA;профиль</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Микроскоп. профили(*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Зафиксировать</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>Фактическ.</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>Сохранить PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>Сохранить PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Сохранить изображение</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Сохранить отчет</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>Анализ не выполнен</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>Сбой анализа ВОЛС . Перед повторным анализом проверьте , чтобы торец ВОЛС отображался на видео в реальном времени и был сфокусирован .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Выбрать изображение</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Файлы изображения (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Отчет о проведении проверки и сипытания ВОЛС</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Информация о ВОЛС</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>Дополнительная информация о ВОЛС не определена</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ПРОЙДЕНО</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ОШИБКА</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Профиль:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Подсказка :</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Обзор проверки</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>ДЕФЕКТЫ</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>РАБОЧИЕ ОБЛАСТИ</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>или</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>Критерии</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Порог</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Количество</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>Результат</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>любой</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>нет данных</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Сбой создания обзора проверки .</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>СЛАБОЕ УВЕЛИЧЕНИЕ</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>СИЛЬНОЕ УВЕЛИЧЕНИЕ</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Склерометрическое испытание не активировано</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Задание :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Кабель :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Разъем:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Компания :</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Специалист:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Абонент:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Местоположен .:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Задание :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Кабель :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Разъем:</translation>
        </message>
    </context>
</TS>
