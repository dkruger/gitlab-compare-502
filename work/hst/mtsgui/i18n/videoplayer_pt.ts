<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi Video Player</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Desistir</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Nome de vídeo arquivo </translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Tempo:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>Estado de vídeo</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Escala e recorta</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Abrir &amp;arquivo...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Proporção Aspecto </translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Escala modo</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>Adequar a vista</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Escala e recorta</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Abrir arquivo...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Multimídia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aberto</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>Nenhuma mídia aberta</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Carregando...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Buffering...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Executando...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>Pausado</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Erro...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Livre - parando mídia</translation>
        </message>
    </context>
</TS>
