<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi 현미경</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>보기 FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>종료</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>프리즈</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>줌 인</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>오버레이</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>분석 중...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>프로파일</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>USB 에서 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>팁</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>자동 센터</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>테스트 버튼 :</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>일시 정지</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>기타 설정 ...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>현미경 프로파일 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>프로파일&#xA;가져오기</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>현미경 프로파일 (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>프리즈</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>라이브</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>PNG 저장</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>PDF 저장</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>이미지 저장</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>보고서 저장</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>분석 실패</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>파이버를 분석할 수 없었습니다 . 라이브 비디오가 파이버를 보여주는지 , 다시 테스트하기 전에 초점이 맞춰져있는지 확인하십시오 .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>이미지 선택</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>이미지 파일 (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>파이버 검사 및 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>파이버 정보</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>정의된 추가 파이버 정보가 없습니다</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>프로파일:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>팁 :</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>검사 요약</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>결함</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>스크래치</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>또는</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>기준</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>한계</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>갯수</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>어떤</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>해당 없음</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>검사 요약을 생성하는데 실패했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>저배율</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>고배율</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>스크래치 테스트를 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>작업 :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>케이블 :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>커넥터:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>회사명 :</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>기술자:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>고객:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>위치:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>작업 :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>케이블 :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>커넥터:</translation>
        </message>
    </context>
</TS>
