<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>Quadros de {1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>Quadros de {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>Pacotes de {1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>Pacotes de {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Erro: Ocorreu um excesso de tempo limite ao tenter recuperar {2}, verifique sua conexão e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} burst de quadro: falha</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} burst de quadro: passa</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} de {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} burst de pacote: falha</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} burst de pacote: passa</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} Recuperando {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   Você deseja substituir?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Pressione OK para tentar novamente</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} Testando ID da VLAN {2} para {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Aguardando...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       Você pode alterar o nome para criar uma nova configuração.</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 Top Talkers (em um total de {1} conversas IP)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>As 50 principais conversas TCP retransmissoras (de um total de {1} conversas)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abortar</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Abortar o teste</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Ativo</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>A ativação do circuito não foi bem sucedida. Teste abortado por VLAN ID.</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Teste real</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Acrescentar faixa</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Uma taxa de perda de quadros que excedeu o limiar configurado para perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>Após executar os testes manuais, ou a qualquer momento em que for necessário, você pode</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Encontrado loop de hardware</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>Não foi encontrado loop de hardware</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Todos os testes</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Um aplicativo loopback não é um aplicativo compatível</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Não está disponível uma medição de vazão máxima</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>Não foi encontrado loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analisar</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analisando</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>e</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>e Teste RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>Um circuito LBM/LBR foi encontrado.</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>Um circuito LBM/LBR foi encontrado.</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Loop permanente encontrado</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Acrescente o registro de progresso ao final do relatório</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Nome do aplicativo</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Tempo total aproximado:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Uma faixa de valores teóricos de vazão FTP serão calculados com base nos valores das medições reais do link.  Preencha a largura de banda, o atraso de volta e o encapsulamento medidos para o link.</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Ocorreu tempo limite na resposta.&#xA;Não houve resposta ao último comando&#xA;durante {1} segundos.</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation>Assumindo existência de hard loop        </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Assimétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>Assimétrico transmite da extremidade próxima para a extremidade remota em modo upstream e da extremidade remota para a extremidade próxima em modo downstream. O modo combinado executará o teste duas vezes, transmitindo sequencialmente na direção upstream, usando a configuração local, e, em seguida, na direção downstream, usando a configuração remota. Use o botão para sobrescrever a configuração remota com a configuração local atual.</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Tentando</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Tentando loop up</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Tentando fazer Log-on no servidor ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>Tentativas para circular falharam. Teste parando</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>Tentativa de loop up sem sucesso</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto negociação</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Auto-negociação terminada</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Configurações de auto negociação</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Status da auto negociação</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponível</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Burst médio</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Taxa média de pacote</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Tamanho médio de pacote</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Resultados médio e média máxima do teste de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Latência (RTD) média:</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Latência (RTD) média: N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Média, jitter de pacote:</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Média, jitter de pacote: N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Jitter médio de pct (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>Taxa média</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Granularidade em quadros 'back to back':</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Granularidade em quadros 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Teste de quadros 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Resultados do teste de quadros 'back to back':</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Tempo máximo do ensaio 'back to back':</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Tempo máximo do ensaio 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Resultados do teste 'back to back':</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>Retorna ao resumo</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balão::mensagem</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularidade de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Granularidade de banda (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Precisão na medição da largura de banda:</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Precisão na medição da largura de banda</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>Teste básico de carga</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Início da faixa:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation> Bits</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Ambos</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Tanto Rx como Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>Os endereços local e remoto de IP estão indisponíveis</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>De baixo para cima</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Crédito Buffer&#xA;(necessita Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Duração do ensaio de créditos de buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Duração do ensaio de créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Teste de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Resultados do teste de crédito de buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Vazão de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Vazão de créditos de buffer ({1} bytes:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Throughput de crédito Buffer&#xA;(exige crédito Buffer)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Teste de vazão de créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Resultados do teste de vazão de crédito de buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Tamanho do buffer</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Granularidade do burst (quadros)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>Observando as retransmissões TCP versus a utilização da rede, ao longo do temo, é possível correlacionar baixo desempenho da rede a condições de perdas na rede, como congestionamento.</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>Examinando a tabela de conversações IP, podemos identificar os "Top Talkers" (conversadores principais) pelos bytes ou pelos quadros.  A nomenclatura "S &lt;- D" e "S -> D" se refere à direção do tráfego de bytes e quadros, da origem para o destino ou do destino para a origem.</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(byte)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation> bytes</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(byte)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Bytes&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Bytes&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Comprimento do quadro&#xA;calculado</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Comprimento do pacote&#xA;calculado</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Calculando ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>Impossível prosseguir!</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Resumo da análise da captura</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Duração de captura</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturar&#xA;tela</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>CUIDADO!&#xA;&#xA;Tem certeza que quer excluir&#xA;permanentemente esta configuração?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Taxa Cfg (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Taxa Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Taxa Cfg (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>ID do chassi</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>Iten(s) Rx marcado(s) serão usados para configurar origens de filtro.</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>Iten(s) Tx marcado(s) serão usados para configurar destinos Tx.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Verificando loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Verificando loop de hardware</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Verificando existência de loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Verificação de um circuito LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Verificando loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Verificando deteção de portas half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Verificando existência de quadros ICMP</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Verificando possíveis retransmissões ou utilização de alta largura de banda</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Verificando hard loop</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Verificação de circuito LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Verificando o loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Checando as estatísticas  de hierarquia de protocolo</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Verificando disponibilidade do endereço da origem ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>Marcar essa caixa faz com que as configurações do teste sejam restauradas para seus valores iniciais, ao sair do teste. Para teste assimétrico, eles serão armazenados nos lados tanto local como remoto. Restaurar configurações causará a reinicialização do link.</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Verifique as configurações da porta entre o IP origem e o dispositivo ao qual ela está conectada; assegure-se de que não existe a condição half-duplex.  Pode-se obter maior secionamento movendo o analisador para mais próximo do IP de destino; determine se as retransmissões são eliminadas, de modo a isolar o(s) link(s) com falha.</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Escolher um arquivo de captura, para analisar</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>Escolha&#xA;arquivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Escolha a precisão de medição da largura de banda que deseja&#xA;(recomenda-se 1%, para um teste de duração menor).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Escolah o tipo de login para o controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Escolha o tamanho preferido do quadro ou do pacote</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Escolher a granularidade com a qual gostaria de executar o teste Um a Um em Ordem (Back to Back)</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Escolha a granularidade na qual deseja executar o teste de perda de quadros.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Escolha a largura máxima de banda para a qual o circuito está configurado.  O aparelho usará esse número como largura máxima de banda, para transmitir, reduzindo a duração do teste:</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Escolha o tamanho máximo do crédito de buffer.&#xA;O teste iniciará com este número, reduzindo o comprimento do teste:&#xA;OBS.:  O dispositivo remoto incorporando o tráfego deve ser configurado com&#xA;os seguintes parâmetros:&#xA;&#xA;1. Controle de fluxo LIGADO&#xA;2. {1}&#xA;3. {2} Créditos de buffer ajustados para o mesmo valor como acima.</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Escolher o tempo máxido de tentativa para o teste Um a Um em Ordem.</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Escolha os valores mínimo e máximo de carga a usar com os procedimentos de teste de cima para baixo' e de baixo para cima'</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Escolha o número de ensaios do teste 'back to back' que deseja executar para cada tamanho de quadro.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Escolha o número de ensaios do teste de latência (RTD) que deseja executar para cada tamanho de quadro.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Escolha o número de ensaios do teste de jitter de pacote que deseja executar para cada tamanho de quadro.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Escolha o percentual permitido para tolerância de perda de pacotes de vazão.&#xA;OBS: Um ajuste > 0,00 NÃO está de acordo com a RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Escolha o período de tempo que irá durar cada ensaio de latência (RTD).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Escolher o tempo que cada Pacote de Tremulação (Jitter Packet) irá durar.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Escolher o tempo para o qual um índice pode ser enviado sem erro para passar o Teste de Rendimento.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Escolher o tempo de duração que gostaria para cada tentativa de Perda de Quadro.</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Escolha a duração do ensaio para o teste de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Escolhe que pocedimento usar no teste de perda de quadros.&#xA;OBS.: O procedimento RFC2544 é executado a partir da largura máxima de banda e diminui da granularidade da banda a cada ensaio, terminando após dois ensaios consecutivos nos quais não há perda de quadros.</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Cisco Discovery Protocol</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>Foram detectadas mensagens Cisco Discovery Protocol (CDP) nesta rede e a tabela lista os endereços MAC e portas que informaram configurações half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Limpar tudo</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Clique no botão "Results" para mudar para a interface padrão do usuário.</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violações de código</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Combinado</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>Comentários</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentários</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Comunicação estabelecida com sucesso, com a extremidade remota</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>A comunicação com a extremidade remota não pôde ser estabelecida</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>A comunicação com a extremidade remota foi perdida</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>Completar</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completada</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>concluído&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Configs</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuração</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>Nome da configuração</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Nome da configuração:</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Nome da configuração</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>É necessário o nome da configuração</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Confoguração somente leitura</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Resumo da configuração</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Configure o(s) item(ns) marcado(s)</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Configure em quanto tempo {1} enviará o tráfego.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Confirmar a substituição da configuração</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Confirmar exclusão</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONECTADOS</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Conectar ao aplicativo Test Measurement</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continuar em half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Continuando em modo half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Copiar configuração local&#xA;para configuração remota</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Copiar&#xA;selecionada</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>Impossível incorporar a extremidade remota</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Criar relatório</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>créditos</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation> (Créditos)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA;Script atual: {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Seleção atual</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Taxa de bits de dados</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Taxa de bytes de dados</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Nível de dados parado</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo de dados</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Modo de dados configurado para PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Tamanho de dados</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>Data e hora</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>dias</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Atraso, atual (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Atraso, Atual (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Excluir</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Endereço do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Configuração do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>Endereço IP&#xA;do destino</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>Não encontrado MAC do destino para endereço IP {1}</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>Encontrado MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>End MAC dest</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>rótulo de detalhe</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalhes</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>detectada</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>Detetor</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Largura de banda do link detectado</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Detecta mais quadros do que foram transmitidos para a banda {1} - Teste inválido.</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Determinando a vazão simétrica</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Detalhes do dispositivo   </translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ID de dispositivo</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>Parâmetros DHCP não disponíveis</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>Parâmetros DHCP encontrados.</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Dispositivos descobertos</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Descobrindo</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Determinando tipo de circuito final...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Descoberta&#xA;não&#xA;disponível&#xA;no momento</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Exibir por:</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Direção downstream</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation>Deseja prosseguir assim mesmo? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duração</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Ativado</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Ativar Camada 2 de Extensão do Teste de Tráfego</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Encapsulamento:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Fim</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data de término</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>Final da faixa:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Preencha o endereço IP ou nome do servidor com o qual deseja executar o teste FTP.</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Preencha o nome de login para o servidor ao qual deseja se conectar</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Preencha a senha da conta que deseja usar</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Preencha o novo nome da configração&#xA;(Use somente letras, números, espaços, hifens e sublinhados):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Erro: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ERRO: Ocorreu um resposta de tempo limite&#xA;Não houve resposta dentro</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Erro: Não foi possível estabelecer uma conexão</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Erro de Contagens</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Quadros com erro</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Erro ao carregar arquivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Erro: Falha na resolução do nome do DNS primário</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Erro: impossível localizar o site</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Tempo restante estimado</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Tempo estimado restante</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     Relatório do teste Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>Log de eventos está cheio.</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Foram encontradas retransmissões excessivas</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sair</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>Sair do J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Vazão esperada</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>A vazão esperada é</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>A vazão esperada não está disponível</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>Botão "Expert RFC 2544 Test".</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explícito (porta-e)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Explícito (Fabric/N-Port)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation>Não foi possível completar o login explícito. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALHA</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>FALHA</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>Extremo remoto é um conjunto de teste Ethernet de Classe Smart JDSU</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>A ponta remota é um conjunto de teste Ethernet Smart Class Viavi</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Taste FC</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>O teste FC é executado usando a carga útil de teste Acterna</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Aceita FDX</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Relatório do teste do canal de fibra óptica</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>Configuração de arquivo</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome do arquivo</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Arquivos</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>Tamanho do arquivo</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>Tamanho do arquivo:</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>Tamanho do arquivo</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>Tamanho do arquivo: {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>Tamanhos dos arquivos:</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>Tamanhos dos arquivos</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>Arquivo transferido rapidamente. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Encontrando a vazão esperada</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Como encontrar os "Top Talkers"</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>Primeiras 50 portas half-duplex (de um total de {1})</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>Primeiras 50 mensagens ICMP (de um total de {1})</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Tipo de login para controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Pastas</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation>para cada quadro é reduzido para a metade, para compensar o comprimento duplo da fibra óptica.</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>encontrado</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Loop ativo encontrado.</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Loop em hardware encontrado.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Comprimento&#xA;do quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Comprimento do quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Comprimento do quadro (byte)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Comprimento de quadros:</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Comprimento de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Comprimentos de quadros a testar</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Perdas de quadros (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdas de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Perdas de quadros {1} Bytes:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Perdas de quadros {1} Bytes</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Granularidade da banda em perda de quadros:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Granularidade da banda em perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Largura máxima de banda para perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Largura mínima de banda para perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Frame Loss Rate</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Relação de perdas de quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Teste de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Procedimento de teste de perda de quadros:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procedimento de teste de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Resultados do teste de perda de quadros:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolerância para perda de pacotes (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Duração do ensaio de perda de quadros:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Duração do ensaio de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Quadro ou pacote</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>quadros</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Quadros</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>tamanho do quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamanho do quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Tamanho do quadro:  {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Quadros&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Quadros&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation> (qdrs)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation> (quadro/seg)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>Vazão FTP</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>Teste de vazão FTP&#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>Relatório do teste de vazão FTP completado!</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>Relatório do teste de vazão FTP</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Cheio</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>Obter info de PCAP</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Metade</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Portas half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Loop de hardware</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation> (Hardware&#xA;ou Ativo)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation> (Hardware,&#xA;Permanente&#xA;oo Ativo)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Aceita HDX</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Alta utilização</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Início</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>horas</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>Teste de vazão HTTP</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>Relatório do teste de vazão HTTP</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>Código&#xA;ICMP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>Messages ICMP</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Se os contadores de erros estão sendo incrementados de forma esporádica, execute o</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Se o problema persistir, 'Reset Test to Defaults' (restaurar padrões do teste) no menu Tools (ferramentas).</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Se não for possível solucionar o problema com os erros esporádicos, você poderá configurar</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implícito (link transparente)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>informações</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Inicializando comunicações com</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Para determinar a banda onde o</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>A taxa de entrada para os lados remoto e local não correspondem</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Observam-se problemas intermitentes na linha.</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Erro interno</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Erro interno - Reiniciar PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config inválido</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Configuração IP inválida</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>Endereços IP</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>Conversas IP</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>está saindo</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>está iniciando</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Teste Jitter</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck está completo</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>O J-QuickCheck perdeu o link ou não conseguiu estabelecer um link</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Kill</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>Teste de Tráfego L2 pode ser relançado executando J-QuickCheck (J-Verificação Rápida) outra vez.</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latência</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>Testes de Latência (RTD) e Pacote Jitter</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Média de latência (RTD): N/A</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Limiar para passar na latência (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Limiar para passar na latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Latência (RTD)&#xA;(exige vazão)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Resultados de latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Teste de latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Resultados do teste de latência (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Resultados do teste de latência (RTD): ABORTADO   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Resultados do teste de latência (RTD): FALHA   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Resultados do teste de latência (RTD): PASSA   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Resultados do teste de latência (RTD) ignorados</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Teste de latência (RTD) ignorado</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation>Limiar em latência (RTD): {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Limiar em latência (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Duração do ensaio de latência:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Duração do ensaio de latência</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Latência (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Nível 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Nível 1 / 2&#xA;Saúde da Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Nível 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Encontrada presença do link no nível 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Teste Rápido da Camada 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Nível 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>Nível 3&#xA;Saúde do IP</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Nível 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>Nível 4&#xA;Saúde do TCP</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Loop LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Comprim</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Link encontrado</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Link Layer Discovery Protocol</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Link perdido</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Velocidade do link detectada no arquivo de captura</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Porta de escuta</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Formato da carga</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>CARREGANDO ... Aguarde</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>Endereço de IP de destino local configurado para</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>Endereço de MAC de destino local configurado para</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>Porta de destino local configurada para</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>Tipo loop local configurado para Unicast</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Porta local</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>Endereço de IP remoto local configurado para</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation>Número de série local</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Configuração local</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation>Revisão de software local</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>Filtro de IP local configurado para</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>Filtro de MAC local configurado para</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>Filtro de porta local configurado para</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Resumo local</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation>Nome do instrumento de teste local</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Localize o dispositivo com o(s) endereço(s) MAC de fonte e a(s) porta(s) listadas na tabela e assegure-se de que as configurações duplex estão ajustadas para "full" e não para "auto".  Não é incomum um host e o dispositivo de rede serem ambos ajustados para "auto" e o link, incorretamente, negociar half-duplex.</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>Localização</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Localização</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Login:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Login</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Nome de login:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Nome de login</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Falha no loop</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Desincorporando a unidade da extremidade remota...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Incorporando a unidade da extremidade remota...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Status desconhecido do loop</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Falha no Loop up</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Loop up bem sucedido</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Incorporado com sucesso</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>Detectada perda do link do nível 2!</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Perdido</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Quadros perdidos</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Gerenciamento de endereços</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>Tipo MAU</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MÁX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation> (máximo {1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Média máxima</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Média máx., jitter de pacotes:</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Média máx., jitter de pacotes: N/A</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Máximo Jitter médio de pct (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Largura de banda máx. (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Largura de banda máx. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Tamanho máximo do buffer</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>Latência máxima, a média está autorizada a "passar" no teste de latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Máximo jitter de pacote, à média é permitido "passar" no teste de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Créditos máximos de buffer RX</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Largura máxima de banda para o teste:</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Largura máxima de banda para o teste</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Máxima vazão medida:</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Limite máximo de tempo de {1} por ID da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Limite máximo de tempo de 7 duas por ID da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Tempo máximo do ensaio (segundos)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Créditos máximos de buffer TX</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>Taxa máx</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Alcançou o máximo de tentativas de retransmissão. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Medido</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Taxa medida (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Taxa medida</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Taxa medida (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Precisão das medições</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Medindo a vazão a {1} créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensagem</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Mínimo</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Percentual mínimo de largura de banda</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>O percentual mínimo de largura de banda exigido para "passar", para o teste de fluxo:</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Limite mínimo de tempo de 5 segundos por ID da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Taxa mín.</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>mín.</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>minuto(s)</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>minutos</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modelo</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Modificar</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>Encapsulamento MPLS/VPLS atualmente não é suportado ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/A (hard loop)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Rede ligada</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Utilização da rede</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>Não se detectou a utilização da rede como sendo excessiva, mas este gráfico fornece um gráfico de utilização da rede</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>Não se detectou a utilização da rede como sendo excessiva, mas esta tabela fornece uma lista dos "top talkers" em IP</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Novo</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Novo nome da configuração</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Nova URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Seguinte</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Não.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>Não foi encontrado aplicativo compatível</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt;NENHUMA CONFIGURAÇÃO DISPONÍVEL></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>Nenhum arquivo selecionado para teste</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>Não foi encontrado loop de hardware</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>Não&#xA;JDSU&#xA;Dispositivos&#xA;Descobertos</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>Não foi detectado link do nível 2!</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>Nenhum loop pôde ser estabelecido</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>Nenhum loop pôde ser estabelecido ou encontrado</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>Não foi encontrado Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>Não foi detectado nenhum aplicativo em execução</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>ESTÁ DE ACORDO com a RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Não está conectado</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Não determinada</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>OBSERVAÇÃO:  Um ajuste > 0,00 NÂO</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Obs.:      Assume um hard-loop com menos de 2 créditos de buffer.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;Observação: Assume um loop rígido com créditos de buffer menos de 2.&#xA; Este teste é inválido.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Obs.:      Com base na presunção de hard loop, o cálculo de créditos de buffer mínimo</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Obs.:      Com base na presunção de hard-loop, as medições de vazão são feitas duas vezes.</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Obs.:      Se você usar uma tolerância para perda de quadros, o teste não estará de acordo com</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Obs.</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Não selecionado</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Nenhum&#xA;dispositivo&#xA;Viavi&#xA;descoberto</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>Saindo, agora...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>Verificando</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Verificando agora com {1} créditos.  Isso levará {2} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Número de ensaios 'back to back':</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Número de ensaios 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Número de fracassos</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Número de IDs testadas</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Número de ensaios de latência (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Número de ensaios de latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Número de ensaios de jitter de pacote:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Número de ensaios de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Número de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Número de testes bem sucedidos</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Número de ensaios:</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Número de ensaios</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>de</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Desliga</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>DESLIGADO</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>de quadros foram perdidos durante um segundo.</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>of J-QuickCheck rendimento experado</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation> (% da taxa de linha)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% da taxa de linha</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>da taxa de linha</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ligado</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>LIGADO</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation>* Somente {1} ensaio(s) forneceram dados utilizáveis *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation> (LIGADO ou DESLIGADO)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Quadros OoS</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID do originador</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Fora do intervalo</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        Resultado geral do teste: {1}        </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    Resultado geral do teste: ABORTADO   </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Excesso de escala</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>durante os últimos 10 segundos, apesar de que o tráfego deveria ter sido parado</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Média, jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Limiar para passar em jitter de pacote:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Limiar para passar em jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Jitter de pacotes&#xA;(exige vazão)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Resultados do jitter de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Teste de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Resultados do teste de jitter de pacote: ABORTADO   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Resultados do teste de jitter de pacote: FALHA   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Resultados do teste de jitter de pacote: PASSA   </translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation>Limiar em jitter de pacote: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Limiar de jitter de pacote (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Duração do ensaio de jitter de pacote:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Duração do ensaio de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Comprimento&#xA;Pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Comprimento do pacote (byte)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Comprimentos de pacote:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Comprimentos de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Comprimentos de pacotes a testar</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>tamanho do pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Tamanho do pacote</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASSA</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Passa / Falha</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>PASSA/FALHA</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Taxa de aprovação (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Taxa de aprovação ((frm/sec)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Taxa de aprovação (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Taxa de aprovação (pkts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Senha:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Senha</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Pausar Advrt</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Aceita pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Det pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Quadro(s) de pausa detectado(s)</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Quadro(s) de pausa detectado(s) - Teste inválido</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Erro de análise no arquivo PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>Arquivos PCAP</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>Pendente</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Executando a limpeza</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation> (Permanente&#xA;ou Ativo)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Jitter Pct (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Comprimento do Pct</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perda pct</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation> (pct)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pcts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation> (pct/sec)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Plataforma</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Verifique se existe sync e link,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Verifique se você está adequadamente conectado,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Verifique se você está adequadamente conectado,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Escolha outro nome para a configuração.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Favor introduzir um Nome de Arquivo para salvar o relatório (máximo % {1} de caracteres) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Digite qualquer comentário que você tenha (máx {1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Digite qualquer comentário que possa ter (máx %{1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira quaisquer comentários que você tenha (no máx. {1} caracteres)&#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Digite seu nome de cliente (máx %{1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Por favor, isira o nome do cliente ( caracteres max %{1})</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira o nome do seu cliente&#xA;(no máx. {1} caracteres) &#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira o nome do seu cliente&#xA;(no máx. {1} caracteres) &#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira o nome do seu cliente&#xA;(no máx. {1} caracteres) &#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Digite seu nome de técnico (máx {1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Digite seu nome de técnico (máx %{1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira o nome do seu técnico (no máx. {1} caracteres)&#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Digite o seu local de teste (máx {1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Digite o seu local de teste (máx %{1} caracteres)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Insira o seu local de teste (no máx. {1} caracteres)&#xA;(Use apenas letras, números, espaços, traços e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Pressione o botão "Connect to Remote"</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Verifique o desempenho do link usando um testador manual de tráfego.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Por favor, verifique seu endereço local e remoto de IP e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Por favor verifique seu endereço local de IP e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>Verifique seu endereço IP remoto e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Por favor verifique seu endereço remoto de IP  e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Aguarde...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Aguarde...   </translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Aguarde enquanto o arquivo PDF é criado.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Aguarde enquanto o arquivo PDF é criado.&#xA;Isso pode levar até 90 segundos ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Porta:</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Porta {1}: Você gostaria de salvar um relatório de teste?&#xA;&#xA;Pressione "Yes" ou "No".</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>ID da porta</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Porta:				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Porta:				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP ativo</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Falha na autenticação do PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Falha desconhecida no PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>Falha no PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Falha no PPP LCP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE ativo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Falha no PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE inativo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE iniciado</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>Status PPPoE: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Tempo limite do PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE Up</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>Falha no PPPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Tempo limite do PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>Falha PPP desconhecida</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>Falha em PPP up</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Falha na ligação do PPP</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>Pressione "Close" para retornar à tela principal.</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>Pressione "Exit" para retornar à tela principal ou "Run Test" para executar novamente.</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Pressione&#xA;o botão&#xA;'Refresh'&#xA;para&#xA;descobrir</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Pressione o botão "Exit J-QuickCheck" para sair do J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Pressione o botão 'Refresh', abaixo, para descobrir dispositivos Viavi atualmente na subrede. Selecione um dispositivo para ver os detalhes, na tabela à direita. Se 'Refresh' não estiver disponível, verifique para certificar-se de que 'Discovery' está ativado e que você tem sync e link.</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Pressione o botão "Run J-QuickCheck"&#xA;para verificar a configuração de teste local e remota e a banda disponível</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Anterior</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>Progresso</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Propriedade</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Próximas etapas propostas</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Fornecedor</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-em-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Randômico</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> Faixa</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Faixa</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Taxa</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Taxa (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>taxas mais baixas, o link pode estar tendo problemas de ordem geral, não relacionados com a carga máxima.</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>Recebidos {1} bytes de {2}</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Quadros recebidos</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Recomendação</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Configuração recomendada para teste manual:</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Atualizar</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Endereço IP remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Loop remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation>Número de série do remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>Número de série do remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Configuração remota</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Não foi possível restaurar as configurações remotas</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation>Revisão de software remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Versão do software do remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Resumo remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>Nome do instrumento de teste remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Nome do instrumento de teste remoto</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Remover faixa</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID do respondente</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>IP do roteador&#xA;que responde</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>Reinicie o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Restabelecer as configurações prévias antes de sair. </translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Restaurando configurações do conjunto de teste remoto ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>Restringir RFC a</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>O resultado do teste básico de carga não está disponível, clique em "Proposed Next Steps" para obter possíveis soluções</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Resultados a monitorar:</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Retransmissões</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Foram encontradas retransmissões&#xA;Analisando a ocorrência de retransmissões ao longo do tempo</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>Foram encontradas retransmissões. Exporte o arquivo via USB para mais análises.</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>Recuperação de {1} foi abortada pelo usuário</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>Retornar para a interface RFC 2544 do usuário, clicando no </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>Relatório do teste Ethernet RFC 2544</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>Teste RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>O teste RFC 2544 é executado usando a carga útil de teste Acterna</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>O teste RFC/FC não pode ser executado enquanto o Multistreams Graphical Results estiver sendo executado</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Modo Rfc</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>Det R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Executar</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>Executar o teste FC</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Executar o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>Executar $12rápido::testeDuraçãoNome</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Em funcionamento</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Executando o teste com carga {1}{2}.</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Executando o teste com carga {1}{2}.  Isso levará {3} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>Executar o teste RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Executar Script</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Mbps Rx, L1 atual</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Somente Rx</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>Salvar o relatório do teste de vazão FTP</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>Salvar o relatório do teste de vazão HTTP</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>Salvar relatório de teste de varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> largura de banda em escala</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Cópia de tela capturada</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Script abortado.</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>segundos</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation> (seg)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>seg</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Selecione um nome para a configuração copiada</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Selecione um nome para a nova configuração</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Selecione uma faixa de IDs da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Direção Tx selecionada:</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Direção Tx selecionada&#xA;Downstream (destino)</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Direção Tx selecionada&#xA;Upstream (Origem)</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Aviso de seleção</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Selecione "OK" para modificar a configuração&#xA;Edite o nome para criar uma nova configuração&#xA;(Use somente letras, números, espaços, hifens e sublinhados)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Selecionar configuração do teste:</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Selecione a propriedade pela qual deseja ver listados os dispositivos descobertos.</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Selecione os testes que desejaria executar:</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>Selecionar URL</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Selecionar qual formato usar para carregar as configurações relacionadas. </translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Enviando tráfego para</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>envia tráfego, o rendimento esperado por J-QuickCheck (J-VerificaçãoRápida) será dimensionado por este valor. </translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID da sequência</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Número de série</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID: do servidor</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>ID do servidor</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome do serviço</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Instalação</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Exibir o status passa/falha</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;Exibir o status passa/falha para:</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Tamanho</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Pular</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Ignorando os teste de latência (RTD) e contnuando</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Rev Software</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation>Revisão de software</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Endereço da origem</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>Enereço de origem não disponível</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Estabelecida a disponibilidade da origem...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID da origem</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>Endereço&#xA;IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>O endereço IP da origem é o mesmo do endereço do destino</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC da origem</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Endereço MAC&#xA;da origem</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Especificar a largura de banda do link</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Velocidade</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocidade (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>PARTIDA</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data do início</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Iniciando teste básico de carga</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Iniciando o esnsio</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>PARADA</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Estude o gráfico de modo a determinar se as retransmissões TCP correspondem a uma utilização degradada da rede.  Observe a guia de retransmissões TCP para determinar o IP de origem que está causando significativas retransmissões TCP. Verifique as configurações da porta entre o IP origem e o dispositivo ao qual ela está conectada; assegure-se de que não existe a condição half-duplex.  Pode-se obter maior secionamento movendo o analisador para mais próximo do IP de destino; determine se as retransmissões são eliminadas, de modo a isolar o(s) link(s) com falha.</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>Sucesso!</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Sucesso</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumo</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Resumo dos valores medidos:</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Resumo da Página {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridade de usuário da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>O modo simétrico trasmite e recebe na extremidade próxima usando loopback. Assimétrico transmite da extremidade próxima para a extremidade remota em modo upstream e da extremidade remota para a extremidade próxima em modo downstream.</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simetria</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Clique em um nome de configuração para selecioná-la</translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	Obter arquivo de {1} MB....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	Coloque arquivo de {1} MB....</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Taxa: {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Taxa: {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Quadros Rx {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              ATENÇÃO!&#xA;&#xA;	    Você tem certeza que deseja permanentemente&#xA;	           deletar essa configuração?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Por favor use somente letras, números, espaços,&#xA;		   traço e underline!</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Essa configuração é somente de leitura&#xA;		   e não pode ser deletada.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Você deve entrar com um novo nome para a &#xA;		           configuração usando o teclado.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   A configuração especificada já existe.</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Tempo: {1} segundos&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Quadros Tx {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>Falha para estabelecer conecção TCP host. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>O TCP Host encontrou um erro. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>Retransmissões TCP</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome do técnico</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Encerramento</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Teste abortado pelo usuário.</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Teste na configuração de largura de banda máxima configurada a partir de Configuração - aba Todos os testes</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>Teste manual com taxas menores de tráfego. Se ainda estiver obtendo erros, mesmo com</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Teste completo</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Configuração do teste:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuração do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Duração do Teste</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Duração do teste: Pelo menos 3 vezes a duração configurada para o teste.</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Banda Testada</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Execução do teste terminada ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Testando {1} créditos </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Testando {1} créditos </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Testando em </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Testando a conexão... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation>Nome do instrumento de teste</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>O teste stá começando</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Registro do teste:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nome do teste:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Nome do teste:			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Nome de teste:			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procedimento de teste</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Registro de progresso do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Faixa de teste (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Faixa de teste (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Resultados do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Configuração do conjunto de teste</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Testes a executar:</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Testes a executar</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>O teste foi abortado</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>O loop remoto ativo falhou e nenhum loop físico foi encontrado</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>O loop remoto ativo falhou e nenhum loop permanente ou físico foi encontrado</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>A taxa média para {1} foi de {2} kbps</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>A taxa média para {1} foi de {2} Mbps</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>O arquivo excede o limite de 50.000 pacotes para JMentor</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>o limiar de tolerância para perdas de quadros de modo a tolerar baixas taxas de perda de quadros.</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>O protocolo de mensagens de controle da internet (Internet Control Message Protocol - ICMP) é mais amplamente conhecido no contexto do "Ping" ICMP. A mensagem "ICMP Destination Unreachable" (impossível alcançar o destino ICMP) indica que um destino não pôde ser alcançado pelo roteador ou pelo dispositivo de rede.</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>A contagem dos quadros L2 Filter Rx Acterna continuou a aumentar</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>Loop LBM/LBR falhou.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>Os endereços IP de origem, tanto local como remoto, são idênticos</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation>Os ajustes da configuração local foram copiados com sucesso para a configuração remota</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>O endereço local de IP está indisponível</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>O MTU medido é muito pequeno para continuar. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>O nome escolhido já está sendo usado.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>A porta de elemento da rede à qual estamos conectados está provisionada para half-duplex. Se isto estiver correto, pressione o botão "Continue in Half Duplex". Caso contrário, pressione "Exit J-QuickCheck" e reprovisione a porta.</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>O gráfico de utilização da rede exibe a banda usada por todos os pacotes, no arquivo de captura, ao longo do tempo de duração da captura.  Se as retransmissões TCP também tiverem sido detectadas, é aconselhável estudar os resultados do nível TCP, retornando para a tela principal de análise.</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>o número de creditos de buffer em cada etapa, para compensar o comprimento duplo de fibra óptica.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Calculo teórico</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Valores teóricos e medidos:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>A porta parceira (elemento de rede) tem AutoNeg DESLIGADO e o fluxo esperado não está disponível, portanto, a porta parceira provavelmente está em modo half-duplex. Se o half-duplex na porta parceira não estiver correto, mude as configurações da porta para full-duplex e execute novamente o J-QuickCheck. Em seguida, se o fluxo esperado medido estiver mais razoável, você poderá executar o teste RFC 2544. Se o fluxo esperado ainda não estiver disponível, verifique as configurações da porta no lado remoto. Talvez exista um descasamento do modo na porta HD para FD.&#xA;Se o half-duplex na porta parceira estiver correto, vá para Results -> Setup -> Interface -> Physical Layer e altere a configuração Duplex de Full para Half. Em seguida, retorne para o script RFC 2544 (Results -> Expert RFC2544 Test), saia do J-QuickCheck, vá para Throughput Tap, selecione Zeroing-in Process RFC 2544 Standard (Half Duplex) e execute o teste RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>Existe um problema de comunicação com a extremidade remota.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>O dispositivo de looping remoto não responde ao comando loopback da Viavi mas retorna os quadros transmitidos ao dispositivo local com os campos de endereço de origem e destino intercambiados.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>O dispositivo de looping remoto responde ao comando loopback Viavi e retorna os quadros transmitidos ao dispositivo local com os campos de endereço de origem e destino intercambiados</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>O dispositivo de looping remoto retorna os quadros transmitidos sem troca ao dispositivo local</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>O dispositivo de loop remoto suporta OAM LBM e responde ao quadro recebido LBM, transmitindo o quadro LBR de volta ao disposivito local.</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>O lado remoto está configurado para encapsulamento MPLS</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>O lado remoto parece ser um aplicativo loopback</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>O endereço remoto de IP está indisponível</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA;O relatório foi salvo como "{1}{2}", no formato PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>O relatório foi salvo como "{1}{2}", no formato PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>O relatório foi salvo como "{1}{2}", nos formatos PDF, TXT e LOG</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>O relatório foi salvo como "{1}{2}", nos formatos TXT e LOG</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>O IP do roteador que responde não pode encaminhar o pacote até o endereço IP do destino, portanto, a identificação do problema deve ser feita entre o IP do roteador que responde e o destino.</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>O teste RFC 2544 não admite encapsulamento MPLS.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>O laser de transmissão foi ligado&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>O laser de transmissão está desligado!&#xA;Deseja ligar o laser?&#xA;Pressione "Yes" para ligar o laser ou "No" para cancelar.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>O laser de transmissão está desligado!  Gostaria de ligar o laser?&#xA;&#xA;Pressione "Yes" para ligar e "No" para cancelar.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>O teste de varredura da VLAN não pode ser completado com o OAM CCM ligado.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>O teste de varredura da VLAN não pode ser configurado adequadamente.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Esta configuração é somente leitura e não pode ser modificada.</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Este deveria ser o endereço IP da extremidade remota, ao usar o modo assimétrico</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>Esta tabela identifica os endereços IP de origem que estão encontrando retransmissões TCP. Quando são detectadas retransmissões TCP, isso poderia ser devido a perda de pacotes downstream (em direção ao lado do destino)  Também poderia indicar que existe uma questão com a porta half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>Este teste é executado usando a carga útil de teste Acterna</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Este teste está inválido.</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Esse teste exige que o tráfico  tenha um encapsulamento VLAN. Certifique-se que a rede conectada irá fornecer um endereço IP para essa configuração.</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Isso levará {1} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Vazão (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Vazão ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Teste de vazão e de latência (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Testes de vazão e de jitter de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Tolerância de perda de quadros na vazão:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Tolerância de perda de quadros na vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Testes de vazão, latência (RTD) e jitter de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Limiar para passar na vazão:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Limiar para passar na vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Escala de Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Fator de Escala de Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Teste de vazão (throughput)</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>A duração do teste de vazão foi de {1} segundos.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Resultados de teste de vazão:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados do teste de vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Resultados de teste de vazão: ABORTADO   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Resultados do teste de vazão: FALHA   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Resultados de teste de vazão: PASSA   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Limiar de vazão (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation>Limiar da vazão: {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Limiar de vazão (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Duração do ensaio de vazão:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Duração do ensaio de vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Processo de zerar na vazão:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processo de zerar na vazão</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Hora de término</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Tempo por ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Tempo (segundos)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>Tempo&#xA;(seg)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Hora de início</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>Vezes visitado</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Para continuar, verifique a conexão do cabo e reinicie o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Para determinar o Rendimento Máximo escolher o método padrão RFC 2544 que se iguala à contagem de tx e rx ou o método de Viavi Melhorado que usa o medido L2 Avg % Util.</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>De cima para baixo</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Para salvar tempo de latência (RTD) em modo assimétrico, deve ser executado em apenas uma direção</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>para verificar se são eventos esporádicos ou constantes de perda de quadros.</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Total Bytes</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Total&#xA;Quadros</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Número do total</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Total Útil {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>Total Útil (kbps):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>Total Útil (Mbps):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>Para visualizar o relatório, selecione "View Report" no menu "Report", depois de sair de {1}.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>Até</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Tráfego: Constante com {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Resultados do Tráfego</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>O tráfego ainda estava sendo gerado, na extremidade remota</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>Laser de transmissão desligado!</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Quadros transmitidos</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Transmitindo downstream</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Transmitindo upstream</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Ensaio</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Ensaio {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Ensaio {1} concluído&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Ensaio {1} de {2}:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Duração do teste (segundos)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>ensaios</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Fazendo segunda tetantiva</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>Erro tshark</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>Créditos TX buffer para buffer</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Direção Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Laser Tx desligado</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Mbps Tx, L1 atual</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Somente Tx</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation>Impossível incorporar automaticamente a extremidade remota.</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>Impossível conectar ao aplicativo Test Measurement!</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>Impossível conectar ao aplicativo "Test Measurement"!&#xA;Pressione "Yes" para tentar novamente. "No" para cancelar.</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>Não foi possível obter um endereço DHCP</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>Incapaz de executar o teste RFC2544 com o loopback local ativado.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>Não foi possível executar o teste</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Incapaz de executar o teste de varredura da VLAN com o loopback local ativado.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>INDISP.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Identificador de unidade</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>PARA CIMA</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation> (para cima ou para baixo)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Direção upstream</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation> (us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>Usuário abortou o teste</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>Usuário cancelou o teste</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nome do usuário</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridade do usuário</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Usuário solicitou inativa</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Seleção do usuário&#xA;( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Seleção do usuário      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Use a tela 'Summary Status' para procurar eventos de erro.</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Usando</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Usando tamanho do quadro de</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Utilização e retransmissões TCP</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>Valores destacados em azul são de testes reais.</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Verificando que link esteja ativo...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>verifique seu endereço IP remoto e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>verifique seu endereço IP remoto e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi aperfeiçoado</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Faixas de ID da VLAN a testar</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Teste de varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>Relatório de teste de varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>Resultados do teste de varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>Relatório de teste da VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>Quadro VTP/DTP/PAgP/UDLD detectado</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>Aguardando término da auto-negociação ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>Aguardando MAC de destino para&#xA;  endereço IP</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>Aguardando parâmetros DHCP ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>Aguardando presença do link no nível 2...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>Aguardando link</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>Aguardando ativação de OWD, Sinc ToD, e Sinc 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Aguardando ARP bem sucedida ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>foi detectado no último segundo.</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Tamanho do site na internet</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Temos um loop ativo</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>Ocorreu um erro!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>Ao testar links Half-Duplex, selecione a norma RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Tamanho/capacidade da janela</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>a recomendação RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Você gostaria de salvar um relatório de teste?&#xA;&#xA;Pressione "Yes" ou "No".</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>Também é possível usar o gráfico de resultados da taxa atual deperda de quadros,</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>Não é possível executar este script a partir de {1}.</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Talvez seja necessário aguardar até que termine, para então reconectar</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Zerar na taxa máxima de vazão</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Zerar no melhor buffer de crédito</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processo de zerar em</translation>
        </message>
    </context>
</TS>
