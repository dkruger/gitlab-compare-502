<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Capacidad de la aplicación móvil</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth y WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Solo WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Por favor, reinicie el instrumento de prueba para que tengan efecto todos los cambios.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Pegar</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copiar</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Cortar</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Múltiple</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Abierto</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renombrar</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Nueva carpeta</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Disco</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>Pegar sobrescribirá los archivos existentes.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Sobrescribir archivo</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>No ha sido posible abrir el archivo debido a un error desconocido.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>No ha sido posible abrir el archivo.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>El archivo seleccionado se eliminará permanentemente.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>Los archivos seleccionados %1 se eliminarán permanentemente.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Eliminar archivo</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Por favor, introduzca el nombre de la carpeta:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Crear carpeta</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>No se pudo crear la carpeta "%1".</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>Ya existe la carpeta "%1.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Crear pliegue</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Pegando ...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Eliminando ...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Finalizado.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 falló.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>No se pudo pegar el archivo.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>No se pudieron pegar los archivos %1.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>No hay suficiente espacio libre.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>No se pudo eliminar el archivo.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>No se pudieron eliminar los archivos %1.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Volver</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opción</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>Actualizar URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nombre del Fichero</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Acción</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>Módulo</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Otra red inalámbrica</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Introduzca la información de la red inalámbrica a continuación.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Nombre (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Tipo de codificación:</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Actualización disponible</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync ha determinado que existe una actualización disponible.&#xA;Desea realizar ahora la actualización?&#xA;&#xA;La actualización finalizará cualquier prueba que se esté ejecutando. Al terminar el proceso, volverá a ejecutarse la prueba para finalizar la actualización.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Se encontró un error desconocido. Por favor, inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>No se pudo actualizar</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Información de instrumento:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Opciones de base:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Reiniciar instrumento&#xA;a valores predeterminados</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Exportar registros&#xA;a dispositivo USB </translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Copiar información del sistema&#xA;en archivo</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>La información del sistema se copió&#xA;en este archivo:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Esto requiere un reinicio y colocará las configuraciones del sistema y de prueba en sus valores predeterminados.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Reiniciar y restaurar</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Exportación de registro completada</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Imposible exportar registros. Por favor, verifique si se ha insertado correctamente el USB e intente de nuevo.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 Versión %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Comenzar de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Reiniciar a su estado por defecto</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Seleccione su método de actualización</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Actualizar desde archivos guardados en una memoria USB</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Red</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Descargar actualización desde un servidor Web a través de la red.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Introduzca la dirección del servidor de actualización:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Dirección del servidor</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Ingresar la dirección del servidor proxy, si se requiere para acceder al servidor de mejora:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Dirección proxy</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Pregunte al servidor por actualizaciones disponibles</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Previo</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Siguiente</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Comenzar actualización</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Iniciar degradación</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>No se encontraron actualizaciones</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>No se pudo actualizar</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>Actualizar terminará cualquier prueba de ejecución. Al final del proceso el instrumento de prueba se reiniciará a si mismo para completar la actualización.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Confirmación de actualización</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>Es posible degradar el conjunto de comprobaciones, aunque no se recomienda. Si tiene que hacerlo, recomendamos llamar a Viavi TAC.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>La degradación provocará la terminación de las comprobaciones en curso. Al finalizar el proceso, el conjunto de comprobaciones se reinicializará para completar la degradación.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>No se recomienda degradar la versión</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Se encontró un error desconocido. Por favor, inténtelo de nuevo.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Sistema</translation>
        </message>
    </context>
</TS>
