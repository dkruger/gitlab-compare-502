<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscope</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Tests</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>Compteur de puissance</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>Système</translation>
        </message>
    </context>
</TS>
