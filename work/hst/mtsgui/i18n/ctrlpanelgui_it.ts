<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Capacità applicazione mobile</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth e WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Solo WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Riavviare l'apparecchio di prova per applicare le modifiche.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Incolla</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copia</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Taglia</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Singolo</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Multiplo</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tutto</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aperta</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Rinomina</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Nuova cartella</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Disco</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>L'incollaggio causerà la sostituzione dei file esistenti.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Sovrascrivi file</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>Impossibile aprire il file a causa di un errore sconosciuto.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>Impossibile aprire il file.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>Il file selezionato sarà definitivamente eliminato.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>I file selezionati %1 saranno definitivamente eliminati.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Elimina file</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Immettere il nome della cartella:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Crea cartella</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>Creazione della cartella "%1" non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>La cartella "%1" è già presente.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Crea cartella</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Incollaggio...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Eliminazione...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Completato.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Incollaggio del file non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>Incollaggio di %1 file non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Spazio libero insufficiente.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Eliminazione del file non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>Cancellazione di %1 non riuscita</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Indietro</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opzione</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>URL di upgrade</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome file</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Azione</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>Modulo</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Altra rete wireless</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Inserire le informazioni di rete wireless sotto.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Nome (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Tipo di crittografia</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Aggiornamento disponibile</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync ha rilevato che è disponibile un aggiornamento.&#xA;Eseguire ora l'aggiornamento?&#xA;&#xA;L'aggiornamento interromperà gli eventuali test in esecuzione. Al termine del processo l'apparecchio di prova si riavvierà automaticamente per completare l'aggiornamento.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Si è verificato un errore sconosciuto. Riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Aggiornamento non riuscito</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Informazioni strumento:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Opzioni base:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Reimposta strumento&#xA;sui valori predefiniti</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Esportare registri&#xA;nella chiave USB</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Copia informazioni&#xA;di sistema su file</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>Le informazioni di sistema sono state copiate&#xA;su questo file:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Questo richiede il riavvio e comporta la reimpostazione del Sistema e dei Test ai valori predefiniti.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Riavvia e reimposta</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Esportazione registro effettuata correttamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Impossibile esportare i registri. Verificare che la chiave USB sia inserita correttamente e riprovare.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1Versione%2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Ricomincia</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Ripristinare su impostazioni predefinite</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Selezionare il metodo di aggiornamento:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Aggiorna da file memorizzati su unità flash USB.</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rete</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Scarica aggiornamento da un server web in rete.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Immettere l'indirizzo del server di aggiornamento:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Indirizzo server</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Inserisci l'indirizzo del server proxy, se necessario, per accedere al server di aggiornamento:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Indirizzo proxy</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connetti</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Richiedi gli aggiornamenti disponibili sul server.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Precedente</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Successivo</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Avvia aggiornamento</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Avviare downgrade</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Non ci sono aggiornamenti disponibili</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Aggiornamento non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>L'aggiornamento interromperà gli eventuali test in esecuzione. Al termine del processo l'apparecchio di prova si riavvierà automaticamente per completare l'aggiornamento.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Conferma aggiornamento</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>Il downgrade della serie di test è possibile ma sconsigliato. Qualora fosse necessario, si consiglia di rivolgersi al Viavi TAC.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>L'esecuzione del downgrade comporterà la chiusura di tutti i test in esecuzione. Al termine del processo, il test si riavvierà automaticamente per completare il downgrade.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>Downgrade sconsigliato</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Si è verificato un errore sconosciuto. Riprovare.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Sistema</translation>
        </message>
    </context>
</TS>
