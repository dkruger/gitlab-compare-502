<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CPowerOffProcess</name>
        <message utf8="true">
            <source>Power off</source>
            <translation>Выключение</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set shuts down.</source>
            <translation>Дождитесь отключения прибора.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CRebootProcess</name>
        <message utf8="true">
            <source>Reboot</source>
            <translation>Перезапуск</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set reboots.</source>
            <translation>Подождите до перезапуска испытательной установки .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CShutdownPrompt</name>
        <message utf8="true">
            <source>Power options</source>
            <translation>Параметры питания</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
</TS>
