<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Cet équipement de test a été verrouillé par un utilisateur pour interdire un accès non autorisé.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>Pas de mot de passe requis, pressez OK pour déverrouiller.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Déverrouillez l'équipement de test ?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Veuillez entrer le mot de passe pour déverrouiller :</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Entrez le mot de passe</translation>
        </message>
    </context>
</TS>
