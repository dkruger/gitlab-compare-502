<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>Trames de {1} octets :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>Trames de {1} octets</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>Paquets de {1} octets :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>Paquets de {1} octets</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Erreur: Un timeout est intervenu pendant la récupération de {2}, vérifiez votre connexion et réessayez</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} Burst de trame : echec</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} Burst de trame : passe</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} de {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} Burst de paquet : echec</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} Burst de paquet : passe</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} Réception de {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   Voulez-vous le remplacer ?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Tapez OK pour répondre</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} Test de VLAN ID {2} pour {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Attente...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       Vous devez modifier le nom pour créer une nouvelle configuration.</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 Top Talkers (d'un total de {1} conversations)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 Top Talkers de conversations retransmises (d'un total de {1} conversations)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Annuler le Test</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Activée</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Boucle active</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>La boucle active ne réussit pas. Le test est interrompu pour l'ID de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Ajouter un range</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Un taux de perte de trame qui dépasse le seuil de perte de trames configuré</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>Après avoir fait votre test manuel et à tout moment vous pouvez</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Une boucle matérielle a été trouvée</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>Une boucle matérielle n'a pas été trouvée</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Tous les tests</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Une application de rappel n'est pas une application compatible</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Une mesure de débit maximum n'est pas disponible</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>Une boucle active n'est pas trouvée</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>et</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>et le test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>Une boucle LBM/LBR a été trouvée.</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>Une boucle LBM/LBR a été trouvée.</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Une boucle permanente n'a pas été trouvée</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Ajouter le journal d'avancement à la fin du rapport</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Nom de l'application</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Temps total approx.:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Une gamme de valeurs de débit FTP théoriques vont être calculées fondé sur les valeurs mesurées actuellement sur la liaison. Entrer les mesures de largeur de bande, round trip délai, et l'Encapsulation.</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Une fin de temporisation de réponse est atteinte. &#xA; Il n'y a pas eu de réponse à la dernière commande&#xA;au bout de {1} secondes.</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> En supposant qu'une boucle physique est en place.    </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymétrique</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>Des transmissions asymétriques en mode montant du local au distant et en mode descendant du distant au local. Le mode combiné effectuera l'essai deux fois, transmettant séquentiellement en mode montant utilisant la configuration locale et en mode descendant utilisant la configuration distante. Utilisez le bouton pour remplacer la configuration distante par la configuration locale.</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Essais</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Essai de boucle active</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Essai de connexion au serveur ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>Les tentatives de boucle ont échoué. Le test s'arrête</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>La tentative de boucle active n'a pas réussi</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto Négociation</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Auto Négociation Effectuée</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Config. de l'Auto Négociation</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Etat de l'Auto Négociation</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Moyen</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Burst moyen</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Taux moyen de paquets</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Taille moyenne de paquet</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Moy</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Resultats Test Gigue de Paquets Moy et Moy Max</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Délais moy (RTD) :</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Délais moy (RTD) : N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Gigue moy de packet :</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Gigue de Paquets Moy : N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Gigue Pqt Moy (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>Débit Moy</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Gran. de Trame "Back to Back" :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Gran. de Trame "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test de Trames en "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Résultats du Test de Trames en "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Temps essais max "Back to Back" :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Temps essais max "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Résultats de Test "Back to Back" :</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>Retour au résumé</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularité de Bandwidth (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Granularité de Bandwidth (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Préc. de mesure bande passante :</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Préc. de mesure bande passante</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>Test de charge basique</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Beginning of range:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Les deux</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx et Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>Les deux adresses IP locale et distante sont Non-disponibles</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Du bas vers le haut</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Crédit de mémoire tampon (nécessite un débit)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Buffer Credits Durée de l'essai:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Buffer Credits Durée de l'essai</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Test de Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Buffer Credit Résultats de Test:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Buffer Credit Débit {1} Octets:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Débit du crédit de mémoire tampon&#xA;(nécessite un crédit de mémoire tampon)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Test de Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Buffer Credit Throughput Résultats de Test:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Taille Buffer</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Granularité du Burst (en Trames)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>En regardant les retransmissions de TCP par rapport à l'utilisation de réseau avec le temps, il est possible de corréler la faible performance du réseau avec des conditions de fiabilité de réseau telles que la congestion.</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>En regardant la table de conversations IP, « les top talkers » peuvent être identifiés par des octets ou des trames.  La nomenclature « S &lt; - D » et « S - > D » se rapportent à la source vers destination et à la destination vers source du trafic des octets et des trames.</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(octets)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation> Octets</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(Octets)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Octets&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Octets&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Longueur de&#xA;Trame Calculée</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Longueur de Paquet&#xA; Calculée</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Calcul en cours...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>Ne peut pas procéder !</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Résumé d'analyse de capture</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Capture de la durée</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturer&#xA;Ecran</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>ATTENTION!&#xA;&#xA;Etes-vous certain de vouloir supprimer&#xA;définitivement cette configuration?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Débit Cfg (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Débit Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Débit Cfg (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>ID du châssis</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>L'article(s) Rx vérifié sera utilisé pour paramétrer des configurations de filtre de source.</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>L'article(s) Tx vérifié sera utilisé pour paramétrer des configurations de filtre de destination.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Vérification de la boucle active</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Vérification une boucle de matériel</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Vérification d'une boucle active</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Vérification d'une boucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Vérification d'une boucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Vérification la détection des ports semi-duplex</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Vérification des trames ICMP</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Vérification les retransmissions possibles ou l'utilisation élevée de largeur de bande</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Vérification de la boucle dure</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Vérification de la boucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Vérification de la boucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Vérification des statistiques de hiérarchie de protocole</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Vérification de la disponibilité d'adresse de source ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>L'activation de cette case causera la restauration des installations d'essai à leurs réglages d'origine en quittant l'essai. Pour les essais asymétriques, ils seront restaurés du côté local et distant. La restauration des installations causera la remis à zéro du lien.</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Vérifier les réglages entre l'IP source et l'équipement auquel il est relié; vérifier que l'état half-duplex n'existe pas.  Davantage de localisation peut également être réalisée en déplaçant l'analyseur plus près de l'IP destination ; déterminer si on élimine des retransmissions pour isoler les liens défectueux.</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Choisir un dossier de capture pour analyse</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>Choisir&#xA;fichier PCAP</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Choisir la précision de bande passante désirée&#xA;( 1% est recommandé pour un temps de test limité)</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Choisir le type de login de Contrôle de Flux</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Choisir votre préférence de taille de trame ou de paquet</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Choisissez la granularité à laquelle vous souhaitez exécuter le test de dos à dos.</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Choisir la granularité à laquelle vous voulez faire le test Trame perdue.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Choisissez la Bande Passante Maximale pour laquelle le circuit est configuré. Le testeur utilisera cette valeur comme bande passante maximale à transmettre, réduisant la durée du test :</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Choisissez la période d'essai maximale pour le test de dos à dos.</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Choisissez les valeurs de charge maximale et minimale à utiliser avec les procédures de test 'du haut vers le bas' ou 'du bas vers le haut'</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Choisir le nombre d'essais que souhaitez faire pour chaque longueur de trame lors du test Back to Back.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Choisir le nombre d'essai que vous souhaitez lancer pour la mesure de délais (RTD) pour chaque taille de trame.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Choisir le nombre d'essais à effectuer pour le test de Gigue de Paquets pour chaque taille de trame.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Choisir le pourcentage de tolérance du test "Frame Lost".&#xA;NOTE : Une valeur supérieur à 0% ne correspond plus à la norme RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Choisir le temps de chaque essai de la mesure de délais (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Choisissez le temps que chaque gigue de paquet va durer.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Choisissez la durée pendant laquelle un taux doit être envoyé sans erreur afin de passer le test de débit.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Choisissez le temps que vous voulez faire durer chaque essai de perte de trame.</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Choisir le temps de l'essai pour le test du Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Choisissez quelle procédure utiliser dans le test de perte de trame.&#xA;NOTE : La procédure RFC-2544 commence à la bande passante maximale et diminue de la granularité de bande passante à chaque essai, et se termine après deux essais consécutifs sans perte de trame.</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Protocole de découverte Cisco</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>Des messages "Cisco Discovery Protocol" (CDP) ont été détectés sur ce réseau et le tableau liste les adresses MAC et les ports qui ont effectué le réglage half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Cliquer le bouton « résultats » pour basculer à l'interface utilisateurs standard.</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violation de Code</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Combiné</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation> Commentaires</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commentaires</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Communication correctement établie avec le distant</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>La communication avec le distant ne peut-être établie</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>La communication avec le distant est perdue</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>Complète</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>Terminé&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Configs</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation> Nom de la config.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Nom de la configuration :</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Nom de la config.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>Nom de configuration nécessaire</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Configuration en mode lecture seule</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Résumé de la Configuration</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Configurer article(s) vérifié(s)</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Configurez combien le {1} enverra du trafic.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Confirmer le remplacement de la configuration</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Confirmer la suppression</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTÉ</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connexion en cours</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Connecter à l'application de mesure de Test</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continuer en semi-duplex</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Continuant en mode semi-duplex</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Copier la configuration locale&#xA;à la configuration distante</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Copie&#xA;Choisie</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>N'ai pas pu activer la boucle du distant</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Créer Rapport</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>credits</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(Credits)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA;Script courant : {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Sélection actuelle</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation> Client</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Client</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Débit des données</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Taux d'octets des données</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Couche données arrêtée</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Mode Données</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Mode data réglé à PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Taille des données</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation> Date</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>Date &amp; Heure</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>jours</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Délais, Actuel (us) :</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Délais, Courant (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Effacer</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Adresse destination</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Configuration de la Destination</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID Destination</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP Destination</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>Adresse&#xA;IP Destination</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>Le MAC destination pour l'adresse IP {1} n'a pas été trouvé</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>Adresse MAC Destination trouvée</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>Adresse MAC dest</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>Étiquette de détail</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Détails</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>détecté</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>Détecté</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Largeur de bande du lien détecté </translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Détection de plus de trame que transmises pour {1} de bande passante - Test Invalide.</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Détermination de la sortie symétrique</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Détails de l'appareil</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ID de périphérique</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>Les paramètres DHCP sont indisponibles</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>Paramètres DHCP trouvés</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Appareils découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Découverte</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Découvrir type de boucle distante...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Découverte&#xA;non&#xA;disponible&#xA; actuellement</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Affiché par :</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Direction descendant</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation>Voulez-vous continuer quand même?</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durée</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Activé</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Activer le test de trafic étendu de couche 2</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Encapsulation :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulation</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>End of range:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Entrer l'adresse IP ou le nom du serveur avec lequel vous souhaitez effectuer le test FTP.</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Entrez le nom d'utilisateur pour le serveur auquel vous voulez vous connecter</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Entrer le mot de passe du compte que vous voulez utiliser</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Entrer le nom de votre nouvelle configuration&#xA;(Utiliser lettres, numéros, espaces, tirets et underscores uniquement)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Error: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ERREUR : Une fin de temporisation de réponse est atteinte. &#xA; Il n'y a pas eu de réponse  au bout de</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Erreur : Impossible d'établir une connexion</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Nombre d'erreurs</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Trames Erronées</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Erreur de chargement de fichier PCAP </translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Erreur: Le DNS Primaire a échoué à la résolution du nom.</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Erreur: impossible de localiser le site</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Estimation du temps restant</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Temps restant estimé</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     Rapport de Test Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>Le journal des événements est plein.</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Retransmissions excessives trouvées</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>Sortir de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Débit prévu</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>Débit prévu est</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>Débit prévu est indisponible</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>Bouton « Test Expert RFC 2544 »</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explicite (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Fabric/N-Port Explicite</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Explicit login was unable to complete. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ECHEC</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>Far-end est un ensemble de test ethernet de classe JDSU Smart</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>Le distant est un SmartClass Ethernet</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>Test FC executé avec le paquet de test Acterna</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>Rapport_du_Test_FC</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Supporte FDX</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Rapport du Test FC</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>Configuration de fichier</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nom de fichier</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Fichiers</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>Taille de fichier</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>Taille de fichier :</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>Taille de fichier</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>Taille de fichier : {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>Tailles des fichiers :</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>Tailles des fichiers</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>Fichier transféré trop rapidement. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Trouver le débit prévu</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Trouver les « premiers parleurs »</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>Les premiers 50 ports semi-duplex (d'un total de {1})</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>Les premiers 50 messages ICMP (d'un total de {1})</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Contrôle de Flux</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Type de Login de contrôle de flux</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Dossiers</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation>pour chaque trame est réduit de moitié pour compenser la double longueur de fibre.</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>trouvé</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Boucle active trouvée.</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Boucle matérielle trouvée.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Longueur&#xA;de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Longueur de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Longueur de Trame (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Longueurs de Trame :</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Longueurs de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Longueurs de trame à tester</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Trame perdue (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Trame perdue {1} octets :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Trame perdue {1} octets</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Gran. Bande passante "Frame Loss" :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Gran. Bande passante "Frame Loss"</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Bande passante Maximale pour Perte de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Bande passante Minimale pour Perte de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Taux de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Taux de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Test "Trame perdue"</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Procédure de Test de Perte de Trame :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procédure de Test de Perte de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Résultats du Test "Trame perdue" :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolérance du "Frame Lost" (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Durée d'un essais "Frame Loss" :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Durée d'un essais "Frame Loss"</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Trame ou Paquet</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>trames</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Trames</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>Taille de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Taille de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Taille de trame:  {1} Octets</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Trames&#xA;S&lt;-D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Trames&#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(trms)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(trms/sec)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>FTP Largeur de Bande</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>Test de Débit FTP&#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>Test de Débit FTP Executé !</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>Rapport de Test de Débit FTP</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>Obtenir l'information PCAP</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Ports half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Matériel</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Boucle matérielle</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>(Matériel &#xA; ou Actif)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>(Matériel,&#xA;Permanent&#xA;ou Actif)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Supporte HDX</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Utilisation élevée</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>heures</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>Test de Débit HTTP</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>Rapport de Test de Débit HTTP</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>ICMP&#xA;Code</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>Messages ICMP</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Si les compteurs d'erreur s'incrémentent sporadiquement exécuter le mode manuel</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Si le problème persiste, merci de remettre les paramètres par défaut depuis le menu "Outils"</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Si vous ne pouvez pas résoudre le problème avec les erreurs sporadiques vous pouvez mettre</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implicite (Lien transparent)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>Information</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Communication en cours d'initialisation avec</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Afin de déterminer la largeur de bande à laquelle le</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>Débit d'entrée pour le local et le distant ne correspondent pas</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Des problèmes intermittents ont été observés sur la ligne.</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Erreur Interne</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Erreur interne : Relancer PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config Non Valide</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Config d'adresse IP Invalide</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>Adresses IP</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>Conversations IP</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>est en train de quitter</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>Commence</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Test de gigue</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck est complet</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck a perdu le lien ou ne pouvait pas établir le lien</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>koctets</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Terminer</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>C1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>C2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>Le test de trafic L2 peut être relancé en exécutant J-QuickCheck à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latence</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Délais (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>Tests de latence (RTD) et de gigue de paquet</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Délais (RTD) Moy : N/A</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Valeur du seuil du délais (RTD) :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Valeur du seuil du délais (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Latence (RTD)&#xA;(necessite le&#xA;Test de Débit)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Délais (RTD) résultats</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Test du "Round Trip Time" (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Résultats du test du "Round Trip Time" :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Résultat du Test de Latence (RTD) : INTERROMPU</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Résultat du Test de Latence (RTD) : INTERROMPU</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Résultat du Test de Latence (RTD) : ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Délais (RTD) résultats des essais sautés</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Délais (RTD) résultats sautés</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> Seuil de Délais (RTD) : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Seuil de Délais (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Durée de l'essai du test de délais (RTD) :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Durée de l'essai du test de délais (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Délais (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Couche 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Couche 1 / 2&#xA;Ethernet Health</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Couche 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Le lien de couche 2 a été trouvé</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Test rapide couche 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Couche 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>IP Health&#xA; couche 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Couche 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>TCP Health&#xA; couche 4</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Boucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Longueur</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Lien trouvé</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Protocole de détection automatique des périphériques</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Liaison perdue</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Vitesse de lien détectée dans fichier de capture</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Port d'écoute</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Load Format</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>Le CHARGEMENT... veuillez attendre</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>L'adresse IP destination locale est configuré à </translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>L'adresse MAC destination locale est configuré à </translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>Le port destination local est configuré à </translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>Le type de boucle locale est configuré à Unicast</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Port local</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>L'adresse IP destination distante est configuré à</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation>Numéro de Série Local</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Installation locale</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation>Révision de Logiciel Locale</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>Le filtre local d'IP source est configuré à</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>Le filtre local MAC source est configuré à</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>Le filtre local de port de source est configuré à</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Résumé local</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation>Nom de l'instrument de test local</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Localiser l'équipement avec les adresses MAC et les ports source énumérés dans le tableau et s'assurer que les réglages duplex sont placés à « full » et pas à « auto ».  Il n'est pas rare que l'hôte soit placé en « auto » et le lien négocié en half-duplex par erreur.</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation> Localisation</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Localisation</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Login:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Login</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Nom d'utilisateur:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Nom d'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>La boucle à échouée</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Désactive la boucle de l'équipement distant...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Active la boucle de l'équipement distant...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Etat de boucle Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Activation de boucle à échoué</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Activation de boucle réussie</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Boucle activé avec succès</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>La perte du lien de la couche 2 a été détectée !</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Perdu</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Perte de Trames</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Adresse de direction</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>Type de MAU</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( max {1} caractères )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Moy, Max</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Moy. max de gigue de paquets :</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Gigue de Paquets Moy Max : N/A</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Gigue Pqt Moy Max (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Bande Passante Max&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Bande Passante Max&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Taille Max de Buffer</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>Délais maximum, valeur moyenne pour "passer" le test de délais</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Gigue de Paquets Maximale, Moy limite pour "Réussir" le Test de Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Buffer Credit Rx Maximum</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Bande Passante maximum de Test :</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Bande Passante maximum de Test</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Débit maximum mesuré :</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Limite de temps maximum de {1} par ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Limite de temps maximum de 7 jours par ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Temps d'essais max (secondes)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Buffer Credit Tx Maximum</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>Débit Max</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Les tentatives max de retransmission sont atteintes. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Mesurée</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Débit mesurée (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Débit mesurée</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Débit mesurée (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Précision de mesure</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Mesure du débit au Buffer credit {1}</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Message</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimum</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Pourcentage minimum de Bande Passante</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>Pourcentage minimum de bande passante nécessaire pour "passer" le test de "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Limite de temps minimum de 5 secondes par ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Débit Min</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>mins</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>minute(s)</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>Minutes</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modèle</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Modifier</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>L'encapsulation MPLS/VPLS n'est pas supportée...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/A (boucle physique)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>RESEAU Actif</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Utilisation de réseau</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>L'utilisation de réseau n'a pas été détectée comme excessive, mais ce diagramme fournit un graphique d'utilisation de réseau</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>L'utilisation de réseau n'a pas été détectée comme excessive, mais ce diagramme fournit une liste des "top talkers" IP</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Nouveau</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Nom de la nouvelle configuration</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Nouvelle URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Suivant</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Non.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>Pas d'application compatible trouvée</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt;PAS DE CONFIGURATION DISPONIBLE></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>Aucun fichier selectionné pour tester</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>Une boucle matérielle n'a pas été trouvée</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>No&#xA;JDSU&#xA;Périphériques&#xA;Découvert</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>Aucun lien couche 2 détecté !</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>Aucune boucle n'a pu être établie</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>Aucune boucle n'a pu être établie ou trouvée</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>Une boucle permanente n'a pas été trouvée</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>Pas d'application en fonctionnement détectée</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>correspond plus à RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Non connecté</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Non déterminé</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>NOTE : Une valeur > 0.00 ne</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Note : Suppose qu'une boucle physique avec Buffer ajoute moins de 2.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;Remarque : Suppose une boucle physique avec un crédit de mémoire tampon inférieur à 2.&#xA;Ce test n'est pas valide.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Note: Fondé sur l'hypothèse d'une boucle physique, le buffer credit minimal calculé</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Note: Fondé sur l'hypothèse d'une boucle physique, les mesures de débit sont effectuées à deux fois</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Note : Une fois que vous employez une tolérance de perte de trame l'essai n'est pas conforme</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Remarques</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Non sélectionné</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Aucuns&#xA; équipements&#xA;Viavi&#xA;découverts</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>En cours d'arrêt...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>En cours de vérification</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Vérification avec un credit de {1}. Cela prendra {2} secondes.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Nombre d'essais "Back to Back" :</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Nombre d'essais "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Number of Failures</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Number of IDs tested</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Nombre d'essais du test de délais (RTD) :</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Nombre d'essais du test de délais (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Nombre d'essais de Gigue de Paquets:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Nombre d'essais de Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Nombre de paquets</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Number of Successes</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Nombre d'essais</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Nombre d'essais</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>de</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Inactif</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>des trames ont été perdues dans un délai d'une seconde</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>de débit J-QuickCheck prévu</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(% du Débit de Ligne)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% du Débit de Ligne</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation> du Débit de Ligne</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Actif</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation>* Données Utilisables de seulement {1} Essai(s) *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(MARCHE ou ARRET)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Trames OoS</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID d'Origine</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Hors limite</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        Résultats de Test général : {1}  </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    Résultat General du Test : INTERROMPU</translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Au dessus de la limite</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>au cours des 10 dernières secondes malgré que le trafic devrait être arrêté</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Gigue Paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Gigue Paquet, Moy</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Seuil de réussite Gigue de Paquets:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Seuil de réussite Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Gigue Paquet&#xA;(necessite le&#xA;Test de Débit)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Résultats de gigue de paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Gigue Paquet à tester</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Résultats du Test de Gigue de Paquets : INTERROMPU</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Résultats du Test de Gigue de Paquets : ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Résultats du Test de Gigue de Paquets : PASSE</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> Seuil de Gigue Paquet : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Seuil Gigue de Paquets (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Durée du test Gigue de Paquets:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Durée du test Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Longueur&#xA;de paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Longueur de Paquet (octets)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Longueurs de Paquet :</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Longueurs de Paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Longueurs de Paquet à tester</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>Taille de packet</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Taille de paquet</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>OK / Echec</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>OK/ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Taux de passe (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Taux des passages (trm/sec)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Taux des passages (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Débit (Pqts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Mot de Passe:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Mot de passe</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Pause Advrt</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Supporte les Trames Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Pause Det</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Trame Pause détectées</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Trame Pause détectées - Test Invalide</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Erreur d'analyse de fichier PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>Fichiers PCAP</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>En instance</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Nettoyage en Cours</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanent</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Boucle permanente</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>(Permanent&#xA;ou Actif)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pqt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Gigue paquet (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Longueur de Pqt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perte de paq.</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(pqts)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(pqts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Plateforme</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Veuillez vérifier que vous avez la synchro et le lien</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Veuillez vérifier que vous êtes correctement connectés</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Veuillez vérifier que vous êtes correctement connectés</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Choisissez un autre nom de configuration, SVP.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Veuillez saisir un nom de fichier pour sauvegarder le rapport ( max %{1} caractères) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Entrez des commentaires éventuels ({1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Entrez des commentaires éventuels (%{1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir vos commentaires ({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Entrez le nom de votre client ({1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Entrez le nom de votre client (%{1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir le nom du client&#xA;({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir le nom du client ({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir le nom du client ({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Entrez le nom de votre technicien ({1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Entrez le nom de votre technicien (%{1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir le nom du technicien ({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Entrez l'emplacement de votre test ({1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Entrez l'emplacement de votre test (%{1} caractères max)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Veuillez saisir l'emplacement de votre test ({1} caractères max)&#xA;(utiliser lettres, chiffres, espaces, tirets et caractères de soulignement uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Veuillez presser le bouton "connexion au distant"</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Veuillez vérifier la performance du lien avec un essai manuel du trafic</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Veuillez vérifier les sources locale et distante d'adresse IP et réessayer</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Veuillez vérifier la source locale d'adresse IP et réessayer</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>Merci de vérifier l'adresse IP distante et recommencer</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Veuillez vérifier la source distante d'adresse IP et réessayer</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Attendre SVP...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Attendre SVP...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Merci de patienter pendant la création du fichier PDF.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Attendez svp tandis que le fichier de PDF est écrit.&#xA;Ceci peut avoir besoin de jusqu'à 90 secondes ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Port :</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Port {1}: Voulez-vous sauvegarder un rapport de test ?&#xA;&#xA;Appuyer sur "Oui" ou "Non".</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>Port ID</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Port :				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Port :			{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Actif</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Defaut Authentification PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Defaut PPP Inconnu</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>Defaut PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Defaut LCP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Actif</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Defaut PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE Inactif</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE Démarré</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>Etat PPPoE: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Time Out PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE Up</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPPoE a échoué</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Time Out PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP Echec inconnu</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>Échec PPP up</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Defaut Activite PPP</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>Appuyer sur "Fermer" pour retourner à l'écran principal</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>Appuyer sur "Sortir" pour revenir à l'écran principal ou "Lancer le Test" pour le relancer.</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Pressez&#xA;le bouton&#xA; Rafraîchir&#xA;pour&#xA;découvrir</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Appuyer sur le bouton « quitter J-QuickCheck» pour sortir de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Appuyer sur le bouton Rafraîchir ci-dessous pour découvrir des équipements de Viavi actuellement sur le sous-réseau. Choisir un équipement pour voir des détails dans la table vers la droite. Si Rafraîchir n'est pas disponible assurez-vous que la Découverte est activée et que vous avez la synchro et le lien</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Appuyer sur le bouton « démarrer J-QuickCheck»&#xA;pour les configurations d'essai locales et distantes et la bande passante disponible</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Préc</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>En cours</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Propriété</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Prochaines Etapes Proposées</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Fournisseur</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aléatoire</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> inférieure</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Gamme</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Débit (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>évaluer le lien peut avoir des problèmes généraux non liés à la charge maximum</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>Reçu {1} octets de {2}</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Trames Reçus</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Recommandation</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Essai manuel de configuration recommandé :</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Régénérez</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Distant</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Adresse IP distante</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Boucle distante</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation>Numéro de série distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>Numéro de série distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Essai du distant</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Essai du distant n'a pas pu être reconstitué</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation>Révision de logiciel distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Version du logiciel distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Résumé du distant</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>Nom de l'instrument de test distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Nom de l'instrument de test distant</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Enlever le range</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID du répondeur</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>Réponse&#xA;routeur IP</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>Recommence J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Restaurer les configurations de pré-test avent de quitter</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Restauration des réglages de test distants ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>RFC restreint à</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>Le Résultat du Test Basique de Charge est Indisponible, appuyez sur "Prochaines Etapes Proposées" pour des solutions possibles</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Résultats à analyser :</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Retransmissions</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Des retransmissions ont été trouvées&#xA;Analysant les occurrences de retransmission au fil du temps</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>Des retransmissions ont été trouvées. Veuillez exporter le fichier vers l'USB pour l'analyse approfondie</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>La récupération de {1} a été abandonnée par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>revenir à l'interface utilisateurs de RFC 2544 en cliquant sur</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>Rapport de test Ethernet RFC 2544</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>Mode RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>Mode RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>RFC 2544 Test</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>RFC2544 en cours avec la payload de test Acterna</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>Rapport_de_Test_RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>Le test RFC/FC ne peut pas démarrer tant que Résultats Graphique en flux multiple est en fonctionnement</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Mode RFC2544</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY Det</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Exécuter</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>Lancer test FC</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Lancer J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>Exécutez $l2quick::testLongName</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Test en cours chargé à {1}{2}</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Lancement du test à {1}{2} de charge. Cela va prendre {3} secondes.</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>Lancer essai RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Lancer le script</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Rx Mbps, Act C1</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx Seulement</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>Sauvegarde du rapport de test de débit FTP</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>Sauvegarde du rapport de test de débit HTTP</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>Sauvegarder le rapport de Scan des VLAN</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> bande passante mise à l'échelle</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Capture de screen shot</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Script arrêté</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>sec.</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(sec.)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>secs</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Choisissez un nom pour la configuration copiée</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Choisissez un nom pour la nouvelle configuration</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Select a range of VLAN IDs</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Sélectionner direction Tx :</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Sélectionner direction Tx&#xA;descendant</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Sélectionner direction Tx&#xA;montant</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Selection Warning</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Sélectionner "OK" pour modifier la configuration&#xA;Editer le nom pour créer une nouvelle configuration&#xA;(Utiliser lettres, numéros, espaces, tirets et underscores uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Sélectionner la configuration de test</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Choisir la propriété par laquelle vous souhaitez voir les équipements découverts listés</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Sélectionner les tests que vous souhaitez lancer :</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>Selectionner une URL</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Sélectionnez quel format utiliser pour les configurations liées à la charge.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Envoi d'un ARP Request pour l'adresse MAC Destination.</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Envoi de trafic pour</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>envoie le trafic, le débit attendu découvert par J-QuickCheck sera mis à l'échelle par cette valeur.</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID de Séquence</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID du Serveur:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>ID du Serveur</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nom du service</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Afficher l'état Réussite/Echec</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;Présente l'état passe/echec pour :</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Taille</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>sauter</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Sautant l'essai de latence (RTD) et continuant</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Rév. du logiciel</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> Révision Logicielle</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Adresse Source</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>L'adresse source n'est pas disponible</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Disponibilité de source établie ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID Source</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Source</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>Adresse&#xA;IP Source</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>L'adresse IP Source est identique à l'adresse IP Destination</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Source</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Source MAC&#xA;adresse</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Spécifier la largeur de bande de lien</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Vitesse</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Vitesse (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>DÉBUT</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Démarrage de l'essai de charge basique</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Début du Test</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Heure de début</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Statut inconnu</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>ARRÊTER</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Étudier le graphique pour déterminer si les retransmissions TCP correspondent à  l'utilisation dégradée du réseau.  Regarder l'étiquette de retransmissions TCP pour déterminer l'IP source qui cause les retransmissions TCP significatives. Vérifier les réglages de ports entre l'IP source et le dispositif auquel il est relié; vérifier que l'état half-duplex n'existe pas.  Davantage de localisation peut également être réalisée en déplaçant l'analyseur plus près de l'IP destination ; déterminer si les retransmissions sont éliminées pour isoler les liens défectueux.</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>Réussit!</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Réussit</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Résumé</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Résumé des valeurs mesurées.</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Résumé de page {1} :</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Bits de priorité SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symétrique</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>Le mode symétrique transmet et reçoit vers et depuis un boucleur distant. Le mode asymétrique transmet vers le distant en mode "Montant" et reçoit depuis le distant en mode "Descendant"</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Symétrie</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Cliquez sur le nom de la configuration pour la sélectionner </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	Récupérer {1} MB fichier....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	Put {1} Fichier MB ...</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Débit: {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Débit: {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Trames Rx {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              PRUDENCE !&#xA;&#xA;	    Êtes vous sur de vouloir effacer&#xA;	           cette configuration définitivement ?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Veuillez utiliser des lettres, des chiffres, des espaces,&#xA;		   des tirets et des soulignements seulement !,</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Cette configuration est seulement en lecture&#xA;		   et ne peut pas être effacée.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Vous devez saisir un nom pour la nouvelle&#xA;		           configuration en utilisant le clavier.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   La configuration spécifiée existe déjà.</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Durée: {1} secondes&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tx Trames {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>Hôte TCP n'a pas réussi à établir une connexion. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>Hôte TCP a rencontré une erreur. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>Retransmissions TCP</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation> Technicien</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Technicien</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nom du technicien</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Terminaison</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>                              Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Testez interrompu par l'utilisateur.</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Test à une bande passante max configurée à partir du réglage - Onglet Tous les tests</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>examiner à différents taux inférieurs du trafic. Si vous obtenez toujours des erreurs même sur inférieur</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Configuration du test:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuration du test</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Durée du test </translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Durée d'essai : Au moins 3 fois la durée d'essai configurée </translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Largeur de bande testée</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Test Terminé ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Test de {1} de credit</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Test de {1} de credit</translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Test à </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Test de la connexion...</translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation>Nom de l'Instrument de test</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Le test démarre</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Journal du Test</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nom du test :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Nom de test :			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Nom de test :			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procédure de Test</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Journal du progrès de Test</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Gamme de Test (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Gamme de Test (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Résultats de test</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Config de Test Set</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Tests à lancer :</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Tests à lancer</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Test was aborted</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>La boucle active a échoué et aucune boucle dure n'a été trouvée</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>La boucle active a échoué et aucune boucle dure ou permanente n'a été trouvée</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>Le taux moyen de {1} a {2} kbps</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>Le taux moyen de {1} a {2} Mbps</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>Le fichier dépasse la limite de 50000 paquets pour J-Mentor</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>le seuil de tolérance de perte de trames pour tolérer petits taux de perte de trames</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Le protocole de message de contrôle de l'internet (ICMP) est le plus largement connu dans le cadre de l'ICMP « ping ». Le message «destination ICMP inaccessible» indique qu'une destination ne peut pas être atteinte par le routeur ou l'équipement réseau.</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>Le compteur du filtre C2 Rx Acterna a continué à incrémenter</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>La boucle LBM/LBR a échoué.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>Les adresses IP source locale et distante sont identiques</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation>Les réglages de l'installation locale ont été copiés avec succès à l'installation distante</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>La source locale d'adresse IP est Non-disponible</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Le MTU mesurée est trop faible pour continuer. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>Le nom choisi est déjà utilisé.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>Le port de l'élément de réseau auquel nous sommes reliés est provisionné en semi-duplex. Si ceci est correct, pressez le bouton «continuez en half-duplex». Autrement, presser la «quitter J-QuickCheck» et reprovisionnez le port.</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>Le diagramme d'utilisation de réseau montre la largeur de bande consommée par tous les paquets dans le fichier de capture pendant de la durée de temps de la capture. Si des retransmissions TCP étaient également détectées, il est recommandé d'étudier les résultats de la couche TCP en revenant à l'écran principal d'analyse.</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>la valeur du buffer credit à chaque étape pour compenser la double longueur de fibre.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Calcul Théorique</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Theoretical &amp; Measured Values:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>Le port distant (de l'équipement de réseau) est en "AutoNeg OFF" et le débit attendu n'est pas disponible, donc le port distant en mode "half duplex". Si ce mode n'est pas correct, merci de le changer à "Full duplex" et de relancer J-QuickCheck. Après cela, si le débit attendu est plus raisonable, vous pouvez lancer la RFC-2544. Si le débit attendu n'est toujours pas disponible, vérifier la configuration du port distant.&#xA;&#xA; Si le mode Half-duplex est correct, changer la configuration du port local à half-duplex. Ensuite aller à "Expert RFC2544 Test", sortir de J-QuickCheck et retourner à "RFC2544 Standard" (Half Duplex) et relancer le test RFC2544.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>Il y a un problème de transmission avec l'extrémité distante</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>l'équipement de boucle distant ne répond pas à la commande de boucle Viavi et renvoie les trames transmises à l'équipement local avec les champs source et destination permutés</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>l'équipement de boucle distant répond à la commande de boucle Viavi et renvoie les trames transmises à l'équipement local avec les champs source et destination permutés</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>l'équipement de boucle distant renvoie les trames transmises inchangées à l'équipement local</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>l'appareil distant de boucle prend en charge OAM LBM et répond à une trame LBM reçue par la transmission d'une trame LBR correspondant au dispositif local.</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>Le côté distant est placé pour l'encapsulation MPLS</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>Le côté distant semble être une application de demande de boucle</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>La source distante d'adresse IP est Non-disponible</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA;Le rapport a été sauvegardé sous ""{1}{2}"" au format PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>Le rapport a été sauvegardé sous "{1}{2}" au format PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>Le rapport a été sauvegardé sous "{1}{2}" au formats PDF, TXT et LOG</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>Le rapport a été sauvegardé sous {1}{2} formats txt et log </translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>Le routeur IP répondant ne peut pas expédier le paquet à l'adresse IP destination, ainsi le dépannage devrait être conduit entre le routeur IP répondant et la destination.</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>L'essai du RFC 2544 ne supporte pas l'encapsulation MPLS</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>Le laser de transmission a été allumé&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Le laser de transmission est éteint !&#xA;Voulez-vous l'allumer?&#xA;Appuyer sur "Oui" pour l'allumer.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Le laser de transmission est éteint ! Voulez-vous l'allumer?&#xA;&#xA;Appuyer sur "Oui" pour l'allumer.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>Le scan des VLAN ne peut être effectué avec OAM CCM activé</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>Le scan des VLAN ne peut être configuré correctement</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Cette configuration est en lecteur seulement et ne peut pas être modifiée.</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Cela doit être l'adresse IP du distant en mode Asymétrique</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>Cette table identifie les adresses IP source qui génèrent des retransmissions TCP. Quand des retransmissions TCP sont détectées, ceci pourrait être dû à la perte de paquet descendant (vers la destination).  Elle pourrait également indiquer qu'il y a un problème de port half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>Ce test fonctionne avec la payload de test Acterna</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Ce test est invalide</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Ce test nécessite que le trafic ait une encapsulation VLAN. S'assurer que le réseau connecté fourniré une adresse IP pour cette configuration.</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Cela va prendre {1} secondes.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Débit (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Débit ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Tests de débit et de délais (RDT)</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Tests de débit et de gigue de paquets</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Tol. du trame perdues "Throughput" :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Tol. du trame perdues "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Tests de débit, délais et gigue de paquet</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Seuil du test "Throughput" :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Seuil du test "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Mise à l'échelle du débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Facteur de mise à l'échelle du débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test du "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>La durée du test de débit était {1} secondes</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Résultats du test de "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Résultats du test de "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Résultat du Test de "Throughput" : INTERROMPU</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Résultat du Test de "Throughput" : ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Résultat du Test de "Throughput" : PASSE</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Seuil de débit (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> Seuil de débit : {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Seuil de débit (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Durée d'un essais "Throughput" :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Durée d'un essais "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Processus de mise à zéro du débit :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processus de mise à zéro du débit</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation> Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Temps par ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Durée (secondes)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>Durée&#xA;(secs)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation> Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation># fois visité</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Pour continuer, vérifiez la connexion du câble puis relancez J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Pour déterminer le débit maximum, choisir la méthode standard RFC 2544 qui fait correspondre les compteurs des trames Tx et Rx ou la méthode Viavi qui utilise L2 % Avg. util. Mesurés</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Du haut vers le bas</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Pour économiser du temps la latence (RTD) en mode asymétrique doit être exécutée dans une direction seulement</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>pour voir s'il y a des événements sporadiques ou des pertes de trames constantes </translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Nombre total d'octets</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Trames&#xA;Total</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Nombre Total</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>d'util total {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>d'util total (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>d'util total (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>Pour afficher le rapport, sélectionnez "Afficher le Rapport" dans le menu Rapporta près être sorti {1}.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>dans</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Trafic : constant avec {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Résultats du trafic</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>Le trafic était toujours produit de l'extrémité distante</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>Le laser de transmission est éteint !</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Trames Transmises</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Transmission dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Transmission dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Essais</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Essais {1} :</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Essais {1} terminé&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Test {1} de {2}</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Durée d'un essais (secondes)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>essais</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Essai une deuxième fois</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>Erreur TShark</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>TX Buffer to Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Direction Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Laser de Tx éteint</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Tx Mbps, Act C1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx Seulement</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> Impossible de boucler automatiquement l'équipement&#xA; distant. </translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>Impossible de se connecter avec l'application de test de mesure !</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>Impossible de se connecter avec l'application de test de mesure !&#xA;Appuyer sur "Oui" pour recommencer."Non" pour annuler</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>Impossible d'obtenir une adresse DHCP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>Impossible de lancer la RFC-2544 avec la boucle en local validé</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>Incapable d'exécuter le test</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Unable to run VLAN Scan test with Local Loopback enabled.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>INDISP.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Identifiant d'Unité</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>MON</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>(Haut ou Bas)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Direction montante</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>L'utilisateur a interrompu le test</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>L'utilisateur a annulé le test</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nom d'Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Bits de priorité Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Utilisateur Inactif</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Choisie par l'Utilisateur&#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Choisie par l'Utilisateur ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Utiliser l'écran de statut récapitulatif pour rechercher des événements d'erreur</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Utilisant</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Utilisant une taille de trame de</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Utilisation et retransmissions TCP</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valeur</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>Les Valeurs Surlignées en bleu proviennent du test actuel.</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Vérifier que le lien est actif...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>Vérifiez votre adresse IP distante et réessayez</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>Vérifiez votre adresse IP distante et réessayez</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Enhanced</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Range de VLAN ID à tester</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN Scan Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAn Scan Test Results</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>Trame VTP/PAO/PAgP/UDLD détectée !</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>En attente de la fin de l'Auto-Négociation...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>En attente de l'adresse MAC destination pour l'adresse IP &#xA; </translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>En attente des paramètres DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>En attente de l'activation du couche 2...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>Attente de lien</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>Attend l'activation de l'OWD, Sync ToD et Sync 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Waiting for successful ARP ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>a été détecté à la dernière seconde</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Taille de site web</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Nous avons une boucle active</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>Nous avons une erreur!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>Quand vous testez des liens semi-duplex, sélectionnez le standard RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Taille de fenetre/capacité</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>Avec la recommandation RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Voulez-vous sauvegarder un rapport de test?&#xA;&#xA;Appuyer sur "Oui" ou "Non"</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>Vous pouvez également utiliser les résultats graphiques de taux de perte de trame </translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>Vous ne pouvez pas lancer ce script depuis {1}.</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Vous pourriez devoir attendre jusqu'à ce qu'il cesse de se reconnecter</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Détermination du débit max par répétition</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Détermination de l'optimale de credit buffer</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processus de remise à zéro</translation>
        </message>
    </context>
</TS>
