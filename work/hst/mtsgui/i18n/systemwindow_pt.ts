<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Arquivos</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Armazenamento removível</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Nenhum dispositivo detectado</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>Ao formatar o dispositivo USB, todos os dados serão apagados. Isso inclui todos os arquivos e partições</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Ejetar</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Buscar...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NENHUMA</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NO</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Par Bluetooth solicitado</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>Inserir PIN para emparelhamento</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Par</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Solicitação de emparelhamento</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Solicitação de emparelhamento de:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Para emparelhar com o dispositivo, verifique se o código mostrado a seguir corresponde o código nesse dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Emparelhamento iniciado</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Solicitação de emparelhamento enviada para:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Iniciar varredura</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Parar varredura</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Ajustes</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Bluetooth ativado</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Permitir que outros dispositivos se emparelhem com este dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Nome do dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Aplicativo móvel habilitado</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rede</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>Modo IP</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Estática</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>Endereço MAC</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>Endereço IP</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Máscara de subrede</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Gateway atual</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>Servidor DNS</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>Modo IPv6</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Desabilitado</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manual</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>Endereço IPv6   </translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Comprimento do prefixo da subrede</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>Servidor DNS</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Link - Endereço local</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Endereço sem estado</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Endereço permanente</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Módulos não carregados</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Presente</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Não Presente</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Iniciando</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Ativando</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Inicializando</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Pronto</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Escaneando</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Desabilitando</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Parando</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Associando</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Associado</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Adaptador wireless habilitado</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONECTADOS</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Desconectado</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Conecte à Rede WPA da empresa</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Nome da rede</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>Método de autenticação externa</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Autenticação método interno</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Nome de usuário</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>Identidade Anônima </translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Senha</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>Certificados</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Chave de senha privada</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Conectar à rede WPA Pessoal</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Senha</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>Nenhum dispositivo wireless USB encontrado</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Parar conexão</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Esquecer Rede</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>Não foi possível conectar à rede.</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>3G Serviço</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>Habilitando modem</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>Modem habilitado</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>Modem desabilitado</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>Nenhum modem detectado</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Erro - Modem não está respondendo.</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Erro - controlador do dispositivo não iniciado.</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Erro - Não pôde configurar modem.</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Alerta - Modem não está respondendo. Modem está sendo reiniciado...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Erro - SIM não insertado.</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Erro - Rede registro negado.</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectado</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Desconectando</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Não está conectado</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Conexão falida</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Configuração de modem</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Habilita modem</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>Nenhum modem detectado ou habilitado</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>Rede APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>Estado Conexão</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>Verifique APN e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS Servidor</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec DNS Servidor</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Procuração e Segurança</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Tipo de proxy</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Servidor de proxy</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Segurança de rede</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>Desativar FTP, telnet, e auto StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Gerenciamento de Energia</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>A alimentação CA está conectada</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Funcionando com a bateria</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Nenhuma bateria encontrada</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Carregamento foi interrompido devido ao consumo de energia</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Carregando bateria</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>A bateria está totalmente carregada</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>Bateria não carregando</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Impossibilitado de carregar a bateria devido a falta de energia</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Estado desconhecido da bateria</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>A bateria está muito quente e não pode cobrar</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>A bateria está muito quente&#xA;Conecte a alimentação de CA</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>A bateria está muito quente&#xA;Não desligue a alimentação CA</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>A bateria está muito quente&#xA;Desconectar a alimentação CA forçará o desligamento</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>A bateria está muito fria e não pode ser carregada</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>A bateria está com perigo de sofrer um superaquecimento </translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>A temperatura da bateria está normal</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Carregar</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Habilitar desligamento automático enquanto a bateria estiver ligada</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Tempo inativo (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Dia e Hora</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Zona horária</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Região</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>África</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>América</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antártica</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Ásia</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Oceano Atlântico</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Austrália</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Europa</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Oceano índico</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Oceano Pacífico</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>País</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afeganistão</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Ilhas Åland</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Albânia</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Argélia</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Samoa Americana</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorra</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angola</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguila</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua e Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Argentina</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Armênia</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Áustria</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Azerbaijão</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamas</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahrein</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladesh</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbados</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Bielorrússia</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Bélgica</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belize</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Benim</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermuda</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Butão</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolívia</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bósnia-Herzegovina</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botswana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Ilha Bouvet</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brasil</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Território britânico do oceano índico</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Darussalam</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgária</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Camboja</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>República dos Camarões</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Canadá</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Cabo Verde</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Ilhas Cayman</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>República Centroafricana</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Chade</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Chile</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>China</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Ilha do Natal</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Ilhas Cocos</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Colômbia</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Comoras</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Congo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>República Democrática do Congo</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Ilhas Cook</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Costa Rica</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Costa do Marfim</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Croácia</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Cuba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Chipre</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>República Tcheca</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Dinamarca</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Djibouti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominica</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>República Dominicana</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Equador</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Egito</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>El Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Guiné Equatorial</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Eritrea</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estônia</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Etiópia</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Ilhas Malvinas (Falkland)</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Ilhas Feroe</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fiji</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finlândia</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>França</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Guiana Francesa</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Polinésia Francesa</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Terras Austrais e Antárticas Francesas</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabão</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambia</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Geórgia</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Alemanha</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Gana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Gibraltar</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Grécia</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Groelândia</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Granada</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Guadalupe</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Guernsey</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Guiné</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Guiné-Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guiana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haiti</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Ilha Heard e Ilhas McDonald</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hong Kong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Hungria</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Islândia</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Índia</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Indonésia</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Irã</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Iraque</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Irlanda</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Ilha de Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Israel</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Itália</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Jamaica</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Japão</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Jordânia</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Cazaquistão</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Quênia</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Coreia do Norte</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Coreia do Sul</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Kuwait</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Quirguistão</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Laos</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Letônia</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Líbano</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesoto</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Libéria</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Líbia</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Liechtenstein</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Lituânia</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Luxemburgo</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>República da Macedônia</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagascar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malásia</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>República das Maldivas</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Mali</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Malta</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Ilhas Marshall</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinica</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Mauritânia</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>República de Maurício</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>México</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Estados Federados da Micronésia</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>República da Moldávia</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Mônaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Mongólia</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Montenegro</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Montserrat</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Marrocos</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Moçambique</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Myanmar</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namíbia</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Nepal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Holanda</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Antilhas Holandesas</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Nova Caledônia</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Nova Zelândia</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nicarágua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Níger</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nigéria</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Ilha Norfolk</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Ilhas Marianas Setentrionais</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Noruega</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Omã</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Paquistão</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palau</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Território Palestino</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panamá</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papua Nova Guiné</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguai</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Peru</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Filipinas</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>ilhas Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Polônia</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portugal</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Porto Rico</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Qatar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>A Reunião</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Romênia</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Federação Russa</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Ruanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>São Bartolomeu</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Santa Helena, Ascensão e Tristão da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Saint Kitts e Nevis</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Santa Lúcia</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>San Martin</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>São Pedro e Miquelón </translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>Saint Pierre e Miquelon</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>San Marino</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>São Tomé e Príncipe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Arábia Saudita</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Senegal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Sérvia</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seychelles</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Serra e Leoa</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapura</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Eslováquia</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Eslovênia</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Ilhas Salomão</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somália</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>África do Sul</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Geórgia do Sul e Ilhas Sandwich do Sul</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>Espanha</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Sudão</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Suriname</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Svalbard y Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Suazilândia</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Suécia</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Suíça</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>República Árabe da Síria</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Taiwan</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tadjiquistão</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>República Unida da Tanzânia</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Tailândia</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Timor-Leste</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinidad e Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunísia</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Turquia</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Turcomenistão</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Ilhas Turcas e Caicos</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Uganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ucrânia</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Emirados Árabes Unidos</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Reino Unido</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>Estados Unidos</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Ilhas Menores Distantes dos Estados Unidos</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguai</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Uzbequistão</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Cidade do Vaticano</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Vietnã</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Ilhas Virgens Britânicas</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Ilhas Virgens dos E.U.A</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis e Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Saara Ocidental</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Iêmen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Zâmbia</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Zimbábue</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Área</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Pólo Sul</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Vostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Territótio da Capital Australiana</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Norte</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Nova Gales do Sul</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Sul</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tasmânia</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Vitória</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Oeste</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasília</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasília - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasília + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alasca</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlântico</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Central</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Oriental</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Havaí</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Montanha</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>Terra Nova</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pacífico</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Ilha de Páscua</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Galápagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Marquesas</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Taiti</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Ocidental</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Leste</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Ilhas Fênix</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Ilhas da Linha</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Ilhas Gilbert</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Noroeste</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Açores</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madeira</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrado</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnoyarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Moscou</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Vladivostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Yakutsk</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Ecaterimburgo</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Ilhas Canárias</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Despertar</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Ajusta automaticamente para ao horário de verão</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Data e hora atual</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 horas</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 horas</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Configure relógio com NTP</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>true</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>false</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>Use o formato de 24 horas</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP Server</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP servidor</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>Servidor 1 NTP</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>Servidor 2 NTP</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (inglês)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (alemão)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (espanhol)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (francês)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (chinês simplificado)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Japonês)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (coreano)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (russo)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (italiano)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Turco)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Idioma:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Trocar para formato standard</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Amostras para o formato selecionado:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizar</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Display</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Brilho</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Protetor de telas</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Ativa protetor de tela automático</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensagem</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Senha do protetor de tela</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Calibrar o touchscreen</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>Ativar acesso VNC</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>Alternando o acesso VNC ou proteção com senha, desconectará as conexões existentes</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Senha de acesso remoto</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Essa senha é usada para todo o acesso remoto, por exemplo, vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>Houve um erro ao se inicializar o VNC</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Exigir senha para acesso VNC</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Acesso Inteligente em qualquer lugar</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Código de acesso</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>Endereço IP do LAN</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Endereço IP de Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Número de conexões VNC</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Atualização</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>Não é possível atualizar, a alimentação CA não está conectada. Conecte a alimentação CA e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>Não foi possível detectar um dispositivo USB. Por favor, certifique-se que o dispositivo está conectado e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Não foi possível atualizar. Por favor, especifique o servidor de atualização e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>A conexão de rede não está disponível. Por favor, verifique a conexão de rede e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Não foi possível acessar o servidor de atualização. Por favor, verifique o endereço do servidor e tente novamente</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Foi detectado um erro ao procurar por atualizações disponíveis</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>Não é possível realizar a atualização, a atualização selecionada tem arquivos corrompidos ou ausentes.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>Não é possível executar a atualização, a atualização selecionada tem arquivos corrompidos.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>Falha ao gerar os dados de atualização USB</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Não foram encontradas atualizações, verifique suas configurações e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Falha ao tentar realizar a atualização, pot favor, tente novamente. Se o problema persistir, entre em contato com seu representante de vendas</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Bloqueio do equipamento</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Bloquear o test set evita o acesso sem autorização. A proteção de tela é mostrada para proteger a tela</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Bloquer o equipamento</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Não foi estabelicida uma senha. Os usuários poderão desbloquear o equipamento sem inserir uma senha</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Configurações de senha</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Essas configurações são compartilhadas com o protetor de telas</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Exige senha</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Áudio</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Volume das caixas de som</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Mudo</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Volume do microfone</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Procurando Satélites</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>Latitude</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>Longitude</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>Altitude</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Timestamp</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Número de satélites em fixos</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>Média SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Informação Individual de Satélite</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Informação do sistema</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Estabelecendo conexão com o servidor...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Conexão estabelecida...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Baixando arquivos...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Carregando arquivos...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Baixando informações de upgrade...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Carregando log do arquivo...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Sincronizado com sucesso com o servidor.</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>Bin em espera Esperando para ser adicionado ao inventário...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Falha ao sincronizar com o servidor.</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Conexão perdida durante a sincronização. Verifique a configuração de rede.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Falha ao sincronizar com o servidor. O servidor está ocupado.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Falha ao sincronizar com o servidor. Erro de sistema detectado.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Não foi possível estabelecer uma conexão. Verifique a configuração de rede ou ID da conta.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Sincronização abortada.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Configuração de dados Concluída</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Relatórios completos.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Digite o ID da conta.</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Iniciar sincronização</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Parar sincronização</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Endereço do servidor</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>ID da conta</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuração</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opção</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Redefinir Padrões</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Video Player</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Navegador Web</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Depurar</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Dispositivo série</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Perimitir a Getty controlar o dispositivo serial</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Conteúdo de /raiz/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testes</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Teste Set Up Screening (filtro de configuração)</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Gerente do trabalho</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Informação trabalho</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Número do trabalho</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Local do teste</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>Informação trabalho é adicionada automaticamente aos relatórios de teste.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Histórico</translation>
        </message>
    </context>
</TS>
