<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>文件</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>U 盘</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>移动存储</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>未检测到设备。</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>格式</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>格式化USB设备会删除所有数据。包括所有文件和分区。</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>弹出T</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>浏览...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>蓝牙</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>蓝牙需要配对</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>输入 PIN 进行配对</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>线对</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>配对请求</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>配对请求来自：</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>要与设备配对，请确保以下显示的代码同设备上的代码相匹配</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>已启动配对</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>配对请求发送至：</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>开始扫描</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>停止扫描</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>设置 </translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>启用蓝牙</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>允许其他设备与本设备配对</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>设备名称</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>启用的移动应用</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>网络</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>IP模式</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>静态</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC地址</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP地址</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>子网掩码</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>网关</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>DNS服务器</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>IPv6模式</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>禁用</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>手动</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>自动</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6 地址</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>子网前缀长度</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>DNS 服务器</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>链路本地地址</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>无状态地址</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>有状态地址</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>未加载模块</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>存在</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>不存在</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>启动中</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>启用中</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>初始化</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>就绪</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>扫描中</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>禁用中</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>停止中</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>关联中</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>已连上</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>启用无线适配器</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>已连接</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>已断开</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>故障</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>连接到 WPA 企业网络</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>网络名称</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>外部认证方法</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>内部认证方法</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>匿名身份</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>密码</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>证书</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>私钥密码</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>连接到 WPA 个人网络</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>密码</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>未找到U 盘无线设备。</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>停止连接</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>忘记网络</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>无法连接到网络。</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>3G 服务</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>启用的调制解调器</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>调制解调器被启用</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>调制解调器被禁用</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>未侦测到调制解调器</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>错误 - 调制解调器无响应</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>错误 - 设备控制器未启动</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>错误 - 未能配置调节解调器</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>警报 - 调制解调器无响应调制解调器正被重置…</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>错误 -SIM 未插入</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>错误 - 网络登录遭拒</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>连接</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>断开</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>正在连接</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>断开</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>未连接</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>连接失败</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>调制解调器设置</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>启用调制解调器</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>未找到或启用调制解调器</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>网络 APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>连接状态</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>检查 APN 并重试。</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS 服务器</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec DNS 服务器</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>代理服务器和安全</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>代理</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>代理服务器类型</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>代理服务器</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>网络安全</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>禁用 FTP 、 telnet 和自动 StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>电源管理</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>已插入交流电</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>使用电池电源运行</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>未检测到电池</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>因为电源消耗已停止充电</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>电池充电中</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>电池充电完成</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>不充电电池</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>因为电源故障无法对电池充电</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>未知电池状态</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>电池温度过高，可能无法充电</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>电池温度过高&#xA;请连通交流电源</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>电池温度过高&#xA;不要断开交流电源</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>电池温度过高&#xA;断开交流电源会强行关闭设备</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>电池温度过低，可能无法充电</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>电池处于过热危险</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>电池温度正常</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>充电</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>使用电池时启用自动关闭</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>无效时间（分钟）</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>日期和时间</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>时区</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>地区</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>非洲</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>美洲</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>南极洲</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>亚洲</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>大西洋</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>澳大利亚</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>欧洲</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>印度洋</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>太平洋</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>国家</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>无</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>阿富汗</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Åland Islands</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>阿尔巴尼亚</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>阿尔及利亚</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>美属萨摩亚</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>安道尔</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>安哥拉</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>安圭拉</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>安提瓜和巴布达</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>阿根廷</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>亚美尼亚</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>阿鲁巴</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>奥地利</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>阿塞拜疆</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>巴哈马</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>巴林</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>孟加拉国</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>巴巴多斯</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>白俄罗斯</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>比利时</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>伯利兹</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>贝宁</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>百慕达群岛</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>不丹</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>玻利维亚</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>波斯尼亚和黑塞哥维那</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>博茨瓦纳</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>布韦岛</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>巴西</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>英属印度洋领土</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>文莱达鲁萨兰</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>保加利亚</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>布基纳法索</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>布隆迪</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>柬埔寨</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>喀麦隆</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>加拿大</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>佛得角</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>开曼群岛</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>中非共和国</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>乍得</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>智利</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>中国</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>圣诞岛</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>科科斯群岛</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>哥伦比亚</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>科摩罗</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>刚果</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>刚果民主共和国</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>库克群岛</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>哥斯达黎加</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Côte d'Ivoire</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>克罗地亚</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>古巴</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>塞浦路斯</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>捷克共和国</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>丹麦</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>吉布提</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>多米尼加</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>多米尼加共和国</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>厄瓜多尔</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>埃及</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>萨尔瓦多</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>赤道几内亚</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>厄立特里亚</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>爱沙尼亚</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>爱塞尔比亚</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>福克兰群岛(马尔维纳斯群岛)</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>法罗群岛</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>斐济</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>芬兰</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>法国</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>法属圭亚那</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>法属波利尼西亚</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>法属南部领土</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>加蓬</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>冈比亚</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>格鲁吉亚</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>德国</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>加纳</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>直布罗陀</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>希腊</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>格林兰岛</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>格林纳达</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>瓜德罗普岛</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>关岛</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>危地马拉</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>格恩西岛</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>几内亚</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>几内亚比绍</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>圭亚那</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>海地</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>荷德和马克多纳群岛</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>洪都拉斯</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>香港</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>匈牙利</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>冰岛</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>印度</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>印度尼西亚</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>伊朗</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>伊拉克</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>爱尔兰</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>马恩岛</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>以色列</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>意大利</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>牙买加</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>日本</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>泽西</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>约旦</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>哈萨克斯坦</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>肯尼亚</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>基里巴斯</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>朝鲜民主主义人民共和国</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>韩国</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>科威特</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>吉尔吉斯斯坦</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>老挝人民民主共和国</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>拉脱维亚</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>黎巴嫩</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>莱索托</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>利比里亚</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>利比亚</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>列支敦士登</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>立陶宛</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>卢森堡</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>澳门</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>马其顿，前南斯拉夫共和国</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>马达加斯加</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>马拉维</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>马来西亚</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>马尔代夫</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>马里</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>马耳他</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>马绍尔群岛</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>马提尼克</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>毛里塔尼亚</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>毛里求斯</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>马约特</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>墨西哥</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>密克罗尼西亚联邦</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>摩尔多瓦共和国</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>摩纳哥</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>蒙古</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>黑山</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>蒙特塞拉特岛</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>摩洛哥</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>莫桑比克</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>缅甸</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>纳米比亚</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>瑙鲁</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>尼泊尔</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>荷兰</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>荷属安的烈斯群岛</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>新喀里多尼亚</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>新西兰</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>尼加拉瓜</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>尼日尔</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>尼日利亚</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>纽埃岛</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>诺福克岛</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>北马里亚纳群岛</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>挪威</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>阿曼</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>巴基斯坦</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>帕劳</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>巴勒斯坦民族权利机构</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>巴拿马</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>巴布亚新几内亚</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>巴拉圭</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>秘鲁</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>菲律宾</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>皮特克鲁恩岛</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>波兰</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>葡萄牙</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>波多黎各</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>卡塔尔</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>留尼汪</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>罗马尼亚</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>俄罗斯联邦</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>卢旺达</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>圣巴特岛</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>圣赫勒拿岛/阿森松岛和特里斯坦达库尼亚</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>圣基茨和尼维斯安圭拉</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>圣卢西亚岛</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>圣马丁岛</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>圣皮埃尔和密克隆群岛</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>圣文森特和格林纳丁斯</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>萨摩亚</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>圣马力诺</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>圣多美和普林西比</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>沙特阿拉伯</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>塞内加尔</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>塞尔维亚</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>塞舌尔</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>塞拉利昂</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>新加坡</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>斯洛伐克</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>斯洛文尼亚</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>所罗门群岛</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>索马里</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>南非</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>南乔治亚岛与南桑威奇群岛</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>西班牙</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>斯里兰卡</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>苏丹</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>苏里南</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>斯瓦尔巴德和杨马延群岛</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>斯威士兰</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>瑞典</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>瑞士</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>阿拉伯叙利亚共和国</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>台湾</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>塔吉克斯坦</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>坦桑尼亚联合共和国</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>泰国</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>东帝汶</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>多哥</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>托克劳</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>汤加</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>特立尼达和多巴哥</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>突尼斯</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>土耳其</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>土库曼斯坦</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>特克斯和凯科斯群岛</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>图瓦卢</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>乌干达</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>乌克兰</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>阿拉伯联合酋长国</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>英国</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>美国</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>美国较小的外围群岛</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>乌拉圭</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>乌兹别克斯坦</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>瓦努阿图</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>梵蒂冈城</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>委内瑞拉</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>越南</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>英属维尔京群岛</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>美属维尔京群岛</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>瓦利斯群岛和富图纳群岛</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>西撒哈拉</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>也门</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>赞比亚</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>津巴布韦</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>区域</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>凯西</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>戴维斯岛</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>迪蒙‧迪尔维尔</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>莫森</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>麦克默多 </translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>帕尔默 </translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>罗西拉 </translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>南极</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>昭和</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>沃斯托克</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>澳大利亚首都直辖区</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>北</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>新南威尔士</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>昆斯兰</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>南</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>塔斯马尼亚岛</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>维多利亚</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>西</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>巴西利亚</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>巴西利亚 - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>巴西利亚 + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>阿拉斯加</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>亚利桑那</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>大西洋</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>中</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>东</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>夏威夷</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>山地</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>纽芬兰</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>太平洋</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>萨斯喀彻温省</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>复活节岛</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>金沙萨</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>卢本巴希</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>加拉帕戈斯群岛</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>甘比尔群岛</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>马克萨斯群岛</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>大溪地岛</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>西</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>格陵兰</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>东</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>菲尼克斯群岛</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>莱恩群岛</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>吉尔伯特群岛</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>西北</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>科斯雷</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>特鲁克</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>亚速尔群岛</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>马德拉岛</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>伊尔库茨克</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>加里宁格勒</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>克拉斯诺雅茨克</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>马加丹</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>莫斯科</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>欧姆斯克</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>海参崴</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>雅库次克</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>叶卡特琳堡</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>加那利群岛</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>斯瓦尔巴德</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>杨马延群岛</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>约翰</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>中途岛</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>维克</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>夏令时自动调整</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>当前值日期和时间</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24小时</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12小时</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>设置附带 NTP 的时钟</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>真</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>假</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>使用24小时制</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP 服务器</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP 服务器</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>NTP 服务器 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>NTP 服务器 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English（英语）</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch（德语）</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español（西班牙语）</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français（法文）</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語（日文）</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어（韩文）</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский（俄文）</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português （葡萄牙文）</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano（意大利语）</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk （土耳其语）</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>语言：</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>更改格式标准：</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>所选格式示例：</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>自定义</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>显示</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>亮度</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>屏保</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>启用自动屏保</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>消息</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>屏保程序密码</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>校准触摸屏...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>远程</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>启用VNC访问</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>切换VNC访问或密码保护会断开现有连接。</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>远程访问密码</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>此密码将用于 vnc、ftp、ssh 等所有远程访问。</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>启动VNC发生错误。</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>VNC 访问需要密码</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>随处进行智能访问</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>访问代码</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>LAN IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Wi-Fi IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>VNC连接数</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>升级</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>无法升级，交流电源未接通。请接通交流电源，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>无法检测到U 盘存储器。请确认U 盘存储器插紧然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>无法升级。请指定升级服务器然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>网络连接不可用。请检查网络连接，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>无法连接升级服务器。请检查服务器地址然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>检索可用升级时发生错误。</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>无法执行升级，所选升级文件缺失或已损坏。</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>无法执行升级，所选升级文件已损坏。</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>生成U 盘升级数据发生错误。</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>未找到升级，请检查设置然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>升级失败请重试。如果该问题仍然存在，请联系销售代表。</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>测试设备锁定</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>锁定测试设备可防止非法访问。显示屏保保护屏幕安全。</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>锁定测试设备</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>所需密码未设置。用户无需输入密码解锁测试设备。</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>设置设置</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>这些设置与屏保共享。</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>需要密码</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>音频</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>扬声器音量</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>静音</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>麦克风音量</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>搜索卫星</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>纬度</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>经度</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>海拔</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>时间戳</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>固定的卫星数量</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>平均值 SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>单个卫星信息</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>系统信息</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>正在建立与服务器的连接 ...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>已建立连接 ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>正在下载文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>正在上传文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>正在下载升级信息 ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>正在上传日志文件 ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>成功与服务器同步。</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>在储存箱中。 等待被添加到目录中 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>未能与服务器同步。</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>同步过程中连接中断 检查网络配置。</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>未能与服务器同步。 服务器忙。</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>未能与服务器同步。 检测到系统错误。</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>未能建立连接。 检查网络配置或账户 ID.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>同步已中止。</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>配置数据完成。</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>报告完成。</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>输入账户 ID 。</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>开始同步</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>停止同步</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>服务器地址</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>账户 ID</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>选项</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>恢复默认设置</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>视频播放器</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>网页浏览器</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>调试</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>串行设备</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>允许Getty控制串行设备</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>/root/debug.txt 目录</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>设置屏蔽测试</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>作业管理器</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>作业信息</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>作业编号</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>作业信息自动添加到测试报告。</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>历史记录</translation>
        </message>
    </context>
</TS>
