<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>{1} 字节帧:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>{1} 字节 帧</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>{1} 字节包:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>{1} 字节 包</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} 帧突发: 失败</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} 帧突发: 通过</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{2} 的 {1}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} 包突发: 失败</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} 包突发: 通过</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} 恢复 {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   需要替换吗？</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        按 OK 重试</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} 正在测试 VLAN ID {2} 为 {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} 等待 ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       你可以改变名字来创建一个新的配置 .</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 个主要会话者 (共 {1} 个 IP 会话)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50个主要 TCP 重新传输会话 (共 {1} 个会话)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>放弃测试</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>活跃</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>活动循环</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>工作回路不成功。 VLAN ID 测试已中止</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>实际的测试</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>增加范围</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>超过所设定帧丢失阀值的帧丢失率</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>在您执行手动测试后或您需要的任何时间，您可以</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>发现硬件循环</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>未找到硬件循环</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>全部测试</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>环回应用程序是一个不兼容的应用程序</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>最大值吞吐量测量不可用</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>发现活动循环</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>和</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>和 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>找到 LBM/LBR 回路。</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>找到 LBM/LBR 回路。</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>找到永久环路</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>在报表的结尾添附进展记录</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>应用名</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>近似全部时间:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>发生响应超时。&#xA;{1} 秒内没有对最后一个命令响应 &#xA;</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> 假定存在硬环</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非对称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>上行模式中近端到远端和下行模式中远端到近端的不对称传输。组合模式将运行测试两次，先使用“本地设置”以上行方向传输，再使用“远程设置”以下行方向传输。使用此按钮用当前本地设置覆盖远程设置。</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>正在尝试</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>尝试上行循环</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>正在尝试登录服务器 ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>循环启动尝试已失败。 测试停止中</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>上行循环尝试未成功</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>自协商</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>自协商完成</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>自协商设置</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>自协商状态</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>可获得的</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>平均值突发</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>平均值包速率</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>平均值包大小</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>平均和最大平均数据包抖动测试结果：</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>平均延迟 (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>平均延迟 (RTD): 不适用</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>平均数据包抖动:</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Avg Packet Jitter: 不适用</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>平均包抖动 (纳秒)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>平均值速率</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>背靠背</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>背靠背帧粒度</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>背靠背帧粒度</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>背靠背帧测试</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>背靠背帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>背靠背最长测试时间</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>背靠背最长测试时间</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>背靠背测试结果</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>返回摘要</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>带宽粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>带宽粒度 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>带宽测试精确度</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>带宽测试精确度</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>基本负载测试</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Beginning of range:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation>比特</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>两者都</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx 和 Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>本地和远程源 IP 地址均不可用</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom Up</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>缓冲器</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>缓冲器信用量</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>缓存点&#xA;(需要吞吐量)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>缓冲器间信用量</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>缓冲器信用量测试时长:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>缓冲器信用量测试时长</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>缓冲器信用量测试</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>缓冲器信用量测试结果:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>缓冲器信用量吞吐</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Buffer Credit Throughput {1} Bytes:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>缓存点吞吐量&#xA;(需要缓存点)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>缓冲器信用量吞吐测试</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>缓冲器信用量吞吐测试结果:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>缓冲器尺寸</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>突发</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>突发粒度 (帧)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>通过查看一段时间内 TCP 重新传输与网络利用率，可以将网络性能差与拥塞等网络损耗情况关联。</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>通过查看 IP 会话表，可以通过字节或帧识别“主要会话者”。术语 S &lt;- D 和 S -> D 是指字节和帧的从来源到目标和从目标到来源流量方向。</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(字节)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation>字节</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(字节)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>字节 &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>字节 &#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Calculated&#xA;Frame Length</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Calculated&#xA;Packet Length</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>正在计算 ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>无法继续</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>捕获分析摘要</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>捕捉持续时间</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>截取&#xA;屏幕</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>注意!&#xA;&#xA;你确认你要永久&#xA;删除这个配置吗?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>配置速率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>配置速率</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>配置速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>机箱 ID</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>选中的 Rx 项目将被用于配置筛选器来源设置。</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>选中的 Tx 项目将被用于配置 Tx 目标设置。</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>检查活动循环</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>检查硬件循环</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>检查活动循环</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>正在检查 LBM/LBR 回路。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>正在检查永久环路</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>检查半双工端口的检测</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>检查 ICMP 帧</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>检查可能的重新传输或高带宽利用率</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>检查硬循环</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>正在检查 LBM/LBR 回路</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>正在检查永久环路</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>检查协议层次统计数据</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>检查来源地址可用性 ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>选中此复选框将在退出测试时，把测试设置恢复为原始设置。对于不对称测试，会在本地端和远端恢复设置。恢复设置将使连接被重新设置。</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>检查来源 IP 和其所连设备之间的端口设置；验证半双工情况不存在。通过使分析器更加接近目标 IP 也可以获得进一步的细分；判断是否已取消重新传输以隔离缺陷连接。</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>选择一个要分析的捕获文件</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>选择 &#xA;PCAP 文件</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>选择你需要的带宽测试精度&#xA; (最短的测试时间推荐使用 1%).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>选择流控登录类型</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Choose the Frame or Packet Size Preference</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>选择您希望运行紧接测试的间隔。</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>选择帧丢失测试精度</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>选择线路所配置的最大值带宽。设备将使用该数字作为传输的最大带宽，以减少测试时间长度：</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>选择最大值缓存点大小。&#xA; 测试将使用该数字来启动，以减少测试时间长度：&#xA;注意：必须使用下列参数设置环回流量的&#xA;远程设备：&#xA;&#xA;1.  开启流量控制&#xA;2.  {1}&#xA;3.  {2} 缓存点设置为上面输入的同一值。</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>选择背靠背测试最大值测试时间</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>选择“自顶向下”或“自底向上”测试程序中所用的最小和最大值负载值。</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>选择每个帧尺寸要运行的背靠背测试次数</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>选择每个帧尺寸下要运行的时延测试次数</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>选择为每个帧尺寸想要运行数据包抖动测试的试验次数。</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>选择吞吐量丢帧率所允许的错误比例 .&#xA; 注意: RFC2544 标准中为 0</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>选择每个时延测试的时长</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>选择每次数据包抖动试验持续的时间。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>选择通过吞吐量测试所需的无误发送速率的时间。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>选择希望各丢帧试验持续的时间。</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>选择缓冲器信用量测试运行次数</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>选择丢帧测试中使用的程序。 &#xA; 注意： RFC2544 程序使用最大带宽开始运行，然后在接下来的试验中按带宽间隔逐次降低。如果在连续的两次试验中都没有丢失帧，程序将结束。</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>思科发现协议</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>在此网络中检测到 Cisco 发现协议 (CDP) 消息，此表列出了播发半双工设置的 MAC 地址和端口。</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>全部清除</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>单击“结果”按钮可切换到标准用户界面。</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>编码违例</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>组合</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>注释</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>注释</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>与远端成功建立通信</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>不能与远端建立通信</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>远端通信已断开</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>% 完成</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>完成 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>配置名称</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>配置名</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>配置名称</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>需要配置名</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>配置只读</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>配置概要</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>配置选中的项目</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>配置 {1} 发送流量的时长。</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>确认替换配置</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>确认删除</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>已连接</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>正在连接</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>连接到测试应用</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>继续半双工</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>继续半双工模式</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>将本地设置&#xA;复制到远程设置</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>复制</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>无法上行循环远端</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>创建报告</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>credits</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(信用量)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA; 当前值脚本 : {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>当前值选择</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>用户</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>用户</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>有效比特率</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>数据字节率</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>数据层停止</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>数据模式</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>数据模式设置为 PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>数据大小</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日期</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>日期 &amp; 时间</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>天</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>时延当前值 (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>时延 , 当前值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>目的地址</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>目的配置</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>目的 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>目的 IP&#xA; 地址</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>未找到 IP 地址 {1} 的目标 MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>目的 MAC 发送 .</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>目标 MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>详细信息标签</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>详细</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>检测到</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>检测到</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>检测到连接带宽</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>检测到比所传送的带宽 {1} 更多的帧 - 无效测试</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>确定对称吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>设备详情</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>设备 ID</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>DHCP 参数不可用</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>发现 DHCP 参数 .</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>发现设备</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>正在发现</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>正在发现远端回路类型 ...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>发现&#xA;当前&#xA;不&#xA;可用</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>按以下方式显示：</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>下游</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>下行方向</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> Do you wish to proceed anyway? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>双工模式</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>持续时间/期间</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>启动的</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>启用扩展第 2 层流量测试</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>封装:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>封装</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>结束</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>结束日期</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>End of range:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>中止时间</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>为 FTP 测试输入 IP 地址或服务器名</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Enter the Login Name for the server to which you want to connect</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Enter the password to the account you want to use</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>输入新配置名&#xA;(使用字母, 数字, 空格, 横线和下划线):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Error: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>错误：发生响应超时&#xA;在 内没有响应</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>错误: 无法建立连接</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>错误数</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>差错帧</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>加载 PCAP 文件错误</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>错误: 解析为首的 DNS 失败</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>错误: 无法找出网站</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>预计剩余时间</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>预计剩余时间</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>以太网测试报告</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>事件</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>事件日志已满。</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>发现过度重新传输</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>退出 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>预计吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>预计吞吐量是</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>预计吞吐量不可用</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>“专家 RFC 2544 测试”按钮。</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>显性的 (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>显性 (Fabric/N-Port)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Explicit login was unable to complete. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>故障</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>远端是 JDSU Smart Class 以太网测试设置</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>远端是 Viavi SmartClass 以太网测试仪</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>光纤通道测试</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>使用 Acterna 测试净荷执行 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC 测试报告</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>全双工</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>光纤通道测试报告</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>文件配置</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>文件名</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>文件</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>文件尺寸</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>文件尺寸:</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>文件尺寸</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>文件尺寸: {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>文件尺寸:</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>文件尺寸</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>文件传输太快。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>查找预计吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>查找“主要会话者”</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>前 50 个半双工端口 (共 {1} 个)</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>前 50 条 ICMP 消息 (共 {1} 条)</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>流控</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flow Control Login Type</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>文件夹</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation>各个帧被减少到一半补偿纤维的双重长度。</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>找到</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>已找到工作回路。</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>已找到硬件回路。</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>帧长度</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>帧长度</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>帧长 (字节)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>帧长:</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>帧长</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>测试帧长</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>帧丢失 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>帧丢失 {1} 字节:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>帧丢失 {1} 字节</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>帧丢失带宽粒度:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>帧丢失带宽粒度</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>丢帧最大值带宽</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>丢帧最小值带宽</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>丢帧率</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>丢帧率</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>帧丢失测试</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>帧丢失测试步骤:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>帧丢失测试步骤</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>帧丢失测试结果:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>帧丢失容限 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>帧丢失测试时长:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>帧丢失测试时长</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>帧或包</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>帧大小</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>尺寸: {1} 字节</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>帧 &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>帧 &#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>成帧</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(帧)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(帧/秒)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>FTP 吞吐量</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>FTP 吞吐量测试 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>FTP 吞吐量测试完成</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>FTP 吞吐量测试报告</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>全</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>获取 PCAP 信息</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>半</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>半双工端口</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>硬件</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>硬件循环</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>(硬件&#xA;或活动状态)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>(硬件、&#xA;永久&#xA;或有效)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>半双工</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>高利用率</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>主页</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>小时</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>HTTP 吞吐量测试</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>HTTP 吞吐量测试报告</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>ICMP&#xA; 代码</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>ICMP 消息</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>如果错误计数器偶发性增加，请运行手动</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>如果问题存在 , 请复位测试为缺省设置</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>如果无法解决偶发性错误问题，您可以设置</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implicit (Transparent Link)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>信息</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>初始化通信</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>为确定以下情况的带宽，</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>本地和远端输入速率不匹配</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>线路上断断续续出现问题。</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>内部错误</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>内部错误－重新开始 PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>无效配置</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>无效的 IP 配置</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>IP 会话</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>正在退出</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>正在启动</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>抖动测试</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck 完成</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck 断掉连接或无法建立连接</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>再次运行 J-QuickCheck 可重启 L2 流量测试。</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>环路时延</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>时延 (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>延迟(RTD)和数据包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>平均延迟 (RTD)：不适用</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>环路时延测试通过门限:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>环路时延测试通过门限</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>环路时延测试&#xA;(需要吞吐量测试)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>延迟 (RTD) 结果</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>环路时延测试</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>环路时延测试结果:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>环路时延测试结果: 中止</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>环路时延测试结果: 失败</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>环路时延测试结果: 通过</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>延迟 (RTD) 测试结果已跳过</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>延迟 (RTD) 测试已跳过</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation>环路时延测试门限: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Latency (RTD) Threshold (us)&lt;</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>环路时延测试时长:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>环路时延测试时长:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>环路时延测试 (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>第1层</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>第 1 / 2 层&#xA;以太网运行状况</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>第2层</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>2 层当前链路发现</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>第 2 层快速测试</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>第3层</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>第 3 层 &#xA;IP 运行状况</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>第4层</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>第 4 层 &#xA;TCP 运行状况</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR 环回</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>长度</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>发现链路</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>链路层发现协议</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>连接断掉</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>捕获文件中检测到的连接速度</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>监听端口</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>加载单位</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>加载中 ... 请稍候</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>本地</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>本地目标 IP 地址被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>本地目标 MAC 地址被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>本地目标端口被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>本地循环类型被配置为单播</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>本地端口</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>本地远程 IP 地址被配置为</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> 本地序列号</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>本地设置</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> 本地软件修订</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>本地来源 IP 筛选器被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>本地来源 MAC 筛选器被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>本地来源端口筛选器被配置为</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>本地摘要</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> 本地测试仪表名称</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>用表中列出的来源 MAC 地址和端口定位该设备，并确保双工 设置被设定为“全”或“自动”。主机被设置为“自动”，网络设备被设置为“自动”，连接错误协商为半双工并不罕见。</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>位置</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>位置</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>登录:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>登陆</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>登录名:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>登录名</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>循环失败</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>与远端设备拆除环路</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>与远端设备建立环路 ...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>循环状态未知</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>上行循环失败</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>上行循环成功</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>环路成功</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>检测到第 2 层链路损失</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>丢失</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>丢帧</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>管理地址</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>MAU 类型</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>最大值</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( 最大 {1} 字符 )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>最大平均值</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>最大平均数据包抖动：</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>最大平均包抖动: 不适用</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>最大平均包抖动 (纳秒)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>最大带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>最大带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>最大缓存大小</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>最大值时延，平均值允许时延测试通过</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>最大值包抖动，平均值允许包抖动测试通过</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>最大值 Rx 缓冲器信用量</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>最大值测试带宽</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>最大值测试带宽</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>测到的最大值吞吐量：</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>每个VLAN ID最大值时限{1}</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>每个VLAN ID最大值时限7天</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>最大值时长 (秒)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>最大值 Tx 缓冲器信用量</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>最大速率</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>已达到最大重新传输尝试次数。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Measured</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Measured Rate (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Measured Rate</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Measured Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>测试准确度</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Measuring Throughput at {1} Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>信息</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>最小值</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>百分比带宽最小值</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>吞吐量测试通过最小带宽百分比:</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>每个VLAN ID最大时限5秒</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>最小速率</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>分钟</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>分钟</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>分钟</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>型号</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>修改</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>MPLS/VPLS 封装当前不支持 ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>不适用 (硬环)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>两者都不</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>网络联上</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>网络利用率</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>未检测到网络利用过度，但此图表提供网络利用图</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>未检测到网络利用过度，但此表提供 IP 主要会话者清单</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>新建</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>新配置名称</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>新 URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>下一步</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>否.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>未发现匹配应用</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt; 没有可用配置 ></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>No files have been selected to test</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>未找到硬件环路</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>否&#xA;JDSU&#xA;设备&#xA;已发现</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>未检测到第 2 层链路</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>未能建立循环</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>未建立或找到环路</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>未找到永久环路</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>正在运行应用检测</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>与 RFC2544 不一致</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>未连接</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>未确定</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>注意:</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Note: Assumes a hard-loop with Buffer Credits less than 2.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;注意：假设缓存点小于2的硬回路。&#xA;&#xA;本测试无效。&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Note: Based on the hard loop assumption, minimum buffer credits calculated</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Note: Based on the hard loop assumption, throughput measurements are made at twice</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>注意：如果您使用帧丢失容错，则测试不遵守</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>注释</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>未选择…</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>未&#xA;发现 &#xA;Viavi&#xA; 设备</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>正在退出 ...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>正在校验</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Now verifying with {1} credits.  This will take {2} seconds.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>背靠背测试次数</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>背靠背测试次数</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Number of Failures</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Number of IDs tested</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>环路时延测试次数:</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>环路时延测试次数</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>包抖动计数测试次数:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>包抖动计数测试次数</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>数据包数</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Number of Successes</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>测试次数:</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>测试次数</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>of</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>帧在一秒内丢失</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>J-QuickCheck 预期吞吐量的</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(线速率 %)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>线速率 %</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>线速率 %</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * Only {1} Trial(s) yielded usable data *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(开或关)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 帧</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>发生器 ID 标识</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>超出范围</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>全部测试结果:{1}</translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    全面的测试结果：失败   </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>超出范围</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>在最后 10 秒，即使流量应被停止</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>包</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>包抖动</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>包抖动 , 平均</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>包抖动测试通过门限:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>包抖动测试通过门限</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>包抖动测试&#xA;(需要吞吐量测试)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>数据包抖动结果</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>包抖动测试结果: 中止</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>包抖动测试结果: 失败</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>包抖动测试结果: 通过</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation>包抖动测试门限: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>包抖动测试通过门限 (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>包抖动测试时长</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>包抖动测试时长</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>包长</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>包长 (字节)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>包长:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>包长</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>测试包长</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>数据包大小</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>包尺寸</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>通过 / 失败</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>通过/失败</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>通过率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>通过率 (frm/sec)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>通过率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>通过率 (包/秒)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>密码:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>密码</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>暂停</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>暂停功能</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>暂停能力</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>检测到暂停桢</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>检测到暂停帧</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>检测到暂停帧 - 无效测试</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>PCAP 文件分析错误</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>PCAP 文件</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>待定</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>执行清理</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>永久</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>永久环路</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>(永久&#xA;或有效)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>包</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>数据包抖动 (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>包长度</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>丢包</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(包)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(包/秒)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>平台</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>请检查您已同步和连接，</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>请检查您是否正确连接，</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>请检查您是否正确连接，</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Please choose another configuration name.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>请输入保存报告的文件名称 (%{1} 个字以内 ) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>请输入您的备注（{1}个字以内）</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>请输入您的备注（%{1}个字以内）</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入您的备注（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Please enter your Customer's Name max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Please enter your Customer's Name max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入客户名称&#xA;（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入客户名称（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入客户名称（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Please enter your Technician Name ( max {1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Please enter your Technician Name max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入技术员名称（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Please enter your Test Location ( max {1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Please enter your Test Location max %{1} characters )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>请输入测试位置（{1}个字以内）&#xA;（只能使用字母、数字、空格、短划线和下划线）</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>按“连接到远程”按钮</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>请使用手动流量测试，验证连接的性能。</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>请验证本地和远程源 IP 地址然后重试</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>请验证本地源 IP 地址然后重试</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>请验证你的远程 IP 地址并重试 .</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>请验证远程源 IP 地址然后重试</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>请稍候 ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>请稍候 ...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Please wait while the PDF file is written.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>在生成 PDF 文件，请等待.&#xA;这可能将花费 90 秒 ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>端口:</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>端口</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Port {1}: Would you like to save a test report?&#xA;&#xA;Press Yes or No.</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>端口 ID</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>端口： 				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>端口： 				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 激活</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 认证失败</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 未知故障</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP 失败</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 失败</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPOE 已激活</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 故障</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE 未激活</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE 开始</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>PPPoE 状: 态</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE 超时</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE 上行</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPPoE 失败</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP 超时</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP 未知故障</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP 上行故障</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 失败</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>按 关闭 返回主屏幕 </translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>按“退出”返回至主屏幕或按“运行 RFC 2544 测试”重新运行。</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>按&#xA;刷新&#xA;按钮&#xA;以&#xA;发现</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>按“退出 J-QuickCheck ”按钮可退出 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>按下面的“刷新”按钮可查找当前子网中的 Viavi 设备。选择一个设备可在右表中查看详细信息。如果不能刷新，请检查以确保已启用“发现”，并使您可以同步和连接。</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>按“运行 J-QuickCheck ”按钮&#xA;可验证本地和远程测试设置及可用带宽</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>前一个</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>处理</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>属性</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>建议的下一步操作</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>提供商</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>随机</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> Range</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Range</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>速率</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>估计该连接可能有与最大值负载无关的一般问题。</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>从 {2} 接收到 {1} 字节</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Rx 帧</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>推荐</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>推荐手动测试 配置：</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>翻新</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>远端</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>远端 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>远程循环</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation> 远程序列号</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>远程序列号</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>远程设置</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>无法恢复远程设置</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> 远程软件修订</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>远程软件版本</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>远程摘要</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation> 远程测试仪表名称</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation> 远程测试仪表名称</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>删除范围</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>应答器 ID 标识</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>响应&#xA;路由器 IP</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>重新启动 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>退出前恢复测试前配置</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>正在恢复远程 测试设置 ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>将 RFC 限制为</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>基本负载试验结果不可用。请单击“建议下一步”，获取可能的解决方案</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>要监视的结果：</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>重新传输</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>发现了重新传输&#xA;正在分析一段时间内的重新传输次数</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>发现了重新传输请将文件导出至 U 盘 以进一步分析。</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>用户中断了恢复 {1}</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>单击  返回 RFC 2544 用户界面 </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>RFC 2544 Ethernet 测试报告</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>RFC 2544 模式</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>RFC 2544 模式</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC2544标准式</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>用 Acterna 测试净荷执行 RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544 测试报告</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>当多数据流图形测试结果运行时， RFC/FC 测试无法执行</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>RFC 模式</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY Det</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>运行</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>运行 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>运行 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>运行 $l2quick::testLongName</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>正在测试 {1}{2} 负载 .</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>正在测试 {1}{2} 负载 .  这将要花 {3} 秒 .</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>运行 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>运行脚本</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>第1层 Rx Mbps 当前值</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation> Rx </translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>存储 FTP 吞吐量测试报告</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>存储 HTTP 吞吐量测试报告</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>存储 VLAN 扫描测试报告</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> 缩放的带宽</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>所抓截屏</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>放弃脚本</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(秒)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>秒</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Select a name for the copied configuration</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Select a name for the new configuration</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Select a range of VLAN IDs</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>已选择 Tx 方向：</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>已选择 Tx 方向&#xA;下行</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>已选择 Tx 方向&#xA;上行</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>选择告警</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>选择 OK 来改变配置&#xA; 编辑名字来创建一个新配置&#xA;(使用字母, 数字, 空格, 横线和下划线)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>选择测试配置</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>选择您想要按什么属性查看列出的已发现设备。</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>选择你要进行的测试</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>选择 URL</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>选择负载相关设置所用的格式。</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>为获得目的 MAC 地址，Tx  ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>正在发送流量用于</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>发送流量，被 J-QuickCheck 发现的预期吞吐量将使用这个值缩放。</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>顺序 ID</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>串口号</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>服务器 ID:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>服务器 ID</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>业务名</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>显示通过/失败状态</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;显示通过/失败状态:</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>尺寸</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>略过</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>跳过延迟 (RTD) 测试并继续</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>软件版本</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation>软件修订版</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>源地址</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>源地址不可用</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>来源可用性已建立...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>源 ID</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>源 IP</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>源 IP&#xA;地址</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>源 IP 地址和目的地址一样</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>源 MAC</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>来源 MAC&#xA; 地址</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>指定连接带宽</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>速率</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>开始日期</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>启动基本负载测试</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>开始测试</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>开始时间</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>状态未知</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>研究该图可以判断 TCP 重新传输是否与降级的网络利用率相一致。查看 TCP 重新传输选项卡可以确定造成大量 TCP 重新传输的来源 IP 。检查来源 IP 和其所连设备之间的端口设置；验证半双工情况不存在。通过使分析器更加接近目标 IP 也可以获得进一步的细分；判断是否取消了重新传输以隔离缺陷连接。</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>成功 !</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>成功</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Summary of Measured Values:</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Summary of Page {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>对称</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>同步模式使用环回在近端发送并接收 . 异步模式上行从近端发送到远端 , 下行从远端发送到近端 .</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>对称</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      点击配置名选中 </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	接收到 {1} MB 文件 ....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	加入 {1} MB 文件 ...</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   速率： {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   速率： {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx Frames {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              小心 &#xA;&#xA;	    你确认需要永久 &#xA;	           删除该配置？ &#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   只能使用字母、数字、空格 &#xA;		   破折号和下划线</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   该配置为只读 &#xA;		   不能删除。</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         必须使用小键盘 &#xA;		           输入新配置名称。</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   指定配置已经存在。</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   时间： {1} 秒 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	 发送帧 {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>TCP 主机未能建立连接。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>TCP 主机遇到了一个错误。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>TCP 重新传输</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>工程师</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>工程师</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>工程师姓名</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>终结</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>                               测试失败</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>用户中止了测试。</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>用全部测试标签下配制的最大带宽测试</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>以不同的低流量速率进行测试。如果低流量条件下，仍有错误</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>测试完成</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>测试配置:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>测试配置</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>测试持续时间：至少 3 倍于设定的测试持续时间。</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>已测试的带宽</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### 测试执行完全 ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>测试 {1} 信用量</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>测试 {1} 信用量</translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>正在测试于 </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>测试连接…</translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> 测试仪表名称</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Test is starting up</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>测试地点</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>记录</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>测试名:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>测试名称： 			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>测试名称： 			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>测试步骤</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>测试进展记录</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>测试范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>测试范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>测试结果</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>测试仪表设置</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>运行的测试</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>运行的测试</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Test was aborted</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>工作回路失败，未找到硬回路。</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>工作回路失败，未找到硬或永久回路。</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>{1} 的平均值速率是 {2} kbps</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>{1} 的平均值速率是 {2} Mbps</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>文件超出 JMentor 的 50000 数据包限制</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>帧丢失容错门限，容错小的帧丢失速率。</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Internet 控制消息协议 (ICMP) 在 ICMP Ping 环境中使用最广。“无法连达 ICMP 目标”消息表示无法通过路由器或网络设备连接到目标。</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>L2 筛选器 Rx Acterna 帧计数继续增加</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>LBM/LBR 回路失败。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>本地和远端 IP 地址一样</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> 本地设置已成功复制到远程设置</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>本地源 IP 地址不可用</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>测量的 MTU 太小，不能继续。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>The name you chose is already in use.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>我们所连网元端口为半双工模式。如果连接，请按“继续使用半双工”按钮。否则</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>网络利用图显示捕获期间捕获文件内所有数据包使用的带宽。如果还检测到 TCP 重新传输，则建议返回主分析屏幕研究 TCP 层结果。</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>the number of buffer credits at each step to compensate for the double length of fibre.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>理论计算</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Theoretical &amp; Measured Values:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>邻居端口(网元)使自动协商关闭，预期吞吐量不可用，所以邻居端口最有可能为半双工模式。&#xA;如果邻居端口的半双工模式不正确，请将邻居端口的设置更改为全双工，并重新运行 J-QuickCheck。 然后，&#xA;如果测量的预期吞吐量更合理，则可运行 RFC 2544 测试。如预期吞吐量仍然不可用，请检查&#xA;远程的端口配置。可能存在 HD 至 FD 端口模式不匹配。 &#xA;&#xA; 如果邻居端口的半双工模式正确，请转至结果 -> 设置 -> 接口 -> 物理 &#xA; 层，并将“双工”设置从“全”更改为“半”。然后返回至 RFC2544 脚本(结果 -> 专家 RFC2544 测试)，退出 J-QuickCheck，转至“吞吐量接头”，并选择“ &#xA; 正在归零”的“ RFC 2544 标准(半双工)”，并运行 RFC 2544 测试。</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>远端出现通信故障。</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>远程设备未响应 Viavi 环回命令，但交换源和目标地址段后返回发送的帧到本地设备</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>远程环回设备响应 Viavi 环回命令，交换源和目标地址段后返回发送的帧到本地设备</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>远程回路设备原样返回发送的帧到本地设备</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>远程回路设备支持 OAM LBM ，响应接收到的 LBM 帧，向本地设备发送回相应的 LBR 帧。</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>远端被设置为 MPLS 封装</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>远端好像是环回应用程序</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>远程源 IP 地址不可用</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; 报表 {1}{2} 已用 PDF 格式保存了</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>报表 {1}{2} 已用 PDF 格式保存了</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>报表 {1}{2} 已用 PDF, TXT 和 LOG 格式保存了</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>该报告已被保存为 {1}{2} TXT 和 LOG 格式</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>响应路由器 IP 无法将数据包转发到目标 IP 地址，因此，需要排除响应路由器 IP 和目标之间的故障。</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>RFC 2544 测试不支持 MPLS 封装。</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>发送激光器已经打开&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>发送激光器关闭!&#xA;你要打开激光器吗?&#xA;点击 Yes 打开激光器或 No 关闭它</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>发送激光器关闭!&#xA;你要打开激光器吗?&#xA;点击 Yes 打开激光器或 No 关闭它</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>OAM CCM 打开时, 无法进行 VLAN 扫描</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>无法正确配置 VLAN 扫描</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       这个配置是只读的，不能修改 .</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>当使用异步模式时, 这将是远端的 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>该表显示进行 TCP 重新传输的 IP 来源地址。当检测到 TCP 重新传输时，则可能是由于下行数据包丢失(朝向目标端)。也可能表示出现半双工端口问题。</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>这个测试使用 Acterna 净荷</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>这个测试无效</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>本测试需要通信进行 VLAN 封装。确保所连网络能为该配置提供 IP 地址。</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>只需要花 {1} 秒.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>吞吐量 (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>吞吐量 ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>吞吐量和延迟 (RTD) 测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>吞吐量和数据包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>吞吐量帧丢失容限:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>吞吐量帧丢失容限</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>吞吐量、延迟 (RTD) 和数据包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>吞吐量通过门限:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>吞吐量通过门限</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>吞吐量缩放</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>吞吐量缩放系数</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>吞吐量测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>吞吐量测试持续时间为 {1} 秒</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>吞吐量测试: 中止</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>吞吐量测试: 失败</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>吞吐量测试: 通过</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>吞吐量通过门限 (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation>吞吐量通过门限: {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>吞吐量通过门限 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>吞吐量测试时长:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>吞吐量测试时长</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>吞吐量归零过程：</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>吞吐量归零过程</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>时间结束</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>时间结束</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>每 ID 时间:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>时间 (秒)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>时间&#xA;(秒)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>时间开始</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>时间开始</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>访问次数</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>要继续，请检查线缆连接，然后重新启动 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>为了确定最大吞吐量，选择标准匹配 tx 和 rx 帧数的 RFC 2544 法或使用测量的 L2 平均 % 效用的 Viavi 增强法。</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>如要节省延迟时间(RTD)，非对称模式应单向运行</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>以了解是否有偶发性或经常性帧丢失事件。</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>字节总数</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>所有&#xA;帧</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>所有数字</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>利用率 {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>利用率 (kbps): </translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>利用率 (Mbps): </translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>看报表 , 退出 {1} 以后在报表菜单中选择” 看报表” .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>以内</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>流量： {1} 经常不变</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>流量结果</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>流量仍从远端生成</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>发送激光器已经关闭</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Tx 帧</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>下行传输</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>上行传输</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>试验</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>试验 {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>试验 {1} 完成 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>试验 {2} 之中的第 {1} 个:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>试验持续时间 (秒)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>试验</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>尝试第二次</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>tshark 错误</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>发送缓冲器到缓冲器电头</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Tx 方向</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>发送激光器关闭</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Tx Mbps, 当前 L1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation> Tx </translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation>无法自动环回远端</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>无法连接到测试应用</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>无法连接到测试应用!&#xA;按 Yes 重试  No 放弃</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>无法获取 DHCP 地址。</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>本地环回打开，无法运行 RFC2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>无法运行测试</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Unable to run VLAN Scan test with Local Loopback enabled.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>不可获得</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>不可获得</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>设备标识</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>向上</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>(上或下)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>上游</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>上行方向</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>用户中止测试</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>用户取消的测试</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>用户优先级</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>用户请求未激活</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>用户选择&#xA;( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>用户选择      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>使用摘要状态屏幕查找错误事件。</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>应用中</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>正在使用的帧大小为</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>利用率和 TCP 重新传输</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>数值</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>蓝色的数值是从实际的测试来的</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>正在验证那条链路是否激活 ...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>验证您的远程 ip 地址后重试</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>验证您的远程 ip 地址后重试</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi增强式</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>测试的 VLAN ID 范围</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN Scan Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAN Scan Test Results</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>检测到 VTP/DTP/PAgP/UDLD 帧 !</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>正在等待自协商完成 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>等待目标 MAC&#xA;IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>正在等待 DHCP 参数 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>正在等待 2 层链路 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>等待连接</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>等待OWD启用、ToD同步和1PPS同步</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>正在等待成功的ARP...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>在最后时刻被检测到。</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>网站尺寸</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>我们有活动循环</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>有错误!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>当测试半双工链路时，请选择 RFC 2544 标准。</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>窗口尺寸/容量</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>推荐使用 RFC 2544 。</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>你要存储测试报告吗?&#xA;&#xA;按 Yes 或 No</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>您也可以使用图形结果帧丢失速率当前图示</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>你不能从 {1} 运行这个脚本</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>您可能需要等到它停止重新连接</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>把吞吐量速率减少到零位</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>把缓冲器信用量减少到零位</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>归零过程</translation>
        </message>
    </context>
</TS>
