<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>파일</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>제거 가능한 저장장치</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>탐지된 장치가 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>포맷</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>이 USB 장치를 포맷함으로 기존의 모든 데이터가 지워질 것입니다. 이것은 모든 파일과 파티션을 포함합니다.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>꺼내기</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>검색…</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>블루투스</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NONE</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NO</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>블루투스 페어 요청</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>페어링을 위한 PIN 입력</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>페어</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>페어링 요청</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>페어링 요청 발신자:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>장치와 페어링하기 위해 아래에 보이는 코드가 그 장치 상의 코드와 일치하는지 확인하십시오.</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>페어링이 시작됨</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>페어링 요청 수신자:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>스캐닝 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>스캐닝 정지</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>블루투스 활성</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>다른 장치가 이 장치와 페어링할 수 있도록 허용합니다.</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>장치 이름</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>모바일 앱 활성화</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>네트워크</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>IP 모드</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>정적</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>게이트웨이</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>DNS 서버</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>IPv6 모드</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>비활성화됨</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>수동</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>자동</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6 주소</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>서브넷 프리픽스 길이</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>DNS 서버</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>링크 - 로컬 주소</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>스테이트리스 주소</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>스테이트풀 주소</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>로딩되지 않은 모듈</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>존재함</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>존재하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>시작 중</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>활성화 중</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>초기화 중</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>준비 됨</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>스캐닝 중…</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>비활성화 중</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>정지 중</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>연결 중</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>연결 됨</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>무선 어댑터 활성</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTED</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>연결되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>실패 !</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>WPA 기업 네트워크에 연결</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>네트워크 이름</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>외부 인증 방법</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>내부 인증 방법</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>사용자명</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>익명의 신원</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>비밀번호</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>증명서</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>기밀 키 비밀번호</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>WPA 개인 네트워크에 연결</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>암호</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>USB 무선 장치가 발견되지 않음.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>연결 정지</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>네트워크 잊기</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>네트워크에 연결할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>3G 서비스</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>모뎀 활성화 중</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>모뎀 활성화됨</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>모뎀 비활성화됨</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>발견된 모뎀 없음</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>에러 - 모뎀이 반응하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>에러 - 장치 컨트롤러가 시작하지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>에러 - 모뎀을 구성할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>경고 - 모뎀이 반응하지 않습니다 . 모뎀이 재설정됩니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>에러 - SIM 이 삽입되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>에러 - 네트워크 등록이 거부되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>연결</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>연결 해제</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>연결 중</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>연결 해제 중</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>연결되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>연결 실패</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>모뎀 설정</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>모뎀 활성화</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>발견되거나 활성화된 모뎀 장치가 없음</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>네트워크 APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>연결 상태</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>APN 을 확인하고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS 서버</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec DNS 서버</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>프록시 &amp; 보안</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>프록시</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>프록시 종류</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>프록시 서버</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>네트워크 보안</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>FTP, 텔넷 및 자동 StrataSync 해제</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>전원 관리</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>AC 전원이 연결되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>배터리 전원으로 실행 중</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>배터리가 탐지되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>전원 소비 때문에 충전할 수 없었습니다</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>배터리 충전 중</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>배터리가 완전히 충전되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>배터리를 충전하고 있지 않음</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>전원 문제로 배터리를 충전할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>알 수 없는 배터리 상태</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>배터리가 너무 뜨거워 충전이 되지 않을 수 있습니다</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>배터리가 너무 뜨겁습니다.&#xA;AC 전원을 연결하세요.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>배터리가 너무 뜨겁습니다.&#xA;AC 전원을 분리하지 마세요.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>배터리가 너무 뜨겁습니다.&#xA;AC 전원을 분리하면 강제로 종료될 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>배터리가 너무 차가워 충전이 되지 않을 수 있습니다</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>배터리 과열의 위험이 있습니다</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>배터리 온도가 정상입니다</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>충전</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>배터리가 연결된 동안 자동 OFF 활성</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>비활성 시간 (분)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>날짜와 시간</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>시간대</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>지역</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>아프리카</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>아메리카</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>남극</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>아시아</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>대서양</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>호주</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>유럽</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>인도양</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>태평양</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>국가</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>아프가니스탄</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>올란드 제도</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>알바니아</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>알제리</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>아메리칸 사모아</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>안도라</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>앙골라</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>앙귈라</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>앤티가 바부다</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>아르헨티나</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>아르메니아</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>아루바</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>오스트리아</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>아제르바이잔</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>바하마</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>바레인</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>방글라데시</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>바베이도스</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>벨라루스</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>벨기에</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>벨리즈</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>베닌</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>버뮤다</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>부탄</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>볼리비아</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>보스니아 헤르체고비나</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>보츠와나</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>부베 섬</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>브라질</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>영국령 인도양 식민지</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>브루나이</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>불가리아</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>부르키나파소</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>브룬디</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>캄보디아</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>카메룬</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>캐나다</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>카보베르데</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>케이맨 제도</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>중앙아프리카 공화국</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>차드</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>칠레</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>중국</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>크리스마스 섬</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>코코스 (킬링) 제도</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>콜롬비아</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>코모로스</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>콩고</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>콩고 민주 공화국</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>쿡 제도</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>코스타리카</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>코트디부아르</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>크로아티아</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>쿠바</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>키프로스</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>체코 공화국</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>덴마크</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>지부티</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>도미니카</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>도미니카 공화국</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>에콰도르</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>이집트</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>엘살바도르</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>적도 기니 </translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>에리트레아</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>에스토니아</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>에티오피아</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>포클랜드 제도 (말비나스)</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>페로 제도</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>피지</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>핀란드</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>프랑스</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>프랑스령 기아나</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>프랑스령 폴리네시아</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>프랑스령 남부 지역</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>가봉</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>감비아</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>그루지야</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>독일</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>가나</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>지브롤터</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>그리스</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>그린란드</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>그레나다</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>과들루프</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>괌</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>과테말라</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>건지 섬</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>기니</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>기니비사우</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>가이아나</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>아이티</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>허드 앤 맥도날드 제도</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>온두라스</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>홍콩</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>헝가리</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>아이슬랜드</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>인도</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>인도네시아</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>이란</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>이라크</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>아일랜드</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>맨 섬</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>이스라엘</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>이탈리아</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>자메이카</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>일본</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>저지</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>요르단</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>카자흐스탄</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>케냐</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>키리바시</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>북한</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>대한민국</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>쿠웨이트</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>키르기스탄</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>라오스 인민 민주 공화국</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>라트비아</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>레바논</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>레소토</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>라이베리아</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>리비아</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>리히텐슈타인</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>리투아니아</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>룩셈부르크</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>마카오</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>마케도니아, 구 유고슬라비아 공화국</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>마다가스카</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>말라위</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>말레이시아</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>몰디브</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>말리</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>몰타</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>마샬 제도</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>마르티니크</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>모리타니아</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>모리셔스</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>마요트</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>멕시코</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>미크로네시아 연방공화국</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>몰도바 공화국</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>모로코</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>몽골</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>몬테네그로</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>몬트세라트</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>모로코</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>모잠비크</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>미얀마</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>나미비아</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>나우루</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>네팔</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>네덜란드</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>네덜란드 안틸레스</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>뉴칼레도니아</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>뉴질랜드</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>니카라과</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>니제르</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>나이지리아</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>니우에</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>노퍽 섬</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>북마리아나 제도</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>노르웨이</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>오만</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>파키스탄</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>팔라우</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>팔레스타인 영토</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>파나마</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>파푸아뉴기니</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>파라과이</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>페루</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>필리핀</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>피트케언</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>폴란드</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>포르투갈</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>푸에르토리코</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>카타르</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>레위니옹</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>루마니아</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>러시아 연방</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>르완다</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>생 바르텔레미</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>세인트 헬레나, 어센션 트라스탄다쿠냐</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>세인트 키츠 네비스</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>세인트 루시아</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>세인트 마틴</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>생피에르 미클롱</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>세인트빈센트 그레나딘</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>사모아</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>산마리노</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>상투메프린시페</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>사우디아라비아</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>세네갈</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>세르비아</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>세이셸</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>시에라리온</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>싱가포르</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>슬로바키아</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>슬로베니아</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>솔로몬 제도</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>소말리아</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>남아프리카</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>사우스 조지아 및 사우스 샌드위치 군도</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>스페인</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>스리랑카</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>수단</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>수리남</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>스발바르 얀마옌</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>스와질란드</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>스웨덴</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>스위스</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>시리아</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>대만</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>타지키스탄</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>탄자니아</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>태국</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>동티모르</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>토고</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>토켈라우</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>통가</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>트리니다드토바고</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>튀니지</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>터키</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>투르크메니스탄</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>티크스케이커스 제도</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>투발루</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>우간다</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>우크라이나</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>아랍에미리트 </translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>영국</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>미국</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>미국령 군소 제도</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>우르과이</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>우즈베키스탄</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>비누아투</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>바티칸시티</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>베네수엘라</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>베트남</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>영국령 버진아일랜드</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>미국령 버진아일랜드</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>왈리스 퓌튀나</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>서사하라</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>예멘</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>잠비아</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>짐바브웨</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>지역</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>캐시</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>데이비스</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>뒤몽 뒤르빌</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>모슨</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>맥머도</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>팔머</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>로데라</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>남극</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>쇼와</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>보스톡</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>오스트레일리아 수도 특별 지역</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>북쪽</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>뉴사우스웨일즈</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>퀸즐랜드 </translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>남쪽</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>태즈메이니아</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>빅토리아</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>서쪽</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>브라질</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>브라질 -1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>브라질  + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>알래스카</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>애리조나</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>대서양</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>중부</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>동부</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>하와이</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>산지</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>뉴펀들랜드</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>태평양</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>서스캐처원</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>이스터 섬</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>킨샤사</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>루붐바시</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>갈라파고스</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>갬비어</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>마퀘사스</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>타히티</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>서부</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>덴마크샤븐</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>동쪽</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>피닉스 제도</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>라인 제도</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>길버트 제도</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>북서</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>코스레</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>트루크</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>아조레스</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>마데이라</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>이르쿠츠크</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>칼리닌그라드</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>크라스노야르스크</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>마가단</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>모스크바</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>옴스크</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>블라디보스톡</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>야쿠츠크</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>예카테린부르크</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>카나리아 제도</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>스발바르</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>얀 마옌</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>존스톤</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>미드웨이</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>웨이크</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>자동적으로 일광 절약 시간에 맞게 조정됩니다</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>현재 날짜 &amp; 시간</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24시간</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12시간</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>NTP 로 클록 설정</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>거짓</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>24시간 사용</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP 서버</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP 서버</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>Ntp 서버 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>Ntp 서버 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (영어)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (독일어)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (스페인어)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (프랑스어)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (간체)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (일본어)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (러시아어)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (포르투갈어)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (이탈리아어)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk ( 터키어 )</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>언어:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>포맷팅 기준 변경:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>선택한 포맷팅을 위한 샘플:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>사용자 임의 설정</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>디스플레이</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>밝기</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>스크린세이버</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>오토 스크린세이버 활성</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>메시지</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>스크린세이버 비밀번호</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>터치스크린 보정…</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>원격</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>VNC 접속 활성</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>VNC 접속 또는 비밀번호 보호 토글링은 기존의 연결을 차단할 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>원격 접속 비밀번호</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>이 비밀번호는 vnc, ftp, ssh 등 모든 원격 접속에 사용됩니다.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>VNC를 시작하는데 에러가 발생했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>VNC 접속을 위해 비밀번호 필요</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>어디서나 스마트 접속</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>접속 모드</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>LAN IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Wi-Fi IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>VNC 연결의 수</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>업그레이드</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>AC 전원이 연결되어 있지 않기 때문에 업그레이드 할 수 없습니다. AC 전원을 연결하고 다시 시작하세요.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>USB 플래시 장치를 탐지할 수 없습니다. USB 플래시 장치가 안전하게 삽입되었는지 확인하고 다시 시도하세요.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>업그레이드 할 수 없습니다. 업그레이드 서버를 지정하고 다시 시도하세요.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>네트워크 연결을 사용할 수 없습니다. 네트워크 연결을 확인하고 다시 시도하세요.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>업그레이드 서버를 연결할 수 없습니다. 서버 주소를 확인하고 다시 시도하세요.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>사용할 수 있는 업그레이드를 찾는 중에 에러가 발생했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>업그레이드를 실행할 수 없습니다. 선택한 업그레이드에 빠지거나 손상된 파일이 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>업그레이드를 실행할 수 없습니다. 선택한 업그레이드에 손상된 파일이 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>USB 업그레이드 데이터를 생성하는데 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>업그레이드가 발견되지 않았습니다 . 설정을 확인하고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>업그레이드 시도가 실패했으므로, 다시 시도하세요. 문제가 계속되면, 자신의 영업사원과 연락하세요.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>TestSet Lock</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>테스트 세트를 잠그면 불법적인 접근이 방지됩니다. 화면을 보호하기 위해 스크린세이버가 실행됩니다.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>테스트 세트 잠금</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>필요한 비밀번호가 설정되지 않았습니다. 사용자는 비밀번호를 입력하지 않고 테스트 세트를 잠금 해제 할 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>비밀번호 설정</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>이러한 설정은 스크린세이버와 공유됩니다.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>비밀번호 요구</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>오디오</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>스피커 볼륨</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>음소거</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>마이크 볼륨</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>위성 검색 중</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>위도</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>경도</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>고도</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>타임스탬프</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>고정 위성 수</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>평균 SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>개별 위성 정보</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>시스템 정보</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>서버로의 연결 설정 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>연결이 설정되었습니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>파일 다운로드 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>파일 업로딩 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>업그레이드 정보 다운로드 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>로그 파일 업로딩 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>서버와 동기화 성공 .</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>빈 대기 중 . 물품 목록 추가 대기 중입니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>서버와의 동기화에 실패했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>동기화 중 연결이 손실됨 . 네트워크 설정을 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>서버와의 동기화에 실패했습니다 . 서버가 사용 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>서버와의 동기화에 실패했습니다 . 시스템 에러가 발견되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>연결을 설정하는데 실패하였습니다 . 네트워크 설정이나 계정 ID 를 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>동기화 취소됨 .</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>설정 데이터 완료 .</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>보고서 완료 .</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>계정 ID 입력 .</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Sync 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Sync 정지</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>서버 주소</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>계정 ID</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>옵션</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>디폴트로 리셋</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>비디오 플레이어</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>웹 브라우저</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>디버그</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>시리얼 장치</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>게티가 시리얼 장치를 제어하도록 합니다</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>/root/debug.txt 의 컨텐츠</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>스크리닝 테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>직업 관리자</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>직업 정보</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>직업 번호</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>직업 정보가 자동적으로 테스트 보고서에 추가됩니다.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>히스토리</translation>
        </message>
    </context>
</TS>
