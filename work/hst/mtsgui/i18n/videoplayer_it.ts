<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Lettore video Viavi</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Nome file video</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Tempo:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>Stato video</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Ridimensiona e ritaglia</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Scala</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Apri &amp;file...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Proporzioni</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Scala</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Modalità di ridimensionamento</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>Adatta alla visualizzazione</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Ridimensiona e ritaglia</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Apri file...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Supporto multimediale (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aperta</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>Nessun supporto aperto</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Caricamento in corso...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Bufferizzazione...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Riproduzione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>In pausa</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Errore...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Inattivo: arresto del supporto in corso...</translation>
        </message>
    </context>
</TS>
