<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo del report</translation>
        </message>
        <message utf8="true">
            <source>Device Under Test</source>
            <translation>Dispositivo sotto esame</translation>
        </message>
        <message utf8="true">
            <source>Owner</source>
            <translation>Titolare</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>Software Revision</source>
            <translation>Revisione del software</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>Comment</source>
            <translation>Commento</translation>
        </message>
        <message utf8="true">
            <source>Tester Comments</source>
            <translation>Commenti tester</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Risultati del test</translation>
        </message>
        <message utf8="true">
            <source>Tester</source>
            <translation>Tester</translation>
        </message>
        <message utf8="true">
            <source>Test Instrument</source>
            <translation>Strumento di test</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modello</translation>
        </message>
        <message utf8="true">
            <source>Viavi -BASE_MODEL- -MODULE_NAME-</source>
            <translation>Viavi -BASE_MODEL- -MODULE_NAME-</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Revisione SW</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SOFTWARE_VERSION-</source>
            <translation>-MODULE_SOFTWARE_VERSION-</translation>
        </message>
        <message utf8="true">
            <source>BERT Serial Number</source>
            <translation>Numero di serie BERT</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SERIAL-</source>
            <translation>-MODULE_SERIAL-</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom</source>
            <translation>Datacom HS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT</source>
            <translation>BERT Datacom HS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Terminate</source>
            <translation>Terminare</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT Term</source>
            <translation>BERT Datacom HS Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Diphase</source>
            <translation>Diphase</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT</source>
            <translation>BERT Diphase</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT Term</source>
            <translation>Term BERT Diphase </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1/DS3</source>
            <translation>DS1/DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1</source>
            <translation>DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT</source>
            <translation>DS1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Term</source>
            <translation>Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Thru</source>
            <translation>Thru</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Thru</source>
            <translation>Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Single Monitor</source>
            <translation>Monitor Singolo</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Single Mon</source>
            <translation>Mon singolo BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Monitor</source>
            <translation>Monitor doppio</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Dual Mon</source>
            <translation>Mon doppio BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander</source>
            <translation>Wander DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander Single Mon</source>
            <translation>Mon Wander singolo DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling</source>
            <translation>Segnalazione DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Term</source>
            <translation>Term segnalazione DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Dual Mon</source>
            <translation>Mon doppio segnalazione DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI</source>
            <translation>DS1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Term</source>
            <translation>Term PRI ISDN DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Dual Mon</source>
            <translation>Mon doppio PRI ISDN DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF</source>
            <translation>DS1 VF</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Term</source>
            <translation>Term VF DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Dual Mon</source>
            <translation>Mon doppio VF DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC</source>
            <translation>DS1 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC Dual Mon</source>
            <translation>Mon doppio HDLC DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP</source>
            <translation>DS1 PPP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP Ping Term</source>
            <translation>Term PPP Ping DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Jitter</source>
            <translation>Jitter DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Jitter Term</source>
            <translation>Term Jitter BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Rx Jitter Term</source>
            <translation>DS1 BERT Term Jitter Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Wander Term</source>
            <translation>Term Wander BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3</source>
            <translation>DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT</source>
            <translation>DS3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Term</source>
            <translation>Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Thru</source>
            <translation>Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Single Mon</source>
            <translation>Mon singolo BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Dual Mon</source>
            <translation>Mon doppio BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT</source>
            <translation>E1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Term</source>
            <translation>DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Thru</source>
            <translation>DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Dual Mon</source>
            <translation>DS3 : Mon doppio BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Term</source>
            <translation>DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Thru</source>
            <translation>DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Dual Mon</source>
            <translation>DS3 : Mon doppio BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC</source>
            <translation>DS3 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC Dual Mon</source>
            <translation>Mon doppio HDLC DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Jitter</source>
            <translation>Jitter DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Jitter Term</source>
            <translation>Term Jitter BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Rx Jitter Term</source>
            <translation>DS3 BERT Term Jitter Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Jitter Term</source>
            <translation>DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Rx Jitter Term</source>
            <translation>DS3 : Term Jitter BERT E1 Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Jitter Term</source>
            <translation>DS3 : Term Jitter BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Rx Jitter Term</source>
            <translation>DS3 : Term Jitter BERT DS1 Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Wander</source>
            <translation>Wander DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Wander Term</source>
            <translation>Term Wander BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Wander Term</source>
            <translation>DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Wander Term</source>
            <translation>DS3 : Term Wanter BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1/E3/E4</source>
            <translation>E1/E3/E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1</source>
            <translation>E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Term</source>
            <translation>Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Thru</source>
            <translation>Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Single Mon</source>
            <translation>Mon singolo BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Dual Mon</source>
            <translation>Mon doppio BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander</source>
            <translation>Wander E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander Single Mon</source>
            <translation>Mon Wander singolo E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI</source>
            <translation>E1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI Term</source>
            <translation>E1 ISDN PRI Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Jitter</source>
            <translation>Jitter E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Jitter Term</source>
            <translation>Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Wander Term</source>
            <translation>Term Wander BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3</source>
            <translation>E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT</source>
            <translation>E3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Term</source>
            <translation>Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Thru</source>
            <translation>Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor</source>
            <translation>Monitoraggio</translation>
        </message>
        <message utf8="true">
            <source>E3 BERT Mon</source>
            <translation>Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Term</source>
            <translation>E3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Thru</source>
            <translation>E3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Mon</source>
            <translation>E3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Jitter</source>
            <translation>Jitter E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Jitter Term</source>
            <translation>Term Jitter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Jitter Term</source>
            <translation>E3 : Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Wander</source>
            <translation>Wander E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Wander Term</source>
            <translation>Term Wander BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Wander Term</source>
            <translation>E3 : Term Wanter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4</source>
            <translation>E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT</source>
            <translation>E4 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Term</source>
            <translation>Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Thru</source>
            <translation>Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Mon</source>
            <translation>Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Term</source>
            <translation>E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Thru</source>
            <translation>E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Mon</source>
            <translation>E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Term</source>
            <translation>E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Thru</source>
            <translation>E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Mon</source>
            <translation>E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Jitter</source>
            <translation>Jitter E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Jitter Term</source>
            <translation>Term Jitter BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Jitter Term</source>
            <translation>E4 : Term Jitter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Jitter Term</source>
            <translation>E4 : Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Wander</source>
            <translation>Wander E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Wander Term</source>
            <translation>Term Wander BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Wander Term</source>
            <translation>E4 : Term Wanter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Wander Term</source>
            <translation>E4 : Term Wanter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SONET</source>
            <translation>SONET</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>J-Scan</source>
            <translation>J-Scan</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 J-Scan</source>
            <translation>J-Scan STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Bulk BERT</source>
            <translation>BERT Bulk</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Drop+Insert</source>
            <translation>Scarto+Inserim</translation>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Drop+Insert</source>
            <translation>Scarto+Inserim BERT Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Dual Mon</source>
            <translation>Mon doppio BERT Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Term</source>
            <translation>Term BERT DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Thru</source>
            <translation>Thru BERT DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Mon</source>
            <translation>Mon BERT DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Term</source>
            <translation>DS3 STS-1 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Thru</source>
            <translation>DS3 STS-1 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Mon</source>
            <translation>DS3 STS-1 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Term</source>
            <translation>DS3 STS-1 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Thru</source>
            <translation>DS3 STS-1 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Mon</source>
            <translation>DS3 STS-1 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Term</source>
            <translation>Term BERT Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Single Mon</source>
            <translation>Mon singolo BERT Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Dual Mon</source>
            <translation>Mon doppio BERT Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Term</source>
            <translation>STS-1 VT-1.5 Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Thru</source>
            <translation>STS-1 VT-1.5 Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Single Mon</source>
            <translation>Mon singolo BERT DS1 VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 Mon doppio BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Jitter</source>
            <translation>Jitter STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Jitter Term</source>
            <translation>Term Jitter DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Jitter Term</source>
            <translation>STS-1 DS3 Term Jitter DS1 :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Jitter Term</source>
            <translation>STS-1 DS3 Term Jitter E1 :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Jitter Term</source>
            <translation>STS-1 VT-1.5 Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Wander</source>
            <translation>Wander STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Wander Term</source>
            <translation>Term Wander DS3 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Wander Term</source>
            <translation>STS-1 DS3 Term Wanter DS1 :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Wander Term</source>
            <translation>STS-1 DS3 Term Wanter E1 :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Wander Term</source>
            <translation>Term Wander Bulk VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Wander Term</source>
            <translation>STS-1 VT-1.5 Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3</source>
            <translation>OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 J-Scan</source>
            <translation>J-Scan OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c Bulk BERT</source>
            <translation>BERT Bulk STS-3c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Term</source>
            <translation>OC-3 STS-1 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Thru</source>
            <translation>OC-3 STS-1 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Mon</source>
            <translation>OC-3 STS-1 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Term</source>
            <translation>DS3 STS-1 OC-3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>DS3 STS-1 OC-3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>DS3 STS-1 OC-3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Term</source>
            <translation>DS3 STS-1 OC-3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Thru</source>
            <translation>DS3 STS-1 OC-3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Mon</source>
            <translation>DS3 STS-1 OC-3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Term</source>
            <translation>Term BERT Bulk VT-1.5 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk VT-1.5 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk VT-1.5 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Term</source>
            <translation>OC-3 VT-1.5 Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-3 VT-1.5 Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-3 VT-1.5 Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c-Xv</source>
            <translation>STS-3c-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Term</source>
            <translation>Term BERT STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Mon</source>
            <translation>Mon BERT STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>GFP Ethernet</source>
            <translation>Ethernet GFP:</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Traffic</source>
            <translation>Traffico Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traffic</source>
            <translation>Traffico Livello 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Ping</source>
            <translation>Ping Livello 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-3c-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1-Xv</source>
            <translation>STS-1-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Term</source>
            <translation>Term BERT STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Mon</source>
            <translation>Mon BERT STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-1-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5-Xv</source>
            <translation>VT-1.5-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Term</source>
            <translation>Term BERT VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Mon</source>
            <translation>Mon BERT VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP VT-1.5-Xv OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Jitter</source>
            <translation>Jitter OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Jitter Term</source>
            <translation>OC-3 STS-1 Term Jitter DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>DS3 STS-1 OC-3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Jitter Term</source>
            <translation>DS3 STS-1 OC-3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk VT-1.5 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-3 VT-1.5 Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Wander</source>
            <translation>Wander OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-3c OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-1 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Wander Term</source>
            <translation>OC-3 STS-1 Term Wander DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Wander Term</source>
            <translation>DS3 STS-1 OC-3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Wander Term</source>
            <translation>DS3 STS-1 OC-3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Wander Term</source>
            <translation>Term Wander Bulk VT-1.5 OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Wander Term</source>
            <translation>OC-3 VT-1.5 Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12</source>
            <translation>OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 J-Scan</source>
            <translation>J-Scan OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-12c Bulk BERT</source>
            <translation>BERT Bulk STS-12c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Term</source>
            <translation>OC-12 STS-1 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Thru</source>
            <translation>OC-12 STS-1 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Mon</source>
            <translation>OC-12 STS-1 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Term</source>
            <translation>DS3 STS-1 OC-12 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>DS3 STS-1 OC-12 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>DS3 STS-1 OC-12 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Term</source>
            <translation>DS3 STS-1 OC-12 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Thru</source>
            <translation>DS3 STS-1 OC-12 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Mon</source>
            <translation>DS3 STS-1 OC-12 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Term</source>
            <translation>Term BERT Bulk VT-1.5 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk VT-1.5 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk VT-1.5 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Term</source>
            <translation>OC-12 VT-1.5 Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-12 VT-1.5 Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-12 VT-1.5 Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Term</source>
            <translation>Term BERT STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Mon</source>
            <translation>Mon BERT STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-3c-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Term</source>
            <translation>Term BERT STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Mon</source>
            <translation>Mon BERT STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-1-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Term</source>
            <translation>Term BERT VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Mon</source>
            <translation>Mon BERT VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP VT-1.5-Xv OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Jitter</source>
            <translation>Jitter OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Jitter Term</source>
            <translation>OC-12 STS-1 Term Jitter DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>DS3 STS-1 OC-12 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Jitter Term</source>
            <translation>DS3 STS-1 OC-12 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk VT-1.5 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-12 VT-1.5 Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Wander</source>
            <translation>Wander OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-12c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-3c OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-1 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Wander Term</source>
            <translation>OC-12 STS-1 Term Wander DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Wander Term</source>
            <translation>DS3 STS-1 OC-12 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Wander Term</source>
            <translation>DS3 STS-1 OC-12 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Wander Term</source>
            <translation>Term Wander Bulk VT-1.5 OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Wander Term</source>
            <translation>OC-12 VT-1.5 Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48</source>
            <translation>OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 J-Scan</source>
            <translation>J-Scan OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48c Bulk BERT</source>
            <translation>BERT Bulk STS-48c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Term</source>
            <translation>OC-48 STS-1 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Thru</source>
            <translation>OC-48 STS-1 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Mon</source>
            <translation>OC-48 STS-1 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Term</source>
            <translation>DS3 STS-1 OC-48 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>DS3 STS-1 OC-48 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>DS3 STS-1 OC-48 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Term</source>
            <translation>DS3 STS-1 OC-48 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Thru</source>
            <translation>DS3 STS-1 OC-48 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Mon</source>
            <translation>DS3 STS-1 OC-48 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Term</source>
            <translation>Term BERT Bulk VT-1.5 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk VT-1.5 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk VT-1.5 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Term</source>
            <translation>OC-48 VT-1.5 Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-48 VT-1.5 Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-48 VT-1.5 Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Term</source>
            <translation>Term BERT STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Mon</source>
            <translation>Mon BERT STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-3c-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Term</source>
            <translation>Term BERT STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Mon</source>
            <translation>Mon BERT STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-1-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Term</source>
            <translation>Term BERT VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Mon</source>
            <translation>Mon BERT VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP VT-1.5-Xv OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Jitter</source>
            <translation>Jitter OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Jitter Term</source>
            <translation>OC-48 STS-1 Term Jitter DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>DS3 STS-1 OC-48 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Jitter Term</source>
            <translation>DS3 STS-1 OC-48 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk VT-1.5 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-48 VT-1.5 Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Wander</source>
            <translation>Wander OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-48c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-12c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-3c OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Wander Term</source>
            <translation>Term Wander Bulk STS-1 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Wander Term</source>
            <translation>OC-48 STS-1 Term Wander DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Wander Term</source>
            <translation>DS3 STS-1 OC-48 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Wander Term</source>
            <translation>DS3 STS-1 OC-48 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Wander Term</source>
            <translation>Term Wander Bulk VT-1.5 OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Wander Term</source>
            <translation>OC-48 VT-1.5 Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192</source>
            <translation>OC-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 J-Scan</source>
            <translation>J-Scan OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192c Bulk BERT</source>
            <translation>BERT Bulk STS-192c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-192c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-192c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-192c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-192c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-48c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-48c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-48c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-48c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-12c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-12c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-12c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-12c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-3c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-3c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-3c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-3c OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STS-1 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Term</source>
            <translation>OC-192 STS-1 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Thru</source>
            <translation>OC-192 STS-1 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Mon</source>
            <translation>OC-192 STS-1 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STS-1 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Term</source>
            <translation>DS3 STS-1 OC-192 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>DS3 STS-1 OC-192 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>DS3 STS-1 OC-192 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Term</source>
            <translation>DS3 STS-1 OC-192 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Thru</source>
            <translation>DS3 STS-1 OC-192 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Mon</source>
            <translation>DS3 STS-1 OC-192 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Term</source>
            <translation>Term BERT Bulk VT-1.5 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk VT-1.5 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk VT-1.5 OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Term</source>
            <translation>OC-192 VT-1.5 Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-192 VT-1.5 Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-192 VT-1.5 Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Term</source>
            <translation>Term BERT STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Mon</source>
            <translation>Mon BERT STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-3c-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Term</source>
            <translation>Term BERT STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Mon</source>
            <translation>Mon BERT STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STS-1-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Term</source>
            <translation>Term BERT VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Mon</source>
            <translation>Mon BERT VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP VT-1.5-Xv OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768</source>
            <translation>OC-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Autotest delle reti ottiche</translation>
        </message>
        <message utf8="true">
            <source>OC-768 Optics Self-Test</source>
            <translation>Autotest delle reti ottiche OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL BERT</source>
            <translation>STL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Term</source>
            <translation>Term BERT Bulk STL OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STL OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768c Bulk BERT</source>
            <translation>BERT Bulk STS-768c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-768c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-768c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-768c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-192c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-192c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-192c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-48c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-48c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-48c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-12c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-12c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-12c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-3c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-3c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-3c OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT</source>
            <translation>BERT Bulk STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk STS-1 OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STS-1 OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STS-1 OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SDH</source>
            <translation>SDH</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e</source>
            <translation>STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e J-Scan</source>
            <translation>J-Scan STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-4</source>
            <translation>AU-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>VC-4</source>
            <translation>VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Term</source>
            <translation>AU-4 STM-1e VC-4 Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Thru</source>
            <translation>AU-4 STM-1e VC-4 Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Mon</source>
            <translation>AU-4 STM-1e VC-4 Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E4 STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1e AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1e AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1e AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Term</source>
            <translation>AU-4 STM-1e VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Thru</source>
            <translation>AU-4 STM-1e VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Mon</source>
            <translation>AU-4 STM-1e VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Term</source>
            <translation>AU-4 STM-1e VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Thru</source>
            <translation>AU-4 STM-1e VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Mon</source>
            <translation>AU-4 STM-1e VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1e AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1e AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1e AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Term</source>
            <translation>AU-4 STM-1e VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Thru</source>
            <translation>AU-4 STM-1e VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Mon</source>
            <translation>AU-4 STM-1e VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Term</source>
            <translation>AU-3 STM-1e VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Thru</source>
            <translation>AU-3 STM-1e VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Mon</source>
            <translation>AU-3 STM-1e VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Term</source>
            <translation>AU-3 STM-1e VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Thru</source>
            <translation>AU-3 STM-1e VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Mon</source>
            <translation>AU-3 STM-1e VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E3 STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1e AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1e AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1e AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Term</source>
            <translation>AU-3 STM-1e VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Thru</source>
            <translation>AU-3 STM-1e VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Mon</source>
            <translation>AU-3 STM-1e VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Jitter</source>
            <translation>Jitter STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Jitter Term</source>
            <translation>Term Jitter BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Jitter Term</source>
            <translation>VC-4 AU-4 STM-1e E4 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term Jitter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Jitter Term</source>
            <translation>Term Jitter BERT Bulk STM-1e AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Jitter Term</source>
            <translation>VC-3 AU-4 STM-1e DS3 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term Jitter BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Jitter Term</source>
            <translation>VC-3 AU-4 STM-1e E3 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Term Jitter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Jitter Term</source>
            <translation>Term Jitter BERT Bulk STM-1e AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Jitter Term</source>
            <translation>VC-12 AU-4 STM-1e E1 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Jitter Term</source>
            <translation>Term Jitter BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Jitter Term</source>
            <translation>VC-3 AU-3 STM-1e DS3 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term Jitter BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term Jitter BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Jitter Term</source>
            <translation>VC-3 AU-3 STM-1e E3 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Term Jitter BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Jitter Term</source>
            <translation>Term Jitter BERT Bulk STM-1e AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Jitter Term</source>
            <translation>VC-12 AU-3 STM-1e E1 Term Jitter BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Wander</source>
            <translation>Wander STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Wander Term</source>
            <translation>Term Wander BERT Bulk STM-1e AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Wander Term</source>
            <translation>VC-4 AU-4 STM-1e E4 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term Wander BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Term Wander BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Wander Term</source>
            <translation>Term Wander BERT Bulk STM-1e AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Wander Term</source>
            <translation>VC-3 AU-4 STM-1e DS3 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term Wander BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Term Wander BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Wander Term</source>
            <translation>VC-3 AU-4 STM-1e E3 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Term Wander BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Wander Term</source>
            <translation>Term Wander BERT Bulk STM-1e AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Wander Term</source>
            <translation>VC-12 AU-4 STM-1e E1 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Wander Term</source>
            <translation>Term Wander BERT Bulk STM-1e AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Wander Term</source>
            <translation>VC-3 AU-3 STM-1e DS3 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term Wander BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Term Wander BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Wander Term</source>
            <translation>VC-3 AU-3 STM-1e E3 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Term Wander BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Wander Term</source>
            <translation>Term Wander BERT Bulk STM-1e AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Wander Term</source>
            <translation>VC-12 AU-3 STM-1e E1 Term Wander BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1</source>
            <translation>STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 J-Scan</source>
            <translation>J-Scan STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Term</source>
            <translation>AU-4 STM-1 VC-4 Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Thru</source>
            <translation>AU-4 STM-1 VC-4 Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Mon</source>
            <translation>AU-4 STM-1 VC-4 Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E4 STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Term</source>
            <translation>AU-4 STM-1 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>AU-4 STM-1 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>AU-4 STM-1 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Term</source>
            <translation>AU-4 STM-1 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Thru</source>
            <translation>AU-4 STM-1 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Mon</source>
            <translation>AU-4 STM-1 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Term</source>
            <translation>AU-4 STM-1 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Thru</source>
            <translation>AU-4 STM-1 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Mon</source>
            <translation>AU-4 STM-1 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-Xv</source>
            <translation>VC-4-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-1 AU-4 Term BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Mon BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-4-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-4-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-4-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-4-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-1 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-1 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3-Xv</source>
            <translation>VC-3-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-4 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-1 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-1 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12-Xv</source>
            <translation>VC-12-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-4 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-4 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-1 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-1 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Term</source>
            <translation>AU-3 STM-1 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>AU-3 STM-1 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>AU-3 STM-1 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Term</source>
            <translation>AU-3 STM-1 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Thru</source>
            <translation>AU-3 STM-1 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Mon</source>
            <translation>AU-3 STM-1 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E3 STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-1 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-1 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-1 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Term</source>
            <translation>AU-3 STM-1 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Thru</source>
            <translation>AU-3 STM-1 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Mon</source>
            <translation>AU-3 STM-1 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-3 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-3 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-1 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-1 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-3 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-3 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-3 STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-1 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-1 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Jitter</source>
            <translation>Jitter STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-1 AU-4 Term Jitter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-1 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-4 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-4 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-1 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-4 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-3 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-3 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-1 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-3 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Wander</source>
            <translation>Wander STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-1 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-1 AU-4 Term Wanter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-1 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-4 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-4 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-1 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-4 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-1 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-3 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-3 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-1 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-3 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4</source>
            <translation>STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 J-Scan</source>
            <translation>J-Scan STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-4c Bulk BERT</source>
            <translation>BERT Bulk VC-4-4c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Term</source>
            <translation>AU-4 STM-4 VC-4 Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Thru</source>
            <translation>AU-4 STM-4 VC-4 Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Mon</source>
            <translation>AU-4 STM-4 VC-4 Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E4 STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Term</source>
            <translation>AU-4 STM-4 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>AU-4 STM-4 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>AU-4 STM-4 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Term</source>
            <translation>AU-4 STM-4 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Thru</source>
            <translation>AU-4 STM-4 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Mon</source>
            <translation>AU-4 STM-4 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Term</source>
            <translation>AU-4 STM-4 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Thru</source>
            <translation>AU-4 STM-4 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Mon</source>
            <translation>AU-4 STM-4 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-4 AU-4 Term BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Mon BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-4-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-4-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-4-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-4-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-4 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-4 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-4 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-4 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-4 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-4 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-4 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-4 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-4 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Term</source>
            <translation>AU-3 STM-4 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>AU-3 STM-4 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>AU-3 STM-4 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Term</source>
            <translation>AU-3 STM-4 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Thru</source>
            <translation>AU-3 STM-4 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Mon</source>
            <translation>AU-3 STM-4 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E3 STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-4 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-4 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-4 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Term</source>
            <translation>AU-3 STM-4 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Thru</source>
            <translation>AU-3 STM-4 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Mon</source>
            <translation>AU-3 STM-4 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-3 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-3 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-4 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-4 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-3 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-3 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-3 STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-4 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-4 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Jitter</source>
            <translation>Jitter STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-4 AU-4 Term Jitter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-4 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-4 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-4 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-3 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-3 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-4 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-3 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Wander</source>
            <translation>Wander STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-4 AU-4 Term Wanter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-4 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-4 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-4 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-3 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-3 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-4 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-3 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16</source>
            <translation>STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 J-Scan</source>
            <translation>J-Scan STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-16c Bulk BERT</source>
            <translation>BERT Bulk VC-4-16c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Term</source>
            <translation>AU-4 STM-16 VC-4 Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Thru</source>
            <translation>AU-4 STM-16 VC-4 Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Mon</source>
            <translation>AU-4 STM-16 VC-4 Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E4 STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Term</source>
            <translation>AU-4 STM-16 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>AU-4 STM-16 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>AU-4 STM-16 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Term</source>
            <translation>AU-4 STM-16 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Thru</source>
            <translation>AU-4 STM-16 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Mon</source>
            <translation>AU-4 STM-16 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Term</source>
            <translation>AU-4 STM-16 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Thru</source>
            <translation>AU-4 STM-16 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Mon</source>
            <translation>AU-4 STM-16 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-16 AU-4 Term BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Mon BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-4-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-4-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-4-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-4-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-16 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-16 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-4 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-16 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-16 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-4 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-4 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-16 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-16 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Term</source>
            <translation>AU-3 STM-16 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>AU-3 STM-16 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>AU-3 STM-16 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Term</source>
            <translation>AU-3 STM-16 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Thru</source>
            <translation>AU-3 STM-16 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Mon</source>
            <translation>AU-3 STM-16 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E3 STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-16 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-16 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-16 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Term</source>
            <translation>AU-3 STM-16 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Thru</source>
            <translation>AU-3 STM-16 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Mon</source>
            <translation>AU-3 STM-16 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-3 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-3 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-16 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-16 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-3 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-3 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-3 STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-16 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-16 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Jitter</source>
            <translation>Jitter STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Jitter Term</source>
            <translation>STM-16 AU-4 Term Jitter VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-16 AU-4 Term Jitter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-4 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-4 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-4 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-3 Term Jitter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term Jitter E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term Jitter DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-3 Term Jitter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Term Jitter E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Term Jitter Bulk STM-16 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-3 Term Jitter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Wander</source>
            <translation>Wander STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Wander Term</source>
            <translation>STM-16 AU-4 Term Wander VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-16 AU-4 Term Wanter E4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-4 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-4 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-4 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-3 Term Wanter DS3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term Wander E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Term Wander DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-3 Term Wanter E3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Term Wander E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Term Wander Bulk STM-16 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-3 Term Wanter E1 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64</source>
            <translation>STM-64</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 J-Scan</source>
            <translation>J-Scan STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-64c Bulk BERT</source>
            <translation>BERT Bulk VC-4-64c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Term</source>
            <translation>AU-4 STM-64 VC-4 Term BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Thru</source>
            <translation>AU-4 STM-64 VC-4 Thru BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Mon</source>
            <translation>AU-4 STM-64 VC-4 Mon BERT E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E4 STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Term</source>
            <translation>AU-4 STM-64 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>AU-4 STM-64 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>AU-4 STM-64 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Term</source>
            <translation>AU-4 STM-64 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Thru</source>
            <translation>AU-4 STM-64 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Mon</source>
            <translation>AU-4 STM-64 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-4 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Term</source>
            <translation>AU-4 STM-64 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Thru</source>
            <translation>AU-4 STM-64 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Mon</source>
            <translation>AU-4 STM-64 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-64 AU-4 Term BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Mon BERT VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-4-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-4-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-4-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-4-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-64 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-64 AU-4 VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-4 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-64 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-64 AU-4 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-4 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-4 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-64 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-64 AU-4 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT Bulk STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Term</source>
            <translation>AU-3 STM-64 VC-3 Term BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>AU-3 STM-64 VC-3 Thru BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>AU-3 STM-64 VC-3 Mon BERT DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT DS3 STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Term BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Thru BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Mon BERT DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Term</source>
            <translation>AU-3 STM-64 VC-3 Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Thru</source>
            <translation>AU-3 STM-64 VC-3 Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Mon</source>
            <translation>AU-3 STM-64 VC-3 Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>Scarto + Inserim BERT E3 STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E1 : Term BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E1 : Thru BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E1 : Mon BERT E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-64 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-64 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-64 AU-3 VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Term</source>
            <translation>AU-3 STM-64 VC-12 Term BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Thru</source>
            <translation>AU-3 STM-64 VC-12 Thru BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Mon</source>
            <translation>AU-3 STM-64 VC-12 Mon BERT E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-3 Term BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-3 Mon BERT VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-3-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-3-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-3-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-3-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-64 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-64 AU-3 VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-3 Term BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-3 Mon BERT VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GFP VC-12-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Mon Traffico Livello 2 GFP VC-12-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GFP VC-12-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Mon Traffico Livello 3 GFP VC-12-Xv AU-3 STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Ping Livello 3 GFP STM-64 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Traceroute Livello 3 GFP STM-64 AU-3 VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256</source>
            <translation>STM-256</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 Optics Self-Test</source>
            <translation>Autotest delle reti ottiche STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL Bert</source>
            <translation>STL Bert</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Term</source>
            <translation>Term BERT STM-256 STL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Mon</source>
            <translation>Mon BERT STL STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-256c Bulk BERT</source>
            <translation>BERT Bulk VC-4-256c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-4 VC-4-256c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-4 VC-4-256c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-4 VC-4-256c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4 Bulk BERT</source>
            <translation>BERT Bulk VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3 Bulk BERT</source>
            <translation>BERT Bulk VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk STM-256 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Thru BERT Bulk STM-256 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Mon BERT Bulk STM-256 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1G</source>
            <translation>10/100/1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth TrueSAM</source>
            <translation>TrueSAM 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>L2 Traffic</source>
            <translation>Traffico L2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 10/100/1000 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic</source>
            <translation>Traffico L3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 10/100/1000 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv4</source>
            <translation>IPv4 traffico L3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv6</source>
            <translation>IPv6 traffico L3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv4</source>
            <translation>IPv4 traffico L4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L4 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv6</source>
            <translation>IPv6 traffico L4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L4 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L2 Multiple Streams</source>
            <translation>Flussi multipli L2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 10/100/1000 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 SAMComplete</source>
            <translation>SAMComplete IPv4 traffico L3 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 SAMComplete</source>
            <translation>SAMComplete IPv6 traffico L3 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv4</source>
            <translation>L3 Multiple Streams IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv4 SAMComplete</source>
            <translation>10/100/1000 L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv6</source>
            <translation>L3 Multiple Streams IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv6 SAMComplete</source>
            <translation>10/100/1000 L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 TCP Wirespeed</source>
            <translation>Wirespeed TCP L4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed SAMComplete</source>
            <translation>SAMComplete wirespeed TCP L4 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed</source>
            <translation>TrueSpeed wirespeed TCP L4 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VNF Test</source>
            <translation>Test VNF</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>VNF TrueSpeed wirespeed TCP L4 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Term</source>
            <translation>Term Traffico 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Mon</source>
            <translation>Mon Traffico 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Loopback</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Loopback</source>
            <translation>Loopback 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Thru</source>
            <translation>Doppio Thru</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Dual Thru</source>
            <translation>Doppio Thru Traffico 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Multiple Streams</source>
            <translation>Flussi multipli Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Term</source>
            <translation>Term Flussi 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Triple Play</source>
            <translation>Triple Play Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Triple Play</source>
            <translation>Triple Play 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MiM Traffic</source>
            <translation>Traffico MiM Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Term</source>
            <translation>Term Traffico MiM 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Mon</source>
            <translation>Mon Traffico MiM 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MPLS-TP Traffic</source>
            <translation>Traffico MPLS-TP Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MPLS-TP Traffic Term</source>
            <translation>Term Traffico MPLS-TP 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 PTP/1588</source>
            <translation>PTP/1588 Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 PTP/1588 Term</source>
            <translation>Term PTP/1588 10/100/1000 Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term</source>
            <translation>Term Ping 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term IPv6</source>
            <translation>Term IPv6 Ping 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term IPv6</source>
            <translation>Term IPv6 Traceroute 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term</source>
            <translation>Term Traffico 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon</source>
            <translation>Mon Traffico 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback</source>
            <translation>Loopback 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Dual Thru</source>
            <translation>Doppio Thru Traffico 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term IPv6</source>
            <translation>Term IPv6 Traffico 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon IPv6</source>
            <translation>Mon IPv6 Traffico 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Multiple Streams</source>
            <translation>Flussi multipli Livello 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term</source>
            <translation>Term Flussi 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term IPv6</source>
            <translation>Term IPv6 Flussi 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback IPv6</source>
            <translation>Loopback IPv6 Flussi 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Triple Play</source>
            <translation>Triple Play Livello 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Triple Play</source>
            <translation>Triple Play 10/100/1000 Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Traffic</source>
            <translation>Traffico Livello 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term</source>
            <translation>Term Traffico 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback</source>
            <translation>Loopback 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term IPv6</source>
            <translation>Term IPv6 Traffico 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback IPv6</source>
            <translation>Loopback IPv6 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Multiple Streams</source>
            <translation>Flussi multipli Livello 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term</source>
            <translation>Term Flussi 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback</source>
            <translation>Loopback Flussi 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term IPv6</source>
            <translation>Term IPv6 Flussi 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback IPv6</source>
            <translation>Loopback IPv6 Flussi 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 PTP/1588</source>
            <translation>PTP/1588 Livello 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 PTP/1588 Term</source>
            <translation>Term PTP/1588 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 TCP Wirespeed</source>
            <translation>Wirespeed TCP Livello 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 TCP Wirespeed Term</source>
            <translation>Term Wirespeed TCP 10/100/1000 Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IP Video</source>
            <translation>Video OP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Explorer</source>
            <translation>MPTS Explorer</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Explorer</source>
            <translation>OP Video MPTS Explorer 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Explorer</source>
            <translation>SPTS Explorer</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Explorer</source>
            <translation>OP Video SPTS Explorer 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Analyzer</source>
            <translation>MPTS Analyzer</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Analyzer</source>
            <translation>OP Video MPTS Analyzer 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Analyzer</source>
            <translation>SPTS Analyzer</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Analyzer</source>
            <translation>OP Video SPTS Analyzer 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth VoIP Term</source>
            <translation>Term VoIP 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>J-Profiler</source>
            <translation>J-Profiler</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth J-Profiler</source>
            <translation>J-Profiler 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical</source>
            <translation>100M Optical</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth TrueSAM</source>
            <translation>TrueSAM 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L4 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L4 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 ethernet fibra ottica 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 SAMComplete</source>
            <translation>100M Optical Eth L3 Traffic IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 SAMComplete</source>
            <translation>100M Optical Eth L3 Traffic IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv4 SAMComplete</source>
            <translation>100M Optical Eth L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv6 SAMComplete</source>
            <translation>100M Optical Eth L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Term</source>
            <translation>Term Traffico 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor/Thru</source>
            <translation>Monitor / Thru</translation>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Loopback Term</source>
            <translation>Term Loopback 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Term</source>
            <translation>Term Flussi 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Triple Play</source>
            <translation>Triple Play 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Term</source>
            <translation>Term Traffico 100M Optical MiM Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 100M Optical MiM Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MPLS-TP Traffic Term</source>
            <translation>Term Traffico 100M Optical MPLS-TP Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 PTP/1588 Term</source>
            <translation>Term PTP/1588 100M Optical Eth Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term</source>
            <translation>Term Ping 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term</source>
            <translation>Term Traffico 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback</source>
            <translation>Loopback 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffic IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term</source>
            <translation>Term Flussi 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Triple Play</source>
            <translation>Triple Play 100M Optical Eth Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term</source>
            <translation>Term Traffico 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback</source>
            <translation>Loopback 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback IPv6</source>
            <translation>Loopback IPv6 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term</source>
            <translation>Term Flussi 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback</source>
            <translation>Loopback Flussi 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 PTP/1588 Term</source>
            <translation>Term PTP/1588 100M Optical Eth Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Explorer</source>
            <translation>OP Video MPTS Explorer 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Explorer</source>
            <translation>OP Video SPTS Explorer 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Analyzer</source>
            <translation>OP Video MPTS Analyzer 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Analyzer</source>
            <translation>OP Video SPTS Analyzer 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth VoIP Term</source>
            <translation>Term VoIP 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth J-Profiler</source>
            <translation>J-Profiler 100M Optical Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical</source>
            <translation>1GigE Optical</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE</source>
            <translation>1GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical TrueSAM</source>
            <translation>TrueSAM 1GigE ottico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 1GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 1GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L4 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L4 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 1GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>SAMComplete IPv4 traffico L3 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>SAMComplete IPv6 traffico L3 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv4 SAMComplete</source>
            <translation>1GigE L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv6 SAMComplete</source>
            <translation>1GigE L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed SAMComplete</source>
            <translation>SAMComplete wirespeed TCP L4 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed</source>
            <translation>TrueSpeed wirespeed TCP L4 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>VNF TrueSpeed wirespeed TCP L4 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP Check</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 PTP Check</source>
            <translation>PTP Check 1GigE L4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SyncE Wander</source>
            <translation>SyncE Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical SyncE Wander Mon/Thru</source>
            <translation>1GigE SyncE Wander Mon/Thru Ottico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 BERT</source>
            <translation>BERT Livello 1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Term</source>
            <translation>Term BERT 1GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 1GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Patterns</source>
            <translation>Pattern Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Patterns Term</source>
            <translation>Term Pattern 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Term</source>
            <translation>Term Traffico 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Loopback</source>
            <translation>Loopback 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Term</source>
            <translation>Term Flussi 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Triple Play</source>
            <translation>Triple Play 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Term</source>
            <translation>Term Traffico MiM 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 1GigE MiM Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MPLS-TP Traffic Term</source>
            <translation>Term Traffico MPLS-TP 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Term</source>
            <translation>Term PTP/1588 1GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Dual Mon</source>
            <translation>1GigE Layer 2 PTP/1588 Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term</source>
            <translation>Term Ping 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term</source>
            <translation>Term Traffico 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback</source>
            <translation>Loopback 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffico IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term</source>
            <translation>Term Flussi 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Triple Play</source>
            <translation>Triple Play 1GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term</source>
            <translation>Term Traffico 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback</source>
            <translation>Loopback 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback IPv6</source>
            <translation>Loopback IPv6 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term</source>
            <translation>Term Flussi 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback</source>
            <translation>Loopback Flussi 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 PTP/1588 Term</source>
            <translation>Term PTP/1588 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 TCP Wirespeed Term</source>
            <translation>Term Wirespeed TCP 1GigE Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Explorer</source>
            <translation>OP Video MPTS Explorer 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Explorer</source>
            <translation>OP Video SPTS Explorer 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Analyzer</source>
            <translation>OP Video MPTS Analyzer 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Analyzer</source>
            <translation>OP Video SPTS Analyzer 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Eth VoIP Term</source>
            <translation>Term VoIP 1GigE Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE J-Profiler</source>
            <translation>J-Profiler 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN</source>
            <translation>10GigE LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Optical TrueSAM</source>
            <translation>TrueSAM 10GigE ottico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L4 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L4 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 Traffic IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 Traffic IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed SAMComplete</source>
            <translation>SAMComplete wirespeed TCP L4 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed TrueSpeed</source>
            <translation>TrueSpeed wirespeed TCP L4 LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Term</source>
            <translation>Term BERT 10GigE LAN Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 10GigE LAN Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Term</source>
            <translation>Term Traffico 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Loopback</source>
            <translation>Loopback 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Term</source>
            <translation>Term Flussi 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 2 Triple Play</source>
            <translation>Triple Play 10GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Term</source>
            <translation>Term Traffico MiM 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico MiM 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MPLS-TP Traffic Term</source>
            <translation>Term Traffico MPLS-TP 10GigE LAN Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 PTP/1588 Term</source>
            <translation>Term PTP/1588 10GigE LAN Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term</source>
            <translation>Term Ping 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term</source>
            <translation>Term Traffico 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback</source>
            <translation>Loopback 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffico IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term</source>
            <translation>Term Flussi 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 10GigE LAN Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 3 Triple Play</source>
            <translation>Triple Play 10GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term</source>
            <translation>Term Traffico 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback</source>
            <translation>Loopback 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback IPv6</source>
            <translation>Loopback IPv6 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term</source>
            <translation>Term Flussi 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback</source>
            <translation>Loopback Flussi 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 PTP/1588 Term</source>
            <translation>Term PTP/1588 10GigE LAN Layer 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 TCP Wirespeed Term</source>
            <translation>Term Wirespeed TCP 10GigE LAN Livello 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Explorer</source>
            <translation>OP Video MPTS Explorer 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Explorer</source>
            <translation>OP Video SPTS Explorer 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Analyzer</source>
            <translation>OP Video MPTS Analyzer 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Analyzer</source>
            <translation>OP Video SPTS Analyzer 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN VoIP Term</source>
            <translation>Term VoIP 10GigE LAN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN</source>
            <translation>10GigE WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Term</source>
            <translation>Term BERT 10GigEWAN OC-192c Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 10GigEWAN OC-192c Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 OC-192c WAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 10GigEWAN OC-192c Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Loopback</source>
            <translation>Loopback 10GigEWAN OC-192c Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Term</source>
            <translation>Term Flussi 10GigEWAN OC-192c Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 10GigEWAN OC-192c Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term</source>
            <translation>Term Ping 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 OC-192c WAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback</source>
            <translation>Loopback 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffic IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term</source>
            <translation>Term Flussi 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 10GigEWAN OC-192c Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c L3 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 10GigEWAN OC-192c L3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Term</source>
            <translation>Term BERT 10GigEWAN STM-64 Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 10GigEWAN STM-64 Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 STM-64 WAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 10GigEWAN STM-64 Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Loopback</source>
            <translation>Loopback 10GigEWAN STM-64 Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Term</source>
            <translation>Term Flussi 10GigEWAN STM-64 Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Loopback</source>
            <translation>Loopback Flussi 10GigEWAN STM-64 Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term</source>
            <translation>Term Ping 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 STM-64 WAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffic 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback</source>
            <translation>Loopback 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffic IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback IPv6</source>
            <translation>Loopback IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term</source>
            <translation>Term Flussi 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Loopback</source>
            <translation>Loopback Flussi 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 10GigEWAN STM-64 Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 L3 Streams Loopback IPv6</source>
            <translation>Loopback Flussi IPv6 10GigEWAN STM-64 L3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE</source>
            <translation>40GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Optics Self-Test</source>
            <translation>Autotest delle reti ottiche 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 40GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 40GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 40GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>SAMComplete IPv4 traffico L3 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>SAMComplete IPv6 traffico L3 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv4 SAMComplete</source>
            <translation>40GigE L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv6 SAMComplete</source>
            <translation>40GigE L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 PCS</source>
            <translation>PCS Livello 1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Term</source>
            <translation>Term PCS 40GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Mon/Thru</source>
            <translation>Mon/Thru PCS 40GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Term</source>
            <translation>Term Traffico 40GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 40GigE Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Streams Term</source>
            <translation>Term Flussi 40GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term</source>
            <translation>Term Ping 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term</source>
            <translation>Term Traffico 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 40GigE Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffico IPv6 40GigE Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term</source>
            <translation>Term Flussi 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 40GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE</source>
            <translation>100GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Optics Self-Test</source>
            <translation>Autotest delle reti ottiche 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L2 100GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic QuickCheck</source>
            <translation>QuickCheck traffico L3 100GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 IPv4 traffico L3 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic SAMComplete</source>
            <translation>SAMComplete traffico L2 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Streams SAMComplete</source>
            <translation>SAMComplete flussi L2 100GigE </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>SAMComplete IPv4 traffico L3 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>SAMComplete IPv6 traffico L3 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv4 SAMComplete</source>
            <translation>100GigE L3 Streams IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv6 SAMComplete</source>
            <translation>100GigE L3 Streams IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Term</source>
            <translation>Term PCS 100GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Mon/Thru</source>
            <translation>Mon/Thru PCS 100GigE Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term</source>
            <translation>Term Traffico 100GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 100GigE Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term</source>
            <translation>Term Flussi 100GigE Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term</source>
            <translation>Term Ping 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term</source>
            <translation>Term Traceroute 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term</source>
            <translation>Term Traffico 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 100GigE Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Mon/Thru Traffico IPv6 100GigE Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term</source>
            <translation>Term Flussi 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 100GigE Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE RS-FEC</source>
            <translation>100GigE RS-FEC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term RS-FEC</source>
            <translation>100GigE Layer 2 Traffic Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon RS-FEC</source>
            <translation>100GigE Layer 2 Traffic Mon RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term RS-FEC</source>
            <translation>100GigE Layer 2 Streams Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term RS-FEC</source>
            <translation>100GigE Layer 3 Ping Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6 RS-FEC</source>
            <translation>100GigE Layer 3 Ping Term IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term RS-FEC</source>
            <translation>100GigE Layer 3 Traceroute Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6 RS-FEC</source>
            <translation>100GigE Layer 3 Traceroute Term IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term RS-FEC</source>
            <translation>100GigE Layer 3 Traffic Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon RS-FEC</source>
            <translation>100GigE Layer 3 Traffic Mon RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6 RS-FEC</source>
            <translation>100GigE Layer 3 Traffic Term IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon IPv6 RS-FEC</source>
            <translation>100GigE Layer 3 Traffic Mon IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term RS-FEC</source>
            <translation>100GigE Layer 3 Streams Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6 RS-FEC</source>
            <translation>100GigE Layer 3 Streams Term IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig</source>
            <translation>1Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1G FC</source>
            <translation>1G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Term BERT 1Gig Fibre Channel Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 1Gig Fibre Channel Livello 1 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Term Patterns 1Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Term Traffico 1Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 1Gig Fibre Channel Livello 2 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig</source>
            <translation>2Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2G FC</source>
            <translation>2G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Term BERT 2Gig Fibre Channel Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 2Gig Fibre Channel Livello 1 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Term Patterns 2Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Term Traffico 2Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 2Gig Fibre Channel Livello 2 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig</source>
            <translation>4Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4G FC</source>
            <translation>4G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Term BERT 4Gig Fibre Channel Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 4Gig Fibre Channel Livello 1 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Term Patterns 4Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Term Traffico 4Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 4Gig Fibre Channel Livello 2 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig</source>
            <translation>8Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8G FC</source>
            <translation>8G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>8Gig Fibre Channel Livello 2 Patterns Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>8Gig Fibre Channel Livello 2 Traffic Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>8Gig Fibre Channel Livello 2 Traffic Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig</source>
            <translation>10Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G FC</source>
            <translation>10G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Term BERT 10Gig Fibre Channel Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 10Gig Fibre Channel Livello 1 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Term Traffico 10Gig Fibre Channel Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico 10Gig Fibre Channel Livello 2 </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig</source>
            <translation>16 GB</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16G FC</source>
            <translation>FC da 16 GB</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Term BERT Layer 1 Fibre Channel 16 GB</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT Layer 1 Fibre Channel 16 GB</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Term traffico Layer 2 Fibre Channel 16 GB</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru traffico Layer 2 Fibre Channel 16 GB</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI</source>
            <translation>614.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI Check</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Check</source>
            <translation>CPRI Check 614.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Term</source>
            <translation>614.4M CPRI Livello 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>614.4M CPRI Livello 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 BERT</source>
            <translation>BERT Livello 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Term</source>
            <translation>614.4M CPRI Livello 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>614.4M CPRI Livello 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI</source>
            <translation>1228.8M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Check</source>
            <translation>CPRI Check 1228.8M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Term</source>
            <translation>1228.8M CPRI Livello 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>1228.8M CPRI Livello 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 1228.8M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 1228.8M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI</source>
            <translation>2457.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Check</source>
            <translation>CPRI Check 2457.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Term</source>
            <translation>Term BERT 2457.6M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 2457.6M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 2457.6M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 2457.6M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI</source>
            <translation>3072.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Check</source>
            <translation>CPRI Check 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Term</source>
            <translation>Term BERT 3072.0M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 3072.0M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 3072.0M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 3072.0M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI</source>
            <translation>4915.2M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Check</source>
            <translation>CPRI Check 4915.2M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Term</source>
            <translation>Term BERT 4915.2M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 4915.2M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 4915.2M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 4915.2M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI</source>
            <translation>6144.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Check</source>
            <translation>CPRI Check 6144.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Term</source>
            <translation>Term BERT 6144.0M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 6144.0M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 6144.0M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 6144.0M CPRI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI</source>
            <translation>9830.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Check</source>
            <translation>CPRI Check 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Term</source>
            <translation>Term BERT 9830.4M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 9830.4M CPRI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT 9830.4M CPRI Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 9830.4M CPRI Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137,6 M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI</source>
            <translation>CPRI 10137,6 M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Check</source>
            <translation>CPRI Check 10137.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Term</source>
            <translation>Term BERT Layer 2 CPRI 10137,6 M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT Layer 2 CPRI 10137,6 M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI</source>
            <translation>768M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Term</source>
            <translation>Term BERT 768M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 768M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Term</source>
            <translation>Term BERT 768M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 768M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI</source>
            <translation>1536M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Term</source>
            <translation>Term BERT 1536M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 1536M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Term</source>
            <translation>Term BERT 1536M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 1536M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI</source>
            <translation>3072M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Term</source>
            <translation>Term BERT 3072M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 3072M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Term</source>
            <translation>Term BERT 3072M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 3072M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI</source>
            <translation>6144M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Term</source>
            <translation>Term BERT 6144M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 6144M OBSAI Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Term</source>
            <translation>Term BERT 6144M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT 6144M OBSAI Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1</source>
            <translation>OTU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN Check</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G OTN Check</source>
            <translation>OTN Check OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Term</source>
            <translation>Term BERT Bulk 2.7G OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48</source>
            <translation>STS-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-48c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STS-12c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-12c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STS-3c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-3c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU0</source>
            <translation>ODU0</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1 2.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GMP ODU0 2.7G OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru GMP Livello 2 OTU1 2.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GMP ODU0 2.7G OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru GMP Livello 3 OTU1 2.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Jitter</source>
            <translation>Jitter OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Jitter BERT Term</source>
            <translation>Term BERT Jitter Bulk OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Jitter BERT Term</source>
            <translation>Term BERT Jitter Bulk OTU1 2.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Jitter BERT Term</source>
            <translation>Term BERT Jitter Bulk OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Wander</source>
            <translation>Wander OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Wander BERT Term</source>
            <translation>Term BERT Wander Bulk OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Wander BERT Term</source>
            <translation>Term BERT Wander Bulk OTU1 2.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Wander BERT Term</source>
            <translation>Term BERT Wander Bulk OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2</source>
            <translation>OTU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G OTN Check</source>
            <translation>OTN Check OTU2 10.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Term</source>
            <translation>Term BERT Bulk 10.7G OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192</source>
            <translation>STS-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STS-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-192c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-48c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STS-12c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-12c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STS-3c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-3c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU1</source>
            <translation>ODU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GMP ODU0 10.7G OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru GMP Livello 2 OTU2 10.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 GMP ODU0 10.7G OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru GMP Livello 3 OTU2 10.7G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODUflex</source>
            <translation>ODUflex</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2 10.7G ODUflex</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Mon</source>
            <translation>Mon BERT Bulk OTU2 10.7G ODUflex</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 GMP ODUflex 10.7G OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e</source>
            <translation>OTU1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G OTN Check</source>
            <translation>OTN Check OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Mon/Thru</source>
            <translation>OTU1e 11.05G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Term</source>
            <translation>Term BERT OTU1e 11.05G Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT OTU1e 11.05G Layer 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Term</source>
            <translation>Term Traffico OTU1e 11.05G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico OTU1e 11.05G Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e</source>
            <translation>OTU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G OTN Check</source>
            <translation>OTN Check OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Mon/Thru</source>
            <translation>OTU2e 11.1G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Term</source>
            <translation>Term BERT OTU2e 11.1G Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Mon/Thru</source>
            <translation>Mon/Thru BERT OTU2e 11.1G Layer 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Term</source>
            <translation>Term Traffico OTU2e 11.1G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico OTU2e 11.1G Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3</source>
            <translation>OTU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G Optics Self-Test</source>
            <translation>Autotest delle reti ottiche OTL3.4 43.02G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3.4 43.02G OTN Check</source>
            <translation>OTN Check OTU3.4 43.02G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 43.02G OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L3 43.02G OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 43.02G OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL BERT</source>
            <translation>OTL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Term</source>
            <translation>OTL3.4 43.02G Term BERT OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Mon</source>
            <translation>OTL3.4 43.02G Mon BERT OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Term</source>
            <translation>Term BERT Bulk 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Term</source>
            <translation>Term Traffico OTU3 43.02G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Monitor/Thru</source>
            <translation>OTU3 43.02G Layer 2 Traffic Monitor/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Streams Term</source>
            <translation>Term Flussi OTU3 43.02G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term</source>
            <translation>Term Ping OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term</source>
            <translation>Term Traceroute OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term</source>
            <translation>Term Traffico OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico OTU3 43.02G Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU3 43.02G Livello 3 Traffico Mon/Thru IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term</source>
            <translation>Term Flussi OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 OTU3 43.02G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768</source>
            <translation>STS-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-768c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-768c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-192c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-48c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-12c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-12c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-3c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-3c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STM-256 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STM-256 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STM-256 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STM-256 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G STM-256 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2e</source>
            <translation>ODU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Term</source>
            <translation>Term. BERT layer 1 ODU2e 43,02 GB OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Layer 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU2e 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Livello 2 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2</source>
            <translation>ODU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU2 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Livello 2 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 ODU2 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Livello 3 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 ODU0 ODU2 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 2 OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Layer 3 ODU0 ODU2 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 3 OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 ODU0 ODU1 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 2 OTU3 43.02G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Layer 3 ODU0 ODU1 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 3 OTU3 43.02G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU0 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Layer 2 Traffic Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 ODU0 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Livello 3 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU3 43.02G ODUflex</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 GMP ODUflex 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4</source>
            <translation>OTU4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G Optics Self-Test</source>
            <translation>Autotest delle reti ottiche OTL4.10 111.8G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G OTN Check</source>
            <translation>OTN Check OTU4 111.8G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L2 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L2 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic RFC 2544</source>
            <translation>RFC 2544 traffico L3 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 IPv6 traffico L3 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Term</source>
            <translation>OTL4.10 111.8G Term BERT OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Mon</source>
            <translation>OTL4.10 111.8G Mon BERT OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Term</source>
            <translation>Term BERT Bulk 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Term</source>
            <translation>Term Traffico OTU4 111.8G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico OTU4 111.8G Layer 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Streams Term</source>
            <translation>Term Flussi OTU4 111.8G Livello 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term</source>
            <translation>Term Ping OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term IPv6</source>
            <translation>Term Ping IPv6 OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term</source>
            <translation>Term Traceroute OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term IPv6</source>
            <translation>Term Traceroute IPv6 OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term</source>
            <translation>Term Traffico OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru</source>
            <translation>Mon/Thru Traffico OTU4 111.8G Layer 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term IPv6</source>
            <translation>Term Traffico IPv6 OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU4 111.8G Livello 3 Traffico Mon/Thru IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term</source>
            <translation>Term Flussi OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term IPv6</source>
            <translation>Term Flussi IPv6 OTU4 111.8G Livello 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU3</source>
            <translation>ODU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Term</source>
            <translation>Termin BERT OTU4 111.8G ODU2e Livello 1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Livello 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU2e 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Livello 2 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU2 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Livello 2 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 ODU2 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Livello 3 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 ODU0 ODU2 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 2 OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Layer 3 ODU0 ODU2 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 3 OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 ODU0 ODU1 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 2 OTU4 111.8G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Layer 3 ODU0 ODU1 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Traffico Mon/Thru ODU0 Livello 3 OTU4 111.8G ODU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Term</source>
            <translation>Term Traffico Livello 2 ODU0 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Livello 2 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Term</source>
            <translation>Term Traffico Livello 3 ODU0 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Livello 3 Taffico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex Bulk BERT Term</source>
            <translation>Term BERT Bulk OTU4 111.8G ODUflex</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Term Traffico Layer 2 GMP ODUflex 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Optical BERT</source>
            <translation>BERT Ottico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Term</source>
            <translation>3072.0M Optical Livello 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Mon/Thru</source>
            <translation>3072.0M Optical Livello 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Term</source>
            <translation>9830.4M Optical Livello 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Mon/Thru</source>
            <translation>9830.4M Optical Livello 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing</source>
            <translation>Tempistica</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1PPS</source>
            <translation>1PPS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analisi</translation>
        </message>
        <message utf8="true">
            <source>1PPS Analysis</source>
            <translation>Analisi di 1PPS</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Wander</source>
            <translation>Wander 2MHz</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Clock Wander Analysis</source>
            <translation>Analisi Wander Clock 2MHz</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Wander</source>
            <translation>Wander 10MHz</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Clock Wander Analysis</source>
            <translation>Analisi Wander Clock 10MHz</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Benchmark</source>
            <translation>Benchmark</translation>
            <comment>This text appears in the main Test menu.&#xA;This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Modulo di Temporizzazione</translation>
        </message>
    </context>
</TS>
