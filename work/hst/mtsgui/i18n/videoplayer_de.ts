<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi Videospieler</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Beenden</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Name Videodatei</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Zeit:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>Videostatus</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Messen und kürzen</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Masstab</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Offen &amp;Datei...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Bildformat</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Masstab</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Messmodus</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>In Ansicht eingepasst</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>Messen und kürzen</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Offene Datei...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Öffnen</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>Kein Offenes Medium</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Laden...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Pufferung...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Spielt...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Fehler...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Leerlauf - Media stoppen</translation>
        </message>
    </context>
</TS>
