<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CPowerOffProcess</name>
        <message utf8="true">
            <source>Power off</source>
            <translation>Mise hors tension</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set shuts down.</source>
            <translation>Veuillez patienter pendant que l'équipement de test se met hors tension.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CRebootProcess</name>
        <message utf8="true">
            <source>Reboot</source>
            <translation>Redémarrage</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set reboots.</source>
            <translation>Veuillez patienter pendant que le lot de test redémarre.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CShutdownPrompt</name>
        <message utf8="true">
            <source>Power options</source>
            <translation>Options d'alimentation</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
</TS>
