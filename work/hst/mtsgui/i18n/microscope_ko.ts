<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi 현미경</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>스냅샷</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>보기 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>보기 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>보기 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>보기 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>보기 FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>현미경</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>캡쳐</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>프리즈</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>이미지</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>밝기</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>대조</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>스크린 레이아웃</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>전체 화면</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>타일</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>종료</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>이미지 저장</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>이미지 선택</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>이미지 파일 (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
    </context>
</TS>
