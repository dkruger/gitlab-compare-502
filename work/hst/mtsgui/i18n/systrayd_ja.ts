<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>systrayd::CAudioSystemTrayDataItem</name>
        <message utf8="true">
            <source>Audio</source>
            <translation>ｵｰﾃﾞｨｵ</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBatterySystemTrayDataItem</name>
        <message utf8="true">
            <source>Battery/Charger</source>
            <translation>ﾊﾞｯﾃﾘｰ/充電器</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please plug in AC power to continue using the test instrument.</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎます。 ﾃｽﾄ機器を引き続き使用する場合は、AC 電源を接続してください。</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please keep the AC power plugged in to continue using the test instrument.</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎます。 ﾃｽﾄ機器を引き続き使用する場合は、AC 電源を接続したままにしてください。</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and cannot be used until it cools down. The AC power must remain plugged in to continue using the test instrument.</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎるため、温度が下がるまでは使用できません。 ﾃｽﾄ機器を引き続き使用する場合は、必ず AC 電源を接続したままにしてください。</translation>
        </message>
        <message utf8="true">
            <source>Battery charging is not possible at this time.</source>
            <translation>現在、ﾊﾞｯﾃﾘーは充電できません。</translation>
        </message>
        <message utf8="true">
            <source>A power failure has occurred, the battery is currently not charging.</source>
            <translation>電源障害が発生したことにより、ﾊﾞｯﾃﾘーは、現在、充電されていません。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBeepingWarning</name>
        <message utf8="true">
            <source>System Warning</source>
            <translation>システム警告</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBluetoothSystemTrayDataItem</name>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>File received and placed in the bluetooth inbox.&#xA;Filename: %1</source>
            <translation>ﾌｧｲﾙが受信され、Bluetooth ｲﾝﾎﾞｯｸｽに保存されました。&#xA;ﾌｧｲﾙ名: %1</translation>
        </message>
        <message utf8="true">
            <source>Pair requested. You may go to the Bluetooth system page to complete the pair by clicking on the icon above.</source>
            <translation>ﾍﾟｱﾘﾝｸﾞが要求されています。上のｱｲｺﾝをｸﾘｯｸして、Bluetooth ｼｽﾃﾑ ﾍﾟｰｼﾞを開き、ﾍﾟｱﾘﾝｸﾞを完了することができます。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CNtpSystemTrayDataItem</name>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CP5000iSystemTrayDataItem</name>
        <message utf8="true">
            <source>P5000i</source>
            <translation>P5000i</translation>
        </message>
        <message utf8="true">
            <source>The P5000i Microscope will not function in this USB port.  Please use the other USB port.</source>
            <translation>P5000i ﾏｲｸﾛｽｺｰﾌﾟはこの USB ﾎﾟｰﾄでは機能しません｡ 他の USB ﾎﾟｰﾄを使用してください｡</translation>
        </message>
        <message utf8="true">
            <source>Microscope Error</source>
            <translation>ﾏｲｸﾛｽｺｰﾌﾟｴﾗｰ</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CStrataSyncSystemTrayDataItem</name>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CUsbFlashSystemTrayDataItem</name>
        <message utf8="true">
            <source>USB Flash</source>
            <translation>USB ﾌﾗｯｼｭ</translation>
        </message>
        <message utf8="true">
            <source>Format complete. Please remember to eject the device from the USB system page before removing it.</source>
            <translation>ﾌｫｰﾏｯﾄが完了しました。ﾃﾞﾊﾞｲｽを取り外す前に、USB ｼｽﾃﾑ ﾍﾟｰｼﾞから安全に取り出す操作をしてください。</translation>
        </message>
        <message utf8="true">
            <source>Formatting USB device. Do not remove device until process is completed.</source>
            <translation>USB ﾃﾞﾊﾞｲｽをﾌｫｰﾏｯﾄ中です。処理が完了するまで、ﾃﾞﾊﾞｲｽを取り外さないでください。</translation>
        </message>
        <message utf8="true">
            <source>The USB device was removed without being ejected. Data may be corrupted.</source>
            <translation>安全な取り出し操作がされずに、USB ﾃﾞﾊﾞｲｽが取り外されました。ﾃﾞｰﾀが破損した可能性があります。</translation>
        </message>
        <message utf8="true">
            <source>The USB device has been ejected and is safe to remove.</source>
            <translation>USB ﾃﾞﾊﾞｲｽを取り出す操作が完了し、安全に取り外すことができます。</translation>
        </message>
        <message utf8="true">
            <source>Please remember to eject the device from the USB system page before removing it.</source>
            <translation>ﾃﾞﾊﾞｲｽを取り外す前に、USB ｼｽﾃﾑ ﾍﾟｰｼﾞから安全に取り出す操作をしてください。</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CVncSystemTrayDataItem</name>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CWifiSystemTrayDataItem</name>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
    </context>
</TS>
