<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI 检查</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>编辑上一个配置</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>从配置文件加载配置</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>开始新配置 ( 这将复位所有的缺省设置 )</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>手动</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>手动配置测试设置</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>测试设置</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>保存配置文件</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>结束： 手动配置</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>运行测试</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>已存储</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>加载配置文件</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>结束： 加载配置文件</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>编辑配置</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>运行</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>运行 CPRI 检查</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>SFP 验证</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>终端测试</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>创建报告</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>重复测试</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>查看详细结果</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>退出 CPRI 检查</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>查看</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>接口</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>第2层</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>结束： 审查结果</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>报告信息</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>结束： 创建报告</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>查看详细结果</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>正在运行</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>测试失败</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>未完成</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI 检查：</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** 开始 CPRI 检查 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** 测试已完成 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** 测试已中止 ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>跳过保存配置文件</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>您可以保存用于运行此 测试配置。 &#xA;&#xA; 它可以用于（全部或选择个别 &#xA; ”配置文件”）配置将来测试。</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>跳过加载配置文件</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>本地 SFP 验证</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>连接器</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>请插入 SFP。</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>SFP 波长 (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>SFP 厂商</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>SFP 厂商版本号</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>SFP 供应商 P/N</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>推荐速率</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>显示其他 SFP 数据</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP 正常。</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>无法验证此速率的 SFP。</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP 不可接受。</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>设置 </translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>远端设备</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>其他的</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>硬环回</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>是</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>否</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>光纤接收端信号等级最大限制 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>光纤接收端信号等级最小限制 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>往返延迟最大限制 (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>跳过 CPRI 检查</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP 检查</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>测试状态关键</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>% 完成</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>已计划</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>运行 &#xA; 测试</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>停止&#xA;测试</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>本地 SFP 结果</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>未检测到 SFP。</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>波长 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>最高 Tx 电平（ dBm ）</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>最高 Rx 电平（ dBm ）</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>收发信机</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>界面结果</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI 检查测试结论</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>接口测试</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>第 2 层测试</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD 测试</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT 测试</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>信号显示</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>同步获得</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Rx 最大频偏 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>编码违例</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>光 Rx 电平 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>第 2 层结果</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>启动状态</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>帧同步</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>RTD 结果</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>往返延迟平均值 (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>BERT 结果</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>码型同步</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>码型丢失</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Bit/TSE 错误</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>设备类型</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>L1 同步</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>协议设置</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M 平面设置</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>操作</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>被动链路</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>跳过报告创建</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>测试报告信息</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>客户名称：</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>技术员 ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>测试位置：</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>工作顺序：</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>备注 / 注意：</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>无线电：</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>波段：</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>整体状态</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>增强的 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>光纤通道测试</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>连接</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>对称</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>本地设置</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>连接至远程</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>网络</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>光纤信道设置</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>FC 测试</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>配置模板</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>选择测试</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>利用</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>帧长</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>吞吐量测试</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>帧丢失测试</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>紧接测试</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>缓冲器容量测试</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>测试控制</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>测试阈值</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>更改配置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>高级光纤信道设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>高级利用设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>高级吞吐量延迟设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>高级紧接测试设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>高级丢帧测试设置</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>运行业务激活测试</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>更改配置并返回测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>环路时延</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>背靠背</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>缓冲器容量</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>缓冲器容量吞吐</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>退出 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>创建另一报告</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>封面</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>在本地装置测试时，“单向传输延迟时间源同步”已丢失。请检查“ OWD 时间源”硬件连接。如果测试未完成，将继续进行，但“帧延迟”结果将不可用。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>在远程装置测试时，“单向传输延迟时间源同步”已丢失。请检查“ OWD 时间源”硬件连接。如果测试未完成，将继续进行，但“帧延迟”结果将不可用。</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>已找到工作回路</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>邻居地址解析失败。</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>服务#1：向目标MAC发送ARP请求。</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>本地端服务 #1 ：向目标 MAC 发送 ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>远程端服务 #1 ：向目标 MAC 发送 ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>为获得目的 MAC 地址， Tx  ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>本地端向目标 MAC 发送 ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>远程端向目标 MAC 发送 ARP 请求。</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>提供的网元端口为半双工模式。 如果希望继续，请按“继续使用半双工”按钮。 否则请按“中止测试”。</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>尝试环回操作。 正检查活动的环。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>检查硬件循环.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>正在检查 LBM/LBR 回路。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>正在检查永久环路.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>DHCP参数请求超时。DHCP参数无法获取。</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>选择环回模式会与远程设备断开连接。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>SAMComplete 已经超时，因为无法确定某个最终接收的帧数。测量的接收帧数意外地继续递增。</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>已找到硬循环</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>与远程设备建立连接后， J-Quickcheck 才可以进行流量连接测试。</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>已找到 LBM/LBR 回路</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>本地链路丢失，使用远程设备连接。 一旦链路重新创建，可再次尝试连接远程端。</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>链路当前没激活</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>源和目标 IP 完全相同。 请重新配置源或目标 IP 地址。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>本地和远程应用程序不兼容。本地应用程序是流量应用程序， IP 地址为 #1 的远程应用程序是数据流应用程序。</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>未能建立循环.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>尚未与远程装置建立连接。请转到“连接”页面，验证目标 IP 地址，然后按下“连接到远程”按钮。</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>请转至“网络”页面，检查配置，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>等待光模块准备就绪，或者光模块不存在。</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>已找到永久环路</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>PPPoE 连接超时。 请检查 PPPoE 设置并重试。</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>遇到一个不可恢复的 PPPoE 错误</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>正在尝试登录服务器 ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>发现远程连接问题。 可能无法访问远程设备</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>无法与 IP 地址为 #1 的远程装置建立连接。请检查远程源 IP 地址并重试。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>IP 地址为 #1 的远程应用程序是环回应用程序。与增强型 RFC 2544 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>本地和远程应用不兼容。 IP 地址为 #1 的远程应用不是 TCP WireSpeed 应用。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>远程设备上的软件版本似乎更新。 如果继续测试，某些功能可能受限。 为了获得更佳的性能，建议在此设备上升级软件。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>远程设备上的软件版本似乎更老。 如果继续测试，某些功能可能受限。 为了获得更佳的性能，建议在远程设备上升级软件。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>无法确定远程设备上的软件版本。 如果继续测试，某些功能可能受限。 建议在相同软件版本设备间进行测试。</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>环回操作尝试未成功。 再试一次。</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>所选模板的设置已成功应用。</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>远程设备上的软件版本无法确定，或者与此设备上的版本不匹配。如果继续测试，某些功能可能受限。建议在相同软件版本设备间进行测试。</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>无法完成显性登录。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>本地和远程应用不兼容。 本地应用是第 #1 层应用， IP 地址为 #2 的远程应用是第 #3 层应用。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>本地设备线速率 (#1 Mbps) 与远程设备线速率 (#2 Mbps) 不匹配。 请在“连接”页面上重新配置到非对称。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>本地设备线速率 (#1 kbps) 与远程设备线速率 (#2 kbps) 不匹配。 请在“连接”页面上重新配置到非对称。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>本地设备线速率 (#1 Mbps) 与远程设备线速率 (#2 Mbps) 不匹配。 请在“连接”页面上重新配置到非对称并重启测试。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>本地设备线速率 (#1 kbps) 与远程设备线速率 (#2 kbps) 不匹配。 请在“连接”页面上重新配置到非对称并重启测试。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>本地设备线速率 (#1 Mbps) 与远程设备线速率 (#2 Mbps) 不匹配。 请在RFC2544“对称”页面上重新配置到非对称。</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>本地设备线速率 (#1 kbps) 与远程设备线速率 (#2 kbps) 不匹配。 请在RFC2544“对称”页面上重新配置到非对称。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>无效配置 :&#xA;&#xA; 至少选择一个测试。 请至少选择一个测试并重试。</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>您未选择任何要测试的帧尺寸。 启动前请至少选择一种帧尺寸。</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>在上一秒中检测到超过配置的丢帧阈值的丢帧率。 请通过手动流量测试验证链路性能。 完成手动测试后可返回 RFC 2544 。 &#xA; 推荐手动测试配置： &#xA; 帧大小： #1 字节 &#xA; 流量： 稳定为 #2 Mbps&#xA; 测试持续时间： 至少为配置的测试持续时间的 3 倍。 吞吐量测试持续时间为 #3 秒。 &#xA; 结果监控： &#xA; 使用摘要状态屏幕查找错误事件。 如果错误计数器周期性递增，请在不同的低流量速率下进行手动测试。 如果在低速率下仍然出错，链路可能存在与最大负载无关的一般性问题。 还可使用图形结果丢帧率电流图表查看是否存在周期性或持续的丢帧事件。 如果无法解决周期错误问题，您可以设置丢帧容差阈值，容许较小的丢帧率。 注意： 一旦使用丢帧容差，测试将不满足 RFC 2544 的推荐范围。</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>注意：由于发送器和接收器的VLAN堆栈深度不一，会调节配置的速率，补偿端口间的速率差。</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>#1 字节帧</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>#1 字节包</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>虽然流量应该被停止，但 L2 滤波器接收端天线帧数继续递增，超过前 10 秒。</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>把吞吐量速率减少到零位</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>尝试 #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>尝试 #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>尝试 #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>尝试 #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>正在尝试 #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>现在正在验证 #1 L1 Mbps 。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>现在正在验证 #1 L2 Mbps 。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>现在正在验证 #1 L1 kbps 。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>现在正在验证 #1 L2 kbps 。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>正在校验 #1 %.  这将花 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>测得的最大值吞吐量： #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>测得的最大值吞吐量： #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>测得的最大值吞吐量： #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>测得的最大值吞吐量： #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>测到的最大值吞吐量： #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>最大值吞吐量测量不可用</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>丢帧测试 (RFC 2544 标准 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>丢帧测试 ( 自顶向下 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>丢帧测试 ( 自底向上 )</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>正在以 #1 L1 Mbps 负载运行测试。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>正在以 #1 L2 Mbps 负载运行测试。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>正在以 #1 L1 kbps 负载运行测试。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>正在以 #1 L2 kbps 负载运行测试。 这将占用 #2 秒。</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>正在测试 #1 负载。 这将要花 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>背靠背帧测试</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>试验 #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>检测到暂停帧</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 包突发 : 失败</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 帧突发 : 失败</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 包突发 : 通过</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 帧突发 : 通过</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>突发搜索测试</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>尝试 #1 kB 突发</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>大于 #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>小于 #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>缓存大小为 #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>缓存大小小于 #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>缓存大小大于等于 #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>已发送 #1 帧</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>Rx #1 帧</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>丢帧 #1</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>突发 (CBS) 测试</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>预计 CBS ： #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>预计 CBS ： 无效</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>CBS 测试将被跳过。 测试突发尺寸太大，无法准确测试该配置。</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>CBS 测试将被跳过。 测试持续时间不够长，无法准确测试该配置。</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>数据包突发： 失败</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>帧突发： 失败</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>数据包突发： 通过</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>帧突发： 通过</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>突发管制试验 #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>系统恢复测试</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>该数据包尺寸测试无效</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>该帧大小测试无效</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>试验 #1 之中的第 #2 个 :</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>不能造成帧损失，因为在最大值带宽通过的吞吐量测试没有发现帧损失。</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>无法引起任何丢失事件。 该数据包尺寸测试无效</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>无法引起任何丢失事件。 该帧大小测试无效</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>平均值恢复时间： 大于 #1 秒</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>大于 #1 秒</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>平均值恢复时间： 无效</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>平均恢复时间： #1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 us</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>按最佳点缓存正在归零。 正在测试 #1 点</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>测试 #1 信用量</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>用 #1 信用阀值验证。这将占用 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>缓冲器容量吞吐测试</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>注意：假设缓存点小于 2 的硬回路。 本测试无效。</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>注意：假设为硬回路，每一步骤吞吐量测量值为缓存点的两倍，以补偿双倍光纤长度。</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>#1 缓冲区信用阀值的实测吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>吞吐量和延迟的测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>吞吐量和数据包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>吞吐量、延迟和数据包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>延迟和数据包抖动试验 #1 。这将占用 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>数据包抖动测试试验 #1 。这将占用 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>延迟测试试验 #1 。这将占用 #2 秒</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>已验证吞吐量负载的 #2% 下的延迟测试试验 #1 。 这将占用 #3 秒。</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 开始 RFC 2544 测试 #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 开始 FC 测试 #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>测试完成。</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>正在保存结果，请稍候。</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>扩展负载测试</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>光纤通道测试:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>网络配置</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>帧类型</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>测试模式</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>维护 域级别</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>发送器 TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>封装</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN 堆栈深度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>SVLAN DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>用户 SVLAN TPID （ hex ） </translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>CVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>7 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>用户 SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>SVLAN 7 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>6 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>用户 SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>SVLAN 6 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>5 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>用户 SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>SVLAN 5 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>4 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>用户 SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>SVLAN 4 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>3 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>用户 SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>SVLAN 3 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>2 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>用户 SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>SVLAN 2 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>1 SVLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>用户 SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>SVLAN 1 DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>环路类型</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>以太网类型</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>源 MAC</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>自动递增 SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation># MACs 按序排列</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>禁用 IP EtherType</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>禁用 OoS 结果</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>DA 类型</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>目的 MAC</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>数据模式</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>使用认证</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>密码</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>业务提供商</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>业务名</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>源 IP 类型</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>源 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>缺省网关</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>子网掩码</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>目的 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>TTL （跳数）</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>IP ID 增值</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>源链接 - 本地源地址</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>全局源地址</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>子网前缀长度</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>目的地址</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>流量等级</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>流标签</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>跳数限制</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>流量模式</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>源端口服务类型</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>源端口</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>目标端口服务类型</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>目的端口</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>ATP 监听 IP 类型</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>ATP 监听 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>源 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>目的 ID</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>顺序 ID</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>发生器 ID 标识</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>应答器 ID 标识</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>测试配置</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna 净负荷版本</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>延迟测量精度</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>带宽单元</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>最大测试带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>上游最大测试带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>最大测试带宽 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>上游最大测试带宽 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>下游最大测试带宽 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>下游最大测试带宽 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>最大测试带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>允许真 100% 流量</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>吞吐量测量精度</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>上游吞吐量测量精度</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>下游吞吐量测量精度</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>吞吐量归零过程</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>吞吐量帧丢失容限 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>上游吞吐量丢帧容差 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>下游吞吐量丢帧容差 (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>所有测试持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>所有测试的试验次数</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>吞吐量持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>吞吐量通过门限</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>吞吐量通过阈值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>上游吞吐量通过阈值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>吞吐量通过阈值 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>上游吞吐量通过阈值 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>下游吞吐量通过阈值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>下游吞吐量通过阈值 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>吞吐量通过门限 (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>延迟试验次数：</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>延迟试验持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>延迟带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>延迟通过阈值</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>延迟通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>上游延迟通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>下游延迟通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>包抖动计数测试次数</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>数据包抖动试验持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>包抖动测试通过门限</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>数据包抖动通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>上游数据包抖动通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>下游数据包抖动通过阈值 (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>帧丢失测试步骤</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>丢帧试验持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>丢帧带宽间隔 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>上游丢帧带宽间隔 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>丢帧带宽间隔 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>上游丢帧带宽间隔 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>下游丢帧带宽间隔 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>下游丢帧带宽间隔 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>帧丢失带宽粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>丢帧最小值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>丢帧最小值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>丢帧最小值范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>丢帧最大值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>丢帧最大值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>丢帧最大值范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>上游丢帧最小值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>上游丢帧最小值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>上游丢帧最大值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>上游丢帧最大值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>下游丢帧最小值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>下游丢帧最小值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>下游丢帧最大值范围 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>下游丢帧最大值范围 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>丢帧步骤数</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>紧接试验次数</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>紧接间隔 ( 帧 )</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>紧接最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>上游紧接最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>下游紧接最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>忽视暂停帧</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>突发测试类型</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>突发 CBS 尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>上游突发 CBS 尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>下游突发 CBS 尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>突发搜索最小尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>突发搜索最大尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>上游突发搜索最小尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>上游突发搜索最大尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>下游突发搜索最小尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>下游突发搜索最大尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS 持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>突发测试试验次数</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>突发 CBS 显示通过 / 失败</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>突发搜索显示通过 / 失败</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>突发搜索尺寸阈值 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>上游突发搜索尺寸阈值 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>下游突发搜索尺寸阈值 (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>系统恢复试验数</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>系统恢复开销持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>扩展负载持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>扩展负载吞吐量缩放 (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>扩展负载数据包长度</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>扩展负载帧长</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>缓存点登录类型</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>缓存点最大缓存大小</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>缓存点吞吐量步骤</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>缓存点持续时间</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>远端环路成功拆除： **#1** 设备在 LLB 模式之外</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>远端环路成功拆除： **#1** 设备在透明 LLB 模式之外</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>远端 **#1** 设备已经拆除环路</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>配置更改引起远程回路向下</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>远端环路建立成功 :**#1** 设备处于 LLB 模式</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>远端环路建立成功： **#1** 设备处于透明 LLB 模式</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>远端 **#1** 设备已经处于 LLB 模式</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>远端 **#1** 设备已经处于透明 LLB 模式</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>不支持自环或通过其它端口的环路</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>数据流 #1: 远端环路拆除不成功 : 确认超时</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>数据流 #1: 远端环路建立不成功 : 确认超时</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>拆除远端环路不成功 : 确认超时</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>拆除远端透传环路不成功：确认超时</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>建立远端环路不成功 : 确认超时</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>建立远端透传环路不成功：确认超时</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>全球地址重复地址检测开始。</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>全球地址配置失败（检查设置）。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>等待全球地址重复地址检测。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>重复地址检测成功。全球地址有效。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>重复地址检测失败。全球地址无效。</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>链路本地地址重复地址检测已开始。</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>链路本地地址配置失败（检查设置）。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>等待链路本地地址重复地址检测。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>重复地址检测成功。链路本地地址有效。</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>重复地址检测失败。链路本地地址无效。</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>无状态 IP 检索已开始。</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>获取无状态 IP 失败。</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>成功获取无状态 IP 。</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>全状态 IP 检索已开始。</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>无路由器可提供全状态 IP 。</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>重新尝试全状态 IP 检索。</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>成功获取全状态 IP 。</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>网络不可达</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>主机不可达</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>协议不可达</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>端口不可达</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>信息太长</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>目的网络未知</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>目的主机未知</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>目的网络被禁止</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>目的主机被禁止</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>因为 TOS 网络不可达</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>因为 TOS 主机不可达</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Tx 期间时间超出</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>重新组装期间时间超出</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>ICMP 未知错误</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>目的不可达。</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>地址不可达</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>无到目的地路由</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>目的地址不相邻</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>与目的地的通讯被禁止</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>过大包</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>超过跳数限制</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>碎片组装超时</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>错误头域统计</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>未知头部类型</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>未知 IPv6 选择</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>未连接</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPOE 已激活</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 激活</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>网络联上</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>用户请求未激活</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>数据层停止</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE 超时</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP 超时</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 故障</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 失败</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 认证失败</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 未知故障</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 失败</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>无效配置</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>内部错误</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>地址解析协议 (ARP) 成功 , 目的 MAC 地址已获得。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>等待 ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>No ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>继续半双工</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>停止 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>上游</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>下游</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>J-QuickCheck 测试</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>远端环路状态</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>本地环回状态</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>PPPoE 状态</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>IPv6 状态</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>ARP 状态</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>DHCP 状态</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>ICMP 状态</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>邻居发现状态</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>自动配置状态</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>地址状态</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>设备发现</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>查找设备</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>正在搜索</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>使用所选设备</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>连接到远程</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>连接 &#xA; 到远程</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>断开</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>已连接 &#xA; 至远程</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>正在连接</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>对称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非对称</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>单向</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>环回</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>下游和上游</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>双方向</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>吞吐量：</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>下游和上游吞吐量相同。</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>下游和上游吞吐量不同。</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>只测试单向网络。</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>测量：</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>本地产生流量，本地测试。</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>本地和远程产生流量，各方向测量。</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>测量方向：</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>本地产生流量，远程测试仪器测量。</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>远程产生流量，本地测试仪器测量。</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>环路时延</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>单向延迟</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>仅往返延迟测量。 &#xA; 远程设备不能进行单向延迟。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>仅单向延迟测量。 &#xA; 远程设备不能进行双向 RTD 。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>无法进行任何延迟测量。 &#xA; 远程设备不能进行双向 RTD 或单向延迟。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>无法进行任何延迟测量。 &#xA; 远程设备不能进行单向延迟。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>无法进行任何延迟测量。 &#xA; 远程设备不能进行双向 RTD 。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>仅单向延迟测量。 &#xA; 单向测试时不支持 RTD 。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>无法进行任何延迟测量。 &#xA; 单向测试时不支持 RTD 。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>无法进行任何延迟测量。 &#xA; 远程设备不能进行单向延迟。 &#xA; 单向测试时不支持 RTD 。</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>无法进行任何延迟测量。</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>延迟测量类型</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>本地</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>远端</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>需要远程 Viavi 测试仪器。</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>版本2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>版本3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 校准</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>光学元件选择</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>到远端的通信信道 IP 设置</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>环回测试不需要本地 IP 地址设置。</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>静态</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>静态 - 每一服务</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>静态－单个</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>手动</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>全状态</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>无状态</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>源 IP</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>源链路本地地址</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>源全球地址</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>自动获得</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>用户定义</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>ARP 模式</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>源地址类型</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>PPPoE 模式</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>高级设置</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>启动的</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>禁止的</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>用户源 MAC</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>出厂缺省值</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>缺省源 MAC</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>用户缺省 MAC</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>用户源 MAC</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>用户 MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>跳过连接</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>堆叠 VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>本地网络端口上使用了多少 VLAN ？</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>无 VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLAN (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLAN ( 堆叠 VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>输入 VLAN ID 设置：</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>堆栈深度</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID （十六进制）</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>用户</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>发现服务器设置</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>注意：链接本地目标 IP 无法用于“连接至远程”</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>全局目标 IP</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>正在 Ping</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>帮我找到 &#xA; 目标 IP</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>本地端口</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>状态未知</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>连接断掉</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>本地端口:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>自协商</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>关闭</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>自协商:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>等待连接</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>正在连接...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>正在重试 ...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>已连接</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>连接失败</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>连接已中止</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>通信信道:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>发现服务器高级设置</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>从发现服务器获取目标 IP</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>服务器 IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>服务器端口</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>服务器密码</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>要求的租赁时间 ( 分钟 )</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>授予的租赁时间 ( 分钟 )</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>L2 网络设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>本地装置设置</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>流量</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>LBM 流量</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 （最低）</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 （最高）</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>用户 SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>设置回路类型、以太网类型和 MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>设置 MAC 地址、以太网类型和 LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>设置 MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>设置 MAC 地址和 ARP 模式</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>目的类型</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>单播</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>组播</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>广播</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>VLAN 用户优先级</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>远程设备未连接。</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>L2 网络设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>远程装置设置</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>L3 网络设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>设置流量类别、流标签和跳限制</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>您的网络使用什么样的 IP 优先级？</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>设置剩余时间、IP ID 增值和 PPPoE 模式</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>设置剩余的时间和 PPPoE 模式</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>设置剩余时间和 IP ID 增值</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>设置剩余的时间</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>您的网络使用什么 IP 优先级？</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>L3 网络设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>L4 网络设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>源业务类型</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>DHCP- 客户端</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>DHCP- 服务器</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>指针</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>目的业务类型</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>设置 ATP 监听 IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>L4 网络设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>设置回路类型和序列、响应器和发起人 ID</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>高级 L2 设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>源类型</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>缺省 MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>用户 MAC</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>LBM 配置</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>LBM 类型</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>启用发送器 TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>高级 L2 设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>高级 L3 设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>TTL （跳数）</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>高级 L3 设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>高级 L4 设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>高级 L4 设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>高级网络设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>模板</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>希望使用配置模板？</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Viavi 增强型数据包访问率 > 传输率</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - 尽力而为</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - 欧洲大陆</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - 全球</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - 城市</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - 移动回程 H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - 地区</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - VoIP 数据仿真</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC 2544 Bit 透明</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 数据包访问率 > 传输率</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 数据包访问率 = 传输率</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>应用模板</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>模板配置：</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>吞吐量丢帧 (%) ： #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>延迟阈值 (us) ： #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>包抖动测试通过门限 (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>帧大小： 默认</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>最大值带宽 ( 上游 ) ： #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>最大值带宽 ( 下游 ) ： #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>最大值带宽： #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>测试： 吞吐量和延迟的测试</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>测试： 吞吐量、延迟、丢帧和紧接</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>持续时间和试验： 1 次试验 #1 秒</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>阈值算法： 改进的 Viavi</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>阈值算法： RFC 2544 标准</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>吞吐量阈值 ( 上游 ) ： #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>吞吐量阈值 ( 下游 ) ： #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>吞吐量通过门限 #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>丢帧算法： RFC 2544 标准</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>紧接突发时间： 1 秒</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>紧接间隔： 1 帧</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>设置高级利用设置</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>最大带宽</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>下游最大带宽 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>下游最大带宽 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>下游最大带宽 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>下游最大带宽 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>下游最大带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>上游最大带宽 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>上游最大带宽 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>上游最大带宽 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>上游最大带宽 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>上游最大带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>最大带宽 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>最大带宽 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>最大带宽 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>最大带宽 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>最大带宽 (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>所选帧</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>长度类型</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>帧</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>包</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>复位</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>全部清除</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>选择全部</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>帧长</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>包长度</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>上游 &#xA; 帧长</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>远程设备未连接。</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>下游 &#xA; 帧长</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>远程设备 &#xA; 未连接。</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>所选测试</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>延迟 （需要吞吐量）</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>缓存点 ( 需要吞吐量 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>缓存点吞吐量 ( 需要缓存点 )</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>系统恢复 ( 仅回路，需要吞吐量 )</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>额外测试</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>数据包抖动（需要吞吐量）</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>突发测试</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>扩展负载 ( 仅环回 )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>吞吐量 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>归零过程</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC2544标准式</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi增强式</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>设置高级吞吐量测量设置</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>下游测量精度</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>下游测量精度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>下游测量精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>下游测量精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>下游测量精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>下游测量精度 (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>1.0% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>0.1% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>0.01% 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>在 0.001% 之内</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>400 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>40 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>4 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>.4 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>1000 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>100 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>10 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>1 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>.1 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>.01 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>.001 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>.0001 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>92.942 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>9.2942 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>.9294 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>.0929 Mbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>10000 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>1000 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>100 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>10 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>1 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>.1 kbps 以内</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>上游测量精度</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>上游测量精度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>上游测量精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>上游测量精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>上游测量精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>上游测量精度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>测试准确度</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>测量精度 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>测量精度 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>测量精度 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>测量精度 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>测量精度（ % ）</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>更多信息</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>故障排除</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>调试</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>高级吞吐量设置</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>配置延迟 / 数据包抖动测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>分别配置延迟测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>分别配置数据包抖动测试持续时间</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>丢帧 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>测试步骤</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom Up</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>步骤数</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>每个步骤 #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>每个步骤 #1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>每个步骤 #1 % 线速率</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>下游测试范围 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>下游测试范围 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>下游测试范围 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>下游测试范围 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>下游测试范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>最大</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>下游带宽间隔 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>下游带宽间隔 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>下游带宽间隔 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>下游带宽间隔 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>下游带宽间隔 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>上游测试范围 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>上游测试范围 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>上游测试范围 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>上游测试范围 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>上游测试范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>上游带宽间隔 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>上游带宽间隔 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>上游带宽间隔 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>上游带宽间隔 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>上游带宽间隔 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>测试范围 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>测试范围 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>测试范围 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>测试范围 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>测试范围 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>带宽间隔 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>带宽间隔 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>带宽间隔 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>带宽间隔 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>带宽粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>设置高级丢帧测量设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>高级丢帧设置</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>可选测试测量</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>测量延迟</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>测量数据包抖动</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>紧接 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>下游最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>上游最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>最大突发持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>突发粒度 ( 帧 )</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>设置高级紧接设置</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>暂停帧策略</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>缓存点 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flow Control Login Type</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>隐式 ( 透明链路 )</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>显性的 (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>最大缓存大小</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>吞吐量步骤</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>测试控制</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>分别配置测试持续时间？</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>测试次数</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>包抖动</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>延迟 / 数据包抖动</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>突发 (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>系统恢复</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>全部测试</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>显示 &#xA; 通过 / 失败</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>上游 &#xA; 阈值</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>下游 &#xA; 阈值</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>阈值</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>吞吐量阈值 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>吞吐量阈值 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>吞吐量阈值 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>吞吐量阈值 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>吞吐量通过门限 (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>远程设备 &#xA; 未连接。</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>延迟 RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>延迟 OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>包抖动（微秒）</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>突发搜索尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>突发 (CBS 管制 )</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>高精度 - 低延迟</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>低精度 - 高时延</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>运行 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>跳过 FC 测试</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>开</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>关</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>缓冲器容&#xA;量吞吐</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>上游吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>上游吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>下游吞吐量测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>下游吞吐量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>吞吐量异常</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>上游吞吐量异常</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>下游吞吐量异常</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>吞吐量（Mbps）</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>吞吐量（kbps）</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>吞吐量 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>帧 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>帧 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>帧 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>帧 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>帧 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>帧 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>帧 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>帧 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>帧 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>帧 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>通过 / 失败</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>帧长 &#xA;( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>包长 &#xA;( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>测得的 &#xA; 速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>测得的 &#xA; 速率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>测得的 &#xA; 速率 ( 帧 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>R_RDY&#xA; 检测</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>配置速率&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>测得的 L1&#xA; 速率 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>测得的 L1&#xA; 速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>测得的 L1&#xA;(% 线速率 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>测得的 L2&#xA; 速率 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>测得的 L2&#xA; 速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>测得的 L2&#xA;(% 线速率 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>测得的 L3&#xA; 速率 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>测得的 L3&#xA; 速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>测得的 L3&#xA;(% 线速率 )</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>测得的 L4&#xA; 速率 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>测得的 L4&#xA; 速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>测得的 L4&#xA;(% 线速率 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>测得的速率 &#xA; ( 帧 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>测得的速率 &#xA; ( 数据包 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>暂停 &#xA; 检测</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Cfg 率 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Cfg 率 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Cfg 率 &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Cfg 率 &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>异常</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>检测到 OoS 帧 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>检测到天线净负荷 &#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>检测到 FCS&#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>检测到 IP 校验和 &#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>检测到 TCP/UDP 校验和 &#xA; 错误</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>延迟测试</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>上游延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>上游延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>下游延迟测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>下游延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>延迟 &#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>延迟 &#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>测得的 &#xA;% 线速率</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>暂停 &#xA; 检测</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>帧丢失测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>上游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>下游丢帧测试结果</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>丢帧结果</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>帧 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>配置的速率 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>配置的速率 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>配置的速率 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>配置的速率 (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>配置的速率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>帧丢失 (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>吞吐量率 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>吞吐量率 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>吞吐量率 &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>吞吐量率 &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>吞吐率&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>丢帧率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>最大平均数据包 &#xA; 抖动 (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>错误 &#xA; 检测</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>OoS&#xA; 检测</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>配置速率&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>背靠背测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>上游紧接测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>下游紧接测试结果</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>平均值 &#xA; 突发帧</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>平均值 &#xA; 突发秒数</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>缓冲器容量测试结果</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>注意</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>最小值缓存大小 &#xA;( 点 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>缓冲器容量吞吐测试结果</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>缓存点吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>吞吐量（ L1 Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>点 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>点 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>点 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>点 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>点 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>点 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>点 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>点 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>点 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>点 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>缓冲区大小 &#xA; （信用）</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>测得的速率 &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>测得的速率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>测得的速率 &#xA;( 帧 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>J-Proof - 以太网第 2 层透明性测试</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>J-Proof 帧</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>运行测试</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>运行 J-Proof 测试</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>概要</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>结束： 详细结果</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>退出 J-Proof 测试</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>应用</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>配置第 2 层透明性测试的帧类型</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>名字</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   协议   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>协议</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> 帧类型 </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> 封装 </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>堆叠</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>帧尺寸</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>帧尺寸</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>计数</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>速率 &#xA;( 帧 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>速率</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>超时 &#xA;( 毫秒 )</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>超时</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>控制</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>DEI&#xA;比特</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>用户 SVLAN TPID</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>自动增加 CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>自动增加 CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>自动增加 SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>自动增加 SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>封装</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>封装</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>帧类型</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit 公司</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>快速配置</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>加 &#xA; 帧</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>清除 &#xA; 帧</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>源地址通用于所有帧。 可以通过本地设置页面进行配置。</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Pbit 增量</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>VLAN 堆栈</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>SPbit 增量</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>长度</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>数据</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>确认</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>防抖配置</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>强度</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>快速 (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>全 (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>系列</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>所有的</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>生成树</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>思科</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>激光器 &#xA; 打开</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>激光器 &#xA; 关闭</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>开始帧序列</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>停止帧序列</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof 测试</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>服务 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>停止的</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>进行中</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>净荷错误</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>头错误</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>计数不匹配</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>结果摘要</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>空闲</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>进行中</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>净荷不匹配</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>头不匹配</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>以太网</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>J-Proof 结果</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>名字</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状态</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>光 Rx&#xA;重启</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>运行 QuickCheck 测试</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>QuickCheck 设置</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>QuickCheck 扩展负载结果</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>详细</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>单个的</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>每数据流</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>吞吐量测试:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>QuickCheck 结果</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>扩展负载测试结果</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Tx 帧计数</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Rx 帧计数</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>错误的帧计数</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>OoS 帧计数</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>丢帧统计</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>丢帧率</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>硬件</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>永久</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>活跃</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>吞吐量：%1 Mbps (L1)、持续时间：%2 秒、帧大小：%3 字节</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>吞吐量： %1 Mbps(L1) 、持续时间： %2 秒、帧大小： %3 字节</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>不是您希望的 ?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>开始</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>成功</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>查找目标</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>ARP 状态:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>远程循环</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>检查活动循环</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>检查硬循环</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>正在检查永久环路</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>正在检查 LBM/LBR 回路</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>活动循环</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>永久环路</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR 环回</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>循环失败</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>远程循环:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>测得的吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>未确定</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>无法获得</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>实测吞吐量（ L1 Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>查看错误</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>负载 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>负载 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>吞吐量测试持续时间（秒）</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>帧大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>比特率</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>性能测试持续时间</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 秒</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 秒</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 分钟</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 分</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 分</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 分</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 小时</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 小时</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 小时</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 小时</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>用户定义</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>测试持续时间（秒）</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>性能测试持续时间（分钟）</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>帧长 (字节)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>巨帧</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>用户长度</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>超长帧长度</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  电气连接器：10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>激光器波长</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>电子连接器</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>光连接器</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>详细信息 ...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>激活链路</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>流量结果</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>错误数</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>差错帧</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 帧</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>丢帧</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>接口详细信息</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>SFP/XFP 细节</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>波长（ nm ）：可调谐 SFP ，参考信道 / 波长调谐设置。</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>电缆长度 (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>支持调谐</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>波长，信道</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>波长</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>通道</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>建议速率&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>标称速率 (Mbits/秒)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>最低速率（ Mbits ／秒）</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>最高速率（ Mbits ／秒）</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>厂商</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>厂商 PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>厂商版本</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>功率电平类型</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>诊断监测</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>诊断字节</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>配置 JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>波长 (nm)：可调设备，参考信道/波长调谐设置。</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>CFP/QSFP详细信息</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>CFP2/QSFP详细信息</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>CFP4/QSFP 详细信息</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>厂商 SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>日期代码</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>批次码</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>硬件/软件版本#</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA硬件规范版本号</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA管理I/F版本号</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>功率级别</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx 功率电平类型</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>接收端最大 Lambda 功率 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>发送端最大 Lambda 功率 (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>活动光纤数</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>每一光纤波长</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>诊断字节</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>每一光纤波长范围 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>最大网络线路位速率 (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>模块 ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>支持的额定值</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>标称波长 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>标称比特率 (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** 推荐用于 100GigE RS-FEC 应用程序 ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>CFP 专家模式</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>CFP2 专家模式</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>CFP4 专家</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>启用 Viavi 环回</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>启用 CFP Viavi 环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>启用CFP专家模式</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>启用CFP2专家模式</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>启用 CFP4 专家模式</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP 发送端</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>预加重</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>CFP 发送端预加重</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>缺省 / 默认</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>低</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>标定</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>高</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>时钟分隔器</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>CFP 发送端时钟分频器</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>偏移的偏移值（字节）</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>CFP 发送端倾斜偏移</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>奇偶反转</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP 发送端极性反转</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>重设发送FIFO</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 接收端</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>均衡</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>CFP 接收端均衡</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP 接收端极性反转</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>忽略 LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP 接收端忽略 LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>重设接收FIFO</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>QSFP 专家模式</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>启用QSFP专家模式</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP 接收端忽略 LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>CDR 旁路</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>接收</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR 接收旁路</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>传输</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR 传输旁路</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>如果 QSFP 模块支持，则 CDR 传输和接收旁路控制可用。</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>工程技术</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR 核心环回</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>CFP XCVR 核心环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR 线路环回</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>CFP XCVR 线路环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>模块主环回</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>CFP 模块主机环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>模块网络侧环回</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>CFP 模块网络环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>CFP 变速箱系统环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>CFP 变速箱线路环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>变速箱芯片 ID</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>变速箱芯片版本</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>变速箱唯一识别码版本</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>变速箱唯一识别码 CRC</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>变速箱系统线路</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>变速箱线路</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>CFPn 主机线路</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>CFPn 网络线路</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR 核心环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR 线路环回</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO 速览 DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO 速览 PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO 速览 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO 查询 DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO 查询 PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO 查询 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO 查询值</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>寄存器 A013 控制每条线路激光启用/禁用。</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C 速览 PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C 速览 DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C 速览 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C 查询 PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C 查询 DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C 查询 RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C 查询值</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>注册 0x56 控制每通路激光启用/禁用。</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+ 详细信息</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>信号</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Tx 信号时钟</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>时钟源</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>内部的</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>已恢复</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>外部的</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>外部 1.5M</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>外部 2M</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>外部 10M</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>远端已恢复</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>计时模块</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>VC-12 来源</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>内部 - 频偏 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>远端时钟</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>可调设备</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>调谐模式</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>频率</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>频率 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>第一个可调频率 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>最后一个可调频率 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>网格间隔 (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>偏移警报</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>偏移警报阈值 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>需要重新同步</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>重新同步完成</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>RS-FEC 校准通过</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>RS-FEC 校准失败</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC 通常与 SR4、PSM4、CWDM4 一同使用。</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>要执行 RS-FEC 校准，请执行以下操作（也适用于 CFP4）：</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>在 CSAM 中插入 QSFP28 适配器</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>在适配器中插入 QSFP28 收发器</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>在收发器中插入光纤环回设备</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>单击校准</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>校准</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>校准…</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>重新同步</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>必须立即将收发器重新同步到被测设备 (DUT)。</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>从收发器中移除光纤环回设备</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>建立与被测设备 (DUT) 的连接</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>打开 DUT 激光</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>验证 CSAM 上的信号存在 LED 为绿色</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>单击通路重新同步</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>通路&#xA;重新同步</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>J-QuickCheck 结果</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>按 开始 按钮开始执行 J-QuickCheck 测试。</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>继续半双工模式.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>检查上行方向的流量连接。</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>检查下行方向的流量连接。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>所有服务的流量连接已成功验证。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>流量连接无法成功完成服务： #1 上行方向验证。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>流量连接无法成功完成服务： #1 下行方向验证。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>流量连接无法成功完成验证分别在上行方向服务： #1 和下行方向服务： #2 。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>流量连接无法成功完成上行方向验证。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>流量连接无法成功完成下行方向验证。</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>流量连接无法成功完成上行或下行方向验证。</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>检查连接性</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>流量连接：</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>开始 QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>停止 QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>光学自测</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>结束：保存配置文件</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>报告</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>光学</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>退出光纤自测</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>光信号丢失。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>检测到低功率水平。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>检测到高功率水平。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>检测到一个错误。测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>因为检测到过度偏移，测试失败。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>因为检测到过度偏移，测试失败。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>检测到一个位错误。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>因为 BER 超过配置的阈值，测试失败。</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>因为模式同步丢失，测试失败。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>无法读取插入的 QSFP+ 。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>无法读取插入的 CFP 。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>无法读取插入的 CFP2 。</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>无法读取插入的 CFP4。</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>检测到 RS-FEC 可校正误码。测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>检测到 RS-FEC 不可校正误码。测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>RS-FEC 校准失败</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>检测到输入频率偏差错误</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>检测到输出频率偏差错误</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>丢失同步</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>检测到代码违例</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>丢失对齐标志锁定</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>检测到无效对齐标志</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>检测到 BIP 8 AM 位错误</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>检测到 BIP 块错误</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>检测到偏移</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>检测到块错误</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>检测到小型帧</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>检测到远程错误</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 检测到位错误</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>模式同步丢失</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>因为模式同步丢失，无法准确测量 BER 阈值</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>测得的 BER 超过所选的 BER 阈值</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>检测到 LSS</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>检测到 FAS 错误。</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>检测到帧不足</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>检测到丢帧</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>帧同步丢失</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>检测到逻辑线路标记不足</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>检测到逻辑线路标记错误</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>检测到线路对齐丢失</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>线路对齐丢失</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>检测到 MFAS 错误</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>检测到线路对齐不足</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>检测到 OOMFAS</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>检测到恢复不足</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>检测到过度偏移</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>RS-FEC 校准成功</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>差错</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>光学测试</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>测试类型</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>测试利用 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>光学类型</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>整体测试定论</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>信号有无测试</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>光信号水平测试</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>过度偏移测试</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>位错误测试</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>一般性错误测试</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>BER 阈值测试</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>光纤功率</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>接收端信号功率 Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>接收端信号功率 Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>接收端信号功率 Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>接收端信号功率 Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>接收端信号功率 Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>接收端信号功率 Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>接收端信号功率 Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>接收端信号功率 Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>接收端信号功率 Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>接收端信号功率 Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>接收端信号功率总和 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>发送端信号等级 Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>发送端信号等级 Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>发送端信号等级 Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>发送端信号等级 Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>发送端信号等级 Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>发送端信号等级 Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>发送端信号等级 Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>发送端信号等级 Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>发送端信号等级 Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>发送端信号等级 Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>发送端信号等级总和 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>设置</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>用一根短而干净的跳接电缆连接您想测试的连接器的发送和接收终端。</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>测试 CFP&#xA; 光学</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>测试 CFP2&#xA;光纤/插槽</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>放弃测试</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>测试 QSFP+&#xA; 光学</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>测试 QSFP28&#xA;光纤</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>设置:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>推荐</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 分</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 分</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 小时</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 小时</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>用户持续时间（分钟）</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>推荐持续时间（分钟）</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>此配置的推荐时间为 &lt;b>%1&lt;/b> 分钟。</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>BER 阈值类型</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>FEC 后</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>FEC 前</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>BER 阈值</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>启用 PPM 线偏移值</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>PPM 最大偏移量 (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>错误时停止</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>结果概览</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>光纤/插槽类型</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>当前值 PPM 偏移值</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>当前值 BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>光纤功率 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>接收端信号功率 Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>接收端信号等级 Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>接收端信号等级 Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>接收端信号等级 Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>接收端信号功率 Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>接收端信号功率 Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>接收端信号功率 Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>接收端信号功率 Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>接收端信号功率 Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>接收波长功率 #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>接收端信号功率总和</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>发送端信号等级 Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>发送端信号等级 Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>发送端信号等级 Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>发送端信号等级 Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>发送端信号等级 Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>发送端信号等级 Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>发送端信号等级 Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>发送端信号等级 Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>发送端信号等级 Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>发送端信号等级 Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>发送端信号等级总和</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>CFP 接口详细信息</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>CFP2 接口详细信息</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>CFP4 界面详细信息</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>QSFP 接口详细信息</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+ 界面详细信息</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>QSFP28 界面详细信息</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>无 QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>无法读取 QSFP ，请重新插入 QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP 检查和错误</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>不能询问要求的 QSFP 注册。</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>不能确认 QSFP 身份</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN 检查</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>持续时间和载荷 BERT 测试</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>GCC 透传</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>选择和运行测试</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>高级设置</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>运行 OTN 检查测试</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>净荷 BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>退出 OTN 检查测试</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>测量频率</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>自协商状态</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>RTD 配置</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>所有通道</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN 检查：</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** 开始 OTN 检查测试 ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>检测到载荷误码</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>检测到超过100000个载荷误码</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>超过载荷BERT 误码率阈值</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 检测到的载荷 BERT误码个数</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>载荷误码率: #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>已超出 RTD 阈值</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1：#2 - RTD：最小：#3，最大：#4，平均：#5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1：RTD 不可用</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>正在运行载荷BERT 测试</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>正在运行 RTD 测试</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>正在运行 GCC 透传测试</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>检测到 GCC 位错误</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>已超出 GCC BER 阈值</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 检测到 GCC 位错误</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>检测到 100,000 多个 GCC 位错误</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>GCC 位错误率：#1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** 开始回环检查 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** 跳过回环检查 ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** 回环检查已完成 ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>已检测到回环</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>未检测到回环</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>信道 #1 不可用</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>信道 #1 现在可用</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>码型丢失同步。</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>GCC 码型丢失同步。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>测试已中止：检测到一个位错误。</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>测试失败：BER 已超出配置的阈值。</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>测试失败：RTD 已超出配置的阈值。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>测试已中断：未检测到回环。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>测试已中止：无光信号。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>测试已中止：帧丢失。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>测试已中止：帧丢失同步。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>测试已中止：码型丢失同步。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>测试已中止：GCC 码型丢失同步。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>测试已中止：通道丢失对齐。</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>测试已中止：标记丢失锁定。</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>必须选择至少一个信道。</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>未检查到回环</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>未检测到回环</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>测试选择</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>测试的计划持续时间</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>测试运行时间</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>OTN 检查需要执行流量环回；需要在 OTN 回路远端执行该环回。</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>OTN 检查测试</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>光学元件偏移，信号映射</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>信号映射</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>信号映射和光学元件选择</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>高级 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>信号结构</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP 光口 RTD 偏移 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP 光口 RTD 偏移 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 光口 RTD 偏移 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>CFP4 光纤 RTD 偏移 (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>配置持续时间和有效负载 BERT 测试</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>持续时间和有效负载 Bert Cfg</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>有效载荷 BERT 设置</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>可信度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>根据线路速率、BER 阈值和可信度，建议的测试时间为 &lt;b>%1&lt;/b>。</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>BERT 码型</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>错误阈值</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>显示通过/失败</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>环路延迟测试设置</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>RTD Cfg</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>包括</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>阈值 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>测量频率 (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>GCC Cfg</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>GCC 透传测试设置</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>GCC 信道</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>GCC BER 阈值</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>GCC 中使用 BERT 模式 2^23-1。</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>环回检查</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>跳过回环检查</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>请按“开始”按钮开始回环检查。</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>跳过 OTN 检查测试</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>正在检查回环</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>已检测到回环</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>跳过回环检查</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>回环检测</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>回环检测</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>有效载荷 BERT 结果</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>已测量 BER</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>位错误计数</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>判决</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>有效载荷 BERT 配置</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>环路延迟测试结果</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>最小值 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>最大值 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>平均 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>注意：当平均 RTD 超出阈值时，出现失败情况。</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>环路时延配置</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>GCC 透传测试结果</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>GCC BERT位错误率</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>GCC BERT位错误</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>GCC 透传配置</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>协议分析</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>捕捉</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>捕获和分析 CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>捕获和分析</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>选择分析的协议</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>开始 &#xA; 分析</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>中止 &#xA; 分析</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>分析</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>专家 PTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>手动配置测试设置 </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>阈值</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>运行快速检查</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>退出专家 PTP 测试</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>链路失效。</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>单向延迟时间源同步已丢失。</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>PTP 从属会话意外停止。</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>时间源同步未出现。请验证您的时间源已正确配置并连接。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>无法建立 PTP 从属会话。</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>未检测到 GPS 接收机。</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>TOS 类型</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>通知 Rx 端超时</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>通知</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>每秒 128 次</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>每秒 64 次</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>每秒 32 次</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>每秒 16 次</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>每秒 8 次</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>每秒 4 次</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>每秒 2 次</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>每秒 1 次</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>同步</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>延迟请求</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>租赁期 (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>启用</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>时间误差最大值阈值启用</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>时间误差最大值 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>时间误差最大值阈值 (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>测试持续时间（分钟）</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>快速检查</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>主 IP</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>PTP 域</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>开始&#xA;检查</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>正在开始&#xA;会话</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>会话&#xA;已建立</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>时间误差最大值阈值 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>时间误差最大值 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>时间误差，Cur (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>时间误差，Cur (us) 与时间</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>测试结果</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>持续时间（分钟）</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP 检查</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>PTP 检查测试</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>结束：PTP 检查</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>开始另一项测试</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>退出 PTP 检查</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>增强型 RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>无效设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>高级 IP 设置 - 本地</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>高级 RTD 延迟设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>高级突发 (CBS) 测试设置</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>运行 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>J-QuickCheck 设置</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>抖动</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>扩展负载</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>退出 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>本地网络配置</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>远程网络配置</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>本地自动协商状态</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>速率 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>双工模式</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>流控</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>全双工</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>半双工</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>暂停能力</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>远程自动协商状态</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>半</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>全</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>两者都不</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx 和 Tx </translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation> Tx </translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation> Rx </translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>RTD 帧率</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>每秒 1 帧</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>每秒 10 帧</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>突发 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>承诺突发尺寸</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>CBS 管制 (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>突发搜索</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>下游 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>下游突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>最小值</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>最大值</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>上游 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>上游突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>设置高级 CBS 设置</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>设置高级 CBS 管制设置</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>设置高级突发搜索设置</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>容差</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>扩展负载 Cfg</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>吞吐量缩放 (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544的下列配置设置无效：</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>流量连接已成功验证。 正在运行负载测试。</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>按“开始”在此线速率上运行。</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>按“开始”，使用配置的 RFC 2544 带宽运行。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>测得的吞吐量不用于 RFC 2544 测试。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>测得的吞吐量将用于 RFC 2544 测试。</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>负载测试帧大小： %1 字节。</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>负载测试数据包大小： %1 字节。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>上游负载测试帧大小： %1 字节。</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>下游负载测试帧大小： %1 字节。</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>测得的吞吐量：</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>使用配置的 RFC 2544 最大带宽进行测试</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>使用测得的吞吐量测量值作为 RFC 2544 最大带宽</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>负载测试帧大小 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>负载测试数据包大小 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>上游负载测试帧大小 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>下游负载测试帧大小 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>运行 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>跳过 RFC 2544 测试</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>检查硬件环回</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>包抖动测试</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>上游抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>上游抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>下游抖动测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>下游抖动测试结果</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>最大平均抖动 (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>最大平均抖动</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>最大平均 &#xA; 抖动 (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>上游 CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>下游 CBS 测试结果</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>上游突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>下游突发搜索测试结果</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>上游 CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>下游 CBS 管制测试结果</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>突发 (CBS 管制 ) 测试</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Cfg 突发 &#xA; 尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>发送端突发 &#xA; 尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>平均值接收端 &#xA; 突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>帧已发送</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>帧 &#xA; 已接收</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>丢失 &#xA; 帧</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>突发尺寸 &#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>延迟 &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>抖动 &#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>配置的突发尺寸&#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>发送端突发 &#xA; 管制尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>预计 &#xA;CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>上游系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>上游系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>下游系统恢复测试图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>下游系统恢复测试结果</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>恢复时间 (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>超载率 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>超载率 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>超载率 &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>超载率 &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>过载速率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>恢复率 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>恢复率 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>恢复率 &#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>恢复率 &#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>恢复速率 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>平均值恢复 &#xA; 时间 (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed测试</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>模式</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>控制</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>TrueSpeed 控制</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>正在调整</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>步骤配置</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>选择步骤</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>通道 MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>移动窗口</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>高级 TCP</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>连接设置</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed 控制（高级）</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>选择 Bc 单元</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Walk Window</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>退出 TrueSpeed 测试</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>TCP 主机 未能建立连接。 当前测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>由于选择故障诊断模式，远程设备已断开连接。</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>无效测试选择： 运行 TrueSpeed 最少要选择一个测试。</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>测量的 MTU 太小，不能继续。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>文件传输太快 请选择 TCP 吞吐量文件大小，这样测试可运行至少 5 秒。 建议最少运行测试 10 秒。</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>达到最大值重新传输尝试次数。 测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>TCP 主机 遇到了一个错误。 当前测试已中止。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>启动TrueSpeed测试.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>按 MTU 大小正在归零.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>正在测试有 #2 字节 MSS 的 #1 字节 MTU</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>路径 MTU 确定为 #1 字节。 等于 #2 字节的 MSS 。</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>正在执行 RTT 测试。 这将占用 #1 秒。</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>往返时间（ RTT ）确定为 #1 毫秒。</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>正在执行上游 Walk-the-Window 测试。</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>正在执行下游 Walk-the-Window 测试。</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>发送 TCP 流量的 #1 字节。</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>本地出口流量：未调整</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>远程出口流量：未调整</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>本地出口流量：已调整</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>远程出口流量：已调整</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>持续时间（秒）： #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>连接： #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>窗口大小 (kB)： #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>正在执行上游 TCP 吞吐量测试。</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>正在执行下游 TCP 吞吐量测试。</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>执行高级 TCP 测试。这将占用 #1 秒。</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>本地 IP 类型</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>本地 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>本地默认网关</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>本地子网掩码</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>本地封装</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>远端 IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>选择模式</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>您运行的是什么类型的测试？</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>我正在  安装  或  开通  新线路。 *</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>我正在  诊断  已有线路。</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>* 需要远程 MTS/T-BERD 测试仪器</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>您的吞吐量将如何配置？</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>我的下游和上游吞吐量  相同  。</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>我的下游和上游吞吐量  不同  。</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>设置瓶颈带宽，使之在加载 Truespeed 配置时匹配 SAMComplete CIR 的 &#xA; 最大带宽。</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>设置瓶颈带宽，使之在加载 Truespeed 配置时匹配 RFC 2544 的 &#xA; 最大带宽。</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Iperf 服务器</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>T-BERD/MTS 测试仪器</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>IP 类型</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 地址</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>远程设置</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>TCP 主机服务器设置</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>此步骤会为所有后续 TrueSpeed 步骤配置全局设置。 这包括 CIR( 承诺信息速率 ) 和 TCP 通过 %, ，是通过吞吐量测试所需的 CIR 百分比。</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>运行 Walk-the-Window 测试</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>总测试时间（ s ）</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>自动查找 MTU 大小</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>MTU 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>本地 VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>优先级</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>本地优先级</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>本地 TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>本地 DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>下游 CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>上游 CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>远程 &#xA;TCP 主机</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>远程 VLAN ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>远程优先级</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>远端 TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>远端 DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>设置高级 TrueSpeed 设置</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>选择 Bc 单元</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>流量调整</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Bc 单元</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>调整配置文件</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>本地和远程出口流量已经调整</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>仅本地出口流量已调整</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>仅远程出口流量已调整</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>本地或远程出口流量都未经调整</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>流量调整（仅限 Walk the Window 和吞吐量测试）</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>先运行未经调整的测试，然后运行经过调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) 本地</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) 本地</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) 本地</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) 本地</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) 本地</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) 远程</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc（远程）</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) 远程</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) 远程</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) 远程</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) 远程</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>是否确定调整 TCP 流量？</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>未调整的流量</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>已调整的流量</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>运行未经调整的测试</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>然后</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>运行已调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>运行经本地调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>运行同时经过本地和远程调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>运行未经任何调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>运行经本地调整但未经远程调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>运行经本地调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>运行未经本地调整和远程调整的测试</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>调整本地流量</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 毫秒</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 毫秒</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 毫秒</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 毫秒</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 毫秒</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 毫秒</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>远程设备&#xA;未连接</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>调整远程流量</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>显示其他调整选项</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>TrueSpeed 控制（高级）</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>连接到端口</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>TCP 通过 %</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>MTU 上限（字节）</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>使用多个连接</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>启用饱和窗口</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>提高窗口 (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>提高连接率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>步骤配置</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>TrueSpeed 步骤</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>您必须选择至少一个步骤以运行测试。</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>本步骤使用 RFC4821 定义的步骤自动确定终端网络路径的最大传输单元。 TCP 客户端测试设备会试着使用不同大小的数据包发送 TCP 段，以此确定 MTU ，无需借助 ICMP （常规路径 MTU 发现所用）。</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>这一步骤使用低密度TCP传输，报告下一步测试步骤结果要用的基线往返时间(RTT)。 基线RTT是网络的固有延迟，不包括网络拥塞引起的额外延迟。</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>持续时间 (秒)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>本步骤将执行 TCP “窗口扫描”，然后报告 (4) TCP 窗口大小的 TCP 吞吐量结果。本步骤还报告每一窗口大小下实际和预测 TCP 吞吐量情况。</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>窗口大小</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>窗口大小 1 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation>连接 #</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>窗口 1 连接</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>窗口大小 2 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>窗口 2 连接</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>窗口大小 3 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>窗口 3 连接</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>窗口大小 4 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>窗口 4 连接</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>最大分割字节</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>使用路径 MTU 步骤中找到的 &#xA;MSS</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>最大段宽（字节）</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>根据 &lt;b>%1&lt;/b> Mbps 链路带宽和 &lt;b>%2&lt;/b> ms RRT ，理想 TCP 窗口大小为 &lt;b>%3&lt;/b> 字节。 在 &lt;b>%4&lt;/b> 连接数和每个连接 &lt;b>%5&lt;/b> 字节 TCP 窗口宽度下，大致理想传输吞吐量为 &lt;b>%6&lt;/b> kbps ，因此在每一连接上传输 &lt;b>%7&lt;/b> MB 文件就会占用 &lt;b>%8&lt;/b> 秒。</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>根据 &lt;b>%1&lt;/b> Mbps 链路带宽和 &lt;b>%2&lt;/b> ms RRT ，理想 TCP 窗口大小为 &lt;b>%3&lt;/b> 字节。 &lt;font color="red"> 在 &lt;b>%4&lt;/b> 连接数和每一连接的 &lt;b>%5&lt;/b> 字节 TCP 窗口宽度下，会超出链路的承受能力。 因为数据包丢失和额外延迟，实际结果可能比预测结果差。 请降低连接数，减小 TCP 窗口大小，在链路能力范围内运行测试。 &lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>窗口大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>每次连接的文件大小 (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>自动查找用于 30 秒传输的文件大小</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>连接数</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>使用 RTT 步骤中 &#xA;(%1 ms) 找到的 RTT&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>使用路径 MTU 步骤 &#xA;(%1 字节 ) 中找到的 &#xA;MSS</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>该步骤测试传输中将执行多个TCP连接链路可能均衡共享带宽(流量调整)或不均衡共享带宽(流量策略)。</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>该步骤测试传输中将执行多个TCP连接链路可能均衡共享带宽(流量调整)或不均衡共享带宽(流量策略)。如要自动计算窗口大小和连接数，请运行RTT步骤。</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>窗口尺寸 (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>执行 RTT 步骤时 &#xA; 自动计算</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460字节</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>运行 TrueSpeed 测试</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>跳过 TrueSpeed 测试</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>最大值传输单元（ MTU ）</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>最大值段宽（ MSS ）</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>该步骤确定该链路（端对端）最大传输单元（ MTU ）是 %1 字节。 该数量减去第 3/4 层，开销将用作后续步骤 TCP 最大段宽（ MSS ）的大小。 在此， MSS 就是 %2 字节。</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>RTT总结结果</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>平均 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>最小 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>最大 RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>往返时间（ RTT ）测量为 %1 毫秒。 使用最小 RTT ，因为它最接近网络固有延迟。 后续步骤将它用作预测 TCP 性能的基础。</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>往返时间（RTT）测量为 %1 毫秒。使用平均（基本）RTT，因为它最接近网络固有延迟。后续步骤将它用作预测 TCP 性能的基础。</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>上游 Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>下游 Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>上游窗口 1 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>下游窗口 1 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>上游窗口 2 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>下游窗口 2 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>上游窗口 3 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>下游窗口 3 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>上游窗口 4 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>下游窗口 4 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>上游窗口 5 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>下游窗口 5 大小（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>上游窗口 1 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>下游窗口 1 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>上游窗口 2 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>下游窗口 2 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>上游窗口 3 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>下游窗口 3 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>上游窗口 4 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>下游窗口 4 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>上游窗口 5 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>下游窗口 5 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>上游窗口 1 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>上游窗口 1 未调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>上游窗口 1 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>上游窗口 1 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>上游窗口 2 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>上游窗口 2 未调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>上游窗口 2 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>上游窗口 2 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>上游窗口 3 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>上游窗口 3 未调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>上游窗口 3 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>上游窗口 3 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>上游窗口 4 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Upstream Window 4 aktuell unshaped  (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Upstream Window 4 aktuell shaped (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>上游窗口 4 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>上游窗口 5 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>上游窗口 5 未调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>上游窗口 5 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>上游窗口 5 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>下游窗口 1 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>下游窗口 1 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>下游窗口 1 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>下游窗口 2 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>下游窗口 2 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>下游窗口 2 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>下游窗口 3 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>下游窗口 3 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>下游窗口 3 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>下游窗口 4 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Downstream Fenster 4 tatsächliche Form (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>下游窗口 4 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>下游窗口 5 实际 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>下游窗口 5 已调整的实际值 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>下游窗口 5 预测 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbps，平均值.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>窗口 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>实际</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>未调整的实际值</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>已调整的实际值</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>理想</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>窗口 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>窗口 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>窗口 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>窗口 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>TCP Walk the Window 步骤的结果显示每一测试窗口宽度下 / 每一测试连接的实际和理想吞吐量对比。 因为丢失或拥塞，实际会小于理想值。 如果实际大于理想，则说明作为基线的 RTT 太高。 TCP 吞吐量步骤提供 TCP 传输更深层次的分析。</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>上游 TCP 吞吐量实际对比理想</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>上游 TCP 吞吐量图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>上游 TCP 吞吐量重传图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>上游 TCP 吞吐量 RTT 图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>下游 TCP 吞吐量实际对比理想</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>下游 TCP 吞吐量图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>下游 TCP 吞吐量重传图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>下游 TCP 吞吐量 RTT 图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>上游理想传输时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>上游实际传输时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>上行实际 L4 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>上游实际 L4 未调整的吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>上游实际 L4 已调整的吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>上游理想 L4 吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>上行 TCP 效率（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>上游未调整的 TCP 效率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>上游已调整的 TCP 效率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>上行缓存延迟（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>上游未调整的缓存延迟 (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>上游已调整的缓存延迟 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>下行实际 L4 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>下游实际 L4 未调整的吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>下游已调整的实际 L4 吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>下游理想 L4 吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>下行 TCP 效率（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>下游未调整的 TCP 效率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>下游已调整的 TCP 效率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>下行缓存延迟（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>下游未调整的缓存延迟 (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>下游已调整的缓存延迟 (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>TCP 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>实际对比理想</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>上游测试结果可能表明：</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>无进一步的推荐</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>测试未执行足够的持续时间</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>网络缓冲器 / 调整器需要调整</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>因为 TCP 突发，策略器已丢弃数据包。</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>吞吐量良好，但检测到重传。</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>网络堵塞或正在调整流量</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>CIR 的配置可能不正确</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>文件传输太快 &#xA; 请检查文件预计传输时间。</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>%1 条连接，每一条使用 %2 字节 的窗口在链路上传输 %3 MB 文件（共 %4 MB ）。</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>通过 &lt;b>%2&lt;/b> 条连接发送的 &lt;b>%1&lt;/b> 字节 TCP 窗口。</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>实际 L4 （ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>理想 L4 （ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>传输度量</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>TCP 效率 &#xA; （ % ）</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>缓存延迟 &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>下游测试结果可能表明：</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>上游实际对比理想</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>TCP 效率（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>缓存延迟（ % ）</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>未调整的测试结果可能表明：</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>下游实际对比理想</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>吞吐量图表</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>重传帧</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>基线RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>使用这些图表确定因为重新传输和（或）网络拥塞（ RTT 超出基线）可能引起的 TCP 性能问题。</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>重传图表</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>RTT 图表</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>每一连接理想吞吐量</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>对于流量调整的链路，每一连接应接收相对平稳的带宽。 对于流量监管的链路，每一连接都将来回传递，因为流量策略出现重新发送。 对于每一 %1 连接，每一连接应消耗大约 %2 Mbps 带宽。</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>随机</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF 测试</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>测试配置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>高级服务器连接</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>高级测试配置</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>在本地创建报告</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>测试标识</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>结束：查看详细结果</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>结束：在本地创建报告</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>在本地创建另一报告</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS 测试</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT 测试</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>上游测试</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>下游测试</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>以下配置为必填项。</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>以下配置超出范围。请输入介于 #1 到 #2 之间的值。</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>以下配置中含有非法值。请重新选择。</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>没有客户端到服务器测试许可。</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>没有服务器到服务器测试许可。</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>活动测试过多（最多为 #1）。</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>未预留本地服务器。</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>未预留远程服务器。</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>测试实例已经存在。</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>测试数据库读取错误。</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>测试数据库中未找到测试。</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>测试过期。</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>不支持测试类型。</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>测试服务器不是选项。</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>未预留测试服务器。</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>测试服务器的请求模式不正确。</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>远程服务器不允许进行测试。</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>远程服务器上的 HTTP 请求失败。</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>远程服务器没有充足的可用资源。</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>测试客户端不是选项。</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>不支持测试端口。</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>每小时尝试测试的次数过多。</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>构建测试实例失败。</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>构建测试工作流失败。</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>工作流 HTTP 请求不正确。</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>工作流 HTTP 请求失败。</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>不允许进行远程测试。</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>资源管理器出错。</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>未找到测试实例。</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>测试状态发生冲突。</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>测试状态无效。</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>创建测试失败。</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>更新测试失败。</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>已成功创建测试。</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>已成功更新测试。</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>HTTP 请求失败： #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>VNF 服务器版本 (#2) 与仪器版本 (#1) 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>请为 #1 服务器输入用户名和验证密钥。</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>测试无法初始化： #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>测试已中止： #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>服务器连接</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>没有连接服务器所需的信息。</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>没有执行网络通信的链路。</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>网络通信没有有效的源 IP 地址。</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>指定 IP 地址未进行 ping 操作。</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>在指定 IP 地址无法检测到设备。</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>服务器尚未标识为指定 IP 地址。</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>指定 IP 地址无法标识服务器。</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>服务器已标识但未经过身份验证。</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>服务器身份验证失败。</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>验证失败，尝试标识。</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>未授权或未确定，正在尝试进行 ping 操作。</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>服务器已经过身份验证，可进行测试。</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>服务器已连接，并且正在运行测试。</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>正在识别</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>识别</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>TCP 代理服务器版本</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>测试控制器版本</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>有效链路：</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>服务器 ID:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>服务器状态：</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>高级设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>高级连接设置</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>验证密匙</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>请牢记用户名和密钥</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>本地设备</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>服务器</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Window Walk 持续时间（秒）</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>自动</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>自动持续时间</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>测试配置（高级）</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>高级测试参数</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>TCP 端口</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>自动 TCP 端口</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Window Walks 的数目</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>连接数</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>自动连接数</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>饱和窗口</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>运行饱和窗口</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>提高连接率 (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>服务器报告信息</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>测试名称</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>工程师姓名</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技术员 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>客户名称*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>公司</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>电子邮件*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>电话</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>注释</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>显示测试 ID</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>跳过 TrueSpeed 测试</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>服务器未连接。</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>平均(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>等待测试资源准备就绪。您在等待列表中排在第 #%1 位。</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>窗口</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>饱和&#xA;窗口</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>窗口大小 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>上游诊断：</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>没有要报告的内容</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>吞吐量过低</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT 不一致</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>TCP 效率低</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>缓存延迟过高</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>吞吐量低于 CIR 的 85%</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU 少于 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>下游诊断：</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>验证码</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>验证创建日期</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>到期日期</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>修改时间</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>测试停止时间</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>最后修改</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>用户名</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>电子邮件</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>上游吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>实际对比目标</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>上游实际对比目标</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>上游总结</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>峰值 TCP 吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS（字节）</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>往返时间 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>上游总结结果（最大吞吐量窗口）</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>每次连接的窗口大小 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>聚合窗口 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>目标TCP吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>平均值 TCP 吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>TCP 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>目标</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>窗口 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>窗口 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>窗口 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>窗口 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>窗口 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>窗口 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>最大值吞吐量窗口：</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 kB 窗口：%2 连接 x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均值</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>上游吞吐量图表</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>上游详细信息</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>上游窗口 1 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>每次连接的窗口大小 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>实际吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>目标吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>总重传次数</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>上游窗口 2 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>上游窗口 3 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>上游窗口 4 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>上游窗口 5 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>上游窗口 6 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>上游窗口 7 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>上游窗口 8 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>上游窗口 9 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>上游窗口 10 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>上游饱和窗口吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>平均值吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>下游吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>下游实际对比目标</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>下游总结</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>下游总结结果（最大吞吐量窗口）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>下游吞吐量图表</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>下游详细信息</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>下游窗口 1 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>下游窗口 2 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>下游窗口 3 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>下游窗口 4 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>下游窗口 5 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>下游窗口 6 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>下游窗口 7 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>下游窗口 8 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>下游窗口 9 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>下游窗口 10 吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>下游饱和窗口吞吐量结果</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>过饱和窗口 (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>过饱和连接率 (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>VLAN 扫描</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 开始 VLAN 扫描测试 #2</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>对 VLAN ID #1 进行 #2 秒的测试</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1：通过</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1：失败</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>未成功形成有效回路。VLAN ID #1 测试失败</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>测试的 VLAN ID 总数</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>已通过的 ID 数</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>失败的 ID 数</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>每个 ID 持续时间 (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>范围数</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>选择的范围</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>VLAN ID 最小值</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID 最大值</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>开始测试</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>ID 总数</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>已通过的 ID</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>失败的 ID</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>高级 VLAN 扫描设置</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>带宽 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>通过标准</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>无帧丢失</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>接收到的一些帧</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>配置 TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>运行 TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>预计运行时间</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>故障时停止</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>查看报告</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>查看TrueSAM报告...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>放弃</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>完成</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>故障</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>已由用户停止</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>信号丢失.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>链路丢失.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>与远程测试设备的通信中断。 请重新建立通信信道并重试。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>配置本地测试设备时遇到问题，与远程测试设备的通信中断。 请重新建立通信信道并重试。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>配置远程测试设备时遇到问题，与测试设备的通信中断。请重新建立通信信道并重试。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>配置本地测试设备时遇到问题，TrueSAM即将退出。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>尝试配置本地测试设备时遇到问题。 测试被强制停止，与远程测试设备之间的通信信道中断。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>尝试配置远程测试设备时遇到问题。 测试被强制停止，与远程测试设备之间的通信信道中断。</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>为防止测试过程中发生干扰，已禁用屏保。</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>本地测试设备遇到问题。 TrueSAM即将退出。</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>请求 DHCP 参数</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>DHCP 获得参数。</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>正在初始化以太网接口。请等待。</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>北美</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>北美和韩国</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>北美 PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>印度</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>丢失了外部时间源的时间信号（ TOD ）， 1PPS 同步将显示为关闭。</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>开始与外部时间源的时间信号同步 ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>成功与外部时间源的时间信号同步。</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>环路 &#xA; 拆除</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>封面</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>选择要运行的测试</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>端到端流量连接测试</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>增强型 RFC 2544 / SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>以太网基准测试套装</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Y.1564以太网服务配置和性能测试</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>控制平面帧（如CDP,STP等）的第2层透明性测试。</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>RFC 6349 TCP吞吐量和性能测试</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>L3- 源类型</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>通信信道设置（使用服务 1 ）</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>通信信道设置</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>服务 1 必须配置与远程 Viavi 测试仪器上的流 1 一致。</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>本地状态</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>ToD 同步</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>1PPS 同步</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>连接&#xA;信道</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>物理层</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>暂停长度 ( 数量 )</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>暂停长度 ( 时间 - 毫秒 )</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>运行 &#xA; 测试</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>正在启动 &#xA; 测试</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>正在停止&#xA;测试</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>测试的预计执行时间</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>总计</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>故障时停止</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>所选测试的通过/失败条件</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>上游和下游</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>这里没有通过或失败的概念，因此该设置不适用。</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>若达到下列阈值则为通过：</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>未设置阈值 - 将不会报告通过/失败。</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>本地:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>远端:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>本地和远程：</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>吞吐量（ L2 Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>吞吐量（ L1 kbps ）</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>吞吐量（ L2 kbps ）</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>帧丢失容限 (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>延迟 (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>若满足下列SLA参数则为通过：</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>上行:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>下行:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>CIR 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR 吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>CIR 吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>EIR 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>EIR 吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>EIR 吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - 容差（Mbps）</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - 容差（ L1 Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - 容差（ L2 Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>帧延迟 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>延迟变化 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>通过/失败的结果将由内部决定。</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>若测量的TCP吞吐量达到下列阈值则为通过：</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>预计吞吐量的百分比</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>未启用TCP吞吐量测试 - 将不会报告通过/失败。</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>停止测试</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>这将停止当前运行的测试和后续测试执行。 确定要停止吗?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>无效配置 :&#xA;&#xA; 服务 #1 最大负载合计为 0 或超过 #2 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 服务 #1 上游方向最大负载总值为 0 或超出 #2 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 服务 #1 下游方向最大负载总值为 0 或超出 #2 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>无效配置 :&#xA;&#xA; 服务 #1 最大负载合计为 0 或超过相应的 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>无效配置： &#xA;&#xA; 服务 #1 上游方向最大负载总值为 0 或超出相应的 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>无效配置： &#xA;&#xA; 服务 #1 下游方向最大负载总值为 0 或超出相应的 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>无效配置 :&#xA;&#xA; 最大负载超过 #1 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 最大负载超出上游方向 #1 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 最大负载超出下游方向 #1 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>无效配置 :&#xA;&#xA; 最大负载超过 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 最大负载超出上游方向 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 最大负载超出下游方向 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>无效配置 :&#xA;&#xA;CIR 、 EIR 和策略不能是 0 。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 上行方向 CIR 、 EIR 和策略不能全为 0 。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>无效配置： &#xA;&#xA; 下行方向 CIR 、 EIR 和策略不能全为 0 。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>无效配置 :&#xA;&#xA; 服务 #3 不能使用 #2 Mbps CIR 设置实现最小阶跃负载（ CIR 的 #1% ）。使用服务 #3 的当前 CIR 的最小阶跃值为 #4% 。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>无效配置 :&#xA;&#xA;CIR (L1 Mbps) 合计为 0 或超过 #1 L1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 上游方向总 CIR (L1 Mbps) 为 0 或超出 #1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 下游方向总 CIR (L1 Mbps) 为 0 或超出 #1 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>无效配置 :&#xA;&#xA;CIR (L2 Mbps) 合计为 0 或超过 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 上游方向总 CIR (L2 Mbps) 为 0 或超出 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>无效配置： &#xA;&#xA; 下游方向总 CIR (L2 Mbps) 为 0 或超出 #1 L2 Mbps 线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>无效配置 :&#xA;&#xA; 必须选择业务配置测试或业务性能测试。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>无效配置 :&#xA;&#xA; 至少选择一个服务。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>无效配置：&#xA;&#xA;如果您希望进行服务性能测试，选定的服务 CIR 的合计总数（或 CIR 为 0 的服务的 EIR）不得超过线速率。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>无效配置 :&#xA;&#xA; 在进行服务性能测试时，指定的吞吐量测量（ RFC 2544 ）最大值不得小于选定服务的 CIR 值之和。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>无效配置： &#xA;&#xA; 运行服务性能测试时，指定的上行吞吐测量（ RFC 2544 ）最大值不能低于所选服务的 CIR 值总和。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>无效配置： &#xA;&#xA; 运行服务性能测试时，指定的下行吞吐测量（ RFC 2544 ）最大值不能低于所选服务的 CIR 值总和。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>无效配置 :&#xA;&#xA; 在进行服务性能测试时，指定的吞吐量测量（ RFC 2544 ）最大值不得小于 CIR 值。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>无效配置： &#xA;&#xA; 运行服务性能测试时，指定的上行吞吐测量（ RFC 2544 ）最大值不能低于 CIR 值。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>无效配置： &#xA;&#xA; 运行服务性能测试时，指定的下行吞吐测量（ RFC 2544 ）最大值不能低于 CIR 值。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>SAMComplete 已停止，因为已选择了故障时停止，并且至少一个 KPI 不满足业务 #1 的 SLA 。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete 已停止，因为在流量开始后 10 秒内，业务 #1 中没有返回数据。 远端可能不再将流量环回。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete 已停止，因为在流量开始后 10 秒内，服务 #1 中没有返回数据。远端可能不再发送流量。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete 已停止，因为在流量开始后 10 秒内，没有返回数据。远端可能不再将流量环回。</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete 已停止，因为在流量开始后 10 秒内，没有返回数据。远端可能不再发送流量。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>本地和远程应用程序不兼容。本地应用程序是数据流应用程序， IP 地址为 #1 的远程应用程序是流量应用程序。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>本地和远程应用不兼容。 本地应用是流媒体应用， IP 地址为 #1 的远程应用是 TCP WireSpeed 应用。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>本地和远程应用不兼容。 本地应用是第 3 层应用， IP 地址为 #1 的远程应用是第 2 层应用。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>本地和远程应用程序不兼容。本地应用程序是第 2 层应用程序， IP 地址为 #1 的远程应用程序是第 3 层应用程序。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>IP 地址 #1 的远程应用是针对 WAN IP 设置。与 SAMComplete 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>IP 地址为 #1 的远程应用为堆叠 VLAN 封装设定。 它与 SAMComplete 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>为 VPLS 封装设置 IP 地址为 #1 的远程应用程序。 与 SAMComplete 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>为 MPLS 封装设置 IP 地址为 #1 的远程应用程序。 与 SAMComplete 不兼容。</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>远程应用程序仅支持 #1 服务。 </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>“单向延迟”测试要求本地和远程测试装置均进行 OWD 时间源同步。本地装置的“单向延迟同步”失败。请检查“ OWD 时间源”硬件的所有连接。</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>“单向延迟”测试要求本地和远程测试装置均进行 OWD 时间源同步。远程装置的“单向延迟同步”失败。请检查“ OWD 时间源”硬件的所有连接。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>无效配置：运行 SAMComplete 测试之前必须测试往返时间（ RTT ）。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>无法创建 TrueSpeed 会话。 请转到“网络”页面，验证配置，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>无法与上行方向建立TrueSpeed会话。请转到“网络”页面，验证配置，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>无法与下行方向建立TrueSpeed会话。请转到“网络”页面，验证配置，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>更改当前所选的测试服务，会使往返时间（ RTT ）测试无效。 请转到“ TrueSpeed 控制”页面重新运行 RTT 测试。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>无效配置：&#xA;&#xA;服务#1下行方向总CIR (Mbps) 不能为0。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>无效配置：&#xA;&#xA;服务#1上行方向总CIR (Mbps) 不能为0。</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>无效配置：&#xA;&#xA;服务 #1 的 CIR (Mbps) 不能为 0。</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>未接收到流量。请转到“网络”页面，验证配置，然后重试。</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>主要结果视图</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>停止&#xA;测试</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>   已创建报告，请点击“下一步”进行查看</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>跳过报告显示</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete - Ethernet 业务激活测试</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>TrueSpeed 配对</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>本地网络设置</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>本地端不需要配置。</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>远程网络设置</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>远程端不需要配置。</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>本地IP设置</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>远程IP设置</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>服务</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>标签</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>未使用标签。</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>SLA 吞吐量</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>SLA 管制</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>没有选择管制测试。</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>SLA 突发</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>因为缺少所需功能，已禁用突发测试。</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>服务水平协议（ SLA ）表现</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed 当前被禁用。在非环回测量模式下， TrueSpeed 可在“网络”配置页启用。</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>本地高级设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>高级流量设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>高级 LBM 设置</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>高级 SLA 设置</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>随机/EMIX 大小</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>高级 IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 高级</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>高级标签</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>服务配置测试</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>业务性能测试</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>退出 Y.1564 测试</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>发现服务器状态</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>发现服务器报文</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>MAC 地址和 ARP 模式</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>SFP 选择</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>WAN 源 IP</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>静态 - WAN IP</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>WAN 网关</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>WAN 子网掩码</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>流量源 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>流量子网掩码</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>VLAN 标签将被用到本地网络端口上吗？</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q ( 堆叠 VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>等待</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>正在访问服务器 ...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>无法访问服务器</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>获取的 IP</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>授予的租赁期： #1 分钟</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>减少的租赁期： #1 分钟</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>释放的租赁期</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>发现服务器：</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR、#2 ms FTD、#3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>服务 1：VoIP — 流量的 25% — #1 和 #2 字节的帧</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>服务 2：视频电话 — 流量的 10% — #1 和 #2 字节的帧</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>服务 3：优先级数据 — 流量的 15% — #1 和 #2 字节的帧</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>服务 4：BE 数据 — 流量的 50% — #1 和 #2 字节的帧</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>所有业务</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>服务 1 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>语音</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>10GE 时 , 带宽间隔级别为 0.1 Mbps ；因此， CIR 值为此间隔级别的倍数</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>40GE 时 , 带宽间隔级别为 0.4 Mbps ；因此， CIR 值为此间隔级别的倍数</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>100GE 时 , 带宽间隔级别为 1 Mbps ；因此， CIR 值为此间隔级别的倍数</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>配置尺寸</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>帧长 (字节)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>定义长度</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>EMIX 循环长度</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>包长 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Calc. 帧长</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>使用数据包长度和封装计算出帧大小。</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>服务 2 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>服务 3 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>服务 4 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>服务 5 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>服务 6 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>服务 7 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>服务 8 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>服务 9 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>服务 10 三重播放 属性</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>小尺寸</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>业务数</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>层</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>LBM 设置</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>配置三重播放...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>业务属性</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC 和帧大小设置</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC、帧大小设置和以太网类型</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC 、数据包长度和 TTL 设置</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>DA MAC 和数据包长度设置</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>网络设置（高级）</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>本地/远程设备不兼容，要求 &lt;b>%1&lt;/b> 字节或更大的数据包。</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>业务</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>配置 ...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>用户尺寸</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (hops)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>目的 MAC 地址</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>显示两者</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>仅远程</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>仅本地</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>LBM 设置 ( 高级 )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>网络设置随机/EMIX 大小</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>服务 1 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>帧尺寸 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>数据包长度 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>用户人数 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>巨型帧尺寸 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>帧尺寸 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>数据包长度 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>用户人数 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>巨型帧尺寸 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>帧尺寸 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>数据包长度 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>用户人数 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>巨型帧尺寸 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>帧尺寸 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>数据包长度 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>用户人数 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>巨型帧尺寸 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>帧尺寸 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>数据包长度 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>用户人数 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>巨型帧尺寸 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>帧尺寸 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>数据包长度 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>用户人数 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>巨型帧尺寸 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>帧尺寸 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>数据包长度 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>用户人数 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>巨型帧尺寸 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>帧尺寸 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>数据包长度 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>用户人数 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>巨型帧尺寸 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>服务 1 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>服务 1 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>服务 2 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>服务 2 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>服务 2 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>服务 3 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>服务 3 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>服务 3 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>服务 4 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>服务 4 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>服务 4 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>服务 5 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>服务 5 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>服务 5 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>服务 6 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>服务 6 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>服务 6 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>服务 7 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>服务 7 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>服务 7 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>服务 8 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>服务 8 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>服务 8 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>服务 9 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>服务 9 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>服务 9 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>服务 10 随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>服务 10 随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>服务 10 随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>各服务的 VLAN ID 或用户优先级不同吗？</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>否，所有服务使用相同的 VLAN 设置</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI 比特</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>高级设置 ...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>用户优先级</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>SVLAN&#xA; 优先级</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>VLAN 标签（封装）设置</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>SVLAN 优先级</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>所有服务使用相同的流量目标 IP。</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>流量目标 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>目的 IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>流量目标IP</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>设置 IP ID 增值</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>IP 设置（仅本地）</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>IP 设置</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>IP 设置（仅远程）</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>IP 设置 ( 高级 )</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>高级 IP 设置</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>各服务的 TOS 或 DSCP 设置不同吗？</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>否，所有服务的 TOS/DSCP 相同</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>聚合 SLA</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>聚合 CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>上游聚合 CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>下游聚合 CIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>聚合 EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>上游聚合 EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>下游聚合 EIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>聚合模式</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>吞吐量测试本地最大 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>吞吐量测试远程最大 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>启用聚合模式</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>警告：当前所选权重值的和为 &lt;b>%1&lt;/b>%，不是 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>服务水平协议（ SLA ）吞吐量， Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA 吞吐量 , L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>SLA 吞吐量 , L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>SLA 吞吐量 , L1 Mbps ( 单向 )</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA 吞吐量 , L2 Mbps ( 单向 )</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>权重 (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>策略</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>最大负载</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>仅下行</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>仅上行</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>设置高级流量设置</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>服务水平协议（ SLA ）策略， Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>最高策略</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>执行突发测试</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>容差 (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>容差 (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>您希望执行突发测试吗？</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>突发测试类型：</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>设置高级突发设置</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>帧延迟精度</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>包含作为 SLA 要求的帧延迟</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>包含作为 SLA 要求的帧延迟变更</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>服务水平协议（ SLA ）表现（单向）</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>帧延迟(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>帧延迟(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>设置高级 SLA 设置</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>业务配置</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>低于 CIR 的步骤数</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>步骤持续时间（秒）</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>阶跃 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>阶跃 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>阶跃 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>阶跃 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>阶跃 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>阶跃 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>阶跃 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>阶跃 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>阶跃 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>阶跃 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>步骤百分比</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>业务性能</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>各方向分别测试，这样整体测试持续时间将是输入值的两倍。</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>故障时停止测试</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>高级突发测试设置</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>跳过 J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>选择 Y.1564 测试</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>选择要测试的业务</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>设置所有</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  总计:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>业务配置测试</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>业务性能测试</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>可选测量</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>吞吐量（ RFC 2544 ）</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>最大 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>最大 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>最大 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>当“ TrueSpeed ”服务启动时，无法进行可选测量。</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>运行 Y.1564 测试</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>跳过 Y.1564 测试</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>延迟</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>延迟变化</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>测试失败摘要</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>定论</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Y.1564 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>配置测试定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>配置测试 Svc 1 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>配置测试 Svc 2 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>配置测试 Svc 3 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>配置测试 Svc 4 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>配置测试 Svc 5 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>配置测试 Svc 6 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>配置测试 Svc 7 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>配置测试 Svc 8 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>配置测试 Svc 9 定论</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>配置测试 Svc 10 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>性能测试定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>性能测试 Svc 1 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>性能测试 Svc 2 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>性能测试 Svc 3 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>性能测试 Svc 4 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>性能测试 Svc 5 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>性能测试 Svc 6 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>性能测试 Svc 7 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>性能测试 Svc 8 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>性能测试 Svc 9 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>性能测试 Svc 10 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>性能测试 IR 定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>性能测试丢帧定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>性能测试延迟定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>性能测试延迟变化定论</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>性能测试TrueSpeed定论</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>业务配置结果</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>业务 1 配置结果</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>最大吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>下游最大吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>上游最大吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>最大吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>下游最大吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>上游最大吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>CIR 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>下游 CIR 定论</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>帧延迟变化 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>OoS 统计</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>错误帧检测</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>暂停检测</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>上游 CIR 定论</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>CBS 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>下游 CBS 定论</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>配置的突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>发送端突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>平均 Rx 突发尺寸 (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>预计 CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>上游 CBS 定论</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>EIR 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>下游 EIR 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>上游 EIR 定论</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>策略定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>下游策略定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>上游策略定论</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>步骤 1 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>下游步骤 1 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>上游步骤 1 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>步骤 2 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>下游步骤 2 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>上游步骤 2 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>步骤 3 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>下游步骤 3 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>上游步骤 3 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>步骤 4 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>下游步骤 4 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>上游步骤 4 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>步骤 5 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>下游步骤 5 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>上游步骤 5 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>步骤 6 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>下游步骤 6 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>上游步骤 6 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>步骤 7 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>下游步骤 7 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>上游步骤 7 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>步骤 8 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>下游步骤 8 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>上游步骤 8 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>步骤 9 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>下游步骤 9 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>上游步骤 9 定论</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>步骤 10 定论</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>下游步骤 10 定论</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>上游步骤 10 定论</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>最大吞吐量 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>最大吞吐量 (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>最大吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>步骤</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>突发</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>键</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>单击这些栏查看每个步骤的结果。</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR（吞吐量Mbps）</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR（吞吐量 L1 Mbps）</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR（吞吐量 L2 Mbps）</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>错误检测</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>1 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>2 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>3 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>4 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>5 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>6 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>7 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>8 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>9 步长</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>10 步长</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>SLA 门限</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>业务 2 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>业务 3 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>业务 4 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>业务 5 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>业务 6 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>业务 7 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>业务 8 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>业务 9 配置结果</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>业务 10 配置结果</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>业务性能结果</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Svc. 定论</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR 当前的</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR 最大值</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR 最小值</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR 平均值</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>丢帧秒</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>帧丢失计数</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>延迟当前</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>延迟最大</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>延迟分钟</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>延迟平均</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>延迟变量当前</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>延迟变量最大</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>延迟变量最小</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>延迟变量平均</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>可用性</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>可获得的</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>可用秒</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>无效秒</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>严重错误秒</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>业务性能吞吐量</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>概观</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>延迟变化</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>TrueSpeed 服务。 在 TrueSpeed 结果页面查看结果。</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR 平均值&#xA;（吞吐量&#xA;L1 Mbps）</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR 平均值&#xA;（吞吐量&#xA;L2 Mbps）</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>丢帧率</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>单向延迟平均&#xA;(毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>延迟变化 &#xA; 最大 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>检测到错误</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>延迟平均&#xA;(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR 电流 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR 电流 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR 最大值 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR 最大值 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR 最小值 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR 最小值 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>帧丢失计数</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>丢帧率阈值</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>延迟当前(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>延迟最大&#xA;(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>延迟最小&#xA;(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>延迟平均&#xA;(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>延迟阈值&#xA;(OWD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>延迟当前&#xA;(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>延迟最大&#xA;(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>延迟最小&#xA;(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>延迟阈值&#xA;(RTD ，毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>延迟变量当前&#xA;(毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>延迟变化平均值&#xA;(毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>延迟变化 &#xA; 阈值 (毫秒)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>可用秒率</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>无效秒</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>严重错误秒</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>服务配置 &#xA; 吞吐量 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>服务配置 &#xA; 吞吐量 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>服务性能 &#xA; 吞吐量 &#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>服务性能 &#xA; 吞吐量 &#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>网络设置</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>用户帧长</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>巨帧长</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>用户数据包长度</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>巨帧长度</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>预测的帧长 (字节)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>网络设置（随机/EMIX 大小）</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>随机/EMIX 长度（远程）</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>随机/EMIX 长度（本地）</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>随机/EMIX 长度</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>高级 IP 设置 - 远程</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>下行 CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>下行 EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>下行管制</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>下行 M</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>上行 CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>上行 EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>上行策略</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>上行 M</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA 高级突发</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>下行丢帧率</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>下行帧延迟（ OWD ， ms ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>下游帧延迟 (RTD ， ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>下行延迟变更（ ms ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>上行丢帧率</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>上行帧延迟（ OWD ， ms ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>上游帧延迟 (RTD ， ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>上行延迟变化（ ms ）</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>包含 SLA 要求</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>包含帧延迟</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>包含帧延迟变更</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete的下列配置设置无效：</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>业务配置结果</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>运行 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>注意：TrueSpeed测试要求先启用服务性能测试。</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>设置数据包长度 TTL</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>设置 TCP/UDP 端口</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>源目标 类型</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Src. 端口</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>目标类型</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>目标端口</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>TCP 吞吐量阈值 (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>推荐总窗口宽度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>提高总窗口宽度（字节）</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>推荐 # 连接</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>提高连接数</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>上游推荐总窗口宽度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>上游提高总窗口宽度（字节）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>上游推荐 # 连接</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>上游提高连接数</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>下游推荐总窗口宽度 ( 字节 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>下游提高总窗口宽度（字节）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>下游推荐 # 连接</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>下游提高连接数</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>推荐 # 的连接</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>TCP 吞吐量（ % ）</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Acterna 净荷</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>往返时间 (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>RTT 将被用在后续步骤中，推荐窗口大小和连接数。</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>TrueSpeed 结果</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>TCP 传输度量</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>目标 L4 （ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>TrueSpeed 服务仲裁</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>上行TrueSpeed 服务裁定</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>实际TCP吞吐量 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>上行实际 TCP 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>上行目标 TCP 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>目标 TCP 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>下行TrueSpeed 服务裁定</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>下行实际 TCP 吞吐量（ Mbps ）</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>下行目标 TCP 吞吐量（ Mbps ）</translation>
        </message>
    </context>
</TS>
