<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation>Êtes-vous sur de vouloir annuler ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Il y a eu une erreur lors de la lecture du fichier</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>Le fichier contient trop de trames (plus de 50 000). Essayez de sauvegarder un buffer partiel,&#xA;ou exportez le sur clé USB pour le charger sur un équipement différent</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>Voulez-vous effacer toutes les données stockées?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>Voulez-vous supprimer l'élément sélectionné?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Le nom existe déjà.&#xA;Voulez-vous écraser les anciennes données?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>L'application graphique est en erreur pendant l'écriture sur le disque. Prière de sauvegarder vos graphiques maintenant pour conserver&#xA;votre travail. Les graphiques s'arrêteront et s'effaceront automatiquement dès que le niveau critique est atteint.</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>L'application graphique a eu trop d'erreurs de disque pour continuer et elle sera arrêtée.&#xA;Les données de graphiques sont effacées. Vous pouvez recommencer a tracer des graphiques si vous voulez.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Dispositif démontable</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>Log est pleine</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Configuration invalide :&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Invalide</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voix</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Données 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Données 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Révision de spécification CFP MSA Mgmt I/F prise en charge par SW</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Révision de spécification QSFP MSA Mgmt I/F prise en charge par SW</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>Info DMC</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Code barre</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Date de fabrication</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Date d'étalonnage</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Version Logicielle</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>ID Challenge Option</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Numéro de Série du Châssis</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Code barres de l'ensemble</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Logiciel installé</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Options:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Attend paquets...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Analyse des paquets...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>Attend lien...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>Appuyez sur "démarrer l'analyse" pour commencer...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Sauvegarde des résultats en cours, veuillez patienter.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossible d'enregistrer les résultats.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Les résultats sont sauvegardés.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>En attendant le démarrage</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>Le test précédent a été interrompu par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>Le test précédent a été interrompu avec erreur</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>Le test précédent a échoué et l'arrêt sur échec a été activé</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non exécuté</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Temps restant: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>Le test n'a pas pu se terminer et a été interrompu avec erreur</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Test complété</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Test complété avec des résultats défaillants</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Succès</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Test complété avec tous les résultats corrects</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrêté par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>Le test a été interrompu par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Mode d'arrêt</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Pas de test en cours</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Commençant</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Connexion vérifiée</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Connexion perdue</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Vérification de la connexion</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuration invalide</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Sauvegarde des résultats en cours, veuillez patienter.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossible d'enregistrer les résultats.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Les résultats sont sauvegardés.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Graphique de test de débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Résultats du test de "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Graphique de test de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Résultats de test de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Graphique de test de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Résultats de test de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalies</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Anomalies de flux en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Anomalies de flux en aval</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Graphique de test de latence</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Résultats de test de latence</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Graphique de test de latence en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Résultats de test de latence en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Graphique de test de latence en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Résultats de test de latence en aval</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Graphique de test de gigue</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Résultats de test de gigue</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Graphique de test de gigue en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Résultast de test de gigue en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Graphique de test de gigue en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Résultast de test de gigue en aval</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>Résultats de  test de perte de trame %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame en amont %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en amont %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame en aval %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en aval %1 octet</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Résultats de test CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Résultats de test CBS en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Résultats de test CBS en aval</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS en aval</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche salve</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche de salve en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche de salve en aval</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Résultats de Test Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Retour en amont vers resultats de test de retour</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Retour en aval vers resultats de test de retour</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Graphique test de Recovery System (système de recouvrement)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Résultats du test de récupération du système</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Graphique de test de Récupération système en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Résultats de test de récupération système en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Graphique de test de Récupération système en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Résultats de test de récupération système en aval</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Rsultats du test de charge étendue</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Etape de chemin MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>Etape RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Etape de parcours de fenêtre ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Etape parcours de fenêtre descendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Etape de débit TCP ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Etape débit TCP descendant</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Sauvegarde des résultats en cours, veuillez patienter.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossible d'enregistrer les résultats.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Les résultats sont sauvegardés.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossible d'obtenir la synchronisation. Veuillez vous assurer que tous les câbles sont raccordés, et que le port est correctement configuré.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossible de trouver un lien actif. Veuillez vous assurer que tous les câbles sont raccordés, et que le port est configuré correctement.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossible de déterminer la vitesse ou le duplex du port connecté. Veuillez vous assurer que tous les câbles sont raccordés, et que le port est configuré correctement.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Impossible d'obtenir une adresse IP pour l'ensemble de test local. Veuillez vérifier vos paramètres de communication et essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>Incapable d'établir un canal de communication au lot de test distant. Veuillez vérifier vos paramètres de communication et essayez à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>Le testeur distant ne semble pas avoir une version compatible du logiciel BERT. La version minimum compatible est %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>L'application TCP de couche 4 Wirespeed doit être disponible à la fois sur l'unité locale et l'unité distante afin d'exécuter TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>Le choix du test TrueSpeed ??n'est pas valide pour ce paramètre d'encapsulation et sera supprimé.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>La sélection du test TrueSpeed n'est pas valide pour ce réglage de type de trame et il sera supprimé.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>Le choix du test TrueSpeed ??n'est pas valable pour l'encapsulation à distance et sera supprimé.</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>La sélection du test SAM-Complete n'est pas valide pour ce paramètre d'encapsulation et il sera supprimé.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuration invalide</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>Contrôle(policing) de CBS</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Contrôle</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Etape #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Etape #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Etape #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Etape #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Etape #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Etape #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Etape #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Etape #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Etape #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Etape #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Sauvegarde des résultats en cours, veuillez patienter.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossible d'enregistrer les résultats.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Les résultats sont sauvegardés.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>ARP local a échoué.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>Échec de l'ARP distant.</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Table, cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Screenshot</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - L'histogramme est réparti sur plusieurs pages</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Echelle de temps</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>Pas disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Info utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Groupes de configuration</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Groupes de résultat</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Log d'événements</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Histogrammes</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Copie d'écran</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Débit TCP Estimé</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation>Rapports de tests multiples</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Rapport de Test</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Généré par Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Généré par Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Généré par Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Généré par l'instrument de test de Viavi </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> OK</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Fréquence de scan</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>OK / Echec</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Configuration : Filtres /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation>(Séquence et masque)</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Séquence</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Masque</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Setup pas applicable...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Radio</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Bande</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Résultat  de test global de CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incomplet</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>En cours</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>Actif</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Non.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Nom de l' événement</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Durée/Valeur</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Journal d'évènement de test de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Inconnu - Etat</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>En cours. Patientez...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Echec!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>Commande Terminé !</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>Abandonné!</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Boucle active</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Boucle inactive</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Arm</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Désarmé</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Batterie basse</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Envoyer une commande de boucle</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Switch</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Remise à zéro du commutateur</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Issue Query</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Demande de boucle</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Near End Arm</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Near End Disarm</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Demande de Puissance</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Span Query</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>dévalidé</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>remis à jour</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Boucle séquentielle</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Stratum 1 Trace</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Réservé</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Réservé</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Transit Node Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Réservé</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Réservé</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Stratum 2 Trace</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Réservé</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Réservé</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Stratum 3 Trace</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Réservé</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet Min Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Stratum 3E Trace</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provision by Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Don't Use for Synchronization</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Occupé, non-spécifique</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>Structure TUG</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>TU Fermé</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>34M/45M Asynch.</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>140M Asynch.</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>Mapping ATM</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>Mapping MAN (DQDB)</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>Mapping FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>Mapping HDLC/PPP</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 unscrambled</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>Signal de test O.181</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-SIA</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Asynchrone</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Synchrone bit</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Synchrone octet</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Réservé</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT-Structured STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Locked VT mode</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Mapping DS3 Asynch.</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Mapping DS4NA Asynch.</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Mapping FDDI Asynch.</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>Défaut de la payload 1 VT</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>Défauts de payload 2 VT</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>Défauts de payload 3 VT</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>Défauts de payload 4 VT</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>Défauts de payload 5 VT</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>Défauts de payload 6 VT</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>Défauts de payload 7 VT</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>Défauts de payload 8 VT</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>Défauts de payload 9 VT</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>Défauts de payload 10 VT</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>Défauts de payload 11 VT</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>Défauts de payload 12 VT</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>Défauts de payload 13 VT</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>Défauts de payload 14 VT</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>Défauts de payload 15 VT</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>Défauts de payload 16 VT</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>Défauts de payload 17 VT</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>Défauts de payload 18 VT</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>Défauts de payload 19 VT</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>Défauts de payload 20 VT</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>Défauts de payload 21 VT</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>Défauts de payload 22 VT</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>Défauts de payload 23 VT</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>Défauts de payload 24 VT</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>Défauts de payload 25 VT</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>Défauts de payload 26 VT</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>Défauts de payload 27 VT</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>Défauts de payload 28 VT</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Format?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Hor plage</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>HISTORIQUE</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + HISTORIQUE</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Espace</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Signe</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>VERT</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>JAUNE</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>ROUGE</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>AUCUN</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>TOUT</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>REJETÉ</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>Incertain</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ACCEPTÉ</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>INCONNU</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ECHEC</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incomplet</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Statut inconnu</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Identifié</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>Ne peut pas identifier</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Indentité inconnue</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>%1 heures et %2 minutes restantes</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>%1 minutes restantes</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>TROP BASSE</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>TROP ÉLEVÉE</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>(TROP BASSE) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>(TROP ÉLEVÉE) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Octet</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valeur</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Port Affluent</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>Non définie</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non occupé</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Mapping sous Développement</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC sur SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Simple Data Link Mapping (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>Mapping HCLC/LAP-S</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Simple Data Link Mapping (set-reset)</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>Mapping 10 Gbps Ethernet</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>Mapping GFP</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>Mapping 10 Gbps Fiber Channel</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Réservé - Propriétaire</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Réservé - National</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Signal de test O.181</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Réservé (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Occupé, non-spécifique</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Mapping DS3 Asynchrone</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Mapping DS4NA Asynchrone</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Mapping FDDI Asynchrone</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC sur SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>Défaut de la payload %1 VT</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI non affecté.</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Attente TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Est. attente TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI Attribue</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Attente est.</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Attente réal.</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Tames mult. est.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Rétablissement de temporisateur</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Lien inconnu</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>ATTENTE ETABLISSEMENT</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MULTITRAMES ETABLIES</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>TONALITÉDE NUMÉROTATION</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>NUMÉROTATION ENBLOCK</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>SONNERIE</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>CONNECTÉ</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>RELÂCHEMENT D'APPEL</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Parole</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Données</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletex</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Vidéotexte</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>Parler à BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Données BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Données 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Recherche</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Demande d'inscription</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Demande de tentative</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Congé</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Rafale ack complète</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Réponse à l'inscription</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Rafale complète</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Réponse de statut</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Connaître le trou dans le flux de données</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erreur</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Err: Le service n'est pas encore disponible</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Err: La demande de renouvellement de paquet d'essai est invalide</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Err: Pas de tel service</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Err: Pas de telle section</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Err: Erreur de session</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Err: Version de commande et de contrôle non supportée</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Err: Serveur saturé</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Err: Combinaison (join) en double</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Err: Identificateur de session en double</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Err: Mauvais débit</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Err: Session détruite par le server</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>Court-circuit</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>Circuit Ouvert</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>normal</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>inversé</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Niveau trop bas</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 octets</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 Octets</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Sélectionné</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Non sélectionné</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>DÉBUT</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>ARRÊTER</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Trames Erronées</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Trames OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Perte de Trames</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violation de Code</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>Le journal des événements est plein</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>Aucune clé USB trouvée. Veuillez insérer un disque et essayez à nouveau. &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>Aide non disponible pour cet élément</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>ID d'unité</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>Pas de signal</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Signal</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Prêt</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Utilisé</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>ID de satellite</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>ID GNSS</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Masque</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>Masque MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>Masque TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>Résultats MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>Masque MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>Résultats TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>Masque TDEV</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Données original de TIE</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Données de la courbe dans l'Offset</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>Style de courbe MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Ligne + points</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Uniquement les points</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Uniquement MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Uniquement TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Type du masque</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE Passé</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>Echec MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV Passé</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>Echec TDEV</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Intervalle d'observation (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Calcul en cours </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Calcul annulé</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Calcul terminé</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>Mise à jour en cours données TIE </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>TIE données chargées</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>Pas de données TIE chargées</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Mémoire insuffisante pour lancer l'analyse Wander localement.&#xA;256MB de RAM nécéssaire. Utiliser le logiciel d'analyse externe.&#xA;Voir le manuel pour les détails.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Offset de Fréq (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Taux de dév (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Echantillons</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Taux d'échan. (par sec)</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Blocs</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Bloc courant</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Supprimer l'Offset</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Sélection de la courbe</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Les deux courbes</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>sans l'offset</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Capture d'écran</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Heure (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Distant</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Non-étiqueté</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Port 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Port 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Barre d'outils</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Enregistrer un message</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Info. Générale :</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Graphiques Désactivés</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Erreur d'impression !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation># Prog</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps Min</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps Max</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Gigue PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Gigue PCR Max</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err CC Tot.</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Max</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err PMT Tot.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Max</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err PID Tot.</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Flux</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errs Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errs Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID Flux Transport</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Present</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Perte Pqt Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perte Pqt Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Perte Pqt Pic</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Gigue Pqt (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Gigue Pqt Max (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Pqts OoS Tot.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Pqts OoS Act</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Pqts OoS Max</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Err Dist Tot</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Err Dist Act</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Max Err Dist</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Err Période Tot</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err Période Act</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Err Période Max</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Période de Perte Max</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Dist Perte Min</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Pertes de Sync Tot</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Err Octet Sync Tot</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err Octet Sync Act</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err Octet Sync Max</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>DF MDI Act</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MLR MDI Act</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Err Transp Tot</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err Transp Act</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err Transp Max</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err PAT Tot.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err PAT Act</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># Flux analysés</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Mbps C1 Total</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errs Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errs Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID Flux Transport</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Num de Prog</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Prog Mbps Act</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Prog Mbps Min</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Prog Mbps Max</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Present</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Perte Pqt Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perte Pqt Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Perte Pqt Pic</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Gigue Pqt (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Gigue Pqt Max (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>Total pqt OoS</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Pqts OoS Act</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Pqts OoS Max</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Err Dist Tot</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Err Dist Act</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Max Err Dist</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Err Période Tot</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err Période Act</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Err Période Max</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Période de Perte Max</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Dist Perte Min </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Pertes de Sync Tot</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Err Octet Sync Tot</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err Octet Sync Act</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err Octet Sync Max</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>DF MDI Act</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MLR MDI Act</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>Gigue PCR Act</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Gigue PCR Max</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err CC Tot.</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>Err CC Act</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Max</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Err Transp Tot</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err Transp Act</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err Transp Max</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err PAT Tot.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err PAT Act</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Max</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err PMT Tot.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>Err PMT Act</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Max</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err PID Tot.</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>Err PID Act</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>Adr IP</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perte de paq.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Perte Pqt Max</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Gig Pqt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Gig Pqt Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Flux</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Mbps Rx, Actuel C1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Prog Mbps Min </translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>ID Transport</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation># Prog</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Pertes de Sync</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Tot Err Octet Sync</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err Octet Sync</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err Octet Sync Max</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Tot Err PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>Err PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Err PAT Max</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Gigue PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>Gigue PCR Max </translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Total Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Err CC Max</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Tot Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Err PMT Max</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Tot Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Err PID Max</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Tot Err Transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Err Transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err Transp Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># Flux analysés</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Mbps C1 Total</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errs Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errs Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>Historique MPEG</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Present</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perte Pqt Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Perte Pqt Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Perte Pqt Pic</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Gigue Pqt Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Gigue Pqt Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pic-to-Pic</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pic-Pos</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pic-Neg</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Pic-to-Pic</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Pic-Pos</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Pic-Neg</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Information de rapport du test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Code</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Path</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Voie</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>inutilisé</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Résultat de test global d'optiques</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Auto-test d'optiques</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configs :</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Détails</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Empilé</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLANs empilés</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profondeur de la pile VLAN</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>Bit DEI du %1 SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>Bits de priorité %1 SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>TPID %1 SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>TPID %1 SVLAN (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>Aucune trame n'a été définie</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Octet</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valeur</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Port Affluent</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Résultat de test global OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incomplet</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>Capture d'octet POH</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hexa.</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binaire</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configs :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Setup pas applicable...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Octets de l'entête</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>Recepteur CDMA</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Erreur d'impression !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP Test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Profil chargé</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>Résultat global du test PTP</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incomplet</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Résumé</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>RESUME GLOBAL RESULTATS OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Test FC amélioré</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Amélioration de test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Résultats de Test général</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Mode</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Résultats de test global</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latence</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Récupération du système</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrêté par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu avec erreur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>N'est pas exécuté  le test précédent a échoué et l'arrêt sur échec a été activé</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non exécuté</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Bouclage symétrique</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>En amont et en aval symétrique</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Aval et amont asymmétriques</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Descendant</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latence</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Gigue Paquet</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Salve (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Récupération du système</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Tests à lancer</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Des longueurs de trame sélectionné ( octets )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Longueurs de paquet sélectionnées (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Longueurs sélectionnées (octets) de trame en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Longueurs sélectionnées de paquet en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Longueurs sélectionnées (octets) de trame en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Longueurs sélectionnées de paquet en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Longueurs (octets) de trame en aval</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame en amont %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>Graphique de test de perte de trame en aval %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>Graphique de test de débit de crédit de buffer %1 octet</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Durée (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Plus longue</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Plus court</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Dernier</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Moyen</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interruption</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config :</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Connecteur</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Longueur d'onde nominale (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Débit Nominal (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Longueur d'Onde (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Débit Minimum (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Débit Maximum (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Type de Niveau de Puissance</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Niveau Rx Max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Niveau Tx Max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN du vendeur</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Code des date</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Code du lot</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Surveillance du Diagnostique</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Octet de Diagnostique</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Émetteur</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation># de version matériel/logiciel</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation># Rev Spec. matériel MSA</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation># Rev I/F Gestion MSA </translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Classe de puissance</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Type de Niveau de Puissance Rx</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Puissance lambda max (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation># de fibres actives</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Longueurs d'onde par fibre</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL par plage de fibre (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Débit binaire max de la voie réseau (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>débits supportés</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durée</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Invalide</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Délai de trame (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Délai de trame (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Délai dans un sens</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Descendant</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Ligne</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Mise en service</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Dépannage</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Mode</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symétrique</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymétrique</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Symétrie du débit</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Path MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Largeur de Bande</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avancé</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Étapes à exécuter</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrêté par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu avec erreur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>N'est pas exécuté  le test précédent a échoué et l'arrêt sur échec a été activé</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non exécuté</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF Test</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incomplet</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>Le test a été annulé par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>Le test n'a pas démarré.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Succès de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>Le seuil est supérieure à 90% de la cible</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Echec de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>Le seuil est inférieur à 90% de la cible</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Succès de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Echec de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Information de rapport du test</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nom du technicien</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Entreprise</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Courriel</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Téléphone</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identification de test</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nom du test</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Code d'authentification</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Date de création d'authentification</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Heure d'arrêt du test</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commentaires</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Séquence</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>IP:Port Flux</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nom du Flux</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nom Prog</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>En cours</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>Résultat de test global de scan VLAN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config :</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Résultats :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>Résultat global du test TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>Le test n'a pas pu se terminer et a été interrompu avec erreur. Les résultats dans le rapport pourraient être incomplets.</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>Le test a été interrompu par l'utilisateur. Les résultats dans le rapport pourraient être incomplets.</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Résultats de sous-test</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>Résultats du test J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>Résultat du test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>Résultats de test de SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>Résultats du test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>Résultats du test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrêté par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>N'est pas exécuté  le test précédent a été interrompu avec erreur</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>N'est pas exécuté  le test précédent a échoué et l'arrêt sur échec a été activé</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non exécuté</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Enregistrer un message</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Journal des messages (suite )</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Localisation du Test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Commande de travail</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commentaires/Remarques</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numéro de série</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Version logicielle</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>Résultat global du test SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplet</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Démarrer la date</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fin de date</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Résultats globaux des tests de configuration</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Résultats globaux des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variation de délai</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Groupes de rapport</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>La rapport n'a pas pu être créé : </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>espace disque insuffisant.</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Le rapport ne peut pas être créé</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Paramètres du Port</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Impaire</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Paire</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Débit Baud</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Parité</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Contrôle de Flux</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Inactif</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Matériel</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Bits données</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Bits d'arrêt</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Paramètres du Terminal</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Entrée/Retour</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Echo Local</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Activer les Touches Réservées</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Touches Désactivées</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Actif</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Désactivé</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Activer</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>EXPORT, FICHIER, RÉGLAGE, RÉSULTATS, SCRIPT, DÉMARRAGE/ARRÊT, Touches programmables du panneau</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>IMPRIMER, FICHIER, CONFIG, RÉSULTATS, SCRIPT, DÉMARRAGE/ARRÊT, touches de raccourci de l'écran</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FICHIER, CONFIG, RÉSULTATS, SCRIPT, DÉMARRAGE/ARRÊT, touches de raccourci de l'écran</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>Fichier, Mise , les résultats, EXPORT , Start / Stop , Panneau de Touches</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Terminal&#xA;Fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Réstituer&#xA;paramètres par défaut</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100&#xA;Config</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Effacer&#xA;Ecran</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Afficher&#xA;Clavier</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Déplacer&#xA;Clavier</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>Autobaud</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturer&#xA;Ecran</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Cacher&#xA;Clavier</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Boucle en cours</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Résultat :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Erreur inattendue rencontrée</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Lancer&#xA;le Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;Ceci balayera le lien pour des flux et recommencera le test.&#xA;&#xA;Vous ne verrez plus que les flux qui ont été&#xA;transférés de l'application Explorer.&#xA;&#xA;Continuer ?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Auto en cours :</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Détail :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Auto en cours. Patientez...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Auto échoué.</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Auto Terminé.</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Auto abandonné.</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Inconnu - Etat</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Détecter </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Scan en cours...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Erreur inattendue rencontrée</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Détails de périphérique associé</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Envoyer fichier</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connecte</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Oubliez</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Sélectionnez fichier</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Emettre</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Connexion en cours...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Déconnexion en cours…</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Déconnecte</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Nom de paquet : </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Entrez un nom d' ensemble :</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Certificats et clés (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Nom de paquet invalide</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Un paquet avec le nom "%1" existe déjà.&#xA;Veuillez sélectionner un autre nom.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Ajouter fichiers</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Créer paquet</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Renommer paquet</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Modifier paquet</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Ouvrir Dossier</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Ajouter nouveau paquet...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Ajouter certificats à %1...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Supprimer %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>Supprimer %1 de %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>ID de voie virtuelle</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>obliquité Injectée (Bits)</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Obliquité Injectée (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>Voie physique #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Gamme:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>Incapable de fixer la date</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Chiffres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Slot tributaire</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Appliquer</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Défaut</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Bande passante</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Les modifications ne sont pas encore appliquées.</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>Trop de slots tributaires sont sélectionnés.</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>Trop peu de slots tributaires sont sélectionnées.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Autre...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caractères</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Chiffres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Aujourd'hui</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Entrer la date : jj/mm/aaaa</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Entrer la date : mm/jj/aaaa</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Octets</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Jusqu'à </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> Octets</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Chiffres</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Chiffres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Secondes</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Minutes</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Heures</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Jours</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>jj / hh : mn</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh : mn</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mn : ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Durée: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Configurer de l'Adresse Destination sur l'onglet Tous Services.</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Configurer de l'Adresse Destination sur l'onglet Tous les Flux.</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC Destination</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Type Destination</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Type de boucle</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>This Hop Source IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>Next Hop Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Next Hop Subnet Mask</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Entrer l'adresse source dans le tableau Ethernet pour toutes les trames</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Type de Source</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC par défaut</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Configurer l'Adresse Source sur l'onglet Tous Services.</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Configurer l'Adresse Source sur l'onglet Tous les Flux.</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Source</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Bits de priorité SVLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>TPID SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incrément du Pbit</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID SVLAN (hex) utilisateur </translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>Ne peut être configuré en le mode boucle.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>Spécifier ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Bits de priorité Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Longueur</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Longueur de données (Oct.)</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Champs Control</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Type/&#xA;Longueur</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Label&#xA;Tunnel</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>B-Tag Filtre VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>Priorité B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>Bit DEI du B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Label Tunnel</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Priorité du Tunnel</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>TTL du Tunnel</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>Label&#xA;VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>Priorité I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>Bit DEI du I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>I-Tag UCA Bit</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>I-Tag Filtre Service ID</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag Service ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>Label VC</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>Priorité du VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>Label&#xA;MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>Label MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>Priorité MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>Label&#xA;MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>Label MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>Priorité MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Données</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Frame client transportée</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulation</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Type de Trame</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Taille de trame utilisateur</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Taille de trame, Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Taille trop grande</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Taille Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>La section de données contient un paquet IP. Configurer ce paquet dans le tab IP.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Configurer le filtrage des données sur l'onglet Filtre IP</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Payload Tx</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>Configuration du RTD</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Analyse de la Payload</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Payload Rx</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>Timeur LPAC</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Séquence BERT Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Séquence BERT Tx</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Séquence Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Configurer les trames entrantes :</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Configurer les trames sortants :</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>Le champs Longueur/Type est 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>La longueur de trame est aléatoire</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Index de départ de "User Priority"</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Type :</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nom de fichier :</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Etes vous sûr de vouloir effacer&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Etes-vous certain de vouloir supprimer tous les fichiers de ce répertoire ?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Chiffres</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation> Octet</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Jusqu'à </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Octets</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Gamme : </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Chiffres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> est invalide - IPs Sources de Port 1 et 2&#xA;ne peuvent pas être identiques.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Adresse IP incorrect&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Gamme : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>L'adresse IP attribuée n'est pas adaptée à cette configuration.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 caractères par octet, jusqu'à</translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Octets</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 caractères par octet</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Nom de Code-Boucle</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Séquence Bit</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Livraison</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>dans la bande</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Hors Bande</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Boucle active</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Boucle inactive</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Autre</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Nom de Code-Boucle</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Sup.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Ajouter/Enlever</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Trace Tx</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Gamme : </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Sélectionner le canal</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>Entrer valeur KLM</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Gamme : </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Sélectionner Membre VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>Octets PSI</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Valeur d'octet</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>ODU Type</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>Num du port Affluent</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Gamme : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Sélectionner Membre VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Gamme : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Editeur des octets d'entête</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Sélectionnez un octet correct à modifier.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Optique non reconnu</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Page de construction.  Veuillez attendre ...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>La page est vide.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Ajouter</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Modifier</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Effacer</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Ajouter une ligne</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Modifier une ligne</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Effacer une ligne</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Inactif</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Traceability Unknown</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non occupé</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non occupé</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non occupé</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non occupé</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Longueur : </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caractères</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Longueur:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caractères</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Maintenant</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Entrez l'Heure : hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Aucun</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Octet %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nom du service</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Descendant</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Les deux directions</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Type de service</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>Le type de service a été réinitialisé aux données à cause d'un changement de longueur de trame et/ou CIR.</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Taux d'échantillonage (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Appels</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Taille de Trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Long. de paquet</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Canaux</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compression</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>Créer VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>Définir les membre VCG avec les numéros de canaux par défaut</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Définir VCG Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Définir VCG Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Bande passante Payload (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Nombre de Membres</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Distribuer&#xA;membres</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>Distribuer les membres VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Définir une distribution spécifique des membres VCG</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Instance</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Etape</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>Editer les membre VCG</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Format d'adresse</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Nouveau membre</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Défaut</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>Le flux de trafic ont été importé depuis l’explorer,&#xA;appuyer sur [Rejoindre Flux...] pour les ajouter.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Carnet d'Adresses</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Chercher le suivant</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Rupprimer</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Rejoindre Flux...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Ajouter</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Source</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP Destination</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>L’adresse IP entrée est ajoutée à l’Address Book.</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Rejoindre</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Déjà rejoint</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>Rejoindre</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Quitter le Flux</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Adresse IP Source</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>Adresse IP Dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Quitter les Flux...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Aucun</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Config Rapide</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Note: Ceci va modifier la configuration actuelle de trame.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rapide (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Full (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensité</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Famille</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulation</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incrément du Pbit</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Bits de priorité Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>TPID (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Prise en compte</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Index de départ de "User Priority"</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Bits de priorité SVLAN</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Ouvrir...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Date longue :</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Date courte :</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Heure longue :</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Heure courte :</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Numéros :</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Non affecté</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Attribué</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Réservé10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Réservé11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Réservé01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Réservé00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Enregistrer...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Mise à jour du test aux valeurs par défaut</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Configurer&#xA;Flux...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Distribution de la charge</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Auto&#xA;Distribute</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flux</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Taille de Trame</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Débit de trames</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% du Débit de Ligne</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>La rampe démarre à</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Constant</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Seuil max util</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Total (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Total (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Total (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Total (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Note: </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Au moins un flux doit être validé</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>Le seuil maximum d'util est </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>La charge possible maximum est </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>La charge totale indiquée ne doit pas excéder ceci.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Entrez le pourcentage : </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Entrez débit de trame:</translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Entrer le débit : </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Remarque :&#xA;Le débit n'est pas détecté. Veuillez appuyer sur Annuler &#xA;et réessayez lorsque le débit est détecté.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Configurer Triple-Play services</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Taux d'échantil-&#xA;lonage (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Appels</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Débit par&#xA;Appel (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Taux total&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Total taille trame&#xA;base (Octets)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Suppression Silence</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Tampon de gigue (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Canaux</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compression</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Débit (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Total taille trame base. (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Débit de Départ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Type de charge</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Echelle de Temps (Sec)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Pas de la charge (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Mbps C1 Total</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Simulé</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Données 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Données 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Données 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Données 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aléatoire</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voix</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Vidéo</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Données</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Note :</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Au moins un service doit être validé.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>La charge possible maximum est </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>La charge totale indiquée ne doit pas excéder ceci.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Distribution : </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Bande passante</translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Structure : </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>par défaut</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Etape</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Carnet d'Adresses</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Nouvelle Entrée</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>IP Source (0.0.0.0 = "pas de filtre")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>Requis</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Nom (max 16 caractères)</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Ajouter Entrée</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Importer/Exporter</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importer</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>Importer les adresses depuis la clé USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exporter</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Exporter vers la clé USB</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Effacer</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Supprimer Tous</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Sauvegarder et fermer</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Optionel</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Importer les adresses depuis la clé USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Importer les adresses</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>"Comma Separated (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Ajouter une nouvelle adresse</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Chaque adresse doit avoir 4 champs : &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Nom> séparés par une virgule</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Continue</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>ce n'est pas une adresse IP Source valide</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>ce n'est pas une adresse IP Destination valide</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>Ce n'est un PID valide</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Une nouvelle adresse doit avoir un nom (jusqu'à 16 caractères)</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Une nouvelle adresse ne doit pas avoir plus de 16 caractères</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Une adresse avec la même adresse Src IP, Dest IP et PID&#xA;existe déjà et a un nom</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>ECRASER le nom de l'adresse existante&#xA;ou&#xA;ANNULER l'importation de cette adresse</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>sauter</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Remplacer</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Une des adresses importée a une valeur de PID.&#xA;Les adresses avec un PID sont uniquement utilisées avec l'application MPTS</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Exporter les adresses vers la clé USB</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Exporter les adresses</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>Annuaire_adresses_IPTV</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>En cours d'utilisation. Choisir un nouveau nom ou éditer une adresse existante</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>Une adresse avec ces paramètres existe déjà</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>Etes-vous certain de vouloir effacer toutes les adresses?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Source</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP Destination</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Ajouter une nouvelle adresse</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>Une adresse avec le même nom existe déjà</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>Que voulez-vous faire?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Date : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Heure : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données d'événement, utilisez l'option de menu View->Result Windows->Single</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Actif</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Code</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Voie</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Path</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données K1/K2 octets, utilisez l'option de menu de View->Result Windows->Single</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hexa.</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binaire</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour voir plus les données de l'évènement délai dans un sens, utilisez la sélection de menu Voir->Fenêtre de résultats->Simple</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Perte source</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>SIA</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-SIA</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>Err B1</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>SIA-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>Err REI-L</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-SIA</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>Err MS-REI</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>Err B2</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>SIA-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>Err REI-P</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>Erreur B2</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-SIA</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>Err HP-REI</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>Err B3</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>SIA-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>Err REI-V</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>Err BIP-V</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>Erreur B3</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-SIA</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>Err LP-REI</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>Err LP-BIP</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 SIA</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 VMT</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 SIA</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 VMT</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 SIA</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 SIA</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL SIA</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>Err. VT STL</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL VT</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL VMT</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Err Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Perte signal</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Perte Sync Trm</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Err Trm Mot</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>Erreur VT</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Heure démarrage</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Heure de fin</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Plus longue</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Plus court</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Dernier</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Moyen</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interruption</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données de rupture de service, utilisez l'option de menu de View->Result Windows->Single.</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données de rupture de service, utilisez l'option de menu de View->Result Windows->Single.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>Arrêter</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Dépassement</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données de résultats d'appel, utilisez l'option de menu View->Result Windows->Single</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>tonalité d'invitation à numéroter</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  VRAI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durée</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Invalide</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Date : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Heure : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nom Prog</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evénement</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>IP:Port Flux</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nom du Flux</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Exporter la catégorie de résultat spécifique enregistrée</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Catégorie de résultat spécifique enregistrée</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Exporter</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Impossible de localiser la clé USB.&#xA;Merci d'insérer une clé USB (ou un lecteur USB) ou de la retirer et de la réinsérer</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>Export les données PTP à l'USB</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>Fichiers de données PTP (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Export du rapport à l'USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texte (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Export des captures d'écran à l'USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Captures d'écran sauvegardées (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Les fichiers Zip sélectionnées en tant que :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Entrez le nom du fichier : 60 char max</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Zip&#xA;&amp;&amp; Export</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exporter</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Prière de saisir un nom pour le fichier Zip</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Impossible de localiser la clé USB.&#xA;Merci d'insérer une clé USB (ou un lecteur USB) ou de la retirer et de la réinsérer</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Impossible de zipper les fichiers (s)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>Export les données TIE à l'USB</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Fichiers de données TIE Wander (*. Hrd *. Chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Exporter données de timing vers USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Tous les fichiers de données de timing (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Type :</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Ouvrir</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Import de la catégorie de résultats personnalisés de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Catégorie de résultat spécifique enregistrée</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Importer&#xA;la catégorie spécifique</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Import du logo de rapport de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Fichier image (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Importer&#xA;Logo</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Importer une carte rapide de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Fichiers pdf (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Importer&#xA;Carte rapide</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Import&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Dézip&#xA; &amp;&amp; Import</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Erreur - Impossible de décompresser un des fichiers.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>Espace libre insuffisant sur l'équipement de destination.&#xA;Opération de copie annulée.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Copie des fichiers en cours...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Terminé. Fichiers copiés.</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Erreur : Les articles suivants n'ont pas pu être copiés : &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>Sauvegarder données PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>Fichiers PTP (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copier</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Espace disque insuffisant. Supprimez d'autres fichiers sauvegardés ou exportez vers USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%1 de %2 de libre</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Pas de fichiers sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>Total %1 dans fichiers sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Total %1 dans %2 fichiers sélectionnés</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nom de fichier :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Entrez le nom du fichier : 60 char max</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>Supprimer tous les fichiers du répertoire?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Les fichiers en lecture seule ne seront pas effacés.)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Effacement des fichiers...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Etes vous sûr de vouloir effacer&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>Etes vous sûr de vouloir effacer cet article?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>Optique non recommandé</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Affiche les résultats de la découverte et du balayage de la structure du signal.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Classer par :</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Classer</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Re-classer</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Présente une sélection des tests disponibles qui peuvent être utilisés pour analyser la voie sélectionnée.</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Démarrer Test</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Veuillez patienter pendant la configuration de la voie sélectionnée...</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Montrer les détails</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Cacher les détails</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Plage : %1 à %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Chercher</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Original</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>Adaptez largeur</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>Adaptez hauteur</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Sélection d'affichage de test double</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Plus...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(suite)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Attention</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Buffer de log plein</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Capture arrêtée</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Editer une ligne</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Sélectionnez octet :</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>Impossible de capturer un screenshot</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>espace disque insuffisant</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Capture de screen shot : </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>A propos du flux</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>Numérotation DP</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>Numérotation MF</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>Numérotation DTMF</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Réservé</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Unidir</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>SIA-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>En attente</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Extra Trafic</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-SIA</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>Aucun disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>Impossible de suivre des liens </translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>Non trouvée</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Bas de page atteint, poursuite à partir du haut de page</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Sélectionner Outil</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Arrêter des rapports automatiques avant de commencer le script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Démarrer les rapports automatique dévalidés précédemment.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Création</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Défaut</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Effacer</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Supprimer Tous</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Emettre</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Réessayer</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Afficher</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS (vidange)</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS (remplissage)</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Conteneur</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Voie</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Etiq signal</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Trace Format</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Inconnu</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Ceci représente seulement le niveau actuel et ne prend pas en compte toute voie d'ordre inférieur ou supérieur. Seule la voie actuellement sélectionnée recevra des mises à jour en direct.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>L'état de la voie représenté par une icône.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Aucune alarme monitorée présente</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Alarmes présentes</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Monitoring sans alarmes monitorées présentes</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Monitoring avec alarmes présentes</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>Le nom du récipient du canal. Un suffixe 'c' indique un canal enchaîné</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>Le numéro N KLM de la voie selon la spécification RFC 4606</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>L'étiquette du signal du canal</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>Le dernier état connu de la voie.</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>Le canal est non valide</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI Present</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>SIA Present</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP Present</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Monitoring</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>État mis à jour à : </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>jamais</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>info de trace de canal</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>info de format de trace de canal</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>Non supporté</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Scan en cours</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Alarmes</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Invalide</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Non Formaté</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Octet Unique</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>CR/LF Terminated</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Les appareils liés</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Périphériques découverts</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>Cert CA</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Client Cert</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Clé client</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formater</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Espace disponible</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Capacité totale</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Etiquette</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Version de mise à jour</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Version installée</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Catégories</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Liste les noms attribués aux catégories spécifiques. Cliquer sur un nom active ou désactive cette catégorie spécifique.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Autorise la configuration d'une catégorie spécifique lorsque l'on clique dessus.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Autorise l'effacement d'une catégorie spécifique en commutant les catégories que l'on désire effacer.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>Le nom attribué à la catégorie spécifique. Cliquer sur le nom active ou désactive la catégorie spécifique.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Cliquer sur l'icône de configuration pour ouvrir le dialogue de configuration pour une catégorie spécifique.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Cliquer sur l'icône d'effacement sélectionne ou désélectionne la catégorie spécifique à effacer.  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Créer Rapport</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texte (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Création</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualiser le rapport après création</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erreur - Nom de fichier ne peut pas être vide.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>Modification des réglages régénérer les résultats actuels.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nom DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nom du NetBios</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Pas dans le sous-réseau</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>En attente d'un lien actif ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>En attente du DHCP ...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Reconfiguration de l'IP source ...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Mode</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Source</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nom DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nom du NetBios</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Nom du système</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>Pas sur le sous-réseau</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Pas dans le sous-réseau</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nom DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nom du NetBios</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Services</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Pas dans le sous-réseau</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Services</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nom DNS</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Découverte</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>Priorité VLAN</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Périphérique</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>IP réseau</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Périphérique</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nom du NetBios</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Périphérique</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>Réseaux IP</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Domaines</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Serveurs</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Serveurs</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Commutateurs</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Routeurs</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Infrastructure</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Noyau</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Distribution </translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Accès</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Découverte</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Réseaux IP découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Domaines NetBios découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>VLANs découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Routeurs découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Commutateurs découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Serveurs découverts</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Serveurs découverts</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Rapport de découverte de réseau</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Le rapport ne peut pas être créé</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>Espace disque insuffisant</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Généré par Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Généré par Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Généré par Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Généré par l'instrument de test de Viavi </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>Généralités</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Date</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>Le disque est plein, tous les graphiques ont été arretés.&#xA;Libérez de l'espace et redémarrez le test.&#xA;&#xA;Autrement, les graphiques peuvent être désactivés dans Outils->Personnalisation&#xA; dans la barre de menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Taper pour centre l'échelle de temps</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Propriétés du Graph</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>Moyen</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>Ko</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Fenêtre de saturation</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Sauver les données en graphique</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>Espace libre insuffisant sur le périphérique de destination.</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>Vous pouvez exporter directement au port USB si un périphérique USB est inséré.</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Sauvegarde des données du graphique. Cela peut prendre un certain temps, veuillez patienter...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Sauvegarde des données du graphique</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>Espace libre insuffisant sur le périphérique. Le graphique de données exporté est incomplet.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Une erreur est survenue lors de l'exportation des données de graphique. Les données peuvent être incomplètes.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Echelle</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 Jour</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 Heures</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 heure</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 Minutes</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 Minute</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 Secondes</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Plot_Data</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Taper et Glisser pour zoomer</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Saturation</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>Ko</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Conn.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Heure (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Message</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Port Src</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Port dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Effacer...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Confirmez...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>Les catégories sélectionnées seront retirées de tous les tests actuellement en cours d'exécution.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>Êtes-vous sûr de vouloir effacer les éléments sélectionnés ?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Nouveau...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Ouvre le dialogue de configuration d'une nouvelle catégorie de résultats spécifique.</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>En appuyant dessus cela permet de marquer les catégories spécifiques à effacer de l'appareil. Appuyez de nouveau sur la touche lorsque vous avez terminé votre sélection pour effacer les fichiers.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Appuyez sur "%1"&#xA;pour commencer</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Configuration des résultats de la Catégorie personnalisée</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>Les résultats sélectionnés marqués par un '*' ne s'appliquent pas à la configuration actuelle de test et n'apparaitront pas dans la fenêtre des résultats.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Nom de la catégorie :</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Introduisez le nom de la catégorie spécifique : %1 caract. max</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nom de fichier:</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Enregistrer sous </translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Enregistrer comme nouveau</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>Le fichier %1 qui contient la&#xA;catégorie « %2 »</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Résultats Sélectionnés : </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   (Max sélections </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configuration...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Résumé</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Résumé</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Max (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Valeur (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Bouton d'aide début ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Bouton d'aide inversé ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Bouton d'aide suivant ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Bouton d'aide fin ...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>Aucun appel dans l'historique</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Bouton d'aide début ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Bouton d'aide inversé ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Bouton d'aide suivant ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Bouton d'aide fin ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pic-to-Pic</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pic-Pos</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pic-Neg</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pic-to-Pic</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pic-Pos</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pic-Neg</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Délai de transit (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Nombre</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTATS&#xA;INDISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Statut : OK</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Statut : echec</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Etat : N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Développez pour afficher les options de filtrage</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation># MEPs découverts</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Définir comme pair</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Filtrer l'affichage</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Filtre actif</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Entrer valeur de filtre : %1 cars max</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>Type de CCM</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>Taux CCM</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>Id MEP pair</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>Maintenance Domain Level</translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Spécifiez ID de domaine</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ID d'association de domaine</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>ID d'association de maintenance</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Ensemble du test configuré. La ligne en surbrillance a été définie comme le MEP pair pour ce testeur.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Réglage du testeur comme MEP pair surligné a échoué.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>Impossible de définir %1 à %2.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTATS&#xA;INDISPONIBLE</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>GRAPHIQUES&#xA;DESACTIVES</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Cliquez sur cette fenêtre de résultats pour passer en plein écran.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>RESULTATS&#xA;INDISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Spécifiques</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tout</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>Résultats de  test de perte de trame %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en amont %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en aval %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>Résulats de test de débit de crédit de buffer %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>Résultats de test de débit de crédit de buffer en amont  %1 octet</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>Résultats de test de débit de crédit de buffer en aval %1 octet</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Exporter un fichier texte...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Journal exporté à</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Flux&#xA;Détails</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Réduire l'arbre des résultats dans cette fenêtre.</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Développer l'arbre des résultats dans cette fenêtre.</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>RESULTATS&#xA;CATEGORIE&#xA;EST PLEIN</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTATS&#xA;INDISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RESULTATS&#xA;INDISPONIBLE</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>RESUME&#xA;RESULTATS&#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Colonnes</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Afficher Colonnes</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Aucun</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Ne montrer que les erreurs</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonnes...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Montrer&#xA;uniq. lignes&#xA;en erreur</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Ne montrer que les lignes en erreur</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Taux de trafic (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Taux de trafic (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation># Flux Analysés</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Trafic groupé par</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>Nombre total de liens</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Flux 1-128 affichés</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Flux supplémentaires >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonnes...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrêté</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Saut</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Délais en (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Pour regarder plus de données de Traceroute, utilisez l'option de menu de View->Result Windows->Single</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Temps de transfert idéal</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Temps de transfert réel</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Groupe :</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flux</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Programmes</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Perte de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Gigue Paquet</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Err Distance</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Err Période</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err Octet Sync</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Afficher Seulement les Programmes Erronés</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonnes...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Total Prog. Mbps</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Afficher Seule&#xA;Progs Erronés</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Afficher Seulement les Programmes Erronés</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Afficher&#xA;Seulement&#xA;Flux Erronés</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Afficher Seulement les Flux Erronés</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation>#Flux&#xA;analysés</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>Mbps C1&#xA;Total</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>Erreurs&#xA;Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>Erreurs&#xA;Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Lancement&#xA;Analyseur</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonnes...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Attendez SVP..lancement de l'application Analyseur...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sec</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Heure</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Jour</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Veuillez choisir un script.. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Script :</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Etat :</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Etat courant</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrêté</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Temporiseteur :</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Temps de Script Passé :</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Durée du temporisateur</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Sortie :</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Script s'est terminé en :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Choisir&#xA;le Script</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Lancer le script</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Effacer les&#xA;sorties</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Arrêtez le script</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>EN COURS...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Temps de Script Passé :</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Veuillez choisir un script différent..</translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Erreur.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>EN COURS.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>EN COURS..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Personnaliser liste de test</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Montrer les résultats au démarrage</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Monter</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Descendre</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Supprimer</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Supprimer Tous</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renommer</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Séparateur</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Ajouter raccourci</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Ajouter Test sauvegardé</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>Supprimer tous les favoris ?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>La liste des favoris est celle par défaut.</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Tous les favoris personnalisés seront supprimés et la liste par défaut de cet appareil sera rétablie.  Voulez-vous continuer ?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Configurations de test (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Configurations double tests (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Sélectionner test sauvegardé</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Sélectionner</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Veuillez patienter…&#xA;Lancement en cours de Vue de test vide</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Test de pin</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Renommer test avec pin</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>Pin pour liste de tests</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Sauvegarder configuration de test</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nom du test :</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Entrer le nom à afficher</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Ce test est le même que %1</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>C'est un raccourci pour lancer une application de test.</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Description: %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Ceci est la configuration de test enregistrée.</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Nom de fichier : %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Remplacer</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>Impossible de lancer le test ...&#xA;Les options du module BERT ont expiré.&#xA;Quitter et relancer BERT de la page système.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>Ce test ne peut pas être lancé en ce moment.  Il pourrait ne pas peut être pris en charge par la configuration matérielle actuelle, ou peut nécessiter plus de ressources.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Essayez de retirer les tests qui tournent sur d'autres onglets.</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Ce test est exécuté sur un autre port. Voulez-vous aller à ce test ? (sur l'onglet  %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>Un autre test (sur l'onglet %1) peut être reconfiguré pour le test sélectionné. Souhaitez-vous reconfigurer ce test ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>La liste est vide.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Impossible de lancer le test...&#xA;fonction gigue optique éteinte.&#xA;Lancez la fonction de gigue optique à partir de la page Home/System.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>Impossible de lancer le test...&#xA;Ressources d'alimentation indisponibles.&#xA;Arretez/reconfigurez un test existant.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>Impossible de lancer le test...&#xA;Ressources de puissance pas disponible.&#xA;Déselectionner un autre module ou&#xA;annuler un autre test.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Merci d'attendre...&#xA;Ajout du test </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Merci d'attendre...&#xA;Reconfiguration du test à </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>Impossible de lancer le test...&#xA;Les ressources requises sont peut être utilisées.&#xA;&#xA;Merci de contacter le Support Technique.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>Construction des objets de l'Interface Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>UI Synchronisation avec le module d'application</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>Initialisation de l'interface Utilisateur</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Retour</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Choisir&#xA;le Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright Viavi Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Information sur l'instrument</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Options</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>Fichier sauvegardé</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Mode d'accès de l'interface utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>La télécommande est en service.&#xA;&#xA;Mode d'accès lecture seulement empêche l'utilisateur de changer les réglages&#xA;ce qui pourrait impacter le fonctionnement de la télécommande</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Mode d'accès</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>Le Module MSAM a redémarré à cause du changement de configuration du PIM.</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Un module PIM a été inséré ou enlevé, le module MSAM va redémarrer,&#xA;cela va prendre environ 2 mn, merci d'attendre...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>Le module BERT est en surchauffe et&#xA;s'arrêtera automatiquement si la température interne continue&#xA;d'augmenter. Veuillez sauvegarder vos données, arrêter le module BERT&#xA;et appeler le support technique.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>Le module BERT a été forcé de fermer en raison de surchauffe.</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>PIM XFP dans le mauvais slot. Veuillez déplacer le PIM XFP dans le port #1.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>Le module PIM XFP est dans le mauvais slot &#xA; veuillez déplacer le PIM XFP dans le port #1</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>Vous avez sélectionné un test électrique mais le SFP&#xA;semble être un SFP optique.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Vérifier que vous utilisez bien un SFP électrique.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>Le module BERT a détecté une erreur possible sur l'application %1.&#xA;&#xA;Vous avez sélectionné un test électrique mais le SFP ressemble à un SFP optique. Prière de le remplacer ou de sélectionner un autre SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>Vous avez sélectionné un test optique mais le SFP&#xA;semble être un SFP électrique.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Vérifier que vous utilisez bien un SFP optique.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>L'équipement de test a détecté une erreur possible dans l'application %1.&#xA;&#xA;Vous avez sélectionné un test optique mais le SFP ressemble à un&#xA;SFP électrique. Veuillez remplacer ou sélectionner un autre SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>Vous avez sélectionné un test 10G mais l'émetteur-récepteur n'a pas l'air d'être un SFP+.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Assurez-vous que vous utilisez un SFP+.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>L'équipement de test a détecté une erreur possible. Ce test nécessite un SFP+ mais l'émetteur-récepteur n'a pas l'air de l'être. Veuillez remplacer avec un SFP+.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Paramètres pour des Rapports Automatiques</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Réutilisez le même fichier</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoRapport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Attention : Le lecteur sélectionné est plein. Vous pouvez libérer de place manuellement ou laisser les 5 plus anciens rapports&#xA;s'effacés automatiquement</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Permettez les rapports automatiques</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Rapport périodique</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Min:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Max:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Redémarrez le test après création de rapport</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Mode</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Créez un nouveau fichier</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Nom du rapport</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>Ajoutez la date et l'heure au nom du fichier</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format :</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Texte</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Les rapports automatiques ne sont pas supportés en mode d'accès en lecture seule</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>Rapport automatique sera sauvegardé sur le disque dur</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>Le rapport automatique nécessite un disque dur. Il semble qu'aucun disque dur ne soit installé.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Des rapports automatiques ne peuvent pas être activés lorsque un script automatisé est en cours d'exécution .</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Créer un Rapport Automatique</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>En préparation...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Création </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Effacement du rapport précédent...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Fini.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** ALIMENTATION INSUFFISANTE.  DESELECTIONNER UN AUTRE MODULE ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Connection en série réussie</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Verification des applications pour la mise à jour</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Application prête pour les communications</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ERREUR LORS DE LA MISE A JOUR DU KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Vérifier que Ethernet Security = Standard" et Réinstaller le logiciel BERT *****"</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERREUR LORS DE LA MISE A JOUR DE L'APPLICATION. Réinstaller le logiciel du module ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** ERREUR DE MISE A JOUR. PUISSANCE INSUFFISANTE ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Erreur de démarage : Merci de désactiver Module BERT puis réactiver ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Afficher</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Rapports</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Outils</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Créer Rapport...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Rapport automatique ...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Démarrer Test</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Arrêter Test</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Personnaliser...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Mode d'accès</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Mise à jour du test aux valeurs par défaut</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Effacer l'historique</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Lancer les Scripts...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>Emulation VT100</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Paramètres du Modem...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Restaurer la mise en page par défaut</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Fenêtre de résultat</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Un seul</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Diviser Gauche/Droite</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Diviser Haut/Bas</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>Grille 2 x 2</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>Rassembler le bas</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>Rassembler la gauche</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Afficher Seulement les Résultats</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>État du Test</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Panneau de Config</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Panneau d'Actions</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Choisir le Script</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>Script (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Choisissez&#xA;le script</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Connexions de Signaux</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Créer Rapport</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texte (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Choisir&#xA;Matières</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Création</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualiser le rapport après création</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erreur - Nom de fichier ne peut pas être vide.</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Choisir le contenu de</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Formater</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nom de fichier</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Selectionner...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualiser le rapport après création</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Inclure le message journal</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Créer&#xA;Rapport</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Vue&#xA;Rapport</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texte (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selectionner</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erreur - Nom de fichier ne peut pas être vide.</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>Rapport sauvegardé</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Atténuez le signal, SVP.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>Le log des événements et l'histogramme est plein.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>Le log K1/K2 est plein.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>Le log est plain.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>Le test continue sans log d'événement.&#xA;Le re-démarrage du test va effacer&#xA;le fichier log et les histogrammes.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Reset&#xA;Optique</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Fin de Test</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Surcharge optique</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>Log est pleine</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>ATTENTION</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Pas de test en cours</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Personnaliser le Format de l'interface utilisateur</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Sauvegarder le test</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Sauvegarder le test double</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Charger test</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Tous fichiers (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Tests sauvegardés (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Tests doubles sauvegardés (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Charger réglage</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Charger un test double</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Importer le test sauvegardé de l'USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Tous les fichiers (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Configurations de test RFC classique sauvegardées (*.classic_rf)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Configuration de test RFC sauvegardée (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>Configurations de test FC sauvegardées (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Configurations TrueSpeed sauvegardées (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Configurations VNF TrueSpeed (*vts) sauvegardée</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Configurations SAMComplete sauvegardées (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>Configurations SAMComplete sauvegardées (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Configurations de vérif OTN Check sauvegardée (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Configurations auto-test optiques sauvegardées (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Configurations de vérification CPRI sauvegardées '*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Configurations de vérification PTP sauvegardées (*/ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>Fichiers Zip sauvegardés (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Exporter le test sauvegardé vers l'USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Tous les fichiers (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Multi-tests utilisateur sauvegardés</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Tests utilisateur enregistrés</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>La télécommande est en service.&#xA;&#xA;Cette opération n'est pas autorisée en mode d'accès lecture seulement.&#xA;Utilisez Outils->Mode d'accès pour permettre l'accès total.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>Les options du module BERT ont expiré.&#xA;Prière de quitter et de relancer BERT de la page système.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Cache</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Redémarrer les deux tests</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Re-&#xA;démarrer</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Outils:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Actions</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Fenêtre de résultat maximum pour test :</translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Affichage complet</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Ajout d'un test</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuel d'utilisation</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Optiques Recommandés</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Grille de fréquence</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Qu'est-ce que c'est ?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Sauvegarder la configuration de test double sous ...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Configuration de test double local ,,,</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Afficher</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Sélection de changement de test ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Allez à l'affichage complet des tests</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Rapports</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Création d'un rapport de test double</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Vue Rapport...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Aide</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Attention</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Sauvegarde de fichier</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Nouveau nom :</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Entrer un nouveau nom de fichier</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renommer</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>Ne peut pas renommer puisqu'un fichier existe avec le même nom.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Re-&#xA;démarrer</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Afficher les Informations Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Aucun sélectionné...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caractères max : </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Sélectionner le logo...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>L'aperçu n'est pas disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Aider les diagrammes</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuel d'utilisation</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Retour</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Suivant</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>LancementRapide</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERREUR LORS DE LA MISE A JOUR DE LA GIGUE. Réinstaller le logiciel du module ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ERREUR DANS LE MODULE DE GIGUE OPTIQUE. PUISSANCE INSUFFISANTE. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>La fonction de Gigue Optique est en fonctionnement</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Profiles (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Sélectionner profiles</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Déselectionner Tous</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Remarque : Le chargement du profile "Connect" connectera le canal de communication à l'unité distante si nécessaire.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Supprimer Tous</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Supprimer</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Profile incompatible </translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Charger&#xA;profiles</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Etes-vous sûr que vous voulez supprimer %1 ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Etes vous sur de vouloir effacer tous les profiles %1 ?&#xA;&#xA;Cette opération ne peut pas être annulée.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Les fichiers en lecture seule ne seront pas effacés.)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>Echec de chargement de profiles de :</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Profiles chargés de :</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Certaines configurations n'ont pas été chargées correctement, car elles n'ont pas été trouvées dans cette application.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Les profiles chargés avec succès de :</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Activer test double</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Classer les applications</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Valider les options</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>Pas de tests disponibles pour le matériel ou options installés</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>Impossible de lancer tous les tests.  Vérifiez les options installées et la configuration actuelle du matériel,</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Restauration de l'application en cours lors de l'arrêt de l'équipement</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Applications en cours</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>Impossible d'installer la flash USB interne.&#xA;&#xA;Veuillez vérifier que le dispositif est correctement inséré à côté de la batterie. Une fois insérée, veuillez redémarrer le module BERT.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>Le système de fichiers de la flash USB interne semble être corrompu. Veuillez contacter le support technique pour de l'aide.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>La capacité flash USB interne est inférieure à la taille conseillée de 1 G&#xA;&#xA;Veuillez vérifier que le dispositif est correctement inséré à côté de la batterie. Une fois insérée, veuillez redémarrer l'appareil.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Une erreur s'est produite lors de la restauration des configurations.&#xA;Veuillez réessayer.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Tous les tests en cours seront arrêtés avant de charger les tests sauvegardés.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Prière d'attendre ...&#xA;Chargement des tests enregistrés</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>Le nom du document du test ou du chemin n'est pas valide.&#xA; Utiliser "Charger sauvegarder config." pour localiser le document.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>Une erreur s'est produite lors de la restauration d'un test.&#xA;Veuillez réessayer.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Chargeur de batterie activé</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Merci de noter que le chargeur de batterie ne sera pas activé dans ce mode. Le chargeur sera automatiquement activé quand le test sera choisit.</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>Le contrôle à distance est cours d'utilisation pour ce&#xA;module et l'affichage a été desactivé</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Messages enregistrés. Cliquez pour voir ...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Journal de messages pour %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>Pas de messages</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 message</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 messages</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Journal de messages pour %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Enregistrer un message</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Paramètres du Modem</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Sélectionner une adresse IP pour ce serveur et une adresse IP pour être assigné au client</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Serveur IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>Client IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Localisation (code postal)</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>Impossible de détecter le modem. Assurez-vous que le modem est correctement branché et réessayez</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>Equipement occupé. Déconnecter toutes les sessions d'appel et recommencer</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>Impossible de mettre à jour le modem. Déconnecter toutes les sessions en cours et réessayer</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Redémarrage du/des test(s) en raison du changement d'heure.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Entrez un nouvelle clé optionnelle ci-dessous pour l'installer.</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Clé optionnelle</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Installation</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importer</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Contactez Viavi pour acheter les options du logiciel.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>ID Challenge Option:</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Touche acceptée! Redémarrer pour activer la nouvelle option</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Clé rejetée - Les crénaux des options arrivées à terme sont pleines.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Clé rejetée - Les options arrivées à terme sont déjà installées.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Touche non valide - Réessayer</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%1 de %2 clé(s) Accepté ! Redémarrez pour activer la(les) nouvelle(s) option(s).</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>Impossible d'ouvrir '%1' sur un lecteur flash USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>Un problème pour obtenir les informations des Options est survenu...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Octets Sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Défauts</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Légende</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Légende:</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI (non-utilisé)</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Réservé</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>A propos Module BERT</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Module Transport</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importer une carte rapide</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importer une carte rapide</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation>Cacher Menu</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Tous les tests</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personnalise</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Au moins une des captures d'écran sélectionnées a été prise avant le début&#xA;du test actuel. Assurez vous d'avoir choisi le bon fichier.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Sélectionner une&#xA;copie d'écran</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Copie d'écran&#xA;non sélectionnée</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Etalonnage RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Etalonner</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Etalonnage...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>L'étalonnage n'est pas terminé. L'étalonnage requiert d'utiliser RS-FEC.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(L'étalonnage peut être exécuté depuis l'onglet RS-FEC dans les pages de configuration.</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Réessayer Etalonnage</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Quitter Etalonnage</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Etalonnage incomplet</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC est généralement utilisé avec SR4, PSM4, CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Pour réaliser l'étalonnage RS-FEC, faites ce qui suit (s'applique également au CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Insérer un adaptateur QSFP28 dans le CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Insérer un émetteur-récepteur QSFP28 dans l'adaptateur</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Insérer un périphérique de loopback fibre dans l'émetteur-récepteur</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Cliquez sur Etalonnner</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resynchroniser</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Doit à présent resynchroniser l'émetteur-récepteur vers le périphérique sous test (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Supprimez le périphérique loopback fibre depuis l'émetteur-récepteur</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Etablir une connexion au périphérique sous test (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Mettez en marche le laser DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Vérifiez que le signal présent de LED sur votre CSAM est vert</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Cliquez sur resynchroniser voie</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Resynchronisation terminée. La boîte de dialogue peut à présent être refermée.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Enregistrer en lecture seule</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>Pin pour list de test</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nom de fichier</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Selectionner...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Enregistrer en lecture seule</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Sauvegarde des&#xA;profiles</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Profiles (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selectionner</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Profils </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>est un fichier en lecture seul. Il ne peut pas être remplacé.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Profiles sauvegardés</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Sélectionner le logo</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Fichier image (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selectionner</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>Le fichier est trop grand. Veuillez choisir un autre fichier.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Octets Sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Défauts</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Légende</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Démarrer&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Arrêter&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrêté</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Plein accès</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Lecture seulement</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Sombre</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Clair</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Lancement rapide</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Voir résultats</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Débit Total TCP Estimé (Mbps) basé sur :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Largeur de Bande disponible Max</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Moyenne Latence en boucle (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Nombre de sessions TCP nécessaires pour atteindre le débit maximum :</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Taille de la fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Sessions</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Débit Total TCP Estimé (kbps) basé sur :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Largeur de bande maximum disponible (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Largeur de Bande</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>Une erreur est survenue lors de la sauvegarde des paramètres du test.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>Une erreur est survenue lors du chargement des paramètres du test.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>Le disque sélectionné est plein.&#xA;Supprimer des fichiers et réessayer.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>Les fichiers enregistrés suivants de la catégorie de résultat spécifique diffèrent de ceux actuellement chargés :</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 autres</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>Le remplacement peut affecter d'autres tests.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>Continuer le remplacement ?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Veuillez patienter...&#xA;Restauration des configurations...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Ressources insuffisantes pour charger le test %1 maintenant.&#xA;Voir dans le menu "Test" la liste des tests disponibles avec la configuration actuelle</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Impossible de restaurer tous les paramètres du test.&#xA;Le contenu du fichier est peut être ancien ou endommagé.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>Mode d'accès en lecture seulement. Pour le changer utilisez Outils->Mode d'accès</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Pas de test en cours</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>Ceci va remettre à zéro toutes les installations par défaut.&#xA;&#xA;Continuer?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Ceci va arrêter et re-démarrer le test.&#xA;La configuration de test sera rechargé aux valeurs par défaut.&#xA;&#xA;Continuer ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Ce flux de travail est actuellement en cours. Voulez-vous en finir et commencer le nouveau? &#xA;&#xA;Cliquez sur Annuler pour continuer l'exécution du flux de travail précédent.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>Voulez-vous terminer ce flux de travail?&#xA;&#xA;Cliquez sur Annuler pour continuer l'exécution.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>Impossible de lancer VT100. Equipement série déjà utilisé.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Module </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>Merci de noter que "Redémarrer" va effacer tous les résultats des 2 ports</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Choisir&#xA;le Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Charger test...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Sauvegarder le test sous...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Charger les réglages seulement...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Ajout d'un test</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Suppression de test</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Aide</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Aider les diagrammes</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Vue Rapport...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Exporter le rapport ...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Afficher les Informations Utilisateur...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Importer Rapport Logo ...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Import de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Tests sauvegardés...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Sauvegarder la catégorie personnalisée ...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Exporter vers USB</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Screenshot...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Données de timing…</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Visualiser/Installer Options...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Prendre une capture d'écran</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuel d'utilisation</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Optiques Recommandés</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Grille de fréquence</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Connexions de Signaux</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Qu'est-ce que c'est ?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Cartes rapides</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Merci d'attendre...&#xA;Suppression d'un test...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Port 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Port 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Aucun sélectionné...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Sélectionner Fichier...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Import du paquet capturé de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Sélectionner&#xA;Fichier</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Capture de paquets sauvegardée (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Vue Rapport</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous les fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texte (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Afficher</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Visualiser&#xA;ce rapport</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Voir d'autres&#xA;rapports</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>Renommer&#xA;rapport</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Sélectionner</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> existe déjà.&#xA;Voulez-vous le remplacer?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Rapport renommé</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Erreur - Nom de fichier ne peut pas être vide.</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>Un rapport a été sauvegardé sous</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>Aucun rapport n'a été enregistré.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Allez</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>Enregistrer les données de TIE...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Sauvegarder en tant que type : </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>Fichier HRD</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>Fichier CHRD</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Enregistrement </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>Ceci va prendre plusieurs minutes...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Erreur : Impossible d'ouvrir le fichier HRD. Veuillez réessayer de sauvegarder.</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Annulation...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>Données TIE sauvegardées.</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Erreur : Le fichier ne peut être sauvegardé. Veuillez réessayer.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Attention</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>En fermant l'Analyse Wander, tous les résultats seront perdus.&#xA;Pour Continuer l'analyse, cliquez sur Continuer l'Analyse.</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Fermer l'analyse</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Continuer l'analyse</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Analyse du Wander</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Actualiser&#xA;TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>Arrêt du TIE&#xA;Mise à jour</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Calcul du&#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Arrêt&#xA;du calcul en cours</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Capture&#xA;d'écran</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Donnée de charge&#xA;TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>Arrêtez de TIE&#xA;charge</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Fermer&#xA;l'analyse</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Données de charge TIE</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Tous les fichiers d'écartement (*.chrd *.hrd);;fichiers Hrd (*.hrd);;fichiers Chrd (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Appuyez deux fois pour définir le rectangle</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Supprimer Tous</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Supprimer</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Charger profil</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Etes-vous sûr que vous voulez supprimer %1 ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Etes vous sur de vouloir effacer tous les profiles %1 ?&#xA;&#xA;Cette opération ne peut pas être annulée.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Les fichiers en lecture seule ne seront pas effacés.)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 Profiles (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>Impossible de charger le profil.</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Le chargement a échoué</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Suivant</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Message</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erreur</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Allez</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Attention</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>Que voulez-vous faire ensuite ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>Êtes-vous sur vous voulez quitter ?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Restaurer les réglages en quittant</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Sortie sur les résultats</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Retour</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Étape-par-étape</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Suivant</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Guidez moi</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Autre port</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Recommencer</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Aller à...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Autre port</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Le test démarre, Attendre SVP...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Le test se termine, veuillez attendre ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Enregistrer un message</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Menu principal</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Montrer étapes</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Réaction : </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Aucun sélectionné...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo du rapport</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>RAZ</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Sélectionner le logo...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>L'aperçu n'est pas disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Le test démarre, Attendre SVP...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Le test se termine, veuillez attendre ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>Test est en cours ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Temps restant:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Pas de test en cours</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incomplet</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test terminé</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Arrêter des rapports automatiques avant de commencer le script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Démarrer les rapports automatique dévalidés précédemment.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Menu principal</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Capture d'écran sauvegardée</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Capture d'écran sauvegardée:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Sélection de profil</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Couche de fonctionnement</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Charger un profil sauvegardé</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>Comment souhaitez-vous configurer TrueSAM?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Charger des configurations à partir d'un profil sauvegardé</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Allez</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Commencer un nouveau profil</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>Votre service fonctionne sur quelle couche ?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Couche 2 : Test utilisant les adresses MAC, ex. 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Couche 3 : Test utilisant les adresses IP, ex. 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Veuillez patienter... vers l'étape soulignée.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Sélectionner tests</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Etablir les communications</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Configurer RFC 2544 amélioré</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Configurer SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Configurer J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Configurer TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Sauvegarder la configuration</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Ajouter l'information du rapport</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>Lancer les tests sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Vue Rapport</translation>
        </message>
    </context>
</TS>
