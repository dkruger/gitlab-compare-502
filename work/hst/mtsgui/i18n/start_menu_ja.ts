<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>ﾚﾎﾟｰﾄ ﾛｺﾞ</translation>
        </message>
        <message utf8="true">
            <source>Device Under Test</source>
            <translation>ﾃｽﾄ対象</translation>
        </message>
        <message utf8="true">
            <source>Owner</source>
            <translation>所有者</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>Software Revision</source>
            <translation>ｿﾌﾄｳｪｱﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>Comment</source>
            <translation>ｺﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tester Comments</source>
            <translation>作業者 ｺﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Tester</source>
            <translation>作業者</translation>
        </message>
        <message utf8="true">
            <source>Test Instrument</source>
            <translation>測定器</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>ﾓﾃﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Viavi -BASE_MODEL- -MODULE_NAME-</source>
            <translation>Viavi -BASE_MODEL- -MODULE_NAME-</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>SW ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SOFTWARE_VERSION-</source>
            <translation>-MODULE_SOFTWARE_VERSION-</translation>
        </message>
        <message utf8="true">
            <source>BERT Serial Number</source>
            <translation>BERT ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SERIAL-</source>
            <translation>-MODULE_SERIAL-</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom</source>
            <translation>HS ﾃﾞｰﾀ通信</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT</source>
            <translation>HS ﾃﾞｰﾀ通信 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Terminate</source>
            <translation>終端</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT Term</source>
            <translation>HS ﾃﾞｰﾀ通信 BERT 時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Diphase</source>
            <translation>Diphase</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT</source>
            <translation>Diphase BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT Term</source>
            <translation>Diphase BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1/DS3</source>
            <translation>DS1/DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1</source>
            <translation>DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT</source>
            <translation>DS1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Term</source>
            <translation>DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Thru</source>
            <translation>ｽﾙｰ</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Thru</source>
            <translation>DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Single Monitor</source>
            <translation>Single ﾓﾆﾀ</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Single Mon</source>
            <translation>DS1 BERT Single ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Monitor</source>
            <translation>Dual ﾓﾆﾀ</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Dual Mon</source>
            <translation>DS1 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander</source>
            <translation>DS1 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander Single Mon</source>
            <translation>DS1 Wander 単一 Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling</source>
            <translation>DS1 ｼｸﾞﾅﾘﾝｸﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Term</source>
            <translation>DS1 ｼｸﾞﾅﾘﾝｸﾞ時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Dual Mon</source>
            <translation>DS1 ｼｸﾞﾅﾘﾝｸﾞ･ﾃﾞｭｱﾙ･ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI</source>
            <translation>DS1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Term</source>
            <translation>DS1 ISDN PRI 時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Dual Mon</source>
            <translation>DS1 ISDN PRI ﾃﾞｭｱﾙ･ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF</source>
            <translation>DS1 VF</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Term</source>
            <translation>DS1 VF 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Dual Mon</source>
            <translation>DS1 VF Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC</source>
            <translation>DS1 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC Dual Mon</source>
            <translation>DS1 HDLC Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP</source>
            <translation>DS1 PPP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP Ping Term</source>
            <translation>DS1 PPP Ping 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Jitter</source>
            <translation>DS1 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Jitter Term</source>
            <translation>DS1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Rx Jitter Term</source>
            <translation>DS1 BERT Rx ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Wander Term</source>
            <translation>DS1 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3</source>
            <translation>DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT</source>
            <translation>DS3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Term</source>
            <translation>DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Thru</source>
            <translation>DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Single Mon</source>
            <translation>DS3 BERT Single ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Dual Mon</source>
            <translation>DS3 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT</source>
            <translation>E1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Term</source>
            <translation>DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Thru</source>
            <translation>DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Dual Mon</source>
            <translation>DS3 : E1 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Term</source>
            <translation>DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Thru</source>
            <translation>DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Dual Mon</source>
            <translation>DS3 : DS1 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC</source>
            <translation>DS3 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC Dual Mon</source>
            <translation>DS3 HDLC Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Jitter</source>
            <translation>DS3 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Jitter Term</source>
            <translation>DS3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Rx Jitter Term</source>
            <translation>DS3 BERT Rx ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Jitter Term</source>
            <translation>DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Rx Jitter Term</source>
            <translation>DS3 :E1 BERT Rx ｼﾞｯﾀー項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Jitter Term</source>
            <translation>DS3 : DS1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Rx Jitter Term</source>
            <translation>DS3 :DS1 BERT Rx ｼﾞｯﾀー項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Wander</source>
            <translation>DS3 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Wander Term</source>
            <translation>DS3 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Wander Term</source>
            <translation>DS3 : E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Wander Term</source>
            <translation>DS3 : DS1 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1/E3/E4</source>
            <translation>E1/E3/E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1</source>
            <translation>E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Term</source>
            <translation>E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Thru</source>
            <translation>E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Single Mon</source>
            <translation>E1 BERT Single ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Dual Mon</source>
            <translation>E1 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander</source>
            <translation>E1 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander Single Mon</source>
            <translation>E1 Wander 単一 Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI</source>
            <translation>E1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI Term</source>
            <translation>E1 ISDN PRI 条件</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Jitter</source>
            <translation>E1 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Jitter Term</source>
            <translation>E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Wander Term</source>
            <translation>E1 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3</source>
            <translation>E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT</source>
            <translation>E3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Term</source>
            <translation>E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Thru</source>
            <translation>E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor</source>
            <translation>ﾓﾆﾀ</translation>
        </message>
        <message utf8="true">
            <source>E3 BERT Mon</source>
            <translation>E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Term</source>
            <translation>E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Thru</source>
            <translation>E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Mon</source>
            <translation>E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Jitter</source>
            <translation>E3 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Jitter Term</source>
            <translation>E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Jitter Term</source>
            <translation>E3 : E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Wander</source>
            <translation>E3 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Wander Term</source>
            <translation>E3 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Wander Term</source>
            <translation>E3 : E1 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4</source>
            <translation>E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT</source>
            <translation>E4 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Term</source>
            <translation>E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Thru</source>
            <translation>E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Mon</source>
            <translation>E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Term</source>
            <translation>E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Thru</source>
            <translation>E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Mon</source>
            <translation>E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Term</source>
            <translation>E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Thru</source>
            <translation>E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Mon</source>
            <translation>E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Jitter</source>
            <translation>E4 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Jitter Term</source>
            <translation>E4 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Jitter Term</source>
            <translation>E4 : E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Jitter Term</source>
            <translation>E4 : E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Wander</source>
            <translation>E4 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Wander Term</source>
            <translation>E4 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Wander Term</source>
            <translation>E4 : E3 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Wander Term</source>
            <translation>E4 : E1 BERT ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SONET</source>
            <translation>SONET</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>J-Scan</source>
            <translation>J-Scan</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 J-Scan</source>
            <translation>STS-1 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Bulk BERT</source>
            <translation>ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Term</source>
            <translation>STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Thru</source>
            <translation>STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Mon</source>
            <translation>STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Drop+Insert</source>
            <translation>ﾄﾞﾛｯﾌﾟ + 挿入</translation>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Drop+Insert</source>
            <translation>STS-1 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + 挿入</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Dual Mon</source>
            <translation>STS-1 Bulk BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Term</source>
            <translation>STS-1 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Thru</source>
            <translation>STS-1 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Mon</source>
            <translation>STS-1 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Drop+Insert</source>
            <translation>STS-1 DS3 BERT ﾄﾞﾛｯﾌﾟ + 挿入</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Term</source>
            <translation>STS-1 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Thru</source>
            <translation>STS-1 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Mon</source>
            <translation>STS-1 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Term</source>
            <translation>STS-1 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Thru</source>
            <translation>STS-1 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Mon</source>
            <translation>STS-1 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Term</source>
            <translation>VT-1.5 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Thru</source>
            <translation>STS-1 VT-1.5 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Single Mon</source>
            <translation>STS-1 VT-1.5 Bulk BERT Single ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 Bulk BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Term</source>
            <translation>STS-1 VT-1.5 DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Thru</source>
            <translation>STS-1 VT-1.5 DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Single Mon</source>
            <translation>STS-1 VT-1.5 DS1 BERT Single ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 DS1 BERT Dual ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Jitter</source>
            <translation>STS-1 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Jitter Term</source>
            <translation>STS-1 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Jitter Term</source>
            <translation>STS-1 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Jitter Term</source>
            <translation>STS-1 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Jitter Term</source>
            <translation>STS-1 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Jitter Term</source>
            <translation>STS-1 VT-1.5 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Jitter Term</source>
            <translation>STS-1 VT-1.5 DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Wander</source>
            <translation>STS-1 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Wander Term</source>
            <translation>STS-1 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Wander Term</source>
            <translation>STS-1 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Wander Term</source>
            <translation>STS-1 DS3 : DS1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Wander Term</source>
            <translation>STS-1 DS3 : E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Wander Term</source>
            <translation>STS-1 VT-1.5 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Wander Term</source>
            <translation>STS-1 VT-1.5 DS1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3</source>
            <translation>OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 J-Scan</source>
            <translation>OC-3 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c Bulk BERT</source>
            <translation>STS-3c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Term</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Thru</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Mon</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Term</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Thru</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Mon</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Term</source>
            <translation>OC-3 STS-1 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-3 STS-1 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Term</source>
            <translation>OC-3 VT-1.5 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-3 VT-1.5 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-3 VT-1.5 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Term</source>
            <translation>OC-3 VT-1.5 DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-3 VT-1.5 DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-3 VT-1.5 DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c-Xv</source>
            <translation>STS-3c-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Term</source>
            <translation>OC-3 STS-3c-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Mon</source>
            <translation>OC-3 STS-3c-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>GFP Ethernet</source>
            <translation>GFP ｲｰｻﾈｯﾄ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Traffic</source>
            <translation>ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traffic</source>
            <translation>ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Ping</source>
            <translation>ﾚｲﾔ 3 Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traceroute</source>
            <translation>ﾚｲﾔ 3 Traceroute</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 STS-3c-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1-Xv</source>
            <translation>STS-1-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Term</source>
            <translation>OC-3 STS-1-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Mon</source>
            <translation>OC-3 STS-1-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 STS-1-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5-Xv</source>
            <translation>VT-1.5-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Term</source>
            <translation>OC-3 VT-1.5-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Mon</source>
            <translation>OC-3 VT-1.5-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 VT-1.5-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Jitter</source>
            <translation>OC-3 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Jitter Term</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Jitter Term</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-3 VT-1.5 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-3 VT-1.5 DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Wander</source>
            <translation>OC-3 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Wander Term</source>
            <translation>OC-3 STS-3c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Wander Term</source>
            <translation>OC-3 STS-1 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Wander Term</source>
            <translation>OC-3 STS-1 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Wander Term</source>
            <translation>OC-3 VT-1.5 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Wander Term</source>
            <translation>OC-3 VT-1.5 DS1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12</source>
            <translation>OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 J-Scan</source>
            <translation>OC-12 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-12c Bulk BERT</source>
            <translation>STS-12c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Term</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Thru</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Mon</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Term</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Thru</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Mon</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Term</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Thru</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Mon</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Term</source>
            <translation>OC-12 STS-1 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-12 STS-1 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Term</source>
            <translation>OC-12 VT-1.5 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-12 VT-1.5 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-12 VT-1.5 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Term</source>
            <translation>OC-12 VT-1.5 DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-12 VT-1.5 DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-12 VT-1.5 DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Term</source>
            <translation>OC-12 STS-3c-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Mon</source>
            <translation>OC-12 STS-3c-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 STS-3c-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Term</source>
            <translation>OC-12 STS-1-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Mon</source>
            <translation>OC-12 STS-1-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 STS-1-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Term</source>
            <translation>OC-12 VT-1.5-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Mon</source>
            <translation>OC-12 VT-1.5-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 VT-1.5-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Jitter</source>
            <translation>OC-12 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Jitter Term</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Jitter Term</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Jitter Term</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-12 VT-1.5 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-12 VT-1.5 DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Wander</source>
            <translation>OC-12 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Wander Term</source>
            <translation>OC-12 STS-12c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Wander Term</source>
            <translation>OC-12 STS-3c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Wander Term</source>
            <translation>OC-12 STS-1 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Wander Term</source>
            <translation>OC-12 STS-1 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Wander Term</source>
            <translation>OC-12 VT-1.5 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Wander Term</source>
            <translation>OC-12 VT-1.5 DS1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48</source>
            <translation>OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 J-Scan</source>
            <translation>OC-48 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48c Bulk BERT</source>
            <translation>STS-48c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Term</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Thru</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Mon</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Term</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Thru</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Mon</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Term</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Thru</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Mon</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Term</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Thru</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Mon</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Term</source>
            <translation>OC-48 STS-1 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-48 STS-1 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Term</source>
            <translation>OC-48 VT-1.5 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-48 VT-1.5 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-48 VT-1.5 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Term</source>
            <translation>OC-48 VT-1.5 DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-48 VT-1.5 DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-48 VT-1.5 DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Term</source>
            <translation>OC-48 STS-3c-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Mon</source>
            <translation>OC-48 STS-3c-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 STS-3c-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Term</source>
            <translation>OC-48 STS-1-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Mon</source>
            <translation>OC-48 STS-1-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 STS-1-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Term</source>
            <translation>OC-48 VT-1.5-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Mon</source>
            <translation>OC-48 VT-1.5-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 VT-1.5-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Jitter</source>
            <translation>OC-48 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Jitter Term</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Jitter Term</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Jitter Term</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Jitter Term</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-48 VT-1.5 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-48 VT-1.5 DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Wander</source>
            <translation>OC-48 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Wander Term</source>
            <translation>OC-48 STS-48c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Wander Term</source>
            <translation>OC-48 STS-12c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Wander Term</source>
            <translation>OC-48 STS-3c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Wander Term</source>
            <translation>OC-48 STS-1 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Wander Term</source>
            <translation>OC-48 STS-1 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Wander Term</source>
            <translation>OC-48 VT-1.5 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Wander Term</source>
            <translation>OC-48 VT-1.5 DS1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192</source>
            <translation>OC-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 J-Scan</source>
            <translation>OC-192 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192c Bulk BERT</source>
            <translation>STS-192c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Term</source>
            <translation>OC-192 STS-192c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Thru</source>
            <translation>OC-192 STS-192c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Mon</source>
            <translation>OC-192 STS-192c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-192c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Term</source>
            <translation>OC-192 STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Thru</source>
            <translation>OC-192 STS-48c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Mon</source>
            <translation>OC-192 STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-48c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Term</source>
            <translation>OC-192 STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Thru</source>
            <translation>OC-192 STS-12c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Mon</source>
            <translation>OC-192 STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-12c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Term</source>
            <translation>OC-192 STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Thru</source>
            <translation>OC-192 STS-3c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Mon</source>
            <translation>OC-192 STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-3c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Term</source>
            <translation>OC-192 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Thru</source>
            <translation>OC-192 STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Mon</source>
            <translation>OC-192 STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-1 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Term</source>
            <translation>OC-192 STS-1 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-192 STS-1 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Term</source>
            <translation>OC-192 VT-1.5 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-192 VT-1.5 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-192 VT-1.5 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Term</source>
            <translation>OC-192 VT-1.5 DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-192 VT-1.5 DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-192 VT-1.5 DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Term</source>
            <translation>OC-192 STS-3c-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Mon</source>
            <translation>OC-192 STS-3c-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 STS-3c-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Term</source>
            <translation>OC-192 STS-1-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Mon</source>
            <translation>OC-192 STS-1-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 STS-1-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Term</source>
            <translation>OC-192 VT-1.5-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Mon</source>
            <translation>OC-192 VT-1.5-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 VT-1.5-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768</source>
            <translation>OC-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>光ｾﾙﾌﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>OC-768 Optics Self-Test</source>
            <translation>OC-768 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL BERT</source>
            <translation>STL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Term</source>
            <translation>OC-768 STL ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Mon</source>
            <translation>OC-768 STL ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768c Bulk BERT</source>
            <translation>STS-768c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Term</source>
            <translation>OC-768 STS-768c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Thru</source>
            <translation>OC-768 STS-768c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Mon</source>
            <translation>OC-768 STS-768c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Term</source>
            <translation>OC-768 STS-192c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Thru</source>
            <translation>OC-768 STS-192c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Mon</source>
            <translation>OC-768 STS-192c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Term</source>
            <translation>OC-768 STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Thru</source>
            <translation>OC-768 STS-48c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Mon</source>
            <translation>OC-768 STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Term</source>
            <translation>OC-768 STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Thru</source>
            <translation>OC-768 STS-12c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Mon</source>
            <translation>OC-768 STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Term</source>
            <translation>OC-768 STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Thru</source>
            <translation>OC-768 STS-3c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Mon</source>
            <translation>OC-768 STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT</source>
            <translation>STS-1 ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Term</source>
            <translation>OC-768 STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Thru</source>
            <translation>OC-768 STS-1 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Mon</source>
            <translation>OC-768 STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SDH</source>
            <translation>SDH</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e</source>
            <translation>STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e J-Scan</source>
            <translation>STM-1e J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-4</source>
            <translation>AU-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>VC-4</source>
            <translation>VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-1e AU-3 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-1e AU-3 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-1e AU-3 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Jitter</source>
            <translation>STM-1e ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 ﾊﾞﾙｸ BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-12 ﾊﾞﾙｸ BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-12 ﾊﾞﾙｸ BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Wander</source>
            <translation>STM-1e ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 ﾊﾞﾙｸ BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 ﾊﾞﾙｸ BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-12 ﾊﾞﾙｸ BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 ﾊﾞﾙｸ BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-12 ﾊﾞﾙｸ BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1</source>
            <translation>STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 J-Scan</source>
            <translation>STM-1 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-Xv</source>
            <translation>VC-4-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-4-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3-Xv</source>
            <translation>VC-3-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12-Xv</source>
            <translation>VC-12-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-1 AU-3 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-1 AU-3 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-1 AU-3 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-3 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-3 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Jitter</source>
            <translation>STM-1 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-1 AU-3 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Wander</source>
            <translation>STM-1 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-4 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-1 AU-3 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-1 AU-3 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4</source>
            <translation>STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 J-Scan</source>
            <translation>STM-4 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-4c Bulk BERT</source>
            <translation>VC-4-4c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-4-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-4 AU-3 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-4 AU-3 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-4 AU-3 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-3 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-3 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Jitter</source>
            <translation>STM-4 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-4 AU-3 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Wander</source>
            <translation>STM-4 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-4-4c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-4 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-4 AU-3 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-4 AU-3 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16</source>
            <translation>STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 J-Scan</source>
            <translation>STM-16 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-16c Bulk BERT</source>
            <translation>VC-4-16c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-4-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-16 AU-3 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-16 AU-3 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-16 AU-3 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-3 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-3 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Jitter</source>
            <translation>STM-16 ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Jitter Term</source>
            <translation>STM-16 AU-4 VC-4-16c ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-16 AU-3 VC-12 ﾊﾞﾙｸ ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-12 E1 ｼﾞｯﾀ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Wander</source>
            <translation>STM-16 ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Wander Term</source>
            <translation>STM-16 AU-4 VC-4-16c ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-4 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-16 AU-3 VC-3 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E3 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 ﾜﾝﾀﾞｰ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-16 AU-3 VC-12 ﾊﾞﾙｸ ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-12 E1 ﾜﾝﾀﾞ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64</source>
            <translation>STM-64</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 J-Scan</source>
            <translation>STM-64 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-64c Bulk BERT</source>
            <translation>VC-4-64c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-4-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT ﾄﾞﾛｯﾌﾟ + ｲﾝｻｰﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E3 : E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E3 : E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E3 : E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-64 AU-3 VC-12 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-64 AU-3 VC-12 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-64 AU-3 VC-12 ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-3 VC-3-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-3 VC-12-Xv BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP ﾚｲﾔ 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256</source>
            <translation>STM-256</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 Optics Self-Test</source>
            <translation>STM-256 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL Bert</source>
            <translation>STL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Term</source>
            <translation>STM-256 STL BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Mon</source>
            <translation>STM-256 STL BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-256c Bulk BERT</source>
            <translation>VC-4-256c ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-256c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-256c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-256c Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-64c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-64c Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-16c Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-4c Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4 Bulk BERT</source>
            <translation>VC-4 ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4 Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3 Bulk BERT</source>
            <translation>VC-3 ﾊﾞﾙｸ BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-256 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-256 AU-3 VC-3 ﾊﾞﾙｸ BERT ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-256 AU-3 VC-3 Bulk BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>ｲｰｻﾈｯﾄ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1G</source>
            <translation>10/100/1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth TrueSAM</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>L2 Traffic</source>
            <translation>L2 ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic QuickCheck</source>
            <translation>10/100/1000 L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic</source>
            <translation>L3 ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic QuickCheck</source>
            <translation>10/100/1000 L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic RFC 2544</source>
            <translation>10/100/1000 L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv4</source>
            <translation>L3 ﾄﾗﾌｨｯｸ IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 RFC 2544</source>
            <translation>10/100/1000 L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv6</source>
            <translation>L3 ﾄﾗﾌｨｯｸ IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 RFC 2544</source>
            <translation>10/100/1000 L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv4</source>
            <translation>L4 ﾄﾗﾌｨｯｸ IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv4 RFC 2544</source>
            <translation>10/100/1000 L4 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv6</source>
            <translation>L4 ﾄﾗﾌｨｯｸ IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv6 RFC 2544</source>
            <translation>10/100/1000 L4 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic SAMComplete</source>
            <translation>10/100/1000 L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L2 Multiple Streams</source>
            <translation>L2 複数ｽﾄﾘｰﾑ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Streams SAMComplete</source>
            <translation>10/100/1000 L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 SAMComplete</source>
            <translation>10/100/1000 L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 SAMComplete</source>
            <translation>10/100/1000 L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv4</source>
            <translation>L3 ﾏﾙﾁ ｽﾄﾘｰﾑ IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv4 SAMComplete</source>
            <translation>10/100/1000 L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv6</source>
            <translation>L3 ﾏﾙﾁ ｽﾄﾘｰﾑ IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv6 SAMComplete</source>
            <translation>10/100/1000 L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 TCP Wirespeed</source>
            <translation>L4 TCP Wirespeed</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed SAMComplete</source>
            <translation>10/100/1000 L4 TCP Wirespeed SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed</source>
            <translation>10/100/1000 L4 TCP Wirespeed TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VNF Test</source>
            <translation>VNF ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Mon</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Thru</source>
            <translation>ﾃﾞｭｱﾙ ｽﾙｰ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Dual Thru</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾃﾞｭｱﾙ ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Multiple Streams</source>
            <translation>ﾚｲﾔ 2 ﾏﾙﾁ ｽﾄﾘｰﾑ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Triple Play</source>
            <translation>ﾚｲﾔ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Triple Play</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MiM Traffic</source>
            <translation>ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Mon</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MPLS-TP Traffic</source>
            <translation>ﾚｲﾔ 2 MPLS-TP ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MPLS-TP Traffic Term</source>
            <translation>10/100/1000 Eth ﾚｲﾔ 2 MPLS-TP ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 PTP/1588</source>
            <translation>ﾚｲﾔ 2 PTP/1588</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 PTP/1588 Term</source>
            <translation>10/100/1000 Eth ﾚｲﾔ 2 PTP/1588 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Dual Thru</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾃﾞｭｱﾙ ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Multiple Streams</source>
            <translation>ﾚｲﾔ 3 ﾏﾙﾁ ｽﾄﾘｰﾑ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Triple Play</source>
            <translation>ﾚｲﾔ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Triple Play</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Traffic</source>
            <translation>ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Multiple Streams</source>
            <translation>ﾚｲﾔ 4 ﾏﾙﾁ ｽﾄﾘｰﾑ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback IPv6</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 PTP/1588</source>
            <translation>ﾚｲﾔ 4 PTP/1588</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 PTP/1588 Term</source>
            <translation>10/100/1000 Eth ﾚｲﾔ 4 PTP/1588 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 TCP Wirespeed</source>
            <translation>ﾚｲﾔ 4 TCP ﾜｲﾔ速度</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 TCP Wirespeed Term</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ･ﾚｲﾔ 4 TCP ﾜｲﾔｽﾋﾟｰﾄﾞ時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IP Video</source>
            <translation>IP ﾋﾞﾃﾞｵ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Explorer</source>
            <translation>MPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Explorer</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ IP 映像 MPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Explorer</source>
            <translation>SPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Explorer</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ IP 映像 SPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Analyzer</source>
            <translation>MPTS ｱﾅﾗｲｻﾞ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Analyzer</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ IP 映像 MPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Analyzer</source>
            <translation>SPTS ｱﾅﾗｲｻﾞ</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Analyzer</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ IP 映像 SPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth VoIP Term</source>
            <translation>10/100/1000 Eth VoIP 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>J-Profiler</source>
            <translation>J-Profiler</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth J-Profiler</source>
            <translation>10/100/1000 ｲｰｻﾈｯﾄ J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical</source>
            <translation>100M 光ｲﾝﾀｴｰｽ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth TrueSAM</source>
            <translation>100M 光ｲｰｻﾈｯﾄ TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic QuickCheck</source>
            <translation>100M 光 Eth L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic QuickCheck</source>
            <translation>100M 光 Eth L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic RFC 2544</source>
            <translation>100M 光 Eth L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 RFC 2544</source>
            <translation>100M 光 Eth L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 RFC 2544</source>
            <translation>100M 光 Eth L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv4 RFC 2544</source>
            <translation>100M 光 Eth L4 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv6 RFC 2544</source>
            <translation>100M 光 Eth L4 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic SAMComplete</source>
            <translation>100M 光 Eth L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Streams SAMComplete</source>
            <translation>100M 光 Eth L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 SAMComplete</source>
            <translation>100M 光ｲｰｻﾈｯﾄ L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 SAMComplete</source>
            <translation>100M 光ｲｰｻﾈｯﾄ L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv4 SAMComplete</source>
            <translation>100M 光ｲｰｻﾈｯﾄ L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv6 SAMComplete</source>
            <translation>100M 光ｲｰｻﾈｯﾄ L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor/Thru</source>
            <translation>ﾓﾆﾀ / ｽﾙｰ</translation>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Mon/Thru</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Loopback Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Loopback</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Triple Play</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Term</source>
            <translation>100M Optical ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Mon/Thru</source>
            <translation>100M Optical ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MPLS-TP Traffic Term</source>
            <translation>100M 光ﾚｲﾔ 2 MPLS-TP ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 PTP/1588 Term</source>
            <translation>100M 光 Eth ﾚｲﾔ 2 PTP/1588 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Triple Play</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback IPv6</source>
            <translation>100M Optical ｲｰｻﾈｯﾄ ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 PTP/1588 Term</source>
            <translation>100M 光 Eth ﾚｲﾔ 4 PTP/1588 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Explorer</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ IP 映像 MPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Explorer</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ IP 映像 SPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Analyzer</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ IP 映像 MPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Analyzer</source>
            <translation>100M ｵﾌﾟﾃｨｶﾙ ｲｰｻﾈｯﾄ IP 映像 SPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth VoIP Term</source>
            <translation>100M 光 Eth VoIP 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth J-Profiler</source>
            <translation>100M 光ｲｰｻﾈｯﾄ J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical</source>
            <translation>1GigE 光ｲﾝﾀｴｰｽ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE</source>
            <translation>1GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical TrueSAM</source>
            <translation>1GigE 光 TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic QuickCheck</source>
            <translation>1GigE L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic QuickCheck</source>
            <translation>1GigE L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic RFC 2544</source>
            <translation>1GigE L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>1GigE L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>1GigE L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv4 RFC 2544</source>
            <translation>1GigE L4 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv6 RFC 2544</source>
            <translation>1GigE L4 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic SAMComplete</source>
            <translation>1GigE L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Streams SAMComplete</source>
            <translation>1GigE L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>1GigE L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>1GigE L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv4 SAMComplete</source>
            <translation>1GigE L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv6 SAMComplete</source>
            <translation>1GigE L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed SAMComplete</source>
            <translation>1GigE L4 TCP Wirespeed SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed</source>
            <translation>1GigE L4 TCP Wirespeed TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>1GigE L4 TCP Wirespeed TrueSpeed VNF</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP ﾁｪｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 PTP Check</source>
            <translation>1GigE L4 PTP ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SyncE Wander</source>
            <translation>SyncE Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical SyncE Wander Mon/Thru</source>
            <translation>1GigE 光 SyncE Wander Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 BERT</source>
            <translation>ﾚｲﾔ 1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Term</source>
            <translation>1GigE ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Mon/Thru</source>
            <translation>1GigE ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Patterns</source>
            <translation>ﾚｲﾔ 2 ﾊﾟﾀｰﾝ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Patterns Term</source>
            <translation>1GigE ﾚｲﾔ 2 ﾊﾟﾀｰﾝ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Term</source>
            <translation>1GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Mon/Thru</source>
            <translation>1GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Loopback</source>
            <translation>1GigE ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Term</source>
            <translation>1GigE ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Loopback</source>
            <translation>1GigE ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Triple Play</source>
            <translation>1GigE ﾚｲﾔ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Term</source>
            <translation>1GigE ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Mon/Thru</source>
            <translation>1GigE ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MPLS-TP Traffic Term</source>
            <translation>1GigE ﾚｲﾔ 2 MPLS-TP ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Term</source>
            <translation>1GigE ﾚｲﾔ 2 PTP/1588 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Dual Mon</source>
            <translation>1GigE レイヤ 2 PTP/1588 デュアル モニタ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term</source>
            <translation>1GigE ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term</source>
            <translation>1GigE ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term</source>
            <translation>1GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru</source>
            <translation>1GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback</source>
            <translation>1GigE ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term</source>
            <translation>1GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback</source>
            <translation>1GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback IPv6</source>
            <translation>1GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Triple Play</source>
            <translation>1GigE ﾚｲﾔ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term</source>
            <translation>1GigE ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback</source>
            <translation>1GigE ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback IPv6</source>
            <translation>1GigE ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term</source>
            <translation>1GigE ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback</source>
            <translation>1GigE ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term IPv6</source>
            <translation>1GigE ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback IPv6</source>
            <translation>1GigE ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 PTP/1588 Term</source>
            <translation>1GigE ﾚｲﾔ 4 PTP/1588 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 TCP Wirespeed Term</source>
            <translation>1GigE ﾚｲﾔ 4 TCP ﾜｲﾔｽﾋﾟｰﾄﾞ時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Explorer</source>
            <translation>1GigE IP 映像 MPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Explorer</source>
            <translation>1GigE IP 映像 SPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Analyzer</source>
            <translation>1GigE IP 映像 MPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Analyzer</source>
            <translation>1GigE IP 映像 SPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Eth VoIP Term</source>
            <translation>1GigE Eth VoIP 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE J-Profiler</source>
            <translation>1GigE J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN</source>
            <translation>10GigE LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Optical TrueSAM</source>
            <translation>10 ｷﾞｶﾞﾋﾞｯﾄ ｲｰｻ 光 TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic QuickCheck</source>
            <translation>10GigE LAN L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic QuickCheck</source>
            <translation>10GigE LAN L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic RFC 2544</source>
            <translation>10GigE LAN L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 RFC 2544</source>
            <translation>10GigE LAN L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 RFC 2544</source>
            <translation>10GigE LAN L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv4 RFC 2544</source>
            <translation>10GigE LAN L4 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv6 RFC 2544</source>
            <translation>10GigE LAN L4 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic SAMComplete</source>
            <translation>10GigE LAN L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Streams SAMComplete</source>
            <translation>10GigE LAN L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed SAMComplete</source>
            <translation>10GigE LAN L4 TCP Wirespeed SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed TrueSpeed</source>
            <translation>10GigE LAN L4 TCP Wirespeed TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Term</source>
            <translation>10GigE LAN ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Mon/Thru</source>
            <translation>10GigE LAN ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Term</source>
            <translation>10GigE LAN ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE LAN ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Term</source>
            <translation>10GigE LAN ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 2 Triple Play</source>
            <translation>10GigE ﾚｲﾔ 2 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Term</source>
            <translation>10GigE LAN ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Mon/Thru</source>
            <translation>10GigE LAN ﾚｲﾔ 2 MiM ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MPLS-TP Traffic Term</source>
            <translation>10GigE LAN ﾚｲﾔ 2 MPLS-TP ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 PTP/1588 Term</source>
            <translation>10GigE LAN ﾚｲﾔ 2 PTP/1588 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term</source>
            <translation>10GigE LAN ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term</source>
            <translation>10GigE LAN ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 3 Triple Play</source>
            <translation>10GigE ﾚｲﾔ 3 ﾄﾘﾌﾟﾙﾌﾟﾚｲ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback IPv6</source>
            <translation>10GigE LAN ﾚｲﾔ 4 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 PTP/1588 Term</source>
            <translation>10GigE LAN ﾚｲﾔ 4 PTP/1588 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 TCP Wirespeed Term</source>
            <translation>10GigE LAN ﾚｲﾔ 4 TCP ﾜｲﾔｽﾋﾟｰﾄﾞ時間</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Explorer</source>
            <translation>10GigE IP 映像 MPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Explorer</source>
            <translation>10GigE IP 映像 SPTS ｴｸｽﾌﾟﾛｰﾗ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Analyzer</source>
            <translation>10GigE IP 映像 MPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Analyzer</source>
            <translation>10GigE IP 映像 SPTS ｱﾅﾗｲｻﾞ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN VoIP Term</source>
            <translation>10GigE LAN VoIP 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN</source>
            <translation>10GigE WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Mon/Thru</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Loopback</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Loopback</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Loopback</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c L3 Streams Loopback IPv6</source>
            <translation>10GigE WAN OC-192c ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Mon/Thru</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Loopback</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 2 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Loopback</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 2 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Loopback</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 L3 Streams Loopback IPv6</source>
            <translation>10GigE WAN STM-64 ﾚｲﾔ 3 ｽﾄﾘｰﾑ ﾙｰﾌﾟﾊﾞｯｸ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE</source>
            <translation>40GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Optics Self-Test</source>
            <translation>40GigE 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic QuickCheck</source>
            <translation>40GigE L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic QuickCheck</source>
            <translation>40GigE L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic RFC 2544</source>
            <translation>40GigE L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>40GigE L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>40GigE L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic SAMComplete</source>
            <translation>40GigE L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Streams SAMComplete</source>
            <translation>40GigE L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>40GigE L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>40GigE L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv4 SAMComplete</source>
            <translation>40GigE L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv6 SAMComplete</source>
            <translation>40GigE L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 PCS</source>
            <translation>ﾚｲﾔ 1 PCS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Term</source>
            <translation>40GigE ﾚｲﾔ 1 PCS 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Mon/Thru</source>
            <translation>40GigE ﾚｲﾔ 1 PCS ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Term</source>
            <translation>40GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Mon/Thru</source>
            <translation>40GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Streams Term</source>
            <translation>40GigE ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term</source>
            <translation>40GigE ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term IPv6</source>
            <translation>40GigE ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term</source>
            <translation>40GigE ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term IPv6</source>
            <translation>40GigE ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term</source>
            <translation>40GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru</source>
            <translation>40GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term IPv6</source>
            <translation>40GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>40GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term</source>
            <translation>40GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term IPv6</source>
            <translation>40GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE</source>
            <translation>100GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Optics Self-Test</source>
            <translation>100GigE 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic QuickCheck</source>
            <translation>100GigE L2 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic QuickCheck</source>
            <translation>100GigE L3 ﾄﾗﾌｨｯｸ QuickCheck</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic RFC 2544</source>
            <translation>100GigE L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>100GigE L3 ﾄﾗﾌｨｯｸ IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>100GigE L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic SAMComplete</source>
            <translation>100GigE L2 ﾄﾗﾌｨｯｸ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Streams SAMComplete</source>
            <translation>100GigE L2 ｽﾄﾘｰﾑ SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>100GigE L3 ﾄﾗﾌｨｯｸ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>100GigE L3 ﾄﾗﾌｨｯｸ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv4 SAMComplete</source>
            <translation>100GigE L3 ｽﾄﾘｰﾑ IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv6 SAMComplete</source>
            <translation>100GigE L3 ｽﾄﾘｰﾑ IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Term</source>
            <translation>100GigE ﾚｲﾔ 1 PCS 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Mon/Thru</source>
            <translation>100GigE ﾚｲﾔ 1 PCS ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term</source>
            <translation>100GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon/Thru</source>
            <translation>100GigE ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term</source>
            <translation>100GigE ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term</source>
            <translation>100GigE ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6</source>
            <translation>100GigE ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term</source>
            <translation>100GigE ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6</source>
            <translation>100GigE ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term</source>
            <translation>100GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru</source>
            <translation>100GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6</source>
            <translation>100GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>100GigE ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term</source>
            <translation>100GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6</source>
            <translation>100GigE ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE RS-FEC</source>
            <translation>100GigE RS-FEC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term RS-FEC</source>
            <translation>100GigE レイヤ 2 トラフィック条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon RS-FEC</source>
            <translation>100GigE レイヤ 2 トラフィック モニタ RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term RS-FEC</source>
            <translation>100GigE レイヤ 2 ストリーム条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term RS-FEC</source>
            <translation>100GigE レイヤ 3 Ping 条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6 RS-FEC</source>
            <translation>100GigE レイヤ 3 Ping 条件 IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term RS-FEC</source>
            <translation>100GigE レイヤ 3 Traceroute 条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6 RS-FEC</source>
            <translation>100GigE レイヤ 3 Traceroute 条件 IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term RS-FEC</source>
            <translation>100GigE レイヤ 3 トラフィック条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon RS-FEC</source>
            <translation>100GigE レイヤ 3 トラフィック モニタ RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6 RS-FEC</source>
            <translation>100GigE レイヤ 3 トラフィック条件 IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon IPv6 RS-FEC</source>
            <translation>100GigE レイヤ 3 トラフィック モニタ IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term RS-FEC</source>
            <translation>100GigE レイヤ 3 ストリーム条件 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6 RS-FEC</source>
            <translation>100GigE レイヤ 3 ストリーム条件 IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig</source>
            <translation>1Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1G FC</source>
            <translation>1G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>1Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>1Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>1Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾊﾟﾀｰﾝ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>1Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>1Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig</source>
            <translation>2Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2G FC</source>
            <translation>2G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>2Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>2Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>2Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾊﾟﾀｰﾝ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>2Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>2Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig</source>
            <translation>4Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4G FC</source>
            <translation>4G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>4Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>4Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>4Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾊﾟﾀｰﾝ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>4Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>4Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig</source>
            <translation>8Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8G FC</source>
            <translation>8G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>8 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 2 ﾊﾟﾀｰﾝ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>8 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>8 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig</source>
            <translation>10Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G FC</source>
            <translation>10G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>10Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>10Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>10Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>10Gig ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig</source>
            <translation>16Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16G FC</source>
            <translation>16G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>16 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>16 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>16 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>16 ｷﾞｶﾞﾋﾞｯﾄ ﾌｧｲﾊﾞ ﾁｬﾈﾙ ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI</source>
            <translation>614.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI ﾁｪｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Check</source>
            <translation>614.4M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Term</source>
            <translation>614.4M CPRI ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>614.4M CPRI ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 BERT</source>
            <translation>ﾚｲﾔ 2 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Term</source>
            <translation>614.4M CPRI ﾚｲﾔ 2 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>614.4M CPRI ﾚｲﾔ 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI</source>
            <translation>1228.8M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Check</source>
            <translation>1228.8M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Term</source>
            <translation>1228.8M CPRI ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>1228.8M CPRI ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Term</source>
            <translation>1228.8M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>1228.8M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI</source>
            <translation>2457.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Check</source>
            <translation>2457.6M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Term</source>
            <translation>2457.6M CPRI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>2457.6M CPRI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Term</source>
            <translation>2457.6M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>2457.6M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI</source>
            <translation>3072.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Check</source>
            <translation>3072.0M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Term</source>
            <translation>3072.0M CPRI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>3072.0M CPRI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Term</source>
            <translation>3072.0M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>3072.0M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI</source>
            <translation>4915.2M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Check</source>
            <translation>4915.2M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Term</source>
            <translation>4915.2M CPRI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>4915.2M CPRI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Term</source>
            <translation>4915.2M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>4915.2M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI</source>
            <translation>6144.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Check</source>
            <translation>6144.0M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Term</source>
            <translation>6144.0M CPRI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>6144.0M CPRI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Term</source>
            <translation>6144.0M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>6144.0M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI</source>
            <translation>9830.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Check</source>
            <translation>9830.4M CPRI ﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Term</source>
            <translation>9830.4M CPRI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>9830.4M CPRI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Term</source>
            <translation>9830.4M CPRI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>9830.4M CPRI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI</source>
            <translation>10137.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Check</source>
            <translation>10137.6M CPRI テスト</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Term</source>
            <translation>10137.6M CPRI ﾚｲﾔ 2 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>10137.6M CPRI ﾚｲﾔ 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI</source>
            <translation>768M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Term</source>
            <translation>768M OBSAI ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>768M OBSAI ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Term</source>
            <translation>768M OBSAI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>768M OBSAI ﾚｲﾔ 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI</source>
            <translation>1536M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Term</source>
            <translation>1536M OBSAI ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>1536M OBSAI ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Term</source>
            <translation>1536M OBSAI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>1536M OBSAI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI</source>
            <translation>3072M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Term</source>
            <translation>3072M OBSAI ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>3072M OBSAI ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Term</source>
            <translation>3072M OBSAI ﾚｲﾔ 2 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>3072M OBSAI ﾚｲﾔ 2 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI</source>
            <translation>6144M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Term</source>
            <translation>6144M OBSAI ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>6144M OBSAI ﾚｲﾔ 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Term</source>
            <translation>6144M OBSAI ﾚｲﾔ 2 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>6144M OBSAI ﾚｲﾔ 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1</source>
            <translation>OTU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN ﾁｪｯｸ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G OTN Check</source>
            <translation>OTU1 2.7G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Term</source>
            <translation>OTU1 2.7G ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G バルク BERT モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48</source>
            <translation>STS-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-3 VC-3 バルク BERT モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU0</source>
            <translation>ODU0</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Term</source>
            <translation>OTU1 2.7G ODU0 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 バルク BERT モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>OTU1 2.7G ODU0 GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>OTU1 2.7G ODU0 GMP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 GMP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Jitter</source>
            <translation>OTU1 2.7G ｼﾞｯﾀ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G ﾊﾞﾙｸ ｼﾞｯﾀ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G STS-48c ﾊﾞﾙｸ ｼﾞｯﾀ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ ｼﾞｯﾀ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Wander</source>
            <translation>OTU1 2.7G ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G ﾊﾞﾙｸ ﾜﾝﾀﾞｰ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G STS-48c ﾊﾞﾙｸ ﾜﾝﾀﾞ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c ﾊﾞﾙｸ ﾜﾝﾀﾞｰ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2</source>
            <translation>OTU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G OTN Check</source>
            <translation>OTU2 10.7G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Term</source>
            <translation>OTU2 10.7G ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192</source>
            <translation>STS-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-192c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-192c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU1</source>
            <translation>ODU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Term</source>
            <translation>OTU2 10.7G ODU1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Term</source>
            <translation>OTU2 10.7G ODU0 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>OTU2 10.7G ODU0 GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>OTU2 10.7G ODU0 GMP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 GMP ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODUflex</source>
            <translation>ODUflex</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Term</source>
            <translation>OTU2 10.7G ODUflex ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Mon</source>
            <translation>OTU2 10.7G ODUflex ﾊﾞﾙｸ BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU2 10.7G ODUflex GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e</source>
            <translation>OTU1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G OTN Check</source>
            <translation>OTU1e 11.05G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Term</source>
            <translation>OTU1e 11.05G ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Mon/Thru</source>
            <translation>OTU1e 11.05G ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Term</source>
            <translation>OTU1e 11.05G ﾚｲﾔ 1 BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Mon/Thru</source>
            <translation>OTU1e 11.05G ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Term</source>
            <translation>OTU1e 11.05G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU1e 11.05G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e</source>
            <translation>OTU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G OTN Check</source>
            <translation>OTU2e 11.1G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Term</source>
            <translation>OTU2e 11.1G ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Mon/Thru</source>
            <translation>OTU2e 11.1G ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Term</source>
            <translation>OTU2e 11.1G ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Mon/Thru</source>
            <translation>OTU2e 11.1G ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Term</source>
            <translation>OTU2e 11.1G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU2e 11.1G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3</source>
            <translation>OTU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G Optics Self-Test</source>
            <translation>OTL3.4 43.02G 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3.4 43.02G OTN Check</source>
            <translation>OTU3.4 43.02G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L2 Traffic RFC 2544</source>
            <translation>OTL3.4 43.02G L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic RFC 2544</source>
            <translation>OTL3.4 43.02G L3 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic IPv6 RFC 2544</source>
            <translation>OTL3.4 43.02G L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL BERT</source>
            <translation>OTL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Term</source>
            <translation>OTL3.4 43.02G OTL BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Mon</source>
            <translation>OTL3.4 43.02G OTL BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Term</source>
            <translation>OTU3 43.02G ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Monitor/Thru</source>
            <translation>OTU3 43.02G レイヤ 2 トラフィック モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Streams Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term IPv6</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term IPv6</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term IPv6</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term IPv6</source>
            <translation>OTU3 43.02G ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768</source>
            <translation>STS-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-768c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-768c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-192c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-192c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-48c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-48c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-12c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-12c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-3c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-3c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c バルク BERT モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-64c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-64c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-16c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-16c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-4c ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-4c ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-3 VC-3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-3 VC-3 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2e</source>
            <translation>ODU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2e ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Term</source>
            <translation>OTU3 43.02G ODU2e ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2e ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2</source>
            <translation>ODU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU1 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 バルク BERT モニタ/スルー</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU0 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex Bulk BERT Term</source>
            <translation>OTU3 43.02G ODUflex ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODUflex GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4</source>
            <translation>OTU4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G Optics Self-Test</source>
            <translation>OTL4.10 111.8G 光ｾﾙﾌﾃｽﾄ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G OTN Check</source>
            <translation>OTU4 111.8G OTN ﾁｪｯｸ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L2 Traffic RFC 2544</source>
            <translation>OTU4 111.8G L2 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic RFC 2544</source>
            <translation>OTU4 111.8G L3 ﾄﾗﾌｨｯｸ RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic IPv6 RFC 2544</source>
            <translation>OTU4 111.8G L3 ﾄﾗﾌｨｯｸ IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Term</source>
            <translation>OTL4.10 111.8G OTL BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Mon</source>
            <translation>OTL4.10 111.8G OTL BERT ﾓﾆﾀ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Term</source>
            <translation>OTU4 111.8G ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Streams Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 2 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 Ping 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term IPv6</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 Ping 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 Traceroute 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term IPv6</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 Traceroute 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term IPv6</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term IPv6</source>
            <translation>OTU4 111.8G ﾚｲﾔ 3 ｽﾄﾘｰﾑ 終端 IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU3</source>
            <translation>ODU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU3 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU3 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2e ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Term</source>
            <translation>OTU4 111.8G ODU2e ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2e ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU1 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾊﾞﾙｸ BERT 項目</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU0 ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 ﾊﾞﾙｸ BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 ﾚｲﾔ 3 ﾄﾗﾌｨｯｸ ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex Bulk BERT Term</source>
            <translation>OTU4 111.8G ODUflex ﾊﾞﾙｸ BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODUflex GMP ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Optical BERT</source>
            <translation>光 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Term</source>
            <translation>3072.0M 光 ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Mon/Thru</source>
            <translation>3072.0M 光 ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Term</source>
            <translation>9830.4M 光 ﾚｲﾔ 1 BERT 終端</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Mon/Thru</source>
            <translation>9830.4M 光 ﾚｲﾔ 1 BERT ﾓﾆﾀ / ｽﾙｰ</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing</source>
            <translation>ﾀｲﾐﾝｸﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1PPS</source>
            <translation>1PPS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>解析</translation>
        </message>
        <message utf8="true">
            <source>1PPS Analysis</source>
            <translation>1PPS 解析</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Wander</source>
            <translation>2MHz Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Clock Wander Analysis</source>
            <translation>2MHz ｸﾛｯｸ Wander Analysis</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Wander</source>
            <translation>10MHz ﾜﾝﾀﾞ</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Clock Wander Analysis</source>
            <translation>10MHz ｸﾛｯｸ Wander Analysis</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Benchmark</source>
            <translation>ﾍﾞﾝﾁﾏｰｸ</translation>
            <comment>This text appears in the main Test menu.&#xA;This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>タイミング モジュール</translation>
        </message>
    </context>
</TS>
