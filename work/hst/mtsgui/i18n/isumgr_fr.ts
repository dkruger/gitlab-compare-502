<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Adresse IP invalide.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>Affectation de passerelle invalide : 127.29.0.7. Recommencez le système MTS.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>L'adresse IP a changé.  Redémarrer le Module BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>Initialisation du module BERT. Demande OFF rejetée.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>Initialisation du module BERT avec la fonction optique de gigue. Demande OFF rejetée.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>Le module BERT s'arrête</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>Module BERT hors service</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>Le module BERT est éteint. Appuyez sur HOME/SYSTEM, puis sur l'icône BERT pour l'activer</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>Module BERT en service</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** CHARGEMENT DU LOGICIEL D' APPLICATION *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** MISE A JOUR TERMINE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** LOGICIEL DE GIGUE EN COURS DE MISE A JOUR *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** MISE A JOUR DU CUR DU LOGICIEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** DEMARRAGE DU CHARGEMENT DE L'UPGRADE *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>L'interface du Module BERT a échoué</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Démarrage du module BERT avec la fonction de Gigue Optique</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>Lancement du Module BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>Echec du Module BERT.  Module hors service</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Module 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Module 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Module 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Module 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Module </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Module 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Module 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Module 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Module 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Module 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Module 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Module 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Module 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Module 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Module 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Module 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Module 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Module 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Module 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Module 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Module 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Module 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Module 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Mise à jour du logiciel de base requise. Révision actuelle non compatible avec le Module BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>La réinstallation du logiciel BERT est nécessaire. Le logiciel BERT approprié n'est pas installé pour le module BERT joint.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>Une mise à niveau matérielle du BERT est nécessaire. Le matériel actuel n'est pas compatible.</translation>
        </message>
    </context>
</TS>
