<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo para el Informe</translation>
        </message>
        <message utf8="true">
            <source>Device Under Test</source>
            <translation>Equipo bajo prueba</translation>
        </message>
        <message utf8="true">
            <source>Owner</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>Software Revision</source>
            <translation>Revisión Software</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>Comment</source>
            <translation>Comentario</translation>
        </message>
        <message utf8="true">
            <source>Tester Comments</source>
            <translation>Comentarios del Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Resultados de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Tester</source>
            <translation>Técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Instrument</source>
            <translation>Instrumento de Medida</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modelo</translation>
        </message>
        <message utf8="true">
            <source>Viavi -BASE_MODEL- -MODULE_NAME-</source>
            <translation>Viavi -BASE_MODEL- -MODULE_NAME-</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Revisión de software</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SOFTWARE_VERSION-</source>
            <translation>-MODULE_SOFTWARE_VERSION-</translation>
        </message>
        <message utf8="true">
            <source>BERT Serial Number</source>
            <translation>BERT Serial Number</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SERIAL-</source>
            <translation>-MODULE_SERIAL-</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom</source>
            <translation>Datacom HS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT</source>
            <translation>BERT Datacom HS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Terminate</source>
            <translation>Terminar</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT Term</source>
            <translation>Term BERT Datacom HS</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Diphase</source>
            <translation>Difase</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT</source>
            <translation>BERT de difase</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT Term</source>
            <translation>Term BERT de difase</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1/DS3</source>
            <translation>DS1/DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1</source>
            <translation>DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT</source>
            <translation>DS1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Term</source>
            <translation>DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Thru</source>
            <translation>Trsp</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Thru</source>
            <translation>DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Single Monitor</source>
            <translation>Single Monitor</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Single Mon</source>
            <translation>DS1 BERT Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Monitor</source>
            <translation>Dual Monitor</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Dual Mon</source>
            <translation>DS1 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander</source>
            <translation>DS1 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander Single Mon</source>
            <translation>Desviación única DS1 Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling</source>
            <translation>Señalización  DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Term</source>
            <translation>Term señalización DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Dual Mon</source>
            <translation>Mon dual señalización DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI</source>
            <translation>DS1 RDSI PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Term</source>
            <translation>Term PRI RDSI DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Dual Mon</source>
            <translation>Monitor dual PRI RDSI DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF</source>
            <translation>DS1 VF</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Term</source>
            <translation>DS1 VF Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Dual Mon</source>
            <translation>DS1 VF Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC</source>
            <translation>DS1 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC Dual Mon</source>
            <translation>DS1 HDLC Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP</source>
            <translation>DS1 PPP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP Ping Term</source>
            <translation>Término ping DS1 PPP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Jitter</source>
            <translation>DS1 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Jitter Term</source>
            <translation>DS1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Rx Jitter Term</source>
            <translation>DS1 BERT Rx Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Wander Term</source>
            <translation>DS1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3</source>
            <translation>DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT</source>
            <translation>DS3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Term</source>
            <translation>DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Thru</source>
            <translation>DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Single Mon</source>
            <translation>DS3 BERT Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Dual Mon</source>
            <translation>DS3 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT</source>
            <translation>E1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Term</source>
            <translation>DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Thru</source>
            <translation>DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Dual Mon</source>
            <translation>DS3 : E1 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Term</source>
            <translation>DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Thru</source>
            <translation>DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Dual Mon</source>
            <translation>DS3 : DS1 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC</source>
            <translation>DS3 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC Dual Mon</source>
            <translation>DS3 HDLC Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Jitter</source>
            <translation>DS3 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Jitter Term</source>
            <translation>DS3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Rx Jitter Term</source>
            <translation>DS3 BERT Rx Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Jitter Term</source>
            <translation>DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Rx Jitter Term</source>
            <translation>Variación de retardo DS3 : E1 BERT Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Jitter Term</source>
            <translation>DS3 : DS1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Rx Jitter Term</source>
            <translation>Variación de retardo DS3 : DS1 BERT Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Wander</source>
            <translation>DS3 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Wander Term</source>
            <translation>DS3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Wander Term</source>
            <translation>DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Wander Term</source>
            <translation>DS3 : DS1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1/E3/E4</source>
            <translation>E1/E3/E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1</source>
            <translation>E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Term</source>
            <translation>E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Thru</source>
            <translation>E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Single Mon</source>
            <translation>E1 BERT Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Dual Mon</source>
            <translation>E1 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander</source>
            <translation>E1 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander Single Mon</source>
            <translation>Desviación única E1 Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI</source>
            <translation>PRI RDSI E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI Term</source>
            <translation>Plazo de PRI RDSI E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Jitter</source>
            <translation>E1 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Jitter Term</source>
            <translation>E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Wander Term</source>
            <translation>E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3</source>
            <translation>E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT</source>
            <translation>E3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Term</source>
            <translation>E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Thru</source>
            <translation>E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor</source>
            <translation>Monitor</translation>
        </message>
        <message utf8="true">
            <source>E3 BERT Mon</source>
            <translation>E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Term</source>
            <translation>E3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Thru</source>
            <translation>E3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Mon</source>
            <translation>E3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Jitter</source>
            <translation>E3 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Jitter Term</source>
            <translation>E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Jitter Term</source>
            <translation>E3 : E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Wander</source>
            <translation>E3 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Wander Term</source>
            <translation>E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Wander Term</source>
            <translation>E3 : E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4</source>
            <translation>E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT</source>
            <translation>E4 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Term</source>
            <translation>E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Thru</source>
            <translation>E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Mon</source>
            <translation>E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Term</source>
            <translation>E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Thru</source>
            <translation>E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Mon</source>
            <translation>E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Term</source>
            <translation>E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Thru</source>
            <translation>E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Mon</source>
            <translation>E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Jitter</source>
            <translation>E4 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Jitter Term</source>
            <translation>E4 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Jitter Term</source>
            <translation>E4 : E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Jitter Term</source>
            <translation>E4 : E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Wander</source>
            <translation>E4 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Wander Term</source>
            <translation>E4 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Wander Term</source>
            <translation>E4 : E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Wander Term</source>
            <translation>E4 : E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SONET</source>
            <translation>SONET</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>J-Scan</source>
            <translation>J-Scan</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 J-Scan</source>
            <translation>STS-1 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Bulk BERT</source>
            <translation>Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Term</source>
            <translation>STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Thru</source>
            <translation>STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Mon</source>
            <translation>STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Drop+Insert</source>
            <translation>Drop+Insert</translation>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Drop+Insert</source>
            <translation>STS-1 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Dual Mon</source>
            <translation>STS-1 Bulk BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Term</source>
            <translation>STS-1 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Thru</source>
            <translation>STS-1 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Mon</source>
            <translation>STS-1 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Drop+Insert</source>
            <translation>STS-1 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Term</source>
            <translation>STS-1 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Thru</source>
            <translation>STS-1 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Mon</source>
            <translation>STS-1 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Term</source>
            <translation>STS-1 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Thru</source>
            <translation>STS-1 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Mon</source>
            <translation>STS-1 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Term</source>
            <translation>STS-1 VT-1.5 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Thru</source>
            <translation>STS-1 VT-1.5 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Single Mon</source>
            <translation>STS-1 VT-1.5 Bulk BERT Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 Bulk BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Term</source>
            <translation>STS-1 VT-1.5 DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Thru</source>
            <translation>STS-1 VT-1.5 DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Single Mon</source>
            <translation>STS-1 VT-1.5 DS1 BERT Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 DS1 BERT Dual Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Jitter</source>
            <translation>STS-1 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Jitter Term</source>
            <translation>STS-1 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Jitter Term</source>
            <translation>STS-1 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Jitter Term</source>
            <translation>STS-1 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Jitter Term</source>
            <translation>STS-1 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Jitter Term</source>
            <translation>STS-1 VT-1.5 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Jitter Term</source>
            <translation>STS-1 VT-1.5 DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Wander</source>
            <translation>STS-1 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Wander Term</source>
            <translation>STS-1 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Wander Term</source>
            <translation>STS-1 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Wander Term</source>
            <translation>STS-1 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Wander Term</source>
            <translation>STS-1 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Wander Term</source>
            <translation>STS-1 VT-1.5 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Wander Term</source>
            <translation>STS-1 VT-1.5 DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3</source>
            <translation>OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 J-Scan</source>
            <translation>OC-3 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c Bulk BERT</source>
            <translation>STS-3c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Term</source>
            <translation>OC-3 STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Thru</source>
            <translation>OC-3 STS-3c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Mon</source>
            <translation>OC-3 STS-3c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-3 STS-3c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Term</source>
            <translation>OC-3 STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Thru</source>
            <translation>OC-3 STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Mon</source>
            <translation>OC-3 STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-3 STS-1 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Term</source>
            <translation>OC-3 STS-1 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-3 STS-1 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Term</source>
            <translation>OC-3 VT-1.5 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-3 VT-1.5 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-3 VT-1.5 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Term</source>
            <translation>OC-3 VT-1.5 DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-3 VT-1.5 DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-3 VT-1.5 DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c-Xv</source>
            <translation>STS-3c-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Term</source>
            <translation>OC-3 STS-3c-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Mon</source>
            <translation>OC-3 STS-3c-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>GFP Ethernet</source>
            <translation>GFP Ethernet</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Traffic</source>
            <translation>Capa 2 Tráfico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traffic</source>
            <translation>Capa 3 Tráfico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Ping</source>
            <translation>Capa 3 Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traceroute</source>
            <translation>Capa 3 Traceroute</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 STS-3c-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1-Xv</source>
            <translation>STS-1-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Term</source>
            <translation>OC-3 STS-1-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Mon</source>
            <translation>OC-3 STS-1-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 STS-1-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 STS-1-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 STS-1-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 STS-1-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 STS-1-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 STS-1-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5-Xv</source>
            <translation>VT-1.5-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Term</source>
            <translation>OC-3 VT-1.5-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Mon</source>
            <translation>OC-3 VT-1.5-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-3 VT-1.5-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Jitter</source>
            <translation>OC-3 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Jitter Term</source>
            <translation>OC-3 STS-3c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Jitter Term</source>
            <translation>OC-3 STS-1 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-3 VT-1.5 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-3 VT-1.5 DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Wander</source>
            <translation>OC-3 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Wander Term</source>
            <translation>OC-3 STS-3c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Wander Term</source>
            <translation>OC-3 STS-1 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Wander Term</source>
            <translation>OC-3 STS-1 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Wander Term</source>
            <translation>OC-3 VT-1.5 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Wander Term</source>
            <translation>OC-3 VT-1.5 DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12</source>
            <translation>OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 J-Scan</source>
            <translation>OC-12 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-12c Bulk BERT</source>
            <translation>STS-12c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Term</source>
            <translation>OC-12 STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Thru</source>
            <translation>OC-12 STS-12c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Mon</source>
            <translation>OC-12 STS-12c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-12c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Term</source>
            <translation>OC-12 STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Thru</source>
            <translation>OC-12 STS-3c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Mon</source>
            <translation>OC-12 STS-3c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-3c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Term</source>
            <translation>OC-12 STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Thru</source>
            <translation>OC-12 STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Mon</source>
            <translation>OC-12 STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-12 STS-1 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Term</source>
            <translation>OC-12 STS-1 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-12 STS-1 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Term</source>
            <translation>OC-12 VT-1.5 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-12 VT-1.5 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-12 VT-1.5 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Term</source>
            <translation>OC-12 VT-1.5 DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-12 VT-1.5 DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-12 VT-1.5 DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Term</source>
            <translation>OC-12 STS-3c-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Mon</source>
            <translation>OC-12 STS-3c-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 STS-3c-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Term</source>
            <translation>OC-12 STS-1-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Mon</source>
            <translation>OC-12 STS-1-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 STS-1-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 STS-1-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 STS-1-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 STS-1-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 STS-1-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 STS-1-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Term</source>
            <translation>OC-12 VT-1.5-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Mon</source>
            <translation>OC-12 VT-1.5-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-12 VT-1.5-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Jitter</source>
            <translation>OC-12 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Jitter Term</source>
            <translation>OC-12 STS-12c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Jitter Term</source>
            <translation>OC-12 STS-3c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Jitter Term</source>
            <translation>OC-12 STS-1 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-12 VT-1.5 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-12 VT-1.5 DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Wander</source>
            <translation>OC-12 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Wander Term</source>
            <translation>OC-12 STS-12c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Wander Term</source>
            <translation>OC-12 STS-3c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Wander Term</source>
            <translation>OC-12 STS-1 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Wander Term</source>
            <translation>OC-12 STS-1 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Wander Term</source>
            <translation>OC-12 VT-1.5 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Wander Term</source>
            <translation>OC-12 VT-1.5 DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48</source>
            <translation>OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 J-Scan</source>
            <translation>OC-48 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48c Bulk BERT</source>
            <translation>STS-48c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Term</source>
            <translation>OC-48 STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Thru</source>
            <translation>OC-48 STS-48c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Mon</source>
            <translation>OC-48 STS-48c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-48c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Term</source>
            <translation>OC-48 STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Thru</source>
            <translation>OC-48 STS-12c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Mon</source>
            <translation>OC-48 STS-12c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-12c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Term</source>
            <translation>OC-48 STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Thru</source>
            <translation>OC-48 STS-3c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Mon</source>
            <translation>OC-48 STS-3c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-3c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Term</source>
            <translation>OC-48 STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Thru</source>
            <translation>OC-48 STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Mon</source>
            <translation>OC-48 STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-48 STS-1 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Term</source>
            <translation>OC-48 STS-1 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-48 STS-1 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Term</source>
            <translation>OC-48 VT-1.5 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-48 VT-1.5 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-48 VT-1.5 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Term</source>
            <translation>OC-48 VT-1.5 DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-48 VT-1.5 DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-48 VT-1.5 DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Term</source>
            <translation>OC-48 STS-3c-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Mon</source>
            <translation>OC-48 STS-3c-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 STS-3c-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Term</source>
            <translation>OC-48 STS-1-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Mon</source>
            <translation>OC-48 STS-1-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 STS-1-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 STS-1-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 STS-1-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 STS-1-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 STS-1-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 STS-1-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Term</source>
            <translation>OC-48 VT-1.5-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Mon</source>
            <translation>OC-48 VT-1.5-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-48 VT-1.5-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Jitter</source>
            <translation>OC-48 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Jitter Term</source>
            <translation>OC-48 STS-48c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Jitter Term</source>
            <translation>OC-48 STS-12c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Jitter Term</source>
            <translation>OC-48 STS-3c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Jitter Term</source>
            <translation>OC-48 STS-1 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Jitter Term</source>
            <translation>OC-48 VT-1.5 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-48 VT-1.5 DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Wander</source>
            <translation>OC-48 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Wander Term</source>
            <translation>OC-48 STS-48c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Wander Term</source>
            <translation>OC-48 STS-12c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Wander Term</source>
            <translation>OC-48 STS-3c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Wander Term</source>
            <translation>OC-48 STS-1 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Wander Term</source>
            <translation>OC-48 STS-1 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Wander Term</source>
            <translation>OC-48 VT-1.5 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Wander Term</source>
            <translation>OC-48 VT-1.5 DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192</source>
            <translation>OC-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 J-Scan</source>
            <translation>OC-192 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192c Bulk BERT</source>
            <translation>STS-192c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Term</source>
            <translation>OC-192 STS-192c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Thru</source>
            <translation>OC-192 STS-192c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Mon</source>
            <translation>OC-192 STS-192c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-192c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Term</source>
            <translation>OC-192 STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Thru</source>
            <translation>OC-192 STS-48c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Mon</source>
            <translation>OC-192 STS-48c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-48c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Term</source>
            <translation>OC-192 STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Thru</source>
            <translation>OC-192 STS-12c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Mon</source>
            <translation>OC-192 STS-12c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-12c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Term</source>
            <translation>OC-192 STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Thru</source>
            <translation>OC-192 STS-3c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Mon</source>
            <translation>OC-192 STS-3c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-3c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Term</source>
            <translation>OC-192 STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Thru</source>
            <translation>OC-192 STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Mon</source>
            <translation>OC-192 STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Drop+Insert</source>
            <translation>OC-192 STS-1 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Term</source>
            <translation>OC-192 STS-1 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Drop+Insert</source>
            <translation>OC-192 STS-1 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Term</source>
            <translation>OC-192 VT-1.5 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Thru</source>
            <translation>OC-192 VT-1.5 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Mon</source>
            <translation>OC-192 VT-1.5 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Term</source>
            <translation>OC-192 VT-1.5 DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-192 VT-1.5 DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-192 VT-1.5 DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Term</source>
            <translation>OC-192 STS-3c-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Mon</source>
            <translation>OC-192 STS-3c-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 STS-3c-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Term</source>
            <translation>OC-192 STS-1-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Mon</source>
            <translation>OC-192 STS-1-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 STS-1-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 STS-1-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 STS-1-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 STS-1-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 STS-1-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 STS-1-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Term</source>
            <translation>OC-192 VT-1.5-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Mon</source>
            <translation>OC-192 VT-1.5-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>OC-192 VT-1.5-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768</source>
            <translation>OC-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica</translation>
        </message>
        <message utf8="true">
            <source>OC-768 Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL BERT</source>
            <translation>STL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Term</source>
            <translation>OC-768 STL Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Mon</source>
            <translation>OC-768 STL Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768c Bulk BERT</source>
            <translation>STS-768c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Term</source>
            <translation>OC-768 STS-768c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Thru</source>
            <translation>OC-768 STS-768c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Mon</source>
            <translation>OC-768 STS-768c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Term</source>
            <translation>OC-768 STS-192c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Thru</source>
            <translation>OC-768 STS-192c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Mon</source>
            <translation>OC-768 STS-192c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Term</source>
            <translation>OC-768 STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Thru</source>
            <translation>OC-768 STS-48c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Mon</source>
            <translation>OC-768 STS-48c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Term</source>
            <translation>OC-768 STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Thru</source>
            <translation>OC-768 STS-12c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Mon</source>
            <translation>OC-768 STS-12c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Term</source>
            <translation>OC-768 STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Thru</source>
            <translation>OC-768 STS-3c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Mon</source>
            <translation>OC-768 STS-3c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT</source>
            <translation>STS-1 Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Term</source>
            <translation>OC-768 STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Thru</source>
            <translation>OC-768 STS-1 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Mon</source>
            <translation>OC-768 STS-1 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SDH</source>
            <translation>SDH</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e</source>
            <translation>STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e J-Scan</source>
            <translation>STM-1e J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-4</source>
            <translation>AU-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>VC-4</source>
            <translation>VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-1e AU-4 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-1e AU-4 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-1e AU-4 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-1e AU-3 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-1e AU-3 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-1e AU-3 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Jitter</source>
            <translation>STM-1e Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 Bulk BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-12 Bulk BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-12 Bulk BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Wander</source>
            <translation>STM-1e Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 Bulk BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 Bulk BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : DS1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-12 Bulk BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-12 E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 Bulk BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : DS1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : E3 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-12 Bulk BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-12 E1 BERT Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1</source>
            <translation>STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 J-Scan</source>
            <translation>STM-1 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-1 AU-4 VC-4 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-1 AU-4 VC-4 E4 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-1 AU-4 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-1 AU-4 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-1 AU-4 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-Xv</source>
            <translation>VC-4-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-4-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-4-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3-Xv</source>
            <translation>VC-3-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12-Xv</source>
            <translation>VC-12-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-4 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-4 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-1 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 E3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-1 AU-3 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-1 AU-3 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-1 AU-3 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-3 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-3 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-3 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-1 AU-3 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Jitter</source>
            <translation>STM-1 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-1 AU-4 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-1 AU-3 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Wander</source>
            <translation>STM-1 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-4 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-1 AU-4 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-1 AU-3 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-1 AU-3 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4</source>
            <translation>STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 J-Scan</source>
            <translation>STM-4 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-4c Bulk BERT</source>
            <translation>VC-4-4c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-4 AU-4 VC-4 E4 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-4 AU-4 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-4 AU-4 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-4 AU-4 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-4-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-4-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-4 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-4 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-4 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 E3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-4 AU-3 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-4 AU-3 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-4 AU-3 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-3 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-3 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-3 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-4 AU-3 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Jitter</source>
            <translation>STM-4 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-4 AU-4 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-4 AU-3 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Wander</source>
            <translation>STM-4 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-4-4c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-4 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-4 AU-4 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-4 AU-3 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-4 AU-3 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16</source>
            <translation>STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 J-Scan</source>
            <translation>STM-16 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-16c Bulk BERT</source>
            <translation>VC-4-16c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4-16c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-16c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4-16c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-16 AU-4 VC-4 E4 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-16 AU-4 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-16 AU-4 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-16 AU-4 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-4-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-4-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-4 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-4 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-16 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 E3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-16 AU-3 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-16 AU-3 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-16 AU-3 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-3 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-3 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-3 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-16 AU-3 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Jitter</source>
            <translation>STM-16 Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Jitter Term</source>
            <translation>STM-16 AU-4 VC-4-16c Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>STM-16 AU-4 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>STM-16 AU-3 VC-12 Bulk Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-12 E1 Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Wander</source>
            <translation>STM-16 Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Wander Term</source>
            <translation>STM-16 AU-4 VC-4-16c Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-4-4c Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-4 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Wander Term</source>
            <translation>STM-16 AU-4 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Wander Term</source>
            <translation>STM-16 AU-3 VC-3 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : DS1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : E3 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Wander Term</source>
            <translation>STM-16 AU-3 VC-12 Bulk Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-12 E1 Wander Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64</source>
            <translation>STM-64</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 J-Scan</source>
            <translation>STM-64 J-Scan</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-64c Bulk BERT</source>
            <translation>VC-4-64c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-64c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-64c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-64c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-64c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-16c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-16c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-16c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4-4c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-4c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4-4c Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-64 AU-4 VC-4 E4 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Term</source>
            <translation>STM-64 AU-4 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>STM-64 AU-4 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>STM-64 AU-4 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-4-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-4-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-4 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-4 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-64 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 Bulk BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 DS3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : DS1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 E3 BERT Drop+Insert</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E1 : E3 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E1 : E3 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E1 : E3 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Term</source>
            <translation>STM-64 AU-3 VC-12 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>STM-64 AU-3 VC-12 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>STM-64 AU-3 VC-12 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-12 E1 BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-3 VC-3-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-3 VC-3-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-3 VC-12-Xv BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 3 Ping</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>STM-64 AU-3 VC-12-Xv GFP Capa 3 Traceroute</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256</source>
            <translation>STM-256</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL Bert</source>
            <translation>Bert STL</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Term</source>
            <translation>STM-256 STL Term BERT </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Mon</source>
            <translation>STM-256 STL BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-256c Bulk BERT</source>
            <translation>VC-4-256c Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-256c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-256c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-256c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-64c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-64c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-64c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-16c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-16c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4-4c Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4-4c Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4 Bulk BERT</source>
            <translation>VC-4 Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>STM-256 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>STM-256 AU-4 VC-4 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>STM-256 AU-4 VC-4 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3 Bulk BERT</source>
            <translation>VC-3 Bulk BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>STM-256 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>STM-256 AU-3 VC-3 Bulk BERT Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>STM-256 AU-3 VC-3 Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1G</source>
            <translation>10/100/1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth TrueSAM</source>
            <translation>10/100/1000 Eth TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>Comprobación rápida</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>L2 Traffic</source>
            <translation>Tráfico C2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 10/100/1000 C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic</source>
            <translation>Tráfico C3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 10/100/1000 C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico 10/100/1000 C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv4</source>
            <translation>IPv4 de tráfico C3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 10/100/1000 C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv6</source>
            <translation>IPv6 de tráfico C3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 10/100/1000 C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv4</source>
            <translation>IPv4 de tráfico C4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 10/100/1000 C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv6</source>
            <translation>IPv6 de tráfico C4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 10/100/1000 C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAM Completa</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic SAMComplete</source>
            <translation>SAM completo de 10/100/1000 tráfico C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L2 Multiple Streams</source>
            <translation>Múltiples secuencias C2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Streams SAMComplete</source>
            <translation>SAM completo de 10/100/1000 secuencias C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv4 de 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico 10/100/1000 L3 IPv6 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv4</source>
            <translation>multi-flujos de capa 3 en IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv4 SAMComplete</source>
            <translation>multi-flujos capa 3 SAMComplete IPv4 de 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv6</source>
            <translation>Multi-flujos Capa 3 IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv6 SAMComplete</source>
            <translation>Multi-flujos capa 3 SAMComplete IPv6 de 10/100/1000</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 TCP Wirespeed</source>
            <translation>Velocidad de cable TCP C4 TCP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed SAMComplete</source>
            <translation>SAM completo de velocidad de cable TCP 10/100/1000 C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed</source>
            <translation>Velocidad real de velocidad de cable TCP 10/100/1000 C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VNF Test</source>
            <translation>Prueba VNF</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Term</source>
            <translation>10/100/1000 Eth Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Mon</source>
            <translation>10/100/1000 Eth Capa 2 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Bucle de retorno</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Loopback</source>
            <translation>10/100/1000 Eth Capa 2 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Thru</source>
            <translation>Dual Trsp</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Dual Thru</source>
            <translation>10/100/1000 Eth Capa 2 Tráfico Dual Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Multiple Streams</source>
            <translation>Capa 2 Flujos Múltiples</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Term</source>
            <translation>10/100/1000 Eth Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Loopback</source>
            <translation>10/100/1000 Eth Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Triple Play</source>
            <translation>Capa 2 Triple Play</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Triple Play</source>
            <translation>10/100/1000 Eth Capa 2 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MiM Traffic</source>
            <translation>Capa 2 MiM Tráfico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Term</source>
            <translation>10/100/1000 Eth Capa 2 MiM Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Mon</source>
            <translation>10/100/1000 Eth Capa 2 MiM Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MPLS-TP Traffic</source>
            <translation>Capa 2 Tráfico MPLS-TP </translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MPLS-TP Traffic Term</source>
            <translation>10/100/1000 Eth Capa 2 MPLS-TP Traffic Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 PTP/1588</source>
            <translation>Capa 2 PTP/1588</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 PTP/1588 Term</source>
            <translation>10/100/1000 Eth Capa 2 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term</source>
            <translation>10/100/1000 Eth Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term</source>
            <translation>10/100/1000 Eth Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term</source>
            <translation>10/100/1000 Eth Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon</source>
            <translation>10/100/1000 Eth Capa 3 Tráfico Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback</source>
            <translation>10/100/1000 Eth Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Dual Thru</source>
            <translation>10/100/1000 Eth Capa 3 Tráfico Dual Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Tráfico Mon IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Multiple Streams</source>
            <translation>Capa 3 Flujos Múltiples</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term</source>
            <translation>10/100/1000 Eth Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback</source>
            <translation>10/100/1000 Eth Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback IPv6</source>
            <translation>10/100/1000 Eth Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Triple Play</source>
            <translation>Capa 3 Triple Play</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Triple Play</source>
            <translation>10/100/1000 Eth Capa 3 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Traffic</source>
            <translation>Capa 4 Tráfico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term</source>
            <translation>10/100/1000 Eth Capa 4 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback</source>
            <translation>10/100/1000 Eth Capa 4 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term IPv6</source>
            <translation>10/100/1000 Eth Capa 4 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback IPv6</source>
            <translation>10/100/1000 Eth Capa 4 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Multiple Streams</source>
            <translation>Capa 4 Flujos Múltiples</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term</source>
            <translation>10/100/1000 Eth Capa 4 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback</source>
            <translation>10/100/1000 Eth Capa 4 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term IPv6</source>
            <translation>10/100/1000 Eth Capa 4 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback IPv6</source>
            <translation>10/100/1000 Eth Capa 4 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 PTP/1588</source>
            <translation>Capa 4 PTP/1588</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 PTP/1588 Term</source>
            <translation>10/100/1000 Eth Capa 4 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 TCP Wirespeed</source>
            <translation>Capa 4 Wirespeed TCP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 TCP Wirespeed Term</source>
            <translation>Term TCP Wirespeed 10/100/1000 Eth Capa 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IP Video</source>
            <translation>IP Vídeo</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Explorer</source>
            <translation>Explorador MPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Explorer</source>
            <translation>10/100/1000 Eth Explorador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Explorer</source>
            <translation>Explorador SPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Explorer</source>
            <translation>10/100/1000 Eth Explorador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Analyzer</source>
            <translation>Analizador MPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Analyzer</source>
            <translation>10/100/1000 Eth Analizador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Analyzer</source>
            <translation>Analizador SPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Analyzer</source>
            <translation>10/100/1000 Eth Analizador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth VoIP Term</source>
            <translation>10/100/1000 Eth Term VoIP </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>J-Profiler</source>
            <translation>J-Profiler</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth J-Profiler</source>
            <translation>10/100/1000 Eth J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical</source>
            <translation>100M Optical</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth TrueSAM</source>
            <translation>100M óptico Eth TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico de Ethernet óptico C2 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico de Ethernet óptico C3 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic RFC 2544</source>
            <translation>RFC 2544 tráfico de Ethernet óptico C2 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv4 de Ethernet óptico C3 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 de Ethernet óptico C3 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv4 de Ethernet óptico C4 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 de Ethernet óptico C4 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic SAMComplete</source>
            <translation>SAM completo de tráfico de Ethernet óptico C2 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Streams SAMComplete</source>
            <translation>SAM completo de secuencias de Ethernet óptico C2 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv4 de Ethernet óptico 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv6 de 100Mb óptico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv4 SAMComplete</source>
            <translation>multi-flujos capa 3 SAMComplete IPv4 de Ethernet óptico 100M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv6 SAMComplete</source>
            <translation>Multi-flujos capa 3 SAMComplete IPv6 de 100Mb óptico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Term</source>
            <translation>100M Optical Eth Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor/Thru</source>
            <translation>Monitor/Trsp</translation>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Mon/Thru</source>
            <translation>100M Optical Eth Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Loopback Term</source>
            <translation>100M Optical Eth Capa 2 Bucle Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Term</source>
            <translation>100M Optical Eth Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Loopback</source>
            <translation>100M Optical Eth Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Triple Play</source>
            <translation>100M Optical Eth Capa 2 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Term</source>
            <translation>100M Optical Capa 2 MiM Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Mon/Thru</source>
            <translation>100M Optical Capa 2 MiM Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MPLS-TP Traffic Term</source>
            <translation>100M Capa 2 Óptica MPLS-TP Traffic Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 PTP/1588 Term</source>
            <translation>100M Optical Eth Capa 2 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term</source>
            <translation>100M Optical Eth Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term IPv6</source>
            <translation>100M Optical Eth Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term</source>
            <translation>100M Optical Eth Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term IPv6</source>
            <translation>100M Optical Eth Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term</source>
            <translation>100M Optical Eth Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru</source>
            <translation>100M Optical Eth Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback</source>
            <translation>100M Optical Eth Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term IPv6</source>
            <translation>100M Optical Eth Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>100M Optical Eth Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback IPv6</source>
            <translation>100M Optical Eth Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term</source>
            <translation>100M Optical Eth Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback</source>
            <translation>100M Optical Eth Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term IPv6</source>
            <translation>100M Optical Eth Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback IPv6</source>
            <translation>100M Optical Eth Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Triple Play</source>
            <translation>100M Optical Eth Capa 3 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term</source>
            <translation>100M Optical Eth Capa 4 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback</source>
            <translation>100M Optical Eth Capa 4 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term IPv6</source>
            <translation>100M Optical Eth Capa 4 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback IPv6</source>
            <translation>100M Optical Eth Capa 4 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term</source>
            <translation>100M Optical Eth Capa 4 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback</source>
            <translation>100M Optical Eth Capa 4 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term IPv6</source>
            <translation>100M Optical Eth Capa 4 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback IPv6</source>
            <translation>100M Optical Eth Capa 4 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 PTP/1588 Term</source>
            <translation>100M Optical Eth Capa 4 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Explorer</source>
            <translation>100M Optical Eth Explorador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Explorer</source>
            <translation>100M Optical Eth Explorador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Analyzer</source>
            <translation>100M Optical Eth Analizador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Analyzer</source>
            <translation>100M Optical Eth Analizador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth VoIP Term</source>
            <translation>100M Eth Óptico VoIP Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth J-Profiler</source>
            <translation>100M óptico Eth J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical</source>
            <translation>1GigE Optical</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE</source>
            <translation>1GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical TrueSAM</source>
            <translation>1GigE TrueSAM Óptico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 1GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 1GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico 1GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 1GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 1GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 1GigE C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 1GigE C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic SAMComplete</source>
            <translation>Tráfico capa 2 de SAMComplete de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Streams SAMComplete</source>
            <translation>multi-flujos capa 2 SAMComplete de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico de L3 de SAMComplete IPv4 de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico de L3 de SAMComplete IPv6 de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv4 SAMComplete</source>
            <translation>Secuencias L3 de SAMComplete IPv4 de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv6 SAMComplete</source>
            <translation>Secuencias L3 de SAMComplete IPv6 de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed SAMComplete</source>
            <translation>1GigE L4 TCP Wirespeed SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed</source>
            <translation>Tráfico a velocidad de línea capa 4 TrueSpeed TCP de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>Tráfico a velocidad de línea capa 4 TrueSpeed VNF TCP de 1 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Comprobación de PTP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 PTP Check</source>
            <translation>Comprobación de PTP en 1GigE C4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SyncE Wander</source>
            <translation>SyncE Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical SyncE Wander Mon/Thru</source>
            <translation>Desviación óptica 1GigE SyncE Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 BERT</source>
            <translation>Capa 1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Term</source>
            <translation>1GigE Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Mon/Thru</source>
            <translation>1GigE Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Patterns</source>
            <translation>Capa 2 Patrones</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Patterns Term</source>
            <translation>1GigE Capa 2 Patrones Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Term</source>
            <translation>1GigE Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Mon/Thru</source>
            <translation>1GigE Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Loopback</source>
            <translation>1GigE Capa 2 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Term</source>
            <translation>1GigE Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Loopback</source>
            <translation>1GigE Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Triple Play</source>
            <translation>1GigE Capa 2 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Term</source>
            <translation>1GigE Capa 2 MiM Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Mon/Thru</source>
            <translation>1GigE Capa 2 MiM Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MPLS-TP Traffic Term</source>
            <translation>1GigE Capa 2 MPLS-TP Traffic Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Term</source>
            <translation>1GigE Capa 2 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Dual Mon</source>
            <translation>Doble PTP/1588 de 1 GigE de Capa 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term</source>
            <translation>1GigE Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term IPv6</source>
            <translation>1GigE Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term</source>
            <translation>1GigE Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term IPv6</source>
            <translation>1GigE Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term</source>
            <translation>1GigE Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru</source>
            <translation>1GigE Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback</source>
            <translation>1GigE Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term IPv6</source>
            <translation>1GigE Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>1GigE Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback IPv6</source>
            <translation>1GigE Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term</source>
            <translation>1GigE Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback</source>
            <translation>1GigE Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term IPv6</source>
            <translation>1GigE Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback IPv6</source>
            <translation>1GigE Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Triple Play</source>
            <translation>1GigE Capa 3 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term</source>
            <translation>1GigE Capa 4 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback</source>
            <translation>1GigE Capa 4 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term IPv6</source>
            <translation>1GigE Capa 4 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback IPv6</source>
            <translation>1GigE Capa 4 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term</source>
            <translation>1GigE Capa 4 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback</source>
            <translation>1GigE Capa 4 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term IPv6</source>
            <translation>1GigE Capa 4 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback IPv6</source>
            <translation>1GigE Capa 4 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 PTP/1588 Term</source>
            <translation>1GigE Capa 4 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 TCP Wirespeed Term</source>
            <translation>Term TCP Wirespeed 1GigE Capa 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Explorer</source>
            <translation>1GigE Explorador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Explorer</source>
            <translation>1GigE Explorador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Analyzer</source>
            <translation>1GigE Analizador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Analyzer</source>
            <translation>1GigE Analizador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Eth VoIP Term</source>
            <translation>1GigE Eth Term VoIP </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE J-Profiler</source>
            <translation>1GigE J-Profiler</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN</source>
            <translation>10GigE LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Optical TrueSAM</source>
            <translation>10GigE TrueSAM Óptico</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico LAN C2 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico LAN C3 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico de LAN C2 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv4 LAN C3 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 LAN C3 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv4 LAN C4 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 LAN C4 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic SAMComplete</source>
            <translation>SAM completo de tráfico LAN C2 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Streams SAMComplete</source>
            <translation>SAM completo de secuencias LAN C2 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv4 de LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico 10GigE LAN L3 IPv6 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv4 SAMComplete</source>
            <translation>multi-flujos capa 3 SAMComplete IPv4 de LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv6 SAMComplete</source>
            <translation>Multi-flujos capa 3 SAMComplete IPv6 de LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed SAMComplete</source>
            <translation>SAM completo de velocidad de cable TCP de LAN C4 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed TrueSpeed</source>
            <translation>Velocidad real de velocidad de cable TCP de LAN C4 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Term</source>
            <translation>10GigE LAN Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Mon/Thru</source>
            <translation>10GigE LAN Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Term</source>
            <translation>10GigE LAN Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE LAN Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Loopback</source>
            <translation>10GigE LAN Capa 2 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Term</source>
            <translation>10GigE LAN Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Loopback</source>
            <translation>10GigE LAN Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 2 Triple Play</source>
            <translation>10GigE Capa 2 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Term</source>
            <translation>10GigE LAN Capa 2 MiM Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Mon/Thru</source>
            <translation>10GigE LAN Capa 2 MiM Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MPLS-TP Traffic Term</source>
            <translation>10GigE LAN Capa 2 MPLS-TP Traffic Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 PTP/1588 Term</source>
            <translation>10GigE LAN Capa 2 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term</source>
            <translation>10GigE LAN Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term IPv6</source>
            <translation>10GigE LAN Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term</source>
            <translation>10GigE LAN Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE LAN Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term</source>
            <translation>10GigE LAN Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE LAN Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback</source>
            <translation>10GigE LAN Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term IPv6</source>
            <translation>10GigE LAN Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE LAN Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback IPv6</source>
            <translation>10GigE LAN Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term</source>
            <translation>10GigE LAN Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback</source>
            <translation>10GigE LAN Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term IPv6</source>
            <translation>10GigE LAN Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback IPv6</source>
            <translation>10GigE LAN Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 3 Triple Play</source>
            <translation>10GigE Capa 3 Triple Play</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term</source>
            <translation>10GigE LAN Capa 4 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback</source>
            <translation>10GigE LAN Capa 4 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term IPv6</source>
            <translation>10GigE LAN Capa 4 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback IPv6</source>
            <translation>10GigE LAN Capa 4 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term</source>
            <translation>10GigE LAN Capa 4 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback</source>
            <translation>10GigE LAN Capa 4 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term IPv6</source>
            <translation>10GigE LAN Capa 4 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback IPv6</source>
            <translation>10GigE LAN Capa 4 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 PTP/1588 Term</source>
            <translation>10GigE LAN Capa 4 PTP/1588 Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 TCP Wirespeed Term</source>
            <translation>Term TCP Wirespeed 10GigE LAN Capa 4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Explorer</source>
            <translation>10GigE Explorador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Explorer</source>
            <translation>10GigE Explorador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Analyzer</source>
            <translation>10GigE Analizador MPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Analyzer</source>
            <translation>10GigE Analizador SPTS IP Vídeo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN VoIP Term</source>
            <translation>10GigE LAN Term VoIP </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN</source>
            <translation>10GigE WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Term</source>
            <translation>10GigE WAN OC-192c Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Mon/Thru</source>
            <translation>10GigE WAN OC-192c Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Term</source>
            <translation>10GigE WAN OC-192c Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE WAN OC-192c Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Loopback</source>
            <translation>10GigE WAN OC-192c Capa 2 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Term</source>
            <translation>10GigE WAN OC-192c Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Loopback</source>
            <translation>10GigE WAN OC-192c Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term</source>
            <translation>10GigE WAN OC-192c Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term</source>
            <translation>10GigE WAN OC-192c Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term</source>
            <translation>10GigE WAN OC-192c Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE WAN OC-192c Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback</source>
            <translation>10GigE WAN OC-192c Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term</source>
            <translation>10GigE WAN OC-192c Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Loopback</source>
            <translation>10GigE WAN OC-192c Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c L3 Streams Loopback IPv6</source>
            <translation>10GigE WAN OC-192c Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Term</source>
            <translation>10GigE WAN STM-64 Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Mon/Thru</source>
            <translation>10GigE WAN STM-64 Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Term</source>
            <translation>10GigE WAN STM-64 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Mon/Thru</source>
            <translation>10GigE WAN STM-64 Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Loopback</source>
            <translation>10GigE WAN STM-64 Capa 2 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Term</source>
            <translation>10GigE WAN STM-64 Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Loopback</source>
            <translation>10GigE WAN STM-64 Capa 2 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term</source>
            <translation>10GigE WAN STM-64 Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term</source>
            <translation>10GigE WAN STM-64 Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term</source>
            <translation>10GigE WAN STM-64 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru</source>
            <translation>10GigE WAN STM-64 Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback</source>
            <translation>10GigE WAN STM-64 Capa 3 Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term</source>
            <translation>10GigE WAN STM-64 Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Loopback</source>
            <translation>10GigE WAN STM-64 Capa 3 Flujos Bucle</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 L3 Streams Loopback IPv6</source>
            <translation>10GigE WAN STM-64 Capa 3 Flujos Bucle IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE</source>
            <translation>40GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 40GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 40GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico 40GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 40GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 40GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic SAMComplete</source>
            <translation>Tráfico capa 2 SAMComplete 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Streams SAMComplete</source>
            <translation>Multi-flujos capa 2 SAMComplete 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv4 de 40GE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico 40GigE L3 IPv6 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv4 SAMComplete</source>
            <translation>Flujos 40GigE L3 IPv4 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv6 SAMComplete</source>
            <translation>Secuencias de L3 de SAMComplete IPv6 de 40 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 PCS</source>
            <translation>Capa 1 PCS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Term</source>
            <translation>40GigE Capa 1 PCS Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Mon/Thru</source>
            <translation>40GigE Capa 1 PCS Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Term</source>
            <translation>40GigE Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Mon/Thru</source>
            <translation>40GigE Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Streams Term</source>
            <translation>40GigE Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term</source>
            <translation>40GigE Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term IPv6</source>
            <translation>40GigE Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term</source>
            <translation>40GigE Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term IPv6</source>
            <translation>40GigE Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term</source>
            <translation>40GigE Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru</source>
            <translation>40GigE Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term IPv6</source>
            <translation>40GigE Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>40GigE Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term</source>
            <translation>40GigE Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term IPv6</source>
            <translation>40GigE Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE</source>
            <translation>100GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 100GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic QuickCheck</source>
            <translation>Comprobación rápida de tráfico 100GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico 100GigE C2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv4 100GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico IPv6 100GigE C3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic SAMComplete</source>
            <translation>Trafico 100GigE L2 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Streams SAMComplete</source>
            <translation>Flujos 100GigE L2  SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>Tráfico capa 3 SAMComplete IPv4 de 100GE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>Tráfico 100GigE L3 IPv6 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv4 SAMComplete</source>
            <translation>Flujos 100GigE L3 IPv4 SAMCompleto</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv6 SAMComplete</source>
            <translation>Secuencias de L3 de SAMComplete IPv6 de 100 GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Term</source>
            <translation>100GigE Capa 1 PCS Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Mon/Thru</source>
            <translation>100GigE Capa 1 PCS Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term</source>
            <translation>100GigE Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon/Thru</source>
            <translation>100GigE Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term</source>
            <translation>100GigE Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term</source>
            <translation>100GigE Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6</source>
            <translation>100GigE Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term</source>
            <translation>100GigE Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6</source>
            <translation>100GigE Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term</source>
            <translation>100GigE Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru</source>
            <translation>100GigE Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6</source>
            <translation>100GigE Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>100GigE Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term</source>
            <translation>100GigE Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6</source>
            <translation>100GigE Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE RS-FEC</source>
            <translation>100GigE RS-FEC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term RS-FEC</source>
            <translation>Tráfico de RS-FEC de 100 GigE Capa 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon RS-FEC</source>
            <translation>100GigE Capa 2 Tráfico Mon RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term RS-FEC</source>
            <translation>Secuencias de RS-FEC de 100 GigE Capa 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term RS-FEC</source>
            <translation>Ping de RS-FEC de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6 RS-FEC</source>
            <translation>Ping de RS-FEC IPv6 de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term RS-FEC</source>
            <translation>Trazado de ruta RS-FEC de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6 RS-FEC</source>
            <translation>Trazado de ruta de RS-FEC de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term RS-FEC</source>
            <translation>100GigE Capa 3 Tráfico Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon RS-FEC</source>
            <translation>100GigE Capa 3 Tráfico Mon RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6 RS-FEC</source>
            <translation>Tráfico de RS-FEC IPv6 de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon IPv6 RS-FEC</source>
            <translation>Tráfico de RS-FEC Mon de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term RS-FEC</source>
            <translation>100GigE Capa 3 Flujos Term RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6 RS-FEC</source>
            <translation>Secuencias de RS-FEC IPv6 de 100 GigE Capa 3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig</source>
            <translation>1Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1G FC</source>
            <translation>1G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>1Gig Fibre Channel Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>1Gig Fibre Channel Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>1Gig Fibre Channel Capa 2 Patrones Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>1Gig Fibre Channel Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>1Gig Fibre Channel Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig</source>
            <translation>2Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2G FC</source>
            <translation>2G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>2Gig Fibre Channel Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>2Gig Fibre Channel Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>2Gig Fibre Channel Capa 2 Patrones Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>2Gig Fibre Channel Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>2Gig Fibre Channel Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig</source>
            <translation>4Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4G FC</source>
            <translation>4G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>4Gig Fibre Channel Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>4Gig Fibre Channel Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>4Gig Fibre Channel Capa 2 Patrones Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>4Gig Fibre Channel Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>4Gig Fibre Channel Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig</source>
            <translation>8Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8G FC</source>
            <translation>8G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>8Gig Fibre Channel Capa 2 Patrones Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>8Gig Fibre Channel Capa  2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>8Gig Fibre Channel Capa 2 Tráfico  Mon/Transp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig</source>
            <translation>10Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G FC</source>
            <translation>10G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>10Gig Fibre Channel Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>10Gig Fibre Channel Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>10Gig Fibre Channel Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>10Gig Fibre Channel Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig</source>
            <translation>16Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16G FC</source>
            <translation>16G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Canal de fibra 16Gig Capa 1 BERT Término</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Canal de fibra 16Gig Capa 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Canal de fibra 16Gig Capa 2 Tráfico Término</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Canal de fibra 16Gig Capa 2 Tráfico Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614,4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI</source>
            <translation>614,4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Comprobación de CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Check</source>
            <translation>Comprobación 614.4M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Term</source>
            <translation>614,4M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>614,4M CPRI  Capa 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 BERT</source>
            <translation>Capa 2 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Term</source>
            <translation>614,4M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>614,4M CPRI Capa 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI</source>
            <translation>1228.8M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Check</source>
            <translation>Comprobación 1228.8M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Term</source>
            <translation>1228,8M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>1228,8M CPRI Capa 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Term</source>
            <translation>1228.8M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>1228.8M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI</source>
            <translation>2457.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Check</source>
            <translation>Comprobación 2457.6M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Term</source>
            <translation>2457.6M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>2457.6M CPRI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Term</source>
            <translation>2457.6M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>2457.6M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI</source>
            <translation>3072.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Check</source>
            <translation>Comprobación 3072.0M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Term</source>
            <translation>3072.0M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>3072.0M CPRI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Term</source>
            <translation>3072.0M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>3072.0M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI</source>
            <translation>4915.2M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Check</source>
            <translation>Comprobación 4915.2M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Term</source>
            <translation>4915.2M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>4915.2M CPRI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Term</source>
            <translation>4915.2M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>4915.2M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI</source>
            <translation>6144.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Check</source>
            <translation>Comprobación 6144.0M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Term</source>
            <translation>6144.0M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>6144.0M CPRI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Term</source>
            <translation>6144.0M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>6144.0M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI</source>
            <translation>9830.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Check</source>
            <translation>Comprobación 9830.4M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Term</source>
            <translation>9830.4M CPRI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>9830.4M CPRI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Term</source>
            <translation>9830.4M CPRI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>9830.4M CPRI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI</source>
            <translation>10137.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Check</source>
            <translation>Comprobación 10137.6M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Term</source>
            <translation>10137.6M CPRI Capa 2 BERT Término</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>10137.6M CPRI Capa 2 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI</source>
            <translation>768M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Term</source>
            <translation>768M OBSAI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>768M OBSAI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Term</source>
            <translation>768M OBSAI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>768M OBSAI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI</source>
            <translation>1536M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Term</source>
            <translation>1536M OBSAI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>1536M OBSAI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Term</source>
            <translation>1536M OBSAI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>1536M OBSAI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI</source>
            <translation>3072M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Term</source>
            <translation>3072M OBSAI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>3072M OBSAI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Term</source>
            <translation>3072M OBSAI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>3072M OBSAI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI</source>
            <translation>6144M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Term</source>
            <translation>6144M OBSAI Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>6144M OBSAI Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Term</source>
            <translation>6144M OBSAI Capa 2 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>6144M OBSAI Capa 2 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1</source>
            <translation>OTU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Comprobación de OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G OTN Check</source>
            <translation>Comprobación de OTN en OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Term</source>
            <translation>OTU1 2.7G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Mon/Thru</source>
            <translation>BERT masivo OTU1 2.7G Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48</source>
            <translation>STS-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-48c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-12c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-3c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Term</source>
            <translation>OTU1 2.7G STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STS-1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>BERT masivo OTU1 2.7G STM-16 AU-3 VC-3 Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU0</source>
            <translation>ODU0</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Term</source>
            <translation>OTU1 2.7G ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>BERT masivo OTU1 2.7G ODU0 Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>OTU1 2.7G ODU0 GMP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU1 2.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>OTU1 2.7G ODU0 GMP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU1 2.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Jitter</source>
            <translation>OTU1 2.7G Jitter</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G Bulk Jitter BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G STS-48c Bulk Jitter BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Jitter BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Jitter BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Wander</source>
            <translation>OTU1 2.7G Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G Bulk Wander BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G STS-48c Bulk Wander BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Wander BERT Term</source>
            <translation>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Wander BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2</source>
            <translation>OTU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G OTN Check</source>
            <translation>Comprobación de OTN en OTU2 10.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Term</source>
            <translation>OTU2 10.7G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192</source>
            <translation>STS-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-192c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-192c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-48c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-12c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-3c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Term</source>
            <translation>OTU2 10.7G STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STS-1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU1</source>
            <translation>ODU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Term</source>
            <translation>OTU2 10.7G ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Term</source>
            <translation>OTU2 10.7G ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>OTU2 10.7G ODU0 GMP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU2 10.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>OTU2 10.7G ODU0 GMP Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU2 10.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODUflex</source>
            <translation>ODUflex</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Term</source>
            <translation>OTU2 10.7G ODUflex Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Mon</source>
            <translation>OTU2 10.7G ODUflex Bulk BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU2 10.7G ODUflex GMP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e</source>
            <translation>OTU1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G OTN Check</source>
            <translation>Comprobación de OTN en OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Term</source>
            <translation>OTU1e 11.05G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Mon/Thru</source>
            <translation>OTU1e 11.05G Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Term</source>
            <translation>OTU1e 11.05G Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Mon/Thru</source>
            <translation>OTU1e 11.05G Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Term</source>
            <translation>OTU1e 11.05G Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU1e 11.05G Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e</source>
            <translation>OTU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G OTN Check</source>
            <translation>Comprobación de OTN en OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Term</source>
            <translation>OTU2e 11.1G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Mon/Thru</source>
            <translation>OTU2e 11.1G Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Term</source>
            <translation>OTU2e 11.1G Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Mon/Thru</source>
            <translation>OTU2e 11.1G Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Term</source>
            <translation>OTU2e 11.1G Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU2e 11.1G Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3</source>
            <translation>OTU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica OTL3.4 43.02G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3.4 43.02G OTN Check</source>
            <translation>Comprobación de OTN en OTU3.4 43.02G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico de 43.02G C2 OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico de 43.02G C3 OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 43.02G C3 OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL BERT</source>
            <translation>OTL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Term</source>
            <translation>OTL3.4 43.02G OTL BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Mon</source>
            <translation>OTL3.4 43.02G OTL BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Term</source>
            <translation>OTU3 43.02G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Monitor/Thru</source>
            <translation>Monitor de tráfico de OTU3 43.02G de capa 2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Streams Term</source>
            <translation>OTU3 43.02G Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term</source>
            <translation>OTU3 43.02G Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term IPv6</source>
            <translation>OTU3 43.02G Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term</source>
            <translation>OTU3 43.02G Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term IPv6</source>
            <translation>OTU3 43.02G Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term IPv6</source>
            <translation>OTU3 43.02G Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU3 43.02G Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term</source>
            <translation>OTU3 43.02G Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term IPv6</source>
            <translation>OTU3 43.02G Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768</source>
            <translation>STS-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-768c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-768c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-192c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-192c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-48c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-48c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-12c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-12c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-3c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-3c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Term</source>
            <translation>OTU3 43.02G STS-1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STS-1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</source>
            <translation>BERT masivo OTU3 43.02G STM-256 AU4 VC4-256c Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2e</source>
            <translation>ODU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2e Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Term</source>
            <translation>OTU3 43.02G ODU2e Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2e Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2e Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2</source>
            <translation>ODU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU1 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU3 43.02G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU3 43.02G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>BERT masivo OTU3 43.02G ODU1 ODU0 Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU3 43.02G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU3 43.02G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Term</source>
            <translation>OTU3 43.02G ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU0 Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex Bulk BERT Term</source>
            <translation>OTU3 43.02G ODUflex Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU3 43.02G ODUflex GMP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4</source>
            <translation>OTU4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica >OTL4.10 111.8G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G OTN Check</source>
            <translation>Comprobación de OTN en OTU4 111.8G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L2 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico de 111.8G C2 OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic RFC 2544</source>
            <translation>RFC 2544 de tráfico de 111.8G C3 OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic IPv6 RFC 2544</source>
            <translation>RFC 2544 de tráfico de IPv6 111.8G C3 OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Term</source>
            <translation>OTL4.10 111.8G OTL BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Mon</source>
            <translation>OTL4.10 111.8G OTL BERT Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Term</source>
            <translation>OTU4 111.8G Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Streams Term</source>
            <translation>OTU4 111.8G Capa 2 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term</source>
            <translation>OTU4 111.8G Capa 3 Ping Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term IPv6</source>
            <translation>OTU4 111.8G Capa 3 Ping Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term</source>
            <translation>OTU4 111.8G Capa 3 Traceroute Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term IPv6</source>
            <translation>OTU4 111.8G Capa 3 Traceroute Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term IPv6</source>
            <translation>OTU4 111.8G Capa 3 Tráfico Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>OTU4 111.8G Capa 3 Tráfico Mon/Trsp IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term</source>
            <translation>OTU4 111.8G Capa 3 Flujos Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term IPv6</source>
            <translation>OTU4 111.8G Capa 3 Flujos Term IPv6</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU3</source>
            <translation>ODU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU3 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU3 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2e Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Term</source>
            <translation>OTU4 111.8G ODU2e Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2e Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2e Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU1 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU4 111.8G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU4 111.8G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 término BERT masivo</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 2 OTU4 111.8G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Monitoreo tráfico de paso - Capa 3 OTU4 111.8G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Bulk BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODU0 Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Capa 2 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Term</source>
            <translation>OTU4 111.8G ODU0 Capa 3 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU0 Capa 3 Tráfico Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex Bulk BERT Term</source>
            <translation>OTU4 111.8G ODUflex Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>OTU4 111.8G ODUflex GMP Capa 2 Tráfico Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Optical BERT</source>
            <translation>BERT Óptico</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Term</source>
            <translation>3072.0M Optical Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Mon/Thru</source>
            <translation>3072.0M Optical Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Term</source>
            <translation>9830.4M Optical Capa 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Mon/Thru</source>
            <translation>9830.4M Optical Capa 1 BERT Mon/Trsp</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing</source>
            <translation>Temporización</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1PPS</source>
            <translation>1PPS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Análisis</translation>
        </message>
        <message utf8="true">
            <source>1PPS Analysis</source>
            <translation>Análsis 1PPS</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Wander</source>
            <translation>Desviación 2MHz</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Clock Wander Analysis</source>
            <translation>Análisis de desviación de reloj 2MHz</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Wander</source>
            <translation>10MHz Wander</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Clock Wander Analysis</source>
            <translation>Análisis de desviación de reloj 10MHz</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Benchmark</source>
            <translation>Benchmark</translation>
            <comment>This text appears in the main Test menu.&#xA;This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Módulo de sincronización</translation>
        </message>
    </context>
</TS>
