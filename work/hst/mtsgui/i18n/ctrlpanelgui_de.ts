<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Mobilapp Fähigkeit</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth und WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Nur WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Sie müssen das Prüfgerät neu starten, damit die Änderungen übernommen werden.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Einfügen</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Kopieren</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Ausschneiden</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Einfach</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Mehrfach</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Alle</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keines</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Öffnen</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Umbenennen</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Neuer Ordner</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Datenträger</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>Beim Einfügen werden bestehende Dateien überschrieben.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Datei überschreiben</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>Wir können die Datei aufgrund eines unbekannten Fehlers nicht öffnen.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>Datei kann nicht geöffnet werden.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>Diese ausgewählte Datei wird endgültig gelöscht.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>Die %1 ausgewählten Dateien werden endgültig gelöscht.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Datei löschen</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>geben Sie den Ordnernamen ein:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Ordner erstellen</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>Der Ordner %1 konnte nicht erstellt werden.</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>Der Ordner %1 ist bereits vorhanden.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Ordner erstellen</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Daten werden eingefügt...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Daten werden gelöscht...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Abgeschlossen.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Fehler beim Einfügen der Datei.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>Fehler beim Einfügen von %1 Dateien.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Nicht genügend freier Speicherplatz.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Fehler beim Löschen der Datei.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>Fehler beim Löschen von %1 Dateien.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Zurück</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Option</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>URL verbessern</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dateiname</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Aktion</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation> Modul</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Anderes WLAN</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Geben Sie die WLAN-Daten unten ein.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Name (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Verschlüsselungstyp:</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Upgrade verfügbar</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync hat festgestellt, dass ein Upgrade verfügbar ist.&#xA;Möchten Sie das Upgrade jetzt durchführen? &#xA;&#xA;Während des Upgrades werden alle laufenden Tests abgebrochen. Wenn der Prozess beendet wurde, wird der Test erneut gebootet, um das Upgrade zu vervollständigen.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Ein unbekannter Fehler ist aufgetreten. Wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Upgrades fehlgeschlagen</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Geräteinformationen:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Basisoptionen:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Standardeinstellungen &#xA;wiederherstellen</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Exportieren der Logs&#xA;in den USB Stick</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Systeminformationen &#xA;in Datei kopieren</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>Die Systeminformationen wurden in &#xA;folgende Datei kopiert:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Dieser Vorgang erfordert einen Neustart. Die System- und die Prüfeinstellungen werden auf die Standardwerte zurückgesetzt.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Zurücksetzen und neu starten</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Logexport erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Logs können nicht exportiert werden. Bitte verifizieren, dass ein USB Stick einsetzt ist und erneut versuchen.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 Version %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Wiederholen</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Auf Vorgabewerte zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Wählen Sie ein Installationsverfahren für das Upgrade aus:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Upgrade über Dateien auf einem USB-Flashmedium</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Netzwerk</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Upgrade über einen Webserver im Netzwerk herunterladen</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Geben Sie die Adresse des Upgradeservers ein:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Serveradresse</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Adresse des Proxy-Servers eingeben, falls Zugriff auf Upgrade-Server erforderlich:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Proxyadresse</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Der Server wird auf verfügbare Upgrades überprüft.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Bisherige</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Weiter</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Upgrade jetzt installieren</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Downgrade starten</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Keine Upgrades gefunden</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Upgrades fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>Vor der Installation des Upgrades werden alle aktuellen Prüfungen beendet. Nach der Installation des Upgrades wird das Prüfgerät neu gestartet, um den Vorgang abzuschließen.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Upgradebestätigung</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>Das Nachschalten des Testsatzes ist möglich, wird aber nicht empfohlen. Wenn Sie dies tun müssen, rufen Sie Viavi TAC an.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>Das Nachschalten beendet alle laufenden Tests. Am Ende des Vorgangs fährt sich der Testsatz selbst wieder hoch, um die Nachschaltung abzuschließen.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>Nachschaltung nicht empfohlen</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Ein unbekannter Fehler ist aufgetreten. Wiederholen Sie den Vorgang.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>System</translation>
        </message>
    </context>
</TS>
