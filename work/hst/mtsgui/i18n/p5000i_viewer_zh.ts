<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi显微镜</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>视图FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>冻结</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>放大</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>覆盖</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>分析...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>配置</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>从 U 盘 导入</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>窍门</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>自动对中</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>测试按钮：</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>冻结帧</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>其他设置…</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>导入显微镜配置文件</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>导入&#xA;配置文件</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>显微镜配置文件 (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>测试</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>冻结</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>当前的</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>保存为 PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>保存为 PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>保存图像</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>保存报告</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>分析失败</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>未能分析光纤。请检查播放的食品表面光线末端，并在测试前再次聚焦</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>选择图像</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>图像文件（*.png）</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有文件（*）</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>光纤检查与测试报告</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>光纤信息</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>未定义额外的光纤信息</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>通过</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失败</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>配置:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>窍门：</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>检查摘要</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>缺陷</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>划痕</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>或</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>标准</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>阈值</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>计数</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>结果</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>任意</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>不适用</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>生成检查结果失败。</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>小放大率</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>大放大率</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>划痕测试未启用</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>工作：</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>电缆：</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>连接器:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>公司：</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>工程师:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>用户:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>位置:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>工作：</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>电缆：</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>连接器:</translation>
        </message>
    </context>
</TS>
