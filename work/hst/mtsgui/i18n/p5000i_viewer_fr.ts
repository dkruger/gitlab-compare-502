<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscope Viavi</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauvegarde</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextLabel</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vision FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Quitter</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Gel</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Zoom avant</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Incrustation</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Analyse...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Profil</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Import de l'USB</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>pointe</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Auto - centre</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Bouton de test :</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Tests</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Freezes</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>D'autres paramètres ...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Importer profil microscope</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Importer&#xA;Profil</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Profils microscope (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Gel</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>En fonction</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>Sauvegarder PNG</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>enregistrer le document PDF</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Sauvegarder image</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Enregistrer le rapport</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>L'analyse  a échoué</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>Impossible d'analyser la fibre . S'il vous plaît vérifier que la vidéo en direct montre l' extrémité de la fibre et qu'elle soit centrée avant de tester à nouveau .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Sélectionner image</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Fichiers image (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Sélectionner</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Inspection de fibre et rapport d'essai</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Informations de fibre</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>Aucune information de fibre supplémentaire défini</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Profil:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Astuce:</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Résumé de l' inspection</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>DEFAUTS</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>RAYURES</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>ou</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>critères</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Seuil</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>résultat</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>n'importe</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Impossible de générer le résumé de l'inspection .</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>faible grossissement</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>fort grossissement</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Tests Scratch pas activé</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>travail :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>câble :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Connecteur:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Société :</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Technicien:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Client:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Localisation:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>travail :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>câble :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Connecteur:</translation>
        </message>
    </context>
</TS>
