<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> Está seguro que desea cancelar?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Hubo un error leyendo el archivo.</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>El archivo tiene muchas tramas (más de 50.000). Trate de guardar un búfer parcial,&#xA;o expórtelo a una unidad USB y cárguelo en un dispositivo diferente. </translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>¿Desea borrar todos los datos almacenados?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>¿Desea eliminar el elemento seleccionado?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Ya existe el nombre- &#xA;¿desea sobrescribir los datos antiguos?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>Los gráficos están encontrando errores de escritura en el disco. Por favor, guarde los gráficos ahora para conservar&#xA;su trabajo. Los gráficos se detendrán y se borrarán automáticamente después de alcanzar el nivel crítico.</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>Los gráficos encontraron demasiados errores de disco para continuar y se detuvieron.&#xA;Borrados los datos para graficar. Si lo desea puede reiniciar los gráficos.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>Memoria USB</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Dispositivo desmontable</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>Registro lleno</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Configuración inválida:&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>No válido</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Datos 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Datos 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisión de la especificación CFP MSA Mgmt I/F soportada por SW</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisión de la especificación QSFP MSA Mgmt I/F soportada por SW</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>Información DMC</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Código de barras</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Fecha de fabricación</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Fecha de calibración</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Revisión de software</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>Option Challenge ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Assembly Serial Number</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Código de la barra de ensamble</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Software instalado</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Opciones:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Esperando por paquetes...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Analizando paquetes...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Análisis</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>Esperando por el enlace...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>Presione "Iniciar el análisis" para comenzar...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Guardando resultados, por favo, espere.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>No se pudieron guardar los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Se guardaron los resultados.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>Esperando iniciar</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>El usuario detuvo la prueba previa</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>La prueba previa se anuló con errores</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>La prueba anterior falló y se activó la detención en falla </translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>No en ejecución</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Tiempo restante: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>La prueba no se pudo completar y se anuló con errores</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Prueba completada</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fallado</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Prueba completada con resultados en falla</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Pasó</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Prueba completada con todos los resultados aprobados</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>La prueba fue detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Deteniendo</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>No corriendo</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Iniciando</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Conexión verificada</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Conexión pérdida</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Verificando conexión</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuración inválida</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Guardando resultados, por favo, espere.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>No se pudieron guardar los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Se guardaron los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados Medida Throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Resultados de la prueba de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Resultados de la prueba de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalias</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Anomalías en sentido ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Anomalías en sentido descendente</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Resultados de la prueba de latencia</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Resultados de prueba de latencia de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Resultados de prueba de latencia de bajada</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Resultados de la prueba de vibración</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Resultados de prueba de vibración de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Resultados de prueba de vibración de bajada</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marcos de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 Resultados de prueba de pérdida de marcos de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marco de subida de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 Resultados de prueba de pérdida de marco de subida de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marco de bajada de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 Resultado de prueba de pérdida de marco de bajada de bytes</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Resultados de la prueba CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Resultados de la prueba CBS de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Resultados de la prueba CBS de bajada</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Resultados de la prueba de política CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Resultados de la prueba de políticas CBS de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Resultados de la prueba de políticas CBS de bajada</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga de bajada</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Resultados Test Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Resultados de la prueba de subida opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Resultados de la prueba de bajada opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Gráfico de prueba de la recuperación del sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación de sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Gráfico de prueba de recuperación del sistema de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación del sistema de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Gráfico de prueba de recuperación del sistema de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación del sistema de bajada</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Resultados de prueba de carga extendida</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Paso MTU de la ruta</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>Paso RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Paso Walk de Window del flujo descendente </translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Paso Walk de Window del flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Pasos de rendimiento TCP del flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Etapa del rendimiento TCP del flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Guardando resultados, por favo, espere.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>No se pudieron guardar los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Se guardaron los resultados.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>No se ha podido adquirir la sincronización. Por favor, asegure que todos los cables estén conectados y el puerto esté configurado correctamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>No se pudo encontrar un enlace activo. Por favor, asegure que todos los cables estén conectados y el puerto esté configurado correctamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>No se pudo determinar la velocidad o dúplex del puerto conectado. Por favor, asegure que todos los cables estén conectados y el puerto esté configurado correctamente.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Es posible que no obtenga una dirección IP para el instrumento de prueba local. Por favor, revise su configuración de comunicación e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>No se pudo establecer un canal de comunicación al instrumento de prueba remoto. Por favor, revise su configuración de comunicación e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>El instrumento de prueba remoto no parece tener una versión compatible con el software BERT. La mínima versión compatible es %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>La aplicación TCP Wirespeed de la Capa 4 debe estar disponible, tanto en la unidad local como en la remota, con el fin de ejecutar TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>La selección de la prueba TrueSpeed no es válida para esta configuración de encapsulación y será eliminada.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>La selección de prueba TrueSpeed no es válida para este tipo de ajuste de marco y será eliminada.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>La selección de la prueba TrueSpeed no es válida con la encapsulación remota y será eliminada.</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>La selección de prueba completa SAM no es válida para este ajuste de encapsulado y será eliminada.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configuración inválida</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>Control CBS</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Políticas</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Paso #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Paso #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Paso #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Paso #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Paso #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Paso #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Paso #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Paso #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Paso #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Paso #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Guardando resultados, por favo, espere.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>No se pudieron guardar los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Se guardaron los resultados.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>ARP local falló.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>Falló el ARP remoto.</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Tabla, cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Imprimir Pantalla</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - El histógrama ocupa varias páginas</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Escala de tiempos</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>No disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Info Usuario</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Grupos de configuración</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Grupos de resultados</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Registro de eventos</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Histogramas</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Capturas Pantalla</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Throughput TCP estimado</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation>Reporte de pruebas múltiples</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generado por Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generado por Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generado por Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Generado por Viavi Test Instrument </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> Pasa</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Frecuencia de Barrido (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Pasa / Fallo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Configuraciones: Filtros /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation>(Patrón y Máscara)</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Patrón</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Máscara</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Configuraciones no aplicables...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Radio</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Banda</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Resultado general de la comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>En Curso</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Nombre del evento</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Duración / Valor</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Registro de eventos de prueba de carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Estado - Desconocido</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>En curso. Por favor, espere...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>¡Fallado!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>¡Comando Completado!</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>¡Anulado!</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Bucle Activo</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Bucle Inferior</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Arm</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Disarm</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Alimentación fuera</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Enviar Comando Bucle</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Switch</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Conmutar reinicio</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Pregunta Asunto</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Pregunta Bucle</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Near End Arm</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Near End Disarm</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Petición de alimentación</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Span Query</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>Timeout Disable</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>Timeout Reset</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Bucle Secuencial</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Stratum 1 Trace</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Transit Node Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Reservado</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Stratum 2 Trace</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Stratum 3 Trace</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Reservado</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet Min Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Stratum 3E Trace</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provision by Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Don't Use for Synchronization</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Equipado no Específico</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>Estructura TUG</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>TU Bloqueada</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>34M/45M Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>140M Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>Mapeo ATM</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>Mapeo MAN (DQDB)</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>Mapeo FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>Mapeo HDLC/PPP</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 Unscrambled</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>Señal Test O.181</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Bit síncrono</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Byte síncrono</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT-Structured STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Modo VT Bloq</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Mapeo DS3 Asínc.</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Mapeo DS4NA Asínc.</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Mapeo FDDI Asínc.</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 VT Payload Defect</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 VT Payload Defects</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>¿Format?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Fuera de rango</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>HISTÓRICO</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + HISTÓRICO</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Espacio</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Marca</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>VERDE</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>AMARILLO</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>ROJO</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NINGUNO</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>TODOS</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>RECHAZAR</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>INCIERTO</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ACEPTAR</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>DESCONOCIDO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FAIL</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASA</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Estado desconocido</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Identificado</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>No se puede identificar</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Identidad desconocida</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>queda %1 horas y %2 minutos</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>%1 queda minuto</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>DEMASIADO BAJO</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>DEMASIADO ALTO</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>(DEMASIADO BAJO) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>(DEMASIADO ALTO) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Puerto tributario</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>indefinido</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>No equipado</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Mapeo bajo desarrollo</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC sobre SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Simple Data Link Mapping (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>Mapeo HCLC/LAP-S</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Simple Data Link Mapping (set-reset)</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>Mapeo Tramas Ethernet 10 Gbit/s</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>Mapeo GFP</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>Mapeo Tramas Fiber Channel 10 Gbit/s</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Reservado - Propietario</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Reservado - Nacional</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Test Mapeo Señal O.181</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Reservado (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Equipado - no específico</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Mapeo DS3 Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Mapeo DS4NA Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Mapeo FDDI Asíncrono</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC sobre SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 VT Payload Defect</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI sin asignación</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Esper. TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Est. Esper. TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI Asignado</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Esper. Est.</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Esper. Rel.</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Mult. Trama Est.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Recuperación de temporizador</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Enlace desconocido</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>EN ESPERA DE ESTABLECIMIENTO</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MARCO MÚLTIPLE ESTABLECIDO</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>COLGAR</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>TONO DE MARCAR</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>MARCACIÓN EN BLOQUE</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>LLAMANDO</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>CONECTADO</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>LIBERANDO LLAMADA</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Datos</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletexto</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotexto</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>Voz BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Datos BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Datos 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Buscando</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Solicitud de join</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Reintentar solicitud</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Abandonar</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Recon. ráfaga completa</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Respuesta de join</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Completar ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Respuesta de estado</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Know Hole in Stream</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Error</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Err: Servicio todavía sin búfer</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Err: Solicitud inválida de reintento de paquete</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Err: No existe el servicio</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Err: No existe la sección</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Err: Err: </translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Err: Comando y versión de control no soportados</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Err: Servidor lleno</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Err: Duplicar ingreso</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Err: Duplicar Identificaciones de sesión</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Err: Tasa de bit errónea</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Err: Sesión destruida por el servidor</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>corto</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>abierto</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>normal</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>contrario</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Nivel demasiado bajo</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 bytes</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Seleccionado</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>No Seleccionado</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>INICIO</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>FIN</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Tramas con Error</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Tramas OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Tramas Perdidas</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violaciones Código</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>El registro del evento está lleno</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>No se encontró la llave USB. Por favor, inserte uno e inténtelo de nuevo.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>No hay ayuda para este asunto</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>ID de unidad</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>Sin señal</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Señal</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Listo</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Utilizado</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>ID de satélite</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>ID de GNSS</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Tramas</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Máscara</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>Máscara MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>Máscara TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>Resultados MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>Máscara MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>Resultados TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>Máscara TDEV</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Datos TIE orig.</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Elim. datos offset</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>Estilo de Curva MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Línea + Puntos</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Solo puntos</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Solo MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Solo TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Tipo de Máscara</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>Pasa MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>Falla MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>Pasa TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>Falla TDEV</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Intervalo de Observación (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Calculando </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Cálculo canceled</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Cálculo finalizado</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>Actualizando datos TIE </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>Cargados datos TIE</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>No se han cargado datos de TIE</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Memoria insuficiente para realizar Análisis de Wander localmente.&#xA;se necesitan 256 MB. Utilice análisis de software externo.&#xA;Consulte el manual.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Offset Frec (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Deriva (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Muestras</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Tasa de muestra (por seg.)</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Bloques</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Bloque Actual</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Eliminar Offset</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Selección de Curva</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Ambas curvas</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Elim.solo.offs</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Capturar pantalla</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tiempo (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Sin etiqueta</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Puerto 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Puerto 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Barra de herramientas</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Log de mensajes</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Info General:</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Desactivar gráficos</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Error de impresión</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps Min</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps Máx</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Jitter de PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Máx Jitter de PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err tot CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Máx err CC</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err tot PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Máx err PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err tot PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Máx err PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Streams</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Err Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Err Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID transporte Strm</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pérd paq Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pérd paq Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pérd paq Pico</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Jitter paq (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Máx Jitter paq (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Tot. Paq OoS</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Paq OoS Act</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Paq OoS Máx</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot.Dist. Err</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Dist. Err act</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Máx dist err</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Tot. Período err</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Período err Act</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Máx Período err</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Período de pérdidas máx</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Dist pérd mín</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Tot. Pérdidas sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Tot. Err byte sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err byte sincr Act</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err byte sinc máx</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Act</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>Máx DF MDI</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Act</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>Máx MLR MDI</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Tot. Err transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err transp act</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err máx transp</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err tot PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err PAT Act</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Máx err PAT</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># Analizar Flujos</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>C1 Total Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Err Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Err Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID transporte Strm</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Prog Nú.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Prog Mbps Act</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Prog Mbps mín</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Prog Mbps máx</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pérd paq Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pérd paq Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pérd paq Pico</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Jitter paq (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Máx Jitter paq (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>Tot pqts OoS</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Paq OoS Act</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Paq OoS Máx</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot.Dist. Err</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Dist. Err act</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Máx dist err</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Tot. Período err</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Período err Act</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Máx Período err</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Período de pérdidas máx</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Dist pérd mín </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Tot. Pérdidas sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Tot. Err byte sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err byte sincr Act</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err byte sinc máx</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Act</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>Máx DF MDI</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Act</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>Máx MLR MDI</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>Jitter de PCR  act</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Máx Jitter de PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Err tot CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>Err CC Act</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Máx err CC</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Tot. Err transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err transp act</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err máx transp</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Err tot PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err PAT Act</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Máx err PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Err tot PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>Err PMT Act</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Máx err PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Err tot PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>Err PID Act</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Máx err PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>Direc IP</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pérd. Paq.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Pér máx paq</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Jit paq</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Máx Jit paq</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Streams</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Mbps, act C1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Prog Mbps mín </translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>ID transporte</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Pérdidas sincr</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Total err Byte sincr</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err byte sinc</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Err byte sinc máx</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Total err PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>Err PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Máx err PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Jitter de PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>Máx Jitter de PCR </translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Total err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Máx err CC</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Total err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Máx err PMT</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Total err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Máx err PID</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Total err transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Err transp</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Err máx transp</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># Analizar Flujos</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>C1 Total Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Err Chksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Err Chksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>Histórico MPEG</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pérd paq Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pérd paq Tot</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pérd paq Pico</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Jitter Pqt Act</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Máx Jitter paq</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico a Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pico Positivo</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pico Negativo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Pico a Pico</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Pico Pos</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Pico Neg</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Información del reporte de prueba</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Código</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Trayecto</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>no usado</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Resultado del test general óptico</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalles</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Apilado</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trama</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLAN apilada</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profundidad de pila VLAN</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>Bit DEI SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>Prioridad Usuario SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>Usuario SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>No se han definido tramas</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valor</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Puerto tributario</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Resultado de la general de Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>Captura de byte POH</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trama</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hexa</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binario</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Configuraciones no aplicables...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Bytes de Cabecera</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>Receptor CDMA</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Error de impresión</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP Test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Perfil cargado</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>Resultado de la prueba general PTP </translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Comprobación de PTP</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumen</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>RESUMEN RESULTADOS OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Prueba FC mejorada</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Prueba RFC 2544 mejorada</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Resultados Totales Medida</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Resultados generales de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latencia</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Pérdida Trama</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Ráfagas</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recup. sistema</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>No se ejecutó - La prueba anterior fue detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>No se ejecutó - La prueba anterior fue anulada con errores</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>No se ejecutó - La prueba anterior falló y se activó la detención por falla</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>No en ejecución</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Bucle inverso simétrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>Subida y bajada simétrica</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Subidas y bajadas asimérticas</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Flujo descendente</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latencia</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Pérdida Trama</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Ráfaga (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recup. sistema</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Medidas a realizar</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Longitudes de marcos seleccionadas (bis)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Longitudes del paquete seleccionadas</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Longitudes de marcos de subida seleccionados (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Longitudes del paquete de subida seleccionadas (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Longitudes de marcos de bajada seleccionados (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Longitudes del paquete de bajada seleccionadas (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Longitudes de marcos de bajada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marcos de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marco de subida de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 Gráfico de prueba de pérdida de marco de bajada de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 Gráfico de pruebas de rendimiento de crédito de bytes del buffer</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Duración (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Más largo</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Más corto</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Último</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interrupciones</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Conector</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Ancho de banda nominal (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Tasa de bit nominal (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Longitud de onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Tasa mínima bit (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Tasa máxima bit (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo de nivel potencia</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fabricante</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Fabricante PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Fabricante Rev</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Nivel Rx Máx(dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Nivel Tx Máx(dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN fabricante</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Código de fecha</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Código de lote</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitorización diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceptor</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>No. versión hardware/software</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>No. rev. Especificación hardware MSA</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>No. rev. admin. I/F</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Clase de potencia</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo de nivel de potencia Rx</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Potencia Lambda máxima (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation># de capas activas</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Anchos de banda por fibra</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>Ancho de banda por rango de fibras (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Tasa máxima bit de carril de red (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Tasas soportadas</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duración</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>No válido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Retardo de trama (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Retardo de trama (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Retardo de ida</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Flujo descendente</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Fila</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Encender</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Solución de problemas</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimétrico</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Simetría de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Ruta MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Pasear la ventana</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avanzado</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Pasos para ejecutar</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>No se ejecutó - La prueba anterior fue detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>No se ejecutó - La prueba anterior fue anulada con errores</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>No se ejecutó - La prueba anterior falló y se activó la detención por falla</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>No en ejecución</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Prueba TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>El usuario abortó la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>No se inició la prueba.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Pasa corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>El rendimiento es mayor que el 90% del objetivo.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Fallo corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>El rendimiento es menor que el 90% del objetivo.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Pasa corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Falla corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Información del reporte de prueba</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nombre Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Compañía</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Correo electrónico</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Teléfono</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificación de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nombre de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Código de autenticación</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Fecha de creación de la autenticación</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Hora de detención de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentarios</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Traza</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Secuencia</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>StrmIP:Puerto</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nombre Strm</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog Nombre</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>En Curso</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>Resultado de la comprobación general de exploración de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resulta.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>Resultado de la prueba general TrueSAM </translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>La prueba no se pudo completar y se anuló con errores. Los resultados del informe pueden estar incompletos.</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>La prueba fue detenida por el usuario. Los resultados del informe pueden estar incompletos.</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Resultados de la sub-prueba</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>Resultado de la prueba J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>Resultado de la prueba RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>Resultado de la prueba SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>Resultado de la prueba J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>Resultado de la prueba TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>No se ejecutó - La prueba anterior fue detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>No se ejecutó - La prueba anterior fue anulada con errores</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>No se ejecutó - La prueba anterior falló y se activó la detención por falla</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>No en ejecución</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Log de mensajes</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Mensaje registro (continuo)</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Orden de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Comando / Notas</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero de Serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versión de software</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>Resultado de la prueba general SAMComplete </translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAM Completa</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Fecha inicial</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Fecha final</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Resultados de prueba general de configuración</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Resultados de prueba general de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Pérdida Trama</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variación de retardo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Grupos de informes</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>No se pudo crear el informe: </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>espacio de disco insuficiente.</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>El Informe no pudo crearse</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Configuración puerto</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Impar</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Par</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Tasa baud</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Paridad</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Control de flujo</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Off</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Bits datos</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Bits de parada</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Configuración terminal</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Aceptar/volver</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Eco local</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Activar teclas reservadas</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Teclas inactivas</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Desactivar</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Habilitar</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>EXPORTAR, ARCHIVO, CONFIGURAR, RESULTADOS, SCRIPT, INICIAR/DETENER, Teclas de panel programables</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ARCHIVO, CONFIGURACIÓN, RESULTADOS, SCRIPT, INICIO/PARADA, teclas programables del panel</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ARCHIVO, CONFIGURACIÓN, RESULTADOS, SCRIPT, INICIO/PARADA, teclas programables del panel</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>ARCHIVO, CONFIGURACIÓN, EXPORTAR, INICIO / PARADA, teclas programables del panel</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Terminal&#xA;Window</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Restaurar valores&#xA;por defecto</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>Vt100&#xA;Configuración</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Borrar&#xA;Pantalla</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Mostrar&#xA;Teclado</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Mover&#xA;Teclado</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>Autobaud</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Capturar&#xA;Pantalla</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Ocultar&#xA;Teclado</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Situación Bucle:</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Resultado:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Se encontró error inesperado</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Lanzar&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;Esto examinará de nuevo el enlace de flujos y reiniciará la prueba.&#xA;&#xA;Usted no verá solamente los flujos que fueron&#xA;transferidos desde la aplicación Explorer.&#xA;&#xA;¿Continúa?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Auto en curso:</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Detalle:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Auto en curso. Por favor, espere...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Falló automáticamente.</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Completado automáticamente.</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Anulado automáticamente.</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Estado - Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Detectando </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Explorando...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fallado</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Se encontró error inesperado</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Detalles de dispositivo emparejado</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Enviar archivo</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Olvidar</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Seleccionar archivo</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Enviar</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Conectando...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Desconectando...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Nombre de paquete: </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Introducir nombre de agrupación</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Certificados y claves (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Nombre de la agrupación no válido</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Ya existe una agrupación con el nombre de "%1".&#xA;Por favor, seleccione otro nombre.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Añadir archivos</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Crear agrupación</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Renombrar agrupación</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Agrupación modificada</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Abrir carpeta</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Añadir nueva agrupación...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Añadir certificados a %1 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Eliminar %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>Eliminar %1 de %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>ID de vía virtual</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>Sesgado inyectado (bits)</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Sesgado inyectado (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>Carril físico #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Margen:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>No se puede configurar la fecha</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Ranura tributaria</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Aplicar</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predeter.</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Velocidad</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Todavía no se han aplicado cambios</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>Se han seleccionado demasiadas ranuras trib.</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>Se han seleccionado demasiadas pocas ranuras trib.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Otro...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Hoy</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Introducir fecha: dd/mm/aaaa</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Introducir fecha: mm/dd/aaaa</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Hasta </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> bytes</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Segs</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Minutos</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Horas</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Días</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>dd/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Duración: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Configurar las Direcciones Destino en etiqueta Todos Servicios.</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Configurar las Direcciones Destino en etiqueta Todos los Flujos.</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>Destino MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo Destino</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo Bucle</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>IP Fuente salto</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>IP dest siguiente Salto</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Máscar Subred siguiente salto</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Configure la dirección de origen en la sección Ethernet para todas las tramas.</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo Fuente</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC por defecto</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC Usuario</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Configurar dirección Fuente en etiqueta Todos Servicios.</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Configurar dirección Fuente en etiqueta Todos Flujos.</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Fuente</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridad Usuario SVLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incremento Bit Prioridad</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>Usuario SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>No se puede configurar en modo bucle.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>Especificar ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridad Usuario</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Long.</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Longitud Datos (bytes)</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Control</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Longitud/&#xA;Tipo</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Túnel&#xA;Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>Filtro B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>Prioridad B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>Bit DEI B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Túnel Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Prioridad Túnel</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>Túnel TTL</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA;Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>Prioridad I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>Bit DEI I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>Bit UCA I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>Filtro I-Tag Service ID</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag Service ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>VC Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>Prioridad VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA;Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>MPLS1 Label</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>Prioridad MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA;Etiqueta</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>MPLS2 Label</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>Prioridad MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Datos</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Trama del cliente que se transporta:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulación</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo de Trama</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Tamaño de trama del Cliente</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Tamaño Trama Usuario</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Tamaño unsersized</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Tamaño Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>Sección de datos contiene un paquete IP. Configurar este paquete en etiqueta IP.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Configurar datos filtrando en la tab Filtro IP</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Carga Tx</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>Configuración de RTD</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Análisis carga</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Carga Rx</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>Temporizador LPAC</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Patrón BERT Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Patrón BERT Tx</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Patrón Usuario</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Configurar tramas de entrada:</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Configurar tramas de salida:</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>Campo Long/Tipo es 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>Longitud de los datos aleatoria</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Use el ?ndice de comienzo prioritario</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Tipo archivo:</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nombre del Fichero:</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>¿Está seguro de querer borrar&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Seguro que desea borrar todos los archivos en esta carpeta</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Dígitos</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation> Byte</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Hasta </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Rango:  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> dígitos</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> es inválido - IPS fuentes de los puertos 1 y&#xA;2 no deberían coincidir. IP previa restaurada.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Dirección IP Inválida&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Rango:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>La dirección IP dada no es aconsejable para esta configuración.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 caracteres por byte, hasta </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 caracteres por byte, </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Nombre Código-Bucle</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Patrón de Bit</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Entrega</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>En banda</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Fuera de banda</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Bucle Activo</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Bucle Inferior</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Otro</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Nombre Código-Bucle</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Del.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Agregar / Quitar</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Rastro Tx</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Rango:  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Seleccione canal</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>Entre valor KLM</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Rango:  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Seleccione el miembro VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>Byte PSI</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Valor del Byte</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>Tipo ODU</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>No. del puerto tributario</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Rango:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Seleccione el miembro VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Rango:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Editor Byte Cabecera</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Seleccione un byte válido para editar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Óptica no reconocida</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Construyendo la página.  Por favor, espere...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>La página está vacía.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Añadir</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Modificar</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Añadir Fila</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Modificar Fila</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Borrar Fila</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Off</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Traceability Unknown</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>No equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>No equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>No equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>No equipado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Longitud:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Long.:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caracteres</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Ahora</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Introducir tiempo: hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Selecc nada</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Byte %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nombre del Servicio</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Ambas direcciones</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Tipo del servicio</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>El tipo de servicio se reinició a Datos, a causa de los cambios para longitud de trama y/o CIR.</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Frec de Muestreo (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Llamadas</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamaño Trama</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Longitud del Paquete</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Canales</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compresión</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>Crea VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>Define los miembros VCG con numeración de canal estandar</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Define el VCG de Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Define el VCG de Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Velocidad de carga (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Número de miembros</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Distribuye&#xA;Miembros</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>Distribuye miembros VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Defina distribución de clientes de miembros VCG</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Instancia</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Paso</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>Editar miembros VCG</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Formato de dirección</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Nuevo miembro</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predeter.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>Los streams seleccionados se han importado del Explorer&#xA;Pulse la tecla [Join Streams...] para seleccionar</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Directorio</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Seleccione Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Encuentre el próximo</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Quitar</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Seleccione Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Join Streams...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Añadir</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Destino IP</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>Las IPs introducidas se añadirán a la Libreta de direcciones</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Para Unirse</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Ya pertenece</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>Ensambla</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Salir Stream</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Dirección IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>Dirección IP Dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Salir Streams...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Selecc nada</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Config Rápida</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Nota: se sobreescribirá la configuración actual de la trama.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rápido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Full (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Profundidad</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Grupo</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulación</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Incremento Bit Prioridad</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridad Usuario</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>Usuario TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Aplicando</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Use el ?ndice de comienzo prioritario</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridad Usuario SVLAN</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Cargar...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Fecha larga:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Fecha corta:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Tiempo prolongado:</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Tiempo corto:</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Números:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Sin localizar</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Asignado</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>reservado 10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reservado11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reservado01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reservado00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Guardar...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Poner Valores del Test por Defecto</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Configurar&#xA;flujos...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Distribución Carga</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Selecc&#xA;todo</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Borrar&#xA;todo</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Distribución&#xA;automática</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flujo</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamaño Trama</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Velocidad de trama</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% de tasa lín.</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>Rampa iniciando en</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Ráfagas</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Constante</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Umbral de util, Max</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Total (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Total (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Total (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Total (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Nota: </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Al menos se debe activar un flujo.</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>El umbral de máxima utilización es </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>La máxima carga posible es </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>La carga total especificada no lo puede sobrepasar.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Introducir porcentaje:  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Especifique la velocidad de imagen:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Activar tasa de bit:  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Nota:&#xA;No se detectó la tasa de bits. Por favor, presione Cancelar&#xA;y reintente cuando se haya detectado la tasa de bits.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Definir Servicios Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Frec de&#xA;Muestreo (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Llamadas</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Tasa por&#xA;llamada (Kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Tasa Total&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Tamaño Trama básico&#xA;total (bytes)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Supresión de silencio</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Búfer para jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Canales</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compresión</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Tasa (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Tamaño Trama básico total (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Tasa Iniciar (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Tipo Carga</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Pasos Tiempo (s)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Paso carga (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>C1 Total Mbps</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Simulado</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Datos 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Datos 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Datos 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Datos 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aleatorio</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Video</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Datos</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Nota:</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Al menos se debe activar un servicio.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>La máxima carga posible es </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>La carga total especificada no lo puede sobrepasar.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Distribución: </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Velocidad:</translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Estructura: </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>predeterminado</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Paso</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Directorio</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Nueva entrada</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>IP Fuente (0.0.0.0 = "Alguno")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>Necesaria</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Nombre (16 caracteres maximo)</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Añadir Entrada</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Importar/Exportar</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importar</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>Importe las entradas de un perisférico USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Exporte entradas a un disco USB</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Borrar Todos</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Archive y cierre</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Opcional</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Importe las entradas de un USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Importe las entradas</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Separada por commas (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Añadiendo dirección</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Cada entrada debe tener 4 campos: dirección de origen, destino, PID y nombre separado por comas. Selección anulada</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abort</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Continue</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>Dirección origen IP inválida. Selección anulada.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>Dirección destino IP inválida. Selección anulada.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>PID inválido. Selección anulada.</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Una entrada necesita un nombre (16 caracteres máximo). Selección anulada</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Una entrada no puede ser más de 16 caracteres. Selección anulada</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Una entrada con la misma dirección de origen, destino y PID&#xA;existe y está nombrada</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>¿SOBREESCRIBA el nombre de la entrada existente&#xA;ó&#xA;anule la importación de la entrada?</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Skip</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Sobreescribir</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Una de las entradas tien un valor PID.&#xA;Entradas con valores PID son solo usadas en aplicaciones MPTS.</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Exporte entradas a USB</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Exporte entradas</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>Agenda_IPTV</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>En uso. Elija un nombre diferente ó corrija la entrada.</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>Existe entrada con estos parametros.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>¿Seguro que desea cancelar TODAS las entradas?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Destino IP</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Añadiendo dirección</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>Existe una entrada con el mismo nombre</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>¿Qué desea hacer?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Fecha: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Hora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos de Eventos, utilice la selección de menú Ver->Ventana de resultados->Sencillo.</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Código</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Trayecto</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos de byte K1/K2, utilice la selección de menú Ver->Ventana de resultados->Sencillo</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trama</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hexa</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binario</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver datos de más de un evento de retardo unidireccional, use la selección de menú Ver->Ventanas de resultado->Simple.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Pérdida Fuente</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>Err B1</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>Err REI-L</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>Err MS-REI</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>Err B2</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>Err REI-P</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>Error B2</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>Err HP-REI</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>Err B3</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>Err REI-V</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>Err BIP-V</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>Error B3</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>Err LP-REI</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>Err LP-BIP</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>Errores STL FAS</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Err Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Pérdida señal</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Pérd Sinc Trm</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Err Pal Trm</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>Error FAS</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Inciar Tiempo</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Fin Tiempo</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Más largo</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Más corto</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Último</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interrupciones</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos de interrupción de servicio, utilice la selección de menú Ver->Ventana de resultados->Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos de interrupción de servicio, utilice la selección de menú Ver->Ventana de resultados->Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>INICIO</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>FIN</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Sobrecarga</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos de resultados de llamada, utilice la selección de menú Ver->Ventana de resultados->Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>tono de discado</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  VERDAD</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Duración</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>No válido</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Fecha: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Hora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog Nombre</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>Strm IP. Puerto</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nombre Strm</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Exportar categoría archivada de resultado de cliente</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoría de resultado de cliente archivada</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>No se encuentra la memoria USB flash.&#xA;Por favor, inserte una flash, o retírela e insértela de nuevo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>Exportar Datos PTP al puerto USB</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>Archivos de datos PTP (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Exportar informe al puerto USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Exportar capturas de pantallas al puerto USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Captura de pantalla guardada (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Comprima con Zip archivos seleccionados como:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Introducir nombre de fichero: máx 60 carac</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Comprimir en zip&#xA;&amp;&amp; Exportar</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportar</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Por favor, ingrese un nombre para el archivo zip</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>No se encuentra la memoria USB flash.&#xA;Por favor, inserte una flash, o retírela e insértela de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Incapaz de comprimir el(los) archivo(s)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>Exportar Datos TIE al puerto USB</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Archivo de datos TIE Wander (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Exportar datos de tiempo a USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Todos los archivos de datos de tiempo (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Tipo archivo:</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Abierto</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Importar categoría de resultados guardados de clientes desde USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoría de resultado de cliente archivada</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Importar&#xA;Categoría de cliente</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Importar logo de informe desde USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Ficheros Imagen (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Importar &#xA; Logo</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Importar una ficha rápida desde USB</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Archivos PDF (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Importar&#xA;Ficha rápida</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Importar&#xA;Medida</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Descomprimir el zip&#xA; &amp;&amp; Importar</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Error - Incapaz de unTAR uno de los archivos.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>No hay suficiente espacio en el destino.&#xA;La operación de copiar será cancelada.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Copiando ficheros...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Hecho. Archivos copiados.</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Error: Los siguientes elementos no se pudieron copiar: &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>Guardar datos PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>Archivos PTP (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copiar</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Espacio en disco insuficiente. Borre otros archivos guardados, o exporte a USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%1 libre de %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>No hay archivos seleccionados</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>Total %1 en archivo seleccionado</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Total %1 en %2 archivos seleccionadas</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nombre del Fichero:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Introducir nombre de fichero: máx 60 carac</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>¿Borrar todos los ficheros dentro de esta carpeta?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Los archivos de solo lectura no serán eliminados.)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Eliminando archivos ...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>¿Está seguro de querer borrar&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>¿Está seguro de querer borrar este ítem?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>No es óptica recomendada</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16 G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614,4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Muestra los resultados del descubrimiento y escaneo de la estructura de la señal.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Ordenar por:</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Clasificación</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Reordene</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Presenta una selección de pruebas disponibles que se pueden utilizar para analizar el canal seleccionado</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Iniciar Test</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Por favor espere ... configurando canal seleccionado ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Mostrar detalles</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Ocultar detalles</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Rango: %1 a %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Encontrar</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Original</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>Ajustar ancho</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>Ajustar altura</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Selección de la vista de prueba doble</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Más...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(Continuación)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Atención</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Búfer de registro lleno</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Captura terminada</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Editar Fila</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Seleccionar byte:</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>No es posible capturar impresión de la pantalla</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>insuficiente espacio en disco</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Impresión Pantalla capturada: </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>Sobre Stream</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>Teclado DP</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>Teclado MF</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>Teclado DTMF</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Unidir</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Idle</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Tráfico extra</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>No disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>No se puede navegar a enlaces externos</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>No encontrado</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Se ha llegado al final de la página, se continúa desde arriba</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Seleccionar herramienta</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Desactivando Informes Automáticos antes de iniciar script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Activando Informes Automáticos antes de iniciar script.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crear</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predeter.</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Borrar Todos</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Enviar</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Repetir</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ver</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS (Carga)</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS (Origen)</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Contenedor </translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Etiq. Señal</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Traza</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Formato de Traza</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Desconocido</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Esto representa solamente el nivel actual y no toma en consideración canales de orden inferior o superior. Solamente el canal actualmente seleccionado recibirá actualizaciones en tiempo real.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>El estado del canal representado por un ícono.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Alarmas no monitorizadas presentes</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Alarmas presentes</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Monitorizando con alarmas no monitorizadas presentes</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Monitorizando con alarmas presentes</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>El nombre del contenedor del canal. Un sufijo "c"  indica un canal concatenado.</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>El número N KLM del canal según las especificaciones de RFC 4606</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>La etiqueta de señal del canal</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>El estado más reciente conocido del canal</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>El canal es inválido.</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI presente</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS presente</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP presente</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Monitorizando</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>Estado actualizado en: </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>nunca</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>La información de traza del canal</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>La información de formato de traza del canal</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>No soportado</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Explorando</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Alarma</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>No válido</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Sin formatear</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Un Byte</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>CR/LF Terminación</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>dispositivos emparejados </translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Dispositivos detectados</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>Certificación CA</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Certificación de cliente</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Clave de cliente</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Espacio libre</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Capacidad total</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fabricante</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Rótulo</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Versión actualizada</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Versión instalada</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Categorías</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Liste los nombres asignados a las categorías de cliente. Haciendo clic en un nombre se habilitará/deshabilitará esa categoría de cliente.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Permite la configuración de una categoría de cliente cuando hace clic.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Permite la eliminación de una categoría de cliente al seleccionar las categorías que se desean eliminar.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>El nombre asignado a la categoría de cliente. Haciendo clic en el nombre habilitará/deshabilitará la categoría de cliente.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Haciendo clic en el ícono de configuración se abrirá un diálogo de configuración para la categoría de cliente.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Haciendo clic en el ícono de eliminar se marcará o desmarcará la categoría de cliente para eliminación.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crear Informe</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crear</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ver informe después de la creación</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Error - El archivo no puede estar vacío.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>La modificación de las configuraciones refrescarán los resultados actuales.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nombre DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nombre de NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>No está en la subred</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>Esperando por enlace activo...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>Esperando por DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Reconfigurando IP de origen...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Fuente</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nombre DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nombre de NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Nombre del sistema</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>No está en la subred</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>No está en la subred</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nombre DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nombre de NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servicios</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>No está en la subred</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servicios</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nombre DNS</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Descubrimiento</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>Prioridad VLAN</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>IP de red</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nombre de NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivos</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>Redes IP</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Dominios</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Servidores</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Dispositivos</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Conmutadores</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Enrutadores</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Parámetros</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Infraestructura</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Medular</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Distribución</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Acceso</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Redes IP descubiertas</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Dominios NetBIOS descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>VLANS descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Enrutadores descubiertas</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Conmutadores descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Dispositivos descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Servidores descubiertas</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Informe de descubrimiento de red</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>El Informe no pudo crearse</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>insuficiente espacio en disco</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generado por Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generado por Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generado por Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Generado por Viavi Test Instrument </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>General</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>el disco está lleno, se pararán todos los gráficos.&#xA;Por favor, libere espacio y reinicie la medida.&#xA;&#xA;Alternativamente, se puede desactivar los gráficos de Herramientas->Customizar en&#xA;la barra de herramientas.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Pulsar para centrar escala de tiempo</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Propiedades gráfico</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx.</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>KB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Ventana</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Ventana de saturación</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Guardar datos muestra</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>No hay espacio suficiente en el dispositivo de servicio.</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>Puede exportar directamente a USB si se ha introducido un dispositivo USB.</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Guardando datos de gráficos. Esto puede llevar cierto tiempo. Por favor, espere...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Guardando datos de gráficos</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>No hay espacio suficiente en el dispositivo de servicio. Los datos de gráficos exportados no están completos.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Se ha producido un error durante la exportación de los datos del gráfico. Es posible que los datos estén incompletos.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 día</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 horas</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 hora</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minutos</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 segundos</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Exhibir datos de forma gráfica</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Pulsar y arrastrar para zoom</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Saturación</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>KB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Ventana</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Con.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tiempo (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensaje</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Puerto Src</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Puerto dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Suprimir...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Confirmar ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>Las categorías seleccionadas serán eliminadas de todas las pruebas que se están ejecutando actualmente.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>¿Está seguro de que desea eliminar los elementos seleccionados?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Nuevo ...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Abre un diálogo para confirmar una nueva categoría de resultados de cliente.</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>Puede presionar esto para marcar categorías de cliente y eliminarlas de la unidad. Presione de nuevo el botón cuando esté lista su selección para eliminar los archivos.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Presione "%1"&#xA;para comenzar</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Configurar Categoría Resultados</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>Los resultados marcados con un '*' no aplican a la configuración actual de prueba y no aparecerán en la ventana de resultados.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Nombre de categoría:</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Ingrese el nombre de la categoría del cliente: %1 caracteres máximo</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nombre del Fichero:</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Guardar como</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Guardar nuevo</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>El archivo %1 que contiene la&#xA;categoría "%2" </translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Resultados seleccionados: </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   (Selecciónes max </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configurar...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumen</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumen</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Máx (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Valor (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Auto-Y</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Inicio de la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Devolver la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Adelantar la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Final de la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>Histórico de no llamadas</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Inicio de la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Devolver la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Adelantar la ayuda botón de herramientas ...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Final de la ayuda botón de herramientas ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico a Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pico Positivo</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pico Negativo</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Auto-Y</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Pico a Pico</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pico Positivo</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Pico Negativo</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Auto-Y</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Latencia (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Número</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NO&#xA;DISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Estado: PASA</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Estado: FALLA</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Estado: N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Expandir para ver las opciones de filtro</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation># MEPs descubiertos</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Establecer como igual</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Filtrar la pantalla</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Filtro encendido</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Ingresar valor de filtro: %1 caracteres máximo</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>Tipo CCM</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>Tasa CCM</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>ID contraparte MEP</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>Maintenance Domain Level</translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Especificar ID de dominio</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ID dominio de mantenimiento</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>ID asociación de mantenimiento</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Ajustes de prueba configurados. La fila realzada se ha establecido como la contraparte MEP para esta prueba.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Ajustando la prueba establecida como la que la contraparte MEP realzada falló.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>No fue posible establecer %1 en %2.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NO&#xA;DISPONIBLE</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>GRÁFICOS&#xA;DESACTIVADOS</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Pulse esta ventana de resultados para expandirla a pantalla completa</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>NO&#xA;DISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Usuario</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 Resultados de prueba de pérdida de marcos de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 Resultados de prueba de pérdida de marco de subida de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 Resultado de prueba de pérdida de marco de bajada de bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 Resultados de pruebas de rendimiento de crédito de bytes del buffer</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Resultados de pruebas de rendimiento de crédito de subida de bytes del buffer</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Resultados de pruebas de rendimiento de crédito de bajada de bytes del buffer</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Exportar archivo de texto...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Se exportó el registro a </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Stream&#xA;Detalles</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Reducir todos los resultados en esta ventana</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Expandir todos los resultado en esta ventana</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>RESULTADOS&#xA;CATEGORÍA&#xA;ESTÁ VACÍO</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NO&#xA;DISPONIBLE</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NO&#xA;DISPONIBLE</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>RESUMEN&#xA;RESULTADOS&#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Columnas</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Mostrar columnas</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Selecc nada</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Mostrar solamente errores</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Columnas...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Muestra&#xA;solamente filas&#xA;con errores</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Muestra solamente las filas con errores</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Tasas de tráfico (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Tasas de tráfico (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation># Flujos analizados</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Tráfico agrupado por</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>Total de enlaces</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Flujos mostrados 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Flujos adicionales >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Columnas...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Salto</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Retardo (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Para ver más datos Traceroute, utilice la selección de menú Ver->Ventana de resultados->Sencillo</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Tiempo ideal de transferencia</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Tiempo real de transferencia</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Grupo:</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flujo</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Programas</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Pérdidas de paquetes</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Distancia err</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Período err</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err byte sinc</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Mostrar sólo programas con error</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Columnas...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Prog. Total Mbps</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Mostrar sólo&#xA;progs con err</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Mostrar sólo programas con error</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Mostrar&#xA;sólo Flujos&#xA;con err</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Mostrar sólo Flujos con error</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analizar</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation># Analizar&#xA;Flujos</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>C1 Mbps&#xA;Total</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>Errores&#xA;Checksum IP</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>Errores&#xA;Checksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Lanzar&#xA;Analizador</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Columnas...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Por favor, espere... lanzando aplicación...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Auto-Y</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>S</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Día</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Auto-Y</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Elija Script.. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Script:</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Estado:</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Estado Actual</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Temporizador:</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Tiempo transcurrido del Script:</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Cantidad contador</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Salida:</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Script finalizado en: </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Escoja&#xA;Script</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Iniciar Script</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Borrar&#xA;Salida</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Parar Script</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>CORRIENDO...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Tiempo transcurrido del Script:</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Elija una diversa Script.. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Error.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>CORRIENDO.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>CORRIENDO..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Personalizar lista de comprobaciones</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Mostrar resultados al iniciar</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Mover hacia arriba</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Mover hacia abajo</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Borrar Todos</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renombrar</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Separador</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Añadir atajo de teclado</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Añadir prueba guardada</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>¿Eliminar todos los favoritos?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>La lista de favoritos es la original.</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Se perderán todos los favoritos personalizados y se restaurará la lista a los valores originales para esta unidad. ¿Desea usted continuar?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Configuraciones de la prueba (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Configuraciones de prueba dual  (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Seleccionar prueba guardada</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Espere...&#xA;Iniciando vista Prueba vacía</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Prueba de patilla</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Cambiar nombre de prueba de patillas</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>Lista de pruebas de patillas</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Guardar configuración de prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nombre Test:</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Ingresar el nombre a mostrar</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Esta prueba es la misma de %1</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>Existe un atajo de teclado para iniciar una aplicación de prueba.</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Descripción: %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Esto se guarda en la configuración de la prueba.</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Nombre del Fichero: %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Reemplazar</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>No se puede iniciar la prueba ...&#xA;Las opciones han caducado.&#xA;Salga y reinicie BERT desde la página del sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>Esta prueba no puede ser lanzada en este momento. Podría no ser soportada por la configuración de hardware actual, o podría requerir más recursos.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Intente eliminar pruebas que se estén corriendo en otras pestañas.</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Esta prueba se está ejecutando en otro puerto.¿Desea ir a esa prueba? (en la pestaña %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>Otra prueba (en la pestaña %1) se puede reconfigurar a la prueba seleccionada.¿Desea reconfigurar esa prueba?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>La lista está vacía</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Imposible lanzar prueba...&#xA;Función de vibración óptica APAGADA.&#xA;Lanzar la función de vibración óptica desde Home/System Page.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>No es posible iniciar test...&#xA;No disponibles recursos alimentación.&#xA;Retirar/reconfigurar una medida existente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>No es posible iniciar test...&#xA;No disponibles recursos alimentación.&#xA;Apague otro módulo o retirar/reconfigurar&#xA;una medida existente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Por favor Espere...&#xA;Añadiendo Test </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Por favor, espere...&#xA;Reconfigurando Test a </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>No se pude iniciar la prueba.&#xA;Se requirieron recursos que tal vez estén en uso. Por favor, póngase en contacto con soporte técnico.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>Construyendo objetos UI</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>UI sincronizándose con el módulo de la aplicación</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>Inicializando vistas UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Volver</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Escoja&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright VIAVI Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Información de instrumento</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Opciones</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>Archivo guardado</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Modo de acceso de interfaz del usuario</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>El control remoto está en uso.&#xA;&#xA;El modo de sólo lectura evita que el usuario cambie las configuraciones&#xA;lo cual puede afectar las operaciones a control remoto.</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Modo de acceso</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>MSAM se ha reinizializado por un cambio en la configuración PIM.</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Un PIM se ha insertado o eliminado. Si se intercambian las PIMs, se puede continuar.&#xA;MSAM comenzará a funcionar de nuevo. Ésto puede durar 2 minutos. Por favor , espere...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>El Módulo BERT se está ejecutando demasiado bien y se&#xA;apagará automáticamente si se mantiene&#xA;subiendo la temperatura interna.Por favor, guarde sus datos, apague el&#xA;módulo BERT y llame al soporte técnico.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>El equipo está sobrecalentado. Módulo BERT se apagará.</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>XFP PIM en la ranura equivocada. Por favor traslade la XFP PIM al Puerto #1.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>El módulo BERT XFP PIM está en la ranura equivocada.&#xA;Por favor, traslade el XFP PIM al puerto #1.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>Se ha seleccionado una prueba eléctrica pero parece que el SFP es un SFP óptico.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Esté seguro que usted está utilizando un SFP eléctrico.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>El módulo BERT ha detectado un posible error en la aplicación %1.&#xA;&#xA;Usted ha seleccionado una prueba eléctrica, pero el SFP parece un SFP óptico.  Por favor, reemplace o seleccione otro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>Se ha seleccionado una prueba óptico pero parece que el SFP es un SFP eléctrica.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Esté seguro que usted está utilizando un SFP óptico.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>El dispositivo de prueba detectó un posible error en la aplicación %1.&#xA;&#xA;Usted seleccionó una prueba óptica, pero el SFP parece&#xA;eléctrico.  Por favor, reemplace o seleccione otro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>Usted ha seleccionado una prueba 10G, pero el transreceptor insertado no luce como un SFP+. </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Asegúrese de usar un SFP+.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>El dispositivo de prueba detectó un posible error.  Esta prueba necesita un SFP+, pero el transceptor no parece que lo sea. Por favor, reemplace con un SFP+.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Configuración automática informe</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Sobreescribir el mismo fichero</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoInforme</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Aviso:    Disco seleccionado está lleno. Puede liberar espacio manualmente, o permitir que los&#xA;5 informes más antiguos sean borrados automáticamente.</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Activar informes automáticos</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Período de informes</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Mín.:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Máx.:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Reiniciar comprobación después de creación de informe</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Crear un fichero separado</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Nombre del Informe</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>La fecha y la hora de creación se anexan automáticamente al nombre</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Texto</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Informes automáticos no soportados en el modo de acceso de Sólo lectura</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>Los Informes Automáticos serán guardados en el Disco Duro</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>Los Informes Automáticos necesitan un disco duro. Parece que no hay uno instalado.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Los informes automáticos no pueden habilitarse mientras funciona una secuencia de comandos automatizada.</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Creando Informe Automático</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>Preparando...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Creando </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Borrando informe anterior...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Hacer.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** INSUFICIENTE POTENCIA. DESACTIVE OTRO MÓDULO ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Conexión serie funcionando</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Revisando Aplicación para actualizaciones</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Aplicación lista para comunicaciones</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ERROR ACTUALIZACION KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Compruebe Seguridad Ethernet = Estándar y/o reinstale software BERT *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERROR ACTUALIZACION APLICACION.  Reinstalar módulo de software ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** ERROR EN LA ACTUALIZACION. ENERGIA INSUFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Error al iniciar: por favor, desactive el módulo BERT y vuelva a activar ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Ver</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Informes</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Herramientas</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Crear Informe...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Informe automático...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Iniciar medida</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Detener medida</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Personalizar...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Modo de acceso...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Poner Valores del Test por Defecto</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Borrar historial</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Lanzar Script...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>Emulación VT100</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Parámetros de modem...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Restaurar valores por defecto</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Ventana de resultados</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Único</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Dividir Dcha/Izda</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Dvidir Arriba/Abajo</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>Retícula 2 x 2</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>Ensamblar en la base</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>Ensamblar a la izquierda</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Mostrar sólo resultados</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>Estado medida</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Panel config</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Panel Acciones</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Escoja Script</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>Script (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Seleccionar&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Conexiones señal</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crear Informe</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Elegir&#xA;Contenidos</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crear</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ver informe después de la creación</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Error - El archivo no puede estar vacío.</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Elegir Contenidos para</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nombre del Fichero</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seleccionar...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ver informe después de la creación</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Incluye mensaje de registro</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Crear&#xA;Informe</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Ver&#xA;Informe</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Error - El archivo no puede estar vacío.</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>Informe guardado</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Por favor, atenúe la señal.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>El registro de eventos y los histogramas están llenos.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>Los registros de eventos K1/K2 están llenos.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>El registro está lleno.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>La prueba continuará sin registrar eventos&#xA;adicionales de este tipo.  Reiniciando la prueba&#xA;borrará todos los registros e histogramas.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Óptico&#xA;Reset</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Finalizar Medida</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Sobrecarga en receptor</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>Registro lleno</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Atención</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>No hay medida en curso</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Interfaz de usuario Look and Feel</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Guardar prueba</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Guardar prueba dual</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Cargar prueba</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Todos los archivos (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Pruebas guardadas (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Pruebas duales guardadas (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Cargar ajuste</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Cargar prueba dual</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Importar prueba guardada desde el USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Todos los archivos (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Configuraciones de prueba RFC clásicas guardadas (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Se guardaron las configuraciones de prueba RFC (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>Configuraciones de prueba FC guardadas (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Configuraciones de TrueSpeed guardadas (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Configuraciones TrueSpeed VNF guardadas (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Configuraciones guardadas de SAMComplete (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>Se guardó la configuración de SAMComplete (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Configuraciones de Comprobación de OTN guardadas (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Guardar configuraciones de autodiagnóstico de ópticas (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Configuraciones de comprobación de CPRI guardadas (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Configuraciones de comprobación de PTP guardadas (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>Se guardaron archivos zip (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Exportar prueba guardada al USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Todos los archivos (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Usuario guardó multi pruebas</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Test guardados de usuario</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>El control remoto está en uso.&#xA;&#xA;Esta operación no está permitida en el modo de acceso de sólo lectura.&#xA;Use el modo Herramientas->Modo de acceso para habilitar Acceso completo.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>Las opciones en el Módulo BERT han expirado.&#xA;Por favor, salga e inicie de nuevo el BERT desde la página del sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Ocultar</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Reiniciar ambas pruebas</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Reiniciar</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Herramientas:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Acciones</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Ventana de resultados maximizados para prueba: </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Vista total</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Añadir medida</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual de Usuario</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Ópticos recomendados</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Malla de frecuencia</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>¿Qué es?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Guardar configuración de prueba doble como ...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Config. de la prueba doble de carga ...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ver</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Cambiar la selección de la prueba ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Vaya a la vista de prueba total</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Informes</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Crear Informe de prueba doble ...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Ver Informe...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Ayuda</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Atención</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Guardando archivo</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Nuevo nombre:</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Introduzca un nuevo nombre de archivo</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renombrar</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>No se puede renombrar, puesto que existe un archivo con ese nombre.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Reiniciar</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Editar Info Usuario</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ninguno seleccionado...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Caracteres Máx: </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Seleccionar logo...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Vista previa no disponible.</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagramas de la ayuda</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual de Usuario</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Volver</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Adelante</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>Lanzamiento rápido</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERROR ACTUALIZACION JITTER. Reinstalar módulo de software ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ERROR FUNCIÓN JITTER OPTICO. ENERGIA INSUFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Función jitter optico en curso</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Perfiles (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Seleccionar perfiles</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Desactive todo</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Nota: Al cargar el perfil "Conectar" se conectará el canal de comunicaciones a la unidad remota si es necesario.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Borrar Todos</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Perfil incompatible</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Cargar&#xA;perfiles</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>¿Está seguro de querer eliminar %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>¿Está seguro que desea eliminar todos los perfiles %1?&#xA;&#xA;Esta operación no se puede deshacer.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Los archivos de solo lectura no serán eliminados.)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>No se pudieron cargar perfiles de:</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Cargados perfiles de:</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Algunas configuraciones no se cargaron correctamente porque no se encontraron en esta aplicación.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Se cargaron correctamente perfiles de:</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Habilitar Prueba doble</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Clasificar los aplicaciónes</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Validar las opciones</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>No hay pruebas disponibles para hardware u opciones instaladas.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>No fue posible iniciar ninguna prueba. Revise las opciones instaladas y la configuración de hardware actual.</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Restaurando aplicación previa a la desconexión</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Aplicación funcionando</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>No es posible montar la memoria USB interna.&#xA;&#xA;Por favor, asegúrese de que el dispositivo está insertado cerca de la batería de forma segura. Una vez insertado, por favor reinicie el módulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>El archivo interno de sistema de la memoria USB aparece como corrupto. Por favor, póngase en contacto con soporte técnico para asistirle.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>La capacidad de la memoria USB interna es menor que la recomendada de 1G. &#xA;&#xA;Por favor, asegúrese de que el dispositivo está insertado cerca de la batería de forma segura. Una vez insertado, por favor, reinicie la unidad.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Ocurrió un error al restaurar los ajustes.&#xA;Por favor intente de nuevo.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Todas las pruebas en funcionamiento se terminarán antes de cargar las pruebas archivadas.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Por favor ...&#xA;Cargando pruebas archivadas</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>El nombre del documento del test o ruta del del fichero&#xA;no válidos.  Utilice "Cargar Configuración"&#xA;para localizar el documento.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>Ocurrió un error durante el restablecimiento de una prueba.&#xA;Por favor, inténtelo de nuevo.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Cargador de bateria habilitado</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Nota cargador de bateria no estara habilitado en este modo. Caragador se habilitara automaticamente al selecionar las pruebas apropiadas.</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>Control Remoto en uso para esta prueba y&#xA;la pantalla ha sido desactivada.</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Mensajes registrados. Haga clic para ver ...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mensaje registro para %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>No hay mensajes.</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 mensaje</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 mensajes</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mensaje registro para %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Log de mensajes</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Parámetros de modem</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Seleccione dirección IP para el servidor y una dirección IP para el cliente remoto</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Servidor IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>Cliente IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Local presente (código de pais)</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>No se puede detectar un módem. Asegúrese que esta conectado y pruebe de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>Equipo esta ocupado. Desconecte todas sesiones y priebe de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>No de puede actualizar el módem. Desconecte todas las sesiónes y pruebe de nuevo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Reiniciando prueba(s) debido al cambio de horario.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Introduzca a continuación el nuevo código de la opción para instalarlo.</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Tecla de opción</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Instalar</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importar</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Póngase en contacto con Viavi para comprar las opciones del software.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>Opción Challenge ID:</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>¡Clave aceptada! Reinicie para activar la nueva opción.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Tecla rechazada - Las ranuras con opción de expiración están llenas</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Tecla rechazada - La opción de expiración ya se instaló.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Clave inválida - Por favor, inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>¡%1 de %2 Tecla(s) Aceptada(s)! Reinicie para activar la(s) nueva opción(es)</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>No se pudo abrir "%1" en la memoria USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>Hubo problemas para obener información de Opciones...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte Seleccionado</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Por defecto</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Leyenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Leyenda:</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI (no se usa)</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reservado</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>Acerca del Módulo BERT</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Módulo transporte</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importar una ficha rápida</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importar una ficha rápida</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> Ocultar menú</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Todas las pruebas</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizar</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Uno o más de las capturas de pantalla seleccionadas se lograron antes del inicio&#xA;de la prueba actual.  Asegúrese de haber seleccionado el archivo correcto.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Selecc&#xA;todo</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Desact&#xA;todo</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Selecc&#xA;captura pantalla</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Desact&#xA;captura pantalla</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibración de RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrando...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>La calibración no está completa.  Para poder utilizar RS-FEC es necesaria la calibración.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(La calibración puede ejecutarse desde la pestaña RS-FEC, en las páginas de configuración.)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Reintentar calibración</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Abandonar calibración</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Calibración incompleta</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>Normalmente, la aplicación RS-FEC se utiliza con dispositivos SR4, PSM4 y CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Para realizar una calibración de RS-FEC, efectúe el siguiente procedimiento (también aplicable a CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserte un adaptador QSFP28 en el CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserte un transceptor QSFP28 en el adaptador</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserte un dispositivo de bucle de fibra en el transceptor</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Haga clic en Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resincronizar</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Ahora debe resincronizar el transceptor al dispositivo en pruebas (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Desconecte el dispositivo de bucle de fibra del transceptor</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Establecer una conexión al dispositivo en pruebas (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Encienda el láser del DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verifique que el LED de Señal presente del CSAM esté en verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Haga clic en Resincronizar carril</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Resincronización completa.  El cuadro de diálogo se cerrará.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Guardar como sólo lectura</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>Lista de patillas a comprobar</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nombre del Fichero</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seleccionar...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Guardar como sólo lectura</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Guardar&#xA;perfiles</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 Perfiles (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Perfiles </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>es un archivo de sólo lectura. No puede sustituirse.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Perfiles guardados</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Seleccionar logo</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Ficheros Imagen (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>El archivo es demasiado largo. Por favor, seleccione otro archivo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte Seleccionado</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Por defecto</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Leyenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Comienzo&#xA;Prueba</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Detener&#xA;Prueba</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Acceso total</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Sólo lectura</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Oscuro</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Luz</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Inicio rápido</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Vista de resultados</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Throughput total TCP estimado (Mbps) basado en:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Ancho de banda máx disponible (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Retardo medio de trayecto (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Se necesitan un número de sesiones TCP paralelas para conseguir su capacidad máxima:</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Tasa Bit</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Tamaño ventana</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Sesiones</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Throughput total TCP estimado (kbps) basado en:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Ancho de banda máx disponible (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>Ha ocurrido un error mientras se guardaba la configuración del test.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>Ha ocurrido un error durante la carga de la configuración del test.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>El disco seleccionado está lleno.&#xA;Elimine algunos archivos e intente guardarlo de nuevo.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>Las siguientes categorías archivadas de resultados de cliente difieren de las cargadas actualmente:</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... otros %1</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>Sobreescribirlos puede afectar otras pruebas.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>¿Continúa sobreescribiendo?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Por favor espere...&#xA;Restaurando ajustes...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Recursos insuficientes para cargar la prueba %1 en este momento.&#xA;Vea en el menú "Prueba"  la lista de pruebas con la configuración actual.</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Imposible restaurar todas las configuraciones.&#xA;Ficheros antiguos o corruptos.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>El modo de acceso es de sólo lectura. Para cambiarlo, utilice Herramientas->Modo de Acceso</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>No hay medida en curso</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>Reiniciar todas las configuraciones por defecto.&#xA;&#xA;¿Continuar?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Cierra esta medida y reinicia con los&#xA;valores por defecto.&#xA;&#xA;¿Continuar?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Esta flujo de trabajo está actualmente en funcionamiento. ¿Desea finalizarlo y empezar uno nuevo?&#xA;&#xA;Haga clic en Cancelar para continuar con el funcionamiento del flujo de trabajo anterior.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>¿Desea terminar este flujo de trabajo?&#xA;&#xA;Haga clic en Cancelar para continuar en funcionamiento.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>Incapaz de lanzar VT100. Dispositivo serie ya en uso.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Puerto</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Módulo </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>La opción "Reiniziar" anulará los resultados de los dos puertos.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Escoja&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Cargar prueba...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Guardar prueba como...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Solo cargar ajustes...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Añadir medida</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Retirar Test</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Ayuda</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagramas de la ayuda</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Ver Informe...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Exportar informe...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Editar Info Usuario...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Importar Logo del Cliente...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importar desde USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Prueba guardada...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Categoría de cliente guardada ...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Exportar a USB</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Imprimir Pantalla...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Datos de tiempo...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Revisar/Instalar Opciones...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Captar imagen</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manual de Usuario</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Ópticos recomendados</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Malla de frecuencia</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Conexiones señal</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>¿Qué es?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Fichas rápidas</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Por favor Espere...&#xA;Retirando Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Puerto 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Puerto 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ninguno seleccionado...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Seleccionar fichero...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Importar captura de paquetes desde USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Seleccionar&#xA;Fichero</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Captura de paquetes guardada (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ver Informe</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Texto (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ver</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Ver este&#xA;informe</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Ver otros&#xA;informes</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>Cambiar nombre&#xA;del reporte</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> ya existe.&#xA;¿Quiere reemplazarlo?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Se cambió el nombre del reporte</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Error - El archivo no puede estar vacío.</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>Se guardó un informe como</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>No se guardaron informes.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vaya</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>Guardar Datos TIE...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Guardar como tipo: </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>Archivo HRD</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>Archivo CHRD</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Guardando </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>Esto podría llevar varios minutos...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Error: no se puede abrir el archivo HRD: Por favor, intente guardarlo otra vez.</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Cancelando...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>Datos TIE guardados.</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Error: Es posible que el archivo no se pueda guardar. Por favor, inténtelo de nuevo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Atención</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Cuando cierre el Análisis Wander se perderán todos los resultados de análisis.&#xA;Para continuar con el análisis haga clic en Continuar análisis.</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Cerrar Análisis</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Continuar Análisis</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Análisis de Wander</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Actualizar&#xA;Datos TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>Parar TIE&#xA;Actualizar</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Calcular&#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Parar&#xA;Cálculo</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Tomar&#xA;Captura de pantalla</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Carga de&#xA;datos TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>Detener&#xA;carga TIE</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Cerrar&#xA;Análisis</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Cargar datos TIE</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Todos los archivos desviado (*.chrd *.hrd);; archivos Hrd (*.hrd);; archivos Chrd (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Golpee dos veces para definir el rectángulo</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Borrar Todos</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Eliminar</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Cargar perfil</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>¿Está seguro de querer eliminar %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>¿Está seguro que desea eliminar todos los perfiles %1?&#xA;&#xA;Esta operación no se puede deshacer.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Los archivos de solo lectura no serán eliminados.)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 Perfiles (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>No se puede cargar el perfil.</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Error al cargar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Siguiente</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensaje</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Error</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vaya</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Atención</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>¿Qué desea hacer ahora?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>¿Está seguro que desea salir?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Restablecer configuraciones para la salida</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Salir a resultados</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Salida</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Volver</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Paso a paso:</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Siguiente</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Guíeme</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Otro puerto</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Comenzar de nuevo</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Ir a...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Otro puerto</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>La prueba está inicializando, por favor, espere ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>La prueba está terminando, por favor, espere ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Log de mensajes</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Principal</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Mostrar Etapas</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Respuesta: </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ninguno seleccionado...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo para el Informe</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Borrar</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Seleccionar logo...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Vista previa no disponible.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>La prueba está inicializando, por favor, espere ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>La prueba está terminando, por favor, espere ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>La prueba está en progreso...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Tiempo restante:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>No corriendo</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Prueba incompleta</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Prueba completa</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Desactivando Informes Automáticos antes de iniciar script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Activando Informes Automáticos antes de iniciar script.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Principal</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Se guardó la captura de pantalla</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Se guardó la captura de pantalla:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Selección de perfil</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Capa de operación</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Se cargó un perfil guardado</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>¿Cómo le gustaría configurar TrueSAM?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Cargar configuraciones desde un perfil guardado</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Vaya</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Empezar un nuevo perfil</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>¿En cual capa opera su servicio?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Capa 2: Prueba utilizando direcciones MAC, ej 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Capa 3: Prueba utilizando direcciones IP, ej 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Por favor, espere... yendo hacia el paso resaltado.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuración</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Seleccionar pruebas</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Establecer comunicaciones</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Configurar RFC 2544 Mejorado</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Configurar SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Configurar J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Configurar TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Guardar configuración</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Agregar información del informe</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>Ejecutar pruebas seleccionadas</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ver Informe</translation>
        </message>
    </context>
</TS>
