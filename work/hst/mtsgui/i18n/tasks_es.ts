<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscopio</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Pruebas</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>Medidor de potencia</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>Sistema</translation>
        </message>
    </context>
</TS>
