<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi Reproductor de Video </translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Abandonar</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>Nombre de Archivo de Video</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>Hora:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>Estado Video</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>escale y recorte </translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>Abrir &amp;Archivo...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>Relación de aspecto</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>Escala</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>Modo de escala</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>Fit in view</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>escale y recorte </translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>Abrir Archivo...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Abierto</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>No Open Media</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>Cargando...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>Almacenamiento temporal...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>Sonando...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>Pausa</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>Error...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>Idle - Stopping Media</translation>
        </message>
    </context>
</TS>
