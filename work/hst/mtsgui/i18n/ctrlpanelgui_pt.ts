<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Capacidade do aplicativo móvel</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth e Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Apenas Wi-Fi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Por favor, reinicie o equipamente para que as mudanças tenham efeito</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Colar</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copiar</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Cortar</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Deletar</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Único</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Múltiplo</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aberto</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renomear</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Nova Pasta</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Disco</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>Colar irá sobrescrever os arquivos existentes.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Sobrescrever o arquivo</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>Não conseguimos abrir o arquivo devido a um erro desconhecido.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>Não é possível abrir o arquivo.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>O arquivo selecionado será permanentemente deletado</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>%1 dos arquivos selecionados serão permanentemente deletados</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Eliminar arquivos</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Por favor, insira o nome da pasta:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Criar pasta</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>Não foi possível criar a pasta "%1".</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>A pasta "%1" já existe.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Criar pasta</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Colando...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Deletando...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Completado.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>falha %1.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Falha ao colar arquivo</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>Falha ao colar %1 dos arquivos</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Não há espaço livre suficiente.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Falha ao deletar o arquivo</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>Falha ao deletar %1 dos arquivos</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Volta</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opção</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>Atualizar URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome do arquivo</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Ação</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>Módulo</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Outra rede wireless</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Entre com a informação de rede wireless  abaixo.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Nome (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Tipo de codificação:</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Melhoramento (upgrade) disponível.</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync determinou que disponibilidade de melhora(upgrade). &#xA;Gostaria de fazer agora? &#xA;&#xA;Fazer  melhoramento (upgrade) irá parar quaisquer testes que estejam funcionando. Ao final do processo, o teste reinicializará para completar  melhoramento (upgrade).</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Um erro desconhecido foi detectado. Por favor, tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Falha ao atualizar</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Informação do instrumento:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Opções de base:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Reiniciar o instrumento com valores pré-determinados</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Exportar logs&#xA;para memória USB</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Copiar informação do sistema no arquivo</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>A informação do sistema foi copiada para este arquivo:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Isto exige um reinício e resetará as configurações do sistema e de teste para os valores pré-determinados</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Reiniciar e restaurar</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Log exportado com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Falha ao exportar logs. Por favor, verifique se há uma memória USB inserida corretamente e tente novamente.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 Versão %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Começar de novo</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Reinicializar para o padrão</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Selecione seu método de atualização</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Atualizar arquivos armanezados em um dispositivo USB</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rede</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Download da atualização de um servidor web através da rede</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Preencha o endereço do servidor da atualização:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Endereço do servidor</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Insira o endereço do servidor proxy, se necessário, para acessar o servidor de atualização.</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Endereço proxy</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Pergunte ao servidor por atualizações disponíveis</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Prévio</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Seguinte</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Iniciar a atualização</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Iniciar Downgrade</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Não se encontraram atualizações</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Falha ao atualizar</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>A atualização fechar m todos os testes em execução. No final do processo, o instrumento irá reiniciar automaticament para completar a atualização</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Confirmação de atualização</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>O downgrade do conjunto de testes é possível, mas não recomendado. Se você precisar fazer isso, recomenda-se chamar Viavi TAC.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>O downgrade encerrará qualquer teste em execução. No final do processo, o conjunto de teste irá reinicializar para concluir o downgrade.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>Downgrade não recomendado</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Um erro desconhecido foi detectado. Por favor, tente novamente.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Sistema</translation>
        </message>
    </context>
</TS>
