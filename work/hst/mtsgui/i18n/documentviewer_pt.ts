<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Saída</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>Carregando:</translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>Falha ao carregar o PDF</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>Falha ao mostrar a página:</translation>
        </message>
    </context>
</TS>
