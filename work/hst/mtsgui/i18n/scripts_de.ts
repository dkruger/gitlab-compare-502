<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>{1} Byte Rahmen:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>{1} Byte Rahmen</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>{1} Byte Pakete:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>{1} Byte Pakete</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Fehler: Beim Herunterladen von {2} wurde das Zeitlimit überschritten; Verbindung prüfen und erneut versuchen.</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} Rahmenburst: fehl</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} Rahmenburst: pass</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} von {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} Paketburst: fehl</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} Paketburst: pass</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} lädt {2} herunter...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   Möchten Sie das Objekt ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Drücken Sie auf 'OK', um den Vorgang zu wiederholen</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} VLAN-ID {2} für {3} wird getestet...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Warten...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       Sie können den Namen ändern, um eine neue Konfiguration zu erzeugen.</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 Top Talkers(von {1} IP-basierten Gesprächen insgesamt)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 Top TCP-Retransmissions (von {1} Gesprächen insgesamt)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Test abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Aktiv</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Aktive Schleife</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>Aktive Schleife nicht erfolgreich. Test für VLAN ID abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Tatsächlicher Test</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Bereich hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Eine Rahmenverlustrate, die den konfigurierten Rahmenverlustschwellwert überschreitet</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>Nach der Durchführung einer manuellen Prüfung und bei Bedarf können Sie</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Es wurde eine Hardwareschleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>Es konnte keine Hardwareschleife gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Alle Tests</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Loopback-Anwendungen sind nicht kompatibel</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Es ist keine maximale Durchsatzmessung verfügbar</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>Eine aktive Schleife konnte nicht gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analysieren</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analyse läuft</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>und</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>und RFC 2544-Test</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>Eine LBM/LBR Schleife wurde gefunden.</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>Eine LBM/LBR Schleife wurde gefunden.</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Es wurde keine permanente Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Fortschrittslog  wird am Ende des Reports  angehängt</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Anwendungsname</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Ungefähre Gesamtzeit:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Eine Reihe von theoretischen FTP Durchsatzergebnissen wird berechnet basierend auf Meßungen am Link.  Bitte geben Sie die Link Bandbreite, die RTD und die Enkapsolierung an. </translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Beim Warten auf eine Antwort ist ein Timeout-Fehler aufgetreten. &#xA;Auf den letzten Befehl erfolgte &#xA;innerhalb von {1} Sekunden keine Antwort.</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> Eine physikalische Schleife wird angenommen.        </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>Bei der asymmetrischen Kommunikation werden Daten vom lokalen Ende an das entfernte Ende (Upstream-Modus) bzw. vom entfernten Ende an das lokale Ende (Downstream-Modus) übertragen. Im kombinierten Modus wird die Prüfung zweimal durchgeführt, wobei die Daten sequentiell unter Verwendung der lokalen Konfiguration im Upstream-Modus und anschließend unter Verwendung der Remotekonfiguration im Downstream-Modus übertragen werden. Verwenden Sie die Schaltfläche, um die Remotekonfiguration mit der aktuellen lokalen Konfiguration zu überschreiben.</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Versuche</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Versuche eine Schleife zu schalten</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Versuch, am Server anzumelden</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>Schleifenbildung fehlgeschlagen. Der Test wird gestoppt.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>Der Versuch,eine Schleife zu schalten war nicht erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto Negotiation</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Auto Negotiation fertig</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Einstellungen Auto Negotiation</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Status Auto Negotiation</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Durchschnitt</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Durchschnitt Burst</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Mittlere Paketrate</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Mittlere Paketgröße</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>I.D.</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Durschschn. und max  Resultate für Durchschn. Paketjitter</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Durchschnittliche Latenzzeit (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Durchschnittliche Latenzzeit (RTD): Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Durchschnitt. Paket-Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Durchschn. Paketjitter: nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Mittl. Pak.-jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>I.D. Rate</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Back to Back Rahmen Granularität:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Back to Back Rahmen Granularität</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Back to Back Rahmen Test</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Back to Back Rahmen Test Ergebnis:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Back to Back Max Versuchszeit:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Back to Back Max Versuchszeit</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Back to Back Test Ergebnis:</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>Zurück zur Übersicht</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Bandbreitengranularität (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Bandbreitengranularität (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Bandbreitenmessgenauigkeit:</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Bandbreitenmessgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>allg. Last-Test</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Beginning of range:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Beide</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx und Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>Es ist weder die lokale, noch die Remotequell-IP-Adresse verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Aufsteigend</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Puffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Buffer Credit&#xA;(Vorraussetzung&#xA;Durchsatztest)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Buffer Credits Dauer je Versuch:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Buffer Credits Dauer je Versuch</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Buffer Credit Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Buffer Credit Test Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Buffer Credit Durchsatz {1} Bytes:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Buffer Credit Durchsatz&#xA;(Vorraussetzung&#xA;Buffer Credit)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Buffer Credit Durchsatz Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Buffer Credit Durchsatz Test Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Buffergröße</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Burst Granularität (Rahmen)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>Durch eine Analyse der B34 und der Netzwerkauslastung über die Zeit kann ein Zusammenhang zwischen einer geringen Netzwerkleistung und Netzwerkeigenschaften, die zu Datenverlust führen (überlastete Engpässe bei der Übertragung) hergestellt werden.B137</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>Durch eine Analyse der Tabelle der IP-basierten Gespräche können die  Top Talker anhand der übertragenen Bytes oder Rahmen ermittelt werden.  Die beiden Richtungen "Q &lt;- Z" und "Q -> Z" bezeichnen die Übertragung von Quelle nach Ziel bzw. von Ziel nach Quelle.</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(Byte)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation>Bytes</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(Byte)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Bytes&#xA;Q &lt;- Z</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Bytes&#xA;Q -> Z</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Berechnete&#xA;Rahmenlänge</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Berechnete&#xA;Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Berechnung wird ausgeführt...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>Der Vorgang kann nicht fortgesetzt werden!</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Übersicht der Aufzeichnungsanalyse</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Erfassungsdauer</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Erfassen&#xA;Bildschirm</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>ACHTUNG!&#xA;&#xA;Sind Sie sicher dass Sie dauerhaft&#xA;diese Konfiguration löschen möchten?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Konf. Rate (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Konf. Rate</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Konf. Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>Fahrgestellnummer</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>Bei der Konfiguration der Filterquellen werden die aktivierten Empfangsobjekte berücksichtigt.</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>Bei der Konfiguration der Übertragungsziele werden die aktivierten Übertragungsobjekte berücksichtigt.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Aktive Schleife wird überprüft</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Überprüfung auf Hardwareschleifen wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Überprüfung auf aktive Schleifen wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Überprüfung auf eine LBM/LBR Schleife.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Überprüfung auf permanente Schleifen wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Erkennung der Halbduplexanschlüsse wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Überprüfung auf ICMP-Rahmen wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Überprüfung auf Retransmissions und hohe Netzwerkauslastung wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Suche nach Hardware Schleife</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Überprüfung LBM/LBR Schleife</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Permanente Schleife wird überprüft</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Statistiken zur Protokollhierarchie werden überprüft</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Überprüfung der Verfügbarkeit der Quelladressen wird durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>Wenn dieses Kontrollkästchen aktiviert ist, wird die Prüfkonfiguration nach Abschluss der Prüfungen auf die ursprünglichen Einstellungen zurückgesetzt. Bei asymmetrischen Prüfungen wird sowohl die lokale als auch die Remotekonfiguration wiederhergestellt. Mit dem Zurücksetzen der Konfiguration wird auch die Verbindung zurückgesetzt.</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Überprüfen Sie die Anschlusseinstellungen zwischen der IP-Adresse des Quellgeräts und dem angeschlossenen Gerät. Stellen Sie sicher, dass die Option für Halbduplex nicht aktiviert ist.  Die Zerlegung der Teilstrecken kann auch erfolgen, indem das Analysegerät an die IP-Adresse des Zielgeräts angenähert wird. Wenn bei einer weiteren Aufteilung in Teilstrecken die Retransmissions verringert werden, können auf diese Weise fehlerhafte Verbindungsabschnitte ermittelt werden.</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Wählen Sie eine Aufzeichnungsdatei zur Analyse aus</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>PCAP-Datei wählen</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Bitte Genauigkeit der Bandbreitenmeßung wählen&#xA;( 1% wird empfohlen um kurze Testzeiten zur erzielen)</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Wählen sie den Flow Control Login Typ</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Bitte Rahmen- oder Paketgröße wählen</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Wählen Sie die Granularität aus, mit der Sie den Back-To-Back-Test ausführen möchten.</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Bitte Granularität wählen in der der Rahmenverlusttest durchgeführt werden soll.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Wählen Sie die maximale Bandbreite, für die die Leitung konfiguriert ist. Das Gerät verwendet diese Angabe als maximale Bandbreite für Übertragungen, was die Testdauer verkürzt:</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Wählen Sie die maximale Versuchszeit für den Back-To-Back-Test aus.</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Wählen Sie die minimalen und maximalen Lastwerte für das 'Absteigend'- oder 'Aufsteigend'-Testverfahren aus.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Bitte wählen Sie die Anzahl der Versuche, die der Back to Back Test je Rahmengröße durchgeführt werden soll.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Wählen Sie die Anzahl der Versuche je Latenzmeßung (RTD) pro Rahmengröße.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Wählen die Anzahl der Versuche die der Paketjittertest je Rahmenlänge durchgeführt werden soll</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Bitte wählen Sie die Toleranz in % für verlorenene Rahmen während des Durchsatztest.&#xA;Bitte beachten Sie das eine Einstellung >0.0&#xA;NICHT den RFC-2544 Standard entspricht.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Wählen Sie die Dauer je Versuch einer Latenzmeßung (RTD).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Geben Sie die Zeitdauer für jeden Paket-Jitter-Versuch an.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Wählen Sie die Zeit aus, in der eine Rate fehlerfrei gesendet werden muss, um den Durchsatztest zu bestehen.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Wählen Sie die Zeit aus, die jeder Frameverlustversuch dauern soll.</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Wählen Sie die Dauer je Buffer Credit Test</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Wählen Sie das Verfahren für den Rahmenverlusttest aus.&#xA;HINWEIS: Das RFC2544-Verfahren beginnt mit der max. Bandbreite, wird bei jedem Test um die Bandbreitengranularität verringert und endet, wenn in zwei aufeinander folgenden Tests keine Rahmen verloren gehen.</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Cisco Discovery Protokoll</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>Im Netzwerk wurden CDP-Nachrichten (Cisco Discovery Protocol-Nachrichten) erkannt. In der Tabelle sind die MAC-Adressen mit den zugehörigen Anschlüssen aufgelistet, die bei der Aushandlung Halbduplexeinstellungen auswiesen.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Klicken Sie auf die Schaltfläche "Ergebnisse", um zur Standardbenutzeroberfläche zurückzukehren.</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Codeverletzungen</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Kombiniert</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation> Kommentare</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Kommentare</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Kommunikation mit Gegenstelle erfolgreich aufgebaut</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>Kommunikation mit Gegenstelle kann nicht aufgebaut werden</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>Die Verbindung zum Remoteende wurde unterbrochen</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>beendet</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>Abgeschlossen&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Konfig.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfiguration</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation> Name der Konfig.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Name der Konfiguration:</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Name der Konfig.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>Name für Konfiguration wird benötigt</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Konfiguration kann nur gelesen werden</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Überblick Konfiguration </translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Ausgewählte Objekte konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Dauer, wie lange {1} den Traffic sendet, konfigurieren.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Bitte bestätigen Sie das Ersetzen der Konfiguratution</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Bitte Löschung bestätigen</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Verbunden</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Verbinde mit der Testapplikation</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Im Halbduplexmodus fortsetzen</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Prüfung wird im Halbduplexmodus fortgesetzt</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Lokale Konfiguration in&#xA;Remotekonfiguration kopieren</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Auswahl&#xA;kopieren</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>Es konnte Schleife am fernen Ende geschalten werden</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>credits</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(credits)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA;Aktuelles Skript: {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Aktuelle Auswahl</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation> Kunde</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Kunde</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Datenbitrate</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Datenbyterate</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Daten Layer angehalten</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Datenmodus</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Datenmodus auf PPPoE festgelegt</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Datengröße</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation> Datum</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>Datum &amp; Zeit</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>Tage</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Verzögerung, akt (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Aktuelle Verzögerung (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Zieladresse</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Konfiguration am Ziel</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>Ziel-ID</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>Ziel&#xA;IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>Die Ziel-MAC-Adresse für IP-Adresse {1} konnte nicht gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>Ziel MAC gefunden</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>Ziel-MAC-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>Detailbeschriftung</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Details</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>erkannt</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>entdeckt</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Erkannte Verbindungsbandbreite</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Mehr Rahmen erkannt als gesendet für {1} Bandbreite - Ungültiger Test.</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Symmetrischer Durchsatz wird ermittelt</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Gerätedetails</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>Gerätenummer</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>Keine DHCP-Parameter verfügbar</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>DHCP Paramater gefunden</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Erkannte Geräte</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Geräte werden erkannt</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Entdeckung Fernnebenschleife Typ...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Erkennung&#xA;zurzeit&#xA;nicht&#xA;verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Anzeige nach:</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Downstream-Richtung</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> Soll der Vorgang fortgesetzt werden? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Dauer</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Erweiterten Traffic-Test für Layer 2 aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Verkapselung:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Verkapselung</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Ende</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>End of range:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Bitte IP Adresse oder Servernamen des FTP Servers eingeben</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Anmeldenamen für den Server eingeben, mit dem eine Verbindung aufgebaut werden soll</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Bitte korrespondierendes Passwort eingeben</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Bitte neuen Konfigurationsnamen eingeben&#xA;(Bitte nur Buchstaben, Zahlen, Leerzeichen, Bindestriche und Unterstriche verwenden):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Error: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>FEHLER: Beim Warten auf eine Antwort ist ein Timeout-Fehler aufgetreten. &#xA;Keine Antwort innerhalb von</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Fehler: es konnte keine Verbindung aufgebaut werden</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Fehleranzahl</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Fehlerhafte Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Fehler beim Laden der PCAP-Datei</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Fehler: Namensauflösung durch primären DNS fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Fehler: Site nicht lokalisierbar</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Verbleibende geschätzte Zeit</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Geschaetzte verbleibende Zeit</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     Ethernet-Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>Ereignisprotokoll ist voll.</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Erhebliche Anzahl von Retransmissions gefunden</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>J-QuickCheck beenden</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Erwarteter Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>Erwarteter Durchsatz ist</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>Erwarteter Durchsatz ist nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>Schaltfläche "Expert- RFC 2544".</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explizit (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Explizit (Fabric/Knotenport)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Explicit login was unable to complete. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FEHLER</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>FEHLGESCHLAGEN</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>Gegenstelle ist ein JDSU Smart Class Ethernet Testgerät</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>Gegenstelle ist ein Viavi Smart Class Ethernet-Messgerät</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>FC Test wird mit Acterna Test Payload ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Testbericht</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX-fähig</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Fibre Channel Testbericht</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>File Konfiguration </translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dateiname</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Dateien</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>File Größe</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>File Größe:</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>File Größe</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>File Größe: {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>File Größen:</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>File Größen</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>Die Datei wurde zu schnell übertragen. Test vorzeitig beendet.</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Erwarteter Durchsatz wird ermittelt</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Top Talker werden ermittelt</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>Erste 50 Halbduplexanschlüsse (von {1} Anschlüssen insgesamt)</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>Erste 50 ICMP-Nachrichten (von {1} Nachrichten insgesamt)</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Flusskontrolle</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flusskontrolle-Anmeldetyp</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Ordner</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> minimale Anzahl Buffer Credits halbiert, um die doppelte Glasfaserlänge auszugleichen.</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>gefunden</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Aktive Schleife gefunden.</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Hardware-Schleife gefunden.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Rahmenlänge</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Rahmenlänge</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Rahmenlänge (Byte)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Rahmenlängen:</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Rahmenlängen</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Zu testende Rahmenlängen</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Rahmenverlust (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Rahmenverlust {1} Bytes:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Rahmenverlust {1} Bytes</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Genauigkeit Rahmenverlustsbandbr.:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Genauigkeit Rahmenverlustsbandbr.</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Rahmenverlust Maximale Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Rahmenverlust Minimale Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Rahmenverlustrate</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Rahmenverlustrate</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Rahmenverlusttest</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Rahmenverlust Testverfahren:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Rahmenverlust Testverfahren</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Rahmenverlust Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Rahmenverlusttoleranz (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Rahmenverlusttest Dauer je Versuch:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Rahmenverlusttest Dauer je Versuch</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Rahmen oder Paket</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>Rahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Rahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Rahmengröße:  {1} Bytes</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Rahmen&#xA;Q &lt;- Z</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Rahmen&#xA;Q -> Z</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Rahmung</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(rhmn)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(rhmn/sek)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>FTP Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>FTP Durchsatz Test&#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>FTP Durchsatztest fertig</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>FTP Durchsatz Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Voll</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>PCAP-Info abrufen</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Halb</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Halbduplex-Ports</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Hardwareschleife</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>("Hardware" &#xA;oder "Aktiv")</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>(Hardware,&#xA;Permanent&#xA;oder Aktiv)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX-fähig</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Hohe Auslastung</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Pos1</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>Stunden</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>HTTP Durchsatz Test</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>HTTP Durchsatz Testbericht</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>ICMP- &#xA;Code</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>ICMP-Nachrichten</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Wenn die Fehlerzähler sporadisch heraufzählen, führen Sie einen manuellen Test durch</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Wenn das Problem bestehen bleibt, "Test auf Standardeinstellungen zurücksetzen" im Menü "Werkzeuge" ausführen.</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Wenn sich das Problem der sporadischen Fehler nicht lösen lässt, können Sie folgende Einstellung verwenden</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implizit (transparenter Link)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>Informationen</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Kommunikation einleiten mit</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Zum Ermitteln der Bandbreite, bei der</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>Eingangsraten für lokalen und fernen Standort stimmen nicht überein</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Es wurden periodische Leitungsfehler ermittelt.</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Interner Fehler</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Interner Fehler - PPPoE Neustart</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Ungültige Konfig</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Ungültige IP Konfig</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>IP-Adressen</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>IP-Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>wird beendet</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>wird gestartet</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Jitter-Prüfung</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck ist abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck: Verbindungsverlust oder Verbindung konnte nicht hergestellt werden</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbit/s</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Entfernen</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>L2-Traffic-Test kann erneut gestartet werden, indem der J-QuickCheck erneut ausgeführt wird.</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Laufzeit (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>RTD-Latenz- und Paket-Jitter-Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Latenz (RTD, durchschn.): Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Erfolgskrit. Verzögerungsmeßung (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Erfolgskrit. Verzögerungsmeßung</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Verzögerung (RTD)&#xA;(Vorraussetzung&#xA;Durchsatztest)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Latenzergebnisse (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Laufzeittest (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Resultate Laufzeitmeßung (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Verzögerung (RTD) Test Resultat: ABGEBROCHEN</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Verzögerung (RTD) Test Resultat: FEHL</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Verzögerung (RTD) Test Resultat: ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Latenzprüfungsergebnisse (RTD) übersprungen</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Latenzprüfung (RTD) übersprungen</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> Erfolgskriterium Laufzeitmeßung (RTD): {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Erfolgskrit. Verzögerung (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Versuchsdauer je Laufzeitmeßung:</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Versuchsdauer je Laufzeitmeßung</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Laufzeit (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Layer 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Schicht-1/2 &#xA;Ethernet-Status</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Layer 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Layer 2 Verbindung vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Layer 2 Schnelltest</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Layer 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>Schicht-3 &#xA;IP-Status</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Layer 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>Schicht-4 &#xA;TCP-Status</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR-Schleife</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Länge</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Link gefunden</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Linkebene Entdeckungsprotokoll</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Verbindungsverlust</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Leitungsgeschwindigkeit in Aufzeichnungsdatei erkannt</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Empfangsport</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Lastformat</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>LADEN ... Bitte warten</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Lokal</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>Lokale Ziel-IP-Adresse ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>Lokale Ziel-MAC-Adresse ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>Lokaler Zielanschluss ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>Lokaler Schleifentyp ist konfiguriert auf Unicast</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Lokaler Port</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>Lokale Remote-IP-Adresse ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> Seriennummer (lokal)</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Lokale Konfiguration</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> Softwareversion (lokal)</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>Lokaler Quell-IP-Filter ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>Lokaler Quell-MAC-Filter ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>Lokaler Quell-Port-Filter ist konfiguriert auf</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Lokale Übersicht</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> Gerätname (lokal)</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Überprüfen Sie diejenigen Geräte, deren Quell-MAC-Adressen und -ports in der Tabelle aufgelistet sind, darauf, dass für die Duplexeinstellungen die Option "voll" anstelle von "auto" festgelegt ist.  Häufig wird für den Host bzw. das Netzwerkgerät die Option "auto" eingestellt, und die Verbindung wird dennoch fälschlicherweise im Halbduplexmodus hergestellt.</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation> Standort</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Standort</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Anmeldung:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Login</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Login-Name:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Login-Name</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Fehler bei Schleife</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Entfernen der Scheife an der Gegenstelle...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Erstellen der Schleife an der Gegenstelle...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Schleifenstatus unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Fehler bei Schleifenschaltung</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Schleife am fernen Ende erfolgreich hergestellt</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Erstellen der Schleife erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>Datenverlust in Schicht-2-Verbindung erkannt!</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Verloren</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Verlorene Rahmen</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Managementadresse</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>MAU-Typ</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( max. {1} Zeichen )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Max I.D.</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Max. durchschnitt. Paket-Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Max durchschn. Paketjitter: nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Max. mittl. Pak.-jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Max. Bandbreite (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Max. Bandbreite (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Max. Puffergröße</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>maximal zugelassene Durchschnitslaufzeit für bestandene Laufzeitmeßung (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Max. Paketjitter, zulässiger Durchschnitt für "Erfolg" bei Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Maximale RX Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Max Testbandbreite:</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Max Testbandbreite</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Maximal gemessener Durchsatz:</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Maximale Zeitbegrenzung von {1} pro VLAN-ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Maximale Zeitbegrenzung von 7 Tagen pro VLAN-ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Max Zeit je Versuch (Sekunden)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Maximale TX Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>Max. Rate</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Die maximale Anzahl an Rückübertragungsversuchen erreicht. Test vorzeitig abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>gemessene</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>gemessene Rate (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>gemessene Rate</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>gemessene Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Durchsatzmeßung bei {1} Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Nachricht</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimum</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Min Bandbreite (%)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>min. erlaubte Bandbreite zum Bestehen des Durchsatztests:</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Mindestzeitlimit von 5 Sekunden pro VLAN-ID</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Min. Rate</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>Protokoll(e=</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>Minuten</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modell</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Verändern</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>MPLS / VPLS Enkapsulierung derzeit nicht unterstützt ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>nicht anwendbar (Schleifenbetrieb)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Netzwerk Aktiv</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Netzauslastung</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>Bei der Netzwerkanalyse wurde keine übermäßige Nutzung feststellt, genaue Zahlen können Sie diesem Netzauslastungsdiagramm entnehmen.</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>Bei der Netzwerkanalyse wurde keine übermäßige Nutzung feststellt, Detailinformationen zu den IP-basierten Hauptteilnehmern können Sie dieser Tabelle entnehmen.</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Neu</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Neuer Konfiguartionsname</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Neue URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Weiter</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nein.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>Keine kompatible Anwendung gefunden</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt;KEINE KONFIGURATION VERFÜGBAR></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>Keine Files für test ausgewählt</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>Es konnte keine Hardwareschleife gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>Kein&#xA;JDSU&#xA;Gerät&#xA;gefunden</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>Keine Schicht-2-Verbindung erkannt!</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>Es konnte keine Schleife hergestellt werden</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>Es konnte keine Schleife hergestellt bzw. gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>Es konnte keine permanente Schleife gefunden werden</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>Keine aktive Anwendung erkannt</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>NICHT den RFC-2544 Standard entspricht.</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Nicht verbunden</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Nicht ermittelt</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>Bitte beachten Sie, dass eine Einstellung > 0.00</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Hinweis: Es wird eine physikalische Schleife mit weniger als 2 Buffer Credits angenommen.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;Hinweis: Setzt eine Schleife mit Buffer Credits unter 2 voraus.&#xA; Test ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Hinweis: Unter Annahme einer physikalischen Schleife wird die für jeden Rahmen berechnete</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Hinweis: Unter Annahme einer physikalischen Schleife werden bei jedem Schritt bei doppelt so vielen</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Hinweis: Sobald ein Wert für die Rahmenverlusttoleranz festgelegt worden ist, entspricht der Test nicht</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Hinweis</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Nicht ausgewählt</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Keine&#xA;Viavi-&#xA;Geräte&#xA;erkannt</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>Wird jetzt beendet...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>Verifiziere nun</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Verifikation mit  {1} credits.  Dies wird {2} Sekunden dauern.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Anzahl der Back to Back Versuche:</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Anzahl der Back to Back Versuche</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Number of Failures</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Number of IDs tested</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Versuchsanzahl Laufzeitmeßung (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Versuchsanzahl Laufzeitmeßung</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Versuchsanzahl Paketjittertest:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Versuchsanzahl Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Anzahl Pakete</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Number of Successes</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Anzahl der Versuche:</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Anzahl der Versuche</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>von</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Aus</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>AUS</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>der Rahmen gingen innerhalb einer Sekunde verloren.</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>der Durchlauf, der von J-QuickCheck erwartet wird</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(% der Line-Rate)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% der Line-Rate</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>der Line-Rate</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>EIN</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * Nur {1} Versuch(e) ergab(en) nutzbare Daten *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(EIN oder AUS)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Originator ID</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Außerhalb des Bereichs</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        Gesamtergebnis: {1}       </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>Gesamtergebnis: ABGEBROCHEN</translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Bereich überschritten</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>über die letzten 10 Sekunden, auch wenn die Datenübertragung angehalten sein sollte</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paket</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Paket-Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Paketjitter, dur</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Grenze für erfolgr. Paketjittertest:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Grenze für erfolgr. Paketjittertst</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Paket-Jitter&#xA;(Vorraussetzung&#xA;Durchsatztest)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Paket-Jitter-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Paketjittertest Resultate: ABGEBROCHEN</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Paketjittertest Resultate: FEHL</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Paketjittertest Resultate: ERFOLG</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> Erfolgskriterium Paketjitter: {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Grenzwert Paketjitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Dauer je Paketjittertest:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Dauer je Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Paket&#xA;länge</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Paketlänge (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Paketlänge:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Zu testende Paketlängen</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>Paketgröße</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Paketgröße</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Erfolg / Fehl</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>ERFOLG/FEHL</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Rate für "Gut" (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Rate für "Gut" (Frames/s)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Rate für "Gut" (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Rate für "Gut" (Pakete/s)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Passwort:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Passwort</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Pause</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Pausenfähig</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Pause erk.</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Pauserahmen erkannt</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Pauserahmen erkannt - Test ungültig</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Fehler beim Parsen der PCAP-Datei</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>PCAP-Dateien</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>Ausstehend</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Performing cleanup</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Dauerhaft</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Permanente Schleife</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>(Permanent&#xA;oder Aktiv)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pkt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Pak.-jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pkt Vrlst</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(pkt)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(pkt/sek)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Plattform</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Bitte überprüfen Sie, ob die Synchronisierung durchgeführt und die Verbindung hergestellt wurde,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Bitte überprüfen Sie, ob die Verbindung ordnungsgemäß hergestellt wurde,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Bitte überprüfen Sie, ob die Verbindung ordnungsgemäß hergestellt wurde,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Wählen Sie einen anderen Konfigurationsnamen aus.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Geben Sie einen Dateinamen ein, um den Bericht zu speichern (max. %{1} Zeichen) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Bitte eventuelle Kommentare eingeben (max. {1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Bitte eventuelle Kommentare eingeben (max. %{1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Kommentare, die Sie haben, ein (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Binde- und Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Bitte den Namen des Kunden eingeben (max. %{1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Bitte den Namen des Kunden eingeben (max. %{1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Ihren Kundennamen ein (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Binde- und Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Ihren Kundennamen ein (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Binde- und Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Ihren Kundennamen bei (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Binde- und Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Bitte den Technikernamen eingeben (max. {1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Bitte den Technikernamen eingeben (max. %{1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Ihren Technikernamen ein (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Binde- und Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Bitte den Testort eingeben (max. {1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Bitte den Testort eingeben (max. %{1} Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Fügen Sie Ihre Test-Lokalisierung ein (max. {1} Sonderzeichen)&#xA;(Benutzen Sie nur Buchstaben, Zahlen, Zwischenräume sowie Unterstriche)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Bitte den "Remotegerät Verbinden" Knopf drücken</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Bitte überprüfen Sie die Verbindungsperformanz durch eine manuelle Prüfung des Datenaufkommens.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Überprüfen Sie die lokale und die Remotequell-IP-Adresse und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Überprüfen Sie die lokale Quell-IP-Adresse und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>Überprüfen Sie Ihre ferne IP-Adresse und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Überprüfen Sie die Remotequell-IP-Adresse und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Bitte warten ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Bitte warten...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Bitte warten, während die PDF-Datei geschrieben wird.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Bitte warten bis die PDF Datei gespeichert ist.&#xA;Dies kann bis zu 90 Sekunden dauern ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Port:</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Port {1}: Möchten Sie einen Testbericht speichern?&#xA;&#xA;"Ja" oder "Nein" drücken.</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>Port-ID</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Anschluss:				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Anschluss:				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Aktiv</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP Bestätigung Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP Unbekannt Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Aktiv</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE inaktiv</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE gestartet</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>PPPoE Status: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE Timeout</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE Up</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPoE-Fehler</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP Timeout</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP Unbekannt fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP Einwahl fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>"Schliessen" drücken um zum Hauptbildschirm zurückzukehren</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>"Beenden" drücken, um zum Hauptbildschirm zurückzukehren, oder zur Wiederholung "Test durchführen" drücken.</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Klicken Sie auf&#xA;"Aktualisieren",&#xA;um die&#xA;Erkennung&#xA;durchzuführen</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Klicken Sie auf die Schaltfläche "J-QuickCheck beenden", um den J-QuickCheck zu beenden. </translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Klicken Sie auf die Schaltfläche "Aktualisieren" unten, um die Viavi-Geräte im aktuellen Subnetz zu erkennen. Wählen Sie ein Gerät aus, um Detailinformationen zu dem Gerät in der Tabelle im rechten Ausschnitt anzuzeigen. Wenn die Schaltfläche "Aktualisieren" nicht verfügbar ist, überprüfen Sie, ob die Synchronisierung durchgeführt und die Verbindung hergestellt wurde.</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Klicken Sie auf die Schaltfläche &#xA;"J-QuickCheck ausführen", um den J-QuickCheck auszuführen. </translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Vorhergehende</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>Stand</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Eigenschaft</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Vorschläge für nächste Schritte</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Provider</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Beenden</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Zufall</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> Bereich</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Bereich</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Rate</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>Raten, möglicherweise treten Verbindungsprobleme auf, die nicht in Zusammenhang mit der maximalen Auslastung stehen.</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>{1} Bytes von {2} empfangen</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Empfangene Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Empfehlung</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Empfohlene Konfiguration für manuelle Prüfung:</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Aktualisieren</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Fern</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Ferne IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Remoteschleife</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation> Seriennummer (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation> Seriennummer (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Remotekonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Remotekonfiguration konnte nicht wiederhergestellt werden</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> Softwareversion (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Softwareversion (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Remoteübersicht</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>Gerätname (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Gerätname (remote)</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Bereich entfernen</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Antworter-ID</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>IP des&#xA;antwortenden Routers</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>J-QuickCheck neustarten</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Konfiguration wie vor Beginn des Tests wiederherstellen bevor der Test verlassen wird</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Remotekonfiguration für die Prüfung wird wiederhergestellt</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>RFC beschränken auf</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>Keine Ergebnisse der Grundauslastungsprüfung verfügbar, bitte klicken Sie zur Behebung dieses Problems auf "Vorschläge für nächste Schritte".</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Zu überwachende Ergebnisse:</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Retransmissions</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Es wurden Retransmissions gefunden&#xA;Das zeitliche Auftreten der Retransmissions wird analysiert</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>Retransmissions gefunden. Bitte exportieren Sie die Datei zur weiteren Analyse an einen USB-Datenträger.</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>Herunterladen von {1} wurde vom Benutzer abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>wechseln Sie zur Standardbenutzeroberfläche zurück durch Klicken auf die </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>RFC 2544-Ethernet-Prüfbericht</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>RFC 2544 Modus</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>RFC 2544 Modus</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544-Standard</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>RFC 2544 Test</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>RFC 2544 wird mit Acterna Test Payload ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Testbericht</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>RFC/FC Test kann nicht ausgeführt werden, weil bereits eine Instanz eines Graphischen Resultate Skripts für mehrere Ströme ausgeführt wird</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>RFC 2544 Modus</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY erk.</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Ausführen</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>FC-Prüfung&#xA;ausführen</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck ausführen</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>$l2quick::testLongName ausführen</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Test wird bei {1}{2} Last ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Test wird mit {1}{2} Last durchgeführt. Dies wird {3} Sekunden dauern.</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>RFC 2544-Prüfung&#xA;ausführen</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Skript ausführen</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Akt. Rx L1 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Nur Rx</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>FTP Durchsatz Testbericht speichern</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>HTTP Durchsatz Testbericht speichern</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>VLAN-Scan Testbericht speichern</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> skalierte Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Screenshot erstellt</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Skript abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>Sekunde</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(sek)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Wählen Sie einen Namen für die kopierte Konfiguration aus.</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Wählen Sie einen Namen für die neue Konfiguration aus.</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Select a range of VLAN IDs</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Ausgewählte Übertragungsrichtung:</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Ausgewählte Testrichtung&#xA;Downstream</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Ausgewählte Testrichtung&#xA;Upstream</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Auswahl Warnung</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>"Ok" wählen um die Konfiguaration zu ändern&#xA;Namen ändern um eine neue Konfiguration zu erzeugen&#xA;(Bitte nur Buchstaben, Zahlen, Leerzeichen und Unterstriche verwenden)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Testkonfigurarion auswählen:</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Wählen Sie die Eigenschaft aus, nach der die erkannten Geräte sortiert werden sollen.</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Bitte wählen Sie die Tests, die ausgeführt werden sollen</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>URL auswählen</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Wählen Sie das Format zum Laden zugehöriger Setups aus.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>ARP-Anfrage nach Ziel-MAC-Adresse senden</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Sende Traffic für</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>Sendet Traffic. Der erwartete, von J-QuickCheck erkannte Durchsatz wird anhand dieses Werts skaliert.</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Sequenz-ID</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>Server-ID:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>Server-ID</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Dienstname</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Erfolg/Fehler-Status anzeigen</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;Bewertung pass / fehl anzeigen für:</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Größen</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Überspringen</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Latenzprüfung (RTD) wird übersprungen, Vorgang wird fortgesetzt</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Softwareversion</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> SW-Version</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Quelladresse</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>Quelladresse nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Verfügbarkeit der Quelle wird hergestellt ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>Quellen-ID</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>Quellen-IP</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>Quellen&#xA;IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>Quellen-IP-Adresse ist mit der Zieladresse identisch</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>Quell-MAC</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Quell-MAC-&#xA;Adresse</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Geben Sie die Verbindungsbandbreite an</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Geschwin.</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Geschwindigkeit (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Grundauslastungsprüfung wird gestartet</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Starte Versuch</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOPP</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Überprüfen Sie das Diagramm daraufhin, ob sich eine zeitliche Korrelation zwischen den TCP-Retransmissions und der übermäßigen Netzwerkauslastung ergibt.  Ermitteln Sie auf der Registerkarte "TCP-Retransmissions" die Quell-IP, die Hauptverursacher der TCP-Retransmissions ist. Überprüfen Sie die Anschlusseinstellungen zwischen der IP-Adresse des Quellgeräts und dem angeschlossenen Gerät. Stellen Sie sicher, dass die Option für Halbduplex nicht aktiviert ist.  Die Zerlegung der Teilstrecken kann auch erfolgen, indem das Analysegerät an die IP-Adresse des Zielgeräts angenähert wird. Wenn bei einer weiteren Aufteilung in Teilstrecken die Retransmissions verringert werden, können auf diese Weise fehlerhafte Verbindungsabschnitte ermittelt werden.</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Übersicht Messwerte:</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Übersicht von Seite {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN Benutzerpri.</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>Symmetrisch bedeutet Senden und Empfangen am lokalen Standort im Loopback-Modus. Asymmetrisch bedeutet Senden vom lokalen Standort an die Gegenstelle im Upstream-Modus und von der Gegenstelle zum lokalen Standort im Downstream-Modus.</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Symmetrie</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Klicken Sie auf den Namen einer Konfiguration, um sie auszuwählen </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	{1} MB Datei laden....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	{1} MB heraufladen...</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Rate: {1} kbit/s&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Rate: {1} Mbit/s&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx Rahmen {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              ACHTUNG!&#xA;&#xA;	    Möchten Sie diese Konfiguration &#xA;	           wirklich unwiderruflich löschen?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Verwenden Sie nur Buchstaben, Zahlen,&#xA;		   Leerzeichen, Bindestriche und Unterstriche.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Diese Konfiguration ist schreibgeschützt&#xA;		   und kann nicht gelöscht werden.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Sie müssen über die Tastatur einen Namen&#xA;		           für die neue Konfiguration eingeben.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   Die angegebene Konfiguration ist bereits vorhanden.</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Dauer: {1} Sekunden&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tx-Frames {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>TCP Host konnte keine Verbindung herstellen. Test vorzeitig abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>TCP Host hat einen Fehler festgestellt. Test vorzeitig abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>TCP-Retransmissions</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation> Techniker</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Techniker</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Name des Technikers</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Abschluß</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>                              Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Test wurde vom User vorzeitig abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Test mit konfigurierter max. Bandbreiteneinstellung über die Registerkarte Setup - Alle Tests</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>Prüfung bei verschiedenen geringeren Datenraten durchführen. Wenn die Fehler auch bei geringeren Datenraten auftreten</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Testkonfiguration:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Testkonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Messdauer</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Prüfdauer: Mindestens 3 mal die konfigurierte Prüfdauer.</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Getestete Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Test abgeschlossen ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Teste {1} credits </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Teste {1} credits </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Test wird durchgeführt unter </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Teste Verbindung ...</translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation>Gerätname</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Test wird gestartet</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Testprotokoll:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Name des Tests:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Prüfung:			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Prüfung:			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Testverfahren</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Testablaufprotokoll</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Testbereich (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Testbereich (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Testergebnisse </translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Messgerät Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Auszuführende Tests:</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Auszuführende Tests</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Test was aborted</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>Fehler in aktiver Aufwärts-Schleife oder keine feste Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>Fehler in aktiver Aufwärts-Schleife oder keine feste bzw. permanente Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>Die mittlere Rate für {1} lag bei {2} kbit/s</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>Die mittlere Rate für {1} war {2} Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>Die Datei überschreitet die maximal zulässige Größe für JMentor (50000 Pakete)</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>den Toleranzschwellwert für die Rahmenverluste, damit geringfügige Rahmenverluste toleriert werden.</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Das ICMP (Internet Control Message Protocol) ist vor allem im Kontext von ICMP-"Ping" bekannt. Die Nachricht "ICMP-Ziel nicht erreichbar" weist darauf hin, dass ein bestimmtes Ziel vom Router (oder einem anderen Netzwerkgerät) aus nicht erreichbar ist.</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>Der Zähler L2 Filter-Rx Acterna-Rahmen wurde weiter hochgezählt</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>Die LBM/LBR Schleife ist fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>Die lokale und die ferne Quellen-IP-Adresse sind identisch.</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> Die Einstellungen für die lokale Konfiguration wurden erfolgreich in die Remotekonfiguration kopiert </translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>Die lokale Quell-IP-Adresse ist nicht verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Die gemessene MTU ist zu klein, um fortzufahren. Test abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>Der ausgewählte Name ist bereits vorhanden.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>Für den Anschluss des Netzwerkelements, zu dem eine Verbindung hergestellt wurde, wurde während der Aushandlung der Verbindung der Halbduplexmodus festgelegt. Wenn diese Angabe richtig ist, klicken Sie auf die Schaltfläche "Im Halbduplexmodus fortsetzen". Klicken Sie ansonsten auf die Schaltfläche "J-QuickCheck beenden", und geben Sie erneut den Anschluss an.</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>In dem Diagramm zur Netzwerkauslastung wird die Bandbreite angezeigt, die von der Gesamtheit der Pakete in der Aufzeichnungsdatei für die gesamte Aufzeichnungsdauer in Anspruch genommen wird.  Wenn TCP-Retransmissions erkannt wurden, wird empfohlen, auch im Hauptanalysebildschirm die Ergebnisse für die einzelnen TCP-Schichten zu analysieren.</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>Buffer Credits Durchsatzmeßungen vorgenommen, um die doppelte Glasfaserlänge auszugleichen.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Theoretische Berechnung</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>theoretischen &amp; gemessenen Werte:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>Bei dem Anschluss der Gegenstelle wurde die Option zur automatischen Aushandlung deaktiviert, und es ist kein Wert für den erwarteten Durchsatz verfügbar. Wahrscheinlich befindet sich der Anschluss der Gegenstelle im Halbduplexmodus. Wenn der Halbduplexmodus für den Anschluss der Gegenstelle nicht richtig ist, ändern Sie die Einstellungen in Vollduplex und führen Sie den J-QuickCheck erneut durch. Wenn der Messwert für den erwarteten Durchsatz anschließend plausibler ist, können Sie die RFC 2544-Prüfung durchführen. Sollte der Wert für den erwarteten Durchsatz hingegen weiterhin nicht verfügbar sein, überprüfen Sie die Anschlusskonfiguration der Gegenstelle. Möglicherweise sind abweichende Anschlusseinstellungen (HD/VD) die Ursache.&#xA;&#xA;Wenn der Halbduplexmodus für den Anschluss der Gegenstelle richtig ist, ändern Sie unter 'Ergebnisse' -> 'Setup' -> 'Schnittstelle' -> 'Physikalische Schicht' die Duplexeinstellung von Vollduplex in Halbduplex. Öffnen Sie dann das RFC 2544-Script ('Ergebnisse' -> 'RFC 2544-Tiefenprüfung'), beenden Sie J-QuickCheck, wählen Sie in 'Durchsatz' unter 'Einnullen' die Option 'RFC 2544-Standard (Halbduplex)' und führen Sie die RFC 2544-Prüfung erneut durch.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>Es ist ein Problem in zusammenhang mit dem Datenaustausch mit dem Remoteende aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>Das Remoteschleifengerät antwortet nicht auf den Viavi-Schleifenbefehl, sondern sendet die übertragenen Frames mit vertauschten Quell- und Zieladressfeldern an das lokale Gerät zurück.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>Das Remoteschleifengerät antwortet auf den Viavi-Schleifenbefehl und sendet die übertragenen Frames mit vertauschten Quell- und Zieladressfeldern an das lokale Gerät zurück.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>das Remoteschleifengerät liefert den uebertragenen Rahmen unverädert zum lokalen Gerät zurück</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>Das Remote-Schleifengerät unterstützt OAM LBM und antwortet auf den erhaltenen LBM Rahmen, indem es einen entsprechenden LBR Rahmen and das lokale Gerät überträgt.</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>Für das Remoteende wurde MPLS-Kapselung festgelegt</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>Bei dem Remoteende handelt es sich möglicherweise um eine Loopback-Anwendung</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>Die Remotequell-IP-Adresse ist nicht verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA;Der Bericht wurde als "{1}{2}" im PDF-Format gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>Der Bericht wurde als "{1}{2}" im PDF-Format gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>Der Bericht wurde als "{1}{2}" im PDF-, TXT- und LOG-Format gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>Der Bericht wurde mit dem Namen "{1}{2}" im TXT- und im LOG-Format gespeichert</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>Die IP-Adresse des antwortenden Routers konnte das Paket nicht an die Ziel-IP-Adresse weiterleiten, daher sollte zunächst dieses Verbindungsproblem zwischen dem antwortenden Router und dem Ziel gelöst werden.</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>Die RFC 2544-Prüfung unterstützt die MPLS-Kapselung nicht.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>Der sendende Laser wurde auf AN geschalten&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Der Tx Laser is aus!&#xA;Soll der Laser eingeschaltet werden?&#xA;Drücken Sie "Ja" für Ein oder "Nein" für abbrechen.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Der Sendelaser ist ausgeschaltet! Soll der Laser eingeschaltet werden?&#xA;&#xA;Drücken Sie auf "Ja" , um Laser einzuschalten, oder auf "Nein", um Vorgang abzubrechen. </translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>Der VLAN-Scan-Test kann nicht beendet werden, wenn OAM CCM aktiv ist.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>Der VLAN-Scan-Test kann nicht korrekt konfiguriert werden.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Diese Konfiguration kann nur gelesen, aber nicht geändert werden.</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Im asymmetrischen Modus muss dies die IP-Adresse der Gegenstelle sein</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>In dieser Tabelle sind die Quell-IP-Adressen aufgeführt, bei denen TCP-Retransmissions aufgetreten sind. Falls TCP-Retransmissions ermittelt wurden, kann deren Ursache in einem Paketverlust in Downstream-Richtung (als Richtung Zielende) liegen.  Sie können jedoch auch auf ein Problem in Zusammenhang mit einem Anschluss im Halbduplexmodus hinweisen.</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>Dieser Test wird mit Acterna Test-Payload ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Dieser Test ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Für diese Prüfung müssen die Daten im Netzwerk VLAN-gekapselt sein Stellen Sie bei dieser Konfiguration sicher, dass das angeschlossene Netzwerk eine IP-Adresse bereitstellt.</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Dies wird {1} Sekunden daueren.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Durchsatz (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Durchsatz ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Durchsatz- und RTD-Latenzprüfungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Durchsatz- und Paket-Jitter-Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Durchsatz Rahmenverlusttoleranz:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Durchsatz Rahmenverlusttoleranz</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Durchsatz-, RTD-Latenz- und Paket-Jitter-Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Erfolgskriterium für Durchsatztest:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Erfolgskriterium für Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Durchsatzskalierung</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Durchsatzskalierungsfaktor</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>Die Dauer der Durchsatzprüfung betrug {1} Sekunden.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Ergebnis Durchsatztest:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Ergebnis Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Durchsatztest Resultate: ABGEBROCHEN</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Durchsatztestergebnisse: FEHL</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Durchsatztestergebnisse: ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Durchsatz Erfolgskriterium (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> Durchsatz Erfolgskriterium: {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Durchsatz Erfolgskriterium (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Durchsatztestdauer:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Durchsatztestdauer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Durchsatz Annäherungsmethode</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Durchsatz Annäherungsmethode</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation> Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Zeit pro ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Zeit: (Sekunden)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>zeit&#xA;(Sek.)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation> Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>Anzahl der Besuche</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Bitte überprüfen Sie die Kabelverbindung, und starten Sie dann den J-QuickCheck erneut, um fortzufahren</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Um den maximalen Durchsatz zu ermitteln, wählen Sie die Option "RFC 2544-Standard", bei der die Zähler für übertragene und empfangene Rahmen verglichen werden oder die Option "Viavi-Erweitert", bei der der Messwert für die durchschnittliche prozentuale L2-Auslastung einbezogen wird.</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Absteigend</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Um im asymmetrischen Modus die Zeit zu sparen, sollte die Latenz (RTD) nur in einer Richtung ausgeführt werden</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>um zu überprüfen, ob die Rahmenverluste sporadisch oder ständig auftreten.</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Bytes&#xA;insgesamt</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Rahmen&#xA;gesamt</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Gesamtzahl</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Gesamtauslastung {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>Gesamtauslastung (kbit/s):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>Gesamtauslastung (Mbit/s):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>Um den Bericht anzuzeigen, nach dem Verlassen von {1} "Bericht anzeigen" im Menü "Bericht" auswählen.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>Innerhalb von</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Datenaufkommen: Konstant mit {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Traffic-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>Es wurden weiterhin Daten vom Remoteende empfangen</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>Der sendende Laser ist AUS!</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Gesendete Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Downstreamübertragung</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Upstreamübertragung</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Versuch</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Versuch {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Versuch {1} Abgeschlossen&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Versuch {1} von {2}</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Dauer des Versuchs (Sekunden)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>Versuche</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Zweiter Versuch</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>Tshark-Fehler</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>TX Buffer zu Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Übertragungsrichtung</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Tx Laser aus</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Akt. Tx L1 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Nur Tx</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation>Versuch, automatisch eine Schleife an der Gegenstelle&#xA; zu schalten, ist fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>Verbinde mit der Testapplikation nicht möglich!</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>Verbinde mit der Testapplikation nicht möglich!&#xA;Drücken Sie " Ja " für Ein oder " Nein " für abbrechen.</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>Es konnte keine DHCP-Adressen abgerufen werden</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>RFC-2544 Test kann nicht ausgeführt werden, die lokale Schleife ist aktiviert.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>Die Prüfung konnte nicht durchgeführt werden</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Unable to run VLAN Scan test with Local Loopback enabled.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>KEINE</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Einheitenkennung</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>UP</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>(Auf oder Ab)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Upstream-Richtung</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>Test durch Benutzer abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>Test durch Benutzer abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Benutzername</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Gefragter Benutzer ist nicht aktiv</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Durch Benutzer gewählt&#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Durch Benutzer gewählt ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Überprüfen Sie den Bildschirm "Übersicht-Status" auf Fehlerereignisse.</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Mit</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Verwende Frame-Größe von</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Auslastung TCP-Retransmissions</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Wert</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>Blau hervorgehobene Werte sind aus tatsächlichen Tests.</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Verifizierung das aktive Verbindung vorhanden ist...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>überprüfen Sie Ihre Remote-IP-Adresse, und wiederholen Sie den Vorgang</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>überprüfen Sie Ihre Remote-IP-Adresse, und wiederholen Sie den Vorgang</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi-optimiert</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Zu testende VLAN-ID-Bereiche</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN Scan Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAN Scan Test Results</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN Test Report</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>VTP/DTP/PAgP/UDLD Rahmen festgestellt!</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>Warte bis Auto Negotiation fertig ist...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>Warten auf Ziel-MAC-Adresse für&#xA;  IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>Warte auf DHCP Paramater ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>Warte bis Layer 2 Verbindung vorhanden ist...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>Warten auf Verbindung</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>Warte auf OWD-Aktivierung, ToD Sync und 1PPS Sync</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Waiting for successful ARP ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>wurde in der letzten Sekunde erkannt.</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Website Größe</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Es ist eine aktive Schleife verfügbar</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>Fehler!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>Wählen Sie zur Prüfung von Halbduplexleitungen die Option 'RFC 2544-Standard'.</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Fenstergröße/Kapazität</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>mit der RFC 2544-Empfehlung.</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Möchten Sie einen Ergebinisbericht speichern?&#xA;&#xA;Drücken Sie "Ja" oder "Nein"</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>Sie können auch das Diagramm mit den graphischen Ergebnissen zur Rahmenverlustrate verwenden</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>Möchten Sie dieses Skript von {1} aus starten.</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Möglicherweise kann eine Verbindung erst wieder nach Abschluss des Vorgangs hergestellt werden</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Ermittlung der maximalen Durchsatzrate</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Annäherung an optimalen Wert für Credit Buffer</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Annäherungsmethode</translation>
        </message>
    </context>
</TS>
