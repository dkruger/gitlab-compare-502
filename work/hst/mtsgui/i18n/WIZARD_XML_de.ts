<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI-Check</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Vorherige Konfiguration bearbeiten</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Lädt eine Konfiguration aus einem Profil</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Eine Neue Konfiguration Starten (Standard)</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Manuell</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Testeinstellungen manuell konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Testeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Profile speichern</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Ende: Manuelle Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Test ausführen</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Gespeichert</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Profile laden</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Ende: Laden der Profile</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Konfiguration bearbeiten</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Ausführen</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>CPRI-Überprüfung ausführen</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>SFP-Nachweis</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>beenden: Test</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Test wiederholen</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Detailergebnisse anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>CPRI-Überprüfung beenden</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Überprüfen</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Interface</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Layer 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Ende: Ergebnisse überprüfen</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Berichtsinfo</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Ende: Berichterstellung</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Detailergebnisse überprüfen</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>UNVOLLSTÄNDIG</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>FERTIG</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FEHLER</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>KEINE</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI Check:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** Starte CPRI-Überprüfung ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Test beendet ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Test abgebrochen ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>überspringen</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>Sie können die Konfiguration für diesen Test speichern.&#xA;&#xA;Sie können die gespeicherten Daten (ganz oder &#xA;profilweise) für die Konfiguration zukünftiger Prüfungen verwenden.</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>überspringen</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Lokale SFP-Verifizierung</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Anschluss</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Bitte legen Sie eine SFP ein.</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>SFP Wellenlänge (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>SFP Anbieter</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>SFP Anbieter Vers.</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>SFP-Lieferant P/N</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Empfohlene Quoten</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Zeige zusätzliche SFP-Daten</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP ist in Ordnung.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>Kann für diesen Wert nicht SFP überprüfen.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP ist nicht akzeptabel.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Messdauer</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Gerät der Gegenstelle</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Weiteres</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Hardwareschleife</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Rx Level optisch Maximalgrenzwert (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Optisches Rx Niveau Mindestgrenze (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Round Trip Verzögerung Maximalgrenzwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>CPRI-Check überspringen</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP-Check</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Statustaste für Prüfung</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>Geplant</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Test&#xA;durchführen</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Test&#xA;stoppe </translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Lokale SFP-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>Kein SFP wird erkannt.</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Wellenlänge (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Max. Tx-Pegel (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Max. Rx-Pegel (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceiver</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Interfaceresultate</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI Überprüfung Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Schnittstelle Test</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Schicht 2 Test</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD Test</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT Test</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Signal vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Sync erreicht</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Rx Max. Freq.-Abweichung (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Codeverletzungen</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Optischer Rx-Pegel (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Layer 2-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>Startstatus</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Rahmensync</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>RTD-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Durchlaufverzögerung Durchschnitt (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>BERT Resultate</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Muster Sync</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Musterverlust</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Bit/TSE-Fehler</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Typ von Netzwerkelement</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>L1-Synchronisation</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Protokolleinrichtung</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M-Ebeneneinstellung</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Operation</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Passiver Link</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>überspringen</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informationen zum Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Name des Kunden:</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>Techniker-ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Ort des Tests:</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Arbeitsauftrag:</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Anmerkungen/Hinweise:</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Radio:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Band:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Gesamtstatus</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Erweiterte FC-Test</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Symmetrie</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Lokale Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Verbindung zu Remoteseite herstellen</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Netzwerk</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Faserkanaleinstellungen</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>FC Tests</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Konfigurierungsvorlagen</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Test auswählen</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>Nutzung</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Rahmenlängen</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Rahmenverlusttest</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Aufeinanderfolgender Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Buffer Credit Test</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Prüfregler</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>Test Laufzeiten</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Test Schwellwerte</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Konfiguration ändern</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Fortgeschrittene Faserkanaleinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Fortgeschrittene Nutzungseinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Fortgeschrittene Durchsatz-Latenzeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Fortgeschrittene aufeinanderfolgende Testeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Fortgeschrittene Frame-Verlust-Testeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Serviceaktivierungstesten ausführen</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Konfiguration ändern und Test erneut ausführen</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>FC Test verlassen</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Weiteren Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Deckblatt</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Während der Prüfung der lokalen Einheit musste die Synchronisierung mit dem Zeitgeber abgebrochen werden. Bitte überprüfen Sie die Anschlüsse der OWD-Zeitgeberhardware. Die Prüfung wird fortgesetzt (falls sie noch nicht abgeschlossen ist), es werden jedoch keine Ergebnisse zur Frameverzögerung verfügbar sein.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Während der Prüfung der Remoteeinheit musste die Synchronisierung mit dem Zeitgeber abgebrochen werden. Bitte überprüfen Sie die Anschlüsse der OWD-Zeitgeberhardware. Die Prüfung wird fortgesetzt (falls sie noch nicht abgeschlossen ist), es werden jedoch keine Ergebnisse zur Frameverzögerung verfügbar sein.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Aktive Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>Auflösung Nachbaradresse nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Service #1: ARP-Anforderung für Ziel-MAC.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Service #1 (lokal): Sendet eine ARP-Anforderung für ein MAC-Ziel.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Service #1 (remote): Sendet eine ARP-Anforderung für ein MAC-Ziel.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>ARP-Anfrage nach Ziel-MAC-Adresse senden</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Lokale Einheit sendet eine ARP-Anforderung für ein MAC-Ziel.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Remoteeinheit sendet eine ARP-Anforderung für ein MAC-Ziel.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>Der Netzwerk Elementport wird für den Wechselverkehrbetrieb bereitgestellt. Wenn Sie weiterfahren möchten, drücken Sie die Taste "Im Wechselverkehr weiterfahren". Falls nicht, währen Sie "Test abbrechen".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Versuch, die Schleife in Aufwärtsrichtung zu setzen. Überprüfung auf aktive Schleifen wird durchgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Überprüfung auf Hardwareschleifen wird durchgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Überprüfung auf eine LBM/LBR Schleife.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Überprüfung auf permanente Schleifen wird durchgeführt.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Zeitüberschreitung der DHCP-Parameteranforderung. Es konnten keine DHCP-Parameter abgerufen werden.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>Die Verbindung zur Remoteeinheit wurde beim Wechsel in den Loopback-Modus zwangsweise beendet.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>Bei SAMComplete ist ein Timeout aufgetreten, da keine abschließende Anzahl an empfangenen Frames ermittelt werden konnte. Die gemessene Anzahl an empfangenen Frames ist weiterhin unerwartet angestiegen.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Harte Schleife Gefunden</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>Die J-QuickCheck-Prüfung kann die Datenanbindung erst dann durchführen, wenn eine Verbindung zu der Remoteeinheit hergestellt ist.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>LBM/LBR Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>Die lokale Verbindung wurde unterbrochen. In der Folge wurde auch die Verbindung zu der Remoteeinheit zwangsweise beendet. Wenn die lokale Verbindung wiederhergestellt wurde, können Sie erneut versuchen, die Verbindung zur Gegenstelle herzustellen.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>Verbindung ist zurzeit nicht aktiv.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>Die Quell- und die Ziel-IP-Adresse sind identisch. Bitte legen Sie eine der beiden IP-Adressen neu fest.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Datenanwendung, die Remoteanwendung an IP-Adresse #1 ist eine Streaming-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>Es konnte keine Schleife hergestellt werden.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>Es konnte keine Verbindung zu der Remoteeinheit hergestellt werden. Wechseln Sie zu der Seite 'Verbinden', überprüfen Sie die Ziel-IP-Adresse und klicken Sie dann auf die Schaltfläche 'Mit Gegenstelle verbinden'.</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Bitte gehen Sie auf die Seite „Netzwerk“, bestätigen die Konfiguration und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>Auf die Verfügbarkeit der Optik warten oder derzeitig keine Optik vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Permanente Schleife gefunden</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>PPPoE Abfallzeit. Bitte prüfen Sie Ihre  PPPoE Einstellungen und versuchen Sie es nochmals.</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>Ein nicht behebbarer PPPoE Fehler trat auf</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Versuch, am Server anzumelden</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>Es wurde ein Problem in Zusammenhang mit der Remoteanbindung festgestellt. Möglicherweise kann auf die Gegenstelle nicht mehr zugegriffen werden.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>Es konnte keine Verbindung zu der Remoteeinheit an IP-Adresse #1 hergestellt werden. Überprüfen Sie die Remotequell-IP-Adresse und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>Die entfernte Anwendung an der IP-Adresse #1 ist eine Loopback-Anwendung. Sie ist nicht mit Enhanced RFC 2544 kompatibel.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die Remoteanwendung an IP-Adresse #1 ist keine TCP WireSpeed-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>Die Version der Software auf der Ferneinheit scheint neuer zu sein. Falls Sie weitertesten,  könnte einige Funktionalität eingeschränkt sein. Es wird empfohlen, die Software auf dieser Einheit für optimale Leistung zu aktualisieren.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>Die Version der Software auf der Ferneinheit scheint älter zu sein. Falls Sie weitertesten,  könnte einige Funktionalität eingeschränkt sein. Es wird empfohlen, die Software auf der Ferneinheit für optimale Leistung zu aktualisieren.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Die Version der Software auf der Ferneinheit konnte nicht bestimmt werden.  Falls Sie weitertesten,  könnte einige Funktionalität eingeschränkt sein. Es wird empfohlen, zwischen Einheiten mit equivalenten Versionen der Software zu testen.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>Bei dem Versuch, die Schleife in Aufwärtsrichtung zu durchlaufen, ist ein Fehler aufgetreten. Der Versuch wird wiederholt.</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>Die Einstellungen für die gewählte Vorlage sind erfolgreich angewandt worden.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Die Softwareversion auf der entfernten Einheit konnte entweder nicht bestimmt werden oder stimmt nicht mit der Version auf dieser Einheit überein. Wenn Sie mit dem Test fortfahren, können einige Funktionen eingeschränkt sein. Es wird empfohlen, zwischen den Einheiten mit gleichwertigen Softwareversionen zu testen.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>Explizite Anmeldung konnte nicht durchgeführt werden.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Schicht #1-Anwendung, die Remoteanwendung an IP-Adresse #2 ist eine Schicht #3-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 Mbps) entspricht nicht der Rate der Ferneinheit (#2 Mbps). Bitte konfigurieren Sie diese erneut auf Asymmetrisch auf der "Connect" Seite.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 kbps) entspricht nicht der Rate der Ferneinheit (#2 kbps). Bitte konfigurieren Sie diese erneut auf Asymmetrisch auf der "Connect" Seite.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 Mbps) entspricht nicht der der Ferneinheit (#2 Mbps). Bitte konfigurieren Sie erneut auf Asymmetrisch auf der "Connect" Seite und starten Sie den Test erneut.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 kbps) entspricht nicht der Rate der Ferneinheit (#2 kbps). Bitte konfigurieren Sie diese erneut auf Asymmetrisch auf der "Connect" Seite und starten Sie den Test erneut.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 Mbps) entspricht nicht der Rate der Ferneinheit (#2 Mbps). Bitte konfigurieren Sie diese erneut auf Asymmetrisch auf der RFC2544 "Symmetrie" Seite und starten Sie den Test erneut.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Die örtliche Einheitsleitungsrate (#1 kbps) entspricht nicht der Rate der Ferneinheit (#2 kbps). Bitte konfigurieren Sie diese erneut auf Asymmetrisch auf der RFC2544 "Symmetrie" Seite und starten Sie den Test erneut.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Mindestens ein Test muss gewählt werden. Bitte wählen Sie mindestens einen Test und versuchen Sie es noch einmal.</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>Sie haben keine Frame Größes zum Testen gewählt. Bitte wählen Sie mindestens eine Frame Größe vor dem Starten.</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>Eine Frameverlustrate, die die konfigurierte Frameverlustschwelle überschritt, wurde entdeckt in der letzten Sekunde. Bitte prüfen Sie die Leistung des Links mit einem manuellen Verkehrstest. Nachdem Sie Ihre manuellen Tests durchgeführt haben, können Sie RFC 2544 erneut laufen lassen.&#xA;Empfohlene manuelle Testkonfiguration:&#xA;Framegröße: #1 Bytes&#xA;Verkehr: Konstant mit #2 Mbps&#xA;Testdauer: Mindestens 3 Mal die konfigurierte Testdauer. Durchlauftestdauer war #3 Sekunden.&#xA;Zu überwachende Ergebnisse:&#xA;Nutzen Sie den Übersichtsstatus-Bildschirm, um nach Fehlerereignissen zu suchen.  Falls die Fehlerzähler unregelmäßig erhöht werden, lassen Sie den manuellen Test zu verschiedenen niedrigeren Verkehrsraten laufen. Falls Sie weiterhin Fehler erhalten, selbst bei niedrigeren Raten, könnte der Link allgemeine nicht mit der Maximallast verbundene Probleme haben. Sie können auch das Grafische Ergebnisse Frameverlustrate Cur Diagramm nutzen, um zu sehen, ob sporadische oder konstante Frameverlustereignisse bestehen. Falls Sie das Problem mit den sporadischen Fehlern nicht lösen können, können Sie die Frameverlust-Toleranzschwelle so setzen, dass kleine Frameverlustraten toleriert werden. Hinweis: Sobald Sie eine Frameverlusttoleranz nutzen, entspricht der Test nicht mit der RFC 2544 Empfehlung.</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Hinweis: Aufgrund unterschiedlicher VLAN-Stapeltiefen für Sender und Empfänger wird die konfigurierte Rate angepasst, um Ratendifferenzen zwischen den Ports zu kompensieren.</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>#1 Byterahmen</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>#1 Bytepakete</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>Der L2 Filter Rx Acterna Framezähler stieg weiter in den letzten 10 Sekunden, obwohl der Verkehr angehalten sein sollte</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Ermittlung der maximalen Durchsatzrate</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Versuchen #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Versuchen #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>#1 L1 kbps wird versucht</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Versuchen #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Versuche #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>Prüfen jetzt #1 L1 Mbps. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>Prüfen jetzt #1 L2 Mbps. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>Jetzt wird #1 L1 kbps geprüft. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>Pfüfen jetzt #1 L2 kbps. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>Verifizierung von #1 % läuft. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Maximum gemessener Durchsatz: #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Maximum gemessener Durchsatz: #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Maximum gemessener Durchsatz : #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Maximum gemessener Durchsatz: #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Maximal gemessener Durchsatz: #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Es ist keine maximale Durchsatzmessung verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>Frame Verlust Test (RFC 2544 Standard)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>Frame Verlust Test (Von oben nach unten)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>Frame Verlust Test (Von unten nach oben)</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Test läuft mit #1 L1 Mbps Last. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Test läuft mit #1 L2 Mbps Last. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Test bei #1 L1 kbps Last laufen lassen. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Test läuft mit #1 L2 kbps Last. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Test wird mit #1 % Last durchgeführt. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Back to Back Rahmen Test</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Versuch #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Pauserahmen erkannt</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 Paketburst: fehl</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 Rahmenburst: fehl</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 Paketburst: pass</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 Rahmenburst: pass</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Burst Jagdtest</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>#1 kB Burst wird versucht</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Größer als #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Weniger als #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>Die Zwischenspeichergröße ist #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>Die Zwischenspeichergröße ist kleiner als #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>Die Zwischenspeichergröße ist größer als oder gleich #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>#1 Frames gesendet</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>Empfangene #1 Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>Verlorene #1 Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Burst (CBS) Test</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>Geschätzte CBS: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>Geschätzte CBS: unverfügbar</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>Der CBS Test wird übersprungen. Die Test Burst Größe ist zu groß, um diese Konfigurierung exakt zu prüfen.</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>Der CBS Test wird übersprungen. Die Test Laufzeit ist nicht lang genug,  um diese Konfigurierung genau zu prüfen.</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Paket gesprengt: fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Rahmen gesprengt: Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Paket gesprengt: durchgelaufen</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>Rahme gesprengt: durchgelaufen</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Burst-Überwachungsversuch #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Prüfung Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>Test ist nicht gültig für diese Paketgröße</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>Test ist nicht gültig für diese Framegröße</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Versuch #1 von #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>Es können keine Frameverluste künstlich induziert werden, da die Durchsatzprüfung bei maximaler Bandbreite ohne erkennbaren Frameverlust durchgeführt wurde</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Es kann kein Verlustereigniss induziert werden. Der Test ist für diese Paketgröße ungültig</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Es kann kein Verlustereigniss induziert werden. Der Test ist für diese Framegröße ungültig</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Durchschnittliche Wiederherstellungszeit: Größer als #1 Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Größer als #1 Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Durchschnittliche Wiederherstellungszeit: Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Durchschnittliche Wiederherstellungszeit: #1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 us</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Anpeilen vom optimalen Kredit Zwischenspeicher.  #1 Kredite werden getestet</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Teste #1 credits</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Verifikation mit #1 credits.  Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Buffer Credit Durchsatz Test</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Anmerkung: Es wird eine physikalische Schleife und ein Buffer Credit-Wert kleiner 2 angenommen. Dieser Test ist ungültig</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Hinweis: Unter Annahme einer physikalischen Schleife werden bei jedem Schritt bei doppelt so vielen Buffer Credits Durchsatzmeßungen vorgenommen, um die doppelte Glasfaserlänge auszugleichen</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Durchsatzmeßung bei #1 Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Durchsatz und Latenz Tests</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Durchsatz- und Paket-Jitter-Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Durchsatz, Latenz und Paket Jitter Tests</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Latenz und Paket Jitter Versuch #1. Dies wird #2 Sekunden dauern.</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Paket Jitter Testversuch #1. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Latenztestversuch #1. Dies wird #2 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Latenztestversuch #1 bei #2% der bestätigten Durchsatzlast. Dies wird #3 Sekunden dauern</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Starten RFC 2544 Test #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Starten FC Test #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Test beendet.</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Ergebnisse werden gespeichert, bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Ausgedehnter Lasttest</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>FC Test:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Netzwerk Konfigurierung</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Rahmentyp</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Testmodus</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Ebene der Wartungsdomäne</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>Sender-TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Verkapselung</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN-Stapeltiefe</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN Benutzerpri.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>SVLAN DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>Anwender-SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>CVLAN-Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>SVLAN 7 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>Anwender-SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>SVLAN 7 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>SVLAN 6 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>Anwender-SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>SVLAN 6 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>SVLAN 5 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>Anwender-SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>SVLAN 5 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>SVLAN 4 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>Anwender-SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>SVLAN 4 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>SVLAN 3 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>Anwender-SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>SVLAN 3 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>SVLAN 2 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>Anwender-SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>SVLAN 2 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>SVLAN 1 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>Anwender-SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>SVLAN 1 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Loop Type</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>Ether-Typ</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>Quell-MAC</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>SA MA automatisch inkrementieren</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation># MAC Adresse in der Reihe</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>IP Ether-Typ deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>OoS Ergebnisse Deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>DA Typ</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>Ziel-MAC</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Datenmodus</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>Anwendungs-Authentifikation</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Benutzername</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Passwort</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Provider des Dienstanbieters</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Dienstname</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Quellen-IP-Typ</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Quellen-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Standard-Gateway</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Subnetzmaske</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>Ziel-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Time To Live (hops)</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>IP ID Schritt</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Source Link-örtliche Adresse</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Quelle Globale Adresse</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Subnetz-Präfixlänge</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Zieladresse</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Lastklasse</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Fluss-Label</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Hop-Grenzwert</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Verkehrsmodus</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Quelle Port Service Typ</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Quellenport</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Ziel-Port Service-Typ</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Zielport</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>ATP Empfangs-IP-Typ</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>ATP Empfangs-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>Quellen-ID</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>Ziel-ID</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Sequenz-ID</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Originator ID</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Antworter-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Testkonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna Payload Version</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Latenzzeitmessung Präzision</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Bandweite Einheit</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Max Test Bandweite (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Upstream Max Test Bandweite (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Max Test Bandweite (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Upstream Max Testbandweite (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Downstream Max Test Bandweite (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Downstream Max Testbandweite (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Max Test Bandweite (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Echten 100% Verkehr erlauben</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Durchsatz Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Upstream Durchsatz Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Downstream Durchsatz Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Durchsatz Annäherungsmethode</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Durchsatz Rahmenverlusttoleranz (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Upstream Durchsatz Frame Verlust Toleranz (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Downstream Durchsatz Frame Verlust Toleranz (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Alle Tests Laufzeit(en)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Alle Tests Anzahl der Versuche</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Durchsatz Laufzeit(en)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Erfolgskriterium für Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Throughput Pass Threshold (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Upstream Durchsatz Bestehschwelle (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Durchsatz Erfolg-Schwellwert (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Upstream Durchsatz Erfolg-Schwellwert (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Downstream Durchsatz Bestehschwelle (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Downstream Durchsatz Erfolg-Schwellwert (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Erfolgskriterium für Durchsatztest (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>Anzahl der Latenz Versuche</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Latenzversuch Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Latenzbandbreite (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Latenz Erfolg-Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Latenz Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Upstream Latenz Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Downstream Latenz Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Versuchsanzahl Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Paket Jitter Versuch Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Paket Jitter Erfolg-Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Paket Jitter Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Upstream Paket Jitter Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Downstream Paket Jitter Erfolg-Schwellwert (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Rahmenverlust Testverfahren</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>Frame Verlust Versuchslaufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Frame-Verlust Bandbreite Granularität (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Upstream Frame-Verlust Bandweite  Granularität (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Frame Verlust Bandweite Granularität (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Upstream Frame Verlust Bandweite Granularität (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Downstream Frame Verlust Bandweite Granularität (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Downstream Frame-Verlust Bandweite  Granularität (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Genauigkeit Rahmenverlustsbandbr (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>Frame-Verlustbereich Minimum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>Frame Verlustbereich Minimum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>Frame Verlustbereich Minimum (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Frame-Verlustbereich Maximum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Frame Verlustbereich Maximum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Frame Verlustbereich Maximum (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Upstream Frame-Verlustbereich Minimum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Upstream Frame Verlustbereich Minimum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Upstream Frame-Verlustbereich Maximum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Upstream Frame Verlustbereich Maximum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Downstream Frame-Verlustbereich Minimum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Downstream Frame Verlustbereich Minimum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Downstream Frame-Verlustbereich Maximum (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Downstream Frame Verlustbereich Maximum (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Frame Verlust Anzahl der Schritte</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Aufeinanderfolgende Anzahl von Versuchen</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Aufeinanderfolgende Granularität (Frames)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Aufeinanderfolgende Max Burst Laurzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Upstream Aufeinanderfolgende Max Burst Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Downstream Aufeinanderfolgende Max Burst Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>PAUSE-Frames ignorieren</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Burst Testtyp</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Burst CBS Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Upstream Burst CBS Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Downstream Burst CBS Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Burst Jagd Min Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Burst Jagd Max Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Upstream Burst Jagd Min Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Upstream Burst Jagd Max Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Downstream Burst Jagd Min Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Downstream Burst Jagd Max Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Burst Test Anzahl der Versuche</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Burst CBS Anzeige Bestehen/Versagen</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Burst Jagd Anzeige Bestehen/Versagen</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Burst Jagdgröße Schwellwert (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Upstream Burst Jagd Größe Schwellwert (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Downstream Burst Jagd Größe Schwellwert (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>Systemwiederherstellung Anzahl der Versuche</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>Systemwiederherstellung Überlastungs-Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Erweiterte Ladezeit(en)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Erweiterte Durchlaufskalierung Laden (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Erweiterte Paketlänge Laden</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Erweitere Rahmenlänge Laden</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Zwischenspeicher Kredit Anmeldungstyp</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Zwischenspeicher Kredit Max Zwischenspeichergröße</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Zwischenspeicher Kredit Durchsatzschritte</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Zwischenspeicher Kreditlaufzeit</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Aufhebung Schleife an Gegenstelle erfolgreich:Gerät **#1** nicht mehr im Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Aufhebung Schleife an Gegenstelle erfolgreich: Gerät **#1** nicht im transparenten Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>Gegenstelle **#1** bereits nicht mehr im Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Ferne Schleife inaktiv aufgrund von Konfigurationsänderungen</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Errichtung Schleife an Gegenstelle erfolgreich:Gerät **#1** im Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Einrichtung Schleife an Gegenstelle erfolgreich: Gerät **#1** im transparenten Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>Gegenstelle **#1** bereits im Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>Gegenstelle **#1** war bereits im transparenten Schleifenmodus</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Selfloop oder Loop eines anderen Ports wird nicht unterstützt</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Datenstrom #1: Aufhebung Schleife an Gegenstelle fehlgeschlagen:Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Datenstrom #1: Errichtung Schleife an Gegenstelle fehlgeschlagen:Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Aufhebung Schleife an Gegenstelle fehlgeschlagen:Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Aufhebung transparente Schleife an Gegenstelle fehlgeschlagen: Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Errichtung Schleife an Gegenstelle fehlgeschlagen:Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Einrichtung transparente Schleife an Gegenstelle fehlgeschlagen: Zeitlimit für Bestätigung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Erfassung von Adressdubletten bei globalen Adressen gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Konfiguration globaler Adressen fehlgeschlagen (Einstellungen prüfen).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>Erfassung von Adressdubletten bei globalen Adressen wird abgewartet.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Erfassung von Adressdubletten erfolgreich ausgeführt. Globale Adresse ist gültig.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Erfassung von Adressdubletten fehlgeschlagen. Globale Adresse ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Erfassung von Adressdubletten bei Link - Lokalen Adressen gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Konfiguration Link - Lokaler Adressen fehlgeschlagen (Einstellungen prüfen).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>Erfassung von Adressdubletten bei Link - Lokalen Adressen wird abgewartet.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Erfassung von Adressdubletten erfolgreich ausgeführt. Link - Lokale Adresse ist gültig.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>Erfassung von Adressdubletten fehlgeschlagen. Link - Lokale Adresse ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Abfrage der zustandslosen IP gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Erhalt der zustandslosen IP fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>Zustandslose IP erhalten.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Abfrage der zustandsbehafteten IP gestartet.</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>Keine Router zur Lieferung einer zustandsbehafteten IP gefunden.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Abfrage einer zustandsbehafteten IP wird erneut versucht.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>Zustandsbehaftete IP erhalten.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>Netzwerk nicht erreichbar</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>Host nicht Erreichbar</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>Protokoll nicht erreichbar</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>Port nicht erreichbar</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Nachricht zu lang</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Zielnetzwerk unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Ziel-Host unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Zielnetzwerk verboten</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Ziel Host nicht zulässig</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Netzwerk nich erreichbar für TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Host nicht erreichbar  für TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Zeitlimit  während Übertragung überschritten</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Zeitlimit während Neuzusammensetzung überschritten</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Unbekannter ICMP-Fehler</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Ziel nicht erreichbar</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Adresse nicht erreichbar</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>Keine Route zum Ziel</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>Ziel ist kein Nachbar.</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>Kommunikation mit Ziel administrativ verboten</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Paket zu groß</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Hop-Grenzwert im Netz abgelaufen</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Fragmentreassembl. Zeit abgelaufen</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Fehlerhaftes Protokollfeld erkannt</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Unerkannter Next Header Typ erkannt</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Unerkannte IPv6 Option erkannt</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Inaktiv</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Aktiv</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Aktiv</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Netzwerk Aktiv</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Gefragter Benutzer ist nicht aktiv</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Daten Layer angehalten</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE Timeout</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP Timeout</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP Bestätigung Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP Unbekannt Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Ungültige Konfig</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Interner Fehler</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP erfolgreich. Ziel-MAC-Adresse erhalten</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Warten auf ARP-Antwort...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>Keine ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Im Halbduplexmodus fortsetzen</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>J-QuickCheck anhalten</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>J-QuickCheck-Test</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Remote-Schleifenstatus</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Lokaler Loop-Status</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>PPPoE Status</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>IPv6 Status</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>ARP Status</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>DHCP-Status</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>ICMP-Status</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Nachbar-Entdeckungsstatus</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Autokonfig Status</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>Adress-Status</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Einheitentdeckung</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Nach Einheiten suchen</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Suche</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Gewählte Einheit nutzen</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Anwendung wird beendet</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Mit Gegenstelle&#xA;verbinden</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Mit Gegenstelle&#xA;verbinden</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Trennen</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Mit ferner Einheit&#xA; verbunden</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>EIN</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Aus</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Eindirektional</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Loopback</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Downstream und Upstream</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>Beide Richtungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Durchsatz:</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>Downstream- und Upstream-Durchsätze sind gleich.</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>Downstream- und Upstream-Durchsätze sind verschieden.</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Netzwerk nur in eine Richtung testen.</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Messungen:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>Traffic wird lokal generiert und die Messungen werden lokal durchgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>Traffic wird lokal und fern generiert und die Messungen werden in jede Richtung durchgeführt. </translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Messrichtung:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>Traffic wird lokal generiert und über das ferne Testinstrument gemessen.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>Traffic wird fern generiert und über das lokale Testinstrument gemessen.</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Einweg-Verzög.</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Nur Rundfahrt Verzögerungsmessungen.&#xA;Die Ferneinheit kann keine unidirektionalen Verzögerungen tätigen.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Nur Unidirektionale Verzögerungsmessung.&#xA;Die Ferneinheit  kann Bidirektionalen RTD nicht tätigen.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden. &#xA;Die Ferneinheit kann keine Bidirektionale RTD oder Unidirektionale Verzögerung tätigen.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden.&#xA;Die Ferneinheit kann keine Unidirektionale Verzögerung tätigen.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden.&#xA;Die Ferneinheit kann keine Bidirektionale RTD  tätigen.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Nur Unidirektionale Verzögerungsmessung.&#xA;RTD ist beim unidirektionalen Test nicht gestützt.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden. &#xA;RTD ist beim Prüfen nicht unterstützt.  Unidirektional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden.&#xA;Die Ferneinheit kann keine Unidirektionale Verzögerung tätigen.&#xA;RTD ist beim Unidirektionalen Test nicht gestützt.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>Es können keine Verzögerungsmessungen getätigt werden.</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Latenz Messtyp</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Lokal</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Fern</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Benötigt ein Viavi-Prüfgerät an der Gegenstelle.</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Version 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Version 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC-Kalibrierung</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Optische Auswahl</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>IP-Einstellungen für Übertragungskanal an die ferne Einheit</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>Für den Loopback-Test sind keine Einstellungen der lokalen IP-Adresse erforderlich.</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statisch</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Statisch  Pro Service</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Statisch - Single</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuell</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>zustandsbehaftet</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Stateless</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>Quellen-IP</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Src Link-Lokale Adr</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Src Globale Adr</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Autom. erhalten</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Benutzerdefiniert</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>ARP-Modus</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Quellenadresstyp</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>PPPoE-Modus</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Erweitert</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Aus</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>Kunden Source MAC</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>Grundeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>Default Source MAC</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>Kunden Default MAC</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>Benutzer Quell-MAC</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>Kund. User MAC</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Verbindung überspringen</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Gestapeltes VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>Wieviele VLANs werden auf dem örtlichen Netzwerkport genutzt?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>Kein VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs (Gestapelte VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>VLAN-ID-Einstellungen eingeben:</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Stapeltiefe</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Benutzer</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Discovery Servereinstellungen</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>HINWEIS: Eine Link-Local Ziel IP kann nicht zum „Mit Gegenstelle verbinden“ verwendet werden</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>Globale Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Pinging</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Helfen Sie mir, die  &#xA;Ziel IP zu finden</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Lokaler Port</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status unbekannt</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>AKTIV (10/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>AKTIV (100/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>AKTIV (1000/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>AKTIV (10000/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>AKTIV (40000/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>AKTIV (100000/FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>AKTIV (10/HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>AKTIV (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>AKTIV (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Verbindungsverlust</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Lokaler Port:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto Negotiation</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analyse läuft</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>AUS</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Auto Negotiation:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>Warte auf Verbindung</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Verbinden...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Erneuter Versuch...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Verbunden</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>Verbindungsfehler</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Verbindung Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Übertragungskanal:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Discovery Server Fortgeschrittene Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Ziel IP von einem Discovery Server holen</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Server IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Server Port</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Server Passphrase</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Erwünschte Mietzeit (min.)</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Mietzeit Genehmigt (min.)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>L2 Netzwerkeinstellungen - örtlich</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Einstellungen der lokalen Einheit</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Traffic</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>LBM Datenverkehr</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 (Niedrig.)</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 (Höchste)</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>Anwender-SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Schleifentyp, Ether-Typ und MAC-Adressen setzen</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>MAC-Adressen, Ether-Typ und LBM setzen</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>MAC Adresses setzen</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>MAC Adressen und ARP Modus einstellen</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Zieltyp</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Unicast</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Multicast</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Broadcast</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>VLAN Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>Die Remoteeinheit ist nicht verbunden.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>L2 Netzwerkeinstellungen - fern</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Einstellungen der Remoteeinheit</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>L3 Netzwerkeinstellungen - örtlich</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Einstellen Lastklasse, Fluss-Label und Hop-Grenzwert</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>Welche Art von IP Prioritätssetzung wird von Ihrem Netzwerk genutzt?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Time to Live, IP ID Schritte und PPPoe-Modus setzen</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Lebensdauer und PPPoE Modus setzen</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Time to Live und IP ID Schritte setzen</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Lebensdauer setzen</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>Welche IP Prioritätssetzung wird von Ihrem Netzwerk genutzt?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>L3 Netzwerkeinstellungen - fern</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>L4 Netzwerkeinstellungen - örtlich</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Quellendiensttyp</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>DHCP-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>DHCP-Server</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Finger</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Ziel-Diensttyp</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>ATP Listen IP setzen</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>L4 Netzwerkeinstellungen - fern</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Schleifentyp und Sequenz-, Antwortsender- und Quellen-IDs setzen</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Fortgeschrittene L2 Einstellungen - Örtlich</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Quellentyp</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>Default MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>Benutzer-MAC</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>LBM-Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>LBM Typ</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Sender-TLV aktivieren</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Fortgeschrittene L2 Einstellungen - Fern</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Fortgeschrittene L3 Einstellungen - Örtlich</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Time To Live (Hops)</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Fortgeschrittene L3 Einstellungen - Fern</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Fortgeschrittene L4 Einstellungen - Örtlich</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Fortgeschrittene L4 Einstellungen - Fern</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Erweiterte Netzwerkeinstellungen - lokal</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>Vorlagen</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>Wollen Sie eine Konfigurierungsvorlage nutzen?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Viavi Erweiterte Paket Zugriffsrate > Transportrate</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - Beste Anstrengung</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - Kontinental</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - Global</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - Metro</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - Mobile Anbindung H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - Regional</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - VoIP Datenemulation</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC2544-Bit-transparent</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 Paket Zugriffsrate > Transport Rate</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 Paket Zugriffsrate = Transport Rate</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Vorlage anwenden</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Vorlagenkonfigurierungen:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>Durchsatz Frame Verlust (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Latenz Schwellwert (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>Grenzwert Paketjitter (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Frame Größen: Voreinstellung</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Maximum Bandweite (Upstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Maximum Bandweite (Downstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Maximum Bandweite: #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Tests: Durchsatz und Latenz Tests</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Tests: Durchsatz, Latenz, Frame Verlust und Aufeinanderfolgende</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Laufzeiten und Versuche: #1 Sekunden mit 1 Versuch</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Durchlauf Algorithmus: Viavi erweitert</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Durchsatz Algorithmus: RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Durchsatz Schwellwert (Upstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Durchsatz Schwellwert (Downstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Durchsatz Erfolgskriterium #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Frame Verlust Algorithmus: RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Aufeinanderfolgende Burst Zeit: 1 Sek</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Aufeinanderfolgende Granularität: 1 Frame</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Fortgeschrittene Nutzungseinstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Max. Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Downstream Max Bandweite (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Downstream Max Bandweite (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Downstream Max Bandweite (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Downstream Max Bandweite (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Downstream Max Bandweite (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Upstream Max Bandweite (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Upstream Max Bandweite (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Upstream Max Bandweite (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Upstream Max Bandweite (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Upstream Max Bandweite (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Max Bandweite (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Max Bandweite (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Max Bandweite (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Max Bandweite (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Max. Bandbreite (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>Gewählte Frames</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Längentyp</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paket</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Rahmenlänge</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Upstream&#xA;Frame Länge</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>Die Remoteeinheit ist nicht verbunden</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Downstream&#xA;Frame Länge</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>Die Remote-&#xA;einheit&#xA;ist nicht&#xA;verbunden</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Gewählte Tests</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>RFC 2544 Tests</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>Latenz (Durchsatz notwendig)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Buffer Credit (Vorraussetzung Durchsatztest)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Buffer Credit Durchsatz (Vorraussetzung Buffer Credit)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>Systemwiederherstellung (nur Loopback und benötigt Durchsatz)</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>WeitereTests</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Paket-Jitter (Vorraussetzung Durchsatztest)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Burst Test</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Ausgedehnte Last (nur Loopback)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Durchsatz Cfg</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Annäherungsmethode</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544-Standard</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi-optimiert</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Einstellen erweiterte Durchlaufmesseinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Downstream Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Downstream Messgenauigkeit (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Downstream Messgenauigkeit (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Downstream Messgenauigkeit (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Downstream Messgenauigkeit (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Downstream Messgenauigkeit (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>Innerhalb von 1.0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>Innerhalb von 0.1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>Innerhalb von 0.01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>Innerhalb von 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>Bis innerhalb 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>Bis innerhalb 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>Bis innerhalb 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>Bis innerhalb .4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>Bis innerhalb 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>Bis innerhalb 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>Bis innerhalb 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>Bis innerhalb 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>Bis innerhalb .1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>Bis innerhalb .01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>Bis innerhalb .001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>Bis innerhalb .0001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>Bis innerhalb 92.942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>Bis innerhalb 9.2942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>Bis innerhalb .9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>Bis innerhalb .0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>Bis innerhalb 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>Bis innerhalb 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>Bis innerhalb 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>Bis innerhalb 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>Bis innerhalb 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>Bis innerhalb .1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Upstream Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Upstream Messgenauigkeit (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Upstream Messgenauigkeit (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Upstream Messgenauigkeit (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Upstream Messgenauigkeit (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Upstream Messgenauigkeit (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Messgenauigkeit</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Messgenauigkeit (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Messgenauigkeit (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Messgenauigkeit (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Messgenauigkeit (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Messgenauigkeit (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Weitere Informationen</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Fehlersuche</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Inbetriebnahme</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Erweiterte Durchlaufeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Latenz konfigurieren / Datenpaket Flimmertestzeit separat</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Latenztestzeit separat konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Datenpaket Flimmertestzeit separat konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Frame Verlust Cfg</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Testverfahren</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Absteigend</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Aufsteigend</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Anzahl von Schritten</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Mbps pro Stufe</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 kbps pro Stufe</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % Leitungsrate pro Stufe</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>Downstream Testbereich (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>Downstream Testbereich (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>Downstream Testbereich (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>Downstream Testbereich (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Downstream Testbereich (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Downstream Bandbreiten Granularität (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Downstream Bandbreiten Granularität (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Downstream Bandbreiten Granularität (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Downstream Bandbreiten Granularität (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Downstream Bandbreiten Granularität (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>Upstream Testbereich (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>Upstream Testbereich (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>Upstream Testbereich (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>Upstream Testbereich (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Upstream Testbereich (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Upstream Bandbreiten Granularität (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Upstream Bandbreiten Granularität (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Upstream Bandbreiten Granularität (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Upstream Bandbreiten Granularität (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Upstream Bandbreiten Granularität (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Testumfange (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Testumfange (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Testbereich (L1 kb/s)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Testbereich (L2 kb/s)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Testbereich (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Bandbreiten Granularität (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Bandbreiten Granularität (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Bandbreiten Granularität (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Bandbreiten Granularität (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Bandbreiten Granularität (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Fortgeschrittene Frame Verlustmesseinstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Fortgeschrittene Frame-Verlusteinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Wahlweise Testmessungen</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Latenz messen</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Paket Jitter messen</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Aufeinanderfolgende Cfg</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Downstream Max Burst Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Upstream Max Burst Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Max Burst Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Burst Granularität (Rahmen)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Fortgeschrittene aufeinanderfolgende Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Richtlinien für PAUSE-Frame</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Zwischenspeicher Kredit Cfg</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flusskontrolle-Anmeldetyp</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Implizite (Transparente Verbindung)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explizit (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Max. Puffergröße</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Durchsatzschritte</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Teststeuerung</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>Testlaufzeit separat konfigurieren?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Laufzeit (en)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Anzahl der Versuche</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Paket-Jitter</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Latenz / Datenpaketflimmern</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Burst (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Alle Tests</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Erfolg/Fehl&#xA;anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Upstream&#xA;Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Downstream&#xA;Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Schwelle</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Durchsatzschwelle (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Durchsatzschwelle (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Durchsatzschwelle (L1 kb/s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Durchsatzschwelle (L2 kb/s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Durchsatz Erfolgskriterium (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>Die Remoteeinheit&#xA;ist nicht verbunden.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>Latenz RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>Latenz OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Paketjitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Burst Jagdgröße (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Burst (CBS Überwachung)</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Hochgenau-gering verzoegert</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Weniger genau-hochverzoegert</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>FC-Tests durchführen</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>FC-Tests überspringen</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>ein</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>aus</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Buffer Credit&#xA;Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Durchsatz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Upstream Durchsatz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Ergebnis Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Upstream Durchsatz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Downstream Durchsatz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Downstream Durchsatz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Durchsatz Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Upstream Durchsatz Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Downstream Durchsatz Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Durchsatzergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Durchsatz (kb/s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Durchsatz (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Rahmen 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Rahmen 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Rahmen 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Rahmen 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Rahmen 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Rahmen 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Rahmen 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Rahmen 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Rahmen 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Rahmen 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>Erfolg/ Fehl</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Rahmen-&#xA;länge&#xA;(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Paketlänge&#xA;(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>gemessene&#xA;Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>gemessene&#xA;Rate (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Gemessene&#xA;Rate (Frms/Sek)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>R_RDY&#xA;Erfassung</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Konf. Rate&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Gemessene L1&#xA;Rate (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Gemessene L1&#xA;Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>Gemessene L1&#xA;(% Leitungsrate)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Gemessene L2&#xA;Rate (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Gemessene L2&#xA;Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>Gemessene L2&#xA;(% Leitungsrate)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Gemessene L3&#xA;Rate (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Gemessene L3&#xA;Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>Gemessene L3&#xA;(% Leitungsrate)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Gemessene L4&#xA;Rate (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Gemessene L4&#xA;Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>Gemessene L4&#xA;(% Leitungsrate)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Gemessene Rate&#xA; (Frms/Sek)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Gemessene Rate&#xA; (Pkts/Sek)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Pause&#xA;Erfassung</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Cfg Rate&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Cfg Rate&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Cfg Rate&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Cfg Rate&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalien</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>OoS Frame(s)&#xA;Erfasst</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Acterna Nutzdaten&#xA;Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>FCS&#xA;Fehler Erfasst</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>IP Prüfsumme&#xA;Fehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>TCP/UDP Prüfsumme&#xA;Fehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Latenztest</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Latenztestdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Upstream Latenz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Latenztestergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Upstream Latenz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Downstream Latenz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Downstream Latenz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Latenz&#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Latenz&#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>Gemessene &#xA;% Leitungsrate</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Pause &#xA;Erkennen</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Rahmenverlust Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Upstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Downstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>Frame Verlust Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Rahmen 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>Konfigurierte Rate (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>Konfigurierte Rate (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Konfigurierte Rate (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Konfigurierte Rate (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Konfigurierte Rate (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Rahmenverlust (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Durchsatzrate&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Durchsatzrate&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Durchsatzrate&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Durchsatzrate&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Durchsatzrate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Rahmen- verlustrate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Frames Verloren</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Max Durchnittlicher Paket&#xA;Jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Fehler&#xA;Erfassung</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>OoS&#xA;Erfassung</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Konf. Rate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Back to Back Test Ergebnis</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Upstream Aufeinanderfolgende Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Downstream Aufeinanderfolgende Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Durchschnittliche&#xA;Burst Frames</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Durchschnittliche&#xA;Burst Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Buffer Credit Test Resultate</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Achtung</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>Minimum Zwischenspeichergröße&#xA;(Kredite)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Buffer Credit Durchsatz Test Resultate</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Zwischenspeicher Kredit Durchsatzergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Durchsatz (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Kred 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Kred 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Kred 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Kred 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Kred 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Kred 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Kred 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Kred 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Kred 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Kred 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Zwischenspeichergröße&#xA;(Kredite)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>gemessene Rate&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>gemessene Rate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Gemessene Rate&#xA;(Frames/Sek)</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>J-Proof - Ethernet L2-Transparenztest</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>J-Proof-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Test durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>J-Proof-Prüfung&#xA;ausführen</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Ende: Detailergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>J-Proof Test verlassen</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Anwenden</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Frame-Typen zum Testen des Service für die Transparenz von Schicht 2 konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Protokoll   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Protokoll</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Rahmentyp </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Verkapselung </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Gestapelt</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Rahmen&#xA;größe</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Rahmengröße</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Anzahl</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Rate&#xA;(Rmn/s)</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Rate</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Timeout&#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Zeitlimit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Control</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>DEI&#xA;Bit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>Benutzer-SVLAN-TPID</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>CPbit autom. erhöhen</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>CPbit autom. erhöhen</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>SPbit autom. erhöhen</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>SPbit autom. erhöhen</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>Verkaps.</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Verkaps.</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Rahmen&#xA;typ</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>PBit-Ink.</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Direkt&#xA;konfig</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Rahmen&#xA;hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Rahmen&#xA;entfernen</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>Quellenadresse gilt für alle Frames. Wird auf der Seite mit lokalen Einstellungen konfiguriert.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>PBit-Inkrement</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>VLAN-Stapel</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>SPbit-Inkrement</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Länge</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Daten</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>J-Proof-Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensität</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Schnell (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Voll (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Familie</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Alle</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Spanning-Tree</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Laser&#xA;Ein</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Laser&#xA;Aus</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Rahmensequenz&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Rahmensequenz&#xA;stoppen</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof Test</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>GESTOPPT</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>VORGANG LÄUFT</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Nutzlastfehler</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Header-Fehler</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Zählerabweichung</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Ergebnisbericht</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Inaktiv</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>Vorgang läuft</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Payload-Abweichung</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>Header-Abweichung</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>J-Proof-Ergebnisse</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Optischen Empfänger&#xA;zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>QuickCheck Test laufen lassen</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>QuickCheck Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>QuickCheck Erweiterte Lastergebnisse </translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Details</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Einzeln</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>Pro Datenstrom</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>Test-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Durchsatztest:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>QuickCheck-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Ausgedehnter Lasttest Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Zähler Tx-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Zähler Rx-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Zähler Rahmenfehler</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Zähler OoS-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Verlorener Frame Zähler</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Rahmenverlustrate</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Dauerhaft</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Aktiv</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Durchsatz: %1 Mbps (L1), Dauer: %2 Sekunden, Rahmengröße: %3 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Durchlauf: %1 kbps (L1), Zeitdauer: %2 Sekunden, Rahmengröße: %3 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>Nicht was Sie wollten?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Bei der Zielsuche</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>ARP Status:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Remoteschleife</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Aktive Schleife wird überprüft</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Suche nach Hardware Schleife</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Permanente Schleife wird überprüft</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Überprüfung LBM/LBR Schleife</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Aktive Schleife</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Permanente Schleife</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR-Schleife</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Fehler bei Schleife</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Remoteschleife:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Gemessener Durchsatz (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Nicht ermittelt</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Gemessener Durchlauf (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Siehe Fehler</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Last (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Ladung (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Durchlauf Testdauer (Sekunden)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Rahmengröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Bitrate</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbit/s</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Testdauer ausführen</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 Sekunde</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 Sekunde</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 Minute</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 Minuten</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 Minuten</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 Minuten</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 Stunde</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 Stunden</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 Stunden</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 Stunden</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Benutzerdefiniert</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Testdauer (Sek)</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Dauer der Leistungsprüfung (Minuten)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Rahmenlänge (Byte)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Benutzerlänge</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>Jumbo Länge</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>Elektrische Verbindungen: 10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Laser-Wellenlänge</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Elektrischer Anschluss</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Optischer Anschluss</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Details...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Link Active</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Traffic-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Fehleranzahl</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Fehlerhafte Rahmen</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Verlorene Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Interface-Details</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>SFP/XFP-Details</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Wellenlänge (nm): Einstellbares SFP, siehe Einrichtung zur Einstellung von Kanal/Wellenlänge</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Kabellänge (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Tuning unterstützt</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Wellenlänge</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Wellenlänge</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Kanal</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Empfohlene Raten&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Nennrate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Min. Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Max. Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Anbieter</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Anbieter PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Anbieter Vers.</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Leistungspegeltyp</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Diagnoseüberwachung</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Diagnosebyte</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>JMEP konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Wellenlänge (nm): Abstimmbares Gerät, Bezug auf Kanal/Wellenlänge Abstimmungseinstellung.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>CFP/QSFP-Details</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>CFP2/QSFP-Details</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>CFP4/QSFP Details</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>Anbieter SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Datumscode</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Chargencode</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW-/SW-Versionsnummer</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA-HW-Spez. Rev-Nr.</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA-Verwaltung I/F Rev-Nr.</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Leistungsklasse</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx Leistungspegeltyp</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Rx Max Lambda Strom (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Tx Max Lambda Strom (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>Anzahl aktiver Fasern</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Wellenlängen pro Faser </translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Diagnosebyte</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL pro Faserbereich (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Max. Netzspur-Bitrate (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Modul ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Unterstützte Raten</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Nominelle Wellenlänge (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Nennbitrate (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Empfohlen für 100GigE RS-FEC Anwendungen ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>CFP Experte</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>CFP2 Experte</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>CFP4 Experte</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Viavi Loopback aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>CFP Viavi Loopback aktivieren</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>CFP-Expert-Modus aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>CFP2-Expert-Modus aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>CFP4 Expertenmodus aktivieren</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Pre-Emphasis</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>CFP Tx Präemphasis</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Default</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Niedrig</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Nominell</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Hoch</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Clock Divider</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>CFP Tx Takt-Frequenzteiler</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Versatz Offset (Byte)</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>CFP Tx Skew Offset</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Polarität invertieren</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP Tx Polarität invertieren</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Tx FIFO zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Ausgleich</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>CFP Rx Entzerrung</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP Rx umpolen</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>LOS ignorieren</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP Rx ignoriere LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Rx FIFO zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>QSFP Experte</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>QSFP-Expert-Modus aktivieren</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx Ignoriere LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>CDR Bypass</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Empfangen</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR Empfangs-Bypass</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Übertragen</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR Sende-Bypass</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>CDR Sende- und Empfangs-Bypass wäre verfügbar wenn das QSFP-Modul dies unterstützen würde.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>Ingenieurwesen</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR Core Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>CFP XCVR Kern Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>CFP XCVR Leitung Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Modul Host-Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>CFP Modul Host Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Modul Netzwerk-Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>CFP Modul Netzwerk Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>CFP Gearbox System Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>CFP Gearbox Leitung Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>Getriebe Chip ID</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Getriebe Chip Umdrehungen</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Getriebe Ucode-Ver</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>Getriebe Ucode-CRC</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Getriebe Systemspuren</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Getriebe Linienspuren</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>CFPn Host-Spuren</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>CFPn Netzwerkspuren</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR Kern-Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR Line-Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO Peek DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO Peek PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO Poke DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO Poke PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO Poke Wert</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Register A013 Steuerungen pro-Spur Laser aktiv/inaktiv.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C Peek PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C Peek DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C Poke PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C Poke DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C Poke Wert</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Register 0x56 controls pro Spur laser ein/aus.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+ Einzelheiten</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Signal</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Tx Signaltakt</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Taktquelle</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Intern</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Abgeleitet</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Extern</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>Extern 1.5M</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>Extern 2M</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>Extern 10M</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Remote wiederhergestellt</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Timing-Modul</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>VC-12-Quelle</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Interner Frequenzversatz (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Externe Taktung</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Einstellbares Gerät</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Tuningmodus</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Frequenz</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Frequenz (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Erste einstellbare Frequenz (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Letzte einstellbare Frequenz (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Rasterabstand (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>Schiefe-Alarm</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Schiefe-Alarm Schwellwert (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Resync erforderlich</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Resync fertiggestellt</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>RS-FEC-Kalibreriung erfüllt</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>RS-FEC-Kalibrierung fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC wird normalerweise mit SR4, PSM4, CWDM4 verwendet.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Führen Sie zur PS-FEC-Kalibrierung folgendes aus (gilt auch für CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Führen Sie einen QSFP28-Adapter in das CSAM ein</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Führen Sie einen QSFP28-Transreceiver in den Adapter ein</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Führen Sie ein Fiber-Loopback-Gerät in den Transceiver ein</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Klicken Sie auf Kalibrieren</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Kalibrieren</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Kalibriere...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resync</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Der Transreceiver muss nun mit dem Gerät unter Test (DUT) neu synchronisiert werden.</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Entfernen Sie das Fiber-Loopback-Gerät vom Transreceiver</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Bauen Sie eine Verbindung zum Gerät unter Test auf (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Schalten Sie den DUT-Laser AN</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Bestätigen Sie, dass das Signal vorhanden LED auf Ihrem CSAM grün leuchtet</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Klicken Sie auf Lane Resync</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Lane&#xA;Resync</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>J-QuickCheck-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Klicken Sie auf die Schaltfläche 'Start', um mit der J-QuickCheck-Prüfung zu beginnen.</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Prüfung wird im Halbduplexmodus fortgesetzt.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Datenanbindung wird in Upstream-Richtung geprüft.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Datenanbindung wird in Downstream-Richtung geprüft.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>Die Datenanbindung wurde für alle Services erfolgreich geprüft.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>Bei der Prüfung der Datenanbindung in Upstream-Richtung traten bei den folgenden Services Fehler auf: #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>Bei der Prüfung der Datenanbindung in Downstream-Richtung traten bei den folgenden Services Fehler auf: #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>Bei der Prüfung der Datenanbindung in Upstream-Richtung traten bei den folgenden Services Fehler auf: #1. In Downstream-Richtung traten Fehler für die folgenden Services auf: #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>Bei der Prüfung der Datenanbindung in Upstream-Richtung sind Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>Bei der Prüfung der Datenanbindung in Downstream-Richtung sind Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>Bei der Prüfung der Datenanbindung in Upstream- bzw. Downstream-Richtung sind Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Konnektivität wird geprüft</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Datenanbindung:</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>QC starten</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>QC stoppen</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Optik Selbsttest</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Ende: Profile speichern</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Berichte</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Optik</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Selbsttest Optik beenden</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Das optische Signal wurde verloren. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Niedriger Stromlevel erfasst. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Hohen Stromlevel erfasst. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>Ein Fehler wurde erkannt. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>Der Test hat FAILED (versagt), weil übermäßige Schiefe erfasst wurde.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>Der Test hat FAILED  (versagt), weil übermäßige Schiefe erfasst wurde. Der Test wurde abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>Ein Bitfehler wurde erfasst. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>Der Test ist FEHLGESCHLAGEN, da der BER die konfigurierte Schwelle überschritten hat.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>Der Test war NICHT ERFOLGREICH aufgrund Verlust von Raster Sync.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Eingefügte QSFP+ kann nicht gelesen werden.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Eingefügte CFP kann nicht gelesen werden.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Eingefügte CFP2 kann nicht gelesen werden.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>Kann eingesetzten CFP4 nicht lesen.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>Es wurde ein RS-FEC korrigierbarer Bitfehler bemerkt. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>Es wurde ein nicht RS-FEC korrigierbarer Bitfehler bemerkt. Der Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>RS-FEC-Kalibrierung fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Eingabefrequenz Abweichungsfehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Output Frequenz Abweichungsfehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Synchronisierung verloren</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Code Verstoß entdeckt</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Einstellungsanzeigerschloss verloren</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Ungültige Anpassungskennzeichen erfasst</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>BIP 8 AM Bit Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>BIP Block Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>Schiefe erfasst</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Block Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>Untergröße Frames erfasst</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Fernfehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 Bit-Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Schema Sync verloren</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>BER Schwellwert konnte wegen Schema Sync Verlust nicht genau gemessen werden</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>Der gemessene BER überschreitet den gewählten BER Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS erfasst</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>FAS Fehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Außerhalb des Frame erfasst</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Verlust eines Frame erfasst</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Frame Sync verloren</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Außerhalb des logischen Bahnkennzeichens erfasst</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Logischer Bahnkennzeichen Fehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Verlust einer Bahnanpassung erfasst</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Bahnanpassung verloren</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>MFAS Fehler erfasst</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Außerhalb der Bahnanpassung erfasst</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS erfasst</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Außerhalb der Wiederherstellung erfasst</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Übermäßige Schiefe Erfasst</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>RS-FEC-Kalibreriung erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Optiktest</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Testtyp</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>Test utilizes 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Optikart</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Gesamttest Beschluss</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Signal Präsenz Test</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Optischer Signal Level Test</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Übermäßiger Schiefe-Test</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Bit Fehlertest</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Genereller Fehlertest</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>BER Schwellwerttest</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Optische Leistung</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Rx Pegel Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Rx Pegel Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Rx Pegel Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Rx Pegel Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Rx Pegel Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Rx Pegel Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Rx Pegel Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Rx Pegel Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Rx Pegel Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Rx Pegel Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Rx Pegel Sum (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Tx Pegel Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Tx Pegel Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Tx Pegel Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Tx Pegel Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Tx Pegel Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Tx Pegel Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Tx Pegel Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Tx Pegel Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Tx Pegel Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Tx Pegel Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Tx Pegel Sum (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Verbinden Sie die Tx und Rx Terminals des Anschlusses, den Sie prüfen wollen, mit einem kurzen, sauberen Patch-Kabel.</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Test CFP&#xA;Optik</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Test CFP2&#xA;Optisch/Buchse</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Test abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Test QSFP+&#xA;Optik</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Teste QSFP28&#xA;Optik</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Empfohlene</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 Minuten</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 Minuten</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 Stunden</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 Stunden</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Nutzer Laufzeit (Minuten)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Empfohlene Dauer (Minuten)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>Messdauer Empfohlen Die empf. Zeit für diese Konfig. &lt;b>%1&lt;/b> Min.</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>BER Schwellwert Typ</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Post-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pre-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>BER Schwellwert</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>PPM Leitungs-Offset aktivieren</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>PPM Max Offset (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Bei Fehler anhalten</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Übersicht der Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Optischer/Slot-Typ</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Aktueller PPM Offset</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>Aktuell BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Optische Leistung (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Rx Pegel Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx Pegel Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx Pegel Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx Pegel Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx Pegel Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx Pegel Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx Pegel Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx Pegel Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx Pegel Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx Pegel Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Rx Pegel Sum</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx Pegel Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx Pegel Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx Pegel Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx Pegel Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx Pegel Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx Pegel Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx Pegel Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx Pegel Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx Pegel Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx Pegel Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Tx Pegel Sum</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>CFP-Schnittstellendetails</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>CFP2-Schnittstellendetails</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>CFP4 Interface Details</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>QSFP-Schnittstellendetails</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+ Interface Details</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>QSFP28 Interface Details</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>Kein QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>QSFP konnte nicht gelesen werden, bitte schließen Sie das QSFP erneut an</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP Fehler Prüfsumme</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Benötigte QSFP Register können nicht abgefragt werden.</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>QSFP Identität kann nicht bestätigt werden.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN-Check</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Dauer- und Nutzlast-Test für die Bitfehlerrate (BERT)</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>GCC Transparenz</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Prüfungen auswählen und durchführen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Erweiterte Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>OTN Check-Tests starten</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>BERT Nutzlast</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>OTN-Check Test beenden</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Messfrequenz</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Status Auto Negotiation</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>RTD-Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Alle Lanes</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN Check:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Beginne OTN Check-Test  ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>BERT Nutzlast Bitfehler erkannt</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Mehr als 100.000 BERT Nutzlast Bitfehler erkannt</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>BERT Nutzlast BER Schwelle überschritten</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 BERT Nutzlast Bitfehler erkannt</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>BERT Nutzlast Bitfehlerrate #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>RTD (Bit-Fehlerrate) Grenzwert überschritten</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Min: #3, Max: #4, Durchschn.: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>BERT Test wird ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>RTD-Test wird ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>GCC Transparenztest wird ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>GCC Bit-Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>GCC Grenzwert Bitfehlerrate (BER) überschritten</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 GCC Bit-Fehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Mehr als 100.000 GCC-Bitfehler entdeckt</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>GCC Bit-Fehlerrate: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** Starte Loopback-Überprüfung ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** Loopback-Überprüfung Überspringen ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** Loopback-Überprüfung Beendet ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Loopback erkannt</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>Kein Loopback erkannt</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>Kanal #1 ist nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>Kanal #1 ist jetzt verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Verlust der GCC Mustersynchronisation</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Verlust der GCC Mustersynchronisation</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>Test abgebrochen: Ein Bit-Fehler wurde entdeckt.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Test fehlgeschlagen: Die BER hat den konfigurierten Grenzwert überschritten</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Test fehlgeschlagen: Die RTD hat den konfigurierten Grenzwert überschritten</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Test abgebrochen: Kein Loopback erkannt.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Test abgebrochen: Keine optisches Signal vorhanden </translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Test abgebrochen: Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Test abgebrochen: Ausfall der Rahmensynchronisierung</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Test abgebrochen: Ausfall der Mustersynchronisierung</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Test abgebrochen: Ausfall der GCC Mustersynchronisierung</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Test abgebrochen: Ausfall von Lane Alignment</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Test abgebrochen: Ausfall von Marker Lock</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Mindestens 1 Kanal muss ausgewählt werden.</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>Loopback nicht überprüft</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>Loopback nicht überprüft</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Testauswahl</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Geplante Dauer des Tests</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Testdauer</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>OTN-Check erfordert einen Datenverkehr-Loopback für die Ausführung; dieser Loopback ist am anderen Ende der OTN-Schaltung notwendig</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>OTN Check-Tests</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Optisches Offset, Signalmapping</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Signalmapping</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Signalmapping und Optische Auswahl</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Fortgeschrittene Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Signalstruktur</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP Optisches RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP Optisches RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 Optisches RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>CFP4 Optics RTD Versatz (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Konfiguriere Dauer- und BERT Nutzlast Test</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Dauer und Nutzlast für die Bitfehlerrate Cfg</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>BERT Nutzlast Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Konfidenznivau (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>Basierend auf der Datenübertragungsrate, dem Grenzwert der Bitfehlerrate (BER) und dem Konfidenzniveau, beträgt die empfohlene Testzeit &lt;b>%1&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Muster</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>BERT-Muster</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Fehlerschwelle</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Bestanden/Nicht bestanden zeigen</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Einrichtung der Umlaufzeit-Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>RTD konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Enthalten</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Grenzwert (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Gemessene Frequenz</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>GCC konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Einrichtung GCC Transparenz</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>GCC-Kanal</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>GCC Grenzwert Bitfehlerrate (BER)</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>BERT (Bit-Fehlerratentest) Muster 2^23-1 wird in GCC verwendet</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Loopback-Check</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Loopback-Überprüfung überspringen</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Drücken Sie die "Start"-Taste zum Start der Loopback-Überprüfung</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>OTN Check Tests überspringen</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Loopback-Überprüfung</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Loopback erkannt</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Loopback-Überprüfung überspringen</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>Loopback-Erkennung</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>Loopback-Erkennung</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>BERT Nutzlast Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>Gemessene BER</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Bit-Fehlerzähler</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Urteil</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Konfiguration der BERT Nutzlast</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Ergebnisse der Umlaufzeit-Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Min. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Max (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Durchschnitt (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>HINWEIS: Fehler tritt auf, wenn der durchschnittliche RTD den Grenzwert überschreitet.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Konfiguration Round-Trip-Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Ergebnisse GCC Transparenz</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>GCC BERT Bitfehlerrate</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>GCC BERT Bitfehler</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Konfiguration GCC Transparenz</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Protokollanalyse</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Erfassen</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>CDP erfassen und analysieren</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Erfassen und analysieren</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Protokoll für die Analyse auswählen</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Analyse&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Analyse&#xA;abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>Expert PTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Testeinstellungen manuell konfigurieren </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Schwellenwerte</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Schnellüberprüfung ausführen</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Expert PTP-Test beenden</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>Link ist nicht mehr aktiv.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>Quellensynchronisation der einseitigen Zeitverzögerung ist verloren gegangen</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>Die PTP Slave-Session wurde unerwartet abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>Zeit der Quellensynchronisation ist nicht vorhanden. Bitte prüfen Sie, dass Ihre Zeitquelle richtig konfiguriert und angeschlossen ist.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>Kann keine PTP Slave-Session aufbauen</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>Kein GPS-Empfänger erkannt.</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>TOS Typ</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Timeout für Empfang von Ansage</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Ansage</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Synchronisierung</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Verzögerungsanforderung</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Leasedauer (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Zeit Fehler Max. Schwellwert gesetzt</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Zeitfehler Max. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Zeitfehler Max. Grenzwert (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Testdauer (Minuten)</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Schnellüberprüfung</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>Master IP</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>PTP Domain</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Start&#xA;Überprüfung</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Sitzung&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Sitzung&#xA;aufgebaut</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Maximaler Zeitfehlergrenzwert (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Maximaler Zeitfehler (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Zeitfehler, Aktuell (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Zeitfehler, Aktuell (us) vs. Zeit</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Testergebnisse </translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Dauer (Minuten)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP-Check</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>PTP Check-Test</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Ende: PTP-Überprüfung</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Anderen Test starten</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>PTP-Überprüfung beenden</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>Erweitertes RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Ungültige Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Fortgeschrittene IP Einstellungen - Örtlich</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Fortgeschrittene RTD Latenzeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Fortgeschrittene Burst (CBS) Testeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck ausführen</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>J-QuickCheck-Einstell.</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Jitter</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Ausgedehnte Last</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>RFC 2544 Test verlassen</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Örtliche Netzwerk Konfigurierung</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Fernnetzwerk Konfigurierung</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Örtlicher Auto Verhandlungsstatus</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Geschwindigkeit (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Flusskontrolle</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX-fähig</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX-fähig</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Pausenfähig</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Fern-Auto-Verhandlungsstatus</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Halb</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Voll</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx und Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Nur Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Nur Rx</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>RTD Frame Rate</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 Frame pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 Frames pro Sekunde</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Burst Cfg</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Verbindliche Burst Größe</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>CBS Kontolle (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Burst Jagd</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>Downstream CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Downstream Burst Größen (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimum</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Maximum</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>Upstream CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Upstream Burst Größen (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Burstgrößen (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Fortgeschrittene CBS Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Fortgeschrittene CBS Kontrolleinstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Fortgeschrittene Burst Jagd Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Toleranz</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Ausgedehnte Last Cfg</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Durchsatzskalierung (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 hat die folgenden ungültigen Konfigurationseinstellungen:</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>Die Verkehrverbindung wurde erfolgreich geprüft.  Der Lasttest läuft.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>"Start" drücken, um zur Leitungsrate laufen zu lassen.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>"Start" drücken, um bei der konfigurierte RFC 2544 Bandweite laufen zu lassen.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>Gemessener Durchsatz wird NICHT für RFC 2544 Tests benutzt.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>Gemessener Durchsatz WIRD für RFC 2544 Tests benutzt.</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Lasttest Frame Größe: %1 Bytes.</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Lasttest-Paketgröße: %1 Bytes.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Upstream Lasttest Frame Größe: %1 Bytes.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Downstream Lasttest Frame Größe: %1 Bytes.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Gemessener Durchsatz:</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Test mit konfigurierter RFC 2544 Max Bandweite</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Die gemessene Durchsatzmessung als RFC 2544 Max Bandweite nutzen</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Lasttest Frame Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Lasttest-Paketgröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Upstream Lasttest Frame Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Downstream Lasttest Frame Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>RFC 2544-Tests ausführen</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>RFC 2544 Tests überscpringen</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Überprüfung der Hardwareschleife</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Paketjittertest</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Upstream Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Upstream Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Downstream Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Downstream Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Max. Durchschn. Flimmern (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Max Durchschn. Jitter</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Max Durchschn.&#xA;Jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Upstream CBS Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Downstream CBS Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Burst Jagdtestergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Upstream Burst Jagd Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Downstream Burst Jagd Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS Kontolle Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Upstream CBS Kontroll-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Downstream CBS Kontroll-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Burst (CBS Überwachung) Test</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Cfg Burst&#xA;Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Tx Burst&#xA;Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Durch-&#xA;schnitt. Rx Burst&#xA;Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Frames&#xA;Gesandt</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Rahmen&#xA;Erhalten</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Rahmen&#xA;Verloren</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Signalgröße&#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Latenz&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Jitter&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Konfigurierte&#xA;Burst-Größe (kb)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Tx Burst&#xA;Überwachungsgröße (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>Geschätzte&#xA;CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Upstream Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Prüfergebnisse Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Upstream Systemwiederherstellung Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Downstream Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Downstream Systemwiederherstellung Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Erholungszeit (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Überlastungsrate&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Überlastungsrate&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Überlastungsrate&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Überlastungsrate&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Overload-Rate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Wiederher- stellungsrate&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Wiederher- stellungsrate&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Wiederher- stellungsrate&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Wiederher- stellungsrate&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>System Erholungsrate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Durchschnittliche Wieder-&#xA;herstellungs Zeit (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modus</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Steuerungen</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>TrueSpeed-Steuerung</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Formen</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Schrittkonfig</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Schritte auswählen</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Pfad MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>Erweiterte TCP</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Verbindungseinstellungen</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed Kontrolle (Fortgeschrittene)</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Bc Einheiten auswählen</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Wanderfenster</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>TrueSpeed Test verlassen</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>TCP-Host konnte keine Verbindung herstellen. Der aktuelle Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>Durch die Auswahl des Troubleshoot-Modus wurden sie von der fernen Einheit getrennt.</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Ungültige Testauswahl: Damit TrueSpeed ausgeführt wird, muss mindestens ein Test ausgewählt sein.</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Die gemessene MTU ist zu klein, um fortzufahren. Test abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>Die Datei wurde zu schnell übertragen! Wählen Sie eine Dateigröße für die TCP-Übertragung, sodass der Test mindestens 5 Sekunden dauert. Es wird allerdings eine Testzeit von mindestens 10 Sekunden empfohlen.</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>Maximale Anzahl erneuter Sendeversuche erreicht. Test abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>TCP-Host hat einen Fehler festgestellt. Der aktuelle Test wurde abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>TrueSpeed-Prüfung wird gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Einnullen der MTU-Größe.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>#1 Byte MTU mit #2 Byte Mss werden getestet.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>Die Pfad-MTU wurde als #1 Byte ermittelt. Dies entspricht einem MSS von #2 Byte.</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>RTT-Test wird ausgeführt. Der Test wird #1 Sekunden dauern.</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>Die RTT (Round Trip Time, Umlaufzeit) wurde als #1 ms gemessen.</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Upstream-Walk-the-Window-Test wird ausgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Downstream-Walk-the-Window-Test wird ausgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Senden von #1 Bytes an TCP-Traffic.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Lokaler Ausgangstraffic: Ungeprägt</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Entfernter Ausgangstraffic: Ungeprägt</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Lokaler Ausgangstraffic: Geprägt</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Entfernter Ausgangstraffic: Geprägt</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Dauer (Sek): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Verbindungen: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Fenstergröße (kB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Upstream-TCP-Durchsatztest wird ausgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Downstream-TCP-Durchsatztest wird ausgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Durchführung erweiterter TCP-Test. Dies Dauert #1 Sekunden.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Typ der lokalen IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Lokale IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Lokales Standard-Gateway</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Lokale Subnetzmaske</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Lokale Verkapselung</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Ferne IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Modus auswählen</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>Welchen Testtyp führen Sie aus?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>Ich &lt;b>installiere einen&lt;/b> oder &lt;b>wechsle zu einem&lt;/b> neuen Kreislauf.*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Ich &lt;b>suche Fehler&lt;/b> in einem vorhandenen Kreislauf.</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>*Hierfür ist ein fernes MTS/T-BERD Testinstrument erforderlich</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>Wird wird Ihr Durchsatz konfiguriert?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>Meine Downstream- und Upstream-Durchsätze sind &lt;b>identisch&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>Meine Downstream- und Upstream-Durchsätze sind &lt;b>abweichend&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Engpassbandbreite beim Laden der TrueSpeed-Konfiguration so festlegen, dass sie der&#xA;SAMComplete CIR entspricht.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Engpassbandbreite beim Laden der TrueSpeed-Konfiguration so festlegen, dass sie der max.&#xA;RFC 2544-Bandbreite entspricht.</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Iperf-Server</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>T-BERD/MTS-Testinstrument</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>IP Typ</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Ferne Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>TCP-Host-Servereinstellungen</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Dieser Schritt konfiguriert globale Einstellungen für alle folgenden TrueSpeed-Schritte. Dazu gehören CIR (Committed Information Rate) und TCP Pass %, was den CIR-Prozentsatz darstellt, der zum Bestehen des Durchsatztests erforderlich ist.</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Walk-the-Fenster Test laufen lassen</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Gesamte Testzeit (s)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>MTU-Größe automatisch suchen</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>MTU-Größe (Byte)</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>Lokale VLAN-ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Priorität</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Lokale Priorität</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>Lokale TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>Lokale DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>Downstream CIR (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>Upstream CIR (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>Ferner&#xA;TCP-Host</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>Ferne VLAN-ID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Ferne Priorität</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>Remote TOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>Remote DSCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Fortgeschrittene TrueSpeed Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Bc Einheit auswählen</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Traffic Shaping</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Bc Einheit</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Prägeprofil</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>Lokaler und entfernter Ausgangstraffic geprägt</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Nur lokaler Ausgangstraffic wird geprägt</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Nur entfernter Ausgangstraffic wird geprägt</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Weder lokaler, noch entfernter Ausgangstraffic geprägt</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Traffic-Prägung (Versuchen Sie nur die Fenster- und Durchsatztests)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Die Tests verlaufen ungeprägt, dann geprägt</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) Lokal</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) Lokal</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) Lokal</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) Lokal</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) Lokal</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) Entfernt</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (Entfernt)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) Remote</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) Entfernt</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) Remote</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) Entfernt</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>Wollen Sie den TCP-Traffic prägen?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Ungeprägten Traffic</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Geprägter Traffic</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Den Test ungeprägt durchführen</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>DANN</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Den Test geprägt durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Den Test geprägt auf lokal durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Mit Prägung auf lokal und entfernt durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Komplett ohne Prägung durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Mit Prägung auf lokal und komplett ohne Prägung auf entfernt durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Mit Prägung auf lokal durchführen</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Komplett ohne Prägung auf lokal und mit Prägung auf entfernt durchführen</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Lokalen Traffic prägen</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>Entfernte Einheit ist&#xA;nicht verbunden</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Entfernten Traffic prägen</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Zusätzliche Prägungsoptionen anzeigen</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>TrueSpeed-Steuerungen (Erweitert)</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Mit Port verbinden</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>TCP Pass %</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>MTU Obere Begrenzung (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Mehrere Verbindungen nutzen</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Als SLA Forderung einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Erhoehtes Fenster [%]</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Erhoehte Verbindungen [%]</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Schrittkonfiguration</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>TrueSpeed-Schritte</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>Sie müssen mindestens einen Schritt auswählen, um den Test auszuführen.</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>Dieser Schritt verwendet die in RFC 4821 definierte Prozedur für die automatische Ermittlung der MTU des End-to-End-Netzwerkpfads. Der Tester des TCP-Clients versucht, TCP-Segmente unterschiedlicher Paketgröße zu senden und die MTU ohne die Notwendigkeit von ICMP (für die traditionelle Pfad-MTU-Erkennung erforderlich) zu ermitteln. </translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>In diesem Schritt wird eine TCP-Übertragung mit geringer Intensität durchgeführt und ein Bericht zur Basis-Umlaufzeit zurückgegeben, auf dessen Grundlage die Ergebnisse der folgenden Prüfschritte berechnet werden. Die Basis-Umlaufzeit ist die inhärente Latenz des Netzwerks, bei der andere Verzögerungen, die auf der Grundlage von Netzwerküberlastungen zustande kommen, nicht berücksichtigt werden.</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Dauer (Sek.)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>Dieser Schritt führt einen TCP-Fensterscan durch und gibt TCP-Durchsatzergebnisse für bis zu 4 TCP-Fenstergröße-/Verbindungskombiniationen zurück. Der Schritt informiert ebenfalls für jede Fenstergröße über den aktuellen TCP-Durchsatz vs. den geschätzten.</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Window Sizes</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Fenstergröße 1 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation>Anzahl der Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Verbindungen Fenster 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Fenstergröße 2 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Verbindungen Fenster 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Fenstergröße 3 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Verbindungen Fenster 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Fenstergröße 4 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Verbindungen Fenster 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Max. Segmentgröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Verwendet die im &#xA;Pfad-MTU-Schritt gefundene MSS </translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Max. Segmentgröße (Byte)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>Basierend auf der Bandweitenverbindung von &lt;b>%1&lt;/b> Mbps und einem RTT von &lt;b>%2&lt;/b> ms, wäre das ideale TCP Fenster  &lt;b>%3&lt;/b> Bytes. Mit &lt;b>%4&lt;/b> Verbindung(en) und einer &lt;b>%5&lt;/b> Byte TCP Fenstergröße für jede Verbindung wäre der ungefähre ideale Weiterleitungsdurchlauf &lt;b>%6&lt;/b> kbps und eine über jede Verbindung weitergeleitete &lt;b>%7&lt;/b> MB Datei sollte &lt;b>%8&lt;/b> Sekunden brauchen.</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>Basierend auf der Bandweitenverbindung von &lt;b>%1&lt;/b> Mbps und einem RTT von &lt;b>%2&lt;/b> ms, wäre das ideale TCP Fenster &lt;b>%3&lt;/b> Bytes. &lt;font color="red"> Mit &lt;b>%4&lt;/b> Verbindung(en) und einer &lt;b>%5&lt;/b> Byte TCP Fenstergröße für jede Verbindung ist die Kapazität der Verbindung überschritten.  Die eigentlichen Ergebnisse könnten schlechter sein als die erwarteten Ergebnisse wegen Paketverlust und weiteren Verzögerungen. Reduzieren Sie die Anzahl der Verbindungen und/oder die TCP Fenstergröße, um einen Test innerhalb der Kapazität der Verbindung durchzuführen. &lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Fenstergröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Dateigröße pro Verbindung (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Dateigröße für 30-sekündige Übertragung automatisch suchen</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Anzahl der Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Verwendet die im&#xA;RTT-Schritt gefundene&#xA;RTT (%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Verwendet das im&#xA;Pfad-MTU-Schritt gefundene&#xA;MSS (%1 Byte)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>In diesem Schritt werden mehrere TCP-Verbindungsübertragungen durchgeführt, um zu ermitteln, ob die Leitung die Bandbreite gleichmäßig (Traffic-Shaping) oder ungleichmäßig (Übertragungsrichtlinien) aufteilt.</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>In diesem Schritt werden mehrere TCP-Verbindungsübertragungen durchgeführt, um zu ermitteln, ob die Leitung die Bandbreite gleichmäßig (Traffic-Shaping) oder ungleichmäßig (Übertragungsrichtlinien) aufteilt. Führen Sie den Umlaufzeit-Schritt aus, damit die Fenstergröße und die Anzahl an Verbindungen automatisch ermittelt werden kann.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Fenstergröße (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Automatische Berechnung bei der Ausführung des &#xA;RTT-Schritts</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 bytes</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>TrueSpeed-Tests durchführen</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>TrueSpeed-Tests überspringen</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Maximale Übertragungseinheit (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Maximale Segmentgröße (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Dieser Schritt hat ermittelt, dass die maximale Übertragungseinheit (MTU) für diesen Link (End-to-End) &lt;b>%1&lt;/b> Byte hat. Dieser Wert minus der Layer 3/4-Auslastung wird für nachfolgende Schritte als Größe für die maximale Segmentgröße (MSS) verwendet. In diesem Fall ist MSS &lt;b>%2&lt;/b> Byte.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Zeit (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Ergebniszusammenfassung Umlaufzeit</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>Durchschnitt. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>Min. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>Max. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Die RTT (Round Trip Time, Umlaufzeit) wurde als &lt;b>%1&lt;/b> ms gemessen. Die minimale RTT wird verwendet, da sie der inhärenten Latenz des Netzwerks am nächsten kommt. Nachfolgende Schritte verwenden sie als Basis für die geschätzte TCP-Leistung.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Die Round Trip Time (RTT) wurde auf %1 msec gemessen. Die durchschnittliche (Basis) RTT wird verwendet, da sie am nächsten die inhärente Netzwerkwartezeit repräsentiert. Die nachfolgenden Schritte verwenden sie als Basis für die Erwartung der TCP Leistung.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Upstream Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Downstream Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>Upstream Fenster 1 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>Downstream Fenster 1 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>Upstream Fenster 2 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>Downstream Fenster 2 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>Upstream Fenster 3 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>Downstream Fenster 3 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>Upstream Fenster 4 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>Downstream Fenster 4 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>Upstream Fenster 5 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>Downstream Fenster 5 Größe (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Upstream Fenster 1 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Downstream Fenster 1 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Upstream Fenster 2 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Downstream Fenster 2 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Upstream Fenster 3 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Downstream Fenster 3 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Upstream Fenster 4 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Downstream Fenster 4 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Upstream Fenster 5 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Downstream Fenster 5 Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Upstream Fenster 1 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Vorschaltung Fenster 1 tatsächlich ungeprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Vorschaltung Fenster 1 tatsächlich geprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Upstream Fenster 1 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Vorschaltung Fenster 2 Ist (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Vorschaltung Fenster 2 tatsächlich ungeprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Vorschaltung Fenster 2 tatsächlich geprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Vorschaltung Fenster 2 Vorhergesagt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Upstream Fenster 3 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Upstream Fenster 3 Aktuell Ungeformt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Upstream Fenster 3 Aktuell Geformt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Upstream Fenster 3 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Upstream Fenster 4 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Upstream Window 4 aktuell unshaped  (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Upstream Window 4 aktuell shaped (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Upstream Fenster 4 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Upstream Fenster 5 Aktuell (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Upstream Window 5 aktuell unshaped  (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Upstream Window 5 aktuell shaped (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Upstream Fenster 5 Erwartet (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Downstream Fenster 1 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Nachschaltung Fenster 1 tatsächlich geprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Downstream Fenster 1 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Nachschaltung Fenster 2 Ist (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Nachschaltung Fenster 2 tatsächlich geprägt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Nachschaltung Fenster 2 Vorhergesagt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Downstream Fenster 3 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Downstream Fenster 3 Aktuell Geformt (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Downstream Fenster 3 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Downstream Fenster 4 tatsächlich (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Downstream Fenster 4 tatsächliche Form (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Downstream Fenster 4 geschätzt (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Downstream Fenster 5 Aktuell (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Downstream Window 5 aktuell shaped (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Downstream Fenster 5 Prognostiziert (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbit/s, Mittl..</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Fenster 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Wirklich</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>Tatsächlich ungeprägt</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Tatsächlich geprägt</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Ideal</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Fenster 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Fenster 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Fenster 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Fenster 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>Die Ergebnisse des TCP-Walk-the-Window-Schritts zeigen den tatsächlichen Durchsatz im Vergleich mit dem idealen Durchsatz für jede getestete Fenstergröße/Verbindung. Ist der aktuelle Durchsatz geringer als der ideale, kann dies durch Verluste oder Überlastung verursacht worden sein. Ist der aktuelle Durchsatz höher als der ideale, wurde die als Basis verwendete RTT zu hoch angesetzt. Der TCP-Durchsatzschritt stellt eine tiefere Analyse der TCP-Übertragungen bereit.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Upstream TCP-Durchsatz tatsächlich vs. Ideal</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Upstream TCP-Durchsatzdiagramme</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Vorschaltung TCP erneute Übertragung Durchsatz Grafiken</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Vorschaltung TCP Durchsatz RTT-Grafiken</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Downstream TCP-Durchsatz tatsächlich vs. Ideal</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Downstream TCP-Durchsatzdiagramme</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Nachschaltung TCP erneute Übertragung Durchsatz Grafiken </translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Nachschaltung TCP RTT-Durchsatz Grafiken</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Ideale Upstream-Übertragungszeit (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Tatsächliche Upstream-Übertragungszeit (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Vorschaltung tatsächlicher L4 Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Vorschaltung tatsächlicher L4 ungeprägter Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Vorschaltung tatsächlicher L4 geprägter Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Vorschaltung idealer L4 Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Upstream-TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Vorschaltung ungeprägte TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Vorschaltung geprägte TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Upstream-Pufferwartezeit (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Vorschaltung ungeprägte Pufferverzögerung (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Vorschaltung geprägte Pufferverzögerung (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Nachschaltung tatsächlicher L4 Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Nachschaltung tatsächlicher L4 ungeprägter Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Nachschaltung tatsächlicher geprägter L4 Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Nachschaltung idealer L4 Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Downstream-TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Nachschaltung ungeprägte TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Nachschaltung geprägte TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Downstream-Pufferwartezeit (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Nachschaltung ungeprägte Pufferverzögerung (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Nachschaltung geprägte Pufferverzögerung (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>TCP-Durchsatzergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Tatsächlich vs. Ideal</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>Upstream Testergebnisse können andeuten: </translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>Keine weitere Empfehlung.</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>Der Test wurde nicht lange genug durchgeführt</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>Netzwerkpuffer/-former muss  abgestimmt werden</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>Überwacher hat Pakete wegen TCP Bursts fallen gelassen. </translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>Der Durchlauf war gut, aber erneute Übertragungen wurden entdeckt.  </translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>Das Netzwerk ist ausgelastet oder der Verkehr wird begrenzt</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>Ihr CIR könnte falsch konfiguriert sein</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>Ihre Datei wurde zu schnell übertragen!&#xA;Bitte prüfen Sie die geschätzte Übertragungsdauer für die Datei.</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>&lt;b>%1&lt;/b> Verbindung(en), jede mit einer Fenstergröße von &lt;b>%2&lt;/b> bytes, wurden für die Übertragung einer Datei von &lt;b>%3&lt;/b> MB über jede Verbindung verwendet (&lt;b>%4&lt;/b> MB gesamt).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> Bytes des TCP Fensters über &lt;b>%2&lt;/b> Verbindung(en) weitergeleitet.</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>Gemessen L4 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>Ideal-L4 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Testmetriken</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>TCP-Effizienz&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Pufferverzög.&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>Downstream Testergebnisse können andeuten:</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Vorschaltung tatsächlich vs. ideal</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>TCP-Effizienz (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Pufferverzögerung (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>Ungeprägte Testergebnisse können anzeigen:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Nachschaltung tatsächlich vs. ideal</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Durchsatzdiagramme</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Rückübertragungsframe</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>Basis-Umlaufzeit</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Verwenden Sie diese Diagramme, um mögliche TCP-Leistungsprobleme zu korrelieren, die aufgrund von erneuter Übertragung und/oder Netzwerküberlastung entstehen können (RTT übersteigt Basis).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Neuübertragung Grafiken</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>RTT-Grafiken</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Idealer Durchsatz pro Verbindung</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Für einen Traffic-geshapten Link sollte jede Verbindung einen gleichen Teil der Bandbreite empfangen. Für einen verkehrsüberwachten Link, springt die Verbindung, sobald aufgrund der Überwachung Neuübertragungen stattfinden. Für jede der &lt;b>%1&lt;/b> Verbindungen sollte jede Verbindungen ca. &lt;b>%2&lt;/b> Mb/s der Bandbreite nutzen.</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbit/s</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Zufall</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Geschwindigkeit VNF Test</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>Realgeschwindigkeit VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Testkonfigurationen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Erweiterte Serververbindung</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Erweiterte Testkonfigurationen</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Bericht lokal erstellen</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Testidentifizierung</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Beenden: Ansicht detaillierte Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Beenden: Bericht lokal erstellen</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Lokal einen anderen Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS Test</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Upstream Test</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Downstreamtest</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>Die folgende Konfiguration ist erforderlich.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>Die folgende Konfiguration ist außerhalb des Bereiches. Geben Sie bitte einen Wert zwischen #1 und #2 ein.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>Die folgende Konfiguration hat einen ungültigen Wert. Bitte wählen Sie neu.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>Keine Testlizenz Kunden-an-Server.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>Kein Testlizenz Server-an-Server.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Es bestehen  zuviele aktive Tests (das Maximum beträgt #1).</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>Der lokale Server ist nicht belegt.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>Der Remote Server ist nicht belegt.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>Der Testvorgang besteht bereits.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Test Datenbank Lesefehler.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>Der Test wurde in der Testdatenbank nicht gefunden.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>Der Test ist abgelaufen.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>Die Testart wird nicht unterstützt.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>Der Testserver ist nicht optioniert.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>Der Testserver ist belegt.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Test Server schlechter Anfragemodus.</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>Tests sind auf dem Remote Server nicht zugelassen.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>HTTP Anfrage auf Remote Server nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>Der Remote Server verfügt über ungenügende Resourcen.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>Der Testkunde ist nicht optioniert.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>Die Testschnittstelle wird nicht unterstützt.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>Zu häufige Testversuche pro Stunde.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>Der Testvorgangsaufbau war nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>Der Testarbeitsflussaufbau war nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>Die Anfrage Arbeitsfluss HTTP ist schlecht.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>Die Anfrage Arbeitsfluss HTTP war nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>Remotetests nicht erlaubt.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>Fehler im Resourcenmanager.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>Der Testvorgang wurde nicht gefunden.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>Der Teststatus hat einen Konflikt.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>Der Teststatus ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>Die Testerstellung war nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>Die Testaktualisierung war nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>Test erforlgreich erstellt.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>Test erfolgreich aktualisiert.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>HTTP Anfrage nicht erfolgreich: #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>VNF Servervversion (#2) muss nicht unbedingt mit Instrumentenversion (#1) kompatibel sein.</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Geben Sie bitte den Benutzernahme und den Bestätigungsschlüssel für den Server bei #1 ein.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Test konnte nicht initiiert werden: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Test abgebrochen: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Serververbindung</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>Information für Verbindung zum Server nicht vorhanden.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>Es besteht keine Verknüpfung zur Durchführung von Netzwerkkommunikationen.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>Keine gültige Quell-IP-Adresse für Netzwerkkommunikation vorhanden.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>Ping bei spezifizierter IP-Adresse nicht durchgeführt.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>Einheit an der spezifizeirten IP Adresse nicht erkennbar.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>Server hat spezifizierte IP Adresse noch nicht identifiziert.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>Server kann bei der spezifizierten IP Addresse nicht identifiziert werden.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Server identifiziert aber nicht bestätigt.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Serverbestätigung nicht erfolgreich.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Bestätigung nicht erfolgreich, Identifizierungsversuch.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>Nicht zugelassen oder identifiziert, Versuch Ping.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Server bestätigt für Test verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>Server ist verbunden und der Test läuft.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Identifizierung läuft</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Identifizieren</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>TCP Proxyversion</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Testkontrollversion</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Verknüpfung Aktiv:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>Server-ID:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Serverstatus:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Erweiterte Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Erweiterte Verbindungseinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Bestätigungsschlüssel</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Benutzernahme und Schlüssel merken</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Lokale Einheit</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Server</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Fenster Durchlaufzeit (Sek)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Automatische Dauer</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Testkonfigurationen (Erweitert)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Erweiterte Testparameter</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>TCP-Port</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Automatischer TCP Port</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Anzahl Fenstergänge</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Verbindungszählung</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Automatische Verbindungszählung</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Fenstersättigung</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Fenstersättigung laufen lassen</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Erhoehte Verbindung [%]</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Server Berichtinformation</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Testname</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Name des Technikers</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Name des Kunden*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Unternehmen</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>Email*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefon</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Kommentare</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Test ID anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Test Realgeschwindigkeit überspringen</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>Server ist nicht verbunden.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS (Byte)</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Durchschn. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>Warten auf bereite Testquellen. Sie sind #%1 in der Warteliste.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenster</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Fenster&#xA;Sättigung</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Fenstergröße (kB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Upstream Diagnose:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Nichts zu Berichten</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>Durchlauf Zu Tief</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>Inkonsistente RTT</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>TCP Effizienz ist Tief</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>Verzögerung Zwischenspeicher ist Hoch</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>Der Durchlauf Liegt Unter 85% des CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU Unter 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Downstreamdiagnose:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Bestätigungscode</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Bestätigungscode Erstellung</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Verfalldatum</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Zeit Ändern</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Test Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Letzte Änderung</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Email</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>Upstream Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Ist vs. Ziel</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Upstream Ist vs. Ziel</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Upstream Zusammenfassung</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Spitze TCP Durchlauf (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Zeit Round Trip (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Upstream Zusammenfassung Ergebnisse (Max. Durchlauffenster)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Fenstergröße pro Verbindung (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Anhäufung Window (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>Ziel-TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Durchschnittler TCP Durchlauf (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>TCP-Durchsatz (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Ziel</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Fenster 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Fenster 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Fenster 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Fenster 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Fenster 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Fenster 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>Maximum Druchgang Fenster:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 kB Window: %2 conn. x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Durchschnitt</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Upstream Durchlaufgrafik</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Upstream Einzelheiten</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Upstream Fenster 1 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Fenstergröße Pro Verbindung (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Aktueller Durchsatz (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Ziel Durchlauf (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Total erneute Übetragungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Upstream Fenster 2 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Upstream Fenster 3 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Upstream Fenster 4 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Upstream Fenster 5 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Upstream Fenster 6 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Upstream Fenster 7 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Upstream Fenster 8 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Upstream Fenster 9 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Upstream Fenster 10 Durchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Upstreamsättigung Fensterdurchlaufergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Durchschnittlicher Durchlauf (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Downstream Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Downstream Ist vs. Ziel</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Zusammenfassung Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Ergebnisse Zusammenfassung Downstream (Max. Durchlaufsfenster)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Downstream Durchlaufsgrafiken</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Einzelheiten Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Downstream Fenster 1 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Downstream Fenster 2 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Downstream Fenster 3 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Downstream Fenster 4 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Downstream Fenster 5 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Downstream Fenster 6 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Downstream Fenster 7 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Downstream Fenster 8 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Downstream Fenster 9 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Downstream Fenster 10 Durchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Downstreamsättigung Fensterdurchlaufsergebnisse</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Übersättigte Fenster (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Übersättigte Verbindungen (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>VLAN-Scan</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 Start VLAN-Scan Test #2</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Testen von VLAN ID #1 für #2 Sekunden</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: GENEHMIGT</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: FEHLGESCHLAGEN</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>Aktive Schleife ist nicht erfolgreich. Test für VLAN-ID #1 fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>Gesamte VLAN IDs getestet</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Anzahl an genehmigten IDs</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Anzahl an fehlgeschlagenen IDs</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Dauer pro ID (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Anzahl an Bereichen</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Ausgewählte Bereiche</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>VLAN ID Min.</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID Max.</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Test starten</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>Gesamte IDs</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>Genehmigte IDs</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>Fehlgeschlagene IDs</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Erweiterte Einstellungen für VLAN-Scan</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Bandbreite (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Genehmigungskriterien</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>Keine Rahmen verloren</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Rahmen erhalten</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Konfiguriere TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>TrueSAM ausführen...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Geschätzte Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Schluß mit Fehlern</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Bericht anzeigen</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>TrueSAM-Bericht anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Bestanden</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Signal-Verlust.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Link Loss.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>Die Kommunikation mit dem fernen Tester wurde abgebrochen. Stellen Sie die Verbindung zum Kommunikationskanal wieder her und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Bei der Konfiguration des lokalen Testers ist ein Problem aufgetreten. Die Kommunikation mit dem fernen Tester wurde abgebrochen. Stellen Sie die Verbindung zum Kommunikationskanal wieder her und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Es ist ein Problem bei der Konfiguration des entfernten Testsatzes aufgetreten, die Kommunikation mit dem Testsatz wurde unterbrochen. Bitte stellen den Kommunikationskanal wieder her und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>Bei der Konfiguration des lokalen Testers ist ein Problem aufgetreten. TrueSAM wird nun beendet.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Beim Versuch, den lokalen Tester zu konfigurieren, ist ein Problem aufgetreten. Der Test wurde abgebrochen und der Kommunikationskanal mit der fernen Tester ging verloren.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Beim Versuch, den fernen Tester zu konfigurieren, ist ein Problem aufgetreten. Der Test wurde abgebrochen und der Kommunikationskanal mit dem fernen Tester ging verloren.</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>Der Bildschirmschutz wurde deaktiviert, um während der Tests Interferenz zu vermeiden.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>In dem lokalen Tester ist ein Problem aufgetreten. TrueSAM wird nun beendet.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>DHCP-Parameter werden angefordert.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>DHCP-Parameter erhalten</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Ethernet-Interface wird initialisiert. Bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>Nordamerika</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>Nordamerika und Korea</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>Nordamerika (PCS)</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Indien</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Verlust des Tageszeitsignals von der externen Zeitquelle verursacht das Verschwinden 1PPS-Sync.</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Starte Synchronisation mit Tageszeitsignal von externer Zeitquelle...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Erfolgreiche Synchronisation mit Tageszeitsignal von externer Zeitquelle.</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Loop&#xA;Down</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Titel-Blätter</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Durchzuführende Tests auswählen</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>End-to end-Verbindungstest</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>Erweitertes RFC 2544 / SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Ethernet Benchmarking Test Suite</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Y.1564 Ethernal Services Konfigurations- und Leistungstest.</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Ebene 2: Test für Control Plane Rahmen (CDP, STP, etc)</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>RFC 6349 TCP Durchsatz- und Performance-test</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>L3-Quellentyp</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Einstellungen für Übertragungskanäle (unter Verwendung von Service 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Übertragungskanaleinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>Service 1 muss für Datenstrom 1 auf dem Viavi-Prüfgerät an der Gegenstelle konfiguriert sein.</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Lokaler Status</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>ToD Sync</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>1PPS Sync</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Verbinden mit &#xA;Kanal</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Physikalische Schicht</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Pausenlänge (Größe)</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Pausenlänge (Zeit - ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Tests&#xA;durchführen</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Starte&#xA;Tests</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Tests werden&#xA;gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Geschätzte Zeit für die Testausführung</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Gesamt</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Bei Fehlern anhalten</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Kriterien damit ausgewählte Tests Erfolgreich/Fehlgeschlagen sind</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>Upstream und Downstream</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>Es gibt kein Erfolgreich/Fehlgeschlagen-Konzept, deshalb wird diese Einstellung nicht angewendet.</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Erfolgreich, wenn die folgenden Schwellenwerte erreicht werden:</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>Kein Schwellenwert festgelegt - keine Information zu Erfolgreich/Fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Lokal:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Fern:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Örtlich und fern:</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Durchlauf (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Durchlauf (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Durchsatz (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Rahmenverlusttoleranz (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Latenz (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Erfolgreich, wenn die folgenden SLA-Parameter erfüllt sind:</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Upstream:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Downstream:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>CIR-Durchsatz (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR Durchlauf (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>CIR-Durchsatz (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>EIR-Durchsatz (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>EIR-Durchsatz (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>EIR Durchlauf (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - Toleranz (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Toleranz (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - Toleranz (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Rahmenverzögerung (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Verzögerungsvariation (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>Erfolgreich/Fehlgeschlagen wird intern ermittelt.</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Erfolgreich, wenn der gemessene TCP-Durchsatz den folgenden Schwellenwert erreicht:</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Prozentsatz des geschätzten Durchsatzes</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>TCP-Durchsatztest nicht aktiviert - keine Information zu Erfolgreich/Fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Stoppen Sie die Tests</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Hiermit stoppen Sie den aktuell ausgeführten Test und die weitere Testausführung. Wollen Sie ihn wirklich stoppen?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsumme für Dienstleistung(en) #1 ist 0 oder überschreitet  die #2 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsumme für die Upstream Richtung für Dienstleistung(en) #1 ist 0 oder überschreitet die #2 L1 Mbps Leistungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsumme für die Downstream Richtung für Dienstleistung(en) #1 ist 0 oder überschreitet die #2 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsummee für Dienstleistung(en) #1 ist 0 oder überschreitet  die jeweiligen L2 Mbps Leitungsrate(n).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsumme für die Upstream Richtung für Dienstleistung(en) #1 ist 0 oder überschreitet die jeweilige(n) L2 Mbps Leitungsrate(n).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallastsumme für die Downstream Richtung für Dienstleistung(en) #1 ist 0 oder überschreitet die jeweiligen L2 Mbps Leitungsrate(n).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallast überschreitet die #1 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallast überschreitet die #1 L1 Mbps Leitungsrate in der Upstream Richtung.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die Maximallast überschreitet die #1 L1 Mbps Leitungsrate in der Downstream Richtung.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die Maximumlast überschreitet die #1 L2 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die Maximumlast überschreitet die #1 L2 Mbps Leitungsrate in der Upstream Richtung.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die Maximumlast überschreitet die #1 L2 Mbps Leitungsrate in der Downstream Richtung.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;CIR, EIR und Überwachung können nicht gleichzeitig 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;CIR, EIR und Überwachung können nicht gleichzeitig in Upstream-Richtung 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;CIR, EIR und Überwachung können nicht gleichzeitig in Downstream-Richtung 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die minimale Schrittauslastung (#1% CIR) konnte unter Verwendung der #2-Mb/s-CIR-Einstellungen für Service #3 nicht erreicht werden. Der kleinste Schrittwert bei Verwendung des aktuellen CIR-Wertes für Service #3 liegt bei #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die CIR (L1 Mbps) Summe ist 0 oder überschreitet die #1 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die CIR (L1 Mbps) Summe für die Upstream Richtung ist 0 oder überschreitet die #1 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Ungültige Konfigurierung:&#xA;&#xA;Die CIR (L1 Mbps) Summe für die Downstream Richtung ist 0 oder überschreitet die #1 L1 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die CIR (L2 Mbps) Summe ist 0 oder überschreitet die #1 L2 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die CIR (L2 Mbps) Summe für die Upstream Richtung ist 0 oder überschreitet die #1 L2 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die CIR (L2 Mbps) Summe für die Downstream Richtung ist 0 oder überschreitet die #1 L2 Mbps Leitungsrate.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Es muss eine der beiden Prüfungen 'Servicekonfiguration' oder 'Serviceperformanz' ausgewählt sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Es muss mindestens ein Service ausgewählt sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die Gesamtanzahl an ausgewählten CIR-Services (oder EIR für Services, die über eine CIR von 0 verfügen) kann die Zeilenrate nicht überschreiten, wenn Sie den Serviceleistungstest durchführen möchten.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceperformanztest nicht unter der Summe der CIR-Werte für die ausgewählten Services liegen.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Upstream-Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceleistungsprüfung nicht unter der Summe der CIR-Werte für die ausgewählten Services liegen.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Upstream-Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceleistungsprüfung nicht unter der Summe der CIR-Werte für die ausgewählten Services liegen.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceperformanztest nicht unter dem CIR-Wert liegen.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Upstream-Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceleistungsprüfung nicht unter dem CIR-Wert liegen.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Der für die Downstream-Durchsatzmessung (RFC 2544) festgelegte Maximalwert darf bei der Serviceleistungsprüfung nicht unter dem CIR-Wert liegen.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>SAMComplete wurde abgebrochen, da die Option 'Bei Fehlern anhalten' aktiviert ist und mindestens ein KPI dem SLA für Service #1 nicht genügt.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete wurde abgebrochen, da innerhalb der ersten 10 Sekunden nach dem Start der Datenübertragungen für Service #1 keine Daten empfangen wurden. Möglicherweise sendet die Gegenstelle die übertragenen Daten nicht in der Schleife zurück.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete wurde abgebrochen, da innerhalb der ersten 10 Sekunden nach dem Start der Datenübertragungen keine Daten für Service #1 zurückgegeben wurden. Möglicherweise überträgt die Gegenstelle keine Daten mehr.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete wurde abgebrochen, da innerhalb der ersten 10 Sekunden nach dem Start der Datenübertragungen keine Daten zurückgegeben wurden. Möglicherweise sendet die Gegenstelle die übertragenen Daten nicht in der Schleife zurück.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete wurde abgebrochen, da innerhalb der ersten 10 Sekunden nach dem Start der Datenübertragungen keine Daten zurückgegeben wurden. Möglicherweise überträgt die Gegenstelle keine Daten mehr.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Streaming-Anwendung, die Remoteanwendung an IP-Adresse #1 ist eine Datenanwendung.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Streaming-Anwendung, die Remoteanwendung an IP-Adresse #1 ist eine TCP WireSpeed-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Schicht 3-Anwendung, die Remoteanwendung an IP-Adresse #1 ist eine Schicht 2-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Die Anwendung auf der lokalen und der Remoteseite sind nicht miteinander kompatibel. Die lokale Anwendung ist eine Schicht 2-Anwendung, die Remoteanwendung an IP-Adresse #1 ist eine Schicht 3-Anwendung.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>Die Remote-Anwendung mit der IP-Adresse #1 ist eingerichtet für die WAN IP. Sie ist nicht kompatibel mit SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Die Fernanwendung bei der IP-Adresse #1 ist für Gestapelte VLAN Verkapselung eingestellt. Dies ist nicht kompatibel mit SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Für die Remoteanwendung an IP-Adresse #1 die VPLS-Kapselung aktiviert. Sie ist nicht mit SAMComplete kompatibel.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Für die Remoteanwendung an IP-Adresse #1 die MPLS-Kapselung aktiviert. Sie ist nicht mit SAMComplete kompatibel.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>Die entfernte Anwendung unterstützt nur #1 Services. </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Bei der Prüfung der Einzelstreckenverzögerung müssen die lokalen und die Remoteprüfgeräte für die Synchronisierung mit einem OWD-Zeitgeber ausgestattet sein. Bei der Synchronisierung der Einzelstreckenverzögerung ist bei der lokalen Einheit ein Fehler aufgetreten. Bitte überprüfen Sie den Anschluss der OWD-Zeitgeberhardware.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Bei der Prüfung der Einzelstreckenverzögerung müssen die lokalen und die Remoteprüfgeräte für die Synchronisierung mit einem OWD-Zeitgeber ausgestattet sein. Bei der Synchronisierung der Einzelstreckenverzögerung ist bei der Remoteeinheit ein Fehler aufgetreten. Bitte überprüfen Sie den Anschluss der OWD-Zeitgeberhardware.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Für die SAMComplete-Prüfung muss vorher eine Umlaufzeit- (RTT-)Prüfung durchgeführt werden.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Die TrueSpeed-Sitzung konnte nicht eingerichtet werden. Bitte überprüfen Sie die Konfiguration auf der Seite "Netzwerk" und wiederholen Sie den Vorgang.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Kann keine TrueSpeed-Sitzung in Upstream-Richtung aufbauen. Gehen Sie zur Seite "Netz", prüfen Sie die Konfigurationen und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Kann keine TrueSpeed-Sitzung in Downstream-Richtung aufbauen. Gehen Sie zur Seite "Netz", prüfen Sie die Konfigurationen und versuchen Sie es erneut. </translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>Die Ergebnisse der Umlaufzeitprüfung sind nicht mehr gültig, da die zu prüfenden Services neu festgelegt wurden. Bitte wechseln Sie zur Seite "TrueSpeed-Steuerung" und führen Sie die Umlaufzeitprüfung (RTT-Prüfung) erneut durch.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die gesamte CIR (Mbps) der Downstream-Richtung für Service(s) #1 kann nicht 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die gesamte CIR (Mbps) der Upstream-Richtung für Service(s) #1 kann nicht 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Ungültige Konfiguration:&#xA;&#xA;Die CIR (Mbps) für Service(s) #1 kann nicht 0 sein.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Kein Traffic empfangen. Wechseln Sie zur Netzwerkseite, prüfen Sie die Konfiguration und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Hauptergebnisanzeige</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Tests&#xA;stoppen</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>  Bericht erstellt, klicken Sie auf Weiter um ihn anzuzeigen</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Berichtsanzeige überspringen</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete&#xA;Ethernet-Serviceaktivierungstest</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>mit TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Lokale Netzeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>Für die lokale Einheit ist keine Konfiguration erforderlich.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Ferne Netzeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>Für die Remoteeinheit ist keine Konfiguration erforderlich.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Lokale IP-Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Entfernen Sie die IP-Settings</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Dienste</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Tagging</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>Tagging wird nicht verwendet.</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>SLA Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>SLA-Überwachung</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>Keine Überwachungstests ausgewählt.</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>SLA Burst</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>Burst Testen wurde deaktiviert wegen der Abwesenheit der benötigten Funktionalität.</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>SLA-Leistung</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed ist zurzeit deaktiviert.&#xA;&#xA;Wenn sich das Gerät nicht im Loopback-Betriebsmodus befindet, kann TrueSpeed über die Konfigurationsseite "Netzwerk" aktiviert werden.</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Örtliche fortgeschrittene Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Fortgeschrittene Verkehrseinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Fortgeschrittene LBM Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Fortgeschrittene SLA Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Zufalls-/EMIX-Größe</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>Erweiterte IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 Erweitert</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Erweitertes Tagging</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Servicekonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Serviceperformanz</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Y.1564-Test beenden</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Discovery Server Status</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Discovery Server Meldung</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>MAC Adresse und ARP Modus</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>SFP Auswahl</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>WAN Quellen-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>Statisch - WAN IP</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>WAN-Gateway</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>WAN-Subnetzmaske</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>Verkehrsquelle IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Verkehr Subnet Mask</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>Wird VLAN-Tagging für den lokalen Netzwerkport verwendet?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q (Stapel-VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Warten</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Beim Zugreifen auf Server...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>kann auf Server nicht zugreifen</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP geholt</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Miete Genehmigt: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Miete Reduziert: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Miete Freigegeben</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Discovery Server:</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbit/s CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 1: VoIP - 25% des Traffic - #1 und #2 Byte-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 2: Videotelephonie - 10% des Traffic - #1 und #2 Byte-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 3: Prioritätsdaten - 15% des Traffic - #1 und #2 Byte-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 4: BE-Daten - 50% des Traffic - #1 und #2 Byte-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Alle Diensten</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Service 1-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Sprache</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>Bei 10 GE beträgt die Bandbreitengranularität 0,1 Mb/s. Infolgedessen ist der CIR-Wert ein Vielfaches der Granularitätsebene</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>Bei 40 GE ist die Ebene der Bandbreitengenauigkeit 0,4 Mbps, der CIR-Wert ist ein Vielfaches dieser Genauigkeitsebene</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>Bei 100 GE beträgt die Bandbreitengranularität 1 Mb/s. Infolgedessen ist der CIR-Wert ein Vielfaches der Granularitätsebene</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Größen konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Rahmengröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Festgelegte Länge</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>EMIX-Zykluslänge</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Paketlänge (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Berechnete Rahmenlänge</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>Die berechnete Framegröße wird durch die Paketlänge und die Kapselung festgelegt.</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Service 2-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Service 3-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Service 4-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Service 5-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Service 6-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Service 7-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Service 8-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Service 9-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Service 10-Triple Play-Eigenschaften</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Zu kleine Rahmen</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Anzahl der Services</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Layer</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>LBM Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Triple-Play konfigurieren...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Serviceeigenschaften</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC und Framegrößeneinstellungen </translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC, Einstellungen und EtherType Rahmengröße</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC, Paketlänge und TTL Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>Einstellungen DA MAC und Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Erweiterte Netzwerkeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>Eine lokale / entfernte Einheiteninkompatibilität erfordert ein Paket mit &lt;b>%1&lt;/b> Byte oder länger.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Dienst</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Konfigurieren...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>Benutzer Größe</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (Hops)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>Ziel-MAC-Adres.</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Beides anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Nur fern</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Nur lokal</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>LBM Einstellungen (Fortgeschrittene)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Netzwerkeisntellungen Zufalls-/EMIX-Größe</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Service 1 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Frahmengröße 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Packetlänge 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Anwendergröße 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Jumbogröße 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Frahmengröße 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Packetlänge 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Anwendergröße 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Jumbogröße 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Frahmengröße 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Packetlänge 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Anwendergröße 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Jumbogröße 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Frahmengröße 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Packetlänge 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Anwendergröße 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Jumbogröße 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Frahmengröße 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Packetlänge 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Anwendergröße 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Jumbogröße 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Frahmengröße 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Packetlänge 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Anwendergröße 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Jumbogröße 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Frahmengröße 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Packetlänge 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Anwendergröße 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Jumbogröße 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Frahmengröße 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Packetlänge 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Anwendergröße 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Jumbogröße 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Service 1 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Service 1 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Service 2 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Service 2 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Service 2 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Service 3 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Service 3 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Service 3 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Service 4 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Service 4 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Service 4 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Service 5 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Service 5 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Service 5 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Service 6 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Service 6 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Service 6 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Service 7 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Service 7 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Service 7 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Service 8 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Service 8 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Service 8 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Service 9 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Service 9 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Service 9 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Service 10 Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Service 10 Zufalls-/EMIX-Länge (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Service 10 Zufalls-/EMIX-Länge</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>Haben Services verschiedene WLAN-iDs oder Benutzerprioritäten?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>Nein, alle Services verwenden dieselben VLAN-Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Erweitert...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Benutzer pri.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>SVLAN Pri</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Einstellungen für VLAN-Tagging (Verkapselung)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>SVLAN Priorität</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Alle Services verwenden dieselbe Verkehrsrichtung IP.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>Verkehrsrichtung IP</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>Verkehrsrichtung IP</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>IP ID Schritte setzen</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>IP-Einstellungen (Nur lokal)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>IP-Einstell.</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>IP-Einstellungen (Nur Remote)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>IP Einstellungen (Fortgeschrittene)</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Fortgeschrittene IP Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>Haben Services verschiedene TOS- oder DSCP-Einstellungen?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>Nein, TOS/DSCP ist für alle Services gleich</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>Gesamte SLAs</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>Gesamte CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>Vorschaltung gesamte CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>Nachschaltung gesamte CIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>Gesamte EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>Vorschaltung gesamte EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>Nachschaltung gesamte EIR</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Gesamtmodus</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Max. Durchsatzprüfung lokal (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Max. Durchsatzprüfung remote (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Gesamtmodus aktivieren</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>WARNUNG: Die ausgewählten Gewichtswerte addieren sich derzeit zu &lt;b>%1&lt;/b>%, nicht 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>SLA-Durchsatz (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA Durchsatz, L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>SLA Durchsatz, L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>SLA Durchsatz, L1 Mbps (Einweg)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA Durchsatz, L2 Mbps (Einweg)</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Gewicht (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Max. Last</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Nur Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Nur Upstream</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Fortgeschrittene Verkehrseinstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>SLA-Richtlinien (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Max. Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Burst Testing durchführen</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Toleranz (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Toleranz (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>Möchten Sie den Burst-Test durchführen?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Burst Testtyp:</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Fortgeschrittene Burst Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Rahmenverzögerungspräzision</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Rahmenverzögerung als SLA Forderungen einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Rahmenverzögerungsvariante als SLA Forderung einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>SLA-Leistung (Einzelstrecke)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Frameverzögerung (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Frameverzögerung (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Fortgeschrittene SLA Einstellungen setzen</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Servicekonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Anzahl Schritte unterhalb CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Schrittdauer (s)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>Schritt 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>Schritt 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>Schritt 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>Schritt 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>Schritt 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>Schritt 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>Schritt 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>Schritt 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>Schritt 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>Schritt 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Schrittprozente</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Serviceperformanz</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Jede Richtung wird separat geprüft, also wird die Gesamttestlaufzeit zweimal der eingegebene Wert sein.</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Test wird bei Fehler gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Fortgeschrittene Burst Testeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>überspringen</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Y.1564-Test auswählen</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Services für Test auswählen</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Alle setzen</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Gesamt:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Servicekonfigurationstest</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Serviceperformanztest</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Optionale Messungen</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Durchsatz (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Max. (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Max. (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Max. (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>Wenn ein TrueSpeed-Service aktiviert wurde, können keine optionalen Messungen durchgeführt werden.</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Y.1564-Test ausführen</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>überspringen</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Verzögerungsvar</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Zusammenfassung der Fehler in Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Urteile</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Y.1564-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Testurteil konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Konfig Test-Svc 1-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Konfig Test-Svc 2-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Konfig Test-Svc 3-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Konfig Test-Svc 4-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Konfig Test-Svc 5-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Konfig Test-Svc 6-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Konfig Test-Svc 7-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Konfig Test-Svc 8-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Konfig Test-Svc 9-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Konfig Test-Svc 10-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Urteil Performanztest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Urteil Performanztest Svc 1</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Urteil Performanztest Svc 2</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Urteil Performanztest Svc 3</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Urteil Performanztest Svc 4</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Urteil Performanztest Svc 5</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Urteil Performanztest Svc 6</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Urteil Performanztest Svc 7</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Urteil Performanztest Svc 8</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Urteil Performanztest Svc 9</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Urteil Performanztest Svc 10</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Urteil Performanztest IR</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Urteil Performanztest Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Urteil Performanztest Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Urteil Performanztest Verzögerungsvariation</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Urteil Performanztest TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Servicekonfigsergebnisse</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 1</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Max Durchsatz (L1 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Downstream Max Durchsatz (L1 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Upstream Max Durchsatz (L1 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Max Durchsatz (L2 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Downstream Max Durchsatz (L2 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Upstream Max Durchsatz (L2 Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>CIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Downstream CIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Rahmenverzögerungsvariation (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>OoS-Zähler</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>Fehler Frame Erfassung</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Pause Erfassung</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Upstream CIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>CBS-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Downstream CBS-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Konfigurierte Burst-Größe (kb)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tx Burst Größe (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Durchschnittl. Rx Burst-Größe (kb)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>Geschätzte CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Upstream CBS-Urteil</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>EIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Downstream EIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Upstream EIR-Urteil</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Überwachungsurteil</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Downstream Überwachungsurteil</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Upstream Überwachungsurteil</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Urteil Schritt 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Downstream Urteil Schritt 1</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Upstream Urteil Schritt 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Urteil Schritt 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Downstream Urteil Schritt 2</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Upstream Urteil Schritt 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Urteil Schritt 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Downstream Urteil Schritt 3</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Upstream Urteil Schritt 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Urteil Schritt 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Downstream Urteil Schritt 4</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Upstream Urteil Schritt 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Urteil Schritt 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Downstream Urteil Schritt 5</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Upstream Urteil Schritt 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Urteil Schritt 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Downstream Urteil Schritt 6</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Upstream Urteil Schritt 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Urteil Schritt 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Downstream Urteil Schritt 7</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Upstream Urteil Schritt 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Urteil Schritt 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Downstream Urteil Schritt 8</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Upstream Urteil Schritt 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Urteil Schritt 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Downstream Urteil Schritt 9</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Upstream Urteil Schritt 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Urteil Schritt 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Downstream Urteil Schritt 10</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Upstream Urteil Schritt 10</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Max. Durchsatz (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Max. Durchsatz (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Max. Durchsatz (Mb/s)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Schritte</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Schlüssel</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Klicken Sie auf die Leisten, um die Ergebnisse für die einzelnen Schritte anzuzeigen.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Durchsatz Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Durchsatz L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Durchsatz L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>Fehler Erfassung</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Schritt 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Schritt 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Schritt 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Schritt 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Schritt 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Schritt 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Schritt 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Schritt 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>Schrit 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Schritt 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>SLA-Schwellen</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 2</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 3</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 4</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 5</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 6</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 7</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 8</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 9</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Konfigurationsergebnisse für Service 10</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Serviceperformanzergbnisse</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Serviceurteil</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR (aktuell)</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR (max.)</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR (min.)</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR (Mittel)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Rahmenverlustsekunden</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Zähler verlorene Frames</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Verzögerung (aktuell)</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Verzögerung (max.)</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Verzögerung (min.)</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Verzögerung (mittel)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Verzögerungsvariation (akt.)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Verzögerungsvar. (max.)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Verzögerungsvar. (min)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Verzögerungsvar. (mittel)</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Verfügbarkeit</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Verfügbare Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Nicht verfügbare Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Sekunden mit schweren Fehlern</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Serviceperformanz - Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Verzögerungsvar</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>TrueSpeed-Service. Ergebnisse werden auf der Seite "TrueSpeed" angezeigt.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, durchschn.&#xA;(Durchsatz&#xA;L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, durchschn.&#xA;(Durchsatz&#xA;L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Rahmen-&#xA;verlustrate</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Einweg-&#xA;Verzögerung&#xA;(Mittel, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Verzögerungs-&#xA;variation&#xA;(max., ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Fehler Erfasst</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Verzögerung&#xA;(mittel, RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Akt.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Akt.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, Max.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, Max.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, Min.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, Min.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Zähler&#xA;verlorene Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Rahmenverlust-&#xA;Verhältnis&#xA;(Schwellwert)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Verzögerung&#xA;(akt., OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Verzögerung&#xA;(max., OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Verzögerung&#xA;(min., OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Verzögerung&#xA;(mittel, OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Verzug&#xA;Schwellenwert&#xA;(OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Verzögerung&#xA;(akt., RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Verzögerung&#xA;(max., RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Verzögerung&#xA;(min., RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Verzug&#xA;Schwellenwert&#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Verzögerung&#xA;(akt., ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Verzögerungs-&#xA;variation&#xA;(Mittel, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Verzögerungs-&#xA;variation&#xA;(Schwellwert, ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Verhältnis&#xA;verfügbare&#xA;Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Nicht verfügbare&#xA;Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Sekunden&#xA;mit schweren&#xA;Fehlern</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Service Konfig&#xA;Durchsatz&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Service Konfig&#xA;Durchsatz&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Service Leistung&#xA;Durchsatz&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Service Leistung&#xA;Durchsatz&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Netzwerkeinstellungen</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Benutzerrahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Länge Jumbo-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Benutzerpaketlänge</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Länge Jumbo-Pakete</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Berechnete Rahmengröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Netzwerkeinstellungen (Zufalls-/EMIX-Größe)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Zufalls-/EMIX-Längen (Remote)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Zufalls-/EMIX-Längen (Lokal)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Zufalls-/EMIX Längen</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Fortgeschrittene IP Einstellungen - Fern</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>Downstream-CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>Downstream-EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Downstream-Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>Downstream-M</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>Upstream-CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>Upstream-EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Upstream-Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>Upstream-M</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA Fortgeschrittene Burst</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Downstream-Frame-Verlustverhältnis </translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Downstream-Frame-Verzögerung (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Downstream-Frame-Verzögerung (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Downstream-Verzögerungsvariation (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Upstream-Frame-Verlustverhältnis</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Upstream-Frame-Verzögerung (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Upstream-Frame-Verzögerung (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Upstream-Verzögerungsvariation (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Als SLA Forderung einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Rahmenverzögerung einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Rahmenverzögerungsvariante einbeziehen</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete hat die folgenden ungültigen Konfigurationseinstellungen:</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Servicekonfigsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>TrueSpeed-Prüfung durchführen</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>HINWEIS: TrueSpeed-Test wird nur ausgeführt, wenn der Serviceleistungstest aktiviert ist.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Paket Länge TTL setzen</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>TCP/UDP Ports setzen</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Quelltyp</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Quellport</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>Zieltyp</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>Zielport</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>TCP Durchsatz Schwellwert (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Empfohlene Gesamtfenstergröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Erhöhte Totalfenstergröße (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>Empfohlene # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>Erhöhte # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Upstream Empfohlene Gesamtfenstergröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Upstream Erhöhte Totalfenstergröße (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>Upstream Empfohlene # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>Upstream Erhöhte # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Downstream Empfohlene Gesamtfenstergröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Downstreamerhöhung Totalfenstergröße (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>Downstream Empfohlene # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>Downstreamerhöhung # Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>Empfohlene # of Verbindungen</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>TCP-Durchsatz (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Acterna Payload</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Rundfahrt Zeit (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>Die RTT wird in den nachfolgenden Schritte zur Erstellung der Fenstergröße und Anzahl der Verbindungsempfehlungen verwendet.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>TrueSpeed-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>TCP-Übertragungsmetrik</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>Ziel-L4 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>TrueSpeed-Serviceurteil</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Bewertung des Upstream-TrueSpeed-Service </translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Tatsächlicher TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Tatsächlicher Upstream-TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Upstream-Ziel-TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>Ziel-TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Bewertung des Downstream-TrueSpeed-Service </translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Tatsächlicher Downstream-TCP-Durchsatz (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Downstream-Ziel-TCP-Durchsatz (Mbps)</translation>
        </message>
    </context>
</TS>
