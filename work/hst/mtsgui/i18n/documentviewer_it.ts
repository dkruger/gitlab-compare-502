<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>Caricamento: </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>Caricamento PDF non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>Impossibile rendere pagina: </translation>
        </message>
    </context>
</TS>
