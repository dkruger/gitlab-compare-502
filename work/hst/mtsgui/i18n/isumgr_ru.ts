<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Установка недостоверного IP- адреса не принята.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>Установка недействительного шлюза : 172.29.0.7. Перезапустить систему MTS.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>IP- адрес был изменен.  Перезапустить модуль BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>Идет инициализация модуля BERT.  Запрос на выключение модуля отклонен.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>Идет инициализация модуля BERT с функцией оптического джиттера. Запрос на выключение модуля отклонен.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>Идет отключение модуля BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>Модуль BERT выключен</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>Модуль BERT выключен . Нажмите HOME/SYSTEM, затем иконку BERT, чтобы активировать ее .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>Модуль BERT включен</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** ОБНОВЛЕНИЕ ПРИКЛАДНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** ОБНОВЛЕНИЕ ЗАВЕРШЕНО *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** ОБНОВЛЕНИЕ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ДЖИТТЕРА *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** ОБНОВЛЕНИЕ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ЯДРА *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** ИДЕТ ЗАПУСК ОБНОВЛЕНИЯ *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>Ошибка интерфейса пользователя модуля BERT</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Идет запуск интерфейса пользователя модуля BERT с функцией оптического джиттера</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>Идет запуск интерфейса пользователя модуля BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>Ошибка интерфейса пользователя модуля BERT. Модуль отключен</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Модуль 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Модуль 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Модуль 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Модуль 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Модуль</translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Модуль 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Модуль 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Модуль 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Модуль 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Модуль 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Модуль 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Модуль 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Модуль 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Модуль 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Модуль 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Модуль 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Модуль 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Модуль 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Модуль 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Модуль 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Модуль 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Модуль 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Модуль 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Требуется обновление базового программного обеспечения. Текущая редакция программного обеспечения не совместима с модулем BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>Требуется переустановка программного обеспечения BERT. Необходимое программное обеспечение BERT  не установлено для прилагаемого модуля BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>Требуется обновление аппаратных средств модуля BERT. Текущие аппаратные средства не совместимы.</translation>
        </message>
    </context>
</TS>
