<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Değişikliklerin tam olarak etkin olması için lütfen cihazı yeniden başlatın.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Yapıştır</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Kopyala</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Kes</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Tek</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Çoklu</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tümü</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aç</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Yeniden Adlandır</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Yeni Klasör</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Disk</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>Yapıştırma mevcut dosyaları üzerine yazacak.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Dosyayı Üzerine yaz</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>Seçilen dosya kalıcı olarak silinecektir.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>Seçilen %1 dosyalar kalıcı olarak silinecektir.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Dosyayı Sil</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Lütfen klasör adı girin:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Klasör Oluştur</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>"%1" Klasörü oluşturulamadı.</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>"%1" klasörü zaten mevcut.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Klasör Oluştur</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Yapıştırıyor...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Siliyor...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Tamamlandı.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 Başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Dosyayı yapıştırma başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>%1 Dosyaları yapıştırma başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Yeterli boş alan yok.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Dosyayı silme başarısız oldu.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>%1 Dosyaları silme başarısız oldu.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Geri</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opsiyon</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zaman</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>URL Güncelle</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dosya Adı</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Eylem</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Diğer Kablosuz Ağlar</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Kablosuz Ağ bilgisini aşağıya giriniz.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>İsim ( SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Şifreleme tipi:</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Güncelleme mevcut</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync bir güncelleme olduğunu tespit etti. &#xA;Şimdi güncelleme yapmak ister misiniz?&#xA;&#xA; Güncelleme çalışan tüm testleri sonlandıracaktır. İşlemin sonunda test cihazı güncellemeyi tamamlamak için yeniden başlatılacak.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Bilinmeyen bir hata ile karşılaşıldı. Lütfen tekrar deneyiniz.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Güncelleme başarısız oldu</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Cihaz bilgisi:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Cihazı &#xA;varsayılan değerlere Resetleyin</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Logları USB hafızaya aktar.</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Sistem&#xA;bilgilerini dosyaya kopyalayın</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>Sistem bilgileri &#xA;bu dosyaya kopyalandı:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Bu yeniden başlatma gerektirir ve Sistem Ayarları ve Test ayarları varsayılan değerlere resetlenecek.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Yeniden Başlat ve Resetle</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Log aktarma başarılı</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Loglar aktarılamıyor. Lütfen USB hafızanın doğru bir şekilde takıldığını kontrol edip tekrar deneyiniz. </translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 Versiyon %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Baştan Başlat</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Varsayılana Resetle</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Güncelleme metodunu seçin:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>USB Flash bellekte yüklü olan dosyalardan güncelleme</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Ağ</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Güncellemeyi Ağdaki bir web sunucudan indir.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Güncelleme sunucusunun adresini giriniz:</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Sunucu adresi</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Güncelleme sunucusuna erişmek için gerektiğinde Proxy sunucunun adresini giriniz:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Bağlan</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Mevcut güncellemeler için sunucuyu sorgula.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Önceki</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Sonraki</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Güncelleme Başlat</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Güncelleme bulunamadı</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Güncelleme başarısız oldu</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>Güncelleme çalışan tüm testleri sonlandıracaktır. İşlemin sonunda test cihazı güncellemeyi tamamlamak için yeniden başlatılacak.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Güncelleme onayı</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Bilinmeyen bir hata ile karşılaşıldı. Lütfen tekrar deneyiniz.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Sistem</translation>
        </message>
    </context>
</TS>
