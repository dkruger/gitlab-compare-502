<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi 顕微鏡</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>テキストラベル</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>ﾋﾞｭｰ FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>固定</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>ﾂﾞｰﾑ ｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>重畳</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>解析中 ...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>ﾌﾟﾛﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>USB からｲﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>ﾋﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>自動ｾﾝﾀﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>ﾃｽﾄ ﾎﾞﾀﾝ :</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>固定する</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>その他の設定 ...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>顕微鏡プロファイルのインポート</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>プロファイルの&#xA;インポート</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>顕微鏡プロファイル (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>固定</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>ﾗｲﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>PNG の保存</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>PDF の保存</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>画像の保存</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>ﾚﾎﾟｰﾄの保存</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>分析に失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>ﾌｧｲﾊﾞを分析できませんでした。ﾗｲﾌﾞ ﾋﾞﾃﾞｵにﾌｧｲﾊﾞ端部が表示され、ﾌｧｲﾊﾞ端部にﾋﾟﾝﾄが合っていることを再ﾃｽﾄ前に確認してください。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>画像の選択</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>画像ﾌｧｲﾙ (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>すべてのﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>ﾌｧｲﾊﾞの検査およびﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>ﾌｧｲﾊﾞ情報</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>ほかに拒否されたﾌｧｲﾊﾞ情報はありません</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>ﾌﾟﾛﾌｧｲﾙ:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>ﾋﾝﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>検査要約</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>障害</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>ｽｸﾗｯﾁ</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>または</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>基準</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>しきい値</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>ｶｳﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>任意の</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>適用外</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>検査要約を生成できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>低倍率</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>高倍率</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>ｽｸﾗｯﾁ ﾃｽﾄが有効になっていません</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>ｼﾞｮﾌﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>ｹｰﾌﾞﾙ :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>ｺﾈｸﾀ:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>会社 :</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>ﾃｸﾆｼｬﾝ:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>顧客:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>ﾛｹｰｼｮﾝ:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>ｼﾞｮﾌﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>ｹｰﾌﾞﾙ :</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>ｺﾈｸﾀ:</translation>
        </message>
    </context>
</TS>
