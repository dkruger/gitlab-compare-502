<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aç</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
    </context>
</TS>
