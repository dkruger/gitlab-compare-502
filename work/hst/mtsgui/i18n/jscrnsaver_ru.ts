<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Данный комплект испытаний заблокирован пользователем для предотвращения несанкционированного доступа.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>Пароль не требуется, нажмите OK, чтобы разблокировать комплект испытаний.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Разблокировать комплект испытаний?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Введите пароль, чтобы разблокировать комплект испытаний:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Ввести пароль</translation>
        </message>
    </context>
</TS>
