<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>systrayd::CAudioSystemTrayDataItem</name>
        <message utf8="true">
            <source>Audio</source>
            <translation>Audio</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBatterySystemTrayDataItem</name>
        <message utf8="true">
            <source>Battery/Charger</source>
            <translation>Akku/Ladegerät</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please plug in AC power to continue using the test instrument.</source>
            <translation>Die Batterie ist zu heiß. Bitte stecken Sie das AC-Kabel ein, um das Testinstrument weiter zu verwenden.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please keep the AC power plugged in to continue using the test instrument.</source>
            <translation>Die Batterie ist zu heiß. Bitte lassen Sie das AC-Kabel eingesteckt, um das Testinstrument weiter zu verwenden.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and cannot be used until it cools down. The AC power must remain plugged in to continue using the test instrument.</source>
            <translation>Die Batterie ist zu heiß und kann nicht mehr verwendet werden, bis sie herunter kühlt. Das AC-Kabel muss eingesteckt bleiben, um das Testinstrument weiter zu verwenden.</translation>
        </message>
        <message utf8="true">
            <source>Battery charging is not possible at this time.</source>
            <translation>Derzeit ist keine Möglichkeit zum Laden des Akkus verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>A power failure has occurred, the battery is currently not charging.</source>
            <translation>Ein Stromausfall ist aufgetreten, die Batterie lädt gerade nicht.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBeepingWarning</name>
        <message utf8="true">
            <source>System Warning</source>
            <translation>Systemwarnung</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBluetoothSystemTrayDataItem</name>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>File received and placed in the bluetooth inbox.&#xA;Filename: %1</source>
            <translation>Die Datei wurde empfangen und im Bluetooth-Eingangsordner gespeichert.&#xA;Dateiname: %1</translation>
        </message>
        <message utf8="true">
            <source>Pair requested. You may go to the Bluetooth system page to complete the pair by clicking on the icon above.</source>
            <translation>Verbindungsanforderung: Navigieren Sie bei Bedarf zu der Systemseite 'Bluetooth', und schließen Sie den Verbindungsvorgang ab, indem Sie auf das oben angezeigte Symbol klicken.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CNtpSystemTrayDataItem</name>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CP5000iSystemTrayDataItem</name>
        <message utf8="true">
            <source>P5000i</source>
            <translation>P5000i</translation>
        </message>
        <message utf8="true">
            <source>The P5000i Microscope will not function in this USB port.  Please use the other USB port.</source>
            <translation>Das P5000i Mikroskop funktioniert nicht an diesem USB-Port. Verwenden Sie den anderen USB-Port.</translation>
        </message>
        <message utf8="true">
            <source>Microscope Error</source>
            <translation>Mikroskopfehler</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CStrataSyncSystemTrayDataItem</name>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CUsbFlashSystemTrayDataItem</name>
        <message utf8="true">
            <source>USB Flash</source>
            <translation>USB-Flashmedium</translation>
        </message>
        <message utf8="true">
            <source>Format complete. Please remember to eject the device from the USB system page before removing it.</source>
            <translation>Formatierung abgeschlossen. Stellen Sie sicher, dass das Gerät über die Systemseite 'USB' ausgeworfen wurde, bevor Sie es entfernen.</translation>
        </message>
        <message utf8="true">
            <source>Formatting USB device. Do not remove device until process is completed.</source>
            <translation>USB-Gerät wird formatiert. Entfernen Sie das Gerät erst, nachdem der Vorgang abgeschlossen wurde.</translation>
        </message>
        <message utf8="true">
            <source>The USB device was removed without being ejected. Data may be corrupted.</source>
            <translation>Das USB-Gerät wurde entfernt, ohne dass es vorher ausgeworfen wurde. Möglicherweise sind Daten auf dem Gerät beschädigt.</translation>
        </message>
        <message utf8="true">
            <source>The USB device has been ejected and is safe to remove.</source>
            <translation>Das USB-Gerät wurde ausgeworfen und kann nun entfernt werden.</translation>
        </message>
        <message utf8="true">
            <source>Please remember to eject the device from the USB system page before removing it.</source>
            <translation>Stellen Sie sicher, dass das Gerät über die Systemseite 'USB' ausgeworfen wurde, bevor Sie es entfernen.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CVncSystemTrayDataItem</name>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CWifiSystemTrayDataItem</name>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
    </context>
</TS>
