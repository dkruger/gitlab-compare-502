<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>Frame {1} byte:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>Frame {1} byte</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>Pacchetti {1} byte:</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>Pacchetti {1} byte</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} Errore: Si è verificato un timeout durante il tentativo di recuperare {2} , controllare la connessione e riprovare</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} frame burst: fallito</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} frame burst: passato</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{1} di {2}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} packet burst: fallito</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} packet burst: passato</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} Recupero di {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        Scegliere OK per riprovare</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} - Esecuzione test VLAN ID {2} per {3}...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} µs</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (µs)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} µs</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} Attesa...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       È possibile modificare il nome per creare una nuova configurazione.</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 Top Talkers (su {1} conversazioni OP totali)</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 principali conversazioni di ritrasmissione TCP (su {1} conversazioni totali)</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Termina</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Annulla Test</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Attivo</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>Loop attivo non riuscito. Test interrotto per VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>Test in atto</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>Aggiungi intervallo</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>Una velocità di perdita di frame superiore alla soglia configurata per la perdita frame</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>Dopo avere eseguito i test manuali o quando necessario è possibile</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>Loop hardware trovato</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>Loop hardware non trovato</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Tutti i test</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>Un'applicazione di loopback non è un'applicazione compatibile</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Una misura di throughput massimo è Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>Un loop attivo non è stato trovato</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analizza</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analisi in corso</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>e</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>e test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>È stato trovato un loop LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>È stato trovato un loop LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>Loop permanente trovato</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>Accorda il registro di avanzamento alla fine del rapporto</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>Nome applicazione</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>Tempo totale appr.:</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>Una gamma di valori di throughput FTP teorici sarà calcolata in base agli effettivi valori misurati del link.  Immettere la larghezza di banda collegamento, il ritardo di andata e ritorno, e l'incapsulamento misurati.</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>Si è verificato un timeout di risposta.&#xA;Non vi è stata alcuna risposta all'ultimo comando&#xA;entro {1} secondi.</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> Si presuppone che sia in posizione un loop hardware.        </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimmetrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>La modalità Asimmetrica trasmette dall'unità vicina all'unità remota in modalità Upstream e dall'unità remota all'unità vicina in modalità Downstream. La modalità combinata eseguirà il test due volte, trasmettendo in sequenza in direzione Upstream usando la Configurazione locale e quindi in direzione Downstream usando la Configurazione remota. Utilizzare il pulsante per sovrascrivere la configurazione remota con l'impostazione locale corrente.</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>Tentativo in corso</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>Tentativo di loop su</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Il tentativo di accesso al server ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>I tentativi di eseguire un loop non hanno avuto esito. Il test è terminato</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>Il tentativo di loop non ha avuto successo</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Autonegoziazione</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>Autonegoziazione eseguita</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>Impostazioni di autonegoziazione</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Stato Autonegoziazione</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponibile</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>Media Burst</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>Velocità media pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>Dimensione media pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>Media</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>Risultati test Pkt Jitter Media e Max Media:</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>Latenza (RTD) media:</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>Latenza (RTD) media: N/P</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>Media Packet Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>Media Packet Jitter: N/P</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>Media Packet Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>Velocità media</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>Granularità frame Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>Granularità frame Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test frame back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>Risultati test Frame back-to-back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>Tempo massimo di prova Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>Tempo massimo di prova Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>Risultati test Back-to-back:</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>Torna a Riepilogo</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$balloon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularità larghezza di banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>Granularità larghezza di banda (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>Accuratezza misura larghezza di banda:</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>Accuratezza misura larghezza di banda</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>Test di carico base</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>Inizio intervallo:</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation> Bit</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>Entrambi</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx e Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>Gli indirizzi OP di origine locale e remoto non sono disponibili</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom-Up</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>Crediti buffer&#xA;(necessita di Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>Durata prova Crediti Buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>Durata prova Crediti Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Test Crediti Buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>Risultati test Crediti buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>Throughput Crediti Buffer {1} byte:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>Throughput di crediti buffer&#xA;(necessita di Crediti buffer)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Test di Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>Risultati test Throughput Crediti buffer:</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>Dimensione buffer</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>Granularità Burst (frame)</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>Osservando le ritrasmissioni TCP a fronte dell'utilizzo della rete nel tempo, è possibile correlare le scarse prestazioni di rete con le condizioni della rete con perdite di dati quali la congestione.</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>Osservando la tabella OP Conversazioni , i principali emittenti ("Top Talkers") possono essere identificati in base ai byte o ai frame.  Le diciture "S -> D" e "S &lt;- D" indicano la direzione Origine (Source) verso Destinazione o Destinazione verso Origine del traffico di byte e frame.</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(byte)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>Byte&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>Byte&#xA; S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>Calcolata&#xA;Lunghezza frame</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>Calcolata&#xA;Lungh. pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>Calcolo in corso ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>Impossibile procedere!</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>Riepilogo analisi acquisizione</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>Durata acquisizione</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Acquisizione&#xA;Schermo</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>ATTENZIONE!&#xA;&#xA;Si è certi di volere eliminare&#xA;permanentemente questa configurazione?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Velocità cfg (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Velocità cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Velocità cfg (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>ID Telaio</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>Le voci Rx selezionate saranno utilizzate per configurare impostazioni del filtro origine.</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>Le voci Tx selezionate saranno utilizzate per configurare impostazioni del filtro Tx di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Verifica loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>Controllo presenza loop hardware</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>Verifica di loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Controllo loop LBM/LBR in corso.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>Controllo presenza loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>Controllo di rilevazione porte Half Duplex</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>Verifica frame ICMP</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>Verifiche di possibili ritrasmissione o elevati l'utilizzi della larghezza di banda</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Verifica Loop hardware</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Controllo loop LBM/LBR in corso</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Verifica di Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>Verifica delle statistiche gerarchia protocollo</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Verifica disponibilità indirizzo di origine...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>Selezionando questa casella si causerà il ripristino delle impostazioni di test sulle impostazioni originali al momento dell'uscita dal test. Per l'esecuzione di test asimmetrici, esse saranno ripristinati sia sul lato locale sia su quello remoto. Il ripristino configurazioni determinerà la reimpostazione del collegamento.</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Controllare le impostazioni delle porte tra l'OP di origine e il dispositivo cui è collegato; verificare Che non sia presente la condizione di half duplex .   Un ulteriore sezionamento può essere ottenuto anche spostando l'analizzatore più vicino al OP di destinazione; determinare se le ritrasmissioni sono eliminate per isolare il link o i link difettosi. </translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>Scegli un file di acquisizione da analizzare</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>Scegli&#xA;file PCAP</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>Scegli la precisione di misura della larghezza di banda desiderata&#xA;(per un tempo di prova più breve è consigliabile l'1%).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>Scegliere il tipo di login controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>Scegliere Preferenza dimensione Frame o Pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>Scegliere la granularità con cui si desidera per l'esecuzione del test Back-to-back.</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>Scegli la granularità con cui si desidera eseguire il test di perdita di frame.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>Scegliere la Larghezza di banda massima per cui è configurato il circuito.  L'unità si utilizza questo numero come una massima larghezza di banda per la trasmissione , riducendo la durata della prova:</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>Scegliere la dimensione massima dei Crediti buffer.&#xA; La prova avrà inizio con questo numero, riducendo così la durata del test:&#xA;NOTA:  Il dispositivo remoto che crea il loop del traffico deve essere &#xA;impostato con i seguenti parametri:&#xA;&#xA;1.  Controllo di flusso ON&#xA;2.  {1}&#xA;3.  {2} Crediti buffer impostati sullo stesso valore immesso sopra.</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>Scegliere il tempo massimo di prova per il test Back-to-back.</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>Scegliere i valori di carico minimo e massimo da utilizzare con le procedure di prova 'Top Down'o 'Bottom up'</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>Scegliere il numero di prove che si desidera eseguire per il test Back to Back dei frame delle diverse dimensioni.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>Scegliere il numero di prove che si desidera eseguire per il test di Latenza (RTD) dei frame delle diverse dimensioni.</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>Scegliere il numero di prove che si desidera eseguire per il test di Packet Jitter dei frame delle diverse dimensioni.</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>Scegliere la percentuale di Tolleranza perdita di frame consentita del Throughput.&#xA;NOTA: Le impostazioni > 0.00 NON SONO CONFORMI con RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>Sceglie re la durata di ogni test di Latenza (RTD).</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>Scegliere la durata di ogni test di Packet Jitter.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>Scegli il tempo per il quale una velocità deve potere essere inviata senza errori per superare il test di Throughput.</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>Scegliere la durata desiderata per prova di perdita di frame.</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>Scegli il tempo di prova per il test dei Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>Scegliere quale procedura da utilizzare nel test di perdita di frame.&#xA;NOTA: La procedura RFC2544 va dalla Larghezza di banda massima e diminuisce con la granularità di Larghezza banda per ogni prova , e termina dopo due prove consecutive senza perdite di frame.</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>Cisco Discovery Protocol</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>In questa rete sono stati rilevati messaggi Cisco Discovery Protocol (CDP ) e nella tabella sono elencati gli indirizzi MAC e le porte che hanno indicato impostazioni Half Duplex.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Cancella tutto</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>Cliccare sul pulsante "Risultati" per passare all'interfaccia utente standard.</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violazioni del codice</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>Combinata</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>Commenti</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commenti</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>Comunicazione stabilita con l'estremità remota</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>Impossibile stabilire la comunicazione con il punto remoto</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>La comunicazione con l'estremità remota è stata persa</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>completato</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>completato&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configurazione</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>Nome configurazione</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>Nome configurazione:</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>Nome configurazione</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>Nome configurazione obbligatorio</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>Configurazione di sola lettura</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>Riepilogo configurazione</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>Configurare la/le voce/i selezionata/e</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>Configurare per quanto tempo {1} invierà traffico.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>Conferma sostituzione configurazione</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>Conferma cancellazione</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNESSO</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connessione in corso</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>Connetti a applicazione di misurazione test</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continua in Half Duplex</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>Continuazione in modalità half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>Copia impostazione locale&#xA;su impostazione remota</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>Copia&#xA;Selezionato</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>Impossibile eseguire loop con estremità remota</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crea rapporto</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>crediti</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(Crediti)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA;Script corrente: {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>Selezione corrente</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>Cliente</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>Velocità bit dati</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>Velocità byte dati</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Arresto layer dati</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo dati</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>Modo dati impostato su PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>Dimensione dati </translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>Data e Ora</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>giorni</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>Ritardo, corr. (µs):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>Ritardo, corr. (µs)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Indirizzo di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>Configurazione della destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>OP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>OP di destinazione&#xA;Indirizzo</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>Il MAC di destinazione per indirizzo OP {1} non è stato trovato</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>MAC di destinazione trovato.</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>Indirizzo MAC dest.</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>Etichetta dettagli</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Dettagli</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>rilevato</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>Rilevato</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>Rilevata larghezza di banda del link</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       Rilevati più frame di quanti trasmessi per Larghezza di banda {1} - Test non valido.</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>Determinare il throughput simmetrico</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>Dettagli della periferica</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ID dispositivo</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>I parametri DHCP non sono disponibili</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>Parametri DHCP trovati.</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>Periferiche rilevate</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>Scoperta</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>Scoperta tipo di loop capo remoto in corso...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>Scoperta&#xA;non&#xA;attualmente&#xA;disponibile</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>Visualizza per:</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>Direzione Downstream</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> Continuare comunque? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durata</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Abilitato</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>Abilita Test di traffico Layer 2 esteso</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>Incapsulamento:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Incapsulamento</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>Fine</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>Fine intervallo:</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>Ora finale</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>Immettere l'indirizzo OP o il nome del server con cui si desidera eseguire il test FTP.</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>Immettere il nome di login per il server a cui ci si vuole connettere</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>Immettere la password per l'account che si desidera utilizzare</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>Immettere il nuovo nome della configurazione&#xA;(Usare soltanto lettere, numeri , spazi , trattini e trattini bassi):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Errore: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ERRORE: Si è verificato un timeout di risposta&#xA;Non c'era alcuna risposta entro</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>Errore: Impossibile stabilire la connessione</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Conteggio errori</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Frame errati</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>Errore di caricamento del file PCAP</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>Errore: Risoluzione nome non riuscita con DNS primario.</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>Errore: impossibile individuare il sito</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>Stima tempo rimanente</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>Tempo rimanente stimato</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     Rapporto Test Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>Il registro eventi è pieno.</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>Trovate ritrasmissioni in eccesso</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>Esci da J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>Throughput atteso</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>Il throughput atteso è</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>Il throughput atteso non è disponibile</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>Pulsante "Test RFC 2544 esperto".</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Esplicito (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Esplicito (Fabric/N-Port)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> Non è stato possibile completare il login esplicito. </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALLITO</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>NON RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>L'estremo remoto è un dispositivo di test JDSU Smart Class Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>L'unità remota è un assieme di prova Ethernet Smart Class Viavi</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Test FC</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>Il test FC viene eseguito usando il Payload del Test Acterna</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Supporto FDX</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>Rapporto Test Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>Configurazione file</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome file</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>File</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>Dimensione file</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>Dimensioni file:</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>Dimensione file</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>Dimensioni file: {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>Dimensioni file:</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>Dimensioni file</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>File trasferito troppo rapidamente. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>Ricerca del throughput atteso</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>Ricerca dei "Top Talkers"</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>Prime 50 porte Half Duplex (sul totale di {1})</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>Primi 50 messaggi ICMP (sul totale di {1})</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Tipo di login controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>Cartelle</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> per ogni frame è ridotta alla metà per compensare doppia lunghezza della fibra.</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>trovato</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>Trovato loop attivo.</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>Trovato loop hardware.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>Frame&#xA;Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Lunghezza frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Lunghezza frame (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>Lunghezze frame:</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Lunghezze frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>Lunghezze frame per test</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Perdita frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>Perdita frame {1} byte:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>Perdita frame {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>Granularità larghezza di banda Perdita frame:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>Granularità larghezza di banda Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>Larghezza di banda massima Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>Larghezza di banda minima Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>Velocità perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Tasso di perdita frm</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Test perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>Procedura del test Perdita frame:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procedura del test Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>Risultati test Perdita frame:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolleranza perdita di frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>Durata prova Perdita frame:</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>Durata prova Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>Frame o Pacchetto</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>frame</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>dimensioni frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Dimensioni frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>Dimensione frame:  {1} bytes</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>Frame&#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>Frame&#xA; S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(frms)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(frms/sec)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>Throughput FTP</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>Test di Throughput FTP&#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>Test Throughput FTP completato!</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>Rapporto di prova Throughput FTP</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>Ottieni informazioni PCAP</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>Porte Half Duplex</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>Loop hardware</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>(Hardware&#xA;o attivo)</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>(Hardware&#xA;Permanente&#xA;o Attivo)</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Supporto HDX</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>Elevato utilizzo</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Pagina iniziale</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>ore</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>Test di Throughput HTTP</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>Rapporto di prova Throughput HTTP</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>Codice&#xA;ICMP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>Messaggi ICMP</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>Se i contatori di errore aumentano in maniera sporadica eseguire il test manuale</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>Se il problema persiste scegliere 'Ripristina valori test predefiniti' Dal menu Strumenti.</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>Se non si può risolvere il problema con gli errori sporadici è possibile impostare</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>Implicito (Transparent Link)</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>Informazioni</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>Inizializzazione delle comunicazioni con</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>Al fine di determinare la larghezza di banda con cui la</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>Le velocità in ingresso del lato locale e di quello remoto non corrispondono</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>Rilevati problemi intermittenti sulla linea.</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Errore interno</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>Errore interno - Riavvia PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config non valida</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>Configurazione OP non valida</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>Indirizzi IP</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>Conversazioni OP</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>sta uscendo</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>in avviamento.</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>Jitter Test</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck è completato</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck ha perso il collegamento o non è stato in grado di stabilire link</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>Il test di traffico L2 può essere riavviato eseguendo nuovamente J-QuickCheck.</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latenza</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>Test di Latenza (RTD) e Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>Media Latenza (RTD): N/P</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>Soglia di riuscita Latenza (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>Soglia di riuscita Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>Latenza (RTD)&#xA;(necessita di Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>Risultati Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>Test di Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>Risultati test di Latenza (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>Risultati test di Latenza (RTD): INTERROTTO   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>Risultati test di Latenza (RTD): NON RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>Risultati test di Latenza (RTD): RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>Risultati test di Latenza (RTD) ignorati</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>Test di Latenza (RTD) ignorato</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> Soglia di Latenza (RTD): {1} µs</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>Soglia di Latenza (RTD) (µs)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>Durata prova di Latenza (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>Durata prova di Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>Latenza (RTD) (µs)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>Layer 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>Layer 1 / 2&#xA;Ethernet Health</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Layer 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>Trovato link layer 2 presente</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>Test Rapido Layer 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>Layer 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>Layer 3&#xA;OP Health</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>Layer 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>Layer 4&#xA; TCP Health</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Loop LBM/LBR </translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>Link trovato</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>Link Layer Discovery Protocol</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Link perso</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>Velocità link rilevata nel file di acquisizione</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>Porta di ascolto</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>Formato del carico</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>CARICAMENTO... Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Locale</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>Filtro OP di destinazione locale è configurato per</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>Filtro MAC di destinazione locale è configurato per</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>Porta di destinazione locale è configurata per</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>Tipo di loop locale è configurato per Unicast</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Porta locale</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>Filtro OP di origine remoto è configurato per</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> Numero di serie locale</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>Impostazione locale</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> Versione software locale</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>Filtro OP di origine locale è configurato per</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>Filtro MAC di origine locale è configurato per</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>Filtro porta origine locale è configurato per</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>Riepilogo locale</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> Nome strumento test locale</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>Individuare il dispositivo con l'indirizzo o gli indirizzi MAC origine e le porte elencate nella tabella e assicurarsi che le impostazioni duplex siano impostate su "full"e non su "auto ".  Non di rado un host è impostato su "auto", un dispositivo di rete è impostato su "auto", e il link si negozia erroneamente su half- duplex.</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>Posizione</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>Posizione</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>Login:</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Accedi</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>Nome login:</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>Nome login</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Loop non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>Disconnessione loop unità remota...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>Loop unità remota attivo...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>Stato del loop sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>Loop non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>Costituzione loop eseguita</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>Connessione loop riuscita</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>Rilevata perdita di Link Layer 2!</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>Perso</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Frame persi</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>Gestione degli indirizzi</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>Tipo MAU</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max.</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>(max {1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>Media Max</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>Packet Jitter media max:</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>Packet Jitter media max: N/P</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>Max Packet Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Largh. banda max. (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>Largh. banda max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Dimensione massima buffer</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>Latenza massima, Media consentita per "Superare" il test di Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>Packet Jitter massimo, Media consentita per "Superare" il test di Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>Massimo Crediti buffer massima RX</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>Massima larghezza di banda dei test:</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>Massima larghezza di banda dei test</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>Throughput massimo misurato:</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>Limite tempo massimo di {1} per VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>Limite tempo massimo di 7 giorni per VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>Tempo massima di prova (secondi )</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>Massimo Crediti buffer massima TX</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>Velocità max</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>Numero max di tentativi di ritrasmissione raggiunto. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>Misurato</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>Velocità misurata (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>Velocità misurata</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>Velocità misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Precisione della misura</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>Misura di Throughput a {1} Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Messaggio</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimo</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>Larghezza di banda percentuale minima</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>Percentuale minima di banda necessaria per "superare" il Test di throughput:</translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>Termine minimo di 5 secondi per VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>Velocità min</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>minuti</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>minuti</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>minuti</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Modello</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>Modifica</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>MPLS / VPLS Encapsulation attualmente non supportato ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/P (loop hardware)</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Nessuno dei due</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Rete attiva</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>Utilizzo della rete</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>L'utilizzo della rete non è stata rilevata per essere eccessivo, ma questo grafico fornisce una rappresentazione dell'utilizzo della rete</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>L'utilizzo della rete non è stato rilevato come eccessivo, ma la tabella fornisce un elenco degli OP top talkers.</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>Nuovo</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>Nome nuova configurazione</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>Nuovo URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Successivo</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>Non è stata trovata nessuna applicazione compatibile</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt;NESSUNA CONFIGURAZIONE DISPONIBILE></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>Non è stato selezionato alcun file per la prova.</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>Loop hardware non trovato</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>Nessun&#xA;dispositivo&#xA;JDSU&#xA;Rilevato</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>Nessun Link Layer 2 rilevato!</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>Non è stato possibile stabilire nessun loop</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>Non è stato possibile attivare né trovare un loop</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>Loop permanente non trovato</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>Non sono state rilevate applicazioni in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>NON È CONFORME con RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Non collegato</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Non determinato</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>NOTA:  Qualsiasi impostazione > 0.00</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>Nota: Presuppone un loop hardware con i crediti Buffer inferiore a 2.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA;Nota: Presuppone un loop hardware con crediti buffer inferiore a 2.&#xA; Questo test non è valido.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>Nota: In base al presupposto di un loop hardware, il minimo di crediti buffer calcolato</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>Nota: In base al presupposto loop hardware, le misurazioni di throughput vengono effettuate con </translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>Nota: quando si usa una Tolleranza perdita dati il test non è conforme</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>Note</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Non selezionato</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Nessun&#xA;dispositivo&#xA;Viavi&#xA;scoperto</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>Uscita in corso...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>Verifica in corso</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>Verifica con {1} crediti in corso.  Questa operazione richiederà circa {2} secondi.</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>Numero di prove Back to Back:</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>Numero di prove Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>Numero di operazioni non riuscite</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>Numero di ID testati</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>Numero di prove di Latenza (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>Numero di prove di Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>Numero di prove Packet Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Numero di prove Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>Numero di pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>Numero di operazioni riuscite</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>Numero di tentativi:</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Numero di tentativi</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>di</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Spento</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>di frame sono state perse in un secondo</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>del throughput di J-QuickCheck previsto.</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(% della velocità della linea)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% della velocità della linea</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>della velocità della linea</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Acceso</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * Solo {1} tentativo/i ha/hanno generato dati utilizzabili *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(ON o OFF)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Frame OoS</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID Originatore</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>Fuori intervallo</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        Risultato complessivo test: {1}        </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    Risultato complessivo test: INTERROTTO   </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>Fuori intervallo</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>per gli ultimi 10 secondi anche se il traffico avrebbe dovuto fermarsi</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Packet Jitter, media</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>Soglia di riuscita Packet Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Soglia di riuscita Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>Packet Jitter&#xA;(necessita di Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>Risultati Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Test Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>Risultati test Packet Jitter INTERROTTO   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>Risultati test Packet Jitter NON RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>Risultati test Packet Jitter RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> Soglia Packet Jitter: {1} µs</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>Soglia Packet Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>Durata prova Packet Jitter:</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>Durata prova Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>Pacchetto&#xA;Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Lungh. pacchetto (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>Lunghezze pacchetti:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>Lunghezze pacchetti:</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>Lunghezze pacchetti per test</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>dimensione packet</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>Dimensioni pacchetto</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Riuscito / Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>PASSATO/FALLITO</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>Velocità riuscito (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>Velocità riuscito (frm/sec)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>Velocità riuscito (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>Velocità riuscito (pkts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>Password:</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Password</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>Pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Advrt. Pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Capace di pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>Pausa Det</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Individuati frame pausa</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>Individuati frame pausa - Test non valido</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>Errore di parsing file PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>File PCAP</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>In sospeso</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>Esecuzione di pulitura</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>(Permanente&#xA;o Attivo)</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pkt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Pkt Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Lunghezza pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perdita pacch.</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(pkts)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(pkts/sec)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>Piattaforma</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Accertarsi di disporre di sincronizzazione e collegamento,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>Accertarsi di essere correttamente connessi,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>Accertarsi di essere correttamente connessi,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>Scegliere un altro nome di configurazione.</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>Immettere un nome di file per salvare il report (max %{1} caratteri) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>Immettere eventuali Commenti (max {1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>Immettere eventuali Commenti (max %{1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire gli eventuali commenti che hai (massimo {1} caratteri)&#xA;(utilizzare solo lettere, numeri, spazi trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>Immettere il Nome del cliente (max %{1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>Immettere il Nome del cliente ( max %{1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire il vostro Nome cliente&#xA;(massimo {1} caratteri) &#xA;(utilizzare solo lettere, numeri, spazi, trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire il vostro Nome cliente (massimo {1} caratteri)&#xA;(utilizzare solo lettere, numeri, spazi, trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire il vostro Nome cliente (max {1} caratteri)&#xA;(utilizzare solo lettere, numeri, spazi, trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>Immettere il Nome del tecnico (max {1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>Immettere il Nome del tecnico (max %{1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire il vostro Nome tecnico (max {1} caratteri)&#xA;(utilizzare solo lettere, numeri, spazi, trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>Immettere la Posizione del test (max {1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>Immettere la Posizione del test (max %{1} caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Inserire la propria posizione di prova (max {1} caratteri)&#xA;(utilizzare solo lettere, numeri, spazi, trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>Premere il tasto "Connetti a remoto"</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>Verificare le prestazioni del link con un test di traffico manuale.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>Verificare i propri indirizzi OP origine locale e remoto e riprovare</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>Verificare il proprio indirizzo OP origine locale e riprovare</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>Verificare il proprio indirizzo OP remoto e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>Verificare il proprio indirizzo OP origine remoto e riprovare</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>Attendere che il file PDF sia stato scritto.</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>Attendere che il file PDF sia stato scritto.&#xA;Potrebbero essere necessari fino a 90 secondi...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>Porta:</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Porta {1}: Salvare rapporto test?&#xA;&#xA;Premere "Sì" o "No".</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>ID Porta</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>Porta:				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>Porta:				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP attivo</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Autenticazione PPP non riuscita</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP non riuscito sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE attivo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE inattivo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE avviato</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>Stato PPPoE: </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Timeout PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE Up</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPPoE non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Timeout PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP non riuscito sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP Up non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>Fare clic su "Chiudi" per tornare alla schermata principale.</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>Premere "Esci" per tornare alla schermata principale o "Esegui test" per eseguire di nuovo.</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>Premere&#xA;il pulsante&#xA;aggiorna&#xA;per&#xA;scoprire</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>Premere il tasto "Esci da J-QuickCheck" per uscire da J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>Premere il pulsante Aggiorna qui sotto per scoprire i dispositivi Viavi attualmente sulla sottorete. Selezionare un dispositivo per vedere i dettagli nella tabella a destra. Se Aggiorna non è disponibile assicurarsi che Scoperta sia abilitato e di avere la sincronizzazione e il collegamento.</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>Premere il pulsante "Esegui J-QuickCheck"&#xA;per verificare la configurazione di test locali e remoti e la larghezza di banda disponibile</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>Precedente</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>Avanzamento</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>Proprietà</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Passi successivi proposti</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>Fornitore</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Casuale</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> gamma</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>Intervallo</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Velocità</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>il link potrebbe avere problemi generali non collegati al carico massimo.</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>Ricevuti {1} byte da {2}</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>Frame ricevuti</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>Raccomandazione</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>Configurazione test manuale raccomandata:</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>Aggiorna</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Indirizzo OP remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Loop remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation>Numero di serie remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>Numero di serie remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>Impostazione remota</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>Non è stato possibile ripristinare le impostazioni remote</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> Versione software remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>Versione software remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>Riepilogo remoto</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>Nome strumento test remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>Nome strumento test remoto</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>Rimuovi intervallo</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID Responder</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>Router OP&#xA;di risposta</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>Riavvia J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>Ripristina configurazioni pre-test prima dell'uscita</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>Ripristino impostazioni remote serie test ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>Limita RFC a</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>I risultato del test di carico non è disponibile, fare clic su "Passi successivi proposti" per le possibili soluzioni</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>Risultati a monitor:</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>Ritrasmissioni</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>Sono state rilevate ritrasmissioni&#xA;Analisi delle ritrasmissioni verificatesi nel tempo</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>ritrasmissioni trovate. Esportare il file USB per ulteriori analisi. </translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>Il recupero di {1} è stati interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>tornare all'interfaccia utente RFC 2544 facendo clic sul  </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>Rapporto di prova Ethernet RFC 2544 </translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>Modo RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Standard RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>Test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>Il test RFC 2544 viene eseguito usando il Payload del Test Acterna</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>Il test RFC/FC non può essere eseguito mentre è in esecuzione Risultati grafici Multistreams</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Modo Rfc</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY Det</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Esegui</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>Esegui test FC</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Esegui J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>Esegui $l2quick::testLongName</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>Esecuzione dei test con carico {1}{2}.</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>Esecuzione dei test con carico {1}{2}.  Questa operazione richiederà circa {3} secondi.</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>Esegui test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Esegui script</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Mbps Rx, L1 corr.</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Solo Rx</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>Salva Rapporto di prova Throughput FTP</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>Salva Rapporto di prova Throughput HTTP</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>Salva rapporto del test di Scansione VLAN</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> larghezza di banda graduata</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>Schermata acquisita</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>Script annullato.</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>secondi</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(secs)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>sec</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>Selezionare un nome per la configurazione copiata.</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>Selezionare un nome per la nuova configurazione.</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>Seleziona un intervallo di ID VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>Direzione Tx selezionata:</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>Direzione Tx selezionata&#xA;Downstream</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>Direzione Tx selezionata&#xA;Upstream</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>Avviso selezione</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>Selezionare "OK" per modificare la configurazione&#xA;Modificare il nome per creare una nuova configurazione&#xA;(Usare soltanto lettere, numeri , spazi , trattini e trattini bassi)</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>Seleziona configurazione test:</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>Selezionare la proprietà secondo cui si desidera disporre in elenco i dispositivi scoperti.</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>Selezionare i test che si vogliono eseguire:</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>Seleziona URL</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>Selezionare il formato da usare per caricare le configurazioni correlate.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Invio di richiesta ARP per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>Invio di traffico per</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>invia il traffico, il throughput atteso scoperto da J-QuickCheck sarà di scalata da questo valore.</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID sequenza</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID: del server</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>ID del server</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome servizio</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>Visualizza stato Passato/Fallito</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA;Mostra stato Passato/Fallito per:</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>Dimensione</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Ignora</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>Annullamento del test di Latenza (RDT ) test e continuazione</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>Rev. software</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> Revisione del software</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Indirizzo di origine</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>L'indirizzo di origine non è disponibile</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Disponibilità origine stabilita...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID origine</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>OP origine</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>OP di origine&#xA;indirizzo</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>L'indirizzo OP di origine è uguale all'indirizzo di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC origine</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Indirizzo MAC&#xA;di origine</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>Specificare la larghezza di banda del link</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>Velocità</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocità (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>AVVIA</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>Avvio del test di carico base</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>Avvio della prova</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Stato sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Studiare il grafico per determinare se le ritrasmissioni TCP sono allineate con condizioni degradate di utilizzo della rete.   Osservare la scheda Ritrasmissioni TCP per determinare l'OP di origine che causa ritrasmissioni TCP significative. Controllare le impostazioni delle porte tra l'OP di origine e il dispositivo cui è collegato; verificare Che non sia presente la condizione di half duplex .   Un ulteriore sezionamento può essere ottenuto anche spostando l'analizzatore più vicino al OP di destinazione; determinare se le ritrasmissioni sono eliminate per isolare il/i link difettoso/i. </translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>Riuscita!</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Operazione completata</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Riepilogo</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>Riepilogo dei valori misurati:</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>Sintesi della pagina {1}:</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Priorità utente SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simmetrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>La modalità Simmetrica trasmette e riceve sull'unità vicina usando il loopback. La modalità Asimmetrica trasmette dall'unità vicina all'unità remota in modalità Upstream e dall'unità remota all'unità vicina in modalità Downstream.</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simmetria</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      Fare clic su un nome di configurazione per selezionarlo. </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>Acquisisci file di {1} MB</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	Mettere {1} MB file....</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   Velocità: {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   Velocità: {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx Frames {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              ATTENZIONE!&#xA;&#xA;	    Si è certi di volere eliminare&#xA;	           permanentemente questa configurazione?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   Usare soltanto lettere, numeri, spazi,&#xA;		   trattini e trattini bassi!</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   Questa configurazione è di sola lettura&#xA;		   e non può essere eliminata.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         Si deve immettere un nome per la nuova&#xA;		           configurazione usando il tastierino.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   La configurazione specificata è già presente.</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   Ora: {1} secondi&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tx Frames {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>L'Host TCP non è riuscito a stabilire una connessione. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>L'Host TCP ha rilevato un errore. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>Ritrasmissioni TCP</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome del Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>Cessazione</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>Prova interrotta dall'utente.</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>Test con configurazione della larghezza di banda massima dalla scheda Impostazione - Tutti i test</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>con quantità di traffico inferiori diverse. Se si riscontrano ancora errori con traffico inferiore</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>Configurazione test:</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Verifica configurazione</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Durata del test</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>Durata del test: Almeno 3 volte la durata configurata del test.</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>Larghezza di banda testata</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### Esecuzione del test completata ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>Test di {1} crediti in corso </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>Test di {1} crediti in corso </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>Test a </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>Prova connessione... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> Nome strumento test</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>Il test si sta avviando</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>Registro del test:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nome test:</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>Nome test:			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>Nome test:			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procedura di prova</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>Registro di avanzamento del test</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Gamma del test (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>Gamma del test (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Risultati del test</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>Impostazione dell'assieme di prova</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>Test da eseguire:</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Test da eseguire</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>La connessione loop attiva non è riuscita e non è stato trovato nessun loop hardware.</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>La connessione loop attiva non è riuscita e non è stato trovato nessun loop hardware o permanente</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>Il tasso medio per {1} è stato {2} kbps</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>Il tasso medio per {1} è stato {2} Mbps</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>Il file supera il limite di 50.000 pacchetti per JMentor</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>la Soglia di tolleranza perdita frame per tollerare piccoli tassi di perdita dei frame.</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Il protocollo Internet Control Message Protocol (ICMP) è più generalmente conosciuto nel contesto del "Ping" ICMP. Il messaggio "ICMP di destinazione irraggiungibile" indica che la destinazione non può essere raggiunta dal router o dispositivo di rete.</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>Il conteggio frame Acterna del Filtro Rx L2 ha continuato ad incrementarsi</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>Il loop LBM/LBR non è riuscito.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>Gli indirizzi OP di origine locale e remoto sono identici.</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> Le impostazioni di configurazione locali sono state correttamente copiate sulla configurazione remota</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>L'indirizzo OP di origine locale non è disponibile</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>La MTU misurata è troppo piccola per continuare. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>Il nome scelto è già in uso.</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>La porta dell'elemento di rete cui siamo connessi è predisposta su half duplex. Se questo è corretto, premere il tasto "Continua in Half Duplex". In caso contrario, premere " Esci da J-QuickCheck" e riconfigurare la porta.</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>Il grafico dell'utilizzo della rete mostra la larghezza banda consumata da tutti i pacchetti nel file di acquisizione per la durata dell'acquisizione.  Se sono state anche rilevate ritrasmissioni TCP, è consigliabile studiare i risultati del Layer TCP tornando alla schermata di analisi principale.</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>un numero di crediti buffer doppio ad ogni passo per compensare la lunghezza raddoppiata della fibra.</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>Calcolo teorico</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>Valori teorici e misurati:</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>La porta partner (elemento di rete) ha Autoneg OFF e il Throughput atteso non è disponibile, dunque la porta partner è presumibilmente in modalità half duplex .  Se l'half duplex sulla porta partner non è corretto, cambiare le impostazioni della porta partner a full duplex ed eseguire di nuovo J-QuickCheck. Dopo di che, se il Throughput atteso è più ragionevole, è possibile eseguire il test RFC 2544. Se il Throughput atteso è ancora non disponibile controllare le configurazioni porta sul lato remoto. Potrebbe sussistere una mancanza di corrispondenza HD/FD tra i modi delle porte.&#xA;&#xA;Se half duplex sulla porta partner è corretto, andare a Risultati -> Impostazioni -> Interfaccia -> Layer fisico e di modificare l'impostazione Duplex da Full a Half. Poi tornare allo script RFC2544 (Risultati -> Test RFC2544 esperto), Uscire da J-QuickCheck, andare a Throughput Tap e selezionare "Puntamento di processo RFC 2544 Standard (Half Duplex)" ed eseguire il test RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>C'è un problema di comunicazione con l'estremità remota.</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>il dispositivo remoto looping non risponde al comando di loopback Viavi ma restituisce i frame trasmessi al dispositivo locale con i campi indirizzi di origine e di destinazione invertiti</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>il dispositivo remoto looping risponde al comando di loopback Viavi e restituisce i frame trasmessi al dispositivo locale con i campi indirizzi di origine e di destinazione invertiti</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>il dispositivo di loop remoto device restituisce i frame inalterati al dispositivo locale</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>il dispositivo di looping remoto supporta OAM LBM e risponde ad un frame LBM ricevuto ritrasmettendo un corrispondente frame LBR al dispositivo locale.</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>La parte remota è predisposta per l'incapsulamento MPLS</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>Il lato remoto sembra essere un'applicazione di Loopback</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>L'indirizzo OP di origine remoto non è disponibile</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA;Il report è stato salvato come "{1} {2}" in formato PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>Il report è stato salvato come "{1} {2}" in formato PDF</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>Il report è stato salvato come "{1} {2}" in formato PDF, TXT e LOG</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>Il report è stato salvato come "{1} {2}" in formato TXT e LOG</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>L'OP del router che risponde non può trasmettere il pacchetto all'indirizzo OP di destinazione , per cui deve essere eseguita la risoluzione dei problemi tra l'OP del router di risposta e la Destinazione.</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>Il test RFC 2544 non supporta l'incapsulamento MPLS.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>Il Laser di trasmissione è stato acceso&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Il Laser trasmissione è spento!&#xA;Accendere il Laser?&#xA;Premere "Sì" per attivare il Laser o "No" per annullare.</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Il Laser trasmissione è spento!  Accendere il Laser?&#xA;&#xA;Premere "Sì" per attivare il Laser o "No" per annullare.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>Il test VLAN Scan non ha potuto esser completato con OAM CCM attivo.</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>Non è stato possibile configurare correttamente il test VLAN Scan.</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       Questa configurazione è di sola lettura e non può essere modificata.</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>Questo deve essere l'indirizzo OP dell'unità remota quando si utilizza la modalità Asimmetrica</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>Questa tabella identifica gli indirizzi OP di origine su cui si verificano le ritrasmissioni TCP. Quando vengono rilevate ritrasmissioni TCP, ciò può essere dovuto a perdita di pacchetti downstream (verso il lato di destinazione).  Ma potrebbe anche indicare la presenza di un problema di porta half duplex.</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>Questo test viene eseguito usando il Payload del Test Acterna</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>Questo test non è valido.</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>Questo test richiede traffico con incapsulamento VLAN. Assicurarsi che la rete connessa fornisca un indirizzo OP per questa configurazione.</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>Questa operazione richiederà circa {1} secondi.</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>Throughput ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>Test Throughput e Latenza (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Test Throughput e Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>Tolleranza perdita di frame Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>Tolleranza perdita di frame Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>Test di Throughput, Latenza (RTD) e Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>Soglia di riuscita Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Soglia di riuscita Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>Riduzione del throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>Fattore di riduzione del throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test di throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>La durata del test Volume dati è stata di {1} secondi.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>Risultati del test di Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Risultati del test di Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>Risultati del test di Throughput: INTERROTTO   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>Risultati del test di Throughput: FALLITO</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>Risultati del test di Throughput: PASSATO</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Soglia di throughput (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> Soglia di Throughput: {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>Soglia di throughput (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>Durata prova di Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>Durata prova di Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>Processo di puntamento Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processo di puntamento Throughput</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>Ora fine</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>Ora fine</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>Tempo per ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>Tempo (secondi)</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>Tempo&#xA;(sec)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>Ora d'inizio</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>Ora d'inizio</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>Numero di visite</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>Per continuare, verificare il collegamento del cavo e riavviare J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>Per determinare il Massimo Throughput scegliere il metodo dello standard RFC 2544 che corrisponde al conteggio frame Tx e Rx o il metodo Viavi Enhanced che utilizza l'utilizzo medio % misurato di L2.</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>Per risparmiare tempo il test di Latenza (RDT) in modalità asimmetrica deve essere eseguito in una sola direzione</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>per vedere se ci sono eventi di perdita frame sporadici o costanti.</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>Byte totali</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>Frame&#xA;Totali</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>Numero totale</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Util. Totale {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>Totale util (kbps):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>Totale util (Mbps):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>Per visualizzare il report, selezionare "Visualizza rapporto" dal menu Rapporto dopo l'uscita da {1}.</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>A entro</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>Traffico: costante con {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Risultati di traffico</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>Il traffico era ancora in corso di generazione dal lato remoto</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>Il Laser trasmissione è spento!</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>Frame trasmessi</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>Trasmissione Downstream</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>Trasmissione Upstream</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>Versione di prova</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>Prova {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>Prova {1} completata&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>Tentativo {1} di {2}:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>Durata tentat. (secondi)</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>prove</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>Esecuzione di un secondo tentativo</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>errore tshark</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>Buffer TX a Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Direzione Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Laser Tx Spento</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Mbps Tx, L1 corr.</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Solo Tx</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> Impossibile eseguire connessione loop automaticamente su unità remota. </translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>Impossibile connettersi con applicazione di misurazione test!</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>Impossibile connettersi con applicazione di misurazione test!&#xA;Premere "Sì" per riprovare. "No" per annullare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>Impossibile ottenere un indirizzo DHCP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>Impossibile eseguire test RFC2544 con loopback locale abilitato.</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>Impossibile eseguire il test</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>Impossibile eseguire test VLAN Scan con loopback locale abilitato.</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>INDISP.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>Identificatore unità</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>SU</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>(Su o Giù)</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>Direzione Upstream</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(µs)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>Test interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>Test annullato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nome utente</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Priorità utente</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>L'utente richiesto non è attivo</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>Selezionato dall'utente&#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>Selezionato da utente      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>Usare la schermata Riepilogo stato per vedere gli eventi di errore.</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>Uso</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>Con dimensioni frame di</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>Utilizzazione e ritrasmissioni TCP</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valore</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>I valori evidenziati in azzurro provengono da prove reali.</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>Verifica che il link è attivo...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>verificare l'indirizzo IP remoto utilizzato e riprovare</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>verificare l'indirizzo OP remoto utilizzato e riprovare</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Enhanced</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>Intervalli di VLAN ID da testare</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Test VLAN Scan</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>Rapporto del test di Scansione VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>Risultati del test di Scansione VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>Rapporto del test VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>Frame VTP/DTP/PAgP/UDLD rilevato!</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>Attesa termine autonegoziazione ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>Attesa di MAC di destinazione per&#xA;  indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>In attesa dei parametri DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>Attesa di link layer 2 presente ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>In attesa di Link</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>In attesa di abilitazione OWD, Sinc. ToD Sync e Sinc. 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>Attesa di ARP riuscito ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>è stata rilevata nell'ultimo secondo.</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>Dimensione sito web</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>Abbiamo un loop attivo</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>Abbiamo un errore!!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>Per il test di collegamenti half-duplex , selezionare lo standard RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>Dimensione finestra/Capacità</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>alle raccomandazioni RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>Salvare rapporto test?&#xA;&#xA;Premere "Sì" o "No".</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>È anche possibile usare il grafico Risultati grafici Velocità perdita frame corr.</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>Non è possibile eseguire questo script da {1}.</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>Potrebbe essere necessario attendere fino al suo arresto per riconnettersi</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Azzeramento sulla velocità massima di throughput</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>Azzeramento su crediti buffer ottimali</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processo di puntamento</translation>
        </message>
    </context>
</TS>
